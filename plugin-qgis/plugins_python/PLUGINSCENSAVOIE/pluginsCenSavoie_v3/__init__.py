# -*- coding: utf-8 -*-
"""
/***************************************************************************
 pluginsCenSavoie
                                 A QGIS plugin
 Load plugins of CEN Savoie
                             -------------------
        begin                : 2016-09-08
        copyright            : (C) 2016 by CEN Savoie
        email                : sig@cen-savoie.org
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):      
    from .pluginsCenSavoie_v3 import pluginsCenSavoie_v3
    return pluginsCenSavoie_v3(iface)
