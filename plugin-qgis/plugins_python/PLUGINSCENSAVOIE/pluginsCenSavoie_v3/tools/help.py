# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CreateSessionAF
								 A QGIS plugin
 Permet de créer une nouvelle session d'animation foncière
							  -------------------
		begin				: 2015-10-02
		git sha			  : $Format:%H$
		copyright			: (C) 2015 by CEN Savoie
		email				: a.lesconnec@cen-savoie.org
 ***************************************************************************/

/***************************************************************************
 *																		 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or	 *
 *   (at your option) any later version.								   *
 *																		 *
 ***************************************************************************/
"""
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QAction
from qgis.core import *

# Initialize Qt resources from file resources.py
from pluginsCenSavoie_v3 import resources

import os.path
import sys 
import qgis
import os
import webbrowser

class help:
	"""QGIS Plugin Implementation."""

	def __init__(self, iface, cenToolbar):
		# Save reference to the QGIS interface
		self.iface = iface
		
		# initialize plugin directory
		self.plugin_dir = os.path.dirname(__file__)
				
		# Add toolbar button
		self.action= QAction(QIcon(":/plugins/pluginsCenSavoie_v3/icons/help.png"),  QCoreApplication.translate('help', u'Aide sur les plugins du CEN Savoie'),  self.iface.mainWindow())
		self.action.triggered.connect(self.run)
		cenToolbar.addAction(self.action)		

	def run(self):
		url= "http://192.168.100.4/Cen_Savoie/Intranet/sig/guide/outils_qgis.php#plugins"
		webbrowser.open_new(url)