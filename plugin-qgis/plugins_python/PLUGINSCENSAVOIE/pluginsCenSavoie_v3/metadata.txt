# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Plugins Cen Savoie
qgisMinimumVersion=2.99
qgisMaximumVersion=3.98
description=Load plugins of CEN Savoie
version=1.0
author=CEN Savoie
email=sig@cen-savoie.org

about=Ce plugin charge tous les plugins que l'utilisateur a le droit d'utiliser. This plugin load every plugins the user is allowed to use.

tracker=a
repository=a
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=a

homepage=a
category=Plugins
icon=icons/icon_base.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

