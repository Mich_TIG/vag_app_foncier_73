# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ParcelleToActe
								 A QGIS plugin
 Permet d'associer des parcelles à des actes
							  -------------------
		begin				: 2015-06-19
		git sha			  : $Format:%H$
		copyright			: (C) 2015 by CEN Savoie
		email				: a.lesconnec@cen-savoie.org
 ***************************************************************************/

/***************************************************************************
 *																		 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or	 *
 *   (at your option) any later version.								   *
 *																		 *
 ***************************************************************************/
"""
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

# Initialize Qt resources from file resources.py
from pluginsCenSavoie import resources

# Import the code for the dialog
from parcelle_to_acte_dialog import ParcelleToActeDialog

# Import du fichier de configuration
from confPython import bd_cen_host, bd_cen_port, bd_cen_name, bd_cen_mdp_1st_cnx

import os.path
import sys 
import qgis
import os
# import processing
import psycopg2 
import psycopg2.extras
import re
from time import localtime, strftime


class ParcelleToActe:
	"""QGIS Plugin Implementation."""

	def __init__(self, iface, cenToolbar):
		"""Constructor.

		:param iface: An interface instance that will be passed to this class
			which provides the hook by which you can manipulate the QGIS
			application at run time.
		:type iface: QgsInterface
		"""
		# Variable HOTE de l'accès à la base de données
		global host
		host = bd_cen_host
		global port
		port = bd_cen_port
		global bdd
		bdd = bd_cen_name
		global mdp_1st_cnx
		mdp_1st_cnx = bd_cen_mdp_1st_cnx
		
		# Save reference to the QGIS interface
		self.iface = iface
		self.canvas = self.iface.mapCanvas()
		# initialize plugin directory
		self.plugin_dir = os.path.dirname(__file__)
		
		# Create the dialog (after translation) and keep reference
		self.dlg = ParcelleToActeDialog()

		# Add toolbar button
		self.action= QAction(QIcon(":/plugins/pluginsCenSavoie/icons/parcelleToActe.png"),  QCoreApplication.translate('ParcelleToActe', u'Associer des parcelles à un acte'),  self.iface.mainWindow())
		self.action.triggered.connect(self.run)
		self.action.setEnabled(False)
		cenToolbar.addAction(self.action)
		self.canvas.selectionChanged.connect(self.toggle)

	def toggle(self):
		# get current active layer 
		layer = self.canvas.currentLayer()
		if layer and layer.type() == layer.VectorLayer:
			if layer.selectedFeatureCount() > 0:
				provider = layer.dataProvider()
				source_postgis = provider.dataSourceUri()
				source_postgis = source_postgis.replace('"', '')
				
				if (source_postgis.count('table=foncier.parcelles_dispo ') == 1):
					self.action.setEnabled(True)
				else:
					self.action.setEnabled(False)
			else:
				self.action.setEnabled(False)
		else:
			self.action.setEnabled(False)
		
	def changeCellValue(self, ligne, col):
		QObject.disconnect(self.dlg.tbl_detail, SIGNAL("cellClicked(int, int)"), self.changeCellValue)
		if self.dlg.tbl_detail.item(ligne, 2).text() == 'OUI' and col != 0:
			self.dlg.tbl_detail.setItem(ligne, 2, QTableWidgetItem('NON'))
			self.dlg.tbl_detail.item(ligne, 1).setBackground(QtGui.QColor(255, 145, 145))
			self.dlg.tbl_detail.item(ligne, 2).setBackground(QtGui.QColor(255, 145, 145))
		elif self.dlg.tbl_detail.item(ligne, 2).text() == 'NON' and col != 0:
			self.dlg.tbl_detail.setItem(ligne, 2, QTableWidgetItem('OUI'))
			self.dlg.tbl_detail.item(ligne, 1).setBackground(QtGui.QColor(115, 215, 115))
			self.dlg.tbl_detail.item(ligne, 2).setBackground(QtGui.QColor(115, 215, 115))
		self.dlg.tbl_detail.item(ligne, col).setSelected(False)	
		QObject.connect(self.dlg.tbl_detail, SIGNAL("cellClicked(int, int)"), self.changeCellValue)
		
	def run(self):
		"""Run method that performs all the real work"""
		# initialise le contenant du formulaire
		self.dlg.tbl_detail.setColumnCount(4)
		self.dlg.tbl_detail.setHorizontalHeaderItem(0, QTableWidgetItem('Par ID'))
		self.dlg.tbl_detail.setHorizontalHeaderItem(1, QTableWidgetItem('Lot ID'))
		self.dlg.tbl_detail.setHorizontalHeaderItem(2, QTableWidgetItem('A Associer'))
		self.dlg.tbl_detail.setHorizontalHeaderItem(3, QTableWidgetItem('cad_site_id'))
		self.dlg.tbl_detail.setColumnHidden(3, True)
		QObject.connect(self.dlg.tbl_detail, SIGNAL("cellClicked(int, int)"), self.changeCellValue)
		
		site_id = []
		tbl_dnulot = []
		tbl_cad_site_id = []
		self.canvas = self.iface.mapCanvas()
		global layer
		layer = self.canvas.currentLayer()
		rowCount = 0
		for feature in layer.selectedFeatures():
			site_id.append(feature["site_id"])
			tbl_dnulot = re.split(',', feature["tbl_dnulot"])
			if len(tbl_dnulot) > 0:
				j = 0
				while j < len(tbl_dnulot):
					rowCount += 1
					j+=1
		self.dlg.tbl_detail.setRowCount(rowCount)
		
		if rowCount > 0:
			ligne = 0
			col = 0
			for feature in layer.selectedFeatures():
				tbl_dnulot = re.split(',', feature["tbl_dnulot"])
				tbl_cad_site_id = re.split(',', feature["tbl_cad_site_id"])
				if len(tbl_dnulot) > 0:
					self.dlg.tbl_detail.setItem(ligne, 0, QTableWidgetItem(feature["par_id"]))
					self.dlg.tbl_detail.setSpan(ligne, 0, len(tbl_dnulot), 1)
					j = 0
					while j < len(tbl_dnulot):
						self.dlg.tbl_detail.setItem(ligne, 1, QTableWidgetItem(tbl_dnulot[j]))
						self.dlg.tbl_detail.item(ligne, 1).setBackground(QtGui.QColor(115, 215, 115))
						self.dlg.tbl_detail.setItem(ligne, 2, QTableWidgetItem('OUI'))
						self.dlg.tbl_detail.item(ligne, 2).setBackground(QtGui.QColor(115, 215, 115))
						self.dlg.tbl_detail.setItem(ligne, 3, QTableWidgetItem(tbl_cad_site_id[j]))
						ligne += 1
						j+=1
			self.dlg.tbl_detail.setEnabled(True)
			self.dlg.no_lot.setVisible(0)
		else:
			self.dlg.tbl_detail.setEnabled(False)
			self.dlg.no_lot.setVisible(1)
		
		self.dlg.tbl_detail.resizeColumnsToContents()
				
		if (len(set(site_id)) > 1):
			QMessageBox.warning(None, "Oups:", 'Selection invalide')			
		else:
			first_conn = psycopg2.connect("host=" + host + " port=" + port + " dbname=" + bdd + " user=first_cnx password=" + mdp_1st_cnx)
			first_cur = first_conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
			first_cur.execute("SELECT mdp, usename FROM pg_catalog.pg_user t1, admin_sig.utilisateurs t2 WHERE t2.oid = t1.usesysid AND utilisateur_id = '" + os.environ["USERNAME"] + "'")
			res_ident = first_cur.fetchone()
			mdp = res_ident[0].decode('base64') 
			user = res_ident[1]
			first_conn.close()
			tbl_site_id = list(set(site_id))
			self.dlg.InitFormulaire(tbl_site_id, 'load', host, port, bdd, user, mdp)
			self.dlg.img_logo.setPixmap(QPixmap(":/plugins/pluginsCenSavoie/tools/images/Logo_CEN_Savoie.png"))
			self.dlg.img_frise.setPixmap(QPixmap(":/plugins/pluginsCenSavoie/tools/images/Frise_portrait.png"))
			self.dlg.img_frise.stackUnder(self.dlg.button_box)
			# show the dialog
			self.dlg.show()
				
			# Run the dialog event loop
			result = self.dlg.exec_()
			# See if OK was pressed
			if result:
				# Do something useful here - delete the line containing pass and
				# substitute with your code.			
				conn = psycopg2.connect("host=" + host + " port=" + port + " dbname=" + bdd + " user=" + user + " password=" + mdp)
				cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
				mfu_id = self.dlg.lst_actes.currentText()
				mfu_table = self.dlg.type_acte_sel.text()
				aujourdhui = strftime("%Y-%m-%d %H:%M:%S", localtime())
				maj_table = 'NON'
				
				for feature in layer.selectedFeatures():
					tbl_dnulot = re.split(',', feature["tbl_dnulot"])
					tbl_cad_site_id = re.split(',', feature["tbl_cad_site_id"])
					if len(tbl_dnulot) == 0:
						if mfu_table == 'r_cad_site_mu':
							rq_insert = "INSERT INTO foncier.r_cad_site_mu (cad_site_id, mu_id, date_entree, date_sortie) VALUES ("+feature["tbl_cad_site_id"]+", '"+mfu_id+"', '"+aujourdhui+"', 'N.D.')"
							cur.execute(rq_insert)
							conn.commit()
							maj_table = 'OUI'
						elif mfu_table == 'r_cad_site_mf':
							rq_insert = "INSERT INTO foncier.r_cad_site_mf (cad_site_id, mf_id, date_entree, date_sortie) VALUES ("+feature["tbl_cad_site_id"]+", '"+mfu_id+"', '"+aujourdhui+"', 'N.D.')"
							cur.execute(rq_insert)
							conn.commit()
							maj_table = 'OUI'
							
				for i in range(self.dlg.tbl_detail.rowCount()):
					if mfu_table == 'r_cad_site_mu' and self.dlg.tbl_detail.item(i, 2).text() == 'OUI':
						rq_insert = "INSERT INTO foncier.r_cad_site_mu (cad_site_id, mu_id, date_entree, date_sortie) VALUES ("+self.dlg.tbl_detail.item(i, 3).text()+", '"+mfu_id+"', '"+aujourdhui+"', 'N.D.')"
						cur.execute(rq_insert)
						conn.commit()
						maj_table = 'OUI'
					elif mfu_table == 'r_cad_site_mf' and self.dlg.tbl_detail.item(i, 2).text() == 'OUI':
						rq_insert = "INSERT INTO foncier.r_cad_site_mf (cad_site_id, mf_id, date_entree, date_sortie) VALUES ("+self.dlg.tbl_detail.item(i, 3).text()+", '"+mfu_id+"', '"+aujourdhui+"', 'N.D.')"
						cur.execute(rq_insert)
						conn.commit()
						maj_table = 'OUI'
						
				if maj_table == 'OUI':
					if mfu_table == 'r_cad_site_mu':
						rq_update = "UPDATE foncier.mu_conventions SET maj_date = '"+aujourdhui+"', maj_user = '"+os.environ["USERNAME"]+"' WHERE mu_id = '"+mfu_id+"'"
					elif mfu_table == 'r_cad_site_mf':
						rq_update = "UPDATE foncier.mf_acquisitions SET maj_date = '"+aujourdhui+"', maj_user = '"+os.environ["USERNAME"]+"' WHERE mf_id = '"+mfu_id+"'"
									
					cur.execute(rq_update)
					conn.commit()
				conn.close()	
				self.canvas.refresh()