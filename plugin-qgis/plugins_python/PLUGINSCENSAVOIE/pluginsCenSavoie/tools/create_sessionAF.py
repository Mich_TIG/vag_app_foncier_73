# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CreateSessionAF
								 A QGIS plugin
 Permet de créer une nouvelle session d'animation foncière
							  -------------------
		begin				: 2015-10-02
		git sha			  : $Format:%H$
		copyright			: (C) 2015 by CEN Savoie
		email				: a.lesconnec@cen-savoie.org
 ***************************************************************************/

/***************************************************************************
 *																		 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or	 *
 *   (at your option) any later version.								   *
 *																		 *
 ***************************************************************************/
"""
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *

# Initialize Qt resources from file resources.py
from pluginsCenSavoie import resources

# Import the code for the dialog
from create_sessionAF_dialog import CreateSessionAFDialog

# Import du fichier de configuration
from confPython import bd_cen_host, bd_cen_port, bd_cen_name, bd_cen_mdp_1st_cnx

import os.path
import sys 
import qgis
import os
import psycopg2 
import psycopg2.extras
import re
from time import localtime, strftime


class CreateSessionAF:
	"""QGIS Plugin Implementation."""

	def __init__(self, iface, cenToolbar):
		"""Constructor.

		:param iface: An interface instance that will be passed to this class
			which provides the hook by which you can manipulate the QGIS
			application at run time.
		:type iface: QgsInterface
		"""
		# Variable HOTE de l'accès à la base de données
		global host
		host = bd_cen_host
		global port
		port = bd_cen_port
		global bdd
		bdd = bd_cen_name
		global mdp_1st_cnx
		mdp_1st_cnx = bd_cen_mdp_1st_cnx
		
		# Save reference to the QGIS interface
		self.iface = iface
		self.canvas = self.iface.mapCanvas()

		# initialize plugin directory
		self.plugin_dir = os.path.dirname(__file__)
		
		# Create the dialog (after translation) and keep reference
		self.dlg = CreateSessionAFDialog()
		
		# Add toolbar button
		self.action= QAction(QIcon(":/plugins/pluginsCenSavoie/icons/createSessionAF.png"),  QCoreApplication.translate('CreateSessionAF', u'Créer une Session d''AF'),  self.iface.mainWindow())
		self.action.triggered.connect(self.run)
		self.action.setEnabled(False)
		cenToolbar.addAction(self.action)		
		self.canvas.selectionChanged.connect(self.toggle)

	def toggle(self):
		layer = self.canvas.currentLayer()
		if layer and layer.type() == layer.VectorLayer:
			if layer.selectedFeatureCount() > 0:
				provider = layer.dataProvider()
				source_postgis = provider.dataSourceUri()
				source_postgis = source_postgis.replace('"', '')				
				if (source_postgis.count('table=animation_fonciere.parcelles_dispo_saf') == 1):
					self.action.setEnabled(True)
					global user, mdp
					first_conn = psycopg2.connect("host=" + host + " port=" + port + " dbname=" + bdd + " user=first_cnx password=" + mdp_1st_cnx)
					first_cur = first_conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
					first_cur.execute("SELECT mdp, usename FROM pg_catalog.pg_user t1, admin_sig.utilisateurs t2 WHERE t2.oid = t1.usesysid AND utilisateur_id = '" + os.environ["USERNAME"] + "'")
					res_ident = first_cur.fetchone()
					mdp = res_ident[0].decode('base64') 
					user = res_ident[1]
					first_conn.close()
				else:
					self.action.setEnabled(False)
			else:
				self.action.setEnabled(False)
		else:
			self.action.setEnabled(False)	

	def changeCellValue(self, ligne, col):
		conn = psycopg2.connect("host=" + host + " port=" + port + " dbname=" + bdd + " user=" + user + " password=" + mdp)
		cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
		if self.dlg.tbl_parcelle.item(ligne, 1).text() == '1' and col != 0:
			self.dlg.tbl_parcelle.setItem(ligne, 1, QTableWidgetItem('2'))
		elif self.dlg.tbl_parcelle.item(ligne, 1).text() == '2' and col != 0:
			self.dlg.tbl_parcelle.setItem(ligne, 1, QTableWidgetItem('3'))
		elif self.dlg.tbl_parcelle.item(ligne, 1).text() == '3' and col != 0:
			self.dlg.tbl_parcelle.setItem(ligne, 1, QTableWidgetItem('1'))
		if col == 0:
			feature_gid = self.dlg.tbl_parcelle.item(ligne, 0).text()		
			cur.execute("SELECT st_xmin(st_buffer(t1.geom,10)) as xmin, st_xmax(st_buffer(t1.geom,10)) as xmax, st_ymin(st_buffer(t1.geom,10)) as ymin, st_ymax(st_buffer(t1.geom,10)) as ymax FROM animation_fonciere.parcelles_dispo_saf t1 WHERE t1.par_id = '"+ self.dlg.tbl_parcelle.item(ligne, 0).text() + "'")
			coord = cur.fetchone()
			xmin = coord[0]
			ymin = coord[2]
			xmax = coord[1]
			ymax = coord[3]
			self.canvas.setExtent(QgsRectangle(xmin, ymin, xmax, ymax))
			self.canvas.refresh()
		self.dlg.tbl_parcelle.item(ligne, col).setSelected(False)
		conn.close()

	def run(self):
		"""Run method that performs all the real work"""
		layer = self.canvas.currentLayer()
		if layer and layer.type() == layer.VectorLayer:
			provider = layer.dataProvider()
			source_postgis = provider.dataSourceUri()
			source_postgis = source_postgis.replace('"', '')				
			if (source_postgis.count('table=animation_fonciere.parcelles_dispo_saf') != 1):
				QMessageBox.critical(None, "Attention", "Vous devez sélectionnez des objets depuis la vue 'parcelles_dispo_saf'")
			else:	
				if layer.selectedFeatureCount() < 1:
					QMessageBox.critical(None, "Attention", "Vous devez sélectionnez au moins une parcelle")
				else:
					self.dlg.tbl_parcelle.setColumnCount(3)
					self.dlg.tbl_parcelle.setHorizontalHeaderItem(0, QTableWidgetItem('Par ID'))
					self.dlg.tbl_parcelle.setHorizontalHeaderItem(1, QTableWidgetItem('Priorite'))
					self.dlg.tbl_parcelle.setHorizontalHeaderItem(2, QTableWidgetItem('cad_site_id'))
					QObject.connect(self.dlg.tbl_parcelle, SIGNAL("cellClicked(int, int)"), self.changeCellValue)
							
					site_id = []
					self.canvas = self.iface.mapCanvas()
					
					rowCount = 0
					for feature in layer.selectedFeatures():
						site_id.append(feature["site_id"])
						rowCount += 1
					self.dlg.tbl_parcelle.setRowCount(rowCount)
					
					if rowCount > 0:
						ligne = 0
						col = 0
						for feature in layer.selectedFeatures():
							self.dlg.tbl_parcelle.setItem(ligne, 0, QTableWidgetItem(feature["par_id"]))
							self.dlg.tbl_parcelle.setItem(ligne, 1, QTableWidgetItem('1'))
							self.dlg.tbl_parcelle.setItem(ligne, 2, QTableWidgetItem(feature["tbl_cad_site_id"]))
							ligne += 1
						self.dlg.tbl_parcelle.setEnabled(True)
					else:
						self.dlg.tbl_parcelle.setEnabled(False)
					self.dlg.tbl_parcelle.hideColumn(2)
					self.dlg.tbl_parcelle.resizeColumnsToContents()
					
					rq_saf_geom = "SELECT st_multi(st_union(geom)) as geom FROM animation_fonciere.parcelles_dispo_saf WHERE par_id IN ("
					for feature in layer.selectedFeatures():
						rq_saf_geom += "'" + str(feature["par_id"]) + "',"
					rq_saf_geom = rq_saf_geom[0:(len(rq_saf_geom)-1)] + ")"
						
					tbl_site_id = list(set(site_id))
					self.dlg.InitFormulaire(tbl_site_id, rq_saf_geom, host, port, bdd, user, mdp)
					self.dlg.img_logo.setPixmap(QPixmap(":/plugins/pluginsCenSavoie/tools/images/Logo_CEN_Savoie.png"))
					self.dlg.lbl_fond.setPixmap(QPixmap(":/plugins/pluginsCenSavoie/tools/images/fond_70.png"))
					self.dlg.lbl_fond.stackUnder(self.dlg.button_box)
												
					# show the dialog
					self.dlg.show()
					# Run the dialog event loop
					result = self.dlg.exec_()
					
					# See if OK was pressed
					if result:
						conn = psycopg2.connect("host=" + host + " port=" + port + " dbname=" + bdd + " user=" + user + " password=" + mdp)
						cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
												
						rq_dnuper = "SELECT DISTINCT t6.dnuper FROM cadastre.r_prop_cptprop_cen t6 JOIN cadastre.cptprop_cen t5 USING (dnupro) JOIN cadastre.cadastre_cen t3 USING (dnupro) JOIN cadastre.lots_cen t2 USING (lot_id) WHERE t6.dnuper NOT IN (SELECT DISTINCT dnuper FROM animation_fonciere.rel_proprio_interloc) AND t3.date_fin = 'N.D.' AND t2.par_id IN ("
						for r in range(self.dlg.tbl_parcelle.rowCount()):
							if self.dlg.tbl_parcelle.isRowHidden(r) == False:
								rq_dnuper += "'" + str(self.dlg.tbl_parcelle.item(r, 0).text()) + "',"								
						rq_dnuper = rq_dnuper[0:(len(rq_dnuper)-1)] + ")"
						# QMessageBox.information(None, "Attention",str(rq_dnuper))
						cur.execute(rq_dnuper)
						res_dnuper = cur.fetchall()
						i=0
						while i < len(res_dnuper): #Boucle sur toutes les options de niveau 1
							rq_new_interloc_id = "SELECT nextval('animation_fonciere.interloc_id_seq'::regclass)";
							cur.execute(rq_new_interloc_id)
							res_new_interloc_id = cur.fetchone()
							new_interloc_id = res_new_interloc_id[0]	
							
							rq_insert_interloc = """
							INSERT INTO animation_fonciere.interlocuteurs (
								interloc_id,
								etat_interloc_id,
								dnuper,
								ccoqua,
								ddenom,
								nom_usage,
								nom_jeunefille,
								prenom_usage,
								nom_conjoint,
								prenom_conjoint,
								dnomlp,
								dprnlp,
								epxnee,
								dnomcp,
								dprncp,
								jdatnss,
								dldnss,
								dlign3,
								dlign4,
								dlign5,
								dlign6,
								email,
								fixe_domicile,
								fixe_travail,
								portable_domicile,
								portable_travail,
								fax,
								gtoper,
								ccogrm,
								dnatpr,
								dsglpm,
								commentaires,
								annee_matrice,
								maj_user,
								maj_date)
							(SELECT 
								""" + str(new_interloc_id) + """, 
								1, 
								t1.dnuper,
								t1.ccoqua,
								t1.ddenom,
								t1.nom_usage,
								t1.nom_jeunefille,
								t1.prenom_usage,
								t1.nom_conjoint,
								t1.prenom_conjoint,
								t1.dnomlp,
								t1.dprnlp,
								t1.epxnee,
								t1.dnomcp,
								t1.dprncp,
								t1.jdatnss,
								t1.dldnss,
								t1.dlign3,
								t1.dlign4,
								t1.dlign5,
								t1.dlign6,
								t1.email,
								t1.fixe_domicile,
								t1.fixe_travail,
								t1.portable_domicile,
								t1.portable_travail,
								t1.fax,
								t1.gtoper,
								t1.ccogrm,
								t1.dnatpr,
								t1.dsglpm,
								t1.commentaires,
								t1.annee_matrice,
								t1.maj_user,
								t1.maj_date 
							FROM cadastre.proprios_cen t1
							WHERE t1.dnuper = '""" + res_dnuper[i][0] + """')"""
							# QMessageBox.information(None, "Attention",str(rq_insert_interloc))
							cur.execute(rq_insert_interloc)
							conn.commit()
							
							rq_insert_rel_proprio_interloc = "INSERT INTO animation_fonciere.rel_proprio_interloc (dnuper, interloc_id) VALUES ('" + str(res_dnuper[i][0]) + "', " + str(new_interloc_id) + ")"
							cur.execute(rq_insert_rel_proprio_interloc)
							conn.commit()
							
							i=i+1
							
						if self.dlg.saf_maj.text() == 'N.P.':
							rq_new_session_af_id = "SELECT nextval('animation_fonciere.session_af_id_seq'::regclass)";
							cur.execute(rq_new_session_af_id)
							res_new_session_af_id = cur.fetchone()
							session_af_id = res_new_session_af_id[0]						
													
							rq_insert_saf = "INSERT INTO animation_fonciere.session_af (session_af_id, session_af_lib, date_creation, date_maj, utilisateur, origine_id, geom) VALUES (" + str(session_af_id) + ", '" + str(self.dlg.saf_name.text()) + "', '" + str(self.dlg.saf_date.text()) + "', '" + str(self.dlg.saf_date.text()) + "', '" + str(self.dlg.saf_utilisateur.text()) + "', '" + str(self.dlg.saf_origin.text()) + "', (" + str(rq_saf_geom) + "))"
							# QMessageBox.information(None, "Attention",str(rq_insert_saf))
							cur.execute(rq_insert_saf)
							conn.commit()
						else :
							session_af_id = self.dlg.saf_maj.text()
						
						for r in range(self.dlg.tbl_parcelle.rowCount()):
							for cad_site_id in self.dlg.tbl_parcelle.item(r, 2).text().split(','):
								rq_check_rel_eaf_foncier = "SELECT rel_eaf_foncier_id FROM animation_fonciere.rel_eaf_foncier WHERE cad_site_id = " + str(cad_site_id)
								cur.execute(rq_check_rel_eaf_foncier)
								res_check_rel_eaf_foncier = cur.fetchone()								
								if cur.rowcount == 1 :
									rel_eaf_foncier_id = res_check_rel_eaf_foncier[0]
									if self.dlg.saf_maj.text() == 'N.P.':
										rq_reset_prix_eaf = "UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id = " + str(rel_eaf_foncier_id) + " AND session_af_id =  " + str(session_af_id)
										# QMessageBox.information(None, "Attention",str(rq_insert_saf))
										cur.execute(rq_reset_prix_eaf)
										conn.commit()											
								else:
									rq_new_rel_eaf_foncier_id = "SELECT nextval('animation_fonciere.rel_eaf_foncier_id_seq'::regclass)";
									cur.execute(rq_new_rel_eaf_foncier_id)
									res_new_rel_eaf_foncier_id = cur.fetchone()
									rel_eaf_foncier_id = res_new_rel_eaf_foncier_id[0]
								
									rq_eaf_id = "SELECT animation_fonciere.f_get_eaf('" + str(cad_site_id) + "')"
									cur.execute(rq_eaf_id)
									if cur.rowcount == 1:
										res_eaf_id = cur.fetchone()
										entite_af_id = res_eaf_id[0]
									elif cur.rowcount > 1:
										res_eaf_id = cur.fetchall()
										e=0
										tbl_eaf = []
										while e < len(res_eaf_id):
											tbl_eaf.append(str(res_eaf_id[e][0]))
											e=e+1
										entite_af_id = tbl_eaf.pop(0)
										lst_eaf = ",".join(str(x) for x in tbl_eaf)
										
										rq_update_bad_eaf = """
										UPDATE
											animation_fonciere.rel_eaf_foncier 
										SET 
											entite_af_id = """+entite_af_id+"""
										WHERE
											entite_af_id IN ("""+str(lst_eaf)+""")"""
										cur.execute(rq_update_bad_eaf)
										
										rq_reset_prix_eaf = """
										UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
											(
												SELECT 
													rel_eaf_foncier_id
												FROM
													animation_fonciere.rel_eaf_foncier
													JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
												WHERE
													session_af_id = """+session_af_id+""" AND (entite_af_id = """+entite_af_id+""" OR entite_af_id IN ("""+str(lst_eaf)+"""))
											)"""
										cur.execute(rq_reset_prix_eaf)
										
										rq_delete_entite_af = """
										DELETE FROM animation_fonciere.entite_af WHERE entite_af_id NOT IN (SELECT entite_af_id FROM animation_fonciere.rel_eaf_foncier)""";
										cur.execute(rq_delete_entite_af)
									else:
										rq_new_eaf_id = "SELECT nextval('animation_fonciere.entite_af_id_seq'::regclass)";
										cur.execute(rq_new_eaf_id)
										res_new_eaf_id = cur.fetchone()
										entite_af_id = res_new_eaf_id[0]
										rq_insert_entite_af = "INSERT INTO animation_fonciere.entite_af (entite_af_id) VALUES (" + str(entite_af_id) + ")"
										# QMessageBox.information(None, "Attention",str(rq_insert_entite_af))
										cur.execute(rq_insert_entite_af)
										conn.commit()
									
									rq_insert_rel_eaf_foncier = "INSERT INTO animation_fonciere.rel_eaf_foncier (rel_eaf_foncier_id, cad_site_id, entite_af_id) VALUES (" +str(rel_eaf_foncier_id)+","+str(cad_site_id)+","+str(entite_af_id)+")"
									# QMessageBox.information(None, "Attention",str(rq_insert_rel_eaf_foncier))
									cur.execute(rq_insert_rel_eaf_foncier)
									conn.commit()	
							
									rq_check_rel_saf_foncier = "SELECT rel_saf_foncier_id FROM animation_fonciere.rel_saf_foncier WHERE rel_eaf_foncier_id = " + str(rel_eaf_foncier_id) + " AND session_af_id = " + str(session_af_id)
									cur.execute(rq_check_rel_saf_foncier)
									res_check_rel_saf_foncier = cur.fetchone()								
									if cur.rowcount == 0 :
										rq_new_rel_saf_foncier_id = "SELECT nextval('animation_fonciere.rel_saf_foncier_id_seq'::regclass)";
										cur.execute(rq_new_rel_saf_foncier_id)
										res_new_rel_saf_foncier_id = cur.fetchone()
										rel_saf_foncier_id = res_new_rel_saf_foncier_id[0]								
										
										rq_insert_rel_saf_foncier = "INSERT INTO animation_fonciere.rel_saf_foncier (rel_saf_foncier_id, rel_eaf_foncier_id, session_af_id, paf_id) VALUES (" + str(rel_saf_foncier_id) + "," + str(rel_eaf_foncier_id) + "," + str(session_af_id) + ", " + str(self.dlg.tbl_parcelle.item(r, 1).text())  + ")"
										# QMessageBox.information(None, "Attention",str(rq_insert_rel_saf_foncier))
										cur.execute(rq_insert_rel_saf_foncier)
										conn.commit()	
									
									rq_lst_interloc_id = """
										SELECT DISTINCT 
											interloc_id,
											COALESCE(ccodro, 'N.P.') as ccodro,
											COALESCE(ccodem, 'N.P.') as ccodem
										FROM
											animation_fonciere.rel_proprio_interloc
											JOIN cadastre.proprios_cen USING (dnuper)
											JOIN cadastre.r_prop_cptprop_cen USING (dnuper)
											JOIN cadastre.cptprop_cen USING (dnupro)
											JOIN cadastre.cadastre_cen USING (dnupro)
											JOIN foncier.cadastre_site USING (cad_cen_id)
										WHERE cad_site_id = """ + str(cad_site_id)
									cur.execute(rq_lst_interloc_id)
									res_lst_interloc_id = cur.fetchall()
									i=0
									while i < len(res_lst_interloc_id):
										rq_check_rel_saf_foncier_interlocs = "SELECT rel_saf_foncier_interlocs_id FROM animation_fonciere.rel_saf_foncier_interlocs WHERE interloc_id = " + str(res_lst_interloc_id[i][0]) + " AND rel_saf_foncier_id = " + str(rel_saf_foncier_id) + " AND type_interloc_id = 1"
										cur.execute(rq_check_rel_saf_foncier_interlocs)
										res_check_rel_saf_foncier_interlocs = cur.fetchone()								
										if cur.rowcount == 0 :
											rq_insert_rel_saf_foncier_interlocs = "INSERT INTO animation_fonciere.rel_saf_foncier_interlocs (interloc_id, rel_saf_foncier_id, type_interloc_id, ccodro, ccodem) VALUES (" + str(res_lst_interloc_id[i][0]) + "," + str(rel_saf_foncier_id)+", 1, CASE WHEN '" + str(res_lst_interloc_id[i][1]) + "' = 'N.P.' THEN NULL ELSE '" + str(res_lst_interloc_id[i][1]) + "' END, CASE WHEN '" + str(res_lst_interloc_id[i][2]) + "' = 'N.P.' THEN NULL ELSE '" + str(res_lst_interloc_id[i][2]) + "' END)"
											# QMessageBox.information(None, "Attention",str(rq_insert_rel_saf_foncier_interlocs))
											cur.execute(rq_insert_rel_saf_foncier_interlocs)
											conn.commit()									
										i=i+1
									
						if self.dlg.saf_maj.text() != 'N.P.':
							rq_update_saf_geom = """
								UPDATE animation_fonciere.session_af SET geom = (
									SELECT 
										st_multi(st_union(t1.geom)) as geom
									FROM
										cadastre.lots_cen t2
										JOIN cadastre.parcelles_cen t1 USING (par_id)
										JOIN cadastre.cadastre_cen t3 USING (lot_id)
										JOIN foncier.cadastre_site t4 USING (cad_cen_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
									WHERE 
										rel_saf_foncier.session_af_id = """+str(session_af_id)+""" AND
										t3.date_fin = 'N.D.' AND
										t4.date_fin = 'N.D.')
								WHERE session_af_id = """+str(session_af_id)
							cur.execute(rq_update_saf_geom)
							conn.commit()	
									
						conn.close()	
						self.canvas.refresh()