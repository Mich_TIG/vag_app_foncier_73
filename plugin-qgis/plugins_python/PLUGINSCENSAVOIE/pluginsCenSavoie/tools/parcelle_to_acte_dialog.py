# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ParcelleToActeDialog
								 A QGIS plugin
 Permet d'associer des parcelles à des actes
							 -------------------
		begin				: 2015-06-19
		git sha			  : $Format:%H$
		copyright			: (C) 2015 by CEN Savoie
		email				: a.lesconnec@cen-savoie.org
 ***************************************************************************/

/***************************************************************************
 *																		 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or	 *
 *   (at your option) any later version.								   *
 *																		 *
 ***************************************************************************/
"""

import os
import psycopg2 
import psycopg2.extras
from PyQt4 import QtGui, uic
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from functools import partial 
import datetime
from time import gmtime, strftime

FORM_CLASS, _ = uic.loadUiType(os.path.join(
	os.path.dirname(__file__), 'parcelle_to_acte_dialog_base.ui'))


class ParcelleToActeDialog(QtGui.QDialog, FORM_CLASS):
	def __init__(self, parent=None):
		"""Constructor."""
				
		super(ParcelleToActeDialog, self).__init__(parent)
		QtGui.QDialog.__init__(self)
		# Set up the user interface from Designer.
		# After setupUI you can access any designer object by doing
		# self.<objectname>, and you can use autoconnect slots - see
		# http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
		# #widgets-and-dialogs-with-auto-connect
		self.setupUi(self)
	
	def InitFormulaire(self, site_id, acces, host, port, bdd, user, mdp):		
		QObject.connect(self.chk_convention, SIGNAL("clicked()"), partial(self.InitFormulaire, site_id = site_id, acces='reload', user = user, mdp = mdp))
		QObject.connect(self.chk_acquisition, SIGNAL("clicked()"), partial(self.InitFormulaire, site_id = site_id, acces='reload', user = user, mdp = mdp))
		QObject.connect(self.flt_date_today, SIGNAL("clicked()"), partial(self.InitFormulaire, site_id = site_id, acces='reload', user = user, mdp = mdp))
		QObject.connect(self.flt_date_week, SIGNAL("clicked()"), partial(self.InitFormulaire, site_id = site_id, acces='reload', user = user, mdp = mdp))
		QObject.connect(self.flt_date_month, SIGNAL("clicked()"), partial(self.InitFormulaire, site_id = site_id, acces='reload', user = user, mdp = mdp))
		QObject.connect(self.flt_date_unknown, SIGNAL("clicked()"), partial(self.InitFormulaire, site_id = site_id, acces='reload', user = user, mdp = mdp))
		QObject.connect(self.lst_actes, SIGNAL("currentIndexChanged(int)"), self.change_lst_acte)
		if acces == 'load':
			self.chk_convention.setChecked(1)
			self.chk_acquisition.setChecked(1)
			self.flt_date_unknown.setChecked(1)
			
		self.no_acte.setVisible(0)
		self.type_acte_sel.setVisible(0)
		self.button_box.setEnabled(False)
		conn = psycopg2.connect("host=" + host + " port=" + port + " dbname=" + bdd + " user=" + user + " password=" + mdp)
		cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
		self.lst_actes.clear() #Vidage de la combobox
		k = 0
		aujourdhui = strftime("%Y-%m-%d", gmtime())
		this_week = strftime("%U", gmtime())
		tbl_site_id = site_id
		
		while k < len(tbl_site_id):
			cur_site_id = tbl_site_id[k]
			k = k + 1
			rq_lst_mfu = "SELECT * FROM (SELECT DISTINCT 'Acquisition' as type_acte, t4.site_id, mf_acquisitions.mf_id as acte_id, mf_acquisitions.mf_lib as acte_lib, d_typmf.typmf_lib as acte_typ, mf_acquisitions.date_sign_acte as acte_date, d_statutmf.statutmf as acte_statut, mf_acquisitions.maj_date as maj_date FROM foncier.cadastre_site t4, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf, (SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1	WHERE t4.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id AND (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id) UNION SELECT DISTINCT 'Convention' as type_acte, t4.site_id, mu_conventions.mu_id as acte_id, mu_conventions.mu_lib as acte_lib, d_typmu.typmu_lib as acte_typ, mu_conventions.date_effet_conv as acte_date, d_statutmu.statutmu as acte_statut, mu_conventions.maj_date as maj_date FROM foncier.cadastre_site t4, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt1 WHERE t4.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id AND (r_mustatut.date = tt1.statut_date AND r_mustatut.mu_id = tt1.mu_id) ORDER BY type_acte , acte_date) tt1 WHERE site_id = '"+cur_site_id+"'"
			
			if self.chk_convention.isChecked() == 0:
				rq_lst_mfu = rq_lst_mfu + " AND type_acte != 'Convention'"
			
			if self.chk_acquisition.isChecked() == 0:
				rq_lst_mfu = rq_lst_mfu + " AND type_acte != 'Acquisition'"
				
			if self.flt_date_today.isChecked() == 1:
				rq_lst_mfu = rq_lst_mfu + " AND LEFT(maj_date, 10) = '"+aujourdhui+"'"
			elif self.flt_date_week.isChecked() == 1:
				rq_lst_mfu = rq_lst_mfu + " AND EXTRACT(WEEK FROM to_date(maj_date, 'YYYY-MM-DD')) = " + this_week
			elif self.flt_date_month.isChecked() == 1:
				rq_lst_mfu = rq_lst_mfu + " AND TO_DATE(maj_date, 'YYYY-MM-DD') BETWEEN TO_DATE('"+strftime("%Y-%m", gmtime())+"-01', '%Y-%m-%d') AND TO_DATE('"+strftime("%Y-%m", gmtime())+"-31', 'YYYY-MM-DD')"
				
			cur.execute(rq_lst_mfu)
			global res_lst_mfu
			res_lst_mfu = cur.fetchall() #requête de récupération des options possible de niveau 2 à partir du niveau 1 sélectionné
			
			if len(res_lst_mfu) > 0:
				self.no_acte.setVisible(0)
				self.lst_actes.setVisible(1)
				
				i = 0 #Déclaration à 0 d'une variable pour boucler sur toutes les options de la requête identifiée précédement
				while i < len(res_lst_mfu): #Boucle sur toutes les options de niveau 2
					lib = res_lst_mfu[i][2] #Déclaration de la variable stockant le libellé complet de la requête
					# QMessageBox.warning(None, "Oups:", str(lib))
					self.lst_actes.addItem(lib) #Ajout à la combobox de l'option en cours de bouclage
					i=i+1 #implémentation de la variable permettant de suivre le bon index de la combobox
				self.lst_actes.setCurrentIndex(-1)
			else :
				self.no_acte.setVisible(1)
				self.lst_actes.setVisible(0)
		conn.close()
	
	def change_lst_acte(self, index):
		if self.lst_actes.currentIndex() != -1:
			self.button_box.setEnabled(True)
		else:
			self.button_box.setEnabled(False)
			
		if res_lst_mfu[index][0] == 'Acquisition':
			self.type_acte_sel.setText('r_cad_site_mf')
		elif res_lst_mfu[index][0] == 'Convention':
			self.type_acte_sel.setText('r_cad_site_mu')