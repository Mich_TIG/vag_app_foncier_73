/**************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
Copyright Conservatoire d’espaces naturels de Savoie, 2018
-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
-	Adresse électronique : sig at cen-savoie.org

Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
-	de gérer et suivre ces actes (convention et acquisition) ;
-	et d’en établir des bilans.
-	Un module permet de réaliser des sessions d’animation foncière.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
***************************************************************************************************************************
***************************************************************************************************************************
**************************************************************************************************************************/

function showHelp(elt) {
	$('.help_mask').removeClass('help_mask');
	$('#help_msg').css('display', 'none');
	$('#help_evidence').css('display', 'none');		
	if(elt.id=='body2') {		
		$('#body2').css('display', 'none');
		$('#explorateur_evenement').css('display', 'none');
		$('#explorateur_gestion_substitution_sousdiv').css('display', 'none');	
		$('#explorateur_gestion_referent_sousdiv').css('display', 'none');	
		$('#explorateur_prix_occsol').css('display', 'none');	
		$('#explorateur_interloc').css('display', 'none');
		$('#type_occsol-25-3 option:eq(2)').prop('selected', true);
		$('#pourc_occsol-25-3').val('40');
		$('#prix_occsol-25-3').val('0.6');	
		$('#lbl_prix_total_occsol-22').css('display', '');
		$('.anime_help').css('display', 'none');
		$('#explorateur_echange').css('display', 'none');
		$('#tbody_echange_resultat').css('display', '');
		$('#tr_ech_rappel').css('display', 'none');
		$('#date_echange').val('06/09/2017');
		$('#lst_type_echange option:selected').index(3)
		$('#description_echange').html("Retour sur le groupe et discussion avec M. Picsou sur les évolutions possibles.");
		$('#chasseur_22').css('display', 'none');
		$('#img_arrow').css('display', 'none');
		$('#img_arrow').css('left', '0px');
		$('#img_arrow').css('right', '0px');
		$('#td_objectif_22').css('background', '#747474')
		$('#td_objectif_22').css('opacity', 1)
		$('#label_objectif_22').html('')
		var onglet;
		onglet = $('.tabs-selected').find('a').attr('href').replace('#', '');		
		$('.img_help_'+onglet).css('display', '');		
		$('.img_tip_'+onglet).css('display', '');
		$('#help_msg').width('auto');
	} else if (elt.id=='glossaire'){
		$('html,body').animate({scrollTop: 0}, 'fast');
		$('#body2').css('display', 'none');	
		$('#explorateur_gestion_substitution_sousdiv').css('display', 'none');	
		$('#explorateur_gestion_referent_sousdiv').css('display', 'none');	
		$('.anime_help').css('display', 'none');
		$('#explorateur_glossaire').css('display', '');
	} else {
		$('#body2').css('display', '');	
		var msg = '';
		var evidence_width = 0;
		switch (elt.id) {
			case 'img_help_general_1':		
				// PARTIE NON VISIBLE				
				$('#general_referent').addClass('help_mask');
				$('#general_resume').addClass('help_mask');				
				$('#tr_bilan_animation').addClass('help_mask');				
				
				//INFO MSG
				msg_top = 280;
				msg_left = 70;
				msg1 = "<div style='margin-top:10px;width:460px;'><label class='label help'>L'événement sera précisé de manière individuelle pour chaque personne présente dans la section \"Echanges\".</label></div>";
				msg2 = "<div style='margin-top:10px;;width:460px;'><label class='label help'>Un événement peut être édité en cliquant sur le bouton d'édition <img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('evt_edit_over');\" onmouseout=\"adaptFiche('evt_edit_out')\">.</label></div>";
				msg3 = "<div style='margin-top:10px;;width:460px;'><label class='label help'>Le commentaire de chaque interlocuteur peut être renseigné ici <img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('interloc_edit_over');\" onmouseout=\"adaptFiche('interloc_edit_out')\">.</label></div>";				
				
				msg = msg1 + msg2 + msg3
				evidence_top = $('#'+elt.id).offset().top+25;
				evidence_left = $('#'+elt.id).offset().left-170;
				evidence_width = 450;
				evidence_height = 60;
				
				$('#explorateur_evenement').css('display', '');
				
			break;
			case 'img_help_interloc_1':		
				// PARTIE NON VISIBLE				
				$('#btn_regroup_interloc').addClass('help_mask');
				$('#tbody_interlocs_corps').addClass('help_mask');				
				
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top+50;
				msg_left = $('#'+elt.id).offset().left-150;
				msg = "<label class='label help'>Cette section permet de filtrer le contenu de la page.<br>Les EAF correspondant aux filtres sont affichées en intégralité.<br>A noter que le filtre s'applique sur TOUS les onglets.<br>Cliquer sur <img class='help_filtre' src='../../images/supprimer.png'> pour défiltrer.</label>";
				evidence_top = $('#'+elt.id).offset().top-10;
				evidence_left = $('#'+elt.id).offset().left-250;
				evidence_width = 680;
				evidence_height = 40;
			break;
			case 'img_help_interloc_2':
				// PARTIE NON VISIBLE
				$('.help_filtre').addClass('help_mask');
				$('#tbody_interlocs_corps').addClass('help_mask');
								
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top+50;
				msg_left = $('#'+elt.id).offset().left-550;
				msg = "<label class='label help'>Ce bouton permet de regrouper des propri&eacute;taires diff&eacute;renci&eacute;s dans la matrice cadastrale<br>alors qu'il s'agit de la m&ecirc;me personne.<br>Il peut aussi être utiliser pour associer un interlocuteur créé manuellement avec son équivalent importé depuis la matrice suite à une mise à jour du cadastre. <br><br>Un formulaire permet de param&eacute;trer et de mettre à jour cette information. En voici un exemple : </label><br><img src='images/help/regroup_interloc.png' width='750px'>";
				evidence_top = $('#'+elt.id).offset().top-10;
				evidence_left = $('#'+elt.id).offset().left+30;
				evidence_width = 205;
				evidence_height = 40;
			break;
			case 'img_help_interloc_3':				
				// PARTIE NON VISIBLE
				$('#tbl_interlocs_entete').addClass('help_mask');
				$('#tr_22').addClass('help_mask');
				$('#interloc_tbl_lots_18').addClass('help_mask');
				$('#tr_16').addClass('help_mask');
								
				//INFO MSG
				msg_top = 240;
				msg_left = 680;
				
				msg1 = "<div style='margin-top:10px;'><label class='label help'>Pour plus d'information sur les types d'interlocuteur, veuillez consulter le glossaire.</label></div>";
				msg2 = "<div style='margin-top:10px;'><label class='label help'>ATTENTION, l'ajout d'un nouvel interlocuteur peut provoquer une modification des Entités d'Animation Foncière. C'est le cas lorsqu'un véritable propriétaire n'est pas concerné par l'intégralité des parcelles <img src='images/help/voir.png' width='20px' style='cursor:pointer;'  onmouseover=\"adaptFiche('new_eaf_over');\" onmouseout=\"adaptFiche('new_eaf_out')\"></label></div>";
				msg3 = "<div style='margin-top:10px;'><label class='label help'>Vous pourrez saisir des informations complémentaires une fois le nouvel interlocuteur créé en éditant sa fiche.</label></div>";				
				msg4 = "<div style='margin-top:10px;'><label class='label help'>Les informations <i>Démembrement/ Indivision</i> et <i>Droit réel ou particulier</i> ne concernent que le type véritable propriétaire <img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('ccodrm_over');\" onmouseout=\"adaptFiche('ccodrm_out')\"> </label></div>";
				msg5 = "<div style='margin-top:10px;'><label class='label help'>Certains types d'interlocuteur concernent obligatoirement l'intégralité des parcelles. C'est le cas pour la plupart des ayants-droit</label></div>";
				
				msg = msg1 + msg2 + msg3 + msg4 + msg5
				evidence_top = $('#'+elt.id).offset().top-10;
				evidence_left = $('#'+elt.id).offset().left+30;
				evidence_width = 185;
				evidence_height = 40;				
				$('#explorateur_interloc').css('display', '');
				$('html,body').animate({scrollTop: 50}, 'fast');
			break;	
			case 'img_help_interloc_4':				
				// PARTIE NON VISIBLE
				$('#tbl_interlocs_entete').addClass('help_mask');
				$('#tr_22').addClass('help_mask');
				$('#tr_18').addClass('help_mask');
				$('#interloc_tbl_lots_16').addClass('help_mask');
				$('#tr_49').addClass('help_mask');
				$('#tr_add_interloc_16').addClass('help_mask');
				$('#tr_gestref_16').addClass('help_mask');
				
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top-150;
				msg_left = 80;
				msg = "<label class='label help'>Organisation des interlocuteurs les uns par rapport aux autres.<br><br>S'il est identifié, le r&eacute;f&eacute;rent est en t&ecirc;te surligné <span class='referent'>de cette mani&egrave;re.</span><br>Les interlocuteurs r&eacute;f&eacute;rencés sont à la suite.<br><br>En face d'un interlocuteur, on peut trouver un interlocuteur de<br>substitution dont la couleur de police dépend du type.<br><br><img  src='../../images/edit.png'> Ce bouton permet d'éditer la fiche interlocuteur<br><img src='../../images/supprimer.png'>Ce bouton permet de supprimer un interlocuteur. Option disponible<br>seulement si aucun échange n'a été passé avec celui-ci.<br><br><span style='border-bottom:1px solid #004494;width:35px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Ce trait marque la fin du groupe référencé.</label>";
				evidence_top = $('#tbl_16').offset().top-5;
				evidence_left = $('#tbl_16').offset().left-10;
				evidence_width = 450;
				evidence_height = 240;
			break;			
			case 'img_help_interloc_5':
				// PARTIE NON VISIBLE
				$('#tbl_interlocs_entete').addClass('help_mask');
				$('#tr_22').addClass('help_mask');
				$('#tr_18').addClass('help_mask');
				$('#interloc_tbl_lots_16').addClass('help_mask');
				$('#tr_30').addClass('help_mask');
				$('#tr_31').addClass('help_mask');
				$('#tr_32').addClass('help_mask');
				$('#tr_33').addClass('help_mask');
				$('#tr_49').addClass('help_mask');
				$('#btn_gestsubst_16').addClass('help_mask');
				$('#tr_add_interloc_16').addClass('help_mask');
				
				$('#dropper_12_1').html("<div class='drop_title'>Mme. DUCK Daisy<br><label class='last_maj'>Véritable propriétaire</label></div>")
				$('#dropper_30_1').html("<div class='drop_title'>M. MOUSE Mickey<br><label class='last_maj'>Véritable propriétaire</label></div><div id='div_40_9' class='draggable 4' draggable='true'>Mme. POPPINS Mary<br><label class='last_maj'>Mandataire spécial</label></div>")
				
				evidence_top = $('#'+elt.id).offset().top-10;
				evidence_left = $('#'+elt.id).offset().left+30;
				evidence_width = 170;
				evidence_height = 40;
				$('#explorateur_gestion_referent_sousdiv').css('display', '');
				$('#explorateur_gestion_referent_sousdiv').css('top', $('#'+elt.id).offset().top-$('#explorateur_gestion_referent_sousdiv').height()-130+'px');
				$('#div_11_move').css('left', $('#div_11').offset().left-5+'px');
				$('#div_11_move').css('top', $('#div_11').offset().top-5+'px');
				$('#div_11_move').width($('#div_11').width());
				$('#div_11_move').height($('#div_11').height());
				msg_top = $('#'+elt.id).offset().top-$('#explorateur_gestion_referent_sousdiv').height()-40;
				msg_left = $('#explorateur_gestion_referent_sousdiv').offset().left+$('#explorateur_gestion_referent_sousdiv').width()+10;
				$('#help_msg').css('top', msg_top+'px');
				$('#help_msg').css('left', msg_left+'px');
				$('#help_msg').width($('#onglet').width() - $('#explorateur_gestion_substitution_sousdiv').width()-100);
				help_msg = "<label class='label help'>Pour définir un interlocuteur comme référent, cocher la case correspondante"
				$('#help_msg').html(help_msg+"</label>");
				time = 600;
				setTimeout(function(){$('#help_msg').css('display', '');}, time);
				time += 3500
				setTimeout(function(){$('#ref_11').addClass('simuli_click')}, time);
				time += 1000
				setTimeout(function(){$('#ref_11').prop('checked', 'checked');}, time);
				setTimeout(function(){$('#ref_11').removeClass('simuli_click')}, time);
				setTimeout(function(){$('#dropper_11').css('display', '');}, time);
				time += 2000
				setTimeout(function(){$('#ref_17').addClass('simuli_click')}, time);	
				time += 1000
				setTimeout(function(){$('#ref_17').prop('checked', 'checked');}, time);
				setTimeout(function(){$('#ref_17').removeClass('simuli_click')}, time);
				setTimeout(function(){$('#dropper_17').css('display', '');}, time);
				time += 1000
				help_msg += "<br><br>Pour enlever le statut de référent, recliquer sur la case correspondante."
				setTimeout(function(){$('#help_msg').html(help_msg+"</label>");}, time);
				time += 3000
				setTimeout(function(){$('#ref_11').addClass('simuli_click')}, time);
				time += 1000
				setTimeout(function(){$('#ref_11').prop('checked', '');}, time);
				setTimeout(function(){$('#ref_11').removeClass('simuli_click')}, time);				
				setTimeout(function(){$('#dropper_11').css('display', 'none');}, time);				
				time += 2000				
				setTimeout(function(){help_msg += "<br><br>Pour associer un interlocuteur à un référent, cliquer/ déplacer celui-ci vers le référent correspondant (en maintenant le clic gauche)."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4500
				setTimeout(function(){$('#div_11_move').css('display', '');}, time);
				setTimeout(function(){$('#div_11_move').animate({left: $('#dropper_17').offset().left-15+'px',top: $('#dropper_17').offset().top-15+'px',opacity:0.8}, 1000);}, time);
				time += 1000
				setTimeout(function(){$('#dropper_17').addClass('drop_hover');}, time);
				time += 2000				
				setTimeout(function(){help_msg += "<br><br>Relâcher le clic gauche lorsque la boite s'entoure de gris pour valider et enregistrer votre choix."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4000
				setTimeout(function(){$('#dropper_17').html("<div class='drop_title'>M. PICSOU Balthazar<br><label class='last_maj'>Véritable propriétaire</label></div><div  class='draggable'>M. DUCK Donald<br><label class='last_maj'>Curateur</label></div>");}, time);
				setTimeout(function(){$('#dropper_17').removeClass('drop_hover');}, time);
				setTimeout(function(){$('#div_11_move').css('display', 'none');}, time);
				setTimeout(function(){$('#div_11').css('display', 'none');}, time);
				time += 2000				
				setTimeout(function(){help_msg += "<br><br>Renouveler l'opération pour associer tous les interlocuteurs nécessaires."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 2000
				setTimeout(function(){
					$('#div_30_move').css('left', $('#div_30').offset().left-5+'px');
					$('#div_30_move').css('top', $('#div_30').offset().top-5+'px');
					$('#div_30_move').width($('#div_30').width());
					$('#div_30_move').height($('#div_30').height());	
					$('#div_30_move').css('display', '');					
					}, time);
				setTimeout(function(){$('#div_30_move').animate({left: $('#dropper_17').offset().left-15+'px',top: $('#dropper_17').offset().top-15+'px',opacity:0.8}, 1000);}, time);
				time += 1000
				setTimeout(function(){$('#dropper_17').addClass('drop_hover');}, time);				
				time += 1000
				setTimeout(function(){$('#dropper_17').html("<div class='drop_title'>M. PICSOU Balthazar<br><label class='last_maj'>Véritable propriétaire</label></div><div class='draggable'>M. DUCK Donald<br><label class='last_maj'>Curateur</label></div><div class='draggable'>M. MOUSE Mickey<br><label class='last_maj'>Véritable propriétaire</label></div>");}, time);
				setTimeout(function(){$('#dropper_17').removeClass('drop_hover');}, time);
				setTimeout(function(){$('#div_30_move').css('display', 'none');}, time);
				setTimeout(function(){$('#div_30').css('display', 'none');}, time);
				time += 1000
				setTimeout(function(){
					$('#div_39_move').css('left', $('#div_39').offset().left-5+'px');
					$('#div_39_move').css('top', $('#div_39').offset().top-5+'px');
					$('#div_39_move').width($('#div_39').width());
					$('#div_39_move').height($('#div_39').height());
					$('#div_39_move').css('display', '');
					}, time);
				setTimeout(function(){$('#div_39_move').animate({left: $('#dropper_17').offset().left-15+'px',top: $('#dropper_17').offset().top-15+'px',opacity:0.8}, 1000);}, time);
				time += 1000
				setTimeout(function(){$('#dropper_17').addClass('drop_hover');}, time);				
				time += 1000
				setTimeout(function(){$('#dropper_17').html("<div class='drop_title'>M. PICSOU Balthazar<br><label class='last_maj'>Véritable propriétaire</label></div><div class='draggable'>M. DUCK Donald<br><label class='last_maj'>Curateur</label></div><div class='draggable'>M. MOUSE Mickey<br><label class='last_maj'>Véritable propriétaire</label></div><div id='div_39bis' class='draggable'>Mme. DUCK Daisy<br><label class='last_maj'>Véritable propriétaire</label></div>");}, time);
				setTimeout(function(){$('#dropper_17').removeClass('drop_hover');}, time);
				setTimeout(function(){$('#div_39_move').css('display', 'none');}, time);
				setTimeout(function(){$('#div_39').css('display', 'none');}, time);
				time += 2000				
				setTimeout(function(){help_msg += "<br><br>Pour dissocier un interlocuteur d'un référent, cliquer/ déplacer celui-ci vers la boite \"<i>Référent</i>\" correspondant (en maintenant le clic gauche)."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4500
				setTimeout(function(){
					$('#chk_39_move').css('display', 'none');
					$('#div_39_move').css('left', $('#div_39bis').offset().left-5+'px');
					$('#div_39_move').css('top', $('#div_39bis').offset().top-5+'px');
					$('#div_39_move').css('display', '');
					}, time);
				setTimeout(function(){$('#div_39_move').animate({left: $('#dropper_ref').offset().left-15+'px',top: $('#dropper_ref').offset().top-15+'px',opacity:0.8}, 1000);}, time);
				time += 1000
				setTimeout(function(){$('#dropper_ref').addClass('drop_hover');}, time);
				time += 2000				
				setTimeout(function(){help_msg += "<br><br>Relâcher le clic gauche lorsque la boite s'entoure de gris pour valider et enregistrer votre choix."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4000
				setTimeout(function(){$('#dropper_ref').html("<div class='drop_title'>Référent</div><div class='draggable'><input class='checkRef' style='float:left;display:normal;' type='checkbox'>M. PICSOU Balthazar<br><label class='last_maj'>Véritable propriétaire</label></div><div class='draggable'><input class='checkRef' style='float:left;display:normal;' type='checkbox'>Mme. DUCK Daisy<br><label class='last_maj'>Véritable propriétaire</label></div>");}, time);
				setTimeout(function(){$('#dropper_ref').removeClass('drop_hover');}, time);
				setTimeout(function(){$('#div_39_move').css('display', 'none');}, time);
				setTimeout(function(){$('#div_39bis').css('display', 'none');}, time);
				time += 1000				
				setTimeout(function(){help_msg += "<br><br>"; $('#help_msg').html(help_msg+"</label><div class='last_maj' style='text-align:center;color:#d77a0f;'>Fin</div>");}, time);
				
			break;			
			case 'img_help_interloc_6':
				// PARTIE NON VISIBLE
				$('#tbl_interlocs_entete').addClass('help_mask');
				$('#tr_22').addClass('help_mask');
				$('#tr_18').addClass('help_mask');
				$('#interloc_tbl_lots_16').addClass('help_mask');
				$('#tr_30').addClass('help_mask');
				$('#tr_31').addClass('help_mask');
				$('#tr_32').addClass('help_mask');
				$('#tr_33').addClass('help_mask');
				$('#tr_49').addClass('help_mask');
				$('#btn_gestref_16').addClass('help_mask');
				$('#tr_add_interloc_16').addClass('help_mask');
				
				$('#dropper_12_1').html("<div class='drop_title'>Mme. DUCK Daisy<br><label class='last_maj'>Véritable propriétaire</label></div>")
				$('#dropper_30_1').html("<div class='drop_title'>M. MOUSE Mickey<br><label class='last_maj'>Véritable propriétaire</label></div><div id='div_40_9' class='draggable 4' draggable='true'>Mme. POPPINS Mary<br><label class='last_maj'>Mandataire spécial</label></div>")
				
				evidence_top = $('#'+elt.id).offset().top-10;
				evidence_left = $('#'+elt.id).offset().left-200;
				evidence_width = 200;
				evidence_height = 40;
											
				$('#explorateur_gestion_substitution_sousdiv').css('display', '');
				$('#explorateur_gestion_substitution_sousdiv').css('top', $('#'+elt.id).offset().top-$('#explorateur_gestion_substitution_sousdiv').height()-40+'px');
				$('#div_11_10_move').css('left', $('#div_11_10').offset().left-5+'px');
				$('#div_11_10_move').css('top', $('#div_11_10').offset().top-5+'px');
				$('#div_11_10_move').width($('#div_11_10').width());
				$('#div_11_10_move').height($('#div_11_10').height());
				$('#div_11_10_move').css('display', '');
				$('#div_40_9_move').css('left', $('#div_40_9').offset().left-5+'px');
				$('#div_40_9_move').css('top', $('#div_40_9').offset().top-5+'px');
				$('#div_40_9_move').width($('#div_40_9').width());
				$('#div_40_9_move').height($('#div_40_9').height());
				$('#div_40_9_move').css('display', '');
				msg_top = $('#'+elt.id).offset().top-$('#explorateur_gestion_substitution_sousdiv').height()-40;
				msg_left = $('#explorateur_gestion_substitution_sousdiv').offset().left+$('#explorateur_gestion_substitution_sousdiv').width()+10;
				$('#help_msg').css('top', msg_top+'px');
				$('#help_msg').css('left', msg_left+'px');
				$('#help_msg').width($('#onglet').width() - $('#explorateur_gestion_substitution_sousdiv').width()-100);
				help_msg = "<label class='label help'>Pour définir un représentant :<br><br>Cliquer/ déplacer un représentant identifié vers un véritable propriétaire (en maintenant le clic gauche)."
				$('#help_msg').html(help_msg+"</label>");
				time = 600;
				setTimeout(function(){$('#help_msg').css('display', '');}, time);
				time += 4000;
				setTimeout(function(){$('#div_11_10_move').animate({left: $('#dropper_22_1').offset().left-15+'px',top: $('#dropper_22_1').offset().top-15+'px',opacity:0.8}, 1000);}, time);
				time += 1000;
				setTimeout(function(){$('#dropper_22_1').addClass('cant_drop_hover')}, time);
				time += 1500;
				setTimeout(function(){help_msg += "<br><br>Si la boite s'entoure de rouge, l'association n'est pas possible."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 2000;
				setTimeout(function(){$('#div_11_10_move').animate({left: $('#dropper_12_1').offset().left-15+'px',top: $('#dropper_12_1').offset().top-15+'px',opacity:0.8}, 1000);}, time);
				time += 100;
				setTimeout(function(){$('#dropper_22_1').removeClass('cant_drop_hover')}, time);
				time += 500;
				setTimeout(function(){$('#dropper_12_1').addClass('drop_hover')}, time);
				time += 1000;
				setTimeout(function(){help_msg += "<br><br>Si elle s'entoure de gris, l'association est possible."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 3000;
				setTimeout(function(){help_msg += "<br><br>Relâcher le clic gauche pour valider et enregistrer votre choix."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 3500;
				setTimeout(function(){$('#div_11_10_move').css('display', 'none');}, time);
				setTimeout(function(){$('#dropper_12_1').removeClass('drop_hover')}, time);
				setTimeout(function(){$('#dropper_12_1').html("<div class='drop_title'>Mme. DUCK Daisy<br><label class='last_maj'>Véritable propriétaire</label></div><div class='draggable 4'>M. DUCK Donald<br><label class='last_maj'>Curateur</label></div>")}, time);
				time += 4000;
				setTimeout(function(){help_msg = "<label class='label help'>Pour enlever un représentant, même principe :<br><br>Cliquer/ déplacer un représentant vers la liste des représentants identifiés."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 1000;
				setTimeout(function(){$('#div_40_9_move').animate({left: $('#div_40_91').offset().left-15+'px',top: $('#div_40_91').offset().top-15+'px',opacity:0.8}, 1000);}, time);
				time += 1000;
				setTimeout(function(){$('#dropper_subst').addClass('drop_hover')}, time);
				time += 1000;
				setTimeout(function(){help_msg += "<br><br>Relâcher le clic gauche pour valider et enregistrer votre choix."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 2000;
				setTimeout(function(){$('#div_40_9_move').css('display', 'none');}, time);
				setTimeout(function(){$('#dropper_subst').removeClass('drop_hover')}, time);
				setTimeout(function(){$('#dropper_30_1').html("<div class='drop_title'>M. MOUSE Mickey<br><label class='last_maj'>Véritable propriétaire</label></div>")}, time);
				time += 1000				
				setTimeout(function(){help_msg += "<br><br>"; $('#help_msg').html(help_msg+"</label><div class='last_maj' style='text-align:center;color:#d77a0f;'>Fin</div>");}, time);
			break;
			
			case 'img_help_parcelle_1':
				// PARTIE NON VISIBLE
				$('#tbody_parc_entete').addClass('help_mask');
				$('#tr_parc_prix_eaf-16 >td >table').addClass('help_mask');
				$('#tbl_parc_interloc-16').addClass('help_mask');
				$('#parc_td_general-22').addClass('help_mask');
				$('#tr_objectif-22').addClass('help_mask');
				$('#tr_nature_culture-22').addClass('help_mask');
				$('#utilisol-22').addClass('help_mask');
				$('#tr_prix-22').addClass('help_mask');
				$('#tr_divers-22').addClass('help_mask');
				$('#tr_parc_comm_eaf-16 >td >table').addClass('help_mask');
				$('#tr_parc_prix_eaf-9').addClass('help_mask');
				$('#tr_parc_infos-9').addClass('help_mask');
				$('#tr_parc_comm_eaf-9').addClass('help_mask');
				$('#type_occsol-25-3 option:eq(0)').prop('selected', true);
				$('#pourc_occsol-25-3').val('');
				$('#prix_occsol-25-3').val('');	
				$('#lbl_prix_total_occsol-22').css('display', 'none');
								
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top-120;
				msg_left = 50;
				msg_width = $('#td_parc_interloc-16').width()
				msg1 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Les options disponibles s'adaptent au fur et à mesure de la saisie</label></div>";
				msg2 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Une ligne supplémentaire s'ajoute jusqu'à obtenir 100% de recouvrement</label></div>";
				msg3 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Un prix estimé du fond s'affiche lorsque la saisie est complète <img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('occsol_over');\" onmouseout=\"adaptFiche('occsol_out')\"> <i>(Exemple)</i></span></label></div>";				
				msg4 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Les prix au m² peuvent être saisis plus simplement avec le bouton <i>Saisir des prix au m² par occupation du sol</i> situé en haut de la fiche</label></div>";				
				
				msg = msg1 + msg2 + msg3 + msg4
				evidence_top = $('#'+elt.id).offset().top-70;
				evidence_left = $('#'+elt.id).offset().left-525;
				evidence_width = 535;
				evidence_height = 150;
			break;
			
			case 'img_help_parcelle_2':
				// PARTIE NON VISIBLE
				$('#tbody_parc_entete').addClass('help_mask');
				$('#tr_parc_prix_eaf-16 >td >table').addClass('help_mask');
				$('#tbl_parc_interloc-16').addClass('help_mask');
				$('#parc_td_general-22').addClass('help_mask');
				$('#tr_objectif-22').addClass('help_mask');
				$('#tr_pratique_sol-22').addClass('help_mask');				
				$('#tr_prix_total_op-22').addClass('help_mask');
				$('#tr_divers-22').addClass('help_mask');
				$('#tr_parc_comm_eaf-16 >td >table').addClass('help_mask');
				$('#tr_parc_prix_eaf-9').addClass('help_mask');
				$('#tr_parc_infos-9').addClass('help_mask');
				$('#tr_parc_comm_eaf-9').addClass('help_mask');				
								
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top-220;
				msg_left = 50;
				msg_width = $('#td_parc_interloc-16').width()
				msg1 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Vous pouvez saisir une infinité de prix différents.</label></div>";
				msg2 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Une ligne supplémentaire s'ajoute à chaque saisie pour toujours avoir la possibilité d'ajouter un prix.</label></div>";
				msg3 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Il est conseillé de préciser a minima le prix du fonds. Soit le prix du fonds total <img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('prix_fond_total_over');\" onmouseout=\"adaptFiche('prix_fond_total_out')\">. Soit le prix du fonds au m² <img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('prix_fond_m2_over');\" onmouseout=\"adaptFiche('prix_fond_m2_out')\">. Dans ce cas, on indique la contenance cadastrale dans le champ <i>Quantité</i>.</label></div>";				
				msg4 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Un total d'acquisition est calculé automatiquement.</label></div>";
				msg5 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Cette méthode très souple permet de répondre aux multiples façons de saisir les prix dans les différents Conservatoires. En revanche elle limite les possibilités d'analyse de cette donnée.</label></div>";				
				
				msg = msg1 + msg2 + msg3 + msg4 + msg5
				evidence_top = $('#'+elt.id).offset().top-75;
				evidence_left = $('#'+elt.id).offset().left-681;
				evidence_width = 685;
				evidence_height = 140;
			break;
			
			case 'img_help_parcelle_3':
				// PARTIE NON VISIBLE
				$('#tbody_parc_entete').addClass('help_mask');
				$('#tr_parc_prix_eaf-16').addClass('help_mask');
				$('#tr_parc_infos-16').addClass('help_mask');
				$('#tr_parc_comm_eaf-16').addClass('help_mask');
				$('#tbl_9').addClass('help_mask');
				$('#tr_parc_comm_eaf-9 >td >table').addClass('help_mask');		
								
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top+40;
				msg_left = 50;
				msg_width = $('#tbl_9').width()+40
				msg1 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Le prix d'acquisition de l'EAF calculé correspond à la somme des prix d'acquisition total opérateur de chaque lot de l'EAF.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('prix_eaf_calcul_over');\" onmouseout=\"adaptFiche('prix_eaf_calcul_out')\"></div>";
				msg2 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Le prix d'acquisition de l'EAF opérateur est saisi manuellement par l'opérateur pour arrondir si nécessaire le prix calculé.</label></div>";	
								
				msg = msg1 + msg2
				evidence_top = $('#'+elt.id).offset().top-15;
				evidence_left = $('#'+elt.id).offset().left-240;
				evidence_width = 725;
				evidence_height = 40;
			break;	
			
			case 'img_help_parcelle_4':
				// PARTIE NON VISIBLE
				$('#tbody_parc_corps').addClass('help_mask');
				$('.help_filtre').addClass('help_mask');				
				$('#td_onglet_parc_ALL').addClass('help_mask');				
								
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top+40;
				msg_left = 50;
				msg_width = $('#tbl_9').width()+40
				msg1 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>ATTENTION, ce bouton n'est disponible que lorsque l'occupation est renseignée à 100% pour TOUTES les parcelles.</label></div>";
				msg2 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Comme indiqué dans la boite de dialogue, seuls les prix qui n'ont pas encore été renseignés seront mis à jour.<br>Vous pouvez également modifier un prix après avoir utilisé cette méthode.</label></div>";	
				$('#explorateur_prix_occsol').css('left', msg_width+50+'px');
				$('#explorateur_prix_occsol').css('display', '');	
								
				msg = msg1 + msg2
				evidence_top = $('#'+elt.id).offset().top;
				evidence_left = $('#'+elt.id).offset().left-320;
				evidence_width = 325;
				evidence_height = 40;
			break;
			
			case 'img_help_echanges_1':			
			
				// PARTIE NON VISIBLE
				$('#td_parc_echange-1').addClass('help_mask');
				$('#tr_echange_info_17_16_6').addClass('help_mask');
				$('#tr_echange_rappel_17_16_6').addClass('help_mask');
				$('#tr_echange_info_17_16_1').addClass('help_mask');
				$('#tbl_11_16').addClass('help_mask');
				$('#tbl_echange_11_16_10').addClass('help_mask');
				$('#tbl_12_16').addClass('help_mask');
				$('#tbl_echange_12_16_1').addClass('help_mask');
				$('#tbl_30_16').addClass('help_mask');
				$('#tbl_echange_30_16_1').addClass('help_mask');
				$('#tbl_40_16').addClass('help_mask');
				$('#tbl_echange_40_16_1').addClass('help_mask');
				$('#tr_echange-2').addClass('help_mask');
				
				$('html,body').animate({scrollTop: $('#tr_echange_ech-1').offset().top}, 'fast');
				$('#explorateur_echange_sousdiv').width($('#tr_echange_ech-1').width() - $('#td_interloc-1_echange-1').width());
								
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top+80;
				msg_left = 50;
				msg_width = $('#td_parc_echange-1').width()+$('#td_interloc-1_echange-1').width()
				msg1 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Voici un exemple simplifié d'une fiche de saisie d'échange.</label></div>";
				msg2 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Le principe est le même pour un nouvel échange <img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('ech_new_over');\" onmouseout=\"adaptFiche('ech_new_out')\"> et pour l'édition d'un échange existant.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('ech_edit_over');\" onmouseout=\"adaptFiche('ech_edit_out')\"></div>";	
				msg3 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Lorsqu'un échange est passé avec un référent, il y a la possibilité d'affecter l'échange à tous les interlocuteurs référencés. Il est également possible d'indiquer une négociation sur leurs parcelles respectives.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('ech_chk_ref_over');\" onmouseout=\"adaptFiche('ech_chk_ref_out')\"></div>";	
				msg4 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Pour en savoir plus sur le \"Prix d'acquisition de l'EAF en cours de négociation\", Cf. Glossaire rubrique \"Prix\".</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('ech_prix_eaf_over');\" onmouseout=\"adaptFiche('ech_prix_eaf_out')\"></div>";	
				$('#explorateur_echange').css('display', '');
				$('#explorateur_echange').css('left',  $('#tr_echange_nego_17_16_10').offset().left+'px');
				$('#explorateur_echange').css('top',  $('#tr_echange_nego_17_16_10').offset().top+10+'px');
								
				msg = msg1 + msg2 + msg3 + msg4
				evidence_top = $('#tr_echange_ech-1').offset().top;
				evidence_left = $('#tr_echange_ech-1').offset().left;
				evidence_width = $('#tr_echange_ech-1').width();
				evidence_height = 100;
			break;
			
			case 'img_help_echanges_2':			
			
				// PARTIE NON VISIBLE
				$('#td_parc_echange-1').addClass('help_mask');
				$('#tr_echange_info_17_16_10').addClass('help_mask');
				$('#tr_echange_nego_17_16_10').addClass('help_mask');
				$('#tbl_11_16').addClass('help_mask');
				$('#tbl_echange_11_16_10').addClass('help_mask');
				$('#tbl_12_16').addClass('help_mask');
				$('#tbl_echange_12_16_1').addClass('help_mask');
				$('#tbl_30_16').addClass('help_mask');
				$('#tbl_echange_30_16_1').addClass('help_mask');
				$('#tbl_40_16').addClass('help_mask');
				$('#tbl_echange_40_16_1').addClass('help_mask');
				$('#tr_echange-2').addClass('help_mask');
				$('#tr_echange_info_17_16_1').addClass('help_mask');
				
				$('#tbody_echange_resultat').css('display', 'none');
				$('#tr_ech_rappel').css('display', '');
				$('#date_echange').val('01/08/2017');
				$('#lst_type_echange option:selected').index(1)
				$('#description_echange').html("Envoi d'informations concernant le projet d'AF. M. Picsou s'occupe de transmettre le projet aux personnes dont il est le référent.");
				
				$('html,body').animate({scrollTop: $('#tr_echange_ech-1').offset().top}, 'fast');
				$('#explorateur_echange_sousdiv').width($('#tr_echange_ech-1').width() - $('#td_interloc-1_echange-1').width());		
								
				//INFO MSG
				msg_top = $('#'+elt.id).offset().top+80;
				msg_left = 50;
				msg_width = $('#td_parc_echange-1').width()+$('#td_interloc-1_echange-1').width()
				msg1 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Voici un exemple simplifié d'une fiche de saisie d'échange.</label></div>";				
				msg2 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>La fenêtre de l'échange peut être réduite afin de consulter les différents onglets. Très pratique pour des précisions lors d'un échange téléphonique. A noter que les informations sont filtrées sur l'interlocuteur de l'échange en cours.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('ech_minimize_over');\" onmouseout=\"adaptFiche('ech_minimize_out')\"></div>";	
				msg3 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Un rappel entraine une alerte sur la page d'accueil ainsi qu'un message dans l'onglet \"Synthèse\" jusqu'à ce qu'il soit précisé comme traité.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('ech_chk_rappel_traite_over');\" onmouseout=\"adaptFiche('ech_chk_rappel_traite_out')\"></div>";	
				
				$('#explorateur_echange').css('display', '');
				$('#explorateur_echange').css('left',  $('#tr_echange_info_17_16_1').offset().left+'px');
				$('#explorateur_echange').css('top',  $('#tr_echange_info_17_16_1').offset().top+'px');
								
				msg = msg1 + msg2 +msg3
				evidence_top = $('#tr_echange_info_17_16_6').offset().top;
				evidence_left = $('#tr_echange_info_17_16_6').offset().left;
				evidence_width = $('#tr_echange_info_17_16_6').width();
				evidence_height = 120;
			break;
			
			case 'img_help_synthese_1':
				// PARTIE NON VISIBLE
				$('#tbody_synthese_parc_entete').addClass('help_mask');
				$('#tr_synthese_22').addClass('help_mask');
				
				$('#td_objectif_22').css('background', '#747474')
				$('#label_objectif_22').html('')
				
				evidence_top = $('#tr_synthese_16').offset().top+5;
				evidence_left = $('#tr_synthese_16').offset().left;
				evidence_width = $('#tr_synthese_16').width();
				evidence_height = $('#tr_synthese_16').height()-10;
				
				msg_top = $('#tr_synthese_22').offset().top+10;
				msg_left = $('#tr_synthese_16').offset().left-10;
				$('#help_msg').css('top', msg_top+'px');
				$('#help_msg').css('left', msg_left+'px');
				$('#help_msg').width($('#tr_synthese_16').width());
				help_msg = "<label class='label help'>Tous les interlocuteurs ne sont pas toujours d'accord. Au survol d'un interlocuteur, on peut consulter la dernière négociation de celui-ci."
				$('#help_msg').html(help_msg+"</label>");
				
				
				time = 600;
				setTimeout(function(){$('#help_msg').css('display', '');}, time);				
				
				time += 4500;
				setTimeout(function(){
					$('#img_arrow').animate({left: $('#td_synthese_interloc-1').offset().left+50+'px',top: $('#td_synthese_interloc-1').offset().top+5+'px'}, 2000);
					$('#img_arrow').css('display', '');
				}, time);											
				time += 1800;
				setTimeout(function(){
					$('#td_objectif_22').css('background', '#70ad47');
					$('#label_objectif_22').html('accord');
				}, time);
				time += 1000;
				setTimeout(function(){help_msg += "<br><br>On sait que Picsou est d'accord pour vendre le lot."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4000;
				setTimeout(function(){
					$('#img_arrow').animate({left: $('#td_synthese_interloc-2').offset().left+50+'px',top: $('#td_synthese_interloc-2').offset().top+5+'px'}, 800);
				}, time);					
				time += 700;
				setTimeout(function(){
					$('#td_objectif_22').css('background', '#70ad47');
					$('#label_objectif_22').html('');		
				}, time);
				time += 1000;
				setTimeout(function(){help_msg += "<br>Donald, quant à lui, n'a pas donné son accord, il s'agit d'une perspective d'acquisition."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4000;
				setTimeout(function(){
					$('#img_arrow').animate({left: $('#td_synthese_interloc-3').offset().left+40+'px',top: $('#td_synthese_interloc-3').offset().top+5+'px'}, 800);
				}, time);							
				time += 700;
				setTimeout(function(){
					$('#td_objectif_22').css('background', '#70ad47');
					$('#label_objectif_22').html('accord');		
				}, time);
				time += 1000;
				setTimeout(function(){help_msg += "<br>Daisy, comme Picsou,  est d'accord pour vendre le lot."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4000;
				setTimeout(function(){
					$('#img_arrow').animate({left: $('#td_synthese_interloc-4').offset().left+90+'px',top: $('#td_synthese_interloc-4').offset().top+5+'px'}, 800);
				}, time);		
				time += 700;
				setTimeout(function(){
					$('#td_objectif_22').css('opacity', '0');
					$('#chasseur_22').css('display', '');
					$('#label_objectif_22').html('');		
				}, time);
				time += 1000;
				setTimeout(function(){help_msg += "<br>On sait qu'Elmer est chasseur grace au fusil.<br>Il n'est pas propriétaire donc pas de négociation.<br>Le fusil est vert donc il est favorable. Dans le cas contraire le fusil serait rouge, ou bien gris s'il ne s'était pas prononcé."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 4000;
				setTimeout(function(){help_msg += "<br><br>On voit ainsi rapidement que seul Donald n'a pas donné son accord mais est déjà dans la perspective d'une vente. On n'est plus très loin de l'acquisition pour ce lot."; $('#help_msg').html(help_msg+"</label>");}, time);
				time += 3000				
				setTimeout(function(){help_msg += "<br><br>"; $('#help_msg').html(help_msg+"</label><div class='last_maj' style='text-align:center;color:#d77a0f;'>Fin</div>");}, time);
			break;
			
			case 'img_help_synthese_2':			
			
				// PARTIE NON VISIBLE
				$('#tbody_synthese_parc_entete').addClass('help_mask');
				$('#lbl_synthese_prix_lot').addClass('help_mask');
				$('#lbl_synthese_prix_eaf').addClass('help_mask');
				$('#tbl_parc_16').addClass('help_mask');
				$('#tbl_parc_22').addClass('help_mask');
				$('#td_synthese_prix_eaf-2').addClass('help_mask');									
								
				//INFO MSG
				msg_top = $('#tbody_synthese_parc_corps').offset().top+10;
				msg_left = $('#tbl_interloc_16').offset().left+300;
				msg_width = $('#tbl_interloc_16').width()-300;
				msg1 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Chaque bouton <img src='../../images/info.png' width='20px'> affiche une pop-up d'information adaptée qui résume tout ce que contient la base de données y compris dans les autres SAF.</label></div>";				
				msg2 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Il y a des informations relatives à l'EAF.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('synthese_fiche_eaf_over');\" onmouseout=\"adaptFiche('synthese_fiche_eaf_out')\"></div>";	
				msg3 = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Il y a des informations relatives à un interlocuteur.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('synthese_fiche_interloc_over');\" onmouseout=\"adaptFiche('synthese_fiche_interloc_out')\"></div>";

				$('#explorateur_infos_eaf').width($('#tbl_parc_16').width());
				$('#explorateur_infos_eaf').css('left',  $('#tbl_parc_16').offset().left+'px');
				$('#explorateur_infos_eaf').css('top',  $('#tbody_synthese_parc_corps').offset().top+10+'px');
				$('#explorateur_infos_interloc').width($('#tbl_parc_16').width());
				$('#explorateur_infos_interloc').css('left',  $('#tbl_parc_16').offset().left+'px');
				$('#explorateur_infos_interloc').css('top',  $('#tbody_synthese_parc_corps').offset().top+10+'px');
				
				msg = msg1 + msg2 +msg3
				evidence_top = $('#tbl_interloc_16').offset().top-20;
				evidence_left = $('#tbl_interloc_16').offset().left-30;
				evidence_width = 300;
				evidence_height = $('#tr_synthese_16').height() + $('#tr_synthese_22').height()+10;
			break;
			
			case 'img_help_synthese_3':			
			
				// PARTIE NON VISIBLE
				$('#tbody_synthese_parc_entete').addClass('help_mask');
				$('#lbl_synthese_prix_lot').addClass('help_mask');
				$('#lbl_synthese_prix_eaf').addClass('help_mask');
				$('#td_synthese_prix_lot-1').addClass('help_mask');
				$('#td_synthese_prix_eaf-2').addClass('help_mask');
				$('#img_info_eaf-1').addClass('help_mask');
				$('#tbl_interloc_16').addClass('help_mask');
				$('#img_info_eaf-2').addClass('help_mask');									
				$('#tbl_interloc_22').addClass('help_mask');									
								
				//INFO MSG
				msg_top = $('#tbody_synthese_parc_corps').offset().top+10;
				msg_left = $('#td_synthese_prix_eaf-2').offset().left+20;
				msg_width = $('#tbl_interloc_16').width()-300;
				msg = "<div style='margin-top:10px;width:"+msg_width+"px;'><label class='label help'>Pour savoir à quoi correspond chaque couleur, une légende est disponible en bas à droite de la page.</label><img src='images/help/voir.png' width='20px' style='cursor:pointer;' onmouseover=\"adaptFiche('synthese_lgd_over');\" onmouseout=\"adaptFiche('synthese_lgd_out')\"></div>";
			
				evidence_top = $('#td_objectif_22').offset().top-10;
				evidence_left = $('#td_objectif_22').offset().left-10;
				evidence_width = 385;
				evidence_height = $('#tr_synthese_16').height() + $('#tr_synthese_22').height()-10;
			break;
		}
		
		if (msg.length > 0) {
			$('#help_msg').css('display', '');
			$('#help_msg').css('top', msg_top+'px');
			$('#help_msg').css('left', msg_left+'px');
			$('#help_msg').html(msg)
		}
		
		if (evidence_width > 0) {
			$('#help_evidence').css('display', '');
			$('#help_evidence').css('top', evidence_top+'px');
			$('#help_evidence').css('left', evidence_left+'px');
			$('#help_evidence').width(evidence_width);
			$('#help_evidence').height(evidence_height);
		}
		
		$('.img_help').css('display', 'none');
		$('.img_tip').css('display', 'none');
	}
}

function loadQuestionMark() {
	$('.tabs-container').each(function(){
		$('#general').css('display', 'none');
		$('#interloc').css('display', 'none');
		$('#parcelle').css('display', 'none');
		$('#echanges').css('display', 'none');
		$('#synthese').css('display', 'none');
		$('#'+this.id).css('display', 'block');
		switch (this.id) {
			case 'general':		
				$('#img_help_general_1').css('top', $('#td_lst_evenements').offset().top-30+'px');
				$('#img_help_general_1').css('left', $('#td_lst_evenements').offset().left+'px');
			break;
			case 'interloc':		
				$('#img_help_interloc_1').css('top', $('#filtre_interloc_section').offset().top+'px');
				$('#img_help_interloc_1').css('left', $('#filtre_interloc_section').offset().left+210+'px');
				$('#img_help_interloc_2').css('top', $('#btn_regroup_interloc').offset().top+'px');
				$('#img_help_interloc_2').css('left', $('#btn_regroup_interloc').offset().left-40+'px');
				$('#img_help_interloc_3').css('top', $('#btn_18').offset().top+'px');
				$('#img_help_interloc_3').css('left', $('#btn_18').offset().left-40+'px');
				$('#img_help_interloc_4').css('top', $('#tbl_16').offset().top+($('#tbl_16').height()/2)-$('#tr_add_interloc_16').height()-$('#tr_gestref_16').height()+'px');
				$('#img_help_interloc_4').css('left', $('#tbl_16').offset().left+($('#tbl_16').width()/2)+'px');
				$('#img_help_interloc_5').css('top', $('#btn_gestref_16').offset().top+'px');
				$('#img_help_interloc_5').css('left', $('#btn_gestref_16').offset().left-40+'px');
				$('#img_help_interloc_6').css('top', $('#btn_gestsubst_16').offset().top+'px');
				$('#img_help_interloc_6').css('left', $('#btn_gestsubst_16').offset().left+195+'px');	
				$('#img_tip_off_interloc_1').css('top', $('#lbl_22-41-1').offset().top+30+'px');
				$('#img_tip_off_interloc_1').css('left', $('#lbl_22-41-1').offset().left-20+'px');
				$('#img_tip_on_interloc_1').css('top', $('#lbl_22-41-1').offset().top+30+'px');
				$('#img_tip_on_interloc_1').css('left', $('#lbl_22-41-1').offset().left-20+'px');
				$('#img_tip_off_interloc_2').css('top', $('#tr_no_vp_18').offset().top+'px');
				$('#img_tip_off_interloc_2').css('left', $('#tr_no_vp_18').offset().left-20+'px');
				$('#img_tip_on_interloc_2').css('top', $('#tr_no_vp_18').offset().top-5+'px');
				$('#img_tip_on_interloc_2').css('left', $('#tr_no_vp_18').offset().left-20+'px');
			break;
			case 'parcelle':		
				$('#img_help_parcelle_1').css('top', $('#img_save_occsol-22').offset().top+'px');
				$('#img_help_parcelle_1').css('left', $('#img_save_occsol-22').offset().left+20+'px');	
				$('#img_help_parcelle_2').css('top', $('#img_save_prix-22').offset().top+'px');
				$('#img_help_parcelle_2').css('left', $('#img_save_prix-22').offset().left+20+'px');	
				$('#img_help_parcelle_3').css('top', $('#tr_parc_prix_eaf-9').offset().top+10+'px');
				$('#img_help_parcelle_3').css('left', $('#tr_parc_prix_eaf-9').offset().left+250+'px');	
				$('#img_help_parcelle_4').css('top', $('#btn_prix_occsol').offset().top+-5+'px');
				$('#img_help_parcelle_4').css('left', $('#btn_prix_occsol').offset().left+$('#btn_prix_occsol').width()+20+'px');	
				$('#img_tip_off_parcelle_1').css('top', $('#lbl_lot_suppr').offset().top+20+'px');
				$('#img_tip_off_parcelle_1').css('left', $('#lbl_lot_suppr').offset().left+20+'px');
				$('#img_tip_on_parcelle_1').css('top', $('#lbl_lot_suppr').offset().top+20+'px');
				$('#img_tip_on_parcelle_1').css('left', $('#lbl_lot_suppr').offset().left+20+'px');
				$('#img_tip_off_parcelle_2').css('top', $('#lbl_lot_bloque-22').offset().top-5+'px');
				$('#img_tip_off_parcelle_2').css('left', $('#lbl_lot_bloque-22').offset().left+80+'px');
				$('#img_tip_on_parcelle_2').css('top', $('#lbl_lot_bloque-22').offset().top-5+'px');
				$('#img_tip_on_parcelle_2').css('left', $('#lbl_lot_bloque-22').offset().left+80+'px');
				$('#img_tip_off_parcelle_3').css('top', $('#lbl_cur_nego-22').offset().top-5+'px');
				$('#img_tip_off_parcelle_3').css('left', $('#lbl_cur_nego-22').offset().left+$('#lbl_cur_nego-22').width()+20+'px');
				$('#img_tip_on_parcelle_3').css('top', $('#lbl_cur_nego-22').offset().top-5+'px');
				$('#img_tip_on_parcelle_3').css('left', $('#lbl_cur_nego-22').offset().left+$('#lbl_cur_nego-22').width()+20+'px');
				$('#img_tip_off_parcelle_4').css('top', $('#td_conv_partie').offset().top+20+'px');
				$('#img_tip_off_parcelle_4').css('left', $('#td_conv_partie').offset().left+180+'px');
				$('#img_tip_on_parcelle_4').css('top', $('#td_conv_partie').offset().top+20+'px');
				$('#img_tip_on_parcelle_4').css('left', $('#td_conv_partie').offset().left+180+'px');
			break;
			case 'echanges':		
				$('#img_help_echanges_1').css('top', $('#tr_echange_info_17_16_10').offset().top+($('#tr_echange_info_17_16_10').height()/2)-13+'px');
				$('#img_help_echanges_1').css('left', $('#tr_echange_info_17_16_10').offset().left+20+'px');
				$('#img_help_echanges_2').css('top', $('#tr_echange_info_17_16_6').offset().top+($('#tr_echange_info_17_16_6').height()/2)-13+'px');
				$('#img_help_echanges_2').css('left', $('#tr_echange_info_17_16_6').offset().left-20+'px');	
				
				$('#img_tip_off_echanges_1').css('top', $('#tbl_40_16').offset().top+($('#tbl_40_16').height()/2)+'px');
				$('#img_tip_off_echanges_1').css('left', $('#tbl_40_16').offset().left+$('#tbl_40_16').width()+20+'px');
				$('#img_tip_on_echanges_1').css('top', $('#tbl_40_16').offset().top+($('#tbl_40_16').height()/2)+'px');
				$('#img_tip_on_echanges_1').css('left', $('#tbl_40_16').offset().left+$('#tbl_40_16').width()+20+'px');
				$('#img_tip_off_echanges_2').css('top', $('#tbl_22_16').offset().top+'px');
				$('#img_tip_off_echanges_2').css('left', $('#tbl_22_16').offset().left-20+'px');
				$('#img_tip_on_echanges_2').css('top', $('#tbl_22_16').offset().top+'px');
				$('#img_tip_on_echanges_2').css('left', $('#tbl_22_16').offset().left-20+'px');
				$('#img_tip_off_echanges_3').css('top', $('#tr_echange_info_11_16_10').offset().top+($('#tr_echange_info_11_16_10').height()/2)-20+'px');
				$('#img_tip_off_echanges_3').css('left', $('#tr_echange_info_11_16_10').offset().left+40+'px');
				$('#img_tip_on_echanges_3').css('top', $('#tr_echange_info_11_16_10').offset().top+($('#tr_echange_info_11_16_10').height()/2)-20+'px');
				$('#img_tip_on_echanges_3').css('left', $('#tr_echange_info_11_16_10').offset().left+40+'px');
				
			break;
			case 'synthese':
				$('#img_help_synthese_1').css('top', $('#tbl_interloc_16').offset().top+($('#tbl_interloc_16').height()/2)-13+'px');
				$('#img_help_synthese_1').css('left', $('#tbl_interloc_16').offset().left+$('.filtre_proprio').width()+20+'px');	
				$('#img_help_synthese_2').css('top', $('#img_info_eaf-2').offset().top+$('#img_info_eaf-2').height()+5+'px');
				$('#img_help_synthese_2').css('left', $('#img_info_eaf-2').offset().left+'px');	
				$('#img_help_synthese_3').css('top', $('#td_objectif_22').offset().top-$('#td_objectif_22').height()-5+'px');
				$('#img_help_synthese_3').css('left', $('#td_objectif_22').offset().left+$('#td_objectif_22').width()+5+'px');	
				
				$('#img_tip_off_synthese_1').css('top', $('#lbl_synthese_prix_lot').offset().top+($('#lbl_synthese_prix_lot').height())+'px');
				$('#img_tip_off_synthese_1').css('left', $('#lbl_synthese_prix_lot').offset().left+($('#lbl_synthese_prix_lot').width()/2)-10+'px');
				$('#img_tip_on_synthese_1').css('top', $('#lbl_synthese_prix_lot').offset().top+($('#lbl_synthese_prix_lot').height())+'px');
				$('#img_tip_on_synthese_1').css('left', $('#lbl_synthese_prix_lot').offset().left+($('#lbl_synthese_prix_lot').width()/2)-10+'px');
				$('#img_tip_off_synthese_2').css('top', $('#lbl_synthese_prix_eaf').offset().top+($('#lbl_synthese_prix_eaf').height())+'px');
				$('#img_tip_off_synthese_2').css('left', $('#lbl_synthese_prix_eaf').offset().left+($('#lbl_synthese_prix_eaf').width()/2)-10+'px');
				$('#img_tip_on_synthese_2').css('top', $('#lbl_synthese_prix_eaf').offset().top+($('#lbl_synthese_prix_eaf').height())+'px');
				$('#img_tip_on_synthese_2').css('left', $('#lbl_synthese_prix_eaf').offset().left+($('#lbl_synthese_prix_eaf').width()/2)-10+'px');
			break;
		}
	});
	$('#general').css('display', 'block');
	$('#interloc').css('display', 'none');
	$('#parcelle').css('display', 'none');
	$('#echanges').css('display', 'none');
	$('#synthese').css('display', 'none');
	
	$('.img_help').not('.img_help_keep').css('display', 'none');
}

function showQuestionMark(onglet) {	
	$('.img_help').not('.img_help_keep').css('display', 'none');
	$('.img_help_'+onglet).css('display', '');
		
	$('.img_tip').not('.img_tip_keep').css('display', 'none');
	$('.img_tip_'+onglet).css('display', '');
}

function skipMsg() {
	$('#msg_accueil').css('display', 'none');
	$('#onglet').css('opacity', '1');
	$('.img_help_general_temp').each(function(){
		$(this).css('display', '');
		$(this).attr('class', $(this).attr('class').replace('img_help_general_temp', 'img_help_general'));
	});
}
	
function fltGlossary(method) {
	if (method == 'find') {
		var search = $('#glossaire_search').val();
	} else {
		$('#glossaire_search').val('')
		var search = '';
	}
	if (search.length > 0) {
		$('#explorateur_glossaire').find(".glossaire_titre").each(function(){
			if ($(this).html().toUpperCase().indexOf(search.toUpperCase()) > 0) {
				$('#'+this.id.replace('title', 'tbody')).css('display', '');
			} else {
				$('#'+this.id.replace('title', 'tbody')).css('display', 'none');
			}
		});
	} else {
		$('.glossaire_tbody').css('display', '');
	}
}

function adaptFiche(method) {
	switch (method) {
		case 'new_eaf_over':		
			$('#new_type_interloc').html("<option selected value='-1'>- - - Type d'interlocuteur - - -</option><option selected>Véritable propriétaire</option>");			
			$('#chk_23').prop('checked', '');
			$('#new_type_interloc').addClass('simuli_click');
			$('#chk_23').addClass('simuli_click');
		break;
		case 'new_eaf_out':		
			$('#new_type_interloc').html("<option selected value='-1'>- - - Type d'interlocuteur - - -</option><option>Véritable propriétaire</option>");
			$('#chk_23').prop('checked', 'checked');
			$('#new_type_interloc').removeClass('simuli_click');
			$('#chk_23').removeClass('simuli_click');
		break;
		case 'ccodrm_over':		
			$('#new_type_interloc').html("<option selected value='-1'>- - - Type d'interlocuteur - - -</option><option selected>Agriculteur (bail rural)</option>");			
			$('.only_proprio').css('opacity', 0.5);
			$('#new_type_interloc').addClass('simuli_click');
			$('#fld_new_parc_interloc').addClass('simuli_click');
		break;
		case 'ccodrm_out':		
			$('#new_type_interloc').html("<option selected value='-1'>- - - Type d'interlocuteur - - -</option><option>Agriculteur (bail rural)</option>");
			$('.only_proprio').css('opacity', 1);
			$('#new_type_interloc').removeClass('simuli_click');
			$('#fld_new_parc_interloc').removeClass('simuli_click');
		break;
		case 'occsol_over':		
			$('#type_occsol-25-3 option:eq(2)').prop('selected', true);
			$('#pourc_occsol-25-3').val('40');
			$('#prix_occsol-25-3').val('0.6');
			$('#type_occsol-25-3').addClass('simuli_click');
			$('#pourc_occsol-25-3').addClass('simuli_click');
			$('#prix_occsol-25-3').addClass('simuli_click');
			$('#tr_prix-22').removeClass('help_mask');	
			$('#lbl_prix_total_occsol-22').css('display', '');	
			$('#lbl_prix_total_occsol-22').addClass('simuli_click');			
		break;
		case 'occsol_out':		
			$('#type_occsol-25-3 option:eq(0)').prop('selected', true);
			$('#pourc_occsol-25-3').val('');
			$('#prix_occsol-25-3').val('');
			$('#type_occsol-25-3').removeClass('simuli_click');
			$('#pourc_occsol-25-3').removeClass('simuli_click');
			$('#prix_occsol-25-3').removeClass('simuli_click');
			$('#tr_prix-22').addClass('help_mask');	
			$('#lbl_prix_total_occsol-22').css('display', 'none');
			$('#lbl_prix_total_occsol-22').removeClass('simuli_click');		
		break;
		case 'prix_fond_total_over':		
			$('#tr_prix_fond_total').css('display', '');
			$('#tr_prix_fond_m2').css('display', 'none');	
			$('#tr_prix_fond_total').addClass('simuli_click');	
		break;
		case 'prix_fond_total_out':		
			$('#tr_prix_fond_total').css('display', '');
			$('#tr_prix_fond_m2').css('display', 'none');
			$('#tr_prix_fond_total').removeClass('simuli_click');			
		break;
		case 'prix_fond_m2_over':		
			$('#tr_prix_fond_total').css('display', 'none');
			$('#tr_prix_fond_m2').css('display', '');	
			$('#tr_prix_fond_m2').addClass('simuli_click');			
		break;
		case 'prix_fond_m2_out':		
			$('#tr_prix_fond_total').css('display', '');
			$('#tr_prix_fond_m2').css('display', 'none');	
			$('#tr_prix_fond_m2').removeClass('simuli_click');
		break;
		case 'prix_eaf_calcul_over':		
			$('#tr_prix-10').css('display', '');
			$('#tr_prix-11').css('display', '');
			$('#lbl_montant_eaf_calcule-19').addClass('simuli_click');
			$('#parc_arrondi_prix-10').addClass('simuli_click');
			$('#parc_arrondi_prix-11').addClass('simuli_click');
		break;
		case 'prix_eaf_calcul_out':		
			$('#tr_prix-10').css('display', 'none');
			$('#tr_prix-11').css('display', 'none');
			$('#lbl_montant_eaf_calcule-19').removeClass('simuli_click');
			$('#parc_arrondi_prix-10').removeClass('simuli_click');
			$('#parc_arrondi_prix-11').removeClass('simuli_click');
		break;
		case 'evt_edit_over':		
			$('#evt_edit').addClass('simuli_click');
		break;
		case 'evt_edit_out':		
			$('#evt_edit').removeClass('simuli_click');
		break;
		case 'interloc_edit_over':		
			$('#interloc_edit').addClass('simuli_click');
			$('#tr_evt_com_40').css('display', '');
		break;
		case 'interloc_edit_out':		
			$('#interloc_edit').removeClass('simuli_click');
			$('#tr_evt_com_40').css('display', 'none');
		break;
		case 'ech_chk_ref_over':
			$('#chk_echange_referent').addClass('simuli_click');
			time = 2500;
			setTimeout(function(){
				$('#chk_echange_referent').prop('checked', '')
				$('.display_tbl_parc_maitrise_other_eaf').css('display', 'none');
				$('#chk_echange_referent').removeClass('simuli_click');
			}, time);
			time += 2000
			setTimeout(function(){				
				$('#chk_echange_referent').addClass('simuli_click');			
			}, time);
			time += 2500				
			setTimeout(function(){
				$('#chk_echange_referent').prop('checked', 'checked')
				$('.display_tbl_parc_maitrise_other_eaf').css('display', '');				
			}, time);
		break;
		case 'ech_chk_ref_out':		
			$('#chk_echange_referent').removeClass('simuli_click');
			$('#chk_echange_referent').prop('checked', 'checked')
			$('.display_tbl_parc_maitrise_other_eaf').css('display', '');
		break;
		case 'ech_prix_eaf_over':
			$('#ech_prix_eaf_nego').addClass('simuli_click');
		break;
		case 'ech_prix_eaf_out':		
			$('#ech_prix_eaf_nego').removeClass('simuli_click');
		break;
		case 'ech_new_over':
			$('#img_ech_new').addClass('simuli_click');
		break;
		case 'ech_new_out':		
			$('#img_ech_new').removeClass('simuli_click');
		break;
		case 'ech_edit_over':
			$('#img_ech_edit').addClass('simuli_click');
		break;
		case 'ech_edit_out':		
			$('#img_ech_edit').removeClass('simuli_click');
		break;
		case 'ech_chk_rappel_traite_over':
			$('#echange_chk_rappel_traite').addClass('simuli_click');
			$('#tr_echange_rappel_17_16_6').addClass('simuli_click');
			$('#echange_chk_rappel_traite').prop('checked', 'checked');
			$('#tr_echange_rappel_17_16_6 label').css('color', '#537a13');
			$('#tr_echange_rappel_17_16_6 textarea').css('color', '#537a13');
		break;
		case 'ech_chk_rappel_traite_out':		
			$('#echange_chk_rappel_traite').removeClass('simuli_click');
			$('#tr_echange_rappel_17_16_6').removeClass('simuli_click');
			$('#echange_chk_rappel_traite').prop('checked', '');
			$('#tr_echange_rappel_17_16_6 label').css('color', '#fe0000');
			$('#tr_echange_rappel_17_16_6 textarea').css('color', '#fe0000');
		break;
		case 'ech_minimize_over':			
			$('#img_ech_maximize').removeClass('simuli_click');
			$('#img_reduce_explorateur_echange').addClass('simuli_click');
			time = 2500;
			setTimeout(function(){
				$('#explorateur_echange').css('display', 'none');
				$('#explorateur_echange_reduit').css('display', '');
			}, time);
		break;
		case 'ech_minimize_out':		
			$('#img_ech_maximize').addClass('simuli_click');
			$('#img_reduce_explorateur_echange').removeClass('simuli_click');
			time = 2500;
			setTimeout(function(){
				$('#explorateur_echange').css('display', '');
				$('#explorateur_echange_reduit').css('display', 'none');
			}, time);
		break;
		case 'synthese_fiche_eaf_over':			
			$('#img_info_eaf-1').addClass('simuli_click');
			$('#explorateur_infos_eaf').css('display', '');
		break;
		case 'synthese_fiche_eaf_out':		
			$('#img_info_eaf-1').removeClass('simuli_click');
			$('#explorateur_infos_eaf').css('display', 'none');
		break;
		case 'synthese_fiche_interloc_over':			
			$('#img_info_interloc-1').addClass('simuli_click');
			$('#explorateur_infos_interloc').css('display', '');
		break;
		case 'synthese_fiche_interloc_out':		
			$('#img_info_interloc-1').removeClass('simuli_click');
			$('#explorateur_infos_interloc').css('display', 'none');
		break;
		case 'synthese_lgd_over':			
			$('#ul_lgd_synthese').css('display', '');
			$('#ul_lgd_synthese').animate({
				width: ['210px', 'swing'],
				height: ['290px', 'swing']
				  }, { duration: "slow", easing: "easein" });	
		break;
		case 'synthese_lgd_out':	
			$('#ul_lgd_synthese').css('display', 'none');	  
			$('#ul_lgd_synthese').width(0);	  
			$('#ul_lgd_synthese').height(0);	  
		break;
	}
} 