/**************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
Copyright Conservatoire d’espaces naturels de Savoie, 2018
-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
-	Adresse électronique : sig at cen-savoie.org

Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
-	de gérer et suivre ces actes (convention et acquisition) ;
-	et d’en établir des bilans.
-	Un module permet de réaliser des sessions d’animation foncière.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
***************************************************************************************************************************
***************************************************************************************************************************
**************************************************************************************************************************/

/* Cette fonction permet de vérifier par, l'intermédiaire de la classe 'with_echange', que des échanges sont associés aux parcelles que l'on souhaite dissocier d'un véritable proprétaire dans l'édition d'une fiche interlocuteur. Si des échanges existent un message d'avertissement s'affiche sinon il ne s'affiche pas. */
function checkEchange(mode) {
	$('#tbody_'+mode+'_parc_interloc').find('input[type=checkbox]').each(function(){// pour chaque parcelle du tableau des parcelles associées d'une fiche interlocuteur
		if ($('#'+this.id).hasClass('with_echange') && !$('#'+this.id).is(':checked')) {// si des échanges existent
			$('#msg_'+this.id).css('display', '');// si des échanges existent on affiche un message
		} else {
			$('#msg_'+this.id).css('display', 'none');// sinon on le masque
		}
	});
}

/* Cette fonction permet de contrôler la modification d'une information de l'onglet Parcelles/ Lots et de rendre utilisable la disquette d'enregistrement de l'information modifiée.
-la variable type permet d'identifier la catégorie de l'information a été modifiée
-la variable rel_saf_foncier_id permet d'identifier l'élément concerné
- les variable entite_af_id, session_af_id et nbr_echanges sont nécessaire pour adapter la fonction updateLot qui se déclenche au clic de la disquette devenue disponible. */
function isUpdatable(id, type, rel_saf_foncier_id, entite_af_id, session_af_id, nbr_echanges) {
	if (type == 'occsol') {// Si la modification concerne l'occupation du sol
		$('#occsol_prob_saisie-'+rel_saf_foncier_id).css('display', 'none'); //On masque un éventuel message d'erreur de saisie
		prob_saisie = 'NON'; //On initialise une variable permettant de contrôler la saisie
		tbl_type = []; //On initialise une variable pour stocker et contrôler les types d'occupation du sol saisis
		sum_pourc = 0; //On initialise une variable pour calculer et contrôler le taux de recouvrement de l'occupation du sol
		for (i=0; i<$('#tbl_parc_'+type+'-'+rel_saf_foncier_id+' tr').length; i++){ //pour chaque ligne du tableau affichant les type d'occupation du sol
			val_type = $('#type_occsol-'+rel_saf_foncier_id+'-'+i).val() //on regarde la valeur du menu déroulant indiquant le type d'occupation du sol
			if($.inArray(val_type,tbl_type) < 0 && val_type != '-1') { // Si l'élément n'est pas déjà présent et que l'élément est bien renseigné
				tbl_type.push(val_type); //on l'ajoute au tableau déclaré précédement
			} else if($.inArray(val_type,tbl_type) > -1 && val_type != '-1') { //sinon, un problème de C
				prob_saisie = 'OUI';
				break;
			}
						
			val_pourc = $('#pourc_occsol-'+rel_saf_foncier_id+'-'+i).val() //on regarde la valeur du taux de recouvrement
			if(Math.floor(val_pourc) == val_pourc && $.isNumeric(val_pourc) && val_pourc != null && val_pourc != '') { //on contrôle cette valeur, est-elle biien numerique et bien renseignée
				sum_pourc += parseFloat(val_pourc); //dans ce cas on ajoute cette valeur à la somme des occupations du sol
			} else if(Math.floor(val_pourc) != val_pourc || !$.isNumeric(val_pourc) && val_pourc != null && val_pourc != '') { //sinon, un problème de saisi est identifié
				prob_saisie = 'OUI';
				break;
			}
			
			val_prix = $('#prix_occsol-'+rel_saf_foncier_id+'-'+i).val() //on regarde la valeur du prix au m²
			if (!$.isNumeric(val_prix) && val_prix != '') {	//si cette valeur n'est pas numérique alors un prolème de saisi est identifié
				prob_saisie = 'OUI';
				break;
			}
		}
		
		if (prob_saisie == 'OUI' || sum_pourc > 100) { //si un problème de saisie est identifié ou que la somme des taux de recouvrement dépasse 100%
			$('#occsol_prob_saisie-'+rel_saf_foncier_id).css('display', ''); //on affiche un message d'avertissement
			changeOpac(30, 'img_save_'+type+'-'+rel_saf_foncier_id); //on empeche l'enregistrement de la donnée
			$('#img_save_'+type+'-'+rel_saf_foncier_id).attr('onclick', '');
			$('#img_save_'+type+'-'+rel_saf_foncier_id).css('cursor', 'auto');
		} else { //sinon on autorise l'enregistrement de la donnée
			if (sum_pourc < 100) {
				
			}
			changeOpac(100, 'img_save_'+type+'-'+rel_saf_foncier_id); //le bouton devient disponible
			$('#img_save_'+type+'-'+rel_saf_foncier_id).attr('onclick', "updateLot('"+type+"-"+rel_saf_foncier_id+"', "+entite_af_id+", "+session_af_id+", "+nbr_echanges+")"); //on adapte la fonction du bouton pour l'élément en question
			$('#img_save_'+type+'-'+rel_saf_foncier_id).css('cursor', 'pointer');
		}
	} else if (type == 'prix') {
		$('#prix_prob_saisie-'+rel_saf_foncier_id).css('display', 'none');
		prob_saisie = 'NON';			
		for (i=0; i<$('#tbl_parc_'+type+'-'+rel_saf_foncier_id+' tr.prix_detail').length; i++){
			val_montant = $('#montant_prix-'+rel_saf_foncier_id+'-'+i).val();
			if ( $('#montant_prix-'+rel_saf_foncier_id+'-'+i).length > 0 && !$.isNumeric(val_montant) && val_montant != '') {				
				prob_saisie = 'OUI';
				break;
			}
			val_quantite = $('#quantite_prix-'+rel_saf_foncier_id+'-'+i).val();
			if($('#quantite_prix-'+rel_saf_foncier_id+'-'+i).length > 0 && (Math.floor(val_quantite) != val_quantite || !$.isNumeric(val_quantite)) && val_quantite != '') {
				prob_saisie = 'OUI';
				break;
			}			
		}			
		val_parc_arrondi = $('#parc_arrondi_prix-'+rel_saf_foncier_id).val();
		if($('#parc_arrondi_prix-'+rel_saf_foncier_id).length > 0 && !$.isNumeric(val_parc_arrondi) && val_parc_arrondi != '') {
			prob_saisie = 'OUI';
		}	
		
		if (prob_saisie == 'OUI') {
			$('#prix_prob_saisie-'+rel_saf_foncier_id).css('display', '');
			changeOpac(30, 'img_save_'+type+'-'+rel_saf_foncier_id);
			$('#img_save_'+type+'-'+rel_saf_foncier_id).attr('onclick', '');
			$('#img_save_'+type+'-'+rel_saf_foncier_id).css('cursor', 'auto');
		} else {			
			changeOpac(100, 'img_save_'+type+'-'+rel_saf_foncier_id);
			$('#img_save_'+type+'-'+rel_saf_foncier_id).attr('onclick', "updateLot('"+type+"-"+rel_saf_foncier_id+"', "+entite_af_id+", "+session_af_id+", "+nbr_echanges+")");
			$('#img_save_'+type+'-'+rel_saf_foncier_id).css('cursor', 'pointer');
		}
	} else if (type == 'montant_eaf') {					
		val_montant_eaf = $('#montant_eaf-'+entite_af_id).val();
		if($('#montant_eaf-'+entite_af_id).length > 0 && $.isNumeric(val_montant_eaf)) {
			changeOpac(100, 'img_save_'+type+'-'+entite_af_id);
			$('#img_save_'+type+'-'+entite_af_id).attr('onclick', "updateLot('"+type+"-"+entite_af_id+"', "+entite_af_id+", "+session_af_id+", "+nbr_echanges+")");
			$('#img_save_'+type+'-'+entite_af_id).css('cursor', 'pointer');
		}	
	} else if (type == 'commentaires_eaf') {
		changeOpac(100, 'img_save_'+type+'-'+entite_af_id);
		$('#img_save_'+type+'-'+entite_af_id).attr('onclick', "updateLot('"+type+"-"+entite_af_id+"', "+entite_af_id+", "+session_af_id+", "+nbr_echanges+")");
		$('#img_save_'+type+'-'+entite_af_id).css('cursor', 'pointer');
	} else {
		if ($('#'+type+'-'+rel_saf_foncier_id).val() != $('#hid_'+type+'-'+rel_saf_foncier_id).val()) {
			changeOpac(100, 'img_save_'+type+'-'+rel_saf_foncier_id);
			$('#img_save_'+type+'-'+rel_saf_foncier_id).attr('onclick', "updateLot('"+type+"-"+rel_saf_foncier_id+"', "+entite_af_id+", "+session_af_id+", "+nbr_echanges+")");
			$('#img_save_'+type+'-'+rel_saf_foncier_id).css('cursor', 'pointer');
		} else {
			changeOpac(30, 'img_save_'+type+'-'+rel_saf_foncier_id);
			$('#img_save_'+type+'-'+rel_saf_foncier_id).attr('onclick', '');
			$('#img_save_'+type+'-'+rel_saf_foncier_id).css('cursor', 'auto');
		}
	}
}

/* Cette fonction est appelée lorsqu'on modifie le type d'interlocuteur d'une fiche interlocuteur (saisie nouelle ou édition existante). Elle permet de contrôler qu'un nterlocuteur avec les mêmes caractéristique n'existe pas déjà. Elle permet également d'adapter les options disponibles en fonction du type d'interlocuteur sélectionné. */
function adaptListParc(elt, mode) {
	$('#fld_'+mode+'parc_interloc').find('input[type=checkbox]').each(function(){
		if ($('#'+elt.id+' option:selected').hasClass('all_eaf')) {
			$(this).prop('checked', 'checked');
			$(this).prop('readonly', true);
			$(this).prop('disabled', true);
		} else {
			$(this).prop('readonly', false);
			$(this).prop('disabled', false);
		}
	});
	
	if (mode == 'update_') {
		if ($('#lbl_'+$('#hid_update_entite_af_id').val()+'-'+$('#update_interloc_id').val()+'-'+$('#update_type_interloc').val()).length > 0 && !$('#update_type_interloc option:selected').hasClass($('#update_interloc_id').val())) {
			msg = "Un interlocuteur avec ces caract&eacute;ristiques existe d&eacute;j&agrave;";
			$('#update_interloc_prob_saisie').html(msg);
			$('#update_interloc_prob_saisie').css('display', '');
		} else {
			$('#update_interloc_prob_saisie').css('display', 'none');
		}
	} else {
		if ($('#lbl_'+$('#need_entite_af_id').val()+'-'+$('#lst_import_proprio').val().substring($('#lst_import_proprio').val().indexOf('interloc_id:')+12, $('#lst_import_proprio').val().indexOf(';'))+'-'+$('#new_type_interloc').val()).length > 0) {
			msg = "Un interlocuteur avec ces caract&eacute;ristiques existe d&eacute;j&agrave;";
			$('#new_interloc_prob_saisie').html(msg);
			$('#new_interloc_prob_saisie').css('display', '');
		} else {
			$('#new_interloc_prob_saisie').css('display', 'none');
		}
	}

	if ($('#'+mode+'type_interloc option:selected').val() == 1) {		
		$('#tbody_'+mode+'parc_interloc .only_proprio').each(function(){
			$(this).css('opacity', 1);
		});
		$('#tbody_'+mode+'parc_interloc .only_proprio').find('select').each(function(){
			$(this).prop('readonly', false);
			$(this).prop('disabled', false);
		});
	} else {	
		$('#tbody_'+mode+'parc_interloc .only_proprio').each(function(){
			$(this).css('opacity', 0.5);
		});
		$('#tbody_'+mode+'parc_interloc .only_proprio').find('select').each(function(){
			$(this).prop('readonly', true);
			$(this).prop('disabled', true);
			$(this).val('-1');
		});
	}
}

/* Cette fonction permet d'adapter l'affichage lors de la reduction ou l'agrandissement d'une fenêtre saisie ou édition d'un échange. */
function reduceForm(div, sous_div_top, interloc_id, session_af_id, entite_af_id) {
	if ($('#tbody_echange_contenu').css('display') == 'none') {
		$('#'+div).height($('#'+div).height()+120);
		$('#'+div).css('top', $('#'+div).offset().top-120+'px');
		$('#'+div+'_sousdiv').css('position', 'absolute');
		$('#'+div+'_sousdiv').css('left', 'auto');
		$('#'+div+'_sousdiv').css('bottom', '');
		$('#'+div+'_sousdiv').css('top', sous_div_top);
		$('#btn_ech_close').css('display', '');
		$('#tbody_echange_contenu').css('display', '');
		$('#tbody_echange_resultat').css('display', '');
		$('#tbody_echange_pied').css('display', '');
		$('#img_reduce_explorateur_echange').attr('src', $('#img_reduce_explorateur_echange').attr('src').replace('agrandir', 'reduire'));
		$('#'+div+'_sousdiv .explorateur_contenu').css('background-color', '#fff');
		$('#'+div+'_sousdiv .explorateur_contenu').css('border-color', '#ccc #6c6c6b #6c6c6b #ccc');
		$('#'+div+'_sousdiv .explorateur_contenu').css('border-width', '1px 2px 2px 1px');
		changeOpac(20, 'onglet');
		$('html,body').animate({scrollTop: ($('#'+div+'_sousdiv').offset().top-60)}, 'slow');
	} else {
		$('#'+div).height($('#'+div).height()-120);		
		$('#'+div).css('top', $('#'+div).offset().top+120+'px');
		$('#'+div+'_sousdiv').css('position', 'fixed');
		$('#'+div+'_sousdiv').css('left', '15px');
		$('#'+div+'_sousdiv').css('top', '');
		$('#'+div+'_sousdiv').css('bottom', '15px');
		$('#btn_ech_close').css('display', 'none');
		$('#tbody_echange_contenu').css('display', 'none');
		$('#tbody_echange_resultat').css('display', 'none');
		$('#tbody_echange_pied').css('display', 'none');
		$('#img_reduce_explorateur_echange').attr('src', $('#img_reduce_explorateur_echange').attr('src').replace('reduire', 'agrandir'));
		$('#'+div+'_sousdiv .explorateur_contenu').css('background-color', '#ccc');
		$('#'+div+'_sousdiv .explorateur_contenu').css('border-color', '#fe0000');
		$('#'+div+'_sousdiv .explorateur_contenu').css('border-width', '3px');
		changeOpac(90, 'onglet');
	}
}

/* Cette fonction permet d'afficher la légende de l'onglet Synthèse. */
function affLgd() {
	if ($('#ul_lgd_synthese').css('display') == 'none') {
		$('#ul_lgd_synthese').css('display', '');
		$('#ul_lgd_synthese').animate({
			width: ['210px', 'swing'],
			height: ['320px', 'swing']
			  }, { duration: "slow", easing: "easein" });
	} else {
		$('#ul_lgd_synthese').animate({
			width: ['30px', 'swing'],
			height: ['35px', 'swing']
			  }, { duration: "slow", easing: "easein" });
		$('#ul_lgd_synthese').css('display', 'none');	  
	}
}

/* Cette fonction d'afficher ou masquer des détails disponibles sur certazins éléments répartis dans les différents onglets. En général en cliquant sur le petit bouton rond marqué d'un + ou d'un -. */
function showDetail(onglet, id) {
	if (id == 'onglet_parc_ALL') {
		if ($('#btn_'+id).attr('src').indexOf('moins.png') > 0) {
			$('#btn_'+id).attr('src', $('#btn_'+id).attr('src').replace('moins', 'plus'));
			$('#lbl_'+id).html('Agrandir tout');
			$('#tbody_parc_corps').find('.td_detail_parc').each(function(){
				$(this).css('display', 'none');
				$('#'+this.id.replace('td', 'btn')).attr('src', $('#'+this.id.replace('td', 'btn')).attr('src').replace('moins', 'plus'));
			});
		} else {
			$('#btn_'+id).attr('src', $('#btn_'+id).attr('src').replace('plus', 'moins'));
			$('#lbl_'+id).html('R&eacute;duire tout');
			$('#tbody_parc_corps').find('.td_detail_parc').each(function(){
				if (this.className.indexOf('lot_bloque') == -1 && this.className.indexOf('lot_supprime') == -1) {
					$(this).css('display', '');
					$('#'+this.id.replace('td', 'btn')).attr('src', $('#'+this.id.replace('td', 'btn')).attr('src').replace('plus', 'moins'));
				}
			});
		}
	} else {
		if ($('#'+onglet+'_td_detail-'+id).css('display') == 'none') {
			$('#'+onglet+'_td_detail-'+id).css('display', '')
			$('#'+onglet+'_btn_detail-'+id).attr('src', $('#'+onglet+'_btn_detail-'+id).attr('src').replace('plus', 'moins'))
		} else {
			$('#'+onglet+'_td_detail-'+id).css('display', 'none')
			$('#'+onglet+'_btn_detail-'+id).attr('src', $('#'+onglet+'_btn_detail-'+id).attr('src').replace('moins', 'plus'))
		}
	}
}

/* Cette fonction est la première étape pour supprimer un interlocuteur. Compte-tenu de l'impact de la suppression d'un interlocuteur pour le système, cette action doit être confirmée via un message de confirmation qui, si confirmation, envoie vers la fonction : deleteInterlocConfirm. */
function deleteInterloc(tbl_rel_saf_foncier_interlocs_id, interloc_id, session_af_id, entite_af_id, type_interloc_id, groupe_interloc_id) {
	$('#confirmDeleteInterloc').css('display', '');
	$('#btn_confirmDeleteInterloc_oui').attr('onclick', "deleteInterlocConfirm('"+tbl_rel_saf_foncier_interlocs_id+"', '"+interloc_id+"', "+session_af_id+", '"+entite_af_id+"', '"+type_interloc_id+"', '"+groupe_interloc_id+"');");	
	$('#confirmDeleteInterloc').height($('#onglet').height());
	changeOpac(20, 'onglet'); 
	id_img = 'img_delete_' + tbl_rel_saf_foncier_interlocs_id
	if ($('#'+id_img).offset().top - 220 + 70 > $('#onglet').height()) {
		id_img_top = $('#onglet').height() - 70;
	} else {
		id_img_top = $('#'+id_img).offset().top - 220;
	}
	$('#confirmDeleteInterloc_sousdiv').css('top', id_img_top +'px');
}

/* Cette fonction permet, dans la fiche de regroupement de propriétaires, de filtrer les propriétaires entre ceux présents sur la SAF ou tous les propriétaires de la table proprios_cen. */
function fltProprioSafSites(session_af_id, mode) {
	if (mode == 'SAF') {
		$('#flt_proprios_saf').css('font-weight', 'bold');
		$('#flt_proprios_saf').attr('onclick', "");
		$('#flt_proprios_sites').css('font-weight', 'normal');	
		$('#flt_proprios_sites').attr('onclick', "fltProprioSafSites("+session_af_id+", 'sites');");
	} else {
		$('#flt_proprios_saf').css('font-weight', 'normal');
		$('#flt_proprios_saf').attr('onclick', "fltProprioSafSites("+session_af_id+", 'SAF');");
		$('#flt_proprios_sites').css('font-weight', 'bold');	
		$('#flt_proprios_sites').attr('onclick', "");
	}
	updateFormRegroupInterloc(session_af_id, 'SIMPLE')
}

/* Cette fonction permet, dans la fiche regroupement de propriétaires, de passer un propriétaire de la liste des disponibles à la liste des associés ou inversement selon le bouton utilisé.
Elle permet egalement de mettre à jour des input hidden utilisés au moment de la validation de la fiche et donc au lancement de la fonction regroupeProprios. */
function adddelRegroupProprio(mode) {	
	proprios_disposHTML = $('#proprios_dispos').html();
	proprios_associesHTML = $('#proprios_associes').html();
	proprioAdding = ''
	proprioDeleting = ''
	if (mode == 'add') {	
		$('#proprios_dispos option:selected').each(function(){
			if ($(this).css('font-weight') == 'bold' || $(this).css('font-weight') == '700') {
				var_option = '<option id="'+$(this).val()+'" style="font-weight:bold;" value="'+$(this).val()+'">'+$(this).html()+'</option>';
				proprioAdding += var_option.replace('font-weight:bold;', 'font-weight:normal;')
			} else {
				var_option = '<option id="'+$(this).val()+'" style="font-weight:normal;" value="'+$(this).val()+'">'+$(this).html()+'</option>';
				proprioAdding += var_option.replace('font-weight:normal;', 'font-weight:bold;')
			}
			proprios_disposHTML = proprios_disposHTML.replace(var_option, '')
		});
		$('#proprios_dispos').html(proprios_disposHTML)
		$('#proprios_associes').html(proprioAdding+proprios_associesHTML)
	}	
	if (mode == 'del') {	
		$('#proprios_associes option:selected').each(function(){
			if ($(this).css('font-weight') == 'bold' || $(this).css('font-weight') == '700') {
				var_option = '<option id="'+$(this).val()+'" style="font-weight:bold;" value="'+$(this).val()+'">'+$(this).html()+'</option>';
				proprioDeleting += var_option.replace('font-weight:bold;', 'font-weight:normal;')
			} else {
				var_option = '<option id="'+$(this).val()+'" style="font-weight:normal;" value="'+$(this).val()+'">'+$(this).html()+'</option>';
				proprioDeleting += var_option.replace('font-weight:normal;', 'font-weight:bold;')
			}
			proprios_associesHTML = proprios_associesHTML.replace(var_option, '')
		});
		$('#proprios_associes').html(proprios_associesHTML)
		$('#proprios_dispos').html(proprioDeleting+proprios_disposHTML)
	}	
}

/* Cette fonction permet d'adapter l'affichage d'une fiche de saisie d'un nouvel interlocuteur entre la création d'un tout nouvel interlocuteur ou l'ajout d'un interlocuteur existant.
En fonction des infos renseignées en premier, d'autres devienntent inaccessible. */
function disableNewInterloc(id) {
	if (id.id == 'lst_import_proprio' && $('#lst_import_proprio option:selected').index() > 0) {
		$('#fld_new_interloc').css('opacity', 0.5);	
		$('#td_new_interloc_lst_particule').attr('class', $('#td_new_interloc_lst_particule').attr('class') + ' readonly');
		$('#fld_new_interloc').find('input').each(function(){
			$(this).prop('disabled', true );
			$(this).prop('readonly', true );
			$(this).css('backgroundColor', '#cbcbcb');
			$(this).val('');
		});
		$('#fld_new_interloc').find('select').each(function(){
			$(this).prop('disabled', true );
			$(this).prop('readonly', true );
			$(this).val(0);
		});
		if ($('#lbl_'+$('#need_entite_af_id').val()+'-'+$('#lst_import_proprio').val().substring($('#lst_import_proprio').val().indexOf('interloc_id:')+12, $('#lst_import_proprio').val().indexOf(';'))+'-'+$('#new_type_interloc').val()).length > 0) {
			msg = "Un interlocuteur avec ces caract&eacute;ristiques existe d&eacute;j&agrave;";
			$('#new_interloc_prob_saisie').html(msg);
			$('#new_interloc_prob_saisie').css('display', '');
		} else {
			$('#new_interloc_prob_saisie').css('display', 'none');
		}
	} else if (id.id == 'lst_import_proprio' && $('#lst_import_proprio option:selected').index() <= 0) {
		$('#fld_new_interloc').css('opacity', 1);
		$('#td_new_interloc_lst_particule').attr('class', $('#td_new_interloc_lst_particule').attr('class').replace('readonly', ''));
		$('#fld_new_interloc').find('input').each(function(){
			$(this).prop('disabled', false );
			$(this).prop('readonly', false );
			$(this).css('backgroundColor', '#ccc');
		});
		$('#fld_new_interloc').find('select').each(function(){
			$(this).prop('disabled', false );
			$(this).prop('readonly', false );
		});	
		$('#new_interloc_prob_saisie').css('display', 'none');
	}
	
	if (id.id == 'new_interloc_nom_usage' && $('#new_interloc_nom_usage').val() != '') {
		$('#new_interloc_ddenom').prop('disabled', true );
		$('#new_interloc_ddenom').prop('readonly', true );
		$('#new_interloc_ddenom').css('opacity', 0.5);
		$('#fld_import_proprio').css('opacity', 0.5);
		$('#td_import_proprio_lst_import_proprio').attr('class', 'readonly');		
		$('#fld_import_proprio').find('input').each(function(){
			$(this).prop('disabled', true );
			$(this).prop('readonly', true );
			$(this).css('backgroundColor', '#cbcbcb');
			$(this).val('');
		});
		$('#fld_import_proprio').find('select').each(function(){
			$(this).prop('disabled', true );
			$(this).prop('readonly', true );
			$(this).val(0);
		});
	} else if (id.id == 'new_interloc_nom_usage' && $('#new_interloc_nom_usage').val() == '') {
		$('#new_interloc_ddenom').prop('disabled', false );
		$('#new_interloc_ddenom').prop('readonly', false );
		$('#new_interloc_ddenom').css('opacity', 1);
		$('#fld_import_proprio').css('opacity', 1);
		$('#td_import_proprio_lst_import_proprio').attr('class', 'normal');
		$('#fld_import_proprio').find('input').each(function(){
			$(this).prop('disabled', false );
			$(this).prop('readonly', false );
			$(this).css('backgroundColor', '#ccc');
		});
		$('#fld_import_proprio').find('select').each(function(){
			$(this).prop('disabled', false );
			$(this).prop('readonly', false );
		});	
	}
	
	if (id.id == 'new_interloc_ddenom' && $('#new_interloc_ddenom').val() != '') {
		$('#new_interloc_nom_usage').prop('disabled', true );
		$('#new_interloc_nom_usage').prop('readonly', true );
		$('#new_interloc_nom_usage').css('opacity', 0.5);
		$('#new_interloc_prenom_usage').prop('disabled', true );
		$('#new_interloc_prenom_usage').prop('readonly', true );
		$('#new_interloc_prenom_usage').css('opacity', 0.5);
		$('#new_interloc_lst_particule').prop('disabled', true );
		$('#new_interloc_lst_particule').prop('readonly', true );
		$('#td_new_interloc_lst_particule').attr('class', $('#td_new_interloc_lst_particule').attr('class') + ' readonly');
		$('#td_new_interloc_lst_particule').css('opacity', 0.5);
		$('#fld_import_proprio').css('opacity', 0.5);
		$('#td_import_proprio_lst_import_proprio').attr('class', 'readonly');		
		$('#fld_import_proprio').find('input').each(function(){
			$(this).prop('disabled', true );
			$(this).prop('readonly', true );
			$(this).css('backgroundColor', '#cbcbcb');
			$(this).val('');
		});
		$('#fld_import_proprio').find('select').each(function(){
			$(this).prop('disabled', true );
			$(this).prop('readonly', true );
			$(this).val(0);
		});
	} else if (id.id == 'new_interloc_ddenom' && $('#new_interloc_ddenom').val() == '') {
		$('#new_interloc_nom_usage').prop('disabled', false );
		$('#new_interloc_nom_usage').prop('readonly', false );
		$('#new_interloc_nom_usage').css('opacity', 1);
		$('#new_interloc_prenom_usage').prop('disabled', false );
		$('#new_interloc_prenom_usage').prop('readonly', false );
		$('#new_interloc_prenom_usage').css('opacity', 1);
		$('#new_interloc_lst_particule').prop('disabled', false );
		$('#new_interloc_lst_particule').prop('readonly', false );
		$('#td_new_interloc_lst_particule').attr('class', $('#td_new_interloc_lst_particule').attr('class').replace('readonly', ''));
		$('#td_new_interloc_lst_particule').css('opacity', 1);
		$('#fld_import_proprio').css('opacity', 1);		
		$('#td_import_proprio_lst_import_proprio').attr('class', 'normal');
		$('#fld_import_proprio').find('input').each(function(){
			$(this).prop('disabled', false );
			$(this).prop('readonly', false );
			$(this).css('backgroundColor', '#ccc');
		});
		$('#fld_import_proprio').find('select').each(function(){
			$(this).prop('disabled', false );
			$(this).prop('readonly', false );
		});	
	}
}

/* Cette fonction permet d'adapter les options de prix disponibles en cas d'accord ou de perspective d'acquisition. Elle permet dans la deuxième partie de pré remplir les négociations concernant les référencés (s'ils existent) à l'identique de celles saisies avec le référent. */
function updateRadMaitrise(rad_id, mode) {
	var rad_class = $('#'+rad_id).attr('class').split(' ');	
	
	if (mode == 'rad') {
		$('.'+rad_class[0]).each(function(){
			if(this.id != rad_id) {
				$(this).prop('checked', false );
				if($('#'+this.id.replace('rad', 'prix')).length) {
					$('#'+this.id.replace('rad', 'prix')).prop('readonly', true);
					$('#'+this.id.replace('rad', 'prix')).prop('disabled', true);
					$('#'+this.id.replace('rad', 'prix')).val('');
					changeOpac(20, this.id.replace('rad', 'prix'));
				}
			} else {
				if($('#'+this.id.replace('rad', 'prix')).length) {
					if($(this).is(':checked')) {
						$('#'+this.id.replace('rad', 'prix')).prop('readonly', false);
						$('#'+this.id.replace('rad', 'prix')).prop('disabled', false);
						changeOpac(100, this.id.replace('rad', 'prix'));
					} else {
						$('#'+this.id.replace('rad', 'prix')).prop('readonly', true);
						$('#'+this.id.replace('rad', 'prix')).prop('disabled', true);
						$('#'+this.id.replace('rad', 'prix')).val('');
						changeOpac(20, this.id.replace('rad', 'prix'));
					}				
				}
			}
		});
	}
	
	if ($('#chk_echange_referent').is(':checked') && rad_class[1] == 'chk_referent' && mode == 'rad') {
		$('.tr_nego').each(function(){
			if ($('#'+this.id).find('input[type=checkbox]:checked').length == 0) {
				$('#'+this.id).find('input[type=checkbox]').each(function(){
					if ($(this).hasClass(rad_class[2]) && $(this).hasClass('chk_reference')) {
						$(this).prop('checked', 'checked');
						if($('#'+this.id.replace('rad', 'prix')).length) {
							$('#'+this.id.replace('rad', 'prix')).prop('readonly', false);
							$('#'+this.id.replace('rad', 'prix')).prop('disabled', false);
							changeOpac(100, this.id.replace('rad', 'prix'));
						}
					} else {
						$(this).prop('checked', false);
					}
				});
			} 	
		});
	} else if ($('#chk_echange_referent').is(':checked') && rad_class[1] == 'chk_referent' && mode == 'prix') {
		$('#tbody_tbl_maitrise_parc input[type=text]').each(function(){
			if ($(this).hasClass(rad_class[2]) && $(this).hasClass('chk_reference') && $(this).val() == '' && $(this).prop('disabled') == false) {
				$(this).val($('#'+rad_id).val())
			}
		});
	}
}

/* Cette fonction affiche le tableau de la négociation des parcelles et adapte le reste des options d'un échange. */
function showTblMaitriseParc() {
	if ($('#chk_maitrise_parc').is(':checked')) {
		$('#tbody_maitrise_parc').css('display', '' );
	} else {
		$('#tbody_maitrise_parc').css('display', 'none' );
	}
	if ($('#chk_modif_infos_interloc').is(':checked') || $('#chk_maitrise_parc').is(':checked')) {
		$('#chk_attente_reponse').prop('readonly', true );
		$('#chk_attente_reponse').prop('disabled', true );
		$('#lbl_attente_reponse').css('color', '#6a6a6a' );
		$('#chk_interloc_injoignable').prop('readonly', true );		
		$('#chk_interloc_injoignable').prop('disabled', true );
		$('#lbl_interloc_injoignable').css('color', '#6a6a6a' );
	} else {
		$('#chk_attente_reponse').prop('readonly', false );		
		$('#chk_attente_reponse').prop('disabled', false);
		$('#lbl_attente_reponse').css('color', '#004494' );
		$('#chk_interloc_injoignable').prop('disabled', false );
		$('#chk_interloc_injoignable').prop('readonly', false );
		$('#lbl_interloc_injoignable').css('color', '#004494' );
	}
}

/* Cette fonction permet d'adapter le tableau sur la négociation des parcelles si l'option "Cet échange concerne tous les interlocuteurs référencés" est cochée ou non.*/
function updateTblMaitriseParc() {
	$('#tbody_maitrise_parc tr.display_tbl_parc_maitrise_other_eaf').each(function(){
		if($('#chk_echange_referent').is(':checked')) {
			$(this).css('display', '')
		} else {
			$(this).css('display', 'none')
		}
	});
}

/* Cette fonction permet, dans l'onglet "Interlocuteurs" d'animer les libellés des interlocuteurs présents plusieurs fois au sein d'une même EAF au survol de l'un d'entre eux. */
function overInterloc(id, tbl, cur_class) {
	$('#'+tbl+' .'+cur_class).each(function(){
		if (this != id) {
			$(this).removeClass("no-breathing").addClass("breathing");
		}
	});
}

/* Cette fonction annule les effets de la fonction précédente une fois que le survol est terminé. */
function outInterloc(id, tbl, cur_class) {
	$('#'+tbl+' .'+cur_class).each(function(){
		$(this).removeClass("breathing").addClass("no-breathing");
	});
}

/* Cette fonction permet, dans l'onglet "Synhtèse", de mettre en évidence la dernière négociation en cours au survol de celui-ci. */
function overLot(tbl, lst_rel_af_foncier_id, lst_type_interloc_id) {
	color = ["#fe0000", "transparent repeating-linear-gradient(45deg, rgb(112, 173, 71), rgb(112, 173, 71) 5px, rgb(68, 114, 196) 5px, rgb(68, 114, 196) 10px) repeat scroll 0% 0%", "#70ad47", "#4472c4", "#70ad47", "#4472c4"];
	color[-1] = "#747474";
	color[-2] = "#fff";
	tbl_parc = lst_rel_af_foncier_id.split(';');
	tbl_type = lst_type_interloc_id.split(';');	
	
	for(i=0; i<tbl_type.length; i++) {
		sous_tbl_type = tbl_type[i].split(':');
		tbl_type_interloc_id = sous_tbl_type[1].split('-'); 
		
		if ($.inArray('1', tbl_type_interloc_id) == -1 && $.inArray('2', tbl_type_interloc_id) == -1) {
			$('#td_objectif_'+sous_tbl_type[0]).css('opacity', 0);
		}
		if ( $.inArray('2', tbl_type_interloc_id) > -1 || $.inArray('3', tbl_type_interloc_id) > -1 || $.inArray('4', tbl_type_interloc_id) > -1 ) {
			$('#agri_'+sous_tbl_type[0]).css('display', '');
			$('#agri_'+sous_tbl_type[0]).css('filter', 'grayscale(100%)');
		}
		if ($.inArray('5', tbl_type_interloc_id) > -1) {
			$('#chasseur_'+sous_tbl_type[0]).css('display', '');
			$('#chasseur_'+sous_tbl_type[0]).css('filter', 'grayscale(100%)');
		}
		if ($.inArray('6', tbl_type_interloc_id) > -1) {
			$('#pecheur_'+sous_tbl_type[0]).css('display', '');
			$('#pecheur_'+sous_tbl_type[0]).css('filter', 'grayscale(100%)');
		}
	}
	
	for(i=0; i<tbl_parc.length; i++) {
		sous_tbl_parc = tbl_parc[i].split(':');
		if (sous_tbl_parc[1] == '-2') {
			$('#label_'+sous_tbl_parc[0]).css('opacity', 0);
			$('#label_'+sous_tbl_parc[0]+'_contenance').css('opacity', 0);
		}
		if (sous_tbl_parc[1] == 4 || sous_tbl_parc[1] == 5) {
			$('#label_objectif_'+sous_tbl_parc[0]).html("accord");
		}
		if (sous_tbl_parc[1] == 1 || sous_tbl_parc[1] == 2 || sous_tbl_parc[1] == 3 || sous_tbl_parc[1] == 4 || sous_tbl_parc[1] == 5 || sous_tbl_parc[1] == 6) {
			$('#agri_'+sous_tbl_parc[0]).css('filter', 'initial');
			$('#chasseur_'+sous_tbl_parc[0]).css('filter', 'initial');
			$('#pecheur_'+sous_tbl_parc[0]).css('filter', 'initial');
		}
		if (sous_tbl_parc[1] == 0 || sous_tbl_parc[1] == 7) {
			$('#agri_'+sous_tbl_parc[0]).css('filter', 'hue-rotate(280deg) saturate(2000%) brightness(80%)');
			$('#chasseur_'+sous_tbl_parc[0]).css('filter', 'hue-rotate(280deg) saturate(2000%) brightness(80%)');
			$('#pecheur_'+sous_tbl_parc[0]).css('filter', 'hue-rotate(280deg) saturate(2000%) brightness(80%)');
		}
		$('#td_objectif_'+sous_tbl_parc[0]).css('background', color[sous_tbl_parc[1]]);
	}
}

/* Cette fonction annule les effets de la fonction précédente une fois que le survol est terminé. */
function outLot(tbl) {
	$('#'+tbl+' td.filtre_objectif').each(function(){
		$(this).css('background', $('#'+this.id.replace('td_objectif', 'hid_color_objectif')).val());
		$('#'+this.id).css('opacity', 1);
		$('#'+this.id.replace('td_objectif', 'label')).css('opacity', 1);
		$('#'+this.id.replace('td_objectif', 'label')+'_contenance').css('opacity', 1);
		$('#'+this.id.replace('td_objectif', 'agri')).css('filter', 'initial');
		$('#'+this.id.replace('td_objectif', 'agri')).css('display', 'none');
		$('#'+this.id.replace('td_objectif', 'chasseur')).css('filter', 'initial');
		$('#'+this.id.replace('td_objectif', 'chasseur')).css('display', 'none');
		$('#'+this.id.replace('td_objectif', 'pecheur')).css('filter', 'initial');
		$('#'+this.id.replace('td_objectif', 'pecheur')).css('display', 'none');
		if ($('#'+this.id.replace('td_objectif', 'hid_label_objectif')).val() == 'accord') {
			$('#'+this.id.replace('td_objectif', 'label_objectif')).html("accord");
		} else {
			$('#'+this.id.replace('td_objectif', 'label_objectif')).html('&nbsp;');
		}		
	});
}

/* Cette fonction permet d'adapter le formulaire en cas de gtoper NULL */
function gtoperUpdate() {
	if ($('#rad_gtoper1').is(':checked')) {
		$('#tr_ech_gtoper-1').css('display', '' );
		$('#tr_ech_gtoper-2').css('display', 'none');		
	} else if ($('#rad_gtoper2').is(':checked')) {
		$('#tr_ech_gtoper-1').css('display', 'none');
		$('#tr_ech_gtoper-2').css('display', '' );
	}
}


/*
  *********************************************
  *********************************************
  ****    IIIIII IIIIII IIIIII  I    I     ****
  ****    I    I    I   I    I   I  I      ****
  ****    IIIIII    I   IIIIII    II       ****
  ****    I    I  I I   I    I   I  I      ****
  ****    I    I  III   I    I  I    I     ****
  *********************************************
  ********************************************* 

  LES FONCTIONS SUIVANTES SONT BASEES SUR AJAX 
  ELLES PERMETTENT D'ADAPTER LE CONTENU DES PAGES
DYNAMIQUEMENT EN FONCTION DES CHOIX DE L'UTILISATEUR
            ET SANS RECHARGER LA PAGE
			
Elles fonctionnent toujours sur le même principe et sur le triptique PHP/JavaScipt/Ajax
A partir d'informations issues de la page elle-même et eventuellement d'autres informtions
filtrantes issues d'un formulaire HTML on adapte le contenu des pages
L'avantage principal est de pouvoir lancer ou relancer une requête SQL en fonction des informations choisies par l'utilisateur et de les afficher SANS RECHARGER LA PAGE
1ère partie on récupère toutes les informations nécessaires sur la page initiale
2ème partie on lance via Ajax une fonction php qui retourne du code (les fonctions PHP appelées dans cette partie sont toutes situées dans la page 'af_fct_ajax.php'
3ème partie ce code est utilisé pour adapter le contenu de la page initiale
 */
 
 /* Cette fonction est utilisée dans les pages fiche_saf.php
Elle permet d'afficher une liste des interlocuteurs sur une SAF
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------session_af_id->id de la saf*/
function create_lst_interlocs(cur_offset, session_af_id, scroll_top){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_nom //variable stockant le nom du propriétaire
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP ppar Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if ($('#filtre_interloc_section').length > 0 && $('#filtre_interloc_section').val() != '') {
		flt_section = $('#filtre_interloc_section').val();
		$('.filtre_section').val($('#filtre_interloc_section').val())
	} else {
		flt_section = "";
	}
	if ($('#filtre_interloc_par_num').length > 0 && $('#filtre_interloc_par_num').val() != '') {
		flt_par_num = $('#filtre_interloc_par_num').val();
		$('.filtre_par_num').val($('#filtre_interloc_par_num').val())
	} else {
		flt_par_num = "";
	}
	if ($('#filtre_interloc_nom').length > 0 && $('#filtre_interloc_nom').val() != '') {
		flt_nom = $('#filtre_interloc_nom').val();
		$('.filtre_nom').val($('#filtre_interloc_nom').val())
	} else {
		if ($('#hid_echange_ddenom').val() != '') {
			$('#filtre_interloc_nom').val($('#hid_echange_ddenom').val())
			$('#explorateur_echange').height($('#onglet').height());
			flt_nom = $('#hid_echange_ddenom').val();
		} else {
			flt_nom = "";
		}
	}
	
	if (typeof xhrPool == "undefined") {
		xhrPool = [];
	}
	$.each(xhrPool, function(idx, jqXHR) {
		jqXHR.abort();
	});
	//2ème partie basée sur Ajax
	$.ajax({
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType  : "json",
		data      :{ 
			fonction:'fct_create_lst_interlocs', params: {page: flt_page, var_session_af_id: session_af_id, section: flt_section, par_num: flt_par_num, nom: flt_nom, offset: cur_offset, var_scroll_top: scroll_top} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(jqXHR, settings){
			xhrPool.push(jqXHR);
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#tbody_interlocs_corps').html(data['code']); //la page est adaptée à partir de la variable 'data'
			$('#td_filtre_lst_interlocs_nom').css('display', '');
			$('html,body').scrollTop(scroll_top);
			if ($('#hid_echange_ddenom').val() != '') {
				$('#filtre_lst_interlocs_nom').val($('#hid_echange_ddenom').val())
				$('#explorateur_echange').height($('#onglet').height());
			}
		}
	});
}

 /* Cette fonction est utilisée dans les pages fiche_saf.php
Elle permet d'afficher une liste des parcelles sur une SAF
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------session_af_id->id de la saf*/
function create_lst_parc(cur_offset, session_af_id, scroll_top){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_nom //variable stockant le nom du propriétaire
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP ppar Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if ($('#filtre_parcelle_nom').length > 0 && $('#filtre_parcelle_nom').val() != '') {
		flt_nom = $('#filtre_parcelle_nom').val();
		$('.filtre_nom').val($('#filtre_parcelle_nom').val())
	} else {
		if ($('#hid_echange_ddenom').val() != '') {
			$('#filtre_parcelle_nom').val($('#hid_echange_ddenom').val())
			$('#explorateur_echange').height($('#onglet').height());
			flt_nom = $('#hid_echange_ddenom').val();
		} else {
			flt_nom = "";
		}
	}
	if ($('#filtre_parcelle_section').length > 0 && $('#filtre_parcelle_section').val() != '') {
		flt_section = $('#filtre_parcelle_section').val();
		$('.filtre_section').val($('#filtre_parcelle_section').val())
	} else {
		flt_section = "";
	}
	if ($('#filtre_parcelle_par_num').length > 0 && $('#filtre_parcelle_par_num').val() != '') {
		flt_par_num = $('#filtre_parcelle_par_num').val();
		$('.filtre_par_num').val($('#filtre_parcelle_par_num').val())
	} else {
		flt_par_num = "";
	}
	
	if (typeof xhrPool == "undefined") {
		xhrPool = [];
	}
	$.each(xhrPool, function(idx, jqXHR) {
		jqXHR.abort();
	});
	//2ème partie basée sur Ajax
	$.ajax({
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType  : "json",
		data      :{ 
			fonction:'fct_create_lst_parc', params: {page: flt_page, var_session_af_id: session_af_id, nom: flt_nom, section: flt_section, par_num: flt_par_num, offset: cur_offset, var_scroll_top: scroll_top} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(jqXHR, settings){
			xhrPool.push(jqXHR);
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#tbody_parc_corps').html(data['code']); //la page est adaptée à partir de la variable 'data'
			$('#td_filtre_lst_parc_nom').css('display', '');
			$('html,body').scrollTop(scroll_top);
			if ($('#hid_echange_ddenom').val() != '') {
				$('#filtre_lst_parc_nom').val($('#hid_echange_ddenom').val())
				$('#explorateur_echange').height($('#onglet').height());
			}
			$('.tbl_parc_occsol').each(function(){
				rel_saf_foncier_id = this.id.replace('tbl_parc_occsol-', '');
				$('#'+this.id).find('input').each(function(){				
					$('#lbl_prix_total_occsol-'+rel_saf_foncier_id).css('display', '')					
					if ($(this).val() == '') {						
						$('#lbl_prix_total_occsol-'+rel_saf_foncier_id).css('display', 'none')
						return false;
					}
				});
			});
		}
	});
}

 /* Cette fonction est utilisée dans les pages fiche_saf.php
Elle permet d'afficher une liste des echanges sur une SAF
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------session_af_id->id de la saf*/
function create_lst_echanges(cur_offset, session_af_id, scroll_top){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_nom //variable stockant le nom du propriétaire
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP ppar Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if ($('#filtre_echange_section').length > 0 && $('#filtre_echange_section').val() != '') {
		flt_section = $('#filtre_echange_section').val();
		$('.filtre_section').val($('#filtre_echange_section').val())
	} else {
		flt_section = "";
	}
	if ($('#filtre_echange_par_num').length > 0 && $('#filtre_echange_par_num').val() != '') {
		flt_par_num = $('#filtre_echange_par_num').val();
		$('.filtre_par_num').val($('#filtre_echange_par_num').val())
	} else {
		flt_par_num = "";
	}
	if ($('#filtre_echange_nom').length > 0 && $('#filtre_echange_nom').val() != '') {
		flt_nom = $('#filtre_echange_nom').val();
		$('.filtre_nom').val($('#filtre_echange_nom').val())
	} else {
		if ($('#hid_echange_ddenom').val() != '') {
			$('#filtre_echange_nom').val($('#hid_echange_ddenom').val())
			$('#explorateur_echange').height($('#onglet').height());
			flt_nom = $('#hid_echange_ddenom').val();
		} else {
			flt_nom = "";
		}
	}
	
	if (typeof xhrPool == "undefined") {
		xhrPool = [];
	}
	$.each(xhrPool, function(idx, jqXHR) {
		jqXHR.abort();
	});
	//2ème partie basée sur Ajax
	$.ajax({
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType  : "json",
		data      :{ 
			fonction:'fct_create_lst_echanges', params: {page: flt_page, var_session_af_id: session_af_id, section: flt_section, par_num: flt_par_num, nom: flt_nom, offset: cur_offset, var_scroll_top: scroll_top} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(jqXHR, settings){
			xhrPool.push(jqXHR);
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data			
			$('#tbody_echanges_corps').html(data['code']); //la page est adaptée à partir de la variable 'data'
			$('html,body').scrollTop(scroll_top);
			
			var myDiv  = document.getElementById('tbody_echanges_corps');
			var inputArr = myDiv.getElementsByTagName( "textarea" );
			for (var i = 0; i < inputArr.length; i++) {
				resizeTextarea(inputArr[i].id);
			}
			if ($('#hid_echange_ddenom').val() != '') {
				$('#filtre_echange_nom').val($('#hid_echange_ddenom').val())
				$('#explorateur_echange').height($('#onglet').height());
	 
			}
		}
	});
}

 /* Cette fonction est utilisée dans les pages fiche_saf.php
Elle permet d'afficher la synthèse sur une SAF
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------session_af_id->id de la saf*/
function create_synthese(passage, cur_offset, session_af_id, scroll_top) {
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_nom //variable stockant le nom du propriétaire
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP ppar Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if ($('#filtre_synthese_section').length > 0 && $('#filtre_synthese_section').val() != '') {
		flt_section = $('#filtre_synthese_section').val();
		$('.filtre_section').val($('#filtre_synthese_section').val())
	} else {
		flt_section = "";
	}
	if ($('#filtre_synthese_par_num').length > 0 && $('#filtre_synthese_par_num').val() != '') {
		flt_par_num = $('#filtre_synthese_par_num').val();
		$('.filtre_par_num').val($('#filtre_synthese_par_num').val())
	} else {
		flt_par_num = "";
	}
	if ($('#filtre_synthese_nom').length > 0 && $('#filtre_synthese_nom').val() != '') {
		flt_nom = $('#filtre_synthese_nom').val();
		$('.filtre_nom').val($('#filtre_synthese_nom').val())
	} else {
		if ($('#hid_echange_ddenom').val() != '') {
			$('#filtre_synthese_nom').val($('#hid_echange_ddenom').val())
			$('#explorateur_echange').height($('#onglet').height());
			flt_nom = $('#hid_echange_ddenom').val();
		} else {
			flt_nom = "";
		}
	}
	
	if (typeof xhrPool == "undefined") {
		xhrPool = [];
	}
	$.each(xhrPool, function(idx, jqXHR) {
		jqXHR.abort();
	});
	$.ajax({
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType  : "json",
		data      :{ 
			fonction:'fct_create_synthese', params: {var_session_af_id: session_af_id, offset: cur_offset, section: flt_section, par_num: flt_par_num, nom: flt_nom, var_scroll_top: scroll_top} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(jqXHR, settings){
			xhrPool.push(jqXHR);
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data			
			//la page est adaptée à partir de la variable 'data'			
			$('#tbody_synthese_evt_globaux').html(data['synthese_evt_globaux']);
			$('#tbody_synthese_bilan_af').html(data['synthese_bilan_af']);
			$('#tbody_synthese_parc_corps').html(data['tbody_synthese_parc_corps']);
			$('html,body').scrollTop(scroll_top);
			$('#lgd_synthese').css('display', '')
			
			/* La procédure qui suit permet de remplir automatiquement les options des filtres en listes déroulantes
			Après verification qu'aucun filtre n'est renseigné et que la page vient d'être chargée on lance la fonction GetComboBoxFilter */
			var flt_null = 'OUI'; //variable permettant de savoir aucun filtre n'est renseigné			
			$('#tbody_synthese_parc_entete select').each(function(){
				width = $(this).closest('.filtre').width() + 0;
				$('#tbody_synthese_parc_corps').find('.'+this.id).each(function(){
					$(this).width(width);
					$(this).css({"maxWidth": width});
					$(this).css({"minWidth": width});
				});
			});
			$('#tbody_synthese_parc_entete').find('input[type=text]').each(function(){
				width = $(this).closest('.filtre').width() + 0;
				$('#tbody_synthese_parc_corps').find('.'+this.id).each(function(){
					$(this).width(width);
					$(this).css({"maxWidth": width});
					$(this).css({"minWidth": width});
				});
			});
			
			td_lot_width = 0;
			$('#tbody_synthese_parc_corps').find('.filtre_lot').each(function(){
				if  ($(this).width() > td_lot_width) {
					td_lot_width = $(this).width();
				}
			});
			$('#tbody_synthese_parc_corps').find('.filtre_lot').each(function(){
				$(this).css({"maxWidth": td_lot_width});
				$(this).css({"minWidth": td_lot_width});
			});
			td_contenance_width = 0;
			$('#tbody_synthese_parc_corps').find('.filtre_contenance').each(function(){
				if  ($(this).width() > td_contenance_width) {
					td_contenance_width = $(this).width();
				}
			});
			$('#tbody_synthese_parc_corps').find('.filtre_contenance').each(function(){
				$(this).css({"maxWidth": td_contenance_width});
				$(this).css({"minWidth": td_contenance_width});
			});
			td_prix_lot_width = 0;
			$('#tbody_synthese_parc_corps').find('.synthese_prix_lot').each(function(){
				if  ($(this).width() > td_prix_lot_width) {
					td_prix_lot_width = $(this).width();
				}
			});
			$('#tbody_synthese_parc_corps').find('.synthese_prix_lot').each(function(){
				$(this).css({"maxWidth": td_prix_lot_width});
				$(this).css({"minWidth": td_prix_lot_width});
			});
			td_prix_eaf_width = 0;
			$('#tbody_synthese_parc_corps').find('.synthese_prix_eaf').each(function(){
				if  ($(this).width() > td_prix_eaf_width) {
					td_prix_eaf_width = $(this).width();
				}
			});
			$('#tbody_synthese_parc_corps').find('.synthese_prix_eaf').each(function(){
				$(this).css({"maxWidth": td_prix_eaf_width});
				$(this).css({"minWidth": td_prix_eaf_width});
			});
		}
	});
}

/* Cette fonction est utilisée dans la page fiche_saf.php
Elle permet de filtrer la liste des prorpiétaires pour l'importation d'un nouvel interlocuteur*/
function flt_lst_proprio_import(scrollto){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_nom //variable stockant le filtre correspondant au nom
	var flt_prenom //variable stockant le filtre correspondant au prénom
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	
	if (document.getElementById('flt_import_nom_usage') != null) {
		flt_nom = document.getElementById('flt_import_nom_usage').value;
	} else {
		flt_nom = "";
	}
	if (document.getElementById('flt_import_prenom_usage') != null) {
		flt_prenom = document.getElementById('flt_import_prenom_usage').value;
	} else {
		flt_prenom = "";
	}
		
	//2ème partie basée sur Ajax
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_flt_lst_proprio_import', params: {nom: flt_nom, prenom: flt_prenom} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();	
			if (scrollto && scrollto != 'null') {
				$('html,body').animate({scrollTop: scrollto}, 'slow');
			}
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#lst_import_proprio').html(data['lst_import_proprio']);
		}
	});
}

/* Cette fonction est utilisée dans la page fiche_saf.php
Elle permet de supprimer DEFINITIVEMENT un interlocuteur d'une EAF*/
function deleteInterlocConfirm(tbl_rel_saf_foncier_interlocs_id, interloc_id, session_af_id, entite_af_id, type_interloc_id, groupe_interloc_id){
	//2ème partie basée sur Ajax
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType  : "json",
		data      :{ 
			fonction:'fct_deleteInterlocConfirm', params: {var_tbl_rel_saf_foncier_interlocs_id: tbl_rel_saf_foncier_interlocs_id, var_interloc_id: interloc_id, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_type_interloc_id: type_interloc_id, var_groupe_interloc_id: groupe_interloc_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#confirmDeleteInterloc').css('display', 'none' );
			changeOpac(100, 'onglet');
			tbl_interloc_HTML = '';
			tbl_entite_af_id_UPDATE = [];
			tbl_entite_af_code_UPDATE = [];
			tbl_entite_af_id_HIDE = [];
			tbl_entite_af_id_NEW = [];
			id_scrollTo = 'N.D.';
			for(var i=0; i<data['entite_af_id'].length; i++) {
				if (document.getElementById('tr_'+data['entite_af_id'][i])) {
					if (data['entite_af_code'][i] == 'N.D.') {
						tbl_entite_af_id_HIDE.push(data['entite_af_id'][i]);							
					} else {
						tbl_entite_af_id_UPDATE.push(data['entite_af_id'][i]);	
						tbl_entite_af_code_UPDATE.push(data['entite_af_code'][i]);	
						if (id_scrollTo == 'N.D.')  {
							id_scrollTo = '#tr_'+data['entite_af_id'][i];
						}
					}
				} else {
					if (data['entite_af_code'][i] != 'N.D.') {
						tbl_entite_af_id_NEW.push(data['entite_af_id'][i]);
						tbl_interloc_HTML += '<tr id="tr_'+data['entite_af_id'][i]+'">'+data['entite_af_code'][i]+'</tr>'
					}
				}	
			}
			
			if (tbl_interloc_HTML != '') {
				$('#tbl_interloc').html(tbl_interloc_HTML+$('#tbl_interloc').html());
			}
			for(var i=0; i<tbl_entite_af_id_HIDE.length; i++) {
				$('#tr_'+tbl_entite_af_id_HIDE[i]).css('display', 'none' );
			}
			for(var i=0; i<tbl_entite_af_id_UPDATE.length; i++) {
				$('#tr_'+tbl_entite_af_id_UPDATE[i]).html(tbl_entite_af_code_UPDATE[i]);
			}
			if (id_scrollTo == 'N.D.' && tbl_entite_af_id_NEW.length > 0) {
				id_scrollTo = '#tr_'+tbl_entite_af_id_NEW[0];
			} else if (id_scrollTo == 'N.D.' && tbl_entite_af_id_NEW.length == 0) {
				id_scrollTo = 'html,body';
			}
			$('html,body').animate({scrollTop: ($(id_scrollTo).offset().top-50)}, 'slow');			
		}
	});
}

/* Cette fonction est utilisée dans la page fiche_saf.php pour supprimer tout une EAF d'une saf.
Elle est lancé suite à la validation du message de confirmation qui apparait lui-même lorsque aucun lot d'une EAF n'a de priorité d'animation*/
function deleteEAF(session_af_id, entite_af_id) {
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_deleteEAF', params: {var_session_af_id: session_af_id, var_entite_af_id: entite_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#confirmDeleteEAF').css('display', 'none' );
			$('#tr_parc_prix_eaf-'+entite_af_id).css('display', 'none' );
			$('#tr_parc_infos-'+entite_af_id).css('display', 'none' );
			$('#tr_parc_comm_eaf-'+entite_af_id).css('display', 'none' );
			changeOpac(100, 'onglet');
			$('#tbl_situation_cadastre').html(data['tbl_situation_cadastre']);
			$('#tbl_bilan_animation').html(data['tbl_bilan_animation']);
		}
	});
}

/* Cette fonction permet d'ajouter un interlocuteur à une EAF. */
function addInterloc() {
	var verif = 'OUI';
	var elements = document.forms['frm_new_interloc'].elements;
	var msg = "Veuillez compl&eacuteter votre saisie";
	for (i=0; i<elements.length; i++){			
		if (elements[i].id == 'lst_import_proprio' || elements[i].id == 'new_interloc_lst_particule') {
			if( $('#'+elements[i].id).prop('disabled') == false && $('#'+elements[i].id+' option:selected').index() == 0) {
				document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
				verif = 'NON';
				msg = "Veuillez v&eacute;rifier votre saisie";
			} else {
				document.getElementById(elements[i].id).style.border = "2px solid #fff";
			}
		}
		if (elements[i].id == 'new_type_interloc') {
			if( $('#'+elements[i].id).prop('disabled') == false && $('#'+elements[i].id+' option:selected').index() == 0) {
				document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
				verif = 'NON';
				msg = "Veuillez v&eacute;rifier votre saisie";
			} else {
				document.getElementById(elements[i].id).style.border = "2px solid #fff ";
			}
		}
		if (elements[i].id == 'new_interloc_nom_usage' || elements[i].id == 'new_interloc_prenom_usage') {
			if ($('#'+elements[i].id).prop('disabled') == false && $('#'+elements[i].id).val() == '') {
				document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
				verif = 'NON';
				msg = "Veuillez v&eacute;rifier votre saisie";
			} else {
				document.getElementById(elements[i].id).style.border = "2px solid #fff";
			}
		}
		if (elements[i].id == 'new_interloc_date_naissance') {			
			var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;		
			if ($('#'+elements[i].id).prop('disabled') == false && $('#'+elements[i].id).val() != '' && dateRegex.test($('#'+elements[i].id).val()) == false) {
				document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
				verif = 'NON';
				msg = "Veuillez v&eacute;rifier votre saisie";
			} else {
				document.getElementById(elements[i].id).style.border = "2px solid #fff";
			}
		}
	}
	
	if (document.getElementById('lbl_'+$('#need_entite_af_id').val()+'-'+$('#lst_import_proprio').val().substring($('#lst_import_proprio').val().indexOf('interloc_id:')+12, $('#lst_import_proprio').val().indexOf(';'))+'-'+$('#new_type_interloc').val())) {
		verif = 'NON';
		msg = "Un interlocuteur avec ces caract&eacute;ristiques existe d&eacute;j&agrave;";
	}
	
	if ($('#tbody_check_parc_interloc').find('input[type=checkbox]:checked').length < 1) {
		verif = 'NON';
		msg = "Veuillez cocher au moins un lot";
	}
	
	if (verif == 'OUI') {
		tbl_parc_interloc = [];
		
		$('.chk_parc').each(function(){
			if ($(this).is(':checked') && $(this).css('display') != 'none') {
				tbl_parc_interloc.push($(this).val()+':'+$('#new_interloc_ccodem-'+this.id.replace('chk_', '')).val()+':'+$('#new_interloc_ccodro-'+this.id.replace('chk_', '')).val());				
			}
		});
		if (tbl_parc_interloc.length == 0) {
			verif = 'NON';
			msg = "Veuillez v&eacute;rifier votre saisie";
			$('#fld_parc_new_interloc').css({'border-color':'#fe0000'});
			$('#fld_parc_new_interloc > legend').css({'color':'#fe0000'});
		}		 
		
		if ($('#lst_import_proprio').prop('disabled') == false) {
			$.ajax({
				url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
				type      : "post", //type de communication entre les différentes pages/fonctions/langages
				dataType  : "json",
				data      :{ 
				fonction:'fct_addInterloc', mode:'import_proprio', params: {session_af_id:$('#need_session_af_id').val(), entite_af_id:$('#need_entite_af_id').val(),  type_interloc:$('#new_type_interloc option:selected').val(), ccodro:$('#new_interloc_ccodro option:selected').val(), ccodem:$('#new_interloc_ccodem option:selected').val(), dnuper_sel:$('#lst_import_proprio option:selected').val(), var_tbl_parc_interloc: tbl_parc_interloc} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
				},
				beforeSend: function(){
					$("#loading").show();
				},
				complete: function(){
					$("#loading").hide();
				},
				success: function(data) { //On récupère du code dans la variable data
					$('#explorateur_interloc').css('display', 'none' );
					changeOpac(100, 'onglet');
					$('#onglet').height('auto');
					tbl_interloc_HTML = '';
					tbl_entite_af_id_UPDATE = [];
					tbl_entite_af_code_UPDATE = [];
					tbl_entite_af_id_HIDE = [];
					tbl_entite_af_id_NEW = [];
					id_scrollTo = 'N.D.';
					for(var i=0; i<data['entite_af_id'].length; i++) {
						if (document.getElementById('tr_'+data['entite_af_id'][i])) {
							if (data['entite_af_code'][i] == 'N.D.') {
								tbl_entite_af_id_HIDE.push(data['entite_af_id'][i]);							
							} else {
								tbl_entite_af_id_UPDATE.push(data['entite_af_id'][i]);	
								tbl_entite_af_code_UPDATE.push(data['entite_af_code'][i]);	
								if (id_scrollTo == 'N.D.')  {
									id_scrollTo = '#tr_'+data['entite_af_id'][i];
								}
							}
						} else {
							if (data['entite_af_code'][i] != 'N.D.') {
								tbl_entite_af_id_NEW.push(data['entite_af_id'][i]);
							}
						}	
					}
					
					if (tbl_entite_af_id_NEW.length > 0) {
						create_lst_interlocs(0, $('#need_session_af_id').val(), 0);
					} else {
						for(var i=0; i<tbl_entite_af_id_HIDE.length; i++) {
							$('#tr_'+tbl_entite_af_id_HIDE[i]).css('display', 'none' );
						}
						for(var i=0; i<tbl_entite_af_id_UPDATE.length; i++) {
							$('#tr_'+tbl_entite_af_id_UPDATE[i]).html(tbl_entite_af_code_UPDATE[i]);
						}
					}
					
					if (id_scrollTo == 'N.D.' && tbl_entite_af_id_NEW.length > 0) {
						id_scrollTo = '#tr_'+tbl_entite_af_id_NEW[0];
					} else if (id_scrollTo == 'N.D.' && tbl_entite_af_id_NEW.length == 0) {
						id_scrollTo = 'html,body';
					}
					
					$('html,body').animate({scrollTop: ($(id_scrollTo).offset().top-50)}, 'slow');
					
				}
			});
		} else {
			$.ajax({
				url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
				type      : "post", //type de communication entre les différentes pages/fonctions/langages
				dataType  : "json",
				data      :{				
				fonction:'fct_addInterloc', mode:'new_interloc', params: {session_af_id:$('#need_session_af_id').val(), entite_af_id:$('#need_entite_af_id').val(),  type_interloc:$('#new_type_interloc option:selected').val(), ccodro:$('#new_interloc_ccodro option:selected').val(), ccodem:$('#new_interloc_ccodem option:selected').val(), particule_id:$('#new_interloc_lst_particule option:selected').val(), nom:$('#new_interloc_nom_usage').val(), prenom:$('#new_interloc_prenom_usage').val(), ddenom:$('#new_interloc_ddenom').val(), var_tbl_parc_interloc: tbl_parc_interloc} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
				},
				beforeSend: function(){
					$("#loading").show();
				},
				complete: function(){
					$("#loading").hide();
				},
				success: function(data) { //On récupère du code dans la variable data
					$('#explorateur_interloc').css('display', 'none' );
					changeOpac(100, 'onglet');
					$('#onglet').height('auto');
					tbl_interloc_HTML = '';
					tbl_entite_af_id_UPDATE = [];
					tbl_entite_af_code_UPDATE = [];
					tbl_entite_af_id_HIDE = [];
					for(var i=0; i<data['entite_af_id'].length; i++) {
						if (document.getElementById('tr_'+data['entite_af_id'][i])) {
							if (data['entite_af_code'][i] == 'N.D.') {
								tbl_entite_af_id_HIDE.push(data['entite_af_id'][i]);							
							} else {
								tbl_entite_af_id_UPDATE.push(data['entite_af_id'][i]);	
								tbl_entite_af_code_UPDATE.push(data['entite_af_code'][i]);					
							}
						} else {
							if (data['entite_af_code'][i] != 'N.D.') {
								tbl_interloc_HTML += '<tr id="tr_'+data['new_entite_af_id']+'">'+data['new_eaf_code']+'</tr>'
							}
						}	
					}	
					if (tbl_interloc_HTML != '') {
						$('#tbl_interloc').html(tbl_interloc_HTML+$('#tbl_interloc').html());
					}
					for(var i=0; i<tbl_entite_af_id_HIDE.length; i++) {
						$('#tr_'+tbl_entite_af_id_HIDE[i]).css('display', 'none' );
					}
					for(var i=0; i<tbl_entite_af_id_UPDATE.length; i++) {
						$('#tr_'+tbl_entite_af_id_UPDATE[i]).html(tbl_entite_af_code_UPDATE[i]);
					}
				}
			});	
		}		
	} else {
		$('#new_interloc_prob_saisie').html(msg);
		$('#new_interloc_prob_saisie').css('display', '');
	}
}

/* Cette fonction permet d'afficher la fiche descriptive d'un interlocuteur et d'adapter les informations à partir de la BD. */
function showFormUpdateInterloc(scrollTo_id, interloc_id, type_interloc_id, interloc_lib, session_af_id, entite_af_id, tbl_rel_saf_foncier_interlocs_id, gtoper) {
	$('#explorateur_update_interloc').css('display', '' );		
	
	if (($('#explorateur_sousdiv_update_interloc').height() + $('#explorateur_sousdiv_update_interloc').offset().top - $('#main').offset().top) > $('#main').height()) {
		$('#main').attr('style', "height:" + ($('#explorateur_sousdiv_update_interloc').height() + $('#explorateur_sousdiv_update_interloc').offset().top - $('#main').offset().top) + "px !important;");
	}
	if ($(scrollTo_id).offset().top - 30 + ($('#explorateur_sousdiv_update_interloc').height()/2) > $('#onglet').height() && $('#onglet').height() > $('#explorateur_sousdiv_update_interloc').height()) {
		id_btn_top = $('#onglet').height() - ($('#explorateur_sousdiv_update_interloc').height());
	} else {
		id_btn_top = $(scrollTo_id).offset().top - ($('#explorateur_sousdiv_update_interloc').height()/2) - 30;
	}
		
	changeOpac(20, 'onglet'); 
	if (gtoper == '2') {
		$('#tr_gtoper-1').css('display', 'none');
		$('#tr_gtoper-2').css('display', '' );	
	} else if (gtoper == '1') {
		$('#tr_gtoper-1').css('display', '' );
		$('#tr_gtoper-2').css('display', 'none');
	} else {
		$('#tr_gtoper-1').css('display', '' );
		$('#tr_gtoper-2').css('display', '');
	}
	
	$('#update_interloc_id').val(interloc_id);
	$('#hid_update_entite_af_id').val(entite_af_id);
	$('#explorateur_update_interloc').height($('#onglet').height());
	$('#update_interloc_interloc_lib').html('Mise &agrave; jour des donn&eacute;es pour ' + interloc_lib);
	$('#explorateur_sousdiv_update_interloc').css('top', id_btn_top +'px');
	$('#btn_valide_update_interloc').attr('onclick', "updateInterloc("+interloc_id+", "+session_af_id+", '"+entite_af_id+"', '"+tbl_rel_saf_foncier_interlocs_id+"', '"+gtoper+"');");
	
	$('html,body').animate({scrollTop: ($(scrollTo_id).offset().top-150)}, 'slow');
	
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showFormUpdateInterloc', params: {var_interloc_id: interloc_id, var_type_interloc_id: type_interloc_id, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			var elements = document.forms['frm_update_interloc'].elements;
			for (i=0; i<elements.length; i++){		
				if (elements[i].type == 'select-one') {
					$('#'+elements[i].id+' option[value="'+data[elements[i].id]+'"]').attr("selected", "selected");
					$('#'+elements[i].id).css({'border-color': '#fff '});
				} else if (elements[i].type == 'text' || elements[i].type == 'textarea') {	
					if(data[elements[i].id] != 'N.D.' && data[elements[i].id] != 'N.P.') {
						$('#'+elements[i].id).val(data[elements[i].id])
					} else {
						$('#'+elements[i].id).val('')
					}
					$('#'+elements[i].id).css({'border-color': '#fff '});
				}
			}
			$('#update_interloc_prob_saisie').css('display', 'none');
			$('#update_type_interloc option:selected').addClass(String(interloc_id));
			$('#hid_update_parc_interloc').val(data['tbl_parc_interloc']);
			$('#tbody_update_check_parc_interloc').html(data['parc_interloc'])
			
			$('#fld_update_parc_interloc').find('input[type=checkbox]').each(function(){
				if ($('#update_type_interloc option:selected').hasClass('all_eaf')) {
					$(this).prop('checked', 'checked');
					$(this).prop('readonly', true);
					$(this).prop('disabled', true);
				} else {
					$(this).prop('readonly', false);
					$(this).prop('disabled', false);
				}
			});
		}
	});
}

/* Cette fonction permet de mettre à jour la fiche descriptive d'un interlocuteur. */
function updateInterloc(interloc_id, session_af_id, entite_af_id, tbl_rel_saf_foncier_interlocs_id, gtoper) {
	verif = 'OUI';
	detail_display = $('#interloc_detail_'+interloc_id+"_"+entite_af_id).css('display');
	if ($('#update_type_interloc') && $('#update_type_interloc option:selected').val() != null && $('#update_type_interloc option:selected').val() != '-1') {
		update_type_interloc = $('#update_type_interloc').val();
		$('#update_type_interloc').css('border-color', '#fff');
	} else {
		verif = 'NON';
		$('#update_type_interloc').css('border-color', '#fe0000');
		msg = "Veuillez v&eacute;rifier votre saisie";
	}
	if (gtoper == '1') {
		if ($('#update_interloc_lst_particule') && $('#update_interloc_lst_particule option:selected').val() != null && $('#update_interloc_lst_particule option:selected').val() != '-1') {
			update_interloc_lst_particule = $('#update_interloc_lst_particule').val();
			$('#update_interloc_lst_particule').css('border-color', '#fff');
		} else {
			verif = 'NON';
			$('#update_interloc_lst_particule').css('border-color', '#fe0000');
			msg = "Veuillez v&eacute;rifier votre saisie";
		}
		if ($('#update_interloc_prenom_usage') && $('#update_interloc_prenom_usage').val() != null && $('#update_interloc_prenom_usage').val() != '') {
			update_interloc_prenom_usage = $('#update_interloc_prenom_usage').val();
			$('#update_interloc_prenom_usage').css('border-color', '#fff');
		} else {
			verif = 'NON';
			$('#update_interloc_prenom_usage').css('border-color', '#fe0000');
			msg = "Veuillez v&eacute;rifier votre saisie";
		}
		if ($('#update_interloc_nom_usage') && $('#update_interloc_nom_usage').val() != null && $('#update_interloc_nom_usage').val() != '') {
			update_interloc_nom_usage = $('#update_interloc_nom_usage').val();
			$('#update_interloc_nom_usage').css('border-color', '#fff');
		} else {
			verif = 'NON';
			$('#update_interloc_nom_usage').css('border-color', '#fe0000');
			msg = "Veuillez v&eacute;rifier votre saisie";
		}
		update_interloc_date_naissance = $('#update_interloc_date_naissance').val();
		update_interloc_lieu_naissance = $('#update_interloc_lieu_naissance').val();
		update_interloc_nom_conjoint = $('#update_interloc_nom_conjoint').val();
		update_interloc_prenom_conjoint = $('#update_interloc_prenom_conjoint').val();
		update_interloc_nom_jeunefille = $('#update_interloc_nom_jeunefille').val();
		update_interloc_ddenom = null;
	} else {
		if ($('#update_interloc_ddenom') && $('#update_interloc_ddenom').val() != null && $('#update_interloc_ddenom').val() != '') {
			update_interloc_ddenom = $('#update_interloc_ddenom').val();
			$('#update_interloc_ddenom').css('border-color', '#fff');
		} else {
			verif = 'NON';
			$('#update_interloc_ddenom').css('border-color', '#fe0000');
			msg = "Veuillez v&eacute;rifier votre saisie";
		}
		update_interloc_lst_particule = null;
		update_interloc_nom_usage = null;
		update_interloc_prenom_usage = null;
		update_interloc_date_naissance = null;
		update_interloc_lieu_naissance = null;
		update_interloc_nom_conjoint = null;
		update_interloc_prenom_conjoint = null;
		update_interloc_nom_jeunefille = null;
	}
		
	if (document.getElementById('lbl_'+entite_af_id+'-'+interloc_id+'-'+$('#update_type_interloc').val()) && !$('#update_type_interloc option:selected').hasClass($('#update_interloc_id').val())) {
		verif = 'NON';
		msg = "Un interlocuteur avec ces caract&eacute;ristiques existe d&eacute;j&agrave;";
	}
	
	tbl_rel_saf_foncier_id_assoc = [];
	tbl_rel_saf_foncier_id_dissoc = [];
	
	$('.update_chk_parc').each(function(){
		if ($(this).is(':checked') && $(this).css('display') != 'none') {
			tbl_rel_saf_foncier_id_assoc.push($(this).val()+':'+$('#update_interloc_ccodem-'+this.id.replace('update_chk_', '')).val()+':'+$('#update_interloc_ccodro-'+this.id.replace('update_chk_', '')).val());
		} else {
			tbl_rel_saf_foncier_id_dissoc.push($(this).val());
		}
	});
	if (tbl_rel_saf_foncier_id_assoc.length == 0) {
		verif = 'NON';
		msg = "Veuillez v&eacute;rifier votre saisie";
		$('#fld_update_parc_interloc').css({'border-color':'#fe0000'});
		$('#fld_update_parc_interloc > legend').css({'color':'#fe0000'});
	}	
	
	if (tbl_rel_saf_foncier_id_dissoc.length == 0) {
		tbl_rel_saf_foncier_id_dissoc = 'N.D.';
	}
		
	if (verif == 'OUI') {	
		$.ajax({ 
			url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json",
			data      :{ 
				fonction:'fct_updateInterloc', params: {var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_interloc_id: interloc_id, var_tbl_rel_saf_foncier_interlocs_id: tbl_rel_saf_foncier_interlocs_id, type_interloc:$('#update_type_interloc option:selected').val(), etat_interloc:$('#update_etat_interloc option:selected').val(), ccodro:$('#update_interloc_ccodro option:selected').val(), ccodem:$('#update_interloc_ccodem option:selected').val(), particule_id:update_interloc_lst_particule, nom:update_interloc_nom_usage, ddenom:update_interloc_ddenom, prenom:update_interloc_prenom_usage, date_naissance:update_interloc_date_naissance, lieu_naissance:update_interloc_lieu_naissance, nom_conjoint:update_interloc_nom_conjoint, prenom_conjoint:update_interloc_prenom_conjoint, nom_jeune_fille:update_interloc_nom_jeunefille, adresse1:$('#update_interloc_adresse1').val(), adresse2:$('#update_interloc_adresse2').val(), adresse3:$('#update_interloc_adresse3').val(), adresse4:$('#update_interloc_adresse4').val(), tel_fixe1:$('#update_interloc_tel_fix1').val(), tel_fixe2:$('#update_interloc_tel_fix2').val(), tel_portable1:$('#update_interloc_tel_portable1').val(), tel_portable2:$('#update_interloc_tel_portable2').val(), fax:$('#update_interloc_tel_fax').val(), mail:$('#update_interloc_mail').val(), commentaires:$('#update_interloc_commentaires').val(), var_tbl_rel_saf_foncier_id_assoc: tbl_rel_saf_foncier_id_assoc, var_tbl_rel_saf_foncier_id_dissoc: tbl_rel_saf_foncier_id_dissoc, var_gtoper: gtoper} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#explorateur_update_interloc').css('display', 'none' );
				
				changeOpac(100, 'onglet');
				
				tbl_interloc_HTML = '';
				tbl_entite_af_id_UPDATE = [];
				tbl_entite_af_code_UPDATE = [];
				tbl_entite_af_id_HIDE = [];
				for(var i=0; i<data['entite_af_id'].length; i++) {
					if (document.getElementById('tr_'+data['entite_af_id'][i])) {
						if (data['entite_af_code'][i] == 'hide') {
							tbl_entite_af_id_HIDE.push(data['entite_af_id'][i]);							
						} else {
							tbl_entite_af_id_UPDATE.push(data['entite_af_id'][i]);	
							tbl_entite_af_code_UPDATE.push(data['entite_af_code'][i]);					
						}
					} else {
						if (data['entite_af_code'][i] != 'hide') {
							tbl_interloc_HTML += '<tr id="tr_'+data['entite_af_id'][i]+'">'+data['entite_af_code'][i]+'</tr>';
						}
					}	
				}	
				for(var i=0; i<tbl_entite_af_id_HIDE.length; i++) {
					$('#tr_'+tbl_entite_af_id_HIDE[i]).css('display', 'none' );
					$('html,body').animate({scrollTop: ($('#tr_'+tbl_entite_af_id_UPDATE[0]).offset().top-150)}, 'fast');
				}
				for(var i=0; i<tbl_entite_af_id_UPDATE.length; i++) {
					if (tbl_entite_af_code_UPDATE[i] != 'hide') {
						$('#tr_'+tbl_entite_af_id_UPDATE[i]).html(tbl_entite_af_code_UPDATE[i]);
						$('#tr_'+tbl_entite_af_id_UPDATE[i]).css('display', '' );
					} else {
						$('#tr_'+tbl_entite_af_id_UPDATE[i]).css('display', 'none');
						$('html,body').animate({scrollTop: ($('#tr_'+tbl_entite_af_id_UPDATE[0]).offset().top-150)}, 'fast');
					}
				}
				if (tbl_interloc_HTML != '') {
					$('#tbody_interlocs_corps').html(tbl_interloc_HTML+$('#tbody_interlocs_corps').html());
				}
			}
		});
	} else {
		$('#update_interloc_prob_saisie').html(msg);
		$('#update_interloc_prob_saisie').css('display', '');
	}
}

/* Cette fonction permet de remplir correctement l'édition d'une fiche interlocuteur à partir de la base de données. */
function showTblModifInfosInterloc(interloc_id, session_af_id, entite_af_id, gtoper) {
	if (gtoper == '1') {
		$('#tr_ech_gtoper-1').css('display', '' );
		$('#tr_ech_gtoper-2').css('display', 'none');		
	} else {
		$('#tr_ech_gtoper-1').css('display', 'none');
		$('#tr_ech_gtoper-2').css('display', '' );
	}
	
	if ($('#chk_modif_infos_interloc').is(':checked')) {
		$('#tbody_modif_infos_interloc').css('display', '' );
	} else {
		$('#tbody_modif_infos_interloc').css('display', 'none' );
	}
	if ($('#chk_modif_infos_interloc').is(':checked') || $('#chk_maitrise_parc').is(':checked')) {
		$('#chk_attente_reponse').prop('readonly', true );
		$('#chk_attente_reponse').prop('disabled', true );
		$('#lbl_attente_reponse').css('color', '#6a6a6a' );
		$('#chk_interloc_injoignable').prop('readonly', true );		
		$('#chk_interloc_injoignable').prop('disabled', true );
		$('#lbl_interloc_injoignable').css('color', '#6a6a6a' );
	} else {
		$('#chk_attente_reponse').prop('readonly', false );		
		$('#chk_attente_reponse').prop('disabled', false);
		$('#lbl_attente_reponse').css('color', '#004494' );
		$('#chk_interloc_injoignable').prop('disabled', false );
		$('#chk_interloc_injoignable').prop('readonly', false );
		$('#lbl_interloc_injoignable').css('color', '#004494' );
	}	
	
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showTblModifInfosInterloc', params: {var_interloc_id: interloc_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#tbody_modif_infos_interloc input[type=text]').each(function(){
				if(data[this.id.replace('ech_update', 'update')] != 'N.D.' && data[this.id.replace('ech_update', 'update')] != 'N.P.') {
					$('#'+this.id).val(data[this.id.replace('ech_update', 'update')])
				} else {
					$('#'+this.id).val('')
				}
				$('#'+this.id).css({'border-color': '#fff '});
			});
			$('#tbody_modif_infos_interloc select').each(function(){
				$('#'+this.id+' option[value="'+data[this.id.replace('ech_update', 'update')]+'"]').attr("selected", "selected");
				$('#'+this.id).css({'border-color': '#fff '});
			});
		}
	});
}

/* Cette fonction permet d'afficher correctement la fenêtre de gestion des référents à partir de la BD */
function showFormGestref(entite_af_id, session_af_id) {
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showFormGestref', params: {var_entite_af_id: entite_af_id, var_session_af_id: session_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#explorateur_gestion_referent').css('display', '' );
			changeOpac(20, 'onglet');
			$('#tbl_gestRef').html(data['code']);
			
			id_btn = 'btn_gestref_' + entite_af_id			
			if (($('#explorateur_gestion_referent_sousdiv').height() + $('#explorateur_gestion_referent_sousdiv').offset().top - $('#main').offset().top) > $('#main').height()) {
				$('#main').attr('style', "height:" + ($('#explorateur_gestion_referent_sousdiv').height() + $('#explorateur_gestion_referent_sousdiv').offset().top - $('#main').offset().top) + "px !important;");
			}
			if ($('#'+id_btn).offset().top - 30 + ($('#explorateur_gestion_referent_sousdiv').height()/2) > $('#onglet').height() && $('#onglet').height() > $('#explorateur_gestion_referent_sousdiv').height()) {
				id_btn_top = $('#onglet').height() - ($('#explorateur_gestion_referent_sousdiv').height());
			} else {
				id_btn_top = $('#'+id_btn).offset().top - ($('#explorateur_gestion_referent_sousdiv').height()/2) - 30;
			}
			id_btn_top = $('#'+id_btn).offset().top - ($('#explorateur_gestion_referent_sousdiv').height()/2) - 30;
			$('#explorateur_gestion_referent').css('top', id_btn_top +'px');			
			
			(function() {        
				var dndHandler = {					
					draggedElement: null, // Propriété pointant vers l'élément en cours de déplacement					
					applyDragEvents: function(element) {						
						element.draggable = true;
						var dndHandler = this; // Cette variable est nécessaire pour que l'événement "dragstart" ci-dessous accède facilement au namespace "dndHandler"						
						element.addEventListener('dragstart', function(e) {
							dndHandler.draggedElement = e.target; // On sauvegarde l'élément en cours de déplacement
							e.dataTransfer.setData('text/plain', ''); // Nécessaire pour Firefox
						}, false);						
					},
			 
					applyDropEvents: function(dropper) {
						dropper.addEventListener('dragover', function(e) {
							var target = e.target,
								draggedElement = dndHandler.draggedElement; // Récupération de l'élément concerné
							
							if (this.id.substring(0, this.id.indexOf('_')).replace('dropper_', '') == draggedElement.id.substring(0, draggedElement.id.indexOf('_')).replace('div_', '') && this.id != 'dropper_ref') {
								drop_class = 'cant_drop_hover';
							} else {
								drop_class = 'drop_hover';
							}
							e.preventDefault(); // On autorise le drop d'éléments
							this.className = 'dropper '+drop_class; // Et on applique le design adéquat à notre zone de drop quand un élément la survole
						}, false);						
						dropper.addEventListener('dragleave', function() {
							this.className = 'dropper'; // On revient au design de base lorsque l'élément quitte la zone de drop
						});						
						var dndHandler = this; // Cette variable est nécessaire pour que l'événement "drop" ci-dessous accède facilement au namespace "dndHandler"

						dropper.addEventListener('drop', function(e) {
							var target = e.target,
								draggedElement = dndHandler.draggedElement, // Récupération de l'élément concerné
								clonedElement = draggedElement.cloneNode(true); // On créé immédiatement le clone de cet élément
								
							while(target.className.indexOf('dropper') == -1) { // Cette boucle permet de remonter jusqu'à la zone de drop parente
								target = target.parentNode;
							}
							
							if (target.id.replace('dropper_', '') != draggedElement.id.replace('div_', '') && dropper.className != 'dropper cant_drop_hover' && dropper.id != draggedElement.parentNode.id) {
								
								clonedElement = target.appendChild(clonedElement); // Ajout de l'élément cloné à la zone de drop actuelle
								dndHandler.applyDragEvents(clonedElement); // Nouvelle application des événements qui ont été perdus lors du cloneNode()
								
								draggedElement.parentNode.removeChild(draggedElement); // Suppression de l'élément d'origine
								
								$.ajax({ //On entre maintenant dans la partie ajax
									url       : 'af_fct_ajax.php', //variable permettant de savoir où se trouve la fonction php appelé plus loin
									type      : "post", //type de communication entre les différentes pages/fonctions/langages
									dataType : "json",
									data      :{ 
										fonction:'fct_majRefInterloc', params: {var_ref_interloc_id: target.id.replace('dropper_', ''), var_interloc_id: clonedElement.id.replace('div_', ''), var_session_af_id: session_af_id, var_entite_af_id: entite_af_id}//variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
									},
									success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
										if (target.id == 'dropper_ref') {
											$('#'+target.id+' .checkRef').css('display', '');
											$('#'+target.id+' .checkRef').click(function(){
												if ($(this).is(':checked')) {
													$('#'+this.id.replace('ref', 'dropper')).css('display', '');
													$(this).parent().attr('draggable', false);
													$(this).parent().removeClass("draggable").addClass("not_draggable");
												} else {
													$('#'+this.id.replace('ref', 'dropper')).css('display', 'none');
													if (!$(this).parent().hasClass('never')) {
														$(this).parent().attr('draggable', true);
														$(this).parent().removeClass("not_draggable").addClass("draggable");
													}
												}
											});
										} else {
											$('#'+target.id+' .checkRef').css('display', 'none');
										}
										$('#tbl_'+entite_af_id).html(data['new_eaf_code']);
										showFormGestref(entite_af_id, session_af_id)
									}
								});
							}
							target.className = 'dropper'; // Application du design par défaut
						});
					}
				};				
				
				$('.checkRef').click(function(){					
					$('#lbl_'+entite_af_id+'-'+this.id.replace('ref_', '')).toggleClass("referent");
					if ($(this).is(':checked')) {
						$('#'+this.id.replace('ref', 'dropper')).css('display', '');
						$(this).parent().attr('draggable', false);
						$(this).parent().removeClass("draggable").addClass("not_draggable");						
					} else {						
						old_interloc_ref_id = this.id.replace('ref_', '');
						$('#'+this.id.replace('ref', 'dropper')).find('.draggable').each(function(){
							document.getElementById('dropper_ref').appendChild(this);
							$('#dropper_ref .checkRef').css('display', '');
							$('#dropper_ref .checkRef').click(function(){
								if ($(this).is(':checked')) {
									$('#'+this.id.replace('ref', 'dropper')).css('display', '');
									$(this).parent().attr('draggable', false);
									$(this).parent().removeClass("draggable").addClass("not_draggable");
								} else {
									$('#'+this.id.replace('ref', 'dropper')).css('display', 'none');
									if (!$(this).parent().hasClass('never')) {
										$(this).parent().attr('draggable', true);
										$(this).parent().removeClass("not_draggable").addClass("draggable");
									}
								}
							});
							dndHandler.applyDragEvents(this.parentNode);
						});
						$.ajax({ //On entre maintenant dans la partie ajax
							url       : 'af_fct_ajax.php', //variable permettant de savoir où se trouve la fonction php appelé plus loin
							type      : "post", //type de communication entre les différentes pages/fonctions/langages
							dataType : "json",
							data      :{ 
								fonction:'fct_RemoveRefInterloc', params: {var_old_interloc_ref_id: old_interloc_ref_id, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id}//variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
							},
							success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
								$('#tbl_'+entite_af_id).html(data['new_eaf_code']);
								showFormGestref(entite_af_id, session_af_id);
							}
						});
						$('#'+this.id.replace('ref', 'dropper')).css('display', 'none');
						if (!$(this).parent().hasClass('never')) {
							$(this).parent().attr('draggable', true);
							$(this).parent().removeClass("not_draggable").addClass("draggable");
						}
					}
				});
				var elements = document.querySelectorAll('.draggable'),
					elementsLen = elements.length;
				
				for(var i = 0 ; i < elementsLen ; i++) {
					dndHandler.applyDragEvents(elements[i]); // Application des paramètres nécessaires aux élément déplaçables			
				}
				
				var droppers = document.querySelectorAll('.dropper'),
					droppersLen = droppers.length;
				
				for(var i = 0 ; i < droppersLen ; i++) {
					dndHandler.applyDropEvents(droppers[i]); // Application des événements nécessaires aux zones de drop
				}		
			})();
		}
	});
}

/* Cette fonction permet d'afficher correctement la fenêtre de gestion des interlocuteurs de substitution à partir de la BD */
function showFormGestsubst(entite_af_id, session_af_id) {
	id_btn = 'btn_gestsubst_' + entite_af_id
	if ($('#'+id_btn).offset().top - 220 + 170 > $('#onglet').height()) {
		id_btn_top = $('#onglet').height() - 170;
	} else {
		id_btn_top = $('#'+id_btn).offset().top - 220;
	}
	$('#explorateur_gestion_substitution').css('top', id_btn_top +'px');
	
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showFormGestsubst', params: {var_entite_af_id: entite_af_id, var_session_af_id: session_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#explorateur_gestion_substitution').css('display', '' );
			changeOpac(20, 'onglet');
			$('#tbl_gestSubst').html(data['code']);
			(function() {        
				var dndHandler = {					
					draggedElement: null, // Propriété pointant vers l'élément en cours de déplacement					
					applyDragEvents: function(element) {						
						element.draggable = true;
						var dndHandler = this; // Cette variable est nécessaire pour que l'événement "dragstart" ci-dessous accède facilement au namespace "dndHandler"						
						element.addEventListener('dragstart', function(e) {
							dndHandler.draggedElement = e.target; // On sauvegarde l'élément en cours de déplacement
							e.dataTransfer.setData('text/plain', ''); // Nécessaire pour Firefox
						}, false);						
					},
			 
					applyDropEvents: function(dropper) {
						dropper.addEventListener('dragover', function(e) {
							var target = e.target,
								draggedElement = dndHandler.draggedElement; // Récupération de l'élément concerné							
							if (this.id.replace('dropper_', '').substring(0, this.id.replace('dropper_', '').indexOf('_')) == draggedElement.id.replace('div_', '').substring(0, draggedElement.id.replace('div_', '').indexOf('_'))) {
								drop_class = 'cant_drop_hover';
							} else {
								drop_class = 'drop_hover';
								$('#'+this.id).find('.draggable').each(function(){
									if ((this.id == draggedElement.id || $(this).hasClass('4') || $('#'+draggedElement.id).hasClass('4')) && dropper.id != 'dropper_subst') {
										drop_class = 'cant_drop_hover';
									}
								});
							}
							e.preventDefault(); // On autorise le drop d'éléments
							this.className = 'dropper '+drop_class; // Et on applique le design adéquat à notre zone de drop quand un élément la survole
						}, false);						
						dropper.addEventListener('dragleave', function() {
							this.className = 'dropper'; // On revient au design de base lorsque l'élément quitte la zone de drop
						});						
						var dndHandler = this; // Cette variable est nécessaire pour que l'événement "drop" ci-dessous accède facilement au namespace "dndHandler"
						
						dropper.addEventListener('drop', function(e) {
							var target = e.target,
								draggedElement = dndHandler.draggedElement, // Récupération de l'élément concerné
								clonedElement = draggedElement.cloneNode(true); // On créé immédiatement le clone de cet élément
								
							while(target.className.indexOf('dropper') == -1) { // Cette boucle permet de remonter jusqu'à la zone de drop parente
								target = target.parentNode;
							}
														
							if (target.id.replace('dropper_', '') != draggedElement.id.replace('div_', '') && dropper.className != 'dropper cant_drop_hover' && dropper.id != draggedElement.parentNode.id) {
								if (target.id != 'dropper_subst') {
									clonedElement = target.appendChild(clonedElement); // Ajout de l'élément cloné à la zone de drop actuelle
								}
								origin = draggedElement.parentNode;
								dndHandler.applyDragEvents(clonedElement); // Nouvelle application des événements qui ont été perdus lors du cloneNode()
								if (target.id == 'dropper_subst') {									
									draggedElement.parentNode.removeChild(draggedElement); // Suppression de l'élément d'origine									
								}
								
								$.ajax({ //On entre maintenant dans la partie ajax
									url       : 'af_fct_ajax.php', //variable permettant de savoir où se trouve la fonction php appelé plus loin
									type      : "post", //type de communication entre les différentes pages/fonctions/langages
									dataType : "json",
									data      :{ 
										fonction:'fct_majSubstInterloc', params: {var_interloc: target.id.replace('dropper_', ''), var_subst_interloc: clonedElement.id.replace('div_', ''), var_interloc_origin: origin.id.replace('dropper_', ''), var_session_af_id: session_af_id, var_entite_af_id: entite_af_id}//variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
									},
									success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
										$('#tbl_'+entite_af_id).html(data['new_eaf_code']);
										showFormGestsubst(entite_af_id, session_af_id);
									}
								});
						   }
						   target.className = 'dropper'; // Application du design par défaut
						});
						
					}
				};
			
				var elements = document.querySelectorAll('.draggable'),
					elementsLen = elements.length;
				
				for(var i = 0 ; i < elementsLen ; i++) {
					dndHandler.applyDragEvents(elements[i]); // Application des paramètres nécessaires aux élément déplaçables			
				}
				
				var droppers = document.querySelectorAll('.dropper'),
					droppersLen = droppers.length;
				
				for(var i = 0 ; i < droppersLen ; i++) {
					dndHandler.applyDropEvents(droppers[i]); // Application des événements nécessaires aux zones de drop
				}		
			})();
		}
	});
}

/* Cette fonction permet de mettre à jour la base de données lorsqu'un clic sur une des disquettes d'enregistrement est réalisé sur l'onglet "Parcelles/ Lots". */
function updateLot(id, entite_af_id, session_af_id, nbr_echanges) {	
	if (id.substring(0,6) == 'occsol') {
		value = '';
		for (i=0; i<$('#tbl_parc_'+id+' tr').length; i++){
			if ($('#type_'+id+'-'+i).val() != '-1' && $('#pourc_'+id+'-'+i).val() != null && $('#pourc_'+id+'-'+i).val() != '') {
				value += $('#type_'+id+'-'+i).val()+':'+$('#pourc_'+id+'-'+i).val();
				if ($('#prix_'+id+'-'+i).val() != null && $('#prix_'+id+'-'+i).val() != '') {
					value += ':'+$('#prix_'+id+'-'+i).val()+';';
				} else {
					value += ':'+'NULL;';
				}
			}
		}
	} else if (id.substring(0,4) == 'prix') {
		value = '';
		for (i=0; i<$('#tbl_parc_'+id+' tr.prix_detail').length; i++){
			if ($('#lib_'+id+'-'+i).val() != null && $('#lib_'+id+'-'+i).val() != '' && $('#montant_'+id+'-'+i).val() != null && $('#montant_'+id+'-'+i).val() != '' && $('#quantite_'+id+'-'+i).val() != null && $('#quantite_'+id+'-'+i).val() != '') {
				value += $('#lib_'+id+'-'+i).val()+':'+$('#montant_'+id+'-'+i).val()+':'+$('#quantite_'+id+'-'+i).val()+';';
			}
		}
		if ($('#parc_arrondi_'+id).val() != null && $('#parc_arrondi_'+id).val() != '') {
			value += '*$*parc_arrondi_prix*$*:'+$('#parc_arrondi_'+id).val()+':*$*parc_arrondi_prix*$*;';
		} else {
			value += '*$*parc_arrondi_prix*$*:NULL:*$*parc_arrondi_prix*$*;';
		}
		
	} else if (id.substring(0,10) == 'lot_bloque') {
		if ($('#'+id).is(':checked')) {
			value = 't';
		} else {
			value = 'f';
		}
	} else {
		value = $('#'+id).val()
	}
	
	$.ajax({ //On entre maintenant dans la partie ajax		
		url       : 'af_fct_ajax.php', //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_updateLot', params: {var_id: id, var_value: value}//variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
			if ($('#img_save_'+id).length) {
				changeOpac(30, 'img_save_'+id);
				$('#hid_'+id).val($('#'+id).val())
				$('#img_save_'+id).attr('onclick', '');
				$('#img_save_'+id).css('cursor', 'auto');
			}
			var lotEnCours = 'NON'
			$('#parc_tbl_lots_'+entite_af_id).find('select').each(function(){ 
				if ($(this).hasClass('paf') && $(this).val() > 0) {
					lotEnCours = 'OUI'
				}
			}); 
			if (lotEnCours == 'NON' && nbr_echanges == 0) {
				$('#confirmDeleteEAF').css('display', '');
				$('#msg_confirmDeleteEAF').html("Il n'y a aucun lot prioritaire sur cette EAF et aucun &eacute;change n'a &eacute;t&eacute; saisi. <br> Souhaitez-vous supprimer cette EAF et tous les lots associ&eacute;s pour cette session d'animation fonci&egrave;re?")
				$('#btn_confirmDeleteEAF_oui').attr('onclick', "deleteEAF(" + session_af_id + ", '" + entite_af_id + "');");	
				$('#confirmDeleteEAF').height($('#onglet').height());
				changeOpac(20, 'onglet'); 
				id_tbl = 'parc_tbl_lots_' + entite_af_id
				id_tbl_top = $('#'+id_tbl).offset().top-200;
				$('#confirmDeleteEAF_sousdiv').css('top', id_tbl_top +'px');
			}
			if (id.substring(0,6) == 'occsol') {
				$('#tbl_parc_'+id).html(data['tbl_parc'])
				$('#lbl_prix_total_'+id).html("<u>Prix estim&eacute; du fond &agrave; partir de l'occupation du sol :</u> "+data['prix_total_occsol']+" &euro;")
				$('#lbl_prix_total_occsol-'+id.replace('occsol-', '')).css('display', '')
				$('#tbl_parc_'+id).find('input').each(function(){
					if ($(this).val() == '') {
						$('#lbl_prix_total_occsol-'+id.replace('occsol-', '')).css('display', 'none')
						return false;
					}
				});
				allPourcentage = 'GOOD';
				$('.pourcentage_occsol').each(function(){
					if ($(this).val() == '') {
						allPourcentage = 'BAD';
						return false;
					}
				});
				allMontant = "BAD";
				$('.prix_occsol').each(function(){
					if ($(this).val() == '') {
						allMontant = 'GOOD';
						return false;
					}
				});
				if (allPourcentage == 'GOOD' && allMontant == 'GOOD') {
					$('#btn_prix_occsol').css('display', '')
				} else {
					$('#btn_prix_occsol').css('display', 'none')
				}
			} else if (id.substring(0,4) == 'prix') {
				$('#tbl_parc_'+id).html(data['tbl_parc'])
				$('#td_prix_eaf-'+entite_af_id).html(data['td_prix_eaf']);
			} else if (id.substring(0,10) == 'lot_bloque') {
				if (value == 't') {
					$('#parc_td_detail-'+id.replace('lot_bloque-', '')).addClass("lot_bloque");
					$('#lbl_'+id).css('color', '#fe0000');
				} else {
					$('#parc_td_detail-'+id.replace('lot_bloque-', '')).removeClass("lot_bloque");
					$('#lbl_'+id).css('color', '#004494');
				}
			} else if (id.substring(0,3) == 'obj') {
				if (value == '1') {
					$('#fieldset_parc_prix-'+id.replace('obj-', '')).css('display', '');
				} else {
					$('#fieldset_parc_prix-'+id.replace('obj-', '')).css('display', 'none');
				}
				$('#td_prix_eaf-'+entite_af_id).html(data['td_prix_eaf']);
			}
		}
	});
}

/* Cette fonction permet d'afficher une pop-up d'informations relatives à une EAF depuis l'onglet "Synthèse". */
function showInfosEAF(scrollTo_id, session_af_id, entite_af_id) {	
	$('#explorateur_infos_eaf').height($('#onglet').height());		

	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showInfosEAF', params: {var_session_af_id: session_af_id, var_entite_af_id: entite_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
			$('#explorateur_infos_eaf').css('display', '' );
			if (parseInt($('#tr_synthese_'+entite_af_id).offset().top + $('#tr_synthese_'+entite_af_id).height() + $('#explorateur_infos_eaf_sousdiv').height()) - parseInt($('#onglet').height()) < 0) {
				$('#explorateur_infos_eaf_sousdiv').css('top', $('#tr_synthese_'+entite_af_id).offset().top+$('#tr_synthese_'+entite_af_id).height()-225 +'px');
				$('#explorateur_infos_eaf_sousdiv').removeClass("explorateur_fond_after").addClass("explorateur_fond_before");
			} else {
				$('#explorateur_infos_eaf_sousdiv').css('top', $('#tr_synthese_'+entite_af_id).offset().top-$('#explorateur_infos_eaf_sousdiv').height()-225 +'px');
				$('#explorateur_infos_eaf_sousdiv').removeClass("explorateur_fond_before").addClass("explorateur_fond_after");
			}
			$('html,body').animate({scrollTop: ($('#explorateur_infos_eaf_sousdiv').css('top'))}, 'slow');
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#lbl_title_infos_eaf').html(data['lbl_title_infos_eaf'])
			$('#tbody_infos_eaf_contenu').html(data['code'])
			if ($(scrollTo_id).offset().top - 30 + ($('#explorateur_infos_eaf_sousdiv').height()/2) > $('#onglet').height()) {
				id_btn_top = $('#onglet').height() - ($('#explorateur_infos_eaf_sousdiv').height());
			} else {
				id_btn_top = $(scrollTo_id).offset().top - 30;
			}
			changeOpac(20, 'onglet'); 			
		}
	});	
}

/* Cette fonction permet d'afficher une pop-up d'informations relatives à un interlocuteur depuis l'onglet "Synthèse". */
function showInfosProprio(scrollTo_id, session_af_id, entite_af_id, interloc_id, interloc_lib) {
	$('#explorateur_infos_eaf').css('display', '' );
	$('#explorateur_infos_eaf').height($('#onglet').height());	
	
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showInfosProprio', params: {var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_interloc_id: interloc_id, var_interloc_lib: interloc_lib} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#lbl_title_infos_eaf').html(data['lbl_title_infos_eaf'])
			$('#tbody_infos_eaf_contenu').html(data['code'])
			if ($(scrollTo_id).offset().top - 30 + ($('#explorateur_infos_eaf_sousdiv').height()/2) > $('#onglet').height()) {
				id_btn_top = $('#onglet').height() - ($('#explorateur_infos_eaf_sousdiv').height());
			} else {
				id_btn_top = $(scrollTo_id).offset().top - $('#explorateur_infos_eaf_sousdiv').height() + 30;
			}
			changeOpac(20, 'onglet'); 
			$('#explorateur_infos_eaf_sousdiv').css('top', id_btn_top +'px');
			$('html,body').animate({scrollTop: ($(scrollTo_id).offset().top-160)}, 'slow');
		}
	});
}

/* Cette fontion permet de spécifier qu'un rappel a été bien traité. Il n'y a alors plus d'alerte sur ce rappel. */
function validRappelEchange(tbl_rel_saf_foncier_interlocs_id, echange_af_id) {
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		data      :{ 
			fonction:'fct_validRappelEchange', params: {var_tbl_rel_saf_foncier_interlocs_id: tbl_rel_saf_foncier_interlocs_id, var_echange_af_id: echange_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#suite_'+echange_af_id).css('display', 'none' );
		}
	});	
}

/* Cette fonction permet d'afficher le formulaire de création d'un interlocuteur et d'adapter l'affichage en fonction de l'EAF. */
function showFormAddInterloc(entite_af_id, session_af_id, tbl_eaf_lot_id, tbl_rel_saf_foncier_id) {
	document.getElementById('explorateur_interloc').style.display='';
	var elements = document.forms['frm_new_interloc'].elements;
	for (i=0; i<elements.length; i++){
		if (elements[i].type == 'select-one') {
			if ($('#'+elements[i].id) == 'new_type_interloc' || $('#'+elements[i].id) == 'new_interloc_ccodem' || $('#'+elements[i].id) == 'new_interloc_ccodro') {
				$('#'+elements[i].id).css({'border-color': '#fff'});
			} else if ($('#'+elements[i].id) == 'lst_import_proprio' || $('#'+elements[i].id) == 'new_interloc_lst_particule') {
				$('#'+elements[i].id).css({'border-color': '#fff'});
			}
			$('#'+elements[i].id+'  option:eq(0)').prop('selected', true);
		} else if (elements[i].type == 'text') {
			$('#'+elements[i].id).css({'border-color': '#fff'});
			$('#'+elements[i].id).val('')			
		}
	}
	
	id_btn = 'tr_' + entite_af_id
	if ($('#'+id_btn).offset().top - 220 + 770 > $('#onglet').height()) {
		id_btn_top = $('#onglet').height() - 770;
	} else {
		id_btn_top = $('#'+id_btn).offset().top - 220;
	}

	id_btn_top = $('#'+id_btn).offset().top - 220;
	flt_lst_proprio_import(id_btn_top+200);
	
	disableNewInterloc('ALL');
	
	$('#new_interloc_prob_saisie').css('display', 'none')
	$('#need_session_af_id').val(session_af_id);
	$('#need_entite_af_id').val(entite_af_id);
	$('#hid_parc_interloc').val(tbl_rel_saf_foncier_id);
	$('#explorateur_new_interloc').css('top', id_btn_top +'px');	
	$('#img_flt_lst_proprio_import').attr('onclick', "flt_lst_proprio_import('null');");	
	changeOpac(20, 'onglet'); 
	
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showFormAddInterloc', params: {var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_tbl_rel_saf_foncier_id: tbl_rel_saf_foncier_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data			
			$('#tbody_check_parc_interloc').html(data['code'])
		}
	});
	
	if ($('#onglet').height() < ($('#explorateur_new_interloc').height()+id_btn_top)) {
		$('#onglet').height($('#explorateur_new_interloc').height()+id_btn_top)
	}
}

/* Cette fonction permet d'initialiser la suppression d'un échange. Une demande de confirmation intervient avant la suppression définitive. A noter qu'en plus d'annuler la suppression, on peut, si c'est utile (échange avec un référent), supprimer, soit seulement l'échange passer avec le référent, soit en plus tous les échange associés à celui passé avec le référent*/
function deleteEchange(btn_id, interloc_id, session_af_id, entite_af_id, echange_af_id, echange_direct) {
	$('#confirmDeleteEchange').css('display', '');
	$('#btn_confirmDeleteEchange_one').attr('onclick', "deleteEchangeConfirm('one', '"+interloc_id+"', "+session_af_id+", '"+entite_af_id+"', '"+echange_af_id+"', '"+echange_direct+"');");	
	$('#btn_confirmDeleteEchange_all').attr('onclick', "deleteEchangeConfirm('all', '"+interloc_id+"', "+session_af_id+", '"+entite_af_id+"', '"+echange_af_id+"', '"+echange_direct+"');");	
	$('#confirmDeleteEchange').height($('#onglet').height());
	changeOpac(20, 'onglet'); 
	if ($(btn_id).offset().top - 220 + 70 > $('#onglet').height()) {
		id_img_top = $('#onglet').height() - 70;
	} else {
		id_img_top = $(btn_id).offset().top - 220;
	}
	$('#confirmDeleteEchange_sousdiv').css('top', id_img_top +'px');
	$('#btn_confirmDeleteEchange_all').css('display', 'none');
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_deleteEchangeUpdateLabel', params: {var_interloc_id: interloc_id, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_echange_af_id: echange_af_id, var_echange_direct: echange_direct} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			lbl = '';
			if (data['nbr_interloc_current_eaf'] == 1) {
				lbl += "Cet &eacute;change ne concerne que cet interlocuteur dans cette EAF."
			} else {
				lbl += "Cet &eacute;change concerne " + data['nbr_interloc_current_eaf'] + " interlocuteurs dans cette EAF."
				if (echange_direct == 1) {
					$('#btn_confirmDeleteEchange_all').css('display', '');
				}
			}
			if (data['nbr_interloc_other_eaf'] == 1) {
				lbl += "<br>Cet &eacute;change concerne un autre interlocuteur dans " + data['nbr_eaf'] + " autre(s) EAF.";
				if (echange_direct == 1) {
					$('#btn_confirmDeleteEchange_all').css('display', '');
				}
			} else if (data['nbr_interloc_other_eaf'] > 1){
				lbl += "<br>Cet &eacute;change concerne " + data['nbr_interloc_other_eaf'] + " autres interlocuteurs dans " + data['nbr_eaf'] + " autre(s) EAF.";
				if (echange_direct == 1) {
					$('#btn_confirmDeleteEchange_all').css('display', '');
				}
			}
		
			$('#lbl_resume_delete_echange').html(lbl);
		}
	});
}

/* Cette fonction, liée à la précédente, supprime un ou plusieurs échange. */
function deleteEchangeConfirm(mode, interloc_id, session_af_id, entite_af_id, echange_af_id, echange_direct) {
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_deleteEchangeConfirm', params: {var_mode: mode, var_interloc_id: interloc_id, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_echange_af_id: echange_af_id, var_echange_direct: echange_direct} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			for(var i=0; i<=data['tbl_echange_id'].length; i++) {
				if($('#tr_echange_info_'+data['tbl_echange_id'][i]).length > 0) {
					$('#tr_echange_info_'+data['tbl_echange_id'][i]).css('display', 'none');
				}
				if($('#tr_echange_rappel_'+data['tbl_echange_id'][i]).length > 0) {
					$('#tr_echange_rappel_'+data['tbl_echange_id'][i]).css('display', 'none');
				}
				if($('#tr_echange_nego_'+data['tbl_echange_id'][i]).length > 0) {
					$('#tr_echange_nego_'+data['tbl_echange_id'][i]).css('display', 'none');
				}
			}
			$('#confirmDeleteEchange').css('display', 'none' );
			changeOpac(100, 'onglet');
		}
	});
}

/* Cette fonction permet d'ouvrir le formulaire de création d'un nouvel événement ou bien d'éditer un événement existant. */
function showFormEvenement(scrollTo_id, session_af_id, evenement_af_id) {
	$('#explorateur_evenement').css('display', '' );
	$('#explorateur_evenement').height($('#onglet').height());	
	var elements = document.forms['frm_evenement'].elements;
	for (i=0; i<elements.length; i++){		
		if (elements[i].type == 'select-one') {
			$('#'+elements[i].id+'  option:eq(0)').prop('selected', true);
			$('#'+elements[i].id).css({'border-color': '#fff'});
		} else if (elements[i].type == 'text') {	
			$('#'+elements[i].id).val('')
			$('#'+elements[i].id).css({'border-color': '#fff'});
		} else if (elements[i].type == 'textarea') {	
			$('#'+elements[i].id).val('')
			$('#'+elements[i].id).css({'border-color': '#fff'});
			resizeTextarea(elements[i].id)
		} else if (elements[i].type == 'checkbox') {	
			$('#'+elements[i].id).prop("checked", false)
		}
	}
	
	if ($(scrollTo_id).offset().top - 30 + ($('#explorateur_evenement_sousdiv').height()/2) > $('#onglet').height()) {
		id_btn_top = $('#onglet').height() - ($('#explorateur_evenement_sousdiv').height());
	} else {
		id_btn_top = $(scrollTo_id).offset().top - ($('#explorateur_evenement_sousdiv').height()/2) - 30;
	}
	changeOpac(20, 'onglet'); 
	
	$('#explorateur_evenement').height($('#onglet').height());
	if (evenement_af_id != '-1') {
		$('#evenement_lib').html('Mise &agrave; jour d\'un &eacute;v&eacute;nement');
	} else {
		$('#evenement_lib').html('Saisie d\'un nouvel &eacute;v&eacute;nement');
	}	
	$('#btn_valide_evenement').attr('onclick', "addupdateEvenement("+session_af_id+", '"+evenement_af_id+"');");
	
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showFormEvenement', params: {var_evenement_af_id: evenement_af_id, var_session_af_id: session_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			if (data['date_evenement'] != 'N.D.') {
				$('#date_evenement').val(data['date_evenement'])
				$('#lst_type_evenement option').each(function(){ 
					if ($(this).val() == data['lst_type_evenement']) {
						$(this).prop('selected', true);
					}
				});
				$('#description_evenement').val(data['description_evenement'])
			}
			
			$('#tbody_tbl_evenement_presents').html(data['tbody_tbl_evenement_presents'])
			$('#main').attr('style', "height:" + ($('#explorateur_evenement_sousdiv').height() + $('#explorateur_evenement_sousdiv').offset().top) + "px !important;");
		}
	});
}

/* Cette fonction permet d'ajouter ou de mettre à jour un événement. Elle se lance à l'enregistrement d'une fiche événement.*/
function addupdateEvenement(session_af_id, evenement_af_id) {
	verif = 'OUI';
	if ($('#date_evenement') && $('#date_evenement').val() != null && $('#date_evenement').val() != '') {
		date_evenement = $('#date_evenement').val();
	} else {
		verif = 'NON';
		$('#date_evenement').css('border-color', '#fe0000');
	}
	if ($('#lst_type_evenement') && $('#lst_type_evenement option:selected').val() != null && $('#lst_type_evenement option:selected').val() != '-1') {
		type_evenement = $('#lst_type_evenement').val();
	} else {
		verif = 'NON';
		$('#lst_type_evenement').css('border-color', '#fe0000');
	}
	if($('#tbody_tbl_evenement_presents').find('input[type=checkbox]:checked').length > 0) {
		tbl_evenement_presents = [];
		$('#tbody_tbl_evenement_presents').find('input[type=checkbox]:checked').each(function(){
			tbl_evenement_presents.push(this.id.replace('event_present_', ''));
		});
	} else {
		verif = 'NON';
	}
	tbl_commentaire_interloc_id = [];
	tbl_commentaire_interloc_txt = [];
	$('#tbody_tbl_evenement_presents').find('textarea').each(function(){
		tbl_commentaire_interloc_id.push(this.id.replace('txt_evt_com_', ''));
		tbl_commentaire_interloc_txt.push($('#'+this.id).val());
	});
	if (verif == 'OUI') {	
		$.ajax({ 
			url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json",
			data      :{ 
				fonction:'fct_addupdateEvenement', params: {var_session_af_id: session_af_id, var_date_evenement: date_evenement, var_type_evenement: type_evenement, var_description: $('#description_evenement').val(), var_tbl_evenement_presents: tbl_evenement_presents, var_tbl_commentaire_interloc_id: tbl_commentaire_interloc_id, var_tbl_commentaire_interloc_txt: tbl_commentaire_interloc_txt, var_evenement_af_id: evenement_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#explorateur_evenement').css('display', 'none' );
				changeOpac(100, 'onglet');
				$('#td_lst_evenements').html(data['td_lst_evenements']);
				$('html,body').animate({scrollTop: (0)}, 'fast');
			}
		});
	}
}

/* Cette fonction permet d'ouvrir le formulaire de création d'un nouvel échange ou bien d'éditer un échange existant. */
function showFormEchange(scrollTo_id, interloc_lib, ddenom, referent, interloc_id, show_nego, session_af_id, entite_af_id, echange_af_id, representant, cur_table, gtoper) {
	$('#explorateur_echange').css('display', '' );
	$('#explorateur_echange').height($('#onglet').height());
	$('#echange_prob_saisie').css('display', 'none');
	var elements = document.forms['frm_echange'].elements;
	for (i=0; i<elements.length; i++){		
		if (elements[i].type == 'select-one') {
			$('#'+elements[i].id+'  option:eq(0)').prop('selected', true);
			$('#'+elements[i].id).css({'border-color': '#fff'});
		} else if (elements[i].type == 'text') {	
			$('#'+elements[i].id).val('')
			$('#'+elements[i].id).css({'border-color': '#fff'});
		} else if (elements[i].type == 'textarea') {	
			$('#'+elements[i].id).val('')
			$('#'+elements[i].id).css({'border-color': '#fff'});
			resizeTextarea(elements[i].id)
		} else if (elements[i].type == 'checkbox') {	
			$('#'+elements[i].id).prop("checked", false)
		}
	}
	if (($('#explorateur_echange_sousdiv').height() + $('#explorateur_echange_sousdiv').offset().top - $('#main').offset().top) > $('#main').height()) {
		$('#main').attr('style', "height:" + ($('#explorateur_echange_sousdiv').height() + $('#explorateur_echange_sousdiv').offset().top - $('#main').offset().top) + "px !important;");
	}
	if ($(scrollTo_id).offset().top - 30 + ($('#explorateur_echange_sousdiv').height()/2) > $('#onglet').height() && $('#onglet').height() > $('#explorateur_echange_sousdiv').height()) {
		id_btn_top = $('#onglet').height() - ($('#explorateur_echange_sousdiv').height());
	} else {
		id_btn_top = $(scrollTo_id).offset().top - ($('#explorateur_echange_sousdiv').height()/2) - 30;
	}
		
	changeOpac(20, 'onglet'); 
	
	$('#tbody_tbl_maitrise_parc').html('')
	$('#tr_chk_echange_referent').css('display', '' );
	$('#explorateur_echange').height($('#onglet').height());
	$('#echange_interloc_lib').html('Saisie d\'un &eacute;change avec ' + interloc_lib);
	$('#explorateur_echange_sousdiv').css('top', id_btn_top +'px');
	$('#tbody_modif_infos_interloc').css('display', 'none');
	$('#tbody_maitrise_parc').css('display', 'none');
	$('#hid_echange_ddenom').val(ddenom);	
	$('#img_reduce_explorateur_echange').attr('onclick', "reduceForm('explorateur_echange', "+id_btn_top+", "+interloc_id+", "+session_af_id+", "+entite_af_id+");");
	$('#chk_modif_infos_interloc').attr('onclick', "showTblModifInfosInterloc("+interloc_id+", "+session_af_id+", "+entite_af_id+", "+gtoper+");");
	$('#btn_valide_echange').attr('onclick', "addupdateEchange('"+interloc_lib+"', '"+referent+"', '"+interloc_id+"', "+session_af_id+", "+entite_af_id+", '"+echange_af_id+"', '"+representant+"', '"+cur_table+"', '"+gtoper+"');");
	$('#chk_attente_reponse').prop('readonly', false);
	$('#chk_attente_reponse').prop('disabled', false);
	$('#lbl_attente_reponse').css('color', '#004494');
	$('#chk_interloc_injoignable').prop('readonly', false);
	$('#chk_interloc_injoignable').prop('disabled', false);
	$('#lbl_interloc_injoignable').css('color', '#004494');
	$('#chk_modif_infos_interloc').prop('readonly', false);
	$('#chk_modif_infos_interloc').prop('disabled', false);
	$('#lbl_modif_infos_interloc').css('color', '#004494');
	$('#chk_maitrise_parc').prop('readonly', false);
	$('#chk_maitrise_parc').prop('disabled', false);
	$('#lbl_maitrise_parc').css('color', '#004494');
	$('html,body').animate({scrollTop: ($(scrollTo_id).offset().top-160)}, 'slow');
	
	if (show_nego == 'true') {
		$('#chk_maitrise_parc').css('display', '');
		$('#lbl_maitrise_parc').css('display', '');
	} else {
		$('#chk_maitrise_parc').prop('checked', false);
		$('#chk_maitrise_parc').css('display', 'none');
		$('#lbl_maitrise_parc').css('display', 'none');
	}
	
	if (echange_af_id != 'N.D.') {
		$.ajax({ 
			url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json",
			data      :{ 
				fonction:'fct_showFormUpdateEchange', params: {var_echange_af_id: echange_af_id, var_interloc_id: interloc_id, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_representant: representant} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#tbody_maitrise_parc').css('display', '' );
				
				var elements = document.forms['frm_echange'].elements;				
				for (i=0; i<elements.length; i++){	
					if (document.getElementById(elements[i].id) != null) {
						if (elements[i].type == 'select-one') {
							$('#'+elements[i].id+'  option:eq(0)').prop('selected', true);
							$('#'+elements[i].id+'  option').each(function(){ 
								if ($(this).val() == data[elements[i].id]) {
									$(this).prop('selected', true);
								}
							});
							$('#'+elements[i].id).css({'border-color': '#fff'});
						} else if (elements[i].type == 'text' && elements[i].id.substring(0,17) != 'prix_result_ech_') {	
							$('#'+elements[i].id).val(data[elements[i].id])
							$('#'+elements[i].id).css({'border-color': '#fff'});
						} else if (elements[i].type == 'textarea') {	
							$('#'+elements[i].id).val(data[elements[i].id])
							$('#'+elements[i].id).css({'border-color': '#fff'});
							resizeTextarea(elements[i].id)
						} else if (elements[i].type == 'checkbox' && elements[i].id.substring(0,15) != 'rad_result_ech_') {							
							if($('#tr_'+elements[i]).length > 0) {
								$('#tr_'+elements[i].id).css("display", data[elements[i].id]['display'])
							}
							$('#'+elements[i].id).prop("checked", data[elements[i].id]['checked'])
							$('#'+elements[i].id).prop("readonly", data[elements[i].id]['readonly'])
							$('#'+elements[i].id).prop("disabled", data[elements[i].id]['disabled'])
							$('#'+elements[i].id.replace('chk', 'lbl')).css("color", data[elements[i].id]['color'])
						}
					}
				}
				if (data['tbody_tbl_maitrise_parc'] != '') {
					$('#tbody_tbl_maitrise_parc').html(data['tbody_tbl_maitrise_parc'])
				}
				if (data['prix_eaf_negocie'] != '') {
					$('#ech_prix_eaf_nego').val(data['prix_eaf_negocie']);
				}
				if (data['chk_maitrise_parc']['checked'] == 'checked') {
					$('#tbody_maitrise_parc').css('display', '' );
				} else {
					$('#tbody_maitrise_parc').css('display', 'none' );		
				}
			}
		});
	} else {
		$('#td_echange_rappel_traite').css('display', 'none' );
		$('#echange_chk_rappel_traite').prop('checked', false );
		$.ajax({ 
			url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json",
			data      :{ 
				fonction:'fct_createTblMaitriseParc', params: {var_interloc_id: interloc_id, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_chk_echange_referent: $('#chk_echange_referent').is(':checked'), var_representant: representant} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#tbody_tbl_maitrise_parc').html(data['code']);
				$('#ech_prix_eaf_nego').val(data['prix_eaf_negocie']);
			}
		});
	}
	if (referent == 'non') {
		$('#tr_chk_echange_referent').css('display', 'none' );
	} else {
		$('#tr_chk_echange_referent').css('display', '' );
	}
}

/* Cette fonction permet d'ajouter ou de mettre à jour un echange. Elle se lance à l'enregistrement d'une fiche échange.*/
function addupdateEchange(interloc_lib, referent, interloc_id, session_af_id, entite_af_id, echange_af_id, representant, cur_table, gtoper) {
	verif = 'OUI';
	if ($('#date_echange') && $('#date_echange').val() != null && $('#date_echange').val() != '') {
		date_echange = $('#date_echange').val();
		$('#date_echange').css('border-color', '#fff');
	} else {
		verif = 'NON';
		$('#date_echange').css('border-color', '#fe0000');
		msg = "Veuillez v&eacute;rifier votre saisie";
	}
	if ($('#lst_type_echange') && $('#lst_type_echange option:selected').val() != null && $('#lst_type_echange option:selected').val() != '-1') {
		type_echange = $('#lst_type_echange').val();
		$('#lst_type_echange').css('border-color', '#fff');
	} else {
		verif = 'NON';
		$('#lst_type_echange').css('border-color', '#fe0000');
		msg = "Veuillez v&eacute;rifier votre saisie";
	}
	if ($('#description_echange') && $('#description_echange').val() != null && $('#description_echange').val() != '') {
		description = $('#description_echange').val();
	} else {
		description = "N.D.";
	}
	if ($('#chk_echange_referent') && $('#chk_echange_referent').is(':checked')) {
		echange_referent = "t";
	} else {
		echange_referent = "f";
	}
	if ($('#date_echange_recontact') && $('#date_echange_recontact').val() != null && $('#date_echange_recontact').val() != '') {
		date_echange_recontact = $('#date_echange_recontact').val();
	} else {
		date_echange_recontact = "N.D.";
	}
	if ($('#motif_echange_recontact') && $('#motif_echange_recontact').val() != null && $('#motif_echange_recontact').val() != '') {
		motif_echange_recontact = $('#motif_echange_recontact').val();
	} else {
		motif_echange_recontact = "N.D.";
	}
	if ($('#echange_chk_rappel_traite') && $('#echange_chk_rappel_traite').is(':checked')) {
		rappel_traite = "t";
	} else {
		rappel_traite = "f";
	}
	
	result_echange_af_id = 'NULL';
	tbl_modif_infos_interloc = 'NULL';
	if($('#chk_attente_reponse').is(':checked')) {
		result_echange_af_id = '2';
	} else if($('#chk_interloc_injoignable').is(':checked')) {
		result_echange_af_id = '1';
	} else if($('#chk_modif_infos_interloc').is(':checked')) {
		if (gtoper == '1') {
			if ($('#ech_update_interloc_lst_particule') && $('#ech_update_interloc_lst_particule option:selected').val() != null && $('#ech_update_interloc_lst_particule option:selected').val() != '-1') {
				ech_update_interloc_lst_particule = $('#ech_update_interloc_lst_particule').val();
				$('#ech_update_interloc_lst_particule').css('border-color', '#fff');
			} else {
				verif = 'NON';
				$('#ech_update_interloc_lst_particule').css('border-color', '#fe0000');
				msg = "Veuillez v&eacute;rifier votre saisie";
			}
			if ($('#ech_update_interloc_prenom_usage') && $('#ech_update_interloc_prenom_usage').val() != null && $('#ech_update_interloc_prenom_usage').val() != '') {
				ech_update_interloc_prenom_usage = $('#ech_update_interloc_prenom_usage').val();
				$('#ech_update_interloc_prenom_usage').css('border-color', '#fff');
			} else {
				verif = 'NON';
				$('#ech_update_interloc_prenom_usage').css('border-color', '#fe0000');
				msg = "Veuillez v&eacute;rifier votre saisie";
			}
			if ($('#ech_update_interloc_nom_usage') && $('#ech_update_interloc_nom_usage').val() != null && $('#ech_update_interloc_nom_usage').val() != '') {
				ech_update_interloc_nom_usage = $('#ech_update_interloc_nom_usage').val();
				$('#ech_update_interloc_nom_usage').css('border-color', '#fff');
			} else {
				verif = 'NON';
				$('#ech_update_interloc_nom_usage').css('border-color', '#fff');
				msg = "Veuillez v&eacute;rifier votre saisie";
			}
			ech_update_interloc_date_naissance = $('#ech_update_interloc_date_naissance').val();
			ech_update_interloc_lieu_naissance = $('#ech_update_interloc_lieu_naissance').val();
			ech_update_interloc_nom_conjoint = $('#ech_update_interloc_nom_conjoint').val();
			ech_update_interloc_prenom_conjoint = $('#ech_update_interloc_prenom_conjoint').val();
			ech_update_interloc_nom_jeunefille = $('#ech_update_interloc_nom_jeunefille').val();
			ech_update_interloc_ddenom = null;
		} else {
			if ($('#ech_update_interloc_ddenom') && $('#ech_update_interloc_ddenom').val() != null && $('#ech_update_interloc_ddenom').val() != '') {
				ech_update_interloc_ddenom = $('#ech_update_interloc_ddenom').val();
				$('#ech_update_interloc_ddenom').css('border-color', '#fff');
			} else {
				verif = 'NON';
				$('#ech_update_interloc_ddenom').css('border-color', '#fe0000');
				msg = "Veuillez v&eacute;rifier votre saisie";
			}
			ech_update_interloc_lst_particule = null;
			ech_update_interloc_nom_usage = null;
			ech_update_interloc_prenom_usage = null;
			ech_update_interloc_date_naissance = null;
			ech_update_interloc_lieu_naissance = null;
			ech_update_interloc_nom_conjoint = null;
			ech_update_interloc_prenom_conjoint = null;
			ech_update_interloc_nom_jeunefille = null;
		}
		result_echange_af_id = '3';
		tbl_modif_infos_interloc='ddenom:'+ech_update_interloc_ddenom+'particule_id:'+ech_update_interloc_lst_particule+';nom:'+ech_update_interloc_nom_usage+';prenom:'+ech_update_interloc_prenom_usage+';date_naissance:'+ech_update_interloc_date_naissance+';lieu_naissance:'+ech_update_interloc_lieu_naissance+';nom_conjoint:'+ech_update_interloc_nom_conjoint+';prenom_conjoint:'+ech_update_interloc_prenom_conjoint+';nom_jeune_fille:'+ech_update_interloc_nom_jeunefille+';adresse1:'+$('#ech_update_interloc_adresse1').val()+';adresse2:'+$('#ech_update_interloc_adresse2').val()+';adresse3:'+$('#ech_update_interloc_adresse3').val()+';adresse4:'+$('#ech_update_interloc_adresse4').val()+';tel_fixe1:'+$('#ech_update_interloc_tel_fix1').val()+';tel_fixe2:'+$('#ech_update_interloc_tel_fix2').val()+';tel_portable1:'+$('#ech_update_interloc_tel_portable1').val()+';tel_portable2:'+$('#ech_update_interloc_tel_portable2').val()+';fax:'+$('#ech_update_interloc_tel_fax').val()+';mail:'+$('#ech_update_interloc_mail').val()+';'
	}
	
	if($('#chk_maitrise_parc').is(':checked') && $('#tbody_tbl_maitrise_parc').find('input[type=checkbox]:checked').length > 0) {
		maitrise_parc = 'oui';
		tbl_maitrise_parc = [];
		tbl_prix_nego_parc = '';
		$('#tbody_tbl_maitrise_parc').find('input[type=checkbox]:checked').each(function(){
			tbl_maitrise_parc.push($(this).val());
			if($('#'+this.id.replace('rad', 'prix')).length) {
				tbl_prix_nego_parc += this.id.replace('rad_result_ech_', '')+':'+$('#'+this.id.replace('rad', 'prix')).val()+';';
			}  else {
				tbl_prix_nego_parc += this.id.replace('rad_result_ech_', '')+':N.P.;';
			} 
		});
	} else {
		maitrise_parc = 'non';
		tbl_maitrise_parc = 'NULL';
		tbl_prix_nego_parc = 'NULL';
	}
	tbl_parc_echange = [];
	$('#tbody_tbl_maitrise_parc' + ' > tr').each(function(){
		if (this.id.length > 0) {
			tbl_parc_echange.push(this.id.replace('tr_', ''));
		}
	});
	
	if (verif == 'OUI') {	
		$.ajax({ 
			url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json",
			data      :{ 
				fonction:'fct_addupdateEchange', params: {var_interloc_lib: interloc_lib, var_referent: referent, var_interloc_id: interloc_id, var_gtoper: gtoper, var_session_af_id: session_af_id, var_entite_af_id: entite_af_id, var_date_echange: date_echange, var_type_echange: type_echange, var_description: description, var_echange_referent: echange_referent, var_date_echange_recontact: date_echange_recontact, var_motif_echange_recontact: motif_echange_recontact, var_rappel_traite: rappel_traite, var_result_echange_af_id: result_echange_af_id, var_tbl_modif_infos_interloc: tbl_modif_infos_interloc, var_maitrise_parc: maitrise_parc, var_tbl_maitrise_parc: tbl_maitrise_parc, var_tbl_prix_nego_parc: tbl_prix_nego_parc,var_tbl_parc_echange: tbl_parc_echange, var_echange_af_id: echange_af_id, var_representant: representant, var_prix_eaf_negocie: $('#ech_prix_eaf_nego').val()} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#hid_echange_ddenom').val(null);
				$('#explorateur_echange').css('display', 'none' );
				changeOpac(100, 'onglet');
				for(var i=0; i<=data['entite_af_id'].length; i++) {
					$('#'+data['entite_af_id'][i]).html(data['entite_af_code'][i]);
					$('#'+data['entite_af_id'][i]).find('textarea').each(function(){
						resizeTextarea(this.id);
					});
				}
				$('html,body').animate({scrollTop: ($('#'+cur_table).offset().top-150)}, 'fast');
				$('#tbody_modif_infos_interloc').css('display', 'none' );
				$('#tbody_maitrise_parc').css('display', 'none' );
			}
		});
	} else {
		$('#echange_prob_saisie').html(msg);
		$('#echange_prob_saisie').css('display', '');
	}
}

/* Cette fonction permet d'ouvrir et d'adapter le contenu du formulaire de saisie simplifié de l'occupation du sol à partir du bouton "Saisir des prix au m² par occupation du sol" dans l'onglet "Parcelles/ Lots". */
function showFormPrixOccsol(session_af_id) {
	$('#explorateur_prix_occsol').css('display', '' );
	$('#explorateur_prix_occsol').height($('#onglet').height());
	$('#explorateur_prix_occsol_sousdiv').css('top', '50px');
		
	changeOpac(20, 'onglet'); 
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showFormPrixOccsol', params: {var_session_af_id: session_af_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#tbl_prix_occsol').html(data['code'])
		}
	});
}

/* Cette fonction permet d'enregistrer la saisie simplifiée des prix au m² par occupation du sol. A noter que seules les informations manquantes sont enregistrées, celles existantes ne sont pas remplacées. */
function prixOccsol(session_af_id) {
	interloc_id = $('#interloc_regroup option:selected').val();
	tbl_prix_occsol = [];
	
	$('#tbl_prix_occsol').find('input').each(function(){
		if ($(this).val() != null && $(this).val() != '') {
			tbl_prix_occsol.push(this.id.replace('prix_occsol-', '')+":"+$(this).val());
		}
	});
	
	if ($('#tbl_prix_occsol').find('input').length == tbl_prix_occsol.length) {
		$('#lbl_prob_prix_occsol').css('display', 'none')
		$.ajax({ 
			url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			data      :{ 
				fonction:'fct_prixOccsol', params: {var_session_af_id : session_af_id, var_tbl_prix_occsol: tbl_prix_occsol} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#explorateur_prix_occsol').css('display', 'none' );
				changeOpac(100, 'onglet');
				create_lst_parc(0, session_af_id, 0);
			}
		});
	} else {
		$('#lbl_prob_prix_occsol').css('display', '')
	}
}

/* Cette fonction permet d'afficher le formulaire dédié au regroupement des dnuper correspondant au même propriétaire. */
function showFormRegroupInterloc(mode, session_af_id) {
	$('#explorateur_regroupe_interloc').css('display', '' );
	$('#explorateur_regroupe_interloc').height($('#onglet').height());
	$('#interloc_regroup').attr('onchange', "updateFormRegroupInterloc("+session_af_id+", 'ALL');");
	$('#btn_regroupe_proprios').attr('onclick', "regroupeProprios("+session_af_id+");");
	if ($('#interloc_regroup option:selected').val() == -1) {
		$('#img_flt_proprios_dispos_nom').attr('onclick', "");
	} else {
		$('#img_flt_proprios_dispos_nom').attr('onclick', "updateFormRegroupInterloc("+session_af_id+", 'SIMPLE');");
	}
	if (mode == 'charge') {
		$('#flt_lst_interloc_regroup').val('');
		flt_interloc_regroup = '';
	} else{
		flt_interloc_regroup = $('#flt_lst_interloc_regroup').val();
	}
	$('#explorateur_regroupe_interloc_sousdiv').css('top', '50px');
	$('#proprios_dispos').html('')
	$('#proprios_associes').html('')
	$('#flt_proprios_dispos_nom').val('')
	$('#flt_proprios_saf').css('font-weight', 'bold');
	$('#flt_proprios_saf').attr('onclick', "");
	$('#flt_proprios_sites').css('font-weight', 'normal');	
	$('#flt_proprios_sites').attr('onclick', "fltProprioSafSites("+session_af_id+", 'sites');");
		
	changeOpac(20, 'onglet');
	if (typeof xhrPool == "undefined") {
		xhrPool = [];
	}
	$.each(xhrPool, function(idx, jqXHR) {
		jqXHR.abort();
	});
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_showFormRegroupInterloc', params: {var_session_af_id: session_af_id, var_flt_interloc_regroup: flt_interloc_regroup} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(jqXHR, settings){
			xhrPool.push(jqXHR);
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#interloc_regroup').html(data['interloc_regroup'])
			if (($('#explorateur_regroupe_interloc_sousdiv').height() + $('#explorateur_regroupe_interloc_sousdiv').offset().top - $('#main').offset().top) > $('#main').height()) {
				$('#main').attr('style', "height:" + ($('#explorateur_regroupe_interloc_sousdiv').height() + $('#explorateur_regroupe_interloc_sousdiv').offset().top - $('#main').offset().top) + "px !important;");
			}			
			if (mode == 'charge') {
				$('#flt_lst_interloc_regroup').css('min-width', $('#interloc_regroup').css('width').replace('px', '')-55+'px');
				$('#interloc_regroup').css('min-width', $('#interloc_regroup').css('width'));
			}
		}
	});
}

/* Cettef onction permet de mettre à jour le formulaire de regroupement des dnuper correspondant au même propriétaire en fonction des filtres réalisés. */
function updateFormRegroupInterloc(session_af_id, mode) {
	interloc_id = $('#interloc_regroup option:selected').val();
	nom = $('#flt_proprios_dispos_nom').val();
	if ($('#flt_proprios_saf').css('font-weight') == 'bold' || $('#flt_proprios_saf').css('font-weight') == '700') {
		mode_proprio = 'SAF';
	} else {
		mode_proprio = 'sites';
	}
	if ($('#interloc_regroup option:selected').val() == -1) {
		$('#img_flt_proprios_dispos_nom').attr('onclick', "");
	} else {
		$('#img_flt_proprios_dispos_nom').attr('onclick', "updateFormRegroupInterloc("+session_af_id+", 'SIMPLE');");
	}
	$.ajax({ 
		url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_updateFormRegroupInterloc', params: {var_interloc_id : interloc_id, var_session_af_id: session_af_id, var_nom: nom, var_mode_proprio: mode_proprio} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			if (mode == 'ALL') {
				$('#proprios_associes').html(data['proprios_associes'])
			}
			proprioDeleting = ''
			$('#proprios_dispos option').each(function(){
				if ($(this).css('font-weight') == 'bold' || $(this).css('font-weight') == '700') {
					var_option = '<option id="'+$(this).val()+'" style="font-weight:bold;" value="'+$(this).val()+'">'+$(this).html()+'</option>';
					proprioDeleting += var_option
				} 
			});			
			$('#proprios_dispos').html(proprioDeleting + data['proprios_dispos'])
		}
	});
}

/* Cette fonction permet d'enregistrer la saisie effectuée sur le formaulaire de regroupement des dnuper correspondant au même propriétaire. */
function regroupeProprios(session_af_id) {
	interloc_id = $('#interloc_regroup option:selected').val();
	tbl_proprios_assoc = [];
	tbl_proprios_dissoc = [];
	
	$('#proprios_associes option').each(function(){
		if ($(this).css('font-weight') == 'bold' || $(this).css('font-weight') == '700') {
			tbl_proprios_assoc.push("'"+$(this).val()+"'");
		}
	});
	$('#proprios_dispos option').each(function(){
		if ($(this).css('font-weight') == 'bold' || $(this).css('font-weight') == '700') {
			tbl_proprios_dissoc.push("'"+$(this).val()+"'");
		}
	});
	
	if ($('#proprios_associes option').length > 0) {
		$('#lbl_regroupe_proprios').css('display', 'none');
		$.ajax({ 
			url       : "af_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json",
			data      :{ 
				fonction:'fct_regroupeProprios', params: {var_interloc_id : interloc_id, var_tbl_proprios_assoc: tbl_proprios_assoc, var_tbl_proprios_dissoc: tbl_proprios_dissoc} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page af_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#explorateur_regroupe_interloc').css('display', 'none' );
				changeOpac(100, 'onglet');
				create_lst_interlocs(0, session_af_id, 0);
			}
		});
	} else {
		$('#lbl_regroupe_proprios').css('display', '')
	}
}