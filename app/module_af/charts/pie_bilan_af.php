<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../../../includes/configuration.php');
	try {
		$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
	}
	catch (Exception $e) {
		die(include('../../../includes/prob_tech.php'));
	}
			
	$session_af_id = $_GET['session_af_id'];

	$rq_acq = "
	SELECT DISTINCT 
		sum(dcntlo) as surf_lot
	FROM 
		cadastre.lots_cen
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
		JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
		JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		JOIN foncier.r_cad_site_mf USING (cad_site_id)
		JOIN foncier.mf_acquisitions USING (mf_id)
		JOIN foncier.r_mfstatut USING (mf_id),
		(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
	WHERE 
		rel_saf_foncier.session_af_id = :session_af_id AND (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id) AND r_mfstatut.statutmf_id = '1'";
	$res_acq = $bdd->prepare($rq_acq);
	$res_acq->execute(array('session_af_id'=>$session_af_id));
	$acq = $res_acq->fetch();
	$pie[0] = round($acq['surf_lot'], 1);
	$pie_lgd[0] = 'Lots acquis';
	
	$rq_prj_acq = "
	SELECT DISTINCT 
		sum(dcntlo) as surf_lot
	FROM 
		cadastre.lots_cen
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
		JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
		JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		JOIN foncier.r_cad_site_mf USING (cad_site_id)
		JOIN foncier.mf_acquisitions USING (mf_id)
		JOIN foncier.r_mfstatut USING (mf_id),
		(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
	WHERE 
		rel_saf_foncier.session_af_id = :session_af_id AND (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id) AND r_mfstatut.statutmf_id = '0'";
	$res_prj_acq = $bdd->prepare($rq_prj_acq);
	$res_prj_acq->execute(array('session_af_id'=>$session_af_id));
	$prj_acq = $res_prj_acq->fetch();
	$pie[1] = round($prj_acq['surf_lot'], 1);
	$pie_lgd[1] = "Lots en projet d'acquisition";
	
	$rq_conv = "
	SELECT DISTINCT 
		sum(dcntlo) as surf_lot
	FROM 
		cadastre.lots_cen
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
		JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
		JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		JOIN foncier.r_cad_site_mu USING (cad_site_id)
		JOIN foncier.mu_conventions USING (mu_id)
		JOIN foncier.r_mustatut USING (mu_id),
		(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
	WHERE 
		rel_saf_foncier.session_af_id = :session_af_id AND (r_mustatut.date = tt2.statut_date AND r_mustatut.mu_id = tt2.mu_id) AND r_mustatut.statutmu_id = '1'";
	$res_conv = $bdd->prepare($rq_conv);
	$res_conv->execute(array('session_af_id'=>$session_af_id));
	$conv = $res_conv->fetch();
	$pie[2] = round($conv['surf_lot'], 1);
	$pie_lgd[2] = 'Lots conventionn&eacute;s';
	
	$rq_prj_conv = "
	SELECT DISTINCT 
		sum(dcntlo) as surf_lot
	FROM 
		cadastre.lots_cen
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
		JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
		JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		JOIN foncier.r_cad_site_mu USING (cad_site_id)
		JOIN foncier.mu_conventions USING (mu_id)
		JOIN foncier.r_mustatut USING (mu_id),
		(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
	WHERE 
		rel_saf_foncier.session_af_id = :session_af_id AND (r_mustatut.date = tt2.statut_date AND r_mustatut.mu_id = tt2.mu_id) AND r_mustatut.statutmu_id = '0'";
	$res_prj_conv = $bdd->prepare($rq_prj_conv);
	$res_prj_conv->execute(array('session_af_id'=>$session_af_id));
	$prj_conv = $res_prj_conv->fetch();
	$pie[3] = round($prj_conv['surf_lot'], 1);
	$pie_lgd[3] = 'Lots en projet de convention';
	
	$rq_nd = "
	SELECT DISTINCT 
		sum(dcntlo) as surf_lot
	FROM 
		cadastre.lots_cen
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
		JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
		JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
	WHERE 
		rel_saf_foncier.session_af_id = :session_af_id AND 
		lot_bloque = 'f'";
	$res_nd = $bdd->prepare($rq_nd);
	$res_nd->execute(array('session_af_id'=>$session_af_id));
	$nd = $res_nd->fetch();
	$pie[4] = round($nd['surf_lot']-array_sum($pie), 1);
	$pie_lgd[4] = 'A d&eacute;terminer';
	
	For($i=0; $i<count($pie); $i++)
		{
			If((($pie[$i]*100)/array_sum($pie)) < 0.09)
				{$explode[$i] = 20000;}
			ElseIf ($i < count($pie)-1 && (($pie[$i]*100)/array_sum($pie)) < 2 && (($pie[$i]*100)/array_sum($pie)) > 0 && (($pie[$i+1]*100)/array_sum($pie)) < 1 && (($pie[$i+1]*100)/array_sum($pie))) 
				{$explode[$i] = 25;}
			ElseIf ((($pie[$i]*100)/array_sum($pie)) < 2 && (($pie[$i]*100)/array_sum($pie)) > 0) 
				{$explode[$i] = 10;}
			Else
				{$explode[$i] = 0;}
		}

	include ("../../jpgraph/jpgraph.php");
	include ("../../jpgraph/jpgraph_pie.php");
	include ("../../jpgraph/jpgraph_pie3d.php");

	$graph = new PieGraph(400,230); 
	$graph->graph_theme = null;
	$title = "Bilan de l'animation\nen pourcentage de la contenance";
	$graph->title->Set($title);
	$graph->title->SetFont(FF_VERDANA,FS_BOLD, 9);
	$graph->SetColor('#ffffff');
	$graph->SetAntiAliasing(); 
	$graph->SetFrame(false);

	$bplot1 = new piePlot3d($pie);
	$bplot1->SetCenter(0.5, 0.40);
	$bplot1->SetSize(80);
	$bplot1->SetHeight(6);
	$bplot1->SetAngle(40);
	$bplot1->explode($explode);
	$bplot1->SetLegends($pie_lgd);
	$bplot1->SetSliceColors(array('#4472c4','#8ab0f0','#70ad47','#a1f565', '#fe0000')); //#FC7F3C','#FFF168','#00adff','#CCCCCC'
	$bplot1->value->SetFont(FF_ARIAL,FS_NORMAL,8);
	$bplot1->value->SetColor('darkgray@0.1');
	$bplot1->SetLabelPos(0.95);
	$bplot1->value->SetFormat("%.0f%%");
	$bplot1->SetStartAngle(90);

	$graph->legend->Pos(0.1, 0.70);
	$graph->legend->SetMarkAbsSize(10);
	$graph->legend->SetFont(FF_VERDANA,FS_NORMAL,8);
	$graph->legend->SetFrameWeight(0);
	$graph->legend->SetFillColor('#ffffff');
	$graph->legend->SetColumns(2);

	$graph->Add($bplot1);
	$graph->Stroke();
?>
