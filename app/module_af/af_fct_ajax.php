<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	//TOUTES LES FONCTIONS DE CETTE PAGE SONT APPELLEES PAR LA METHODE AJAX DEPUIS LA PAGE foncier.js ET RENVOIENT DU CODE PHP DYNAMIQUEMENT INTERPRETE DANS LA NAVIGATION
	
	session_start();
	$fonction = $_POST['fonction'];
	unset($_POST['fonction']);
	$fonction($_POST);
	
	// Cette fonction permet d'afficher les interlocs
	// Elle permet également d'adapter la liste des interlocs en fonction des filtres	
	function fct_create_lst_interlocs($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$page = $data['params']['page']; // Cette variable récupère le nom de la page fiche d'origine
		$session_af_id = $data['params']['var_session_af_id']; // Cette variable récupère l'id de la SAF
		$section = $data['params']['section']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
		$par_num = $data['params']['par_num']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
		$nom = $data['params']['nom']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
		$scroll_top = $data['params']['var_scroll_top'];
		$offset = $data['params']['offset'];
		$nbr_result = 600000;
			
		$result['code'] = '';
		
		$rq_lst_eaf = "
		SELECT DISTINCT 
			entite_af_id,
			array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
			array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
			array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
			t1.nbr_echanges,
			CASE WHEN COALESCE(t_actif.nbr_actif, 0) = 0 OR t_bloque.nbr_lot_bloque = count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 0 ELSE 1 END as ordre
		FROM 		
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.lots_cen USING (lot_id)
			LEFT JOIN 
				(SELECT DISTINCT 
					entite_af_id,
					array_to_string(array_agg(DISTINCT 
						CASE WHEN nom_usage = 'N.P.' THEN
							ddenom
						ELSE
							CASE WHEN nom_jeunefille = 'N.P.' THEN
								nom_usage
							ELSE
								nom_usage||' '||nom_jeunefille
							END
						END), ',') as tbl_eaf_ddenom,
					count(echange_af_id) as nbr_echanges
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
				WHERE 
					session_af_id = :session_af_id	
				GROUP BY entite_af_id
				) t1 USING (entite_af_id)
			LEFT JOIN 
				(SELECT DISTINCT 
					entite_af_id,
					count(interloc_id) as nbr_actif
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
				WHERE 
					session_af_id = :session_af_id AND
					etat_interloc_id = 1
				GROUP BY entite_af_id
				) t_actif USING (entite_af_id)
			LEFT JOIN 
			(
				SELECT 
					entite_af_id,
					count(rel_saf_foncier_id) as nbr_lot_bloque
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				WHERE
					session_af_id = :session_af_id AND
					lot_bloque = 't'
				GROUP BY
					entite_af_id
			) as t_bloque USING (entite_af_id)
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.' AND
			lot_id ILIKE '%".$section.'%'.$par_num."%'";
			
		If ($nom != '' && $nom != NULL) {
			$rq_lst_eaf .= " AND t1.tbl_eaf_ddenom ILIKE '%$nom%'";
		}
		$rq_lst_eaf .= "
		GROUP BY 
			entite_af_id,
			t1.nbr_echanges,
			t_actif.nbr_actif,
			t_bloque.nbr_lot_bloque
		ORDER BY 
			ordre DESC, 
			entite_af_id DESC";											
		$res_lst_eaf = $bdd->prepare($rq_lst_eaf);
		// echo $rq_lst_eaf;
		$res_lst_eaf->execute(array('session_af_id'=>$session_af_id));
		$lst_eaf = $res_lst_eaf->fetchall();
		For ($i=0; $i<$res_lst_eaf->rowCount(); $i++) { 
			$result['code'] .= "
			<tr id='tr_".$lst_eaf[$i]['entite_af_id']."'>
				<td style='border-right: 1px solid #ccc; border-bottom: 4px solid #ccc;width:1%;'>
					<table id='interloc_tbl_lots_".$lst_eaf[$i]['entite_af_id']."' CELLSPACING = '1' CELLPADDING ='0' class='no-border'>";
					
			$rq_info_lots = "
			SELECT DISTINCT
				rel_saf_foncier_id,
				communes.nom,
				t4.cad_site_id,
				t1.dnupla,
				t1.ccosec,
				COALESCE(t2.dnulot, 'N.D.') as dnulot,
				t2.lot_id,
				t2.dcntlo,
				COALESCE(rel_eaf_foncier.utilissol, 'N.D.') AS utilissol,
				rel_saf_foncier.paf_id,
				d_paf.paf_lib,
				COALESCE(d_objectif_af.objectif_af_lib, 'N.D.') as objectif_af_lib,
				lot_bloque::text
			FROM
				administratif.communes
				JOIN cadastre.parcelles_cen t1 ON (code_insee = codcom)
				JOIN cadastre.lots_cen t2 USING (par_id)
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.d_paf USING (paf_id)
				LEFT JOIN animation_fonciere.d_objectif_af USING (objectif_af_id)
			WHERE 
				entite_af_id = :entite_af_id AND 
				rel_saf_foncier.session_af_id = :session_af_id AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'
			ORDER BY t2.lot_id";
			$res_info_lots = $bdd->prepare($rq_info_lots);
			$res_info_lots->execute(array('entite_af_id'=>$lst_eaf[$i]['entite_af_id'], 'session_af_id'=>$session_af_id));
			$info_lots = $res_info_lots->fetchall();
			For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 			
				If ($info_lots[$l]['lot_bloque'] == 'true') {
					$parc_class = 'lot_bloque';
				} Else {
					$parc_class = '';
				}
				
				$result['code'] .= "
						<tr id='interloc_tr_lot-".$info_lots[$l]['rel_saf_foncier_id']."'>
							<td class='no-border ".$parc_class."' style='margin-top:5px;width:1%;'>
								<label class='label'>
									<u>Lot :</u>
								</label>
								<label id='interloc_lbl_lot_id-".$info_lots[$l]['rel_saf_foncier_id']."' class='label'>
									".$info_lots[$l]['nom']." - ".$info_lots[$l]['ccosec']." - ".$info_lots[$l]['dnupla'];
								If ($info_lots[$l]['dnulot'] != 'N.D.') {
									$result['code'] .= " - " . $info_lots[$l]['dnulot'];
								}
								$result['code'] .= "
								</label>
							
								<img id='interloc_btn_detail-".$info_lots[$l]['rel_saf_foncier_id']."' src='".ROOT_PATH."images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick=\"showDetail('interloc', '".$info_lots[$l]['rel_saf_foncier_id']."');\">															
							</td>
						</tr>
						<tr>
							<td class='".$parc_class."' id='interloc_td_detail-".$info_lots[$l]['rel_saf_foncier_id']."' style='display:none' colspan='2'>
								<table class='no-border'>
									<tr>
										<td class='no-border'>
											<label class='label'>
												<u>ID :</u> ".$info_lots[$l]['lot_id']."
											</label>
										</td>
									</tr>
									<tr>
										<td class='no-border'>
											<label class='label'>
												<u>Contenance :</u> ".conv_are($info_lots[$l]['dcntlo'])."
											</label>
										</td>
									</tr>		
									<tr>
										<td class='no-border'>
											<label class='label'>
												<u>Priorit&eacute; d'animation :</u> ".($info_lots[$l]['paf_id'])."
											</label>
										</td>
									</tr>
									<tr>
										<td class='no-border'>
											<label class='label'>
												<u>Objectif :</u> ".($info_lots[$l]['objectif_af_lib'])."
											</label>
										</td>
									</tr>
									<tr>
										<td class='no-border'>";
												$rq_nat_cult = 
												"SELECT DISTINCT
													d_dsgrpf.natcult_ssgrp,
													d_dsgrpf.cgrnum,
													lots_natcult_cen.dcntsf as surf_nat_cult,
													d_dsgrpf.dsgrpf
												FROM
													cadastre.lots_cen t2	
													JOIN cadastre.lots_natcult_cen USING (lot_id)
													JOIN cadastre.d_dsgrpf USING (dsgrpf)
												WHERE t2.lot_id = ?
												ORDER BY surf_nat_cult DESC";
												$res_nat_cult = $bdd->prepare($rq_nat_cult);
												$res_nat_cult->execute(array($info_lots[$l]['lot_id']));					
												$nat_cult = $res_nat_cult->fetchall();
												
											$result['code'] .= "
											<table style='border:0 solid black'>
												<tr>
													<td style='vertical-align:top'>
														<label class='label' style='margin-left:-3px'><u>Natures de culture : </u></label> 
													</td>
												</tr>
												<tr>
													<td>
														<table CELLSPACING = '0' CELLPADDING ='0' style='border:0 solid black;margin-left:5px;'>";
													If (count($nat_cult) == 0) {
														$result['code'] .= "<td><label class='label'>N.D.</label></td>";
													} Else {
														For ($n=0; $n<count($nat_cult); $n++) { 
															$result['code'] .= "
															<tr>
																<td style='text-align:left'>
																	<label class='label'>
																		".retour_ligne($nat_cult[$n]['natcult_ssgrp'] . ' : ' . conv_are($nat_cult[$n]['surf_nat_cult']) . ' (' . round(($nat_cult[$n]['surf_nat_cult']*100)/$info_lots[$l]['dcntlo'], 0) . '%)', 40)."
																	</label>
																</td>
															</tr>";
														}
													} 
														$result['code'] .= "
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>									
								</table>
							</td>
						</tr>";		
			}
					$result['code'] .= "
					</table>
				</td>
				<td style='border-bottom: 4px solid #ccc;'>
					<center>
						<table id='tbl_".$lst_eaf[$i]['entite_af_id']."' style='float:none;' CELLSPACING = '0' CELLPADDING ='8' class='no-border'>";
							$result['code'] .= fct_rq_lst_interlocs($session_af_id, $lst_eaf[$i]['entite_af_id'], $lst_eaf[$i]['tbl_eaf_lot_id'], $lst_eaf[$i]['tbl_rel_saf_foncier_id']);	
							$result['code'] .= "
						</table>
					</center>
				</td>";
		}
		print json_encode($result);
	}
	
	// Cette fonction permet d'afficher les informations de l'onglet "Parcelles/ Lots"
	// Elle permet également d'adapter l'onglet en fonction des filtres	
	function fct_create_lst_parc($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
					
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		$page = $data['params']['page']; // Cette variable récupère le nom de la page fiche d'origine
		$session_af_id = $data['params']['var_session_af_id']; // Cette variable récupère l'id de la SAF
		$nom = $data['params']['nom']; // Cette variable récupère la saisie du filtre interlocuteur
		$section = $data['params']['section']; // Cette variable récupère la saisie du filtre section
		$par_num = $data['params']['par_num']; // Cette variable récupère la saisie du filtre par num
		$scroll_top = $data['params']['var_scroll_top'];
		$offset = $data['params']['offset'];
		$nbr_result = 600000;
		
		$result['code'] = '';
		
		$rq_lst_eaf = "
		SELECT DISTINCT 
			entite_af_id,
			entite_af.commentaires,
			array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
			array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
			array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
			t1.nbr_echanges,
			COALESCE(t2.nbr_obj_acqu, 0) as nbr_obj_acqu,
			CASE WHEN COALESCE(t_actif.nbr_actif, 0) = 0 AND COALESCE(t_bloque.nbr_lot_bloque, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) AND COALESCE(t_supprime.nbr_lot_supprime, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
				-1 
			ELSE
				CASE WHEN COALESCE(t_bloque.nbr_lot_bloque, 0) = count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) AND COALESCE(t_supprime.nbr_lot_supprime, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
					-2 
				ELSE
					CASE WHEN COALESCE(t_supprime.nbr_lot_supprime, 0) = count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
						-3
					ELSE 
						1 
					END 
				END
			END as ordre
		FROM 		
			animation_fonciere.entite_af
			JOIN animation_fonciere.rel_eaf_foncier USING (entite_af_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			LEFT JOIN 
				(
					SELECT 
						entite_af_id,
						count(rel_saf_foncier_id) as nbr_lot_supprime
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN foncier.cadastre_site t4 USING (cad_site_id)
						JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
					WHERE
						session_af_id = :session_af_id AND
						(t3.date_fin != 'N.D.' OR t4.date_fin != 'N.D.')
					GROUP BY
						entite_af_id
				) as t_supprime USING (entite_af_id)
			LEFT JOIN 
				(
					SELECT 
						entite_af_id,
						count(rel_saf_foncier_id) as nbr_lot_bloque
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						session_af_id = :session_af_id AND
						lot_bloque = 't'
					GROUP BY
						entite_af_id
				) as t_bloque USING (entite_af_id)
			LEFT JOIN 
				(
					SELECT DISTINCT 
						entite_af_id,
						count(interloc_id) as nbr_actif
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					WHERE 
						session_af_id = :session_af_id AND
						etat_interloc_id = 1
					GROUP BY entite_af_id
				) t_actif USING (entite_af_id)	
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.lots_cen USING (lot_id)
			JOIN 
				(
					SELECT DISTINCT 
						entite_af_id,
						count(echange_af_id) as nbr_echanges,						
						array_to_string(array_agg(
							CASE WHEN nom_usage = 'N.P.' THEN
								ddenom
							ELSE
								CASE WHEN nom_jeunefille = 'N.P.' THEN
									nom_usage
								ELSE
									nom_usage||' '||nom_jeunefille
								END
							END), ',') as tbl_eaf_ddenom
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
					WHERE
						session_af_id = :session_af_id
					GROUP BY entite_af_id
				) t1 USING (entite_af_id)
			LEFT JOIN 
				(
					SELECT DISTINCT 
						entite_af_id,
						count(rel_saf_foncier_id) as nbr_obj_acqu
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						session_af_id = :session_af_id AND
						objectif_af_id = 1
					GROUP BY entite_af_id
				) t2 USING (entite_af_id)		
			
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			lot_id ILIKE '%".$section.'%'.$par_num."%'";
			
		If ($nom != '' && $nom != NULL) {
			$rq_lst_eaf .= " AND t1.tbl_eaf_ddenom ILIKE '%$nom%'";
		}
		$rq_lst_eaf .= "
		GROUP BY 
			entite_af_id,
			entite_af.commentaires,
			t1.nbr_echanges,
			t2.nbr_obj_acqu,
			t_bloque.nbr_lot_bloque,
			t_supprime.nbr_lot_supprime,
			t_actif.nbr_actif
		ORDER BY 
			ordre DESC, 
			entite_af_id DESC";											
		$res_lst_eaf = $bdd->prepare($rq_lst_eaf);
		// echo $rq_lst_eaf;
		$res_lst_eaf->execute(array('session_af_id'=>$session_af_id));
		$lst_eaf = $res_lst_eaf->fetchall();
		For ($i=0; $i<$res_lst_eaf->rowCount(); $i++) { 
			$result['code'] .= "
			<tr>
				<td colspan='2' style='background:#fff;'>
					&nbsp;
				</td>
			</tr>
			<tr id='tr_parc_prix_eaf-".$lst_eaf[$i]['entite_af_id']."'>
				<td colspan='2' style='border-color:#ccc;border-width:4px 4px 1px 4px;border-style:solid;'>
					<table class='no-border' cellspacing='0' cellpadding='0' width='100%'>
						<tr>
							<td id='td_prix_eaf-".$lst_eaf[$i]['entite_af_id']."' style='text-align:left;padding-left:25px;'>
								<label class='label'><i>Prix d'acquisition de l'EAF => </i></label>
							";
								$result['code'] .= fct_rq_prix_eaf($session_af_id, $lst_eaf[$i]['entite_af_id'], $lst_eaf[$i]['nbr_echanges']);
							$result['code'] .= "
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr id='tr_parc_infos-".$lst_eaf[$i]['entite_af_id']."'>
				<td style='border-right: 1px solid #ccc; border-left: 4px solid #ccc;'>
					<table cellspacing='0' cellpadding='8' id='tbl_".$lst_eaf[$i]['entite_af_id']."' class='no-border'>";
						$rq_info_interlocs = "
						SELECT DISTINCT
							interlocuteurs.interloc_id,
							d_etat_interloc.etat_interloc_id,
							d_etat_interloc.etat_interloc_lib,
							interlocuteurs.dnuper,
							d_ccoqua.ccoqua as particule_id,
							d_ccoqua.particule_lib,
							interlocuteurs.ddenom,
							interlocuteurs.nom_usage,
							COALESCE(interlocuteurs.nom_jeunefille, 'N.D.') as nom_jeunefille,
							interlocuteurs.prenom_usage,
							COALESCE(interlocuteurs.nom_conjoint, 'N.D.') as nom_conjoint,
							COALESCE(interlocuteurs.prenom_conjoint, 'N.D.') as prenom_conjoint,
							COALESCE(REPLACE(interlocuteurs.jdatnss, '-', ''), 'N.D.') as jdatnss,
							COALESCE(interlocuteurs.dlign3, 'N.D.') as dlign3,
							COALESCE(interlocuteurs.dlign4, 'N.D.') as dlign4,
							COALESCE(interlocuteurs.dlign5, 'N.D.') as dlign5,
							COALESCE(interlocuteurs.dlign6, 'N.D.') as dlign6,
							COALESCE(interlocuteurs.email, 'N.D.') as email,
							COALESCE(interlocuteurs.fixe_domicile, 'N.D.') as fixe_domicile,
							COALESCE(interlocuteurs.fixe_travail, 'N.D.') as fixe_travail,
							COALESCE(interlocuteurs.portable_domicile, 'N.D.') as portable_domicile,
							COALESCE(interlocuteurs.portable_travail, 'N.D.') as portable_travail,
							COALESCE(interlocuteurs.fax, 'N.D.') as fax,
							d_type_interloc.type_interloc_id, 
							d_type_interloc.type_interloc_lib, 
							d_type_interloc.can_nego::text, 
							d_type_interloc.avis_favorable::text, 
							d_groupe_interloc.groupe_interloc_id, 
							d_groupe_interloc.groupe_interloc_color, 
							d_groupe_interloc.can_be_subst::text,
							d_groupe_interloc.can_be_ref::text,
							interlocuteurs.gtoper,
							array_to_string(array_agg(rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id ORDER BY rel_saf_foncier_interlocs_id), '_') as tbl_rel_saf_foncier_interlocs_id,
							CASE WHEN ref1.interloc_id IS NOT NULL THEN 
								'1ref:'||ref1.interloc_id || '-a-' || CASE WHEN ref1.interloc_id = ref1.interloc_ref_id THEN 0 ELSE ref1.interloc_ref_id END
							ELSE
								CASE WHEN ref2.interloc_id IS NOT NULL THEN 
									'1ref:'||ref2.interloc_ref_id || '-b-' || ref2.interloc_id
								ELSE
									'2notref:'||interlocuteurs.interloc_id || '-a-1'
								END
							END as ordre
						FROM 
							animation_fonciere.interlocuteurs 
							JOIN animation_fonciere.d_etat_interloc USING (etat_interloc_id)
							LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
							JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							JOIN animation_fonciere.session_af USING (session_af_id)
							LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)									
							LEFT JOIN 
								(
									SELECT DISTINCT
										st1.interloc_id,
										rel_saf_foncier_interlocs.interloc_id as interloc_ref_id,
										rel_saf_foncier_interlocs.type_interloc_id as type_interloc_ref_id
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN
											(
												SELECT DISTINCT 
													rel_saf_foncier_interlocs.interloc_id, 
													interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
												FROM 
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
												WHERE 
													session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id

												UNION

												SELECT DISTINCT 
													t_temp_ref.interloc_id, 
													rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id as foncier_interloc_ref_id
												FROM 
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
													JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (t_temp_ref.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)
												WHERE 
													session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id

												UNION

												SELECT DISTINCT 
													rel_saf_foncier_interlocs.interloc_id, 
													rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
												FROM 
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
													JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id =  interloc_substitution.rel_saf_foncier_interlocs_id_vp)
												WHERE 
													groupe_interloc_id = 3 AND
													session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id

												
											) st1 ON (st1.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id)
								) ref1 ON (ref1.interloc_ref_id = rel_saf_foncier_interlocs.interloc_id AND ref1.type_interloc_ref_id = rel_saf_foncier_interlocs.type_interloc_id)
							LEFT JOIN 
								(
									SELECT
										st2.interloc_id,
										rel_saf_foncier_interlocs.interloc_id as interloc_ref_id,
										rel_saf_foncier_interlocs.type_interloc_id as type_interloc_ref_id
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN
											(
												SELECT DISTINCT 
													rel_saf_foncier_interlocs.interloc_id, 
													interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
												FROM 
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
												WHERE 
													session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id
													
												UNION

												SELECT DISTINCT
													t_subst.interloc_id,
													t_ref.rel_saf_foncier_interlocs_id
												FROM
													(
														SELECT DISTINCT
															rel_saf_foncier_interlocs.interloc_id,
															rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
															rel_saf_foncier_interlocs_id_vp
														FROM
															animation_fonciere.interlocuteurs
															JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
															JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
															JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
															JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
															JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
															JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
														WHERE 
															groupe_interloc_id != 3 AND
															session_af_id = :session_af_id AND 
															entite_af_id = :entite_af_id
													) t_subst
													JOIN
													(
														SELECT DISTINCT
															rel_saf_foncier_interlocs.interloc_id,
															rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
															interloc_referent.rel_saf_foncier_interlocs_id as under_ref
														FROM
															animation_fonciere.interlocuteurs
															JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
															JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)
															JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
															JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
														WHERE 
															session_af_id = :session_af_id AND 
															entite_af_id = :entite_af_id
													) t_ref ON (t_subst.rel_saf_foncier_interlocs_id_vp = t_ref.under_ref)	
											) st2 ON st2.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
								) ref2 ON (ref2.interloc_id = rel_saf_foncier_interlocs.interloc_id AND ref2.type_interloc_ref_id = rel_saf_foncier_interlocs.type_interloc_id)
						WHERE 
							rel_saf_foncier.session_af_id = :session_af_id 
							AND entite_af_id = :entite_af_id 
							AND rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN 
								(
									SELECT DISTINCT 
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
									FROM 
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN 
											(
												SELECT
													rel_saf_foncier_interlocs_id,
													interloc_id,
													type_interloc_id
												FROM
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												WHERE 
													session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id

												UNION

												SELECT
													rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
													rel_saf_foncier_interlocs.interloc_id,
													rel_saf_foncier_interlocs.type_interloc_id
												FROM
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													LEFT JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
													JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
													JOIN animation_fonciere.d_type_interloc ON (t_temp_subst.type_interloc_id = d_type_interloc.type_interloc_id)
													
												WHERE 
													session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id AND
													groupe_interloc_id = 3
											) t1 ON (t1.interloc_id = rel_saf_foncier_interlocs.interloc_id AND t1.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id
								)
							AND rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN
								(
									SELECT DISTINCT 
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
									FROM 
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
										JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)

									WHERE
										can_be_subst = 't' AND
										rel_saf_foncier_interlocs_id NOT IN
											(
												SELECT rel_saf_foncier_interlocs_id_is
												FROM animation_fonciere.interloc_substitution
											)
								)
						GROUP BY 
							interlocuteurs.interloc_id, 
							d_ccoqua.ccoqua,
							d_ccoqua.particule_lib, 
							d_type_interloc.type_interloc_id,
							d_type_interloc.type_interloc_lib,
							d_type_interloc.can_nego, 
							d_type_interloc.avis_favorable, 
							d_groupe_interloc.groupe_interloc_id, 
							d_groupe_interloc.groupe_interloc_color, 
							d_groupe_interloc.can_be_subst, 
							d_groupe_interloc.can_be_ref,
							d_etat_interloc.etat_interloc_id,
							d_etat_interloc.etat_interloc_lib,
							ordre
						ORDER BY ordre, groupe_interloc_id, type_interloc_id DESC
						";
						// echo $rq_info_interlocs;
						$res_info_interlocs = $bdd->prepare($rq_info_interlocs);
						$res_info_interlocs->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
						$lst_interlocs = $res_info_interlocs->fetchall();
						For ($j=0; $j<$res_info_interlocs->rowCount(); $j++) { 
							$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_interlocs[$j]['particule_lib'], $lst_interlocs[$j]['nom_usage'], $lst_interlocs[$j]['prenom_usage'], $lst_interlocs[$j]['ddenom']));	
							
							If ($lst_interlocs[$j]['etat_interloc_id'] == 1) {	
								$color_font_interloc = $lst_interlocs[$j]['groupe_interloc_color'];
							} Else {
								$color_font_interloc = '#ccc';
							}
								$result['code'] .= "									
								<tr>			
									<td class='bilan-af' style='padding-bottom:2px;"; If (substr_count($lst_interlocs[$j]['ordre'], '-b-') > 0) {$result['code'] .= "padding-left:30px;padding-top:10px;";} Else {$result['code'] .= "padding-left:15px;";} If (substr_count($lst_interlocs[$j]['ordre'], '-a-') > 0 && $lst_interlocs[$j]['type_interloc_id'] != 1) {$result['code'] .= "padding-top:0; ";} If ($j>0 && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-'))) {$result['code'] .= "padding-top:20px ";} $result['code'] .= "'>";
										If ($j == 0 || (isset($lst_interlocs[$j-1]['interloc_id']) && $lst_interlocs[$j-1]['interloc_id'] != $lst_interlocs[$j]['interloc_id'])) {
											$result['code'] .= "
											<label id='lbl_".$lst_eaf[$i]['entite_af_id'].'-'.$lst_interlocs[$j]['interloc_id']."' class='label";
											If (substr_count($lst_interlocs[$j]['ordre'], '1ref:') > 0 && substr_count($lst_interlocs[$j]['ordre'], '-a-0') > 0 && substr_count($lst_interlocs[$j]['ordre'], '2notref') == 0 && $lst_interlocs[$j]['can_be_ref'] == 'true') {
												$result['code'] .= " referent"; 
											} 
											$result['code'] .= "' style='color:".$color_font_interloc."'>
												".retour_ligne($interloc_lib, 40)."
											</label>
											<br>";
										}	
											$result['code'] .= "	
											
											<label class='label' style='font-weight:normal;color:".$color_font_interloc.";padding-left:20px;'>";
											If ($lst_interlocs[$j]['groupe_interloc_id'] == 3) {
												$result['code'] .= strtolower($lst_interlocs[$j]['type_interloc_lib'])." de ";
											} Else {
												$result['code'] .= $lst_interlocs[$j]['type_interloc_lib'];
											}
											$result['code'] .= "	
											</label>";
										If ($lst_interlocs[$j]['etat_interloc_id'] != 1) {
											$result['code'] .= "
											<br>
											<label class='last_maj' style='color:red;padding-left:20px;'>	
												(".$lst_interlocs[$j]['etat_interloc_lib'].")
											</label>";
										}
										
										If ($lst_interlocs[$j]['can_be_subst'] == 'true') {	
											$rq_info_interlocs_subst = "
											SELECT DISTINCT
												interlocuteurs.interloc_id,
												d_etat_interloc.etat_interloc_id,
												d_etat_interloc.etat_interloc_lib,
												interlocuteurs.dnuper,
												d_ccoqua.particule_lib,
												interlocuteurs.ddenom,
												interlocuteurs.nom_usage,
												interlocuteurs.nom_jeunefille,
												CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
													CASE WHEN strpos(interlocuteurs.prenom_usage, ' ') > 0 THEN
														substring(interlocuteurs.prenom_usage from 0 for  strpos(interlocuteurs.prenom_usage, ' ')) 
													ELSE 
														interlocuteurs.prenom_usage
													END
												ELSE	
													'N.P.' 
												END as prenom_usage,
												interlocuteurs.nom_conjoint,
												interlocuteurs.prenom_conjoint,
												d_type_interloc.type_interloc_id,
												d_type_interloc.type_interloc_lib,
												d_groupe_interloc.groupe_interloc_id,
												d_groupe_interloc.groupe_interloc_color,
												array_to_string(array_agg(rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id), '_') as tbl_rel_saf_foncier_interlocs_id,
												count(rel_af_echanges.echange_af_id) as nbr_echanges										
											FROM 
												animation_fonciere.interlocuteurs 
												JOIN animation_fonciere.d_etat_interloc USING (etat_interloc_id)
												LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
												JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
												JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
												JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN animation_fonciere.session_af USING (session_af_id)
												JOIN 
													(
														SELECT
															rel_saf_foncier_interlocs_id_vp
														FROM 
															animation_fonciere.interloc_substitution
														WHERE
															rel_saf_foncier_interlocs_id_is IN (".str_replace('_', ',', $lst_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id']).")
													) t1 ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = t1.rel_saf_foncier_interlocs_id_vp)
												LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
											WHERE 
												rel_saf_foncier.session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id										
											GROUP BY 
												interlocuteurs.interloc_id, 
												d_ccoqua.particule_lib, 
												d_type_interloc.type_interloc_id,
												d_type_interloc.type_interloc_lib,
												d_groupe_interloc.groupe_interloc_id,
												d_groupe_interloc.groupe_interloc_color,
												d_etat_interloc.etat_interloc_id,
												d_etat_interloc.etat_interloc_lib";
											// echo $rq_info_interlocs_subst;
											$res_info_interlocs_subst = $bdd->prepare($rq_info_interlocs_subst);
											$res_info_interlocs_subst->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
											$lst_interlocs_subst = $res_info_interlocs_subst->fetchall();
											For ($s=0; $s<$res_info_interlocs_subst->rowCount(); $s++) { 
												If ($s == 0 || (isset($lst_interlocs_subst[$s-1]['interloc_id']) && $lst_interlocs_subst[$s-1]['interloc_id'] != $lst_interlocs_subst[$s]['interloc_id'])) {
													$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_interlocs_subst[$s]['particule_lib'], $lst_interlocs_subst[$s]['nom_usage'], $lst_interlocs_subst[$s]['prenom_usage'], $lst_interlocs_subst[$s]['ddenom']));
													$tbl_type_interloc_subst_index = array_keys(array_values(array_column($lst_interlocs_subst, 'interloc_id')), $lst_interlocs_subst[$s]['interloc_id']);
													If ($lst_interlocs_subst[$s]['etat_interloc_id'] == 1) {	
														$color_font_interloc_subst = $lst_interlocs_subst[$s]['groupe_interloc_color'];
													} Else {
														$color_font_interloc_subst = '#ccc';
													}									
														$result['code'] .= "															
														<li class='label_simple' style='padding-left:60px;'>
															<label id='lbl_".$lst_eaf[$i]['entite_af_id'].'-'.$lst_interlocs_subst[$s]['interloc_id']."' class='label' style='color:".$color_font_interloc_subst."'>
																".retour_ligne($interloc_lib, 40)."
															</label>
															<br>";															
															For ($index_type_subst = 0; $index_type_subst < count($tbl_type_interloc_subst_index); $index_type_subst++) {
																$result['code'] .= "
																<label class='label_simple' style='color:".$color_font_interloc_subst.";padding-left:20px;'>
																	".$lst_interlocs_subst[$tbl_type_interloc_subst_index[$index_type_subst]]['type_interloc_lib']."
																</label>
																<br>";
																If ($lst_interlocs_subst[$s]['etat_interloc_id'] != 1) {
																	$result['code'] .= "
																	<label class='last_maj' style='color:red;padding-left:20px;'>	
																		(".$lst_interlocs_subst[$s]['etat_interloc_lib'].")
																	</label>
																	<br>";
																}
															}
														$result['code'] .= "
														</li>";
												}
											}
										}			
									$result['code'] .= "	
									</td>
								</tr>";
						}
					$result['code'] .= "
					</table>
				</td>
				<td style='border-right: 4px solid #ccc;'>
					<table id='parc_tbl_lots_".$lst_eaf[$i]['entite_af_id']."' CELLSPACING = '1' CELLPADDING ='0' class='no-border' width='100%'>";
			$rq_info_lots = "
			SELECT DISTINCT
				rel_saf_foncier_id,
				t4.cad_site_id,
				t3.date_fin as date_fin_cen,
				t4.date_fin as date_fin_site,
				t2.lot_id,
				communes.nom,
				t1.ccosec,
				t1.dnupla,
				COALESCE(t2.dnulot, 'N.D.') as dnulot,
				t2.dcntlo,
				COALESCE(rel_eaf_foncier.utilissol, 'N.D.') AS utilissol,
				rel_saf_foncier.paf_id,
				d_paf.paf_lib,
				CASE WHEN sum(pourcentage) = 100 THEN
					CASE WHEN rel_saf_foncier.geom IS NULL THEN 
						sum(((rel_saf_foncier_occsol.pourcentage*dcntlo)/100)*rel_saf_foncier_occsol.montant)
					ELSE 
						sum(((rel_saf_foncier_occsol.pourcentage*st_area(rel_saf_foncier.geom))/100)*rel_saf_foncier_occsol.montant)
					END					
				ELSE
					-1
				END as prix_fond_occsol,
				prix_parc_arrondi,
				COALESCE(vl_cen.libelle, 'N.D.') as lieu_dit,
				COALESCE(objectif_af_id, 3) as objectif_af_id,
				CASE WHEN rel_saf_foncier.geom IS NULL THEN dcntlo ELSE round(st_area(rel_saf_foncier.geom)::numeric, 0) END as surf_mu,
				surf_mu_init,
				lot_bloque::text,
				commentaires,
				CASE WHEN tnegoprix.nbr_nego = 1 AND tnegoprix.nbr_all_nego = nbr_interloc_vp THEN
					CASE WHEN tbl_nego_id[1] IN (0, 2, 4) THEN
						tbl_nego_lib[1]
					ELSE
						CASE WHEN tnegoprix.nbr_prix = 1 AND tbl_prix[1] != 0 THEN
							tbl_nego_lib[1] || ' à ' || tbl_prix[1] || ' €'
						ELSE
							tbl_nego_lib[1] || ' mais le prix reste à clarifier'
						END
					END
				ELSE 
					CASE WHEN tnegoprix.nbr_nego = 1 AND tnegoprix.nbr_all_nego != nbr_interloc_vp THEN
						'Certains propriétaires se sont mis d''accord mais ils ne se sont pas encore tous prononcés'
					ELSE
						CASE WHEN tnegoprix.nbr_nego > 1 THEN
							'Les propriétaires ne se sont pas encore mis d''accord'
						ELSE
							'Aucune négociation définie'
						END
					END	
				END as nego,
				COALESCE(t_convention.lib, '') as convention
			FROM
				cadastre.lots_cen t2
				JOIN cadastre.parcelles_cen t1 USING (par_id)
				JOIN administratif.communes ON (code_insee = codcom)
				LEFT JOIN cadastre.vl_cen USING (vl_id)
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				LEFT JOIN animation_fonciere.d_objectif_af USING (objectif_af_id)
				LEFT JOIN animation_fonciere.rel_saf_foncier_occsol USING (rel_saf_foncier_id)
				JOIN animation_fonciere.d_paf USING (paf_id)
				JOIN (
					SELECT
						rel_saf_foncier_id,
						count(DISTINCT interloc_id) as nbr_interloc_vp
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						can_nego = 't' AND 
						can_be_subst = 'f' AND
						entite_af_id = :entite_af_id AND 
						session_af_id = :session_af_id
					GROUP BY
						rel_saf_foncier_id
				) st1 USING (rel_saf_foncier_id)
				LEFT JOIN 
					(
						SELECT DISTINCT
							rel_saf_foncier_id, 
							array_agg(DISTINCT nego_af_lib) as tbl_nego_lib,
							array_agg(DISTINCT nego_af_id) as tbl_nego_id,
							array_length(array_agg(DISTINCT nego_af_id), 1) as nbr_nego,
							array_length(array_agg(nego_af_id), 1) as nbr_all_nego,
							array_agg(DISTINCT nego_af_prix) as tbl_prix,
							array_length(array_agg(DISTINCT nego_af_prix), 1) as nbr_prix
						FROM 
							animation_fonciere.rel_af_echanges
							JOIN animation_fonciere.d_nego_af USING (nego_af_id)
							JOIN animation_fonciere.echanges_af USING (echange_af_id)
							JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN (
								SELECT
									rel_saf_foncier_interlocs_id,
									max(echange_af_date) as max_date
								FROM 
									animation_fonciere.rel_af_echanges
									JOIN animation_fonciere.echanges_af USING (echange_af_id)
									JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								WHERE 
									nego_af_id IS NOT NULL AND
									can_nego = 't' AND 
									can_be_subst = 'f' AND
									entite_af_id = :entite_af_id AND 
									session_af_id = :session_af_id
								GROUP BY
									rel_saf_foncier_interlocs_id
								) st2 ON (echanges_af.echange_af_date = st2.max_date AND rel_af_echanges.rel_saf_foncier_interlocs_id = st2.rel_saf_foncier_interlocs_id)
						WHERE 
							nego_af_id IS NOT NULL AND
							can_nego = 't' AND 
							can_be_subst = 'f'
						GROUP BY
							rel_saf_foncier_id
					) tnegoprix USING (rel_saf_foncier_id)
				LEFT JOIN
					(
						SELECT DISTINCT 
							rel_saf_foncier_id,
							'Lot en cours de convention' as lib
						FROM 
							cadastre.cadastre_cen t3
							JOIN foncier.cadastre_site t4 USING (cad_cen_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
							JOIN foncier.r_cad_site_mu USING (cad_site_id)
							JOIN foncier.mu_conventions USING (mu_id)
							JOIN foncier.r_mustatut USING (mu_id),
							(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
						WHERE 
							rel_saf_foncier.session_af_id = :session_af_id AND 
							r_mustatut.date = tt2.statut_date AND 
							r_mustatut.mu_id = tt2.mu_id AND 
							r_mustatut.statutmu_id = '1' AND
							t3.date_fin = 'N.D.' AND
							t4.date_fin = 'N.D.'
					) t_convention USING (rel_saf_foncier_id)
			WHERE 
				entite_af_id = :entite_af_id AND 
				rel_saf_foncier.session_af_id = :session_af_id
			GROUP BY 
				rel_saf_foncier_id,
				t4.cad_site_id,
				t2.lot_id,
				communes.nom,
				t1.ccosec,
				t1.dnupla,
				t2.dnulot,
				prix_parc_arrondi,
				lot_bloque,
				rel_eaf_foncier.utilissol,
				d_paf.paf_lib,
				lieu_dit,
				nego,
				t_convention.lib,
				t3.date_fin,
				t4.date_fin
			ORDER BY 
				lot_bloque,
				t2.lot_id";
			$res_info_lots = $bdd->prepare($rq_info_lots);
			$res_info_lots->execute(array('entite_af_id'=>$lst_eaf[$i]['entite_af_id'], 'session_af_id'=>$session_af_id));
			// echo $rq_info_lots;
			$info_lots = $res_info_lots->fetchall();
			For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 
				If ($info_lots[$l]['lot_bloque'] == 'true') {
					$parc_class = 'lot_bloque';
					$parc_img_btn = 'plus.png';
					$parc_display_detail = 'none';
				} Else If ($info_lots[$l]['date_fin_cen'] != 'N.D.' || $info_lots[$l]['date_fin_site'] != 'N.D.') {
					$parc_class = 'lot_supprime';
					$parc_img_btn = 'plus.png';
					$parc_display_detail = 'none';
				} Else {
					$parc_class = '';
					$parc_img_btn = 'moins.png';
					$parc_display_detail = 'table';
				}
				$result['code'] .= "
							<tr>
								<td id='parc_td_general-".$info_lots[$l]['rel_saf_foncier_id']."' class='$parc_class'>									
									<table class='no-border' width='100%'>";
									If ($info_lots[$l]['date_fin_cen'] != 'N.D.' || $info_lots[$l]['date_fin_site'] != 'N.D.') {
										$result['code'] .= "
										<tr>
											<td style='text-align:center;' colspan='3'>		
												<label class='label ".$parc_class."' style='margin-right:25px;font-size:11pt'>LOT SUPPRIM&Eacute;</label>
											</td>
										</tr>";
									}	
										$result['code'] .= "
										<tr>
											<td style='text-align:left;width:15px;padding-right:15px;' rowspan='3'>
												<img id='parc_btn_detail-".$info_lots[$l]['rel_saf_foncier_id']."' src='".ROOT_PATH."images/".$parc_img_btn."' width='15px' style='cursor:pointer;' onclick=\"showDetail('parc', '".$info_lots[$l]['rel_saf_foncier_id']."');\">	
											</td>
											<td style='text-align:left;' colspan='2'>									
												<label id='parc_lbl_lot_id-".$info_lots[$l]['rel_saf_foncier_id']."' class='label ".$parc_class."'>
													<u>Lot :</u>".$info_lots[$l]['nom']." - ".$info_lots[$l]['ccosec']." - ".$info_lots[$l]['dnupla'];
												If ($info_lots[$l]['dnulot'] != 'N.D.') {
													$result['code'] .= " - " . $info_lots[$l]['dnulot'];
												}
												$result['code'] .= "
												</label>
											</td>
											
										</tr>
										<tr>
											<td style='text-align:left;'>
												<label class='label ".$parc_class."'>
													<u>Lot ID :</u> ".$info_lots[$l]['lot_id']."
												</label>
											</td>
											<td style='text-align:left;'>
												<label class='label ".$parc_class."'>
													<u>Lieu-dit :</u> ".$info_lots[$l]['lieu_dit']."
												</label>
											</td>
										</tr>
										<tr>
											<td style='text-align:left;'>
												<label class='label ".$parc_class."'>
													<u>Contenance :</u> ".conv_are($info_lots[$l]['dcntlo'])."
												</label>
											</td>
											<td style='text-align:left;'>
												<label class='label ".$parc_class."'>
													<u>Surface à maitriser :</u> ".conv_are($info_lots[$l]['surf_mu'])."
												</label>";
											If ($info_lots[$l]['dcntlo'] > $info_lots[$l]['surf_mu']) {
												$result['code'] .= "<label class='lib_alerte'>pour partie</label>";
											}
											If ($info_lots[$l]['surf_mu_init'] > 0 && conv_are($info_lots[$l]['surf_mu']) != conv_are(round($info_lots[$l]['surf_mu_init'], 0))) {
												$result['code'] .= "
												<label class='label_simple ".$parc_class."'>
													(Surface initiale : ".conv_are(round($info_lots[$l]['surf_mu_init'], 0)).")
												</label>";
											}
											$result['code'] .= "	
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td id='parc_td_detail-".$info_lots[$l]['rel_saf_foncier_id']."' class='td_detail_parc $parc_class' style='display:".$parc_display_detail.";'>									
									<table class='no-border' width='100%'>
										<tr>
											<td class='no-border' width='100%'>
												<fieldset id='fieldset_parc_objectif' class='fieldset_af'>
													<legend>Objectifs et n&eacute;gociations</legend>
													<table style='border:0 solid black;width:100%'>
														<tr>
															<td class='no-border' style='text-align:left;width:50%;'>
																<label class='label'>
																	<u>Priorit&eacute; :</u>
																</label>
																<label class='btn_combobox' style='margin-left:5px;'>			
																	<select name='paf-".$info_lots[$l]['rel_saf_foncier_id']."' id='paf-".$info_lots[$l]['rel_saf_foncier_id']."' class='combobox paf' style='text-align:left' onchange=\"updateLot('paf-".$info_lots[$l]['rel_saf_foncier_id']."', ".$lst_eaf[$i]['entite_af_id'].", ".$session_af_id.", ".$lst_eaf[$i]['nbr_echanges'].");\">";																	
																		$rq_lst_paf = "SELECT d_paf.paf_id as id, d_paf.paf_lib as lib FROM animation_fonciere.d_paf";
																		$res_lst_paf = $bdd->prepare($rq_lst_paf);
																		$res_lst_paf->execute();
																		While ($donnees = $res_lst_paf->fetch()) {
																			$result['code'] .= "<option ";
																			If ($donnees['id'] == $info_lots[$l]['paf_id']) {
																				$result['code'] .= "selected ";
																			}
																			$result['code'] .= "value='$donnees[id]'>$donnees[lib]</option>";
																		}
																	$result['code'] .= "					
																	</select>
																</label>
															</td>
															<td class='no-border' style='text-align:right;width:50%;'>
																<label class='label'>
																	<u>Objectif interne :</u>
																</label>
																<label class='btn_combobox' style='margin-left:5px;'>			
																	<select name='obj-".$info_lots[$l]['rel_saf_foncier_id']."' id='obj-".$info_lots[$l]['rel_saf_foncier_id']."' class='combobox obj-".$lst_eaf[$i]['entite_af_id']."' style='text-align:left' onchange=\"updateLot('obj-".$info_lots[$l]['rel_saf_foncier_id']."', ".$lst_eaf[$i]['entite_af_id'].", ".$session_af_id.", ".$lst_eaf[$i]['nbr_echanges'].");\">";
																		
																		$rq_lst_objectif = "SELECT d_objectif_af.objectif_af_id as id, d_objectif_af.objectif_af_lib as lib FROM animation_fonciere.d_objectif_af ORDER BY objectif_af_id";
																		$res_lst_objectif = $bdd->prepare($rq_lst_objectif);
																		$res_lst_objectif->execute();
																		While ($lst_objectif = $res_lst_objectif->fetch()) {
																			If (substr($lst_objectif['lib'], -10) != substr($info_lots[$l]['convention'], -10)) {
																				$result['code'] .= "<option ";
																				If ($lst_objectif['id'] == $info_lots[$l]['objectif_af_id']) {
																					$result['code'] .= "selected ";
																				}
																				$result['code'] .= "value='$lst_objectif[id]'>$lst_objectif[lib]</option>";
																			}
																		}
																	$result['code'] .= "					
																	</select>
																</label>
															</td>
														</tr>
														<tr>
															<td class='no-border'>
																<label class='label' style='vertical-align:top;'>
																	<u>Négociations :</u> 
																</label>
																<label class='label_simple' style='vertical-align:top;'>"
																	.$info_lots[$l]['nego'].
																"</label>
															</td>
															<td class='no-border' style='text-align:right;width:50%;'>
																<label class='label_simple'><i>".$info_lots[$l]['convention']."</i></label>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr>
											<td class='no-border' width='100%'>
												<fieldset id='fieldset_parc_sol' class='fieldset_af'>
													<legend>Pratiques du sol</legend>";
														$rq_nat_cult = 
														"SELECT DISTINCT
															d_dsgrpf.natcult_ssgrp,
															d_dsgrpf.cgrnum,
															lots_natcult_cen.dcntsf as surf_nat_cult,
															CASE WHEN d_cnatsp.natcult_natspe IS NOT NULL THEN d_cnatsp.natcult_natspe ELSE 'N.D.' END as natcult_natspe,
															d_dsgrpf.dsgrpf
														FROM
															cadastre.lots_cen t2	
															JOIN cadastre.lots_natcult_cen USING (lot_id)
															LEFT JOIN cadastre.d_cnatsp USING (cnatsp)
															JOIN cadastre.d_dsgrpf USING (dsgrpf)
														WHERE t2.lot_id = ?
														ORDER BY surf_nat_cult DESC";
														$res_nat_cult = $bdd->prepare($rq_nat_cult);
														$res_nat_cult->execute(array($info_lots[$l]['lot_id']));					
														$nat_cult = $res_nat_cult->fetchall();
														
													$result['code'] .= "
													<table style='border:0 solid black;'>
														<tr>
															<td style='vertical-align:top'>
																<label class='label'><u>Natures de culture : </u></label> 
															</td>
															<td>
																<table CELLSPACING = '0' CELLPADDING ='0' style='border:0 solid black;margin-left:5px;'>";
															If ($res_nat_cult->rowCount() == 0) {
																$result['code'] .= "<td><label class='label'>N.D.</label></td>";
															} Else {
																For ($n=0; $n<$res_nat_cult->rowCount(); $n++) { 
																	$result['code'] .= "
																	<tr>
																		<td style='text-align:left'>
																			<label class='label'>
																				".retour_ligne($nat_cult[$n]['natcult_ssgrp'] . ' : ' . conv_are($nat_cult[$n]['surf_nat_cult']) . ' (' . round(($nat_cult[$n]['surf_nat_cult']*100)/$info_lots[$l]['dcntlo'], 0) . '%)', 40)."
																			</label>
																		</td>
																	</tr>";
																	If ($nat_cult[$n]['natcult_natspe'] != 'N.D.') { 
																	$result['code'] .= "
																	<tr>
																		<td style='text-align:left;'>
																			<label class='label' style='font-size:9pt;font-weight:normal;'>
																				".'(' . $nat_cult[$n]['natcult_natspe'] . ')'."
																			</label>
																		</td>
																	</tr>";
																	} 
																}
															} 
																$result['code'] .= "
																</table>
															</td>
														</tr>
														<tr>
															<td class='no-border'>
																<label class='label' style='vertical-align:top;'>
																	<u>Occupation du sol :</u> 
																</label>
															</td>
															<td>
																<table CELLSPACING = '0' CELLPADDING ='0' style='border:0;'>
																	<tr>
																		<td>
																			<table id='tbl_parc_occsol-".$info_lots[$l]['rel_saf_foncier_id']."' class='tbl_parc_occsol' CELLSPACING = '0' CELLPADDING ='0' style='border:0;'>";
																			$result['code'] .= fct_rq_occsol_parc($info_lots[$l]['rel_saf_foncier_id'], $session_af_id, $lst_eaf[$i]['entite_af_id'], $lst_eaf[$i]['nbr_echanges']);
																			$result['code'] .= "
																			</table>
																		</td>
																		<td>
																			<img id='img_save_occsol-".$info_lots[$l]['rel_saf_foncier_id']."' src='".ROOT_PATH."images/enregistrer.png' width='15px' style='cursor:auto;opacity:0.3;margin-left:10px;' onclick=''>	
																		</td>
																		<td>
																			<div class='lib_alerte' id='occsol_prob_saisie-".$info_lots[$l]['rel_saf_foncier_id']."' style='display:none;white-space:pre-wrap;padding:10px'>Veuillez v&eacute;rifier votre saisie</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='no-border'>
																<label class='label' style='vertical-align:top;'>
																	<u>Utilisation du sol :</u> 
																</label>
															</td>
															<td>
																<textarea onkeyup=\"resizeTextarea('utilissol-".$info_lots[$l]['rel_saf_foncier_id']."');isUpdatable(this, 'utilissol', ".$info_lots[$l]['rel_saf_foncier_id'].", ".$lst_eaf[$i]['entite_af_id'].", ".$session_af_id.", ".$lst_eaf[$i]['nbr_echanges'].");\" onchange=\"resizeTextarea('utilissol-".$info_lots[$l]['rel_saf_foncier_id']."');\" id='utilissol-".$info_lots[$l]['rel_saf_foncier_id']."' name='utilissol_".$info_lots[$l]['rel_saf_foncier_id']."' class='label textarea_com_edit' rows='2' cols='1000'>".$info_lots[$l]['utilissol']."</textarea>
																<input type='hidden' id='hid_utilissol-".$info_lots[$l]['rel_saf_foncier_id']."' value='".$info_lots[$l]['utilissol']."'>
															</td>
															<td>
																<img id='img_save_utilissol-".$info_lots[$l]['rel_saf_foncier_id']."' src='".ROOT_PATH."images/enregistrer.png' width='15px' style='cursor:auto;opacity:0.3;' onclick=''>	
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>	
										<tr>
											<td class='no-border' width='100%'>
												<fieldset id='fieldset_parc_prix-".$info_lots[$l]['rel_saf_foncier_id']."' class='fieldset_af' ";
												If ($lst_eaf[$i]['nbr_obj_acqu'] == 0) {
													$result['code'] .= "style='display:none;'";
												}
												$result['code'] .= ">
													<legend>Prix d'acquisition</legend>
													<div class='no-border' style='width:100%;text-align:right;'>
														<label id='lbl_prix_total_occsol-".$info_lots[$l]['rel_saf_foncier_id']."' class='label' style='vertical-align:top;'>
															<u>Prix estim&eacute; du fond &agrave; partir de l'occupation du sol :</u> ".number_format($info_lots[$l]['prix_fond_occsol'],2, '.', ' ')." €
														</label>
													</div>";
													$result['code'] .= "
													<table CELLSPACING = '0' CELLPADDING ='0' style='border:0;'>
														<tr>
															<td>
																<table id='tbl_parc_prix-".$info_lots[$l]['rel_saf_foncier_id']."' CELLSPACING = '0' CELLPADDING ='2' style='border:0;'>
																	<tr>
																		<th style='text-align:center;'>
																			<label class='last_maj'>Libellé</label>
																		</th>
																		<th style='text-align:center;'>
																			<label class='last_maj'>Montant</label>
																		</th>
																		<th style='text-align:center;'>
																			<label class='last_maj' style='text-align:center;'>Quantité</label>
																		</th>
																	</tr>";
																$result['code'] .= fct_rq_prix_parc($info_lots[$l]['rel_saf_foncier_id'], $session_af_id, $lst_eaf[$i]['entite_af_id'], $lst_eaf[$i]['nbr_echanges']);
																$result['code'] .= "
																	<tr>
																		<td colspan='4' style='text-align:right;'>
																			<label class='label'><u>Prix d'acquisition total opérateur :</u></label>
																		</td>
																		<td>
																			<input style='text-align:right;margin-left:10px;border:0' class='editbox parc_arrondi_prix' type='text' id='parc_arrondi_prix-".$info_lots[$l]['rel_saf_foncier_id']."' size='8' value='".$info_lots[$l]['prix_parc_arrondi']."' onkeyup=\"isUpdatable(this, 'prix', ".$info_lots[$l]['rel_saf_foncier_id'].", ".$lst_eaf[$i]['entite_af_id'].", ".$session_af_id.", ".$lst_eaf[$i]['nbr_echanges'].");\"><label class='label'>€</label>
																		</td>
																	</tr>
																</table>
															</td>
															<td>
																<img id='img_save_prix-".$info_lots[$l]['rel_saf_foncier_id']."' src='".ROOT_PATH."images/enregistrer.png' width='15px' style='cursor:auto;opacity:0.3;margin-left:10px;' onclick=''>	
															</td>
															<td>
																<div class='lib_alerte' id='prix_prob_saisie-".$info_lots[$l]['rel_saf_foncier_id']."' style='display:none;white-space:pre-wrap;padding:10px'>Veuillez v&eacute;rifier votre saisie</div>
															</td>
														</tr>
													</table>
												</fieldset>	
											</td>
										</tr>
										<tr>
											<td class='no-border' width='100%'>
												<fieldset id='fieldset_parc_divers' class='fieldset_af'>
													<legend>Divers</legend>
													<table style='border:0 solid black'>
														<tr>
															<td class='no-border' colspan='3'>";
															If ($info_lots[$l]['date_fin_cen'] == 'N.D.' && $info_lots[$l]['date_fin_site'] == 'N.D.') {
																$result['code'] .= "
																<input type='checkbox' id='lot_bloque-".$info_lots[$l]['rel_saf_foncier_id']."' class='chk_lot_bloque'"; If ($info_lots[$l]['lot_bloque'] == 'true') {$result['code'] .= " checked ";} $result['code'] .= " onclick=\"updateLot('lot_bloque-".$info_lots[$l]['rel_saf_foncier_id']."', ".$lst_eaf[$i]['entite_af_id'].", ".$session_af_id.", ".$lst_eaf[$i]['nbr_echanges'].");\">
																<label id='lbl_lot_bloque-".$info_lots[$l]['rel_saf_foncier_id']."' class='label' "; If ($info_lots[$l]['lot_bloque'] == 'true') {$result['code'] .= "style='color:#fe0000;'";} $result['code'] .= "><u>Lot bloqu&eacute;</u></label>";
															} Else {
																$result['code'] .= "
																<label id='lbl_lot_supprime-".$info_lots[$l]['rel_saf_foncier_id']."' class='label' style='color:#fe0000;'><u>Lot supprim&eacute; du cadastre</u></label>";
															}
															$result['code'] .= "
															</td>
														</tr>
														<tr>
															<td class='no-border'>
																<label class='label' style='vertical-align:top;'>
																	<u>Commentaires :</u> 
																</label>
															</td>
															<td>
																<textarea onkeyup=\"resizeTextarea('commentaires-".$info_lots[$l]['rel_saf_foncier_id']."');isUpdatable(this, 'commentaires', ".$info_lots[$l]['rel_saf_foncier_id'].", ".$lst_eaf[$i]['entite_af_id'].", ".$session_af_id.", ".$lst_eaf[$i]['nbr_echanges'].");\" onchange=\"resizeTextarea('commentaires-".$info_lots[$l]['rel_saf_foncier_id']."');\" id='commentaires-".$info_lots[$l]['rel_saf_foncier_id']."' name='commentaires-".$info_lots[$l]['rel_saf_foncier_id']."' class='label textarea_com_edit' rows='2' cols='1000'>".$info_lots[$l]['commentaires']."</textarea>
																<input type='hidden' id='hid_commentaires-".$info_lots[$l]['rel_saf_foncier_id']."' value='".$info_lots[$l]['commentaires']."'>
															</td>
															<td>
																<img id='img_save_commentaires-".$info_lots[$l]['rel_saf_foncier_id']."' src='".ROOT_PATH."images/enregistrer.png' width='15px' style='cursor:auto;opacity:0.3;' onclick=''>	
															</td>
														</tr>";
														
														$rq_lst_sessions = "
														SELECT DISTINCT
															session_af_id,
															session_af_lib													
														FROM 
															animation_fonciere.session_af
															JOIN animation_fonciere.rel_saf_foncier USING (session_af_id)
															JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)											
															JOIN foncier.cadastre_site t4 USING (cad_site_id)
														WHERE 
															session_af_id != :session_af_id AND
															cad_site_id = :cad_site_id";
														// echo $rq_lst_sessions;
														$res_lst_sessions = $bdd->prepare($rq_lst_sessions);
														$res_lst_sessions->execute(array('session_af_id'=>$session_af_id, 'cad_site_id'=>$info_lots[$l]['cad_site_id']));
														$lst_sessions = $res_lst_sessions->fetchall();
														If ($res_lst_sessions->rowCount() > 0) { 	
															$result['code'] .= "
															<tr>
																<td colspan='3'>
																	<table style='border:0 solid black'>
																		<tr>
																			<td>
																				<label class='label'>
																					Parcelle présente sur d'autres Sessions d'Animation Foncière :
																				</label>
																			</td>	
																			<td class='bilan-af' align='left'>";
																			For ($s=0; $s<$res_lst_sessions->rowCount(); $s++) { 	
																				$vget = "mode:consult;session_af_id:".$lst_sessions[$s]['session_af_id'].";session_af_lib:".$lst_sessions[$s]['session_af_lib'].";";
																				$vget_crypt = base64_encode($vget);
																				$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_saf.php?d=$vget_crypt#general";
																				$result['code'] .= "
																				<li>
																					<a class='label_simple' style='margin-left:10px' href='$lien'>".$lst_sessions[$s]['session_af_lib']."</a>
																				</li>";
																		
																			}
																			$result['code'] .= "
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>";
														}
													$result['code'] .= "	
													</table>
												</fieldset>	
											</td>
										</tr>";
									$result['code'] .= "
									</table>
								</td>
							</tr>";
						If ($l+1<$res_info_lots->rowCount()) {
							$result['code'] .= "
							<tr>
								<td style='border-bottom:1px dashed #004494;' colspan='3'>
									&nbsp;
								</td>
							</tr>";	
						}
				}
					$result['code'] .= "
					</table>
				</td>
			</tr>
			<tr id='tr_parc_comm_eaf-".$lst_eaf[$i]['entite_af_id']."'>
				<td colspan='2' style='border-color:#ccc;border-width:1px 4px 4px 4px;border-style:solid;'>
					<table class='no-border' cellspacing='0' cellpadding='2'>
						<tr>
							<td style='width:33%;text-align:center;'>
								<label class='label'><i>Commentaires sur l'EAF : </i></label>
							</td>
							<td style='text-align:center;white-space:nowrap;'>
								<textarea onkeyup=\"resizeTextarea('commentaires_eaf-".$lst_eaf[$i]['entite_af_id']."');isUpdatable(this, 'commentaires_eaf', 'ALL', ".$lst_eaf[$i]['entite_af_id'].", ".$session_af_id.", ".$lst_eaf[$i]['nbr_echanges'].")\" onchange=\"resizeTextarea('commentaires_eaf-".$lst_eaf[$i]['entite_af_id']."');\" id='commentaires_eaf-".$lst_eaf[$i]['entite_af_id']."' class='label textarea_com_edit' rows='1' cols='60'>".$lst_eaf[$i]['commentaires']."</textarea>								
							</td>
							<td style='width:15px;text-align:center;'>
								<img id='img_save_commentaires_eaf-".$lst_eaf[$i]['entite_af_id']."' src='".ROOT_PATH."images/enregistrer.png' width='15px' style='cursor:auto;opacity:0.3;' onclick=''>	
							</td>
						</tr>
					</table>
				</td>
			</tr>";
		}
		print json_encode($result);
	}
	
	// Cette fonction permet d'afficher le bilan des echanges d'une SAF
	// Elle permet également d'adapter la liste des echanges en fonction des filtres	
	function fct_create_lst_echanges($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$page = $data['params']['page']; // Cette variable récupère le nom de la page fiche d'origine
		$session_af_id = $data['params']['var_session_af_id']; // Cette variable récupère l'id de la SAF	
		$section = $data['params']['section'];
		$par_num = $data['params']['par_num'];
		$nom = $data['params']['nom'];
		$scroll_top = $data['params']['var_scroll_top'];
		$offset = $data['params']['offset'];
		$nbr_result = 600000;
					
		$rq_lst_eaf = "
		SELECT DISTINCT 
			entite_af_id,
			array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
			array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
			array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
			t1.nbr_echanges,
			CASE WHEN COALESCE(t_actif.nbr_actif, 0) = 0 AND COALESCE(t_bloque.nbr_lot_bloque, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) AND COALESCE(t_supprime.nbr_lot_supprime, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
				-1 
			ELSE
				CASE WHEN COALESCE(t_bloque.nbr_lot_bloque, 0) = count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) AND COALESCE(t_supprime.nbr_lot_supprime, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
					-2 
				ELSE
					CASE WHEN COALESCE(t_supprime.nbr_lot_supprime, 0) = count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
						-3
					ELSE 
						1 
					END 
				END
			END as ordre
		FROM 		
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			LEFT JOIN 
				(
					SELECT 
						entite_af_id,
						count(rel_saf_foncier_id) as nbr_lot_supprime
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN foncier.cadastre_site t4 USING (cad_site_id)
						JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
					WHERE
						session_af_id = :session_af_id AND
						(t3.date_fin != 'N.D.' OR t4.date_fin != 'N.D.')
					GROUP BY
						entite_af_id
				) as t_supprime USING (entite_af_id)
			LEFT JOIN 
				(
					SELECT 
						entite_af_id,
						count(rel_saf_foncier_id) as nbr_lot_bloque
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						session_af_id = :session_af_id AND
						lot_bloque = 't'
					GROUP BY
						entite_af_id
				) as t_bloque USING (entite_af_id)
			LEFT JOIN 
				(
					SELECT DISTINCT 
						entite_af_id,
						count(interloc_id) as nbr_actif
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					WHERE 
						session_af_id = :session_af_id AND
						etat_interloc_id = 1
					GROUP BY entite_af_id
				) t_actif USING (entite_af_id)	
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.lots_cen USING (lot_id)
			JOIN 
				(
					SELECT DISTINCT 
						entite_af_id,
						array_to_string(array_agg(
							CASE WHEN nom_usage = 'N.P.' THEN
								ddenom
							ELSE
								CASE WHEN nom_jeunefille = 'N.P.' THEN
									nom_usage
								ELSE
									nom_usage||' '||nom_jeunefille
								END
							END), ',') as tbl_eaf_ddenom,
						count(echange_af_id) as nbr_echanges
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
					WHERE
						session_af_id = :session_af_id	
					GROUP BY entite_af_id
				) t1 USING (entite_af_id)										
			
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND
			lot_id ILIKE '%".$section.'%'.$par_num."%'";
			
		If ($nom != '' && $nom != NULL) {
			$rq_lst_eaf .= " AND t1.tbl_eaf_ddenom ILIKE '%$nom%'";
		}
		$rq_lst_eaf .= "
		GROUP BY 
			entite_af_id,
			t1.nbr_echanges,
			t_bloque.nbr_lot_bloque,
			t_supprime.nbr_lot_supprime,
			t_actif.nbr_actif
		ORDER BY 
			ordre DESC, 
			entite_af_id DESC";										
		$res_lst_eaf = $bdd->prepare($rq_lst_eaf);
		$res_lst_eaf->execute(array('session_af_id'=>$session_af_id));
		$lst_eaf = $res_lst_eaf->fetchall();
		
		
		$result['code'] = '';
		
		For ($i=0; $i<$res_lst_eaf->rowCount(); $i++) {		
			$result['code'] .= "
			<tr>
				<td style='border-right: 1px solid #ccc; border-bottom: 4px solid #ccc'>
					<table CELLSPACING = '1' CELLPADDING ='0' class='no-border'>";
			$rq_info_lots = "
				SELECT DISTINCT
					t1.par_id,
					t1.ccosec,
					t1.dnupla,
					COALESCE(t2.dnulot, 'N.D.') as dnulot,
					lot_bloque::text,
					t3.date_fin as date_fin_cen,
					t4.date_fin as date_fin_site
				FROM
					cadastre.parcelles_cen t1
					JOIN cadastre.lots_cen t2 USING (par_id)
					JOIN cadastre.cadastre_cen t3 USING (lot_id)
					JOIN foncier.cadastre_site t4 USING (cad_cen_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				WHERE 
					entite_af_id = :entite_af_id AND 
					rel_saf_foncier.session_af_id = :session_af_id
				ORDER BY par_id";
			$res_info_lots = $bdd->prepare($rq_info_lots);
			$res_info_lots->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
			$info_lots = $res_info_lots->fetchall();
			For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 
				If ($info_lots[$l]['lot_bloque'] == 'true') {
					$parc_class = 'lot_bloque';
				} Else If ($info_lots[$l]['date_fin_cen'] != 'N.D.' || $info_lots[$l]['date_fin_site'] != 'N.D.') {
					$parc_class = 'lot_supprime';
					$parc_img_btn = 'plus.png';
					$parc_display_detail = 'none';
				} Else {
					$parc_class = '';
				}
						$result['code'] .= "
						<tr>
							<td class='no-border $parc_class' style='margin-top:5px;'>
								<label class='label'>
									".$info_lots[$l]['ccosec']."-".$info_lots[$l]['dnupla'];
									If ($info_lots[$l]['dnulot'] != 'N.D.') {
										$result['code'] .= "-" . $info_lots[$l]['dnulot'];
									}
									$result['code'] .= "
								</label>
							</td>
						</tr>";		
			}
			
			$result['code'] .= "
					</table>
				</td>			
				<td style='border-bottom: 4px solid #ccc'>
					<table CELLSPACING = '1' CELLPADDING ='0' class='no-border' width='100%'>";
			$rq_info_interlocs = "
			SELECT DISTINCT
				interlocuteurs.interloc_id,
				d_etat_interloc.etat_interloc_id, 
				d_etat_interloc.etat_interloc_lib,
				interlocuteurs.gtoper,
				interlocuteurs.dnuper,
				d_ccoqua.ccoqua as particule_id,
				d_ccoqua.particule_lib,
				interlocuteurs.ddenom,
				interlocuteurs.nom_usage,
				COALESCE(interlocuteurs.nom_jeunefille, 'N.D.') as nom_jeunefille,
				interlocuteurs.prenom_usage,
				COALESCE(interlocuteurs.nom_conjoint, 'N.D.') as nom_conjoint,
				COALESCE(interlocuteurs.prenom_conjoint, 'N.D.') as prenom_conjoint,
				COALESCE(REPLACE(interlocuteurs.jdatnss, '-', ''), 'N.D.') as jdatnss,
				CASE WHEN jdatnss IS NOT NULL AND jdatnss != 'N.P.' AND jdatnss != 'N.D.' THEN EXTRACT(YEAR FROM age(date(jdatnss))) || ' ans' ELSE 'N.D.' END age_interloc,
				COALESCE(interlocuteurs.dldnss, 'N.D.') as dldnss,
				COALESCE(interlocuteurs.dlign3, 'N.D.') as dlign3,
				COALESCE(interlocuteurs.dlign4, 'N.D.') as dlign4,
				COALESCE(interlocuteurs.dlign5, 'N.D.') as dlign5,
				COALESCE(interlocuteurs.dlign6, 'N.D.') as dlign6,
				COALESCE(interlocuteurs.email, 'N.D.') as email,
				COALESCE(interlocuteurs.fixe_domicile, 'N.D.') as fixe_domicile,
				COALESCE(interlocuteurs.fixe_travail, 'N.D.') as fixe_travail,
				COALESCE(interlocuteurs.portable_domicile, 'N.D.') as portable_domicile,
				COALESCE(interlocuteurs.portable_travail, 'N.D.') as portable_travail,
				COALESCE(interlocuteurs.fax, 'N.D.') as fax,
				d_type_interloc.type_interloc_id, 
				d_type_interloc.type_interloc_lib, 
				d_type_interloc.can_nego::text, 
				d_type_interloc.avis_favorable::text, 
				d_groupe_interloc.groupe_interloc_id, 
				d_groupe_interloc.groupe_interloc_color, 
				d_groupe_interloc.can_be_subst::text,
				d_groupe_interloc.can_be_under_subst::text,
				interlocuteurs.gtoper,
				array_to_string(array_agg(DISTINCT rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id), '_') as tbl_rel_saf_foncier_interlocs_id,
				CASE WHEN ref1.interloc_id IS NOT NULL THEN 
					'1ref:'||ref1.interloc_id || '-a-' || CASE WHEN ref1.interloc_id = ref1.interloc_ref_id THEN 0 ELSE ref1.interloc_ref_id END
				ELSE
					CASE WHEN ref2.interloc_id IS NOT NULL THEN 
						'1ref:'||ref2.interloc_ref_id || '-b-' || ref2.interloc_id
					ELSE
						'2notref:'||interlocuteurs.interloc_id || '-a-1'
					END
				END as ordre
			FROM 
				animation_fonciere.interlocuteurs 
				JOIN animation_fonciere.d_etat_interloc USING (etat_interloc_id)
				LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
				JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.session_af USING (session_af_id)
				LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)									
				LEFT JOIN 
					(
						SELECT DISTINCT
							st1.interloc_id,
							rel_saf_foncier_interlocs.interloc_id as interloc_ref_id,
							rel_saf_foncier_interlocs.type_interloc_id as type_interloc_ref_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN
								(
									SELECT DISTINCT 
										rel_saf_foncier_interlocs.interloc_id, 
										interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
									FROM 
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id

									UNION

									SELECT DISTINCT 
										t_temp_ref.interloc_id, 
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id as foncier_interloc_ref_id
									FROM 
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
										JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (t_temp_ref.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id

									UNION

									SELECT DISTINCT 
										rel_saf_foncier_interlocs.interloc_id, 
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
									FROM 
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
										JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id =  interloc_substitution.rel_saf_foncier_interlocs_id_vp)
									WHERE 
										groupe_interloc_id = 3 AND
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id
								) st1 ON (st1.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id)
					) ref1 ON (ref1.interloc_ref_id = rel_saf_foncier_interlocs.interloc_id AND ref1.type_interloc_ref_id = rel_saf_foncier_interlocs.type_interloc_id)
				LEFT JOIN 
					(
						SELECT
							st2.interloc_id,
							rel_saf_foncier_interlocs.interloc_id as interloc_ref_id,
							rel_saf_foncier_interlocs.type_interloc_id as type_interloc_ref_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN
								(
									SELECT DISTINCT 
										rel_saf_foncier_interlocs.interloc_id, 
										interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
									FROM 
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id
										
									UNION

									SELECT DISTINCT
										t_subst.interloc_id,
										t_ref.rel_saf_foncier_interlocs_id
									FROM
										(
											SELECT DISTINCT
												rel_saf_foncier_interlocs.interloc_id,
												rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
												rel_saf_foncier_interlocs_id_vp
											FROM
												animation_fonciere.interlocuteurs
												JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
												JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
												JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
												JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
											WHERE 
												groupe_interloc_id != 3 AND
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id
										) t_subst
										JOIN
										(
											SELECT DISTINCT
												rel_saf_foncier_interlocs.interloc_id,
												rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
												interloc_referent.rel_saf_foncier_interlocs_id as under_ref
											FROM
												animation_fonciere.interlocuteurs
												JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
												JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
											WHERE 
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id
										) t_ref ON (t_subst.rel_saf_foncier_interlocs_id_vp = t_ref.under_ref)	
								) st2 ON st2.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
					) ref2 ON (ref2.interloc_id = rel_saf_foncier_interlocs.interloc_id AND ref2.type_interloc_ref_id = rel_saf_foncier_interlocs.type_interloc_id)
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id 
				AND entite_af_id = :entite_af_id 
				AND groupe_interloc_id != 3
				AND rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN 
					(
						SELECT DISTINCT 
							rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN 
								(
									SELECT
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
										rel_saf_foncier_interlocs.interloc_id,
										rel_saf_foncier_interlocs.type_interloc_id
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
										JOIN animation_fonciere.d_type_interloc ON (t_temp_subst.type_interloc_id = d_type_interloc.type_interloc_id)
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id AND
										groupe_interloc_id != 3
								) t1 ON (t1.interloc_id = rel_saf_foncier_interlocs.interloc_id AND t1.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							session_af_id = :session_af_id AND 
							entite_af_id = :entite_af_id
					)
				AND rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN
					(
						SELECT DISTINCT 
							rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)

						WHERE
							can_be_subst = 't' AND
							rel_saf_foncier_interlocs_id NOT IN
								(
									SELECT rel_saf_foncier_interlocs_id_is
									FROM animation_fonciere.interloc_substitution
								)
					)
			GROUP BY 
				interlocuteurs.interloc_id, 
				d_etat_interloc.etat_interloc_id, 
				d_etat_interloc.etat_interloc_lib, 
				gtoper,
				d_ccoqua.ccoqua,
				d_ccoqua.particule_lib, 
				d_type_interloc.type_interloc_id,
				d_type_interloc.type_interloc_lib,
				d_type_interloc.can_nego, 
				d_type_interloc.avis_favorable, 
				d_groupe_interloc.groupe_interloc_id, 
				d_groupe_interloc.groupe_interloc_color, 
				d_groupe_interloc.can_be_subst, 
				d_groupe_interloc.can_be_under_subst, 
				ordre
			ORDER BY ordre, groupe_interloc_id, type_interloc_id DESC
			";
			// echo $rq_info_interlocs;
			$res_info_interlocs = $bdd->prepare($rq_info_interlocs);
			$res_info_interlocs->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
			$lst_interlocs = $res_info_interlocs->fetchall();			
							$result['code'] .= 
							"<tr>
								<td>
									<table class='no-border' cellspacing='0' cellpadding='5' style='border-collapse:collapse;' width='100%'>";
								For ($j=0; $j<$res_info_interlocs->rowCount(); $j++) {
									$interloc_lib[$lst_interlocs[$j]['interloc_id']] = str_replace('   ', ' ', proprio_lib($lst_interlocs[$j]['particule_lib'], $lst_interlocs[$j]['nom_usage'], $lst_interlocs[$j]['prenom_usage'], $lst_interlocs[$j]['ddenom']));
									If (substr_count($lst_interlocs[$j]['ordre'], '1ref:') > 0 && substr_count($lst_interlocs[$j]['ordre'], '-a-0') > 0 && substr_count($lst_interlocs[$j]['ordre'], '2notref') == 0) {
										$referent[$lst_interlocs[$j]['interloc_id']] = 'oui';
									} Else {
										$referent[$lst_interlocs[$j]['interloc_id']] = 'non';
									}		
									$tbl_index_same_lines = array_keys(array_values(array_column($lst_interlocs, 'ordre')), $lst_interlocs[$j]['ordre']);
									$show_nego = 'false';
									$id_table_lst_type_interloc = array();
									$id_table_lst_type_interloc_cant_be_subst = array();
									For ($index_type = 0; $index_type < count($tbl_index_same_lines); $index_type++) {
										array_push($id_table_lst_type_interloc, $lst_interlocs[$tbl_index_same_lines[$index_type]]['type_interloc_id']);	
										If ($lst_interlocs[$tbl_index_same_lines[$index_type]]['can_be_subst'] == 'false') {
											array_push($id_table_lst_type_interloc_cant_be_subst, $lst_interlocs[$tbl_index_same_lines[$index_type]]['type_interloc_id']);
										}
										If ($lst_interlocs[$tbl_index_same_lines[$index_type]]['can_nego'] == 'true' || $lst_interlocs[$tbl_index_same_lines[$index_type]]['avis_favorable'] == 'true') {
											$show_nego = 'true';
										}
									}
									sort($id_table_lst_type_interloc);
									sort($id_table_lst_type_interloc_cant_be_subst);
									
									If ($j == 0 || (isset($lst_interlocs[$j-1]['ordre']) && $lst_interlocs[$j-1]['ordre'] != $lst_interlocs[$j]['ordre'])) {
										$result['code'] .= 
										"<tr>
											<td width='30%' style='vertical-align:top;"; 
											
											If (($referent[$lst_interlocs[$j]['interloc_id']] == 'oui') || ($j>0 && substr_count($lst_interlocs[$j-1]['ordre'], '2notref') == 0 && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-')))) {
													$result['code'] .= "border-top:4px solid #f0c08d; ";
											} ElseIf ($res_info_interlocs->rowCount() > 1 && $j>0) {
												$result['code'] .= "border-top: solid #004494 1px; ";
											} 							
											
											If (($referent[$lst_interlocs[$j]['interloc_id']] == 'oui') || substr_count($lst_interlocs[$j]['ordre'], '2notref') == 0 && ((($j>0  && isset($lst_interlocs[$j-1]['ordre']) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) == substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-')))) || (isset($lst_interlocs[$j+1]['ordre']) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j+1]['ordre'], 0, strpos($lst_interlocs[$j+1]['ordre'], '-'))))) 
												{$result['code'] .= "border-left:4px solid #f0c08d; ";} 
											
											If (substr_count($lst_interlocs[$j]['ordre'], '2notref') == 0 && (($j+1==$res_info_interlocs->rowCount() && $lst_interlocs[$j]['ordre'] != '' && isset($lst_interlocs[$j-1]['ordre']) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) == substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-'))) || (isset($lst_interlocs[$j+1]['ordre'] ) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j+1]['ordre'], 0, strpos($lst_interlocs[$j+1]['ordre'], '-'))))) 
												{$result['code'] .= "border-bottom:4px solid #f0c08d;";}
											
											If (substr_count($lst_interlocs[$j]['ordre'], '-b-') > 0) 
												{$result['code'] .= "padding-top:20px;padding-left:40px;";}
											If ($lst_interlocs[$j]['etat_interloc_id'] == 1) {	
												$color_font_interloc = $lst_interlocs[$j]['groupe_interloc_color'];
											} Else {
												$color_font_interloc = '#ccc';
											}
											$result['code'] .= "'>
												<table id='tbl_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' class='no-border'>";	
													$result['code'] .= "
													<tr id='tr_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."'>
														<td style='vertical-align:top;'>";
														If ($lst_interlocs[$j]['etat_interloc_id'] == 1 && $lst_eaf[$i]['ordre'] == 1) {
															$result['code'] .= "
															<img width='30px' style='cursor:pointer;margin:-10px 0;' src='".ROOT_PATH."foncier/module_af/images/echange.png' onclick=\"showFormEchange(this, '".str_replace("'", "\'", $interloc_lib[$lst_interlocs[$j]['interloc_id']])."', '";
															If ($lst_interlocs[$j]['nom_usage'] != 'N.P.') {
																$result['code'] .= str_replace("'", "\'", $lst_interlocs[$j]['nom_usage']);
															} Else {
																$result['code'] .= str_replace("'", "\'", $lst_interlocs[$j]['ddenom']);
															}
															$result['code'] .= "', '".$referent[$lst_interlocs[$j]['interloc_id']]."' , '".$lst_interlocs[$j]['interloc_id']."', '".$show_nego."', ".$session_af_id.", '".$lst_eaf[$i]['entite_af_id']."', 'N.D.', 'false', 'tbl_echange_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".implode('-', array_unique($id_table_lst_type_interloc))."', '".$lst_interlocs[$j]['gtoper']."');\" onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"Saisir un &eacute;change\">";
														}	
															$result['code'] .= "
															<label class='label ";
															If ($referent[$lst_interlocs[$j]['interloc_id']] == 'oui') {
																$result['code'] .= "referent";
															}
															$result['code'] .= "' style='color:".$color_font_interloc.";'>
																".$interloc_lib[$lst_interlocs[$j]['interloc_id']]." 
															</label>";
															If ($lst_interlocs[$j]['age_interloc'] <> 'N.D.') { 
																$result['code'] .= "
																<label class='label_simple' style='color:".$color_font_interloc.";'>(".$lst_interlocs[$j]['age_interloc'].")</label>";
															}
															
															$result['code'] .= "
															<img id='echange_btn_detail-".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs[$j]['type_interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' src='".ROOT_PATH."images/plus.png' width='12px' style='cursor:pointer;margin-left:3px;' onclick=\"showDetail('echange', '".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs[$j]['type_interloc_id']."_".$lst_eaf[$i]['entite_af_id']."');\">
														</td>
													</tr>";
												If ($lst_interlocs[$j]['etat_interloc_id'] != 1) {
													$result['code'] .= "
													<tr>
														<td>
															<label class='last_maj' style='color:red;'>	
																(".$lst_interlocs[$j]['etat_interloc_lib'].")
															</label>
														</td>
													</tr>";
												}
													$result['code'] .=
													"<tr>
														<td id='echange_td_detail-".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs[$j]['type_interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' style='display:none;'>
															<table align='CENTER' CELLSPACING='0' CELLPADDING='0' class='no-border'>";
															If ($lst_interlocs[$j]['dldnss'] <> 'N.P.' && $lst_interlocs[$j]['dldnss'] <> 'N.D.') { 
																$result['code'] .= 
																"<tr>
																	<td>
																		<label class='label_simple'>
																			Né"; If ($lst_interlocs[$j]['particule_id'] > 1) {$result['code'] .= 'e';}
																			$result['code'] .= " 
																			&agrave; ".$lst_interlocs[$j]['dldnss']."
																		</label>
																	</td>
																</tr>";
															}				
															If ($lst_interlocs[$j]['nom_conjoint'] <> 'N.P.' && $lst_interlocs[$j]['nom_conjoint'] <> 'N.D.') { 
																$result['code'] .= 
																"<tr>
																	<td>
																		<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>
																			Epou"; If ($lst_interlocs[$j]['particule_id'] == 1) {$result['code'] .= 'x';} ElseIf ($lst_interlocs[$j]['particule_id'] > 1) {$result['code'] .= 'se';} Else {$result['code'] .= 'x/Epouse';} 
																			$result['code'] .= " de ".$lst_interlocs[$j]['nom_conjoint'].' ';
																			If ($lst_interlocs[$j]['prenom_conjoint'] <> 'N.P.' && $lst_interlocs[$j]['prenom_conjoint'] <> 'N.D.') {
																				$prenom_conjoint = ucwords(strtolower($lst_interlocs[$j]['prenom_conjoint']));
																				Foreach (array('-', '\'') as $delimiter) {
																					If (strpos($prenom_conjoint, $delimiter)!==false) {
																						$prenom_conjoint = implode($delimiter, array_map('ucfirst', explode($delimiter, $prenom_conjoint)));
																					}
																				}
																			} Else {
																				$prenom_conjoint = '';
																			}
																			$result['code'] .= $prenom_conjoint."
																		</label>
																	</td>
																</tr>";
															}
															If ($lst_interlocs[$j]['nom_jeunefille'] <> 'N.P.' && $lst_interlocs[$j]['nom_jeunefille'] <> 'N.D.') {
																$result['code'] .=
																"<tr>
																	<td>
																		<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>
																			Nom de jeune fille : ".$lst_interlocs[$j]['nom_jeunefille']."
																		</label>
																	</td>
																</tr>";
															}
															
																$result['code'] .=
																"<tr>
																	<td>
																		<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px;padding-left:50px'>
																			<tr>
																				<td>";
																				If ($lst_interlocs[$j]['dlign3'] != 'N.D.' Or $lst_interlocs[$j]['dlign4'] != 'N.D.' Or $lst_interlocs[$j]['dlign5'] != 'N.D.' Or $lst_interlocs[$j]['dlign6'] != 'N.D.') {
																					$result['code'] .=
																					"<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>
																						<tr>
																							<td rowspan='4' style='vertical-align:top;'>
																								<img src='".ROOT_PATH."images/adresse.gif' width='20px'>
																							</td>
																							<td style='text-align:right;'>";
																							If($lst_interlocs[$j]['dlign3'] != 'N.D.') { 
																								$result['code'] .=
																								"<label class='label_simple'style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['dlign3']."</label>";
																							} 
																							$result['code'] .=
																							"</td>
																						</tr>
																						<tr>
																							<td style='text-align:right;'>";
																							If($lst_interlocs[$j]['dlign4'] != 'N.D.') { 
																								$result['code'] .=
																								"<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['dlign4']."</label>";
																							}
																							$result['code'] .=
																							"</td>	
																						</tr>
																						<tr>
																							<td style='text-align:right;'>";
																							If($lst_interlocs[$j]['dlign5'] != 'N.D.') { 
																								$result['code'] .=
																								"<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['dlign5']."</label>";
																							}
																							$result['code'] .=
																							"</td>	
																						</tr>
																						<tr>
																							<td style='text-align:right;'>";
																							If($lst_interlocs[$j]['dlign6'] != 'N.D.') {
																								$result['code'] .=
																								"<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['dlign6']."</label>";
																							}
																							$result['code'] .=
																							"</td>	
																						</tr>
																					</table>";
																				}	
																				$result['code'] .=
																				"</td>
																			</tr>
																		</table>
																	</td>
																</tr>";
															If ($lst_interlocs[$j]['fixe_domicile'] != 'N.D.' Or $lst_interlocs[$j]['fixe_travail'] != 'N.D.' Or $lst_interlocs[$j]['portable_domicile'] != 'N.D.' Or $lst_interlocs[$j]['portable_travail'] != 'N.D.' Or $lst_interlocs[$j]['fax'] != 'N.D.') {
																$result['code'] .=
																"<tr>
																	<td>
																		<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>";
																		If($lst_interlocs[$j]['fixe_domicile'] != 'N.D.') {
																			$result['code'] .=
																			"<tr>
																				<td style='text-align:right'>
																					<img src='".ROOT_PATH."images/tel_fix.gif' width='20px'>
																				</td>
																				<td>
																					<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['fixe_domicile']." (perso)</label>
																				</td>	
																			</tr>";
																		} 
																		If($lst_interlocs[$j]['fixe_travail'] != 'N.D.') {
																			$result['code'] .=
																			"<tr>
																				<td style='text-align:right'>
																					<img src='".ROOT_PATH."images/tel_fix.gif' width='20px'>
																				</td>
																				<td style='padding-bottom:8px;'>
																					<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['fixe_travail']." (pro)</label>
																				</td>	
																			</tr>";
																		} 
																		If($lst_interlocs[$j]['portable_domicile'] != 'N.D.') {
																			$result['code'] .=
																			"<tr>
																				<td style='text-align:right'>
																					<img src='".ROOT_PATH."images/tel_portable.png' width='12px'>
																				</td>
																				<td>
																					<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['portable_domicile']." (perso)</label>
																				</td>	
																			</tr>";
																		} 
																		If($lst_interlocs[$j]['portable_travail'] != 'N.D.') {
																			$result['code'] .=
																			"<tr>
																				<td style='text-align:right;padding-bottom:8px;'>
																					<img src='".ROOT_PATH."images/tel_portable.png' width='12px'>
																				</td>
																				<td style='padding-bottom:8px;'>
																					<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['portable_travail']." (pro)</label>
																				</td>	
																			</tr>";
																		} 
																		If($lst_interlocs[$j]['fax'] != 'N.D.') {
																			$result['code'] .=
																			"<tr>
																				<td style='text-align:right'>
																					<img src='".ROOT_PATH."images/fax.png' width='20px'>
																				</td>
																				<td>
																					<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['fax']."</label>
																				</td>	
																			</tr>";
																		}
																		$result['code'] .=
																		"</table>
																	</td>
																</tr>";
															}						 
															If($lst_interlocs[$j]['email'] != 'N.D.') {
																$result['code'] .= "
																<tr>
																	<td>
																		<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>
																			<tr>
																				<td style='vertical-align:middle;padding-top:20px;'>
																					<img src='".ROOT_PATH."images/mail.png' width='20px'>
																				</td>
																				<td style='vertical-align:middle;padding-top:20px;'>
																					<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['email']."</label>
																				</td>
																				<td style='vertical-align:middle;padding-top:25px;'>
																					<a href='https://mail.espaces-naturels.fr/zimbra/?view=compose&to=".$lst_interlocs[$j]['email']."#1' class='label_simple' target='_blank'><img src='".ROOT_PATH."images/send_mail.png' width='20px'></a>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>";
															}
															$result['code'] .= "
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table class='no-border' CELLSPACING = '0' CELLPADDING ='0'>";
													For ($index_type = 0; $index_type < count($tbl_index_same_lines); $index_type++) {
														If ($lst_interlocs[$tbl_index_same_lines[$index_type]]['can_be_subst'] == 'false') {
															$rq_info_representant = "
															SELECT DISTINCT
																interlocuteurs.interloc_id,
																interlocuteurs.gtoper,
																interlocuteurs.dnuper,
																d_ccoqua.particule_lib,
																d_ccoqua.ccoqua as particule_id,
																interlocuteurs.ddenom,
																interlocuteurs.nom_usage,
																COALESCE(interlocuteurs.nom_jeunefille, 'N.D.') as nom_jeunefille,
																interlocuteurs.prenom_usage,
																COALESCE(interlocuteurs.nom_conjoint, 'N.D.') as nom_conjoint,
																COALESCE(interlocuteurs.prenom_conjoint, 'N.D.') as prenom_conjoint,
																COALESCE(REPLACE(interlocuteurs.jdatnss, '-', ''), 'N.D.') as jdatnss,
																CASE WHEN jdatnss IS NOT NULL AND jdatnss != 'N.P.' AND jdatnss != 'N.D.' THEN EXTRACT(YEAR FROM age(date(jdatnss))) || ' ans' ELSE 'N.D.' END age_interloc,
																COALESCE(interlocuteurs.dldnss, 'N.D.') as dldnss,
																COALESCE(interlocuteurs.dlign3, 'N.D.') as dlign3,
																COALESCE(interlocuteurs.dlign4, 'N.D.') as dlign4,
																COALESCE(interlocuteurs.dlign5, 'N.D.') as dlign5,
																COALESCE(interlocuteurs.dlign6, 'N.D.') as dlign6,
																COALESCE(interlocuteurs.email, 'N.D.') as email,
																COALESCE(interlocuteurs.fixe_domicile, 'N.D.') as fixe_domicile,
																COALESCE(interlocuteurs.fixe_travail, 'N.D.') as fixe_travail,
																COALESCE(interlocuteurs.portable_domicile, 'N.D.') as portable_domicile,
																COALESCE(interlocuteurs.portable_travail, 'N.D.') as portable_travail,
																COALESCE(interlocuteurs.fax, 'N.D.') as fax,	
																d_type_interloc.type_interloc_id,
																d_type_interloc.type_interloc_lib,
																d_type_interloc.can_nego::text,
																d_type_interloc.avis_favorable::text,
																d_groupe_interloc.groupe_interloc_id,
																d_groupe_interloc.groupe_interloc_color,
																array_to_string(array_agg(DISTINCT rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id), '_') as tbl_rel_saf_foncier_interlocs_id,
																count(rel_af_echanges.echange_af_id) as nbr_echanges
															FROM 
																animation_fonciere.interlocuteurs 
																LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
																JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
																JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
																JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
																JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
																JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
																JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
																LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
															WHERE 
																rel_saf_foncier.session_af_id = :session_af_id AND 
																entite_af_id = :entite_af_id AND
																groupe_interloc_id = 3 AND
																interloc_substitution.rel_saf_foncier_interlocs_id_vp IN (".str_replace('_', ',', $lst_interlocs[$tbl_index_same_lines[$index_type]]['tbl_rel_saf_foncier_interlocs_id']).")
															GROUP BY 
																interlocuteurs.interloc_id, 
																interlocuteurs.gtoper, 
																d_ccoqua.particule_lib,
																particule_id,
																d_type_interloc.type_interloc_id,
																d_type_interloc.type_interloc_lib,
																d_type_interloc.can_nego,
																d_type_interloc.avis_favorable,
																d_groupe_interloc.groupe_interloc_id,
																d_groupe_interloc.groupe_interloc_color";
															// echo $rq_info_representant;
															$res_info_representant = $bdd->prepare($rq_info_representant);
															$res_info_representant->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
															
															$result['code'] .= "
																<tr>
																	<td style='padding-left:40px;'>
																		<label class='label_simple' style='color:".$lst_interlocs[$tbl_index_same_lines[$index_type]]['groupe_interloc_color'].";'>
																			".$lst_interlocs[$tbl_index_same_lines[$index_type]]['type_interloc_lib'];
																		If ($res_info_representant->rowCount() > 0) {
																			$result['code'] .= " représenté par";
																		}
																		$result['code'] .= "
																		</label>
																	</td>
																</tr>";
																			
															If ($res_info_representant->rowCount() > 0) {
																$result['code'] .= "
																<tr>
																	<td style='padding-left:10px;'>
																		<table class='no-border' CELLSPACING = '0' CELLPADDING ='0' style='margin-left:50px;'>";
																		$lst_representant = $res_info_representant->fetchall();
																		For ($r=0; $r<$res_info_representant->rowCount(); $r++) { 	
																			$tbl_rel_saf_foncier_interlocs_id = array_unique(explode('_', $lst_representant[$r]['tbl_rel_saf_foncier_interlocs_id']));
																			sort($tbl_rel_saf_foncier_interlocs_id);
																			$representant_lib[implode('-', $tbl_rel_saf_foncier_interlocs_id)] = str_replace('   ', ' ', proprio_lib($lst_representant[$r]['particule_lib'], $lst_representant[$r]['nom_usage'], $lst_representant[$r]['prenom_usage'], $lst_representant[$r]['ddenom']));
																			$representant_id[implode('-', $tbl_rel_saf_foncier_interlocs_id)] = $lst_representant[$r]['interloc_id'];
																			$representant_gtoper[implode('-', $tbl_rel_saf_foncier_interlocs_id)] = $lst_representant[$r]['gtoper'];
																			$representant_nom_usage[implode('-', $tbl_rel_saf_foncier_interlocs_id)] = $lst_representant[$r]['nom_usage'];
																			$representant_ddenom[implode('-', $tbl_rel_saf_foncier_interlocs_id)] = $lst_representant[$r]['ddenom'];
																			$result['code'] .= 
																			"<tr>
																				<td rowspan='2' style='vertical-align:top;'>";
																				If ($lst_eaf[$i]['ordre'] == '1') {
																					$result['code'] .=  "
																					<img width='30px' style='cursor:pointer;' src='".ROOT_PATH."foncier/module_af/images/echange.png' onclick=\"showFormEchange(this, '".str_replace("'", "\'", $representant_lib[implode('-', $tbl_rel_saf_foncier_interlocs_id)])."', '";
																					If ($lst_representant[$r]['nom_usage'] != 'N.P.') {
																						$result['code'] .= str_replace("'", "\'", $lst_representant[$r]['nom_usage']);
																					} Else {
																						$result['code'] .= str_replace("'", "\'", $lst_representant[$r]['ddenom']);
																					}
																					$result['code'] .= "', '".$referent[$lst_interlocs[$j]['interloc_id']]."', '".$lst_representant[$r]['interloc_id']."', '".$show_nego."', ".$session_af_id.", '".$lst_eaf[$i]['entite_af_id']."', 'N.D.', 'true', 'tbl_echange_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".implode('-', array_unique($id_table_lst_type_interloc))."', '".$lst_representant[$r]['gtoper']."');\" onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"Saisir un &eacute;change\">";
																				}
																				$result['code'] .= "
																				</td>
																				<td style='vertical-align:top;'>
																					<label class='label' style='color:".$lst_representant[$r]['groupe_interloc_color'].";'>
																						".$representant_lib[implode('-', $tbl_rel_saf_foncier_interlocs_id)]." 
																					</label>";
																					If ($lst_representant[$r]['age_interloc'] <> 'N.D.') { 
																						$result['code'] .= "
																						<label class='label_simple' style='color:".$lst_representant[$r]['groupe_interloc_color'].";'>(".$lst_representant[$r]['age_interloc'].")</label>";
																					}	
																					$result['code'] .= "
																					<img id='echange_btn_detail-".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs[$tbl_index_same_lines[$index_type]]['type_interloc_id']."_".$lst_representant[$r]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' src='".ROOT_PATH."images/plus.png' width='12px' style='cursor:pointer;' onclick=\"showDetail('echange', '".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs[$tbl_index_same_lines[$index_type]]['type_interloc_id']."_".$lst_representant[$r]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."');\">
																				</td>";
																				$result['code'] .= "
																				";				 										
																			 $result['code'] .= "	
																			</tr>
																			<tr>
																				<td id='echange_td_detail-".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs[$tbl_index_same_lines[$index_type]]['type_interloc_id']."_".$lst_representant[$r]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' style='display:none;'>
																					<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:10px;border:0px;float:none;'>";
																					If ($lst_representant[$r]['dldnss'] <> 'N.P.' && $lst_representant[$r]['dldnss'] <> 'N.D.') { 
																						$result['code'] .= 
																						"<tr>
																							<td>
																								<label class='label_simple'>
																									Né"; If ($lst_representant[$r]['particule_id'] > 1) {$result['code'] .= 'e';}
																									$result['code'] .= " 
																									&agrave; ".$lst_representant[$r]['dldnss']."
																								</label>
																							</td>
																						</tr>";
																					}								
																					If ($lst_representant[$r]['nom_conjoint'] <> 'N.P.' && $lst_representant[$r]['nom_conjoint'] <> 'N.D.') { 
																						$result['code'] .= 
																						"<tr>
																							<td>
																								<label class='label_simple'>
																									Epou"; If ($lst_representant[$r]['particule_id'] == 1) {$result['code'] .= 'x';} ElseIf ($lst_representant[$r]['particule_id'] > 1) {$result['code'] .= 'se';} Else {$result['code'] .= 'x/Epouse';} 
																									$result['code'] .= " de ".$lst_representant[$r]['nom_conjoint'].' ';
																									If ($lst_representant[$r]['prenom_conjoint'] <> 'N.P.' && $lst_representant[$r]['prenom_conjoint'] <> 'N.D.') {
																										$prenom_conjoint = ucwords(strtolower($lst_representant[$r]['prenom_conjoint']));
																										Foreach (array('-', '\'') as $delimiter) {
																											If (strpos($prenom_conjoint, $delimiter)!==false) {
																												$prenom_conjoint = implode($delimiter, array_map('ucfirst', explode($delimiter, $prenom_conjoint)));
																											}
																										}
																									} Else {
																										$prenom_conjoint = '';
																									}
																									$result['code'] .= $prenom_conjoint."
																								</label>
																							</td>
																						</tr>";
																					}
																					If ($lst_representant[$r]['nom_jeunefille'] <> 'N.P.' && $lst_representant[$r]['nom_jeunefille'] <> 'N.D.') {
																						$result['code'] .=
																						"<tr>
																							<td>
																								<label class='label_simple'>
																									Nom de jeune fille : ".$lst_representant[$r]['nom_jeunefille']."
																								</label>
																							</td>
																						</tr>";
																					}
																						$result['code'] .=
																						"<tr>
																							<td>
																								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px;padding-left:50px'>
																									<tr>
																										<td>";
																										If ($lst_representant[$r]['dlign3'] != 'N.D.' Or $lst_representant[$r]['dlign4'] != 'N.D.' Or $lst_representant[$r]['dlign5'] != 'N.D.' Or $lst_representant[$r]['dlign6'] != 'N.D.') {
																											$result['code'] .=
																											"<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>
																												<tr>
																													<td rowspan='4' style='vertical-align:top;'>
																														<img src='".ROOT_PATH."images/adresse.gif' width='20px'>
																													</td>
																													<td style='text-align:right;'>";
																													If($lst_representant[$r]['dlign3'] != 'N.D.') { 
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_representant[$r]['dlign3']."</label>";
																													} 
																													$result['code'] .=
																													"</td>
																												</tr>
																												<tr>
																													<td style='text-align:right;'>";
																													If($lst_representant[$r]['dlign4'] != 'N.D.') { 
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_representant[$r]['dlign4']."</label>";
																													}
																													$result['code'] .=
																													"</td>	
																												</tr>
																												<tr>
																													<td style='text-align:right;'>";
																													If($lst_representant[$r]['dlign5'] != 'N.D.') { 
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_representant[$r]['dlign5']."</label>";
																													}
																													$result['code'] .=
																													"</td>	
																												</tr>
																												<tr>
																													<td style='text-align:right;'>";
																													If($lst_representant[$r]['dlign6'] != 'N.D.') {
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_representant[$r]['dlign6']."</label>";
																													}
																													$result['code'] .=
																													"</td>	
																												</tr>
																											</table>";
																										}	
																										$result['code'] .=
																										"</td>
																									</tr>
																								</table>
																							</td>";
																						If($lst_representant[$r]['fixe_domicile'] != 'N.D.' Or $lst_representant[$r]['fixe_travail'] != 'N.D.' Or $lst_representant[$r]['portable_domicile'] != 'N.D.' Or $lst_representant[$r]['portable_travail'] != 'N.D.' Or $lst_representant[$r]['fax'] != 'N.D.') {
																							$result['code'] .=
																							"<td>
																								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>";
																								If($lst_representant[$r]['fixe_domicile'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/tel_fix.gif' width='20px'>
																										</td>
																										<td>
																											<label class='label_simple'>".$lst_representant[$r]['fixe_domicile']." (perso)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_representant[$r]['fixe_travail'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/tel_fix.gif' width='20px'>
																										</td>
																										<td style='padding-bottom:8px;'>
																											<label class='label_simple'>".$lst_representant[$r]['fixe_travail']." (pro)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_representant[$r]['portable_domicile'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/tel_portable.png' width='12px'>
																										</td>
																										<td>
																											<label class='label_simple'>".$lst_representant[$r]['portable_domicile']." (perso)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_representant[$r]['portable_travail'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right;padding-bottom:8px;'>
																											<img src='".ROOT_PATH."images/tel_portable.png' width='12px'>
																										</td>
																										<td style='padding-bottom:8px;'>
																											<label class='label_simple'>".$lst_representant[$r]['portable_travail']." (pro)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_representant[$r]['fax'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/fax.png' width='20px'>
																										</td>
																										<td>
																											<label class='label_simple'>".$lst_representant[$r]['fax']."</label>
																										</td>	
																									</tr>";
																								}
																								$result['code'] .=
																								"</table>
																							</td>";
																						}
																						$result['code'] .=
																						"</tr>";										 
																					If($lst_representant[$r]['email'] != 'N.D.') {
																						$result['code'] .=
																						"<tr>
																							<td>
																								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>
																									<tr>
																										<td style='vertical-align:middle;padding-top:20px;'>
																											<img src='".ROOT_PATH."images/mail.png' width='20px'>
																										</td>
																										<td style='vertical-align:middle;padding-top:20px;'>
																											<label class='label_simple'>".$lst_representant[$r]['email']."</label>
																										</td>
																										<td style='vertical-align:middle;padding-top:25px;'>
																											<a href='https://mail.espaces-naturels.fr/zimbra/?view=compose&to=".$lst_representant[$r]['email']."#1' class='label_simple' target='_blank'><img src='".ROOT_PATH."images/send_mail.png' width='20px'></a>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>";
																					}
																					$result['code'] .= "
																					</table>
																				</td>
																			</tr>"; 
																		}
																$result['code'] .= "
																		</table>
																	</td>
																</tr>";
															}
														}
													}
													$result['code'] .= "
															</table>
														</td>
													</tr>";
									}
											If ($lst_interlocs[$j]['can_be_subst'] == 'true' || $lst_interlocs[$j]['can_be_under_subst'] == 'true') {					
												$rq_info_interlocs_subst = "
												SELECT DISTINCT
													interlocuteurs.interloc_id,
													interlocuteurs.dnuper,
													d_ccoqua.particule_lib,
													d_ccoqua.ccoqua as particule_id,
													interlocuteurs.ddenom,
													interlocuteurs.nom_usage,
													COALESCE(interlocuteurs.nom_jeunefille, 'N.D.') as nom_jeunefille,
													interlocuteurs.prenom_usage,
													COALESCE(interlocuteurs.nom_conjoint, 'N.D.') as nom_conjoint,
													COALESCE(interlocuteurs.prenom_conjoint, 'N.D.') as prenom_conjoint,
													COALESCE(REPLACE(interlocuteurs.jdatnss, '-', ''), 'N.D.') as jdatnss,
													CASE WHEN jdatnss IS NOT NULL AND jdatnss != 'N.P.' AND jdatnss != 'N.D.' THEN EXTRACT(YEAR FROM age(date(jdatnss))) || ' ans' ELSE 'N.D.' END age_interloc,
													COALESCE(interlocuteurs.dldnss, 'N.D.') as dldnss,
													COALESCE(interlocuteurs.dlign3, 'N.D.') as dlign3,
													COALESCE(interlocuteurs.dlign4, 'N.D.') as dlign4,
													COALESCE(interlocuteurs.dlign5, 'N.D.') as dlign5,
													COALESCE(interlocuteurs.dlign6, 'N.D.') as dlign6,
													COALESCE(interlocuteurs.email, 'N.D.') as email,
													COALESCE(interlocuteurs.fixe_domicile, 'N.D.') as fixe_domicile,
													COALESCE(interlocuteurs.fixe_travail, 'N.D.') as fixe_travail,
													COALESCE(interlocuteurs.portable_domicile, 'N.D.') as portable_domicile,
													COALESCE(interlocuteurs.portable_travail, 'N.D.') as portable_travail,
													COALESCE(interlocuteurs.fax, 'N.D.') as fax,	
													d_type_interloc.type_interloc_id,
													d_type_interloc.type_interloc_lib,
													d_type_interloc.can_nego::text,
													d_type_interloc.avis_favorable::text,
													d_groupe_interloc.groupe_interloc_id,
													d_groupe_interloc.groupe_interloc_color,
													array_to_string(array_agg(DISTINCT rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id), '_') as tbl_rel_saf_foncier_interlocs_id,
													count(rel_af_echanges.echange_af_id) as nbr_echanges
												FROM 
													animation_fonciere.interlocuteurs 
													LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
													JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
													JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
													JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													JOIN animation_fonciere.session_af USING (session_af_id)
													LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
												WHERE 
													rel_saf_foncier.session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id AND
													rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id IN (
														SELECT 
															rel_saf_foncier_interlocs_id_vp 
														FROM
															animation_fonciere.interloc_substitution
															JOIN animation_fonciere.rel_saf_foncier_interlocs ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
															JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
															JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
														WHERE
															rel_saf_foncier_interlocs_id_is IN (".str_replace('_', ',', $lst_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id']).")	
														)
												GROUP BY 
													interlocuteurs.interloc_id, 
													d_ccoqua.particule_lib,
													particule_id,
													d_type_interloc.type_interloc_id,
													d_type_interloc.type_interloc_lib,
													d_type_interloc.can_nego,
													d_type_interloc.avis_favorable,
													d_groupe_interloc.groupe_interloc_id,
													d_groupe_interloc.groupe_interloc_color
												ORDER BY
													interloc_id	";
												
												$res_info_interlocs_subst = $bdd->prepare($rq_info_interlocs_subst);
												$res_info_interlocs_subst->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
																					
												If ($res_info_interlocs_subst->rowCount() > 0) {													
													$lst_interlocs_subst = $res_info_interlocs_subst->fetchall();
													$result['code'] .= 
													"<tr>
														<td style='padding-top:0;padding-left:40px;'>
															<table class='no-border' CELLSPACING='0' CELLSPACING='0'>
																<tr>
																	<td>
																		<label class='label_simple' style='color:".$lst_interlocs[$j]['groupe_interloc_color'].";'>".$lst_interlocs[$j]['type_interloc_lib']." de :</label>
																	</td>
																</tr>";
														For ($s=0; $s<$res_info_interlocs_subst->rowCount(); $s++) { 
															If ($s == 0 || (isset($lst_interlocs_subst[$s-1]['interloc_id']) && $lst_interlocs_subst[$s-1]['interloc_id'] != $lst_interlocs_subst[$s]['interloc_id'])) {
																$interloc_lib[$lst_interlocs_subst[$s]['interloc_id']] = str_replace('   ', ' ', proprio_lib($lst_interlocs_subst[$s]['particule_lib'], $lst_interlocs_subst[$s]['nom_usage'], $lst_interlocs_subst[$s]['prenom_usage'], $lst_interlocs_subst[$s]['ddenom']));
																$result['code'] .= 
																"<tr>
																	<td style='vertical-align:top;'>
																		<table id='tbl_".$lst_interlocs_subst[$s]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' class='no-border'  CELLSPACING='0' CELLSPACING='0'>
																			<tr>
																				<td style='vertical-align:top;padding-top:0;padding-left:40px;'>
																					<label class='label' style='color:".$lst_interlocs_subst[$s]['groupe_interloc_color'].";'>
																						".$interloc_lib[$lst_interlocs_subst[$s]['interloc_id']]." 
																					</label>";
																					If ($lst_interlocs_subst[$s]['age_interloc'] <> 'N.D.') { 
																						$result['code'] .= "
																						<label class='label_simple' style='color:".$lst_interlocs_subst[$s]['groupe_interloc_color'].";'>(".$lst_interlocs_subst[$s]['age_interloc'].")</label>";
																					}	
																					$result['code'] .= "
																					<img id='echange_btn_detail-".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs_subst[$s]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' src='".ROOT_PATH."images/plus.png' width='12px' style='cursor:pointer;' onclick=\"showDetail('echange', '".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs_subst[$s]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."');\">
																				</td>
																			</tr>
																			<tr>
																				<td style='vertical-align:top;padding-top:0;padding-left:50px;'>";
																				$tbl_type_interloc_subst_index = array_keys(array_values(array_column($lst_interlocs_subst, 'interloc_id')), $lst_interlocs_subst[$s]['interloc_id']);
																				For ($index_type_subst = 0; $index_type_subst < count($tbl_type_interloc_subst_index); $index_type_subst++) {
																					$result['code'] .= "
																					<label class='label_simple' style='color:".$lst_interlocs_subst[$tbl_type_interloc_subst_index[$index_type_subst]]['groupe_interloc_color'].";'>
																						".$lst_interlocs_subst[$tbl_type_interloc_subst_index[$index_type_subst]]['type_interloc_lib']."
																					</label>
																					<br>";
																				}
																				$result['code'] .= "
																				</td>
																			</tr>
																			<tr>
																				<td id='echange_td_detail-".$lst_interlocs[$j]['interloc_id']."_".$lst_interlocs_subst[$s]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' style='display:none;'>
																					<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:10px;border:0px;float:none;'>";
																					If ($lst_interlocs_subst[$s]['dldnss'] <> 'N.P.' && $lst_interlocs_subst[$s]['dldnss'] <> 'N.D.') { 
																						$result['code'] .= 
																						"<tr>
																							<td>
																								<label class='label_simple'>
																									Né"; If ($lst_interlocs_subst[$s]['particule_id'] > 1) {$result['code'] .= 'e';}
																									$result['code'] .= " 
																									&agrave; ".$lst_interlocs_subst[$s]['dldnss']."
																								</label>
																							</td>
																						</tr>";
																					}
																					
																					If ($lst_interlocs_subst[$s]['nom_conjoint'] <> 'N.P.' && $lst_interlocs_subst[$s]['nom_conjoint'] <> 'N.D.') { 
																						$result['code'] .= 
																						"<tr>
																							<td>
																								<label class='label_simple'>
																									Epou"; If ($lst_interlocs_subst[$s]['particule_id'] == 1) {$result['code'] .= 'x';} ElseIf ($lst_interlocs_subst[$s]['particule_id'] > 1) {$result['code'] .= 'se';} Else {$result['code'] .= 'x/Epouse';} 
																									$result['code'] .= " de ".$lst_interlocs_subst[$s]['nom_conjoint'].' ';
																									If ($lst_interlocs_subst[$s]['prenom_conjoint'] <> 'N.P.' && $lst_interlocs_subst[$s]['prenom_conjoint'] <> 'N.D.') {
																										$prenom_conjoint = ucwords(strtolower($lst_interlocs_subst[$s]['prenom_conjoint']));
																										Foreach (array('-', '\'') as $delimiter) {
																											If (strpos($prenom_conjoint, $delimiter)!==false) {
																												$prenom_conjoint = implode($delimiter, array_map('ucfirst', explode($delimiter, $prenom_conjoint)));
																											}
																										}
																									} Else {
																										$prenom_conjoint = '';
																									}
																									$result['code'] .= $prenom_conjoint."
																								</label>
																							</td>
																						</tr>";
																					}
																					If ($lst_interlocs_subst[$s]['nom_jeunefille'] <> 'N.P.' && $lst_interlocs_subst[$s]['nom_jeunefille'] <> 'N.D.') {
																						$result['code'] .=
																						"<tr>
																							<td>
																								<label class='label_simple'>
																									Nom de jeune fille : ".$lst_interlocs_subst[$s]['nom_jeunefille']."
																								</label>
																							</td>
																						</tr>";
																					}
																						$result['code'] .=
																						"<tr>
																							<td>
																								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px;padding-left:50px'>
																									<tr>
																										<td>";
																										If ($lst_interlocs_subst[$s]['dlign3'] != 'N.D.' Or $lst_interlocs_subst[$s]['dlign4'] != 'N.D.' Or $lst_interlocs_subst[$s]['dlign5'] != 'N.D.' Or $lst_interlocs_subst[$s]['dlign6'] != 'N.D.') {
																											$result['code'] .=
																											"<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>
																												<tr>
																													<td rowspan='4' style='vertical-align:top;'>
																														<img src='".ROOT_PATH."images/adresse.gif' width='20px'>
																													</td>
																													<td style='text-align:right;'>";
																													If($lst_interlocs_subst[$s]['dlign3'] != 'N.D.') { 
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_interlocs_subst[$s]['dlign3']."</label>";
																													} 
																													$result['code'] .=
																													"</td>
																												</tr>
																												<tr>
																													<td style='text-align:right;'>";
																													If($lst_interlocs_subst[$s]['dlign4'] != 'N.D.') { 
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_interlocs_subst[$s]['dlign4']."</label>";
																													}
																													$result['code'] .=
																													"</td>	
																												</tr>
																												<tr>
																													<td style='text-align:right;'>";
																													If($lst_interlocs_subst[$s]['dlign5'] != 'N.D.') { 
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_interlocs_subst[$s]['dlign5']."</label>";
																													}
																													$result['code'] .=
																													"</td>	
																												</tr>
																												<tr>
																													<td style='text-align:right;'>";
																													If($lst_interlocs_subst[$s]['dlign6'] != 'N.D.') {
																														$result['code'] .=
																														"<label class='label_simple'>".$lst_interlocs_subst[$s]['dlign6']."</label>";
																													}
																													$result['code'] .=
																													"</td>	
																												</tr>
																											</table>";
																										}	
																										$result['code'] .=
																										"</td>
																									</tr>
																								</table>
																							</td>";
																						If($lst_interlocs_subst[$s]['fixe_domicile'] != 'N.D.' Or $lst_interlocs_subst[$s]['fixe_travail'] != 'N.D.' Or $lst_interlocs_subst[$s]['portable_domicile'] != 'N.D.' Or $lst_interlocs_subst[$s]['portable_travail'] != 'N.D.' Or $lst_interlocs_subst[$s]['fax'] != 'N.D.') {
																							$result['code'] .=
																							"<td>
																								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>";
																								If($lst_interlocs_subst[$s]['fixe_domicile'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/tel_fix.gif' width='20px'>
																										</td>
																										<td>
																											<label class='label_simple'>".$lst_interlocs_subst[$s]['fixe_domicile']." (perso)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_interlocs_subst[$s]['fixe_travail'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/tel_fix.gif' width='20px'>
																										</td>
																										<td style='padding-bottom:8px;'>
																											<label class='label_simple'>".$lst_interlocs_subst[$s]['fixe_travail']." (pro)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_interlocs_subst[$s]['portable_domicile'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/tel_portable.png' width='12px'>
																										</td>
																										<td>
																											<label class='label_simple'>".$lst_interlocs_subst[$s]['portable_domicile']." (perso)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_interlocs_subst[$s]['portable_travail'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right;padding-bottom:8px;'>
																											<img src='".ROOT_PATH."images/tel_portable.png' width='12px'>
																										</td>
																										<td style='padding-bottom:8px;'>
																											<label class='label_simple'>".$lst_interlocs_subst[$s]['portable_travail']." (pro)</label>
																										</td>	
																									</tr>";
																								} 
																								If($lst_interlocs_subst[$s]['fax'] != 'N.D.') {
																									$result['code'] .=
																									"<tr>
																										<td style='text-align:right'>
																											<img src='".ROOT_PATH."images/fax.png' width='20px'>
																										</td>
																										<td>
																											<label class='label_simple'>".$lst_interlocs_subst[$s]['fax']."</label>
																										</td>	
																									</tr>";
																								}
																								$result['code'] .=
																								"</table>
																							</td>";
																						}
																						$result['code'] .=
																						"</tr>";										 
																					If($lst_interlocs_subst[$s]['email'] != 'N.D.') {
																						$result['code'] .=
																						"<tr>
																							<td>
																								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style='padding-top:0px;border:0px'>
																									<tr>
																										<td style='vertical-align:middle;padding-top:20px;'>
																											<img src='".ROOT_PATH."images/mail.png' width='20px'>
																										</td>
																										<td style='vertical-align:middle;padding-top:20px;'>
																											<label class='label_simple'>".$lst_interlocs_subst[$s]['email']."</label>
																										</td>
																										<td style='vertical-align:middle;padding-top:25px;'>
																											<a href='https://mail.espaces-naturels.fr/zimbra/?view=compose&to=".$lst_interlocs_subst[$s]['email']."#1' class='label_simple' target='_blank'><img src='".ROOT_PATH."images/send_mail.png' width='20px'></a>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>";
																					}
																					$result['code'] .= "
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>";
															}
														}
														$result['code'] .=										
															"</table>
														</td>
													</tr>";
												}
											}
												
									If ($j+1==$res_info_interlocs->rowCount() || (isset($lst_interlocs[$j+1]['ordre']) && $lst_interlocs[$j+1]['ordre'] != $lst_interlocs[$j]['ordre'])) {			
												$result['code'] .= "	
												</table>
											</td>";												
																							
											$rq_echanges = "
											SELECT DISTINCT
												'echange' as categorie,
												interloc_id,
												representant,
												echange_af_id,
												echange_af_date,
												echange_af_description,
												echange_referent::text,
												CASE WHEN nbr_representant > 0 THEN 'true' ELSE 'false' END echange_representant,
												type_echanges_af_lib,
												date_recontact,
												commentaires,
												rappel_traite,
												echange_direct,
												result_echange_af_id,
												result_echange_af_lib,
												array_to_string(array_agg(st2.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,	
												array_to_string(array_agg(DISTINCT t_representant.tbl_rel_saf_foncier_interlocs_id_is), '-') as tbl_rel_saf_foncier_representant_id,		
												array_to_string(array_agg(t1.ccosec || t1.dnupla ORDER BY lot_id), ',') as tbl_par_lib,
												array_to_string(array_agg(COALESCE(t2.dnulot, 'N.D.') ORDER BY lot_id), ',') as tbl_dnulot,
												array_to_string(array_agg(COALESCE(d_nego_af.nego_af_lib, 'N.D.') ORDER BY lot_id), ',') as tbl_resultat,
												array_to_string(array_agg(COALESCE(nego_af_prix, -1) ORDER BY lot_id), ',') as tbl_resultat_prix
											FROM
												animation_fonciere.echanges_af
												LEFT JOIN animation_fonciere.d_result_echanges_af USING (result_echange_af_id)
												JOIN animation_fonciere.d_types_echanges_af USING (type_echanges_af_id)
												JOIN
													(
														SELECT DISTINCT
															count(rel_saf_foncier_representant_id) as nbr_representant,
															CASE WHEN groupe_interloc_id = 3 THEN 'true' ELSE 'false' END representant,
															rel_af_echanges.date_recontact,
															rel_af_echanges.commentaires,
															rel_af_echanges.rappel_traite,
															rel_af_echanges.echange_direct,
															rel_af_echanges.nego_af_prix,
															echange_af_id,
															nego_af_id,
															rel_saf_foncier_id,
															interloc_id,
															rel_saf_foncier_representant_id,
															array_to_string(array_agg(type_interloc_id ORDER BY type_interloc_id), ',') as lst_type_interloc_id
														FROM
															animation_fonciere.rel_af_echanges
															JOIN (
																SELECT 
																	rel_saf_foncier_interlocs_id,
																	interloc_id,
																	rel_saf_foncier_id,
																	type_interloc_id,
																	groupe_interloc_id
																FROM
																	animation_fonciere.rel_saf_foncier_interlocs
																	JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
																	JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
																	JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
																WHERE
																	interloc_id = :interloc_id AND 
																	session_af_id = :session_af_id AND
																	entite_af_id = :entite_af_id 
																	AND type_interloc_id IN (".implode(',', array_unique($id_table_lst_type_interloc)).")

																UNION

																SELECT 
																	rel_saf_foncier_interlocs_id_vp,
																	interloc_id,
																	rel_saf_foncier_id,
																	type_interloc_id,
																	groupe_interloc_id
																FROM
																	animation_fonciere.rel_saf_foncier_interlocs
																	JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
																	JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
																	JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
																	JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
																WHERE
																	interloc_id = :interloc_id AND 
																	session_af_id = :session_af_id AND
																	entite_af_id = :entite_af_id
																	AND type_interloc_id IN (".implode(',', array_unique($id_table_lst_type_interloc)).")
															) rel_saf_foncier_interlocs  USING (rel_saf_foncier_interlocs_id)
														GROUP BY
															representant,
															rel_af_echanges.date_recontact,
															rel_af_echanges.commentaires,
															rel_af_echanges.rappel_traite,
															rel_af_echanges.echange_direct,
															rel_af_echanges.nego_af_prix,
															echange_af_id,
															nego_af_id,
															rel_saf_foncier_id,
															interloc_id,
															rel_saf_foncier_representant_id
													) st1 USING (echange_af_id)
												LEFT JOIN animation_fonciere.d_nego_af USING (nego_af_id)
												JOIN 
													(
														SELECT DISTINCT
															rel_saf_foncier_id,
															rel_eaf_foncier_id
														FROM
															animation_fonciere.rel_saf_foncier
															JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
															JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
														WHERE
															interloc_id = :interloc_id AND 
															session_af_id = :session_af_id AND
															entite_af_id = :entite_af_id
															AND type_interloc_id IN (".implode(',', array_unique($id_table_lst_type_interloc)).")
													) st2 USING (rel_saf_foncier_id)

												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN foncier.cadastre_site t4 USING (cad_site_id) 
												JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
												JOIN cadastre.lots_cen t2 USING (lot_id)
												JOIN cadastre.parcelles_cen t1 USING (par_id)
												LEFT JOIN
												 (
													SELECT 
														rel_saf_foncier_interlocs.interloc_id,
														array_to_string(array_agg(DISTINCT rel_saf_foncier_interlocs_id_is), '-') as tbl_rel_saf_foncier_interlocs_id_is
													FROM
														animation_fonciere.rel_saf_foncier_interlocs
														JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
														JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
														JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp
											= rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
														JOIN animation_fonciere.rel_saf_foncier_interlocs t_representant_subst ON (t_representant_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
														JOIN animation_fonciere.d_type_interloc d_type_representant ON (d_type_representant.type_interloc_id = t_representant_subst.type_interloc_id)
													WHERE
														rel_saf_foncier_interlocs.interloc_id = :interloc_id AND 
														session_af_id = :session_af_id AND
														entite_af_id = :entite_af_id AND 
														rel_saf_foncier_interlocs.type_interloc_id IN (".implode(',', array_unique($id_table_lst_type_interloc)).") AND
														groupe_interloc_id = 3
													GROUP BY
														rel_saf_foncier_interlocs.interloc_id
												 ) t_representant USING (interloc_id)
											WHERE 
												interloc_id = :interloc_id AND 
												entite_af_id = :entite_af_id
											GROUP BY
												categorie,
												interloc_id,
												representant,
												echange_representant,
												echange_af_id,
												echange_af_date,
												echange_af_description,
												echange_referent,
												type_echanges_af_lib,
												date_recontact,
												commentaires,
												rappel_traite,
												echange_direct,
												result_echange_af_id,
												result_echange_af_lib
												
											UNION

											SELECT DISTINCT
												'evenement' as categorie,
												interloc_id,
												'false' as representant,
												evenement_af_id as echange_af_id,
												evenement_af_date as echange_af_date,
												evenement_af_description as echange_af_description,
												'false' as echange_referent,
												'false' as echange_representant,
												type_evenements_af_lib as type_echanges_af_lib,
												'N.D.' as date_recontact,
												'N.D.' as commentaires,
												FALSE as rappel_traite,
												FALSE as echange_direct,
												0 as result_echange_af_id,
												NULL as result_echange_af_lib,
												NULL as tbl_rel_saf_foncier_id,	
												NULL as tbl_rel_saf_foncier_representant_id,		
												NULL as tbl_par_lib,
												NULL as tbl_dnulot,
												'N.D.' as tbl_resultat,
												NULL as tbl_resultat_prix
											FROM
												animation_fonciere.evenements_af
												JOIN animation_fonciere.d_types_evenements_af USING (type_evenements_af_id)
												JOIN animation_fonciere.rel_af_evenements USING (evenement_af_id)
											WHERE 
												interloc_id = :interloc_id AND
												session_af_id = :session_af_id											
												
											ORDER BY 
												echange_af_date DESC,
												echange_af_id";
											// echo $rq_echanges;
																						
											$res_echanges = $bdd->prepare($rq_echanges);
											$res_echanges->execute(array('interloc_id'=>$lst_interlocs[$j]['interloc_id'], 'session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
											$lst_echanges = $res_echanges->fetchall();												
																						
											$result['code'] .= "
											<td width='70%' style='"; 
											If ($res_info_interlocs->rowCount() > 1 && $j>0 && $res_echanges->rowCount() > 1 || ($j+1 < $res_info_interlocs->rowCount())) {											
												$result['code'] .= "border-bottom: solid #004494 1px; ";
											} 				
											If ($referent[$lst_interlocs[$j]['interloc_id']] == 'oui') 
												{$result['code'] .= "border-top:4px solid #f0c08d;border-right:4px solid #f0c08d; ";} 
											
											If (substr_count($lst_interlocs[$j]['ordre'], '2notref') == 0 && ((($j>0  && isset($lst_interlocs[$j-1]['ordre']) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) == substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-')))) || (isset($lst_interlocs[$j+1]['ordre']) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j+1]['ordre'], 0, strpos($lst_interlocs[$j+1]['ordre'], '-'))))) 
												{$result['code'] .= "border-right:4px solid #f0c08d; ";} 
											
											If (substr_count($lst_interlocs[$j]['ordre'], '2notref') == 0 && (($j+1==$res_info_interlocs->rowCount() && $lst_interlocs[$j]['ordre'] != '' && isset($lst_interlocs[$j-1]['ordre']) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) == substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-'))) || (isset($lst_interlocs[$j+1]['ordre'] ) && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j+1]['ordre'], 0, strpos($lst_interlocs[$j+1]['ordre'], '-'))))) 
												{$result['code'] .= "border-bottom:4px solid #f0c08d;";}
											
											$result['code'] .= "'>
												<table class='no-border' id='tbl_echange_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".implode('-', array_unique($id_table_lst_type_interloc))."'>";
												
												For ($e=0; $e<$res_echanges->rowCount(); $e++) {	
													$tbl_rel_saf_foncier_representant_id = array_unique(explode('-', $lst_echanges[$e]['tbl_rel_saf_foncier_representant_id']));
													sort($tbl_rel_saf_foncier_representant_id);
												
													$rq_echange_referent = "
													SELECT DISTINCT
														interloc_id
													FROM
														animation_fonciere.rel_saf_foncier_interlocs
														JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)											
													WHERE
														rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id IN (	
															SELECT DISTINCT
																rel_saf_foncier_interlocs_id
															FROM 
																animation_fonciere.rel_saf_foncier_interlocs
																JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
															WHERE
																echange_af_id = :echange_af_id
															)";
													$res_echange_referent = $bdd->prepare($rq_echange_referent);
													$res_echange_referent->execute(array('echange_af_id'=>$lst_echanges[$e]['echange_af_id']));
													$echange_referent = $res_echange_referent->fetch();
													
													If ($e == 0 || (isset($lst_echanges[$e-1]['echange_af_id']) && $lst_echanges[$e-1]['echange_af_id'] != $lst_echanges[$e]['echange_af_id'])) {
														$result['code'] .= "
														<tr id='tr_echange_info_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".$lst_echanges[$e]['echange_af_id']."' class='tr_echange_".$lst_echanges[$e]['echange_af_id']."'>
															<td style='padding-top:10px;vertical-align:top;text-align:right;'>
																<label class='label'>".date_transform($lst_echanges[$e]['echange_af_date'])."<br>".$lst_echanges[$e]['type_echanges_af_lib']."</label>";
																If ($lst_echanges[$e]['result_echange_af_id'] == 1 || $lst_echanges[$e]['result_echange_af_id'] == 2) {
																	$result['code'] .= "
																	<br>
																	<label class='label' style='color:#fe0000;'>".$lst_echanges[$e]['result_echange_af_lib']."</label>";
																}
																$result['code'] .="																
															</td>
															<td style='padding-top:10px;vertical-align:top;text-align:right;'>";
															If ($lst_echanges[$e]['echange_af_description'] != 'N.D.') {
																$result['code'] .="
																<textarea id='description_echange_".$lst_echanges[$e]['echange_af_id']."_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' class='label textarea_com_edit' cols='120' rows='1' style='font-weight:normal;min-height:5px;' READONLY>".$lst_echanges[$e]['echange_af_description']."</textarea>";
															}																
															If ($lst_echanges[$e]['echange_representant'] == 'true') {
																$result['code'] .= "
																<br>
																<label class='label_simple'>Echange avec ".$representant_lib[implode('-', $tbl_rel_saf_foncier_representant_id)]."</label>";
															} ElseIf ($lst_echanges[$e]['echange_referent'] == 'true' && $echange_referent['interloc_id'] != $lst_echanges[$e]['interloc_id']) {
																$result['code'] .= "
																<br>
																<label class='label_simple'>Echange avec le référent</label>";
															}											
															$result['code'] .= "	
															</td>
															<td style='width:20px;padding-top:10px;vertical-align:middle;text-align:right;'>";
															If ($lst_echanges[$e]['categorie'] == 'echange') {
																If ($res_echange_referent->rowCount() == 0 || $echange_referent['interloc_id'] == $lst_echanges[$e]['interloc_id']) {
																	If ($lst_echanges[$e]['echange_representant'] == 'false') {
																		$result['code'] .= "
																		<img width='15px' style='cursor:pointer;padding-left:5px;' src='".ROOT_PATH."images/edit.png' onclick=\"showFormEchange(this, '".str_replace("'", "\'", $interloc_lib[$lst_echanges[$e]['interloc_id']])."', '";
																		If ($lst_interlocs[$j]['nom_usage'] != 'N.P.') {
																			$result['code'] .= str_replace("'", "\'", $lst_interlocs[$j]['nom_usage']);
																		} Else {
																			$result['code'] .= str_replace("'", "\'", $lst_interlocs[$j]['ddenom']);
																		}
																		$result['code'] .= "', '".$referent[$lst_echanges[$e]['interloc_id']]."', ".$lst_echanges[$e]['interloc_id'].", '".$show_nego."', ".$session_af_id.", ".$lst_eaf[$i]['entite_af_id'].", ".$lst_echanges[$e]['echange_af_id'].", 'false', 'tbl_echange_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".implode('-', array_unique($id_table_lst_type_interloc))."', '".$lst_interlocs[$j]['gtoper']."');\">
																		</br>";
																	} Else {
																		$result['code'] .= "
																		<img width='15px' style='cursor:pointer;padding-left:5px;' src='".ROOT_PATH."images/edit.png' onclick=\"showFormEchange(this, '".str_replace("'", "\'", $representant_lib[implode('-', $tbl_rel_saf_foncier_representant_id)])."', '";
																		If ($representant_nom_usage[implode('-', $tbl_rel_saf_foncier_representant_id)] != 'N.P.') {
																			$result['code'] .= str_replace("'", "\'", $representant_nom_usage[implode('-', $tbl_rel_saf_foncier_representant_id)]);
																		} Else {
																			$result['code'] .= str_replace("'", "\'", $representant_ddenom[implode('-', $tbl_rel_saf_foncier_representant_id)]);
																		}
																		$result['code'] .= "', '".$referent[$lst_echanges[$e]['interloc_id']]."', ".$representant_id[implode('-', $tbl_rel_saf_foncier_representant_id)].", '".$show_nego."', ".$session_af_id.", ".$lst_eaf[$i]['entite_af_id'].", ".$lst_echanges[$e]['echange_af_id'].", 'true', 'tbl_echange_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".implode('-', array_unique($id_table_lst_type_interloc))."', '".$representant_gtoper[implode('-', $tbl_rel_saf_foncier_representant_id)]."');\">
																		</br>";
																	}
																	
																}
																$result['code'] .= "
																	<img width='15px' style='cursor:pointer;padding-left:5px;' src='".ROOT_PATH."images/supprimer.png' onclick=\"deleteEchange(this, ".$lst_echanges[$e]['interloc_id'].", ".$session_af_id.", ".$lst_eaf[$i]['entite_af_id'].", ".$lst_echanges[$e]['echange_af_id'].", '".$lst_echanges[$e]['echange_direct']."');\">";
															}
															$result['code'] .= "
															</td>
														</tr>";
														If ($lst_echanges[$e]['date_recontact'] != 'N.D.') {
															If ($lst_echanges[$e]['rappel_traite'] == 't') {
																$suite_color = '#537a13';
															} Else {
																$suite_color = '#fe0000';
															}
															$result['code'] .=											
															"<tr id='tr_echange_rappel_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".$lst_echanges[$e]['echange_af_id']."' class='tr_echange_".$lst_echanges[$e]['echange_af_id']."'>
																<td style='text-align:right;vertical-align:top;padding-left:25px;'>
																	<label class='label' style='margin:0 4px; white-space:pre-line;color:".$suite_color.";'>A recontacter le ".date_transform($lst_echanges[$e]['date_recontact'])." : </label>
																</td>
																<td style='vertical-align:top;text-align:right;'>
																	<textarea id='description_rappel_".$lst_echanges[$e]['echange_af_id']."_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."' class='label textarea_com_edit' cols='120' rows='1' style='font-weight:normal;min-height:5px;color:".$suite_color.";background:none;padding:0;' READONLY>".$lst_echanges[$e]['commentaires']."</textarea>
																</td>
															</tr>";
														}										
													}
													
													$lst_resultat = explode(',', $lst_echanges[$e]['tbl_resultat']);
													$lst_resultat_prix = explode(',', $lst_echanges[$e]['tbl_resultat_prix']);
												If ($lst_echanges[$e]['categorie'] == 'echange' && $lst_echanges[$e]['tbl_resultat'] != 'N.D.' && count($lst_resultat) > 0 || (count($lst_resultat) == 1 && $lst_resultat[0] != 'N.D.')) {
														$lst_par_lib = explode(',', $lst_echanges[$e]['tbl_par_lib']);
														$lst_dnulot = explode(',', $lst_echanges[$e]['tbl_dnulot']);		
														$result['code'] .="
														<tr id='tr_echange_nego_".$lst_interlocs[$j]['interloc_id']."_".$lst_eaf[$i]['entite_af_id']."_".$lst_echanges[$e]['echange_af_id']."' class='tr_echange_".$lst_echanges[$e]['echange_af_id']."'>
															<td>&nbsp;</td>
															<td class='bilan-af'>
																<ul style='margin:0;'>";
																For ($a=0; $a<count($lst_resultat); $a++) {
																	switch (substr($lst_resultat[$a], 0, 5)) {
																		case 'Refus':
																		case 'Défa':
																			$color='#fe0000';
																			break;
																		case 'Accor':
																		case 'Favor':
																			$color='#2b933e';
																			break;
																		case 'Persp':
																			$color='#004494';
																			break;
																		default:
																		   $color='#004494';
																	}
																	If ($lst_resultat[$a] != 'N.D.') {
																		$result['code'] .= "<li class='label_simple' style='color:$color;'>".$lst_par_lib[$a];
																		If ($lst_dnulot[$a] != 'N.D.') {
																			$result['code'] .= " ".$lst_dnulot[$a];
																		}
																		$result['code'] .= " : ".$lst_resultat[$a];
																		If ($lst_resultat_prix[$a] != -1) { 
																			$result['code'] .= " à ".$lst_resultat_prix[$a]." €";
																		}
																		$result['code'] .= "</li>";
																	}
																}
														$result['code'] .="
																</ul>
															</td>
														</tr>";
													}										
												}
												$result['code'] .=	"
												</table>
											</td>
										</tr>";							 
									}	
								} 	
								
						$result['code'] .= "				
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>";			
		}
		$bdd = null;
		
		print json_encode($result);
	}
	
	function fct_create_synthese($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id']; // Cette variable récupère l'id de la SAF
		$scroll_top = $data['params']['var_scroll_top'];
		$offset = $data['params']['offset'];
		$section = $data['params']['section'];
		$par_num = $data['params']['par_num'];
		$nom = $data['params']['nom'];
		$nbr_result = 600000;
		$bad_caract = array("-","–","’", "'");
		$good_caract = array("–","-","''", "''");
			
		$rq_lst_eaf = "
		SELECT DISTINCT 
			entite_af_id,
			COALESCE(prix_eaf_negocie, prix_eaf_arrondi) prix_eaf,
			count(DISTINCT interloc_id) as nbr_interloc,
			min(CASE WHEN date_recontact != 'N.D.' AND date_recontact IS NOT NULL AND rappel_traite = 'f' THEN date_recontact END) ordre1,
			CASE WHEN COALESCE(t_actif.nbr_actif, 0) = 0 AND COALESCE(t_bloque.nbr_lot_bloque, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) AND COALESCE(t_supprime.nbr_lot_supprime, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
				-1 
			ELSE
				CASE WHEN COALESCE(t_bloque.nbr_lot_bloque, 0) = count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) AND COALESCE(t_supprime.nbr_lot_supprime, 0) != count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
					-2 
				ELSE
					CASE WHEN COALESCE(t_supprime.nbr_lot_supprime, 0) = count(DISTINCT rel_saf_foncier.rel_saf_foncier_id) THEN 
						-3
					ELSE 
						1 
					END 
				END
			END as ordre2
		FROM 		
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			LEFT JOIN 
				(
					SELECT 
						entite_af_id,
						count(rel_saf_foncier_id) as nbr_lot_supprime
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN foncier.cadastre_site t4 USING (cad_site_id)
						JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
					WHERE
						session_af_id = :session_af_id AND
						(t3.date_fin != 'N.D.' OR t4.date_fin != 'N.D.')
					GROUP BY
						entite_af_id
				) as t_supprime USING (entite_af_id)
			LEFT JOIN 
				(
					SELECT 
						entite_af_id,
						count(rel_saf_foncier_id) as nbr_lot_bloque
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						session_af_id = :session_af_id AND
						lot_bloque = 't'
					GROUP BY
						entite_af_id
				) as t_bloque USING (entite_af_id)
			LEFT JOIN 
				(
					SELECT DISTINCT 
						entite_af_id,
						count(interloc_id) as nbr_actif
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					WHERE 
						session_af_id = :session_af_id AND
						etat_interloc_id = 1
					GROUP BY entite_af_id
				) t_actif USING (entite_af_id)	
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
			JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.lots_cen USING (lot_id)
			JOIN 
				(SELECT DISTINCT 
					entite_af_id,						
					array_to_string(array_agg(
						CASE WHEN nom_usage = 'N.P.' THEN
							ddenom
						ELSE
							CASE WHEN nom_jeunefille = 'N.P.' THEN
								nom_usage
							ELSE
								nom_usage||' '||nom_jeunefille
							END
						END), ',') as tbl_eaf_ddenom
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
				GROUP BY entite_af_id
				) t1 USING (entite_af_id)
			LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.' AND
			lot_id ILIKE '%".$section.'%'.$par_num."%'";
			
		If ($nom != '' && $nom != NULL) {
			$rq_lst_eaf .= " AND t1.tbl_eaf_ddenom ILIKE '%$nom%'";
		}			
		$rq_lst_eaf .= "
			GROUP BY 
				entite_af_id,
				prix_eaf,
				t_bloque.nbr_lot_bloque,
				t_supprime.nbr_lot_supprime,
				t_actif.nbr_actif
			ORDER BY 
				ordre1,
				ordre2 DESC,
				entite_af_id DESC";
		// echo $rq_lst_eaf;
		$res_lst_eaf = $bdd->prepare($rq_lst_eaf);
		$res_lst_eaf->execute(array('session_af_id'=>$session_af_id));
		$lst_eaf = $res_lst_eaf->fetchall();
		
		$rq_info_lots = "
			SELECT 
				t_resume.rel_saf_foncier_id,	
				t_resume.cad_site_id,
				t_resume.lot_id,
				t_resume.dcntlo,
				t_resume.surf_mu,
				t_resume.surf_mu_init,
				t_resume.utilissol,
				t_resume.paf_id,
				t_resume.paf_lib,
				t_resume.lot_lib,
				t_resume.com_nom,
				COALESCE(t_resume.prix_nego, prix_parc_arrondi)::numeric(10, 2) as prix_nego,
				min(t_resume.perspective) as perspective
			FROM 
				(SELECT DISTINCT
					rel_saf_foncier.rel_saf_foncier_id,
					rel_saf_foncier.prix_parc_arrondi,
					t4.cad_site_id,
					t2.lot_id,
					t2.dcntlo,
					CASE WHEN rel_saf_foncier.geom IS NULL THEN dcntlo ELSE round(st_area(rel_saf_foncier.geom)::numeric, 0) END as surf_mu,
					surf_mu_init,
					COALESCE(rel_eaf_foncier.utilissol, 'N.D.') AS utilissol,
					CASE WHEN lot_bloque = 't' THEN
						4
					ELSE
						rel_saf_foncier.paf_id
					END as paf_id,
					d_paf.paf_lib,
					CASE WHEN t2.dnulot IS NOT NULL THEN v_communes.nom || '<br>' || t1.ccosec || ' - ' || t1.dnupla || ' - ' || t2.dnulot ELSE v_communes.nom || '<br>' || t1.ccosec || ' - ' || t1.dnupla END as lot_lib,
					v_communes.nom as com_nom,
					COALESCE(min(nego_af_id)::numeric, 0.5) as perspective,
					CASE WHEN tnegoprix.nbr_nego = 1 AND tnegoprix.nbr_all_nego = nbr_interloc_vp AND tnegoprix.nbr_prix = 1 AND tbl_prix[1] != 0 THEN
						tbl_prix[1]
					ELSE
						NULL
					END as prix_nego,
					interloc_id
				FROM
					cadastre.parcelles_cen t1
					JOIN administratif.v_communes ON (v_communes.code_insee = t1.codcom)			
					JOIN cadastre.lots_cen t2 USING (par_id)				
					JOIN cadastre.cadastre_cen t3 USING (lot_id)
					JOIN foncier.cadastre_site t4 USING (cad_cen_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.d_paf ON (d_paf.paf_id = rel_saf_foncier.paf_id)
					LEFT JOIN (
						SELECT 
							rel_af_echanges.rel_saf_foncier_interlocs_id, 
							nego_af_id,
							echange_af_date
						FROM 
							animation_fonciere.rel_af_echanges
							JOIN animation_fonciere.echanges_af USING (echange_af_id)
							JOIN (
								SELECT
									rel_saf_foncier_interlocs_id,
									max(echange_af_date) as max_date
								FROM 
									animation_fonciere.rel_af_echanges
									JOIN animation_fonciere.echanges_af USING (echange_af_id)
								WHERE 
									nego_af_id IS NOT NULL
								GROUP BY
									rel_saf_foncier_interlocs_id
							) st2 ON (echanges_af.echange_af_date = st2.max_date AND rel_af_echanges.rel_saf_foncier_interlocs_id = st2.rel_saf_foncier_interlocs_id)
						WHERE 
							nego_af_id IS NOT NULL
						) st1 ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = st1.rel_saf_foncier_interlocs_id)
					JOIN (
						SELECT
							rel_saf_foncier_id,
							count(DISTINCT interloc_id) as nbr_interloc_vp
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							can_nego = 't' AND 
							can_be_subst = 'f' AND
							entite_af_id = :entite_af_id AND 
							session_af_id = :session_af_id
						GROUP BY
							rel_saf_foncier_id
					) st2 USING (rel_saf_foncier_id)
					LEFT JOIN (
						SELECT DISTINCT
							rel_saf_foncier_id, 
							array_agg(DISTINCT nego_af_lib) as tbl_nego_lib,
							array_agg(DISTINCT nego_af_id) as tbl_nego_id,
							array_length(array_agg(DISTINCT nego_af_id), 1) as nbr_nego,
							array_length(array_agg(nego_af_id), 1) as nbr_all_nego,
							array_agg(DISTINCT nego_af_prix) as tbl_prix,
							array_length(array_agg(DISTINCT nego_af_prix), 1) as nbr_prix
						FROM 
							animation_fonciere.rel_af_echanges
							JOIN animation_fonciere.d_nego_af USING (nego_af_id)
							JOIN animation_fonciere.echanges_af USING (echange_af_id)
							JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN (
								SELECT
									rel_saf_foncier_interlocs_id,
									max(echange_af_date) as max_date
								FROM 
									animation_fonciere.rel_af_echanges
									JOIN animation_fonciere.echanges_af USING (echange_af_id)
									JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								WHERE 
									nego_af_id IS NOT NULL AND
									can_nego = 't' AND 
									can_be_subst = 'f' AND
									entite_af_id = :entite_af_id AND 
									session_af_id = :session_af_id
								GROUP BY
									rel_saf_foncier_interlocs_id
								) st2 ON (echanges_af.echange_af_date = st2.max_date AND rel_af_echanges.rel_saf_foncier_interlocs_id = st2.rel_saf_foncier_interlocs_id)
						WHERE 
							nego_af_id IS NOT NULL AND
							can_nego = 't' AND 
							can_be_subst = 'f'
						GROUP BY
							rel_saf_foncier_id
							) tnegoprix USING (rel_saf_foncier_id)
				WHERE 
					entite_af_id = :entite_af_id AND
					session_af_id = :session_af_id AND
					t3.date_fin = 'N.D.' AND
					t4.date_fin = 'N.D.' AND
					can_nego = 't' AND 
					can_be_subst = 'f'
				GROUP BY 
					rel_saf_foncier.rel_saf_foncier_id,
					rel_saf_foncier.prix_parc_arrondi,
					t4.cad_site_id,
					t2.lot_id,
					t2.dcntlo,
					surf_mu,
					surf_mu_init,
					utilissol,
					rel_saf_foncier.paf_id,
					d_paf.paf_lib,
					lot_lib,
					com_nom,
					interloc_id,
					prix_nego
				ORDER BY t2.lot_id) t_resume
			GROUP BY 
				rel_saf_foncier_id,
				cad_site_id,
				lot_id,
				dcntlo,
				surf_mu,
				surf_mu_init,
				utilissol,
				paf_id,
				paf_lib,
				lot_lib,
				com_nom,
				prix_nego,
				prix_parc_arrondi
			ORDER BY lot_id";
		$res_info_lots = $bdd->prepare($rq_info_lots);
		
		$rq_nat_cult = "
			SELECT DISTINCT
				array_to_string(array_agg(DISTINCT replace(d_dsgrpf.natcult_ssgrp, ' ou Pâturages', '')), ',') as tbl_natcult
			FROM
				cadastre.lots_cen t2	
				JOIN cadastre.lots_natcult_cen USING (lot_id)
				LEFT JOIN cadastre.d_cnatsp USING (cnatsp)
				JOIN cadastre.d_dsgrpf USING (dsgrpf)
			WHERE t2.lot_id = ?
			GROUP BY lot_id";
		$res_nat_cult = $bdd->prepare($rq_nat_cult);	
		
		$rq_info_interlocs = "
		SELECT DISTINCT
			interlocuteurs.interloc_id,
			interlocuteurs.dnuper,
			d_ccoqua.particule_lib,
			interlocuteurs.ddenom,
			interlocuteurs.nom_usage,
			COALESCE(interlocuteurs.nom_jeunefille, 'N.D.') as nom_jeunefille,
			CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
				CASE WHEN strpos(interlocuteurs.prenom_usage, ' ') > 0 THEN
					substring(interlocuteurs.prenom_usage from 0 for  strpos(interlocuteurs.prenom_usage, ' ')) 
				ELSE 
					interlocuteurs.prenom_usage
				END
			ELSE	
				'N.P.' 
			END as prenom_usage,
			COALESCE(interlocuteurs.nom_conjoint, 'N.D.') as nom_conjoint,
			COALESCE(interlocuteurs.prenom_conjoint, 'N.D.') as prenom_conjoint,
			array_to_string(array_agg(DISTINCT d_type_interloc.type_interloc_id), '-') as tbl_type_interloc_id, 
			interlocuteurs.gtoper,
			array_to_string(array_agg(DISTINCT rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id), ',') as tbl_rel_saf_foncier_interlocs_id,
			array_to_string(array_agg(DISTINCT COALESCE(interloc_substitution.rel_saf_foncier_interlocs_id_is, 0)), ',') as tbl_rel_saf_foncier_interlocs_id_is,
			CASE WHEN ref1.interloc_id IS NOT NULL THEN 
				ref1.interloc_ref_id || '-a-'
			ELSE
				ref2.interloc_ref_id || '-b-' || ref2.interloc_id
			END as ordre
		FROM 
			animation_fonciere.interlocuteurs 
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
			LEFT JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
			JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
			JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			--JOIN animation_fonciere.session_af USING (session_af_id)
			--LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)									
			LEFT JOIN 
				(
					SELECT
						st1.interloc_id,
						rel_saf_foncier_interlocs.interloc_id as interloc_ref_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN
							(SELECT DISTINCT 
								rel_saf_foncier_interlocs.interloc_id, 
								interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
							FROM 
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
							WHERE 
								session_af_id = :session_af_id AND 
								entite_af_id = :entite_af_id
							) st1 ON st1.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
				) ref1 ON (ref1.interloc_id = rel_saf_foncier_interlocs.interloc_id)
			LEFT JOIN 
				(
					SELECT
						st2.interloc_id,
						rel_saf_foncier_interlocs.interloc_id as interloc_ref_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN
							(SELECT DISTINCT 
								rel_saf_foncier_interlocs.interloc_id, 
								interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
							FROM 
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
							WHERE 
								session_af_id = :session_af_id AND 
								entite_af_id = :entite_af_id
							) st2 ON st2.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
				) ref2 ON (ref2.interloc_id = rel_saf_foncier_interlocs.interloc_id)
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			etat_interloc_id = 1 AND
			entite_af_id = :entite_af_id AND
			groupe_interloc_id != 3 AND
			(can_nego = 't' OR avis_favorable = 't') AND 
			rel_saf_foncier_interlocs_id NOT IN
				(
					SELECT 
						rel_saf_foncier_interlocs_id_vp
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
					WHERE 
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id AND
						groupe_interloc_id = 4
				) AND
			rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN
				(
					SELECT DISTINCT 
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					WHERE
						can_be_subst = 't' AND
						rel_saf_foncier_interlocs_id NOT IN
							(
								SELECT rel_saf_foncier_interlocs_id_is
								FROM animation_fonciere.interloc_substitution
							)
				)
		GROUP BY 
			interlocuteurs.interloc_id, 
			d_ccoqua.particule_lib, 
			ordre
		ORDER BY ordre";
		// echo $rq_info_interlocs;
		$res_info_interlocs = $bdd->prepare($rq_info_interlocs);		
				
		$rq_site_interloc = "
		SELECT 
			array_to_string(array_agg(DISTINCT site_nom), '<br>') as lst_site_interloc
		FROM 
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.cptprop_cen USING (dnupro)
			JOIN cadastre.r_prop_cptprop_cen t6 USING (dnupro)
		WHERE 
			cad_site_id NOT IN (SELECT cad_site_id FROM animation_fonciere.rel_af_foncier WHERE session_af_id = :session_af_id) AND
			t6.dnuper IN (SELECT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = :interloc_id) AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'";
		$res_site_interloc = $bdd->prepare($rq_site_interloc);
		
		$color_objectif = array("#fe0000", "transparent repeating-linear-gradient(45deg, rgb(112, 173, 71), rgb(112, 173, 71) 5px, rgb(68, 114, 196) 5px, rgb(68, 114, 196) 10px) repeat scroll 0% 0%", "#70ad47", "#4472c4", "#70ad47", "#4472c4", "#fe0000");
		$color_objectif['0.5'] = "#747474";
		$color_paf = array("#000000", "#fe0000", "#ffa025", "#00b050", "#747474");
		$result['tbody_synthese_parc_corps'] = "
		<tr id='tr_synthese_entete_prix'>			
			<td>
				<table class='no-border'>
					<tbody>
						<tr>
							<td class='filtre_objectif' style='min-width:44px;text-align:center;'>
								<label id='lbl_synthese_nego' class='last_maj' style='padding:0;'>Négo</label>
							</td>
							<td class='filtre_lot' style='text-align:center;padding-left:10px;'>
								<label id='lbl_synthese_parc_lot' class='last_maj' style='padding:0;'>Parcelle<br>Lot</label>
							</td>
							<td class='filtre_contenance' style='text-align:center;'>
								<label id='lbl_synthese_prix_lot' class='last_maj' style='padding:0;'>Surf. à maitriser<br>(Surf. du lot)</label>
							</td>
							<td style='min-width:75px;text-align:center;'>
								<label id='lbl_synthese_prix_ayantdroit' class='last_maj' style='padding:0;'>Ayant<br>droit</label>
							</td>	
							<td class='synthese_prix_lot' style='text-align:center;'>
								<label id='lbl_synthese_prix_eaf' class='last_maj' style='padding:0;'>Prix d'acquisition<br>total du lot</label>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td class='synthese_prix_eaf' style='text-align:center;'>
				<label id='lbl_synthese_prix_eaf' class='last_maj' style='padding:0;'>Prix d'acquisition<br>de l'EAF</label>
			</td>
			<td colspan='2'>
				&nbsp;
			</td>
		</tr>";
		For ($i=0; $i<$res_lst_eaf->rowCount(); $i++) {		
			$res_info_lots->execute(array('entite_af_id'=>$lst_eaf[$i]['entite_af_id'], 'session_af_id'=>$session_af_id));
			$info_lots = $res_info_lots->fetchall();
			If ($res_info_lots->rowCount() > 0) {
				$result['tbody_synthese_parc_corps'] .= "
				<tr id='tr_synthese_".$lst_eaf[$i]['entite_af_id']."'>
					<td colspan='1' style='border-bottom:1px solid #004494;'>
						<table id='tbl_parc_".$lst_eaf[$i]['entite_af_id']."' class='no-border'>";			
			
				$tbl_rel_saf_foncier_id = [];
				For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 
					$tbl_rel_saf_foncier_id[] = $info_lots[$l]['rel_saf_foncier_id'];
					$res_nat_cult->execute(array($info_lots[$l]['lot_id']));					
					$nat_cult = $res_nat_cult->fetchall();
					
					$result['tbody_synthese_parc_corps'] .= "
								<tr>
									<td id='td_objectif_".$info_lots[$l]['rel_saf_foncier_id']."' colspan='1' class='filtre_objectif' style='background:".$color_objectif[$info_lots[$l]['perspective']].";min-width:44px;border-radius:8px;text-align:center;'>";
									If ($info_lots[$l]['perspective'] == 4 || $info_lots[$l]['perspective'] == 5) {
										$result['tbody_synthese_parc_corps'] .= "
										<label id='label_objectif_".$info_lots[$l]['rel_saf_foncier_id']."' class='label_simple' style='color:#ffffff;font-size:8pt;padding:0;'>accord</label>
										<input type='hidden' id='hid_label_objectif_".$info_lots[$l]['rel_saf_foncier_id']."' value='accord'>";
									} Else {
										$result['tbody_synthese_parc_corps'] .= "
										<label id='label_objectif_".$info_lots[$l]['rel_saf_foncier_id']."' class='label_simple' style='color:#ffffff;font-size:8pt;padding:0;'>&nbsp;</label>
										<input type='hidden' id='hid_label_objectif_".$info_lots[$l]['rel_saf_foncier_id']."' value='N.D.'>";
									}
										$result['tbody_synthese_parc_corps'] .= "
										<input type='hidden' id='hid_color_objectif_".$info_lots[$l]['rel_saf_foncier_id']."' value='".$color_objectif[$info_lots[$l]['perspective']]."'>
									</td>
									<td class='filtre_lot' style='text-align:center;padding-left:10px;'>
										<label id='label_".$info_lots[$l]['rel_saf_foncier_id']."' class='label_simple' style='padding:0;color:".$color_paf[$info_lots[$l]['paf_id']].";'>
											<b>".$info_lots[$l]['lot_lib']."</b>
										</label>
										<input type='hidden' id='hid_color_paf_".$info_lots[$l]['rel_saf_foncier_id']."' value='".$color_paf[$info_lots[$l]['paf_id']]."'>
									</td>
									<td class='filtre_contenance' style='text-align:center;'>
										<label id='label_".$info_lots[$l]['rel_saf_foncier_id']."_contenance' class='label_simple' style='padding:0;color:".$color_paf[$info_lots[$l]['paf_id']].";'>
											".conv_are($info_lots[$l]['surf_mu']);
									If ($info_lots[$l]['surf_mu_init'] > 0) {
										$result['tbody_synthese_parc_corps'] .= "
										<br>(".conv_are($info_lots[$l]['surf_mu_init']).")";
									}
										$result['tbody_synthese_parc_corps'] .= "
										</label>
									</td>
									<td style='min-width:75px;height:25px;'>
										<img id='agri_".$info_lots[$l]['rel_saf_foncier_id']."' width='24px' style='cursor:pointer;display:none;' src='".ROOT_PATH."foncier/module_af/images/tracteur.png'>
									
										<img id='chasseur_".$info_lots[$l]['rel_saf_foncier_id']."' width='20px' style='cursor:pointer;display:none;' src='".ROOT_PATH."foncier/module_af/images/fusil.png'>
									
										<img id='pecheur_".$info_lots[$l]['rel_saf_foncier_id']."' width='25px' style='cursor:pointer;display:none;' src='".ROOT_PATH."foncier/module_af/images/canne-a-peche.png'>
									</td>";
								If ($info_lots[$l]['prix_nego'] != '') {
									$result['tbody_synthese_parc_corps'] .= "	
									<td class='synthese_prix_lot' style='text-align:center;'>
										<label class='label_simple'>".$info_lots[$l]['prix_nego']." €</label>
									</td>";
								}
								$result['tbody_synthese_parc_corps'] .= "
								</tr>";
								
				}
				
				$result['tbody_synthese_parc_corps'] .= "
						</table>
					</td>
					<td class='synthese_prix_eaf' style='border-bottom:1px solid #004494;text-align:center;min-width:98px;'>";
				If ($lst_eaf[$i]['prix_eaf'] != '') {
					$result['tbody_synthese_parc_corps'] .= "					
						<label class='label_simple'>".$lst_eaf[$i]['prix_eaf']." €</label>
					";
				}
					$result['tbody_synthese_parc_corps'] .= "
					</td>
					<td style='border-bottom:1px solid #004494; vertical-align:middle;'>
						<img src='".ROOT_PATH."images/info.png' width='15px' onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"Infos sur l'EAF\" style='cursor:pointer;' onclick=\"showInfosEAF(this, ".$session_af_id.", ".$lst_eaf[$i]['entite_af_id'].");\">
					</td>
					<td style='border-bottom:1px solid #004494;'>
						<table id='tbl_interloc_".$lst_eaf[$i]['entite_af_id']."' class='no-border' width='100%'>";
					
				$res_info_interlocs->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));
				$info_interlocs = $res_info_interlocs->fetchall();	
				For ($j=0; $j<$res_info_interlocs->rowCount(); $j++) { 
					$interloc_lib = str_replace('   ', ' ', proprio_lib($info_interlocs[$j]['particule_lib'], $info_interlocs[$j]['nom_usage'], $info_interlocs[$j]['prenom_usage'], $info_interlocs[$j]['ddenom']));
					$rq_parc_interloc = "
					SELECT
						array_to_string(array_agg(DISTINCT rel_saf_foncier.rel_saf_foncier_id || ':' || 
						CASE WHEN rel_saf_foncier.rel_saf_foncier_id NOT IN
							(SELECT rel_saf_foncier_id 
							FROM animation_fonciere.rel_saf_foncier_interlocs 
							WHERE rel_saf_foncier_interlocs_id IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is']."))
						THEN 
							-2
						ELSE 
							CASE WHEN max_date = 'N.D.' THEN -1 ELSE nego_af_id END
						END), ';') as nego_af_id,
						array_to_string(array_agg(DISTINCT rel_saf_foncier.rel_saf_foncier_id || ':' || 
						CASE WHEN rel_saf_foncier.rel_saf_foncier_id NOT IN
							(SELECT rel_saf_foncier_id 
							FROM animation_fonciere.rel_saf_foncier_interlocs 
							WHERE rel_saf_foncier_interlocs_id IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is']."))
						THEN 
							'N.D.'
						ELSE 
							lst_type_interloc_id
						END), ';') as lst_type_interloc_id
					FROM 
						animation_fonciere.rel_saf_foncier
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN 
							(
								SELECT
									rel_saf_foncier.rel_saf_foncier_id,
									max(COALESCE(echange_af_date, 'N.D.')) as max_date
								FROM 
									animation_fonciere.rel_saf_foncier
									LEFT JOIN 
										(
											SELECT 
												rel_saf_foncier_id,
												echange_af_date
											FROM
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
												JOIN animation_fonciere.echanges_af USING (echange_af_id)
											WHERE 
												rel_saf_foncier_interlocs_id IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].") AND
												nego_af_id IS NOT NULL
												
											UNION

											SELECT 
												rel_saf_foncier_id,
												echange_af_date
											FROM
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
												JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
												JOIN animation_fonciere.echanges_af USING (echange_af_id)
											WHERE 
												rel_saf_foncier_interlocs_id_is IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].") AND
												nego_af_id IS NOT NULL	
										) st1 ON (rel_saf_foncier.rel_saf_foncier_id = st1.rel_saf_foncier_id)
								
								GROUP BY
									rel_saf_foncier.rel_saf_foncier_id
							) t1 ON (rel_saf_foncier_interlocs.rel_saf_foncier_id = t1.rel_saf_foncier_id)
						LEFT JOIN 
							(
								SELECT
									rel_saf_foncier_id,
									array_to_string(array_agg(DISTINCT type_interloc_id), '-') as lst_type_interloc_id
								FROM
									(
										SELECT
											rel_saf_foncier_id,
											type_interloc_id
										FROM 
											animation_fonciere.rel_saf_foncier_interlocs
											JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp
							= rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
										WHERE 
											rel_saf_foncier_interlocs_id_is IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].")					

										UNION

										SELECT
											rel_saf_foncier_id,
											type_interloc_id
										FROM 
											animation_fonciere.rel_saf_foncier_interlocs
										WHERE 
											rel_saf_foncier_interlocs_id IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].")
									) st3
								GROUP BY
									rel_saf_foncier_id
							) t3 ON (rel_saf_foncier_interlocs.rel_saf_foncier_id = t3.rel_saf_foncier_id)
						LEFT JOIN 
							(
								SELECT 
									rel_saf_foncier_interlocs.rel_saf_foncier_id,
									nego_af_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.echanges_af USING (echange_af_id)
									JOIN 
									(
										SELECT 
											rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
											max(COALESCE(echange_af_date, 'N.D.')) as max_date
										FROM
											animation_fonciere.rel_saf_foncier_interlocs
											JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
											LEFT JOIN (
												SELECT 
													rel_saf_foncier_id,
													echange_af_date
												FROM
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
													JOIN animation_fonciere.echanges_af USING (echange_af_id)
												WHERE 
													rel_saf_foncier_interlocs_id IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].") AND
													nego_af_id IS NOT NULL
											) st1 ON (rel_saf_foncier_interlocs.rel_saf_foncier_id = st1.rel_saf_foncier_id)
										WHERE
											rel_saf_foncier_interlocs_id IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].")
										GROUP BY 
											rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
											
										UNION

										SELECT 
											rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
											max(COALESCE(echange_af_date, 'N.D.')) as max_date
										FROM
											animation_fonciere.rel_saf_foncier_interlocs
											JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
											JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
											LEFT JOIN 
												(
													SELECT 
														rel_saf_foncier_id,
														echange_af_date
													FROM
														animation_fonciere.rel_saf_foncier_interlocs
														JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
														JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
														JOIN animation_fonciere.echanges_af USING (echange_af_id)
													WHERE 
														rel_saf_foncier_interlocs_id_is IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].") AND
														nego_af_id IS NOT NULL
												) st1 ON (rel_saf_foncier_interlocs.rel_saf_foncier_id = st1.rel_saf_foncier_id)
										WHERE
											rel_saf_foncier_interlocs_id_is IN (".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id'].','.$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id_is'].")
										GROUP BY 
											rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id	
									) st2 ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = st2.rel_saf_foncier_interlocs_id	AND echanges_af.echange_af_date = st2.max_date)
							) t2 ON (rel_saf_foncier_interlocs.rel_saf_foncier_id = t2.rel_saf_foncier_id)
					WHERE 
						session_af_id = :session_af_id AND
						entite_af_id = :entite_af_id AND
						etat_interloc_id = 1
					";
					
					$res_parc_interloc = $bdd->prepare($rq_parc_interloc);
					$res_parc_interloc->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$lst_eaf[$i]['entite_af_id']));				
					$parc_interloc = $res_parc_interloc->fetch();
					
					$res_site_interloc->execute(array('session_af_id'=>$session_af_id, 'interloc_id'=>$info_interlocs[$j]['interloc_id']));
					$lst_site_interloc = $res_site_interloc->fetch();
					
					If (substr_count($info_interlocs[$j]['ordre'], '-a-') > 0) {
						$class_interloc = 'label';
						$margin_left_interloc = '15px';
					} Else If (substr_count($info_interlocs[$j]['ordre'], '-b-') > 0) {
						$class_interloc = 'label_simple';
						$margin_left_interloc = '30px';
					} Else {
						$class_interloc = 'label_simple';
						$margin_left_interloc = '15px';
					}
					
					$color_interloc = '#000000';
					$result['tbody_synthese_parc_corps'] .= "
							<tr>
								<td class='filtre_proprio' width='1px'>
									<label class='$class_interloc' onmouseover=\"overLot('tbl_parc_".$lst_eaf[$i]['entite_af_id']."', '".$parc_interloc['nego_af_id']."', '".$parc_interloc['lst_type_interloc_id']."');this.style.textDecoration='underline';\" onmouseout=\"outLot('tbl_parc_".$lst_eaf[$i]['entite_af_id']."');this.style.textDecoration='none';\" style='margin-left:$margin_left_interloc;color:$color_interloc;'>".$interloc_lib."</label>";
									
					If ($lst_eaf[$i]['nbr_interloc'] > 1) {
						$result['tbody_synthese_parc_corps'] .= "					
							<img src='".ROOT_PATH."images/info.png' width='15px' onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"Infos sur le propriétaire\" style='cursor:pointer;' onclick=\"showInfosProprio(this, ".$session_af_id.", ".$lst_eaf[$i]['entite_af_id'].", ".$info_interlocs[$j]['interloc_id'].", '".str_replace("'", "\'", $interloc_lib)."');\">";
					}					
					
					If (explode('<br>', $lst_site_interloc['lst_site_interloc'])[0] != '') {
					$tooltip_interloc = 'Propri&eacute;taire pr&eacute;sent sur le(s) site(s)<br>'.str_replace($bad_caract, $good_caract, $lst_site_interloc['lst_site_interloc']);
						$result['tbody_synthese_parc_corps'] .= "
									<img src='".ROOT_PATH."images/info.png' width='15px' onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"$tooltip_interloc\">";
					}
									
					$result['tbody_synthese_parc_corps'] .= "				
								<td>
									<table class='no-border' width='100%'>";
					$rq_suites = "
					SELECT DISTINCT 
						rel_af_echanges.commentaires, 
						date_recontact,
						echange_af_id
					FROM
						animation_fonciere.rel_af_echanges
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
						LEFT JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
					WHERE 
						rappel_traite = 'f' AND 
						date_recontact != 'N.D.' AND 
						(interloc_id = :interloc_id OR rel_saf_foncier_interlocs_id_vp = :interloc_id) AND
						rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id).")
					ORDER BY date_recontact";
					$res_suites = $bdd->prepare($rq_suites);				
					$res_suites->execute(array('interloc_id'=>$info_interlocs[$j]['interloc_id']));
					$suites = $res_suites->fetchall();
					For ($s=0; $s<$res_suites->rowCount(); $s++) { 
						$result['tbody_synthese_parc_corps'] .= "
										<tr id='suite_".$suites[$s]['echange_af_id']."'>
											<td class='filtre_date' width='1px'>
												<label class='label_simple' style='margin-left:15px;'>".date_transform($suites[$s]['date_recontact'])."</label>
											</td>
											<td class='label_suite'>
												<p class='label_simple' style='margin:0 4px; white-space:pre-wrap;'>".$suites[$s]['commentaires']."</p>
											</td>
											<td width='1px'>
												<input type='button' class='btn_frm' value='Traité' onclick=\"validRappelEchange('".$info_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id']."', ".$suites[$s]['echange_af_id'].")\";'>
											</td>
										</tr>";
					}
									
					$result['tbody_synthese_parc_corps'] .= "
									</table>	
								</td>
							</tr>";
				}
				$res_info_interlocs->closeCursor();	
				$result['tbody_synthese_parc_corps'] .= "
						</table>
					</td
				</tr>";
			}
		}
		$bdd = null;	
		print json_encode($result);
	}
	
	// Cette fonction permet de renseigner dans la BD qu'un rappel d'échange a été traité
	function fct_validRappelEchange($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$tbl_rel_saf_foncier_interlocs_id = $data['params']['var_tbl_rel_saf_foncier_interlocs_id'];
		$echange_af_id = $data['params']['var_echange_af_id'];		
		
		$rq_update_echange = "
		UPDATE animation_fonciere.rel_af_echanges
		SET rappel_traite = 't'
		WHERE echange_af_id = ? AND rel_saf_foncier_interlocs_id IN (".$tbl_rel_saf_foncier_interlocs_id.")
		";		
		// echo $rq_update_echange;
		$res_update_echange = $bdd->prepare($rq_update_echange);
		$res_update_echange->execute(array($echange_af_id));
		
		// Ferme le curseur et désactive la connection à la base
		$res_update_echange->closeCursor();
		$bdd = null;
	}
	
	// Cette fonction permet de filtrer la liste des propriétaires à importer pour créer un nouvel interlocuteur
	function fct_flt_lst_proprio_import($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$nom = $data['params']['nom']; // Cette variable récupère le nom de la page fiche d'origine
		$prenom = $data['params']['prenom']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
				
		$rq_lst_proprios = " 
		--Recherche des interlocuteurs physiques dispos donc pas encore associé à cette eaf
		SELECT DISTINCT
			'interloc_id:'||COALESCE(interlocuteurs.interloc_id, NULL)||';dnuper:'||'N.P.' as key,
			NULL,
			interlocuteurs.nom_usage,
			interlocuteurs.prenom_usage,
			interlocuteurs.ddenom,
			d_ccoqua.particule_lib,
			1 as ordre
		FROM 
			animation_fonciere.interlocuteurs
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
		WHERE nom_usage != 'N.P.'";	
		If ($nom <> 'Filtrer sur le nom' AND $nom <> '') {
			$rq_lst_proprios .= " AND nom_usage ILIKE '" . str_replace("'", "''", $nom) . "%'";
		}	
		If ($prenom <> 'Filtrer sur le prénom' AND $prenom <> '') {
			$rq_lst_proprios .= " AND prenom_usage ILIKE '" . str_replace("'", "''", $prenom) . "%'";
		}	
		$rq_lst_proprios .= "
		UNION
		--Recherche des interlocuteurs moraux dispos donc pas encore associé à ce proprio
		SELECT DISTINCT
			'interloc_id:'||COALESCE(interlocuteurs.interloc_id, NULL)||';dnuper:'||'N.P.' as key,
			NULL,
			interlocuteurs.nom_usage,
			interlocuteurs.prenom_usage,
			interlocuteurs.ddenom,
			d_ccoqua.particule_lib,
			1 as ordre
		FROM 
			animation_fonciere.interlocuteurs
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
		WHERE nom_usage = 'N.P.'";	
		If ($nom <> 'Filtrer sur le nom' AND $nom <> '') {
			$rq_lst_proprios .= " AND ddenom ILIKE '" . str_replace("'", "''", $nom) . "%'";
		}		
		If ($prenom <> 'Filtrer sur le prénom' AND $prenom <> '') {
			$rq_lst_proprios .= " AND prenom_usage ILIKE '" . str_replace("'", "''", $prenom) . "%'";
		}
		$rq_lst_proprios .= " 
		UNION
		--Recherche des proprios physiques pas encore interlocuteurs
		SELECT DISTINCT
			'interloc_id:NULL;dnuper:'||t7.dnuper as key,
			t7.dnuper,
			t7.nom_usage,
			t7.prenom_usage,
			t7.ddenom,
			d_ccoqua.particule_lib,
			2 as ordre
		FROM 
			cadastre.proprios_cen t7
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
		WHERE t7.dnuper NOT IN (SELECT DISTINCT rel_proprio_interloc.dnuper FROM animation_fonciere.rel_proprio_interloc) AND nom_usage != 'N.P.'";
		If ($nom <> 'Filtrer sur le nom' AND $nom <> '') {
			$rq_lst_proprios .= " AND nom_usage ILIKE '" . str_replace("'", "''", $nom) . "%'";
		}	
		If ($prenom <> 'Filtrer sur le prénom' AND $prenom <> '') {
			$rq_lst_proprios .= " AND prenom_usage ILIKE '" . str_replace("'", "''", $prenom) . "%'";
		}	
		$rq_lst_proprios .= "
		UNION
		--Recherche des proprios moraux pas encore interlocuteurs
		SELECT DISTINCT
			'interloc_id:NULL;dnuper:'||t7.dnuper as key,
			t7.dnuper,
			t7.nom_usage,
			t7.prenom_usage,
			t7.ddenom,
			d_ccoqua.particule_lib,
			2 as ordre
		FROM 
			cadastre.proprios_cen t7
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
		WHERE t7.dnuper NOT IN (SELECT DISTINCT rel_proprio_interloc.dnuper FROM animation_fonciere.rel_proprio_interloc) AND nom_usage = 'N.P.'";
		If ($nom <> 'Filtrer sur le nom' AND $nom <> '') {
			$rq_lst_proprios .= " AND ddenom ILIKE '" . str_replace("'", "''", $nom) . "%'";
		}	
		If ($prenom <> 'Filtrer sur le prénom' AND $prenom <> '') {
			$rq_lst_proprios .= " AND prenom_usage ILIKE '" . str_replace("'", "''", $prenom) . "%'";
		}		
		$rq_lst_proprios .= " 
		ORDER BY ordre, ddenom
		LIMIT 400";		
		// echo $rq_lst_proprios;
		$res_lst_proprios = $bdd->prepare($rq_lst_proprios);
		$res_lst_proprios->execute();
		$result['lst_import_proprio'] = "<option value='-1'>- - - Propri&eacute;taire - - -</option>";
		While ($donnees = $res_lst_proprios->fetch())  {
			$proprio_lib = str_replace('   ', ' ', proprio_lib($donnees['particule_lib'], $donnees['nom_usage'], $donnees['prenom_usage'], $donnees['ddenom']));
			$result['lst_import_proprio'] .= "<option value='".$donnees['key']."'>".$proprio_lib."</option>";			
		}
				
		// Ferme le curseur et désactive la connection à la base
		$res_lst_proprios->closeCursor();
		$bdd = null;
		
		print json_encode($result);
	}
	
	// Cette fonction permet de mettre à jour la table interlocuteurs depuis la page fiche_saf.php
	function fct_addInterloc($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['session_af_id']; //Identifiant de la session d'AF
		$entite_af_id = $data['params']['entite_af_id']; //Identifiant de l'entité d'AF
		$type_interloc = $data['params']['type_interloc']; //type d'interlocuteur
		$mode = $data['mode']; //mode de création d'un interloc soit par import depuis proprios_cen soit création d'une nouvelle personne
		$tbl_parc_interloc = $data['params']['var_tbl_parc_interloc'];
		
		If ($mode == 'import_proprio') { //mode si on a sélectionné une personne dans le menu déroulant correspondant
			$dnuper_sel = $data['params']['dnuper_sel'];//clé de la combobox composée du dnuper et de l'interloc_id si existant, formatée comme suit : interloc_id:XX;dnuper:XXXXXX
			$interloc_id = substr($dnuper_sel, 12, (strpos($dnuper_sel, ';')-12)); //On récupère l'interloc_id depuis la clé vue précédement
			$dnuper = substr($dnuper_sel, strpos($dnuper_sel, 'dnuper:')+7, (strlen($dnuper_sel)-strpos($dnuper_sel, 'dnuper:')+7)); //On récupère le dnuper depuis la clé vue précédement
			//Si la clé contient un interloc_id alors on sait que le proprio sélectionné est déjà présent dans la table interlocuteurs donc pas besoin de le rajouter
			If ($interloc_id == 'NULL' AND $dnuper != 'N.P.') {
				//Requête permettant de connaitre l'identifiant de l'interlocuteur à ajouter
				$rq_new_interloc_id = "SELECT nextval('animation_fonciere.interloc_id_seq'::regclass)";
				$res_new_interloc_id = $bdd->prepare($rq_new_interloc_id);
				$res_new_interloc_id->execute();
				$new_interloc_id_temp = $res_new_interloc_id->fetch();
				$interloc_id = $new_interloc_id_temp[0];
				$res_new_interloc_id->closeCursor();
				
				//Ajout de l'interlocuteur depuis une sélection d'infos de la table proprios_cen
				$rq_insert_interloc = "
				INSERT INTO animation_fonciere.interlocuteurs (
					interloc_id,
					etat_interloc_id,
					dnuper,
					ccoqua,
					ddenom,
					nom_usage,
					nom_jeunefille,
					prenom_usage,
					nom_conjoint,
					prenom_conjoint,
					dnomlp,
					dprnlp,
					epxnee,
					dnomcp,
					dprncp,
					jdatnss,
					dldnss,
					dlign3,
					dlign4,
					dlign5,
					dlign6,
					email,
					fixe_domicile,
					fixe_travail,
					portable_domicile,
					portable_travail,
					fax,
					gtoper,
					ccogrm,
					dnatpr,
					dsglpm,
					commentaires,
					annee_matrice,
					maj_user,
					maj_date)
					
					(SELECT 
						".$interloc_id." as new_interloc_id, 
						1,
						t1.dnuper,
						t1.ccoqua,
						t1.ddenom,
						t1.nom_usage,
						t1.nom_jeunefille,
						t1.prenom_usage,
						t1.nom_conjoint,
						t1.prenom_conjoint,
						t1.dnomlp,
						t1.dprnlp,
						t1.epxnee,
						t1.dnomcp,
						t1.dprncp,
						t1.jdatnss,
						t1.dldnss,
						t1.dlign3,
						t1.dlign4,
						t1.dlign5,
						t1.dlign6,
						t1.email,
						t1.fixe_domicile,
						t1.fixe_travail,
						t1.portable_domicile,
						t1.portable_travail,
						t1.fax,
						t1.gtoper,
						t1.ccogrm,
						t1.dnatpr,
						t1.dsglpm,
						t1.commentaires,
						t1.annee_matrice,
						t1.maj_user,
						t1.maj_date 
					FROM 
						cadastre.proprios_cen t1 
					WHERE 
						t1.dnuper = '$dnuper')";
						
				// echo $rq_insert_interloc;		
				$res_insert_interloc = $bdd->prepare($rq_insert_interloc);
				$res_insert_interloc->execute();
				$res_insert_interloc->closeCursor();
				
				//Ajout de l'association entre l'interlocuteur et le dnuper de proprio_cen
				$rq_insert_rel_proprio_interloc = "INSERT INTO animation_fonciere.rel_proprio_interloc (dnuper, interloc_id) VALUES ('$dnuper', $interloc_id)";
				// echo $rq_insert_interloc;		
				$res_insert_rel_proprio_interloc = $bdd->prepare($rq_insert_rel_proprio_interloc);
				$res_insert_rel_proprio_interloc->execute();
				$res_insert_rel_proprio_interloc->closeCursor();
			}
		} Else If ($mode == 'new_interloc') {// Si l'interlocuteur n'est pas présent dans le menu déroulant de prorpios on en ajoute un nouveau en remplissant le formulaire dédié et détaillé
			//On récupère toutes les infos du formulaire
			$bad_caract = array("-","–","’", "'");
			$good_caract = array("–","-","''", "''");
			$particule_id = str_replace($bad_caract, $good_caract, $data['params']['particule_id']);
			$nom = str_replace($bad_caract, $good_caract, $data['params']['nom']);
			$prenom = str_replace($bad_caract, $good_caract, $data['params']['prenom']);
			$ddenom = str_replace($bad_caract, $good_caract, $data['params']['ddenom']);
						
			//Requête permettant de connaitre l'identifiant de l'interlocuteur à ajouter
			$rq_new_interloc_id = "SELECT nextval('animation_fonciere.interloc_id_seq'::regclass)";
			$res_new_interloc_id = $bdd->prepare($rq_new_interloc_id);
			$res_new_interloc_id->execute();
			$new_interloc_id_temp = $res_new_interloc_id->fetch();
			$interloc_id = $new_interloc_id_temp[0];
			$res_new_interloc_id->closeCursor();			
			If ($ddenom != '') {
				$rq_insert_interloc =
				"INSERT INTO animation_fonciere.interlocuteurs
					(interloc_id,
					etat_interloc_id,
					ccoqua,
					ddenom,
					nom_usage,
					prenom_usage,
					gtoper)
				VALUES 
					($interloc_id,
					1,
					NULL,
					'".strtoupper($ddenom)."',
					'N.P.',
					'N.P.',
					2)";	
			} Else {
				$rq_insert_interloc =
				"INSERT INTO animation_fonciere.interlocuteurs
					(interloc_id,
					etat_interloc_id,
					ccoqua,
					ddenom,
					nom_usage,
					prenom_usage,
					gtoper)
				VALUES 
					($interloc_id,
					1,
					$particule_id,
					'".strtoupper($nom.'/'.$prenom)."',
					'".strtoupper($nom)."',
					'".strtoupper($prenom)."',
					1)";	
			}
			
			$res_insert_interloc = $bdd->prepare($rq_insert_interloc);
			// echo $rq_insert_interloc;
			$res_insert_interloc->execute();
			$res_insert_interloc->closeCursor();
		}
		$lst_rel_saf_foncier_id = '';
		For ($i=0; $i<count($tbl_parc_interloc) ; $i++) {
			$rq_new_rel_saf_foncier_interlocs_id = "SELECT nextval('animation_fonciere.rel_saf_foncier_interlocs_id_seq'::regclass)";
			$res_new_rel_saf_foncier_interlocs_id = $bdd->prepare($rq_new_rel_saf_foncier_interlocs_id);
			$res_new_rel_saf_foncier_interlocs_id->execute();
			$new_rel_saf_foncier_interlocs_id_temp = $res_new_rel_saf_foncier_interlocs_id->fetch();
			$new_rel_saf_foncier_interlocs_id = $new_rel_saf_foncier_interlocs_id_temp[0];
			$res_new_rel_saf_foncier_interlocs_id->closeCursor();
			$info_parc_interloc = explode(':', $tbl_parc_interloc[$i]);
			$lst_rel_saf_foncier_id .= $info_parc_interloc[0].',';
			
			$rq_insert_rel_saf_foncier_interlocs = "
			INSERT INTO animation_fonciere.rel_saf_foncier_interlocs 
				(rel_saf_foncier_interlocs_id,
				interloc_id, 
				rel_saf_foncier_id, 
				type_interloc_id, 
				ccodro, 
				ccodem)
			VALUES
				($new_rel_saf_foncier_interlocs_id,
				$interloc_id,
				".$info_parc_interloc[0].",
				$type_interloc,
				CASE WHEN '".$info_parc_interloc[2]."' = '-1' THEN NULL ELSE '".$info_parc_interloc[2]."' END,
				CASE WHEN '".$info_parc_interloc[1]."' = '-1' THEN NULL ELSE '".$info_parc_interloc[1]."' END
				)";
			// echo $rq_insert_rel_saf_foncier_interlocs;	
			$res_insert_rel_saf_foncier_interlocs = $bdd->prepare($rq_insert_rel_saf_foncier_interlocs);
			$res_insert_rel_saf_foncier_interlocs->execute();
			$res_insert_rel_saf_foncier_interlocs->closeCursor();
		}
		$lst_rel_saf_foncier_id = substr($lst_rel_saf_foncier_id, 0, -1);
		
		If ($interloc_id != 'NULL') {
			//Si le nouvel interlocuteur existe, on vérifie ses relations au sein de l'EAF pour mettre à jour automatiquement les table interloc_referent et interloc_substitution
			//Le nouvel interlocuteur est représenté par quelqu'un
			$rq_insert_interloc_substitution = "
			WITH t1 AS
			(
				SELECT
					rel_saf_foncier_interlocs_id_vp,
					rel_saf_foncier_interlocs_id_is,
					rel_saf_foncier_interlocs.rel_saf_foncier_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
					JOIN animation_fonciere.d_type_interloc d_type_interloc_subst ON (d_type_interloc_subst.type_interloc_id = t_temp_subst.type_interloc_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					rel_saf_foncier_interlocs.interloc_id = $interloc_id AND
					d_groupe_interloc.can_be_under_subst = 't' AND
					d_type_interloc_subst.groupe_interloc_id != 3
			)
			INSERT INTO animation_fonciere.interloc_substitution
				(
					SELECT
						rel_saf_foncier_interlocs_id,
						t1.rel_saf_foncier_interlocs_id_is
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN t1 USING (rel_saf_foncier_id)
					WHERE
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND
						interloc_id = $interloc_id AND
						can_be_under_subst = 't' AND
						rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id_vp FROM t1)
				)";
			$res_insert_interloc_substitution = $bdd->prepare($rq_insert_interloc_substitution);
			$res_insert_interloc_substitution->execute();
			$res_insert_interloc_substitution->closeCursor();
			
			//Le nouvel interlocuteur est référencé par quelqu'un
			$rq_insert_interloc_referent = "
			WITH t1 AS
			(
				SELECT DISTINCT
					rel_saf_foncier_interlocs_id,
					rel_saf_foncier_interlocs_id_ref,
					rel_saf_foncier_interlocs.rel_saf_foncier_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					interloc_id = $interloc_id AND
					can_be_under_ref = 't'
			)

			INSERT INTO animation_fonciere.interloc_referent
				(
					SELECT DISTINCT
						t1.rel_saf_foncier_interlocs_id_ref,
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN t1 USING (rel_saf_foncier_id)
					WHERE
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND
						interloc_id = $interloc_id AND
						can_be_under_ref = 't' AND
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id FROM t1)
				)";
			$res_insert_interloc_referent = $bdd->prepare($rq_insert_interloc_referent);
			$res_insert_interloc_referent->execute();
			$res_insert_interloc_referent->closeCursor();
			
			//Le nouvel interlocuteur est déjà référent
			$rq_insert_interloc_referent = "
			WITH t1 AS
			(
				SELECT DISTINCT
					interloc_referent.rel_saf_foncier_interlocs_id_ref,
					interloc_referent.rel_saf_foncier_interlocs_id,
					rel_saf_foncier_interlocs.rel_saf_foncier_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_ref)
					JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (t_temp_ref.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id)
					JOIN animation_fonciere.d_type_interloc t_temp_type ON (t_temp_type.type_interloc_id = t_temp_ref.type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc t_temp_groupe ON (t_temp_groupe.groupe_interloc_id = t_temp_type.groupe_interloc_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					rel_saf_foncier_interlocs.interloc_id = $interloc_id AND
					d_groupe_interloc.can_be_ref = 't' AND
					t_temp_groupe.can_be_under_ref = 't'
			)

			INSERT INTO animation_fonciere.interloc_referent
				(
					SELECT DISTINCT
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
						t1.rel_saf_foncier_interlocs_id
						
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN t1 USING (rel_saf_foncier_id)
					WHERE
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND
						interloc_id = $interloc_id AND
						can_be_ref = 't' AND
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id_ref FROM t1)
				)";
			$res_insert_interloc_referent = $bdd->prepare($rq_insert_interloc_referent);
			$res_insert_interloc_referent->execute();
			$res_insert_interloc_referent->closeCursor();
		}
		
		$result['entite_af_id'][] = $entite_af_id;
		// On doit mettre à jour les EAF seulement si l'interlocuteur associé est un véritable propriétaire
		If ($type_interloc == 1) {
			//On recherche une EAF qui correspond au nouveau groupement de propriétaires
			$rq_new_entite_af_id = "
			SELECT
				t2.entite_af_id as new_entite_af_id
			FROM
				(SELECT DISTINCT 
					interloc_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				WHERE
					rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id.") AND
					groupe_interloc_id = 1
				) t1,
				(SELECT 
					entite_af_id, 
					array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
				FROM
					(SELECT DISTINCT
						entite_af_id,
						interloc_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE 
						groupe_interloc_id = 1 
						AND entite_af_id != $entite_af_id
					) t2
				GROUP BY entite_af_id) t2
			GROUP BY t2.entite_af_id, t2.lst_interloc_id
			HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)";
			$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
			// echo $rq_new_entite_af_id;
			$res_new_entite_af_id->execute();
			$new_entite_af_id_temp = $res_new_entite_af_id->fetchall();
					
			//Si une EAF est identifiée on met à jour les parcelles concernées avec la nouvelle EAF
			If ($res_new_entite_af_id->rowCount() == 1) {
				$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];
				
				$rq_check_ref_unique = "
				SELECT DISTINCT
					rel_saf_foncier_interlocs.interloc_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id) 
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id) 
					JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
				WHERE
					session_af_id = $session_af_id
					AND entite_af_id = $new_entite_af_id";
				$res_check_ref_unique = $bdd->prepare($rq_check_ref_unique);
				// echo $rq_check_ref_unique;
				$res_check_ref_unique->execute();
				$check_ref_unique = $res_check_ref_unique->fetch();
				
				If ($res_check_ref_unique->rowCount() == 1 && $new_entite_af_id != $entite_af_id) {
					$rq_delete_ref_interloc = "
					DELETE FROM animation_fonciere.interloc_referent 
					WHERE 
						rel_saf_foncier_interlocs_id_ref IN (
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							session_af_id = $session_af_id AND entite_af_id = $entite_af_id)";
					// echo $rq_delete_ref_interloc;
					$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
					$res_delete_ref_interloc->execute();	
					$res_delete_ref_interloc->closeCursor();
					
					$rq_insert_interloc_referent = "
					INSERT INTO animation_fonciere.interloc_referent (
						SELECT
							t1.rel_saf_foncier_interlocs_id,
							t2.rel_saf_foncier_interlocs_id
						FROM
							(SELECT DISTINCT	
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id) 
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = $session_af_id
								AND entite_af_id = $entite_af_id
								AND interloc_id = ".$check_ref_unique['interloc_id'].") t1,						
							JOIN
							(SELECT DISTINCT	
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id) 
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = $session_af_id
								AND entite_af_id = $entite_af_id
								AND interloc_id != ".$check_ref_unique['interloc_id'].") t2 USING (rel_saf_foncier_id)
					)";
					// echo $rq_insert_interloc_referent;
					$res_insert_interloc_referent = $bdd->prepare($rq_insert_interloc_referent);
					$res_insert_interloc_referent->execute();
					$res_insert_interloc_referent->closeCursor();
				} Else If ($res_check_ref_unique->rowCount() > 1 && $new_entite_af_id != $entite_af_id) {
					$rq_delete_ref_interloc = "
					DELETE FROM animation_fonciere.interloc_referent 
					WHERE 
						rel_saf_foncier_interlocs_id_ref IN 
							(
								SELECT rel_saf_foncier_interlocs_id
								FROM 
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								WHERE 
									session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id)
							)
						OR rel_saf_foncier_interlocs_id IN 
							(
								SELECT rel_saf_foncier_interlocs_id
								FROM 
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								WHERE 
									session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id)
							)";
					// echo $rq_delete_ref_interloc;
					$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
					$res_delete_ref_interloc->execute();	
					$res_delete_ref_interloc->closeCursor();
				}
			} Else If ($res_new_entite_af_id->rowCount() > 1) {
				//Si on entre dans cette boucle, une erreur dans le système a doublonner ou plus une même EAF. Il faut rétablir la situation
				$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];
				$lst_eaf_to_update = array_shift(array_values(array_column($new_entite_af_id_temp, 'new_entite_af_id')));
				$rq_update_bad_eaf = "
				UPDATE
					animation_fonciere.rel_eaf_foncier 
				SET 
					entite_af_id = $new_entite_af_id
				WHERE
					entite_af_id IN (".implode(',', $lst_eaf_to_update).")";
				// echo $rq_update_bad_eaf;
				$res_update_bad_eaf = $bdd->prepare($rq_update_bad_eaf);
				$res_update_bad_eaf->execute();	
				$res_update_bad_eaf->closeCursor();	
				
				$rq_reset_prix_eaf = "
				UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
					(
						SELECT 
							rel_eaf_foncier_id
						FROM
							animation_fonciere.rel_eaf_foncier
							JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id OR entite_af_id IN (".implode(',', $lst_eaf_to_update)."))
					)";
				$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
				// echo $rq_reset_prix_eaf.'<br>';
				$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
				$res_reset_prix_eaf->closeCursor();	
			} Else {
				$rq_check_parc_saf = "
				SELECT
					'complete'
				FROM
					(SELECT DISTINCT
						cad_site_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id.")) t1,
					(SELECT DISTINCT
						cad_site_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						entite_af_id = $entite_af_id AND session_af_id = $session_af_id) t2
				HAVING array_agg(t1.cad_site_id ORDER BY t1.cad_site_id) = array_agg(t2.cad_site_id ORDER BY t2.cad_site_id)";
				$res_check_parc_saf = $bdd->prepare($rq_check_parc_saf);
				// echo $rq_check_parc_saf;
				$res_check_parc_saf->execute();
				
				If ($res_check_parc_saf->rowCount() > 0) {
					$new_entite_af_id = $entite_af_id;						
				} Else {
					$rq_new_entite_af_id = "SELECT nextval('animation_fonciere.entite_af_id_seq'::regclass)";
					$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
					// echo $rq_new_entite_af_id;
					$res_new_entite_af_id->execute();
					$new_entite_af_id_temp = $res_new_entite_af_id->fetch();
					$new_entite_af_id = $new_entite_af_id_temp[0];	
					
					//on intègre la nouvelle EAF
					$rq_insert_entite_eaf = "INSERT INTO animation_fonciere.entite_af (entite_af_id) VALUES ($new_entite_af_id)";
					$res_insert_entite_eaf = $bdd->prepare($rq_insert_entite_eaf);
					// echo $rq_insert_entite_eaf;
					$res_insert_entite_eaf->execute();
					$res_insert_entite_eaf->closeCursor();
				}
			}
			
			$result['entite_af_id'][] = $new_entite_af_id;
			
			If ($new_entite_af_id != $entite_af_id) {
				//On reset les prix par eaf car la liste de parcelles a changé
				$rq_reset_prix_eaf = "
				UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
					(
						SELECT 
							rel_eaf_foncier_id
						FROM
							animation_fonciere.rel_eaf_foncier
							JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id)
					)";
				$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
				// echo $rq_reset_prix_eaf.'<br>';
				$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
				$res_reset_prix_eaf->closeCursor();		
				
				$rq_update_rel_eaf_foncier = "
				UPDATE 
					animation_fonciere.rel_eaf_foncier 
				SET 
					entite_af_id = $new_entite_af_id
				WHERE
					entite_af_id = $entite_af_id AND rel_eaf_foncier_id IN (
						SELECT
							rel_eaf_foncier_id
						FROM
							animation_fonciere.rel_saf_foncier
						WHERE
							rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id.")
						)";
				// echo $rq_update_rel_eaf_foncier;
				$res_update_rel_eaf_foncier = $bdd->prepare($rq_update_rel_eaf_foncier);
				$res_update_rel_eaf_foncier->execute();	
				$res_update_rel_eaf_foncier->closeCursor();
			}
		}	
		
		//on simplifie le tableau des entite_af_id pour eliminer les doublons lorsque l'EAF identifiée est l'EAF d'origine
		$result['entite_af_id'] = array_values(array_unique($result['entite_af_id']));			
			
		//On peut supprimer les eaf qui ne sont plus utilisés dans la table entite_af
		$rq_delete_entite_af = "
		DELETE FROM animation_fonciere.entite_af WHERE entite_af_id NOT IN (SELECT entite_af_id FROM animation_fonciere.rel_eaf_foncier)";
		$res_delete_entite_af = $bdd->prepare($rq_delete_entite_af);
		$res_delete_entite_af->execute();
		$res_delete_entite_af->closeCursor();
			
		// Pour toutes les EAF modifiées ou crées il faut mettre à jour la page avec AJAX.	
		For ($e=0; $e<count($result['entite_af_id']); $e++) {
			$result['entite_af_code'][$e] = 'N.D.';		
			$rq_infos_eaf = "
			SELECT DISTINCT 
				array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
				array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
				array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
				t1.nbr_echanges
			FROM 		
				animation_fonciere.rel_eaf_foncier
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.cadastre_site t4 USING (cad_site_id)
				JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
				JOIN 
					(SELECT DISTINCT 
						entite_af_id,
						count(echange_af_id) as nbr_echanges
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
					GROUP BY entite_af_id
					) t1 USING (entite_af_id)		
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND
				entite_af_id= :entite_af_id AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'
			GROUP BY 
				entite_af_id,
				t1.nbr_echanges";											
			$res_infos_eaf = $bdd->prepare($rq_infos_eaf);
			// echo $rq_infos_eaf;
			$res_infos_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$result['entite_af_id'][$e]));
			$infos_eaf = $res_infos_eaf->fetch();
			
			If ($res_infos_eaf->rowCount() > 0) {
				$result['entite_af_code'][$e] = "
					<td style='border-right: 1px solid #ccc; border-bottom: 4px solid #ccc;'>
						<table id='interloc_tbl_lots_".$result['entite_af_id'][$e]."' CELLSPACING = '1' CELLPADDING ='0' class='no-border'>";
												
				$rq_info_lots = "
					SELECT DISTINCT
						t4.cad_site_id,
						t2.lot_id,
						t2.dcntlo,
						COALESCE(rel_eaf_foncier.utilissol, 'N.D.') AS utilissol,
						rel_saf_foncier.paf_id,
						d_paf.paf_lib
					FROM
						cadastre.lots_cen t2
						JOIN cadastre.cadastre_cen t3 USING (lot_id)
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.d_paf USING (paf_id)
					WHERE 
						entite_af_id = :entite_af_id AND 
						rel_saf_foncier.session_af_id = :session_af_id AND
						t3.date_fin = 'N.D.' AND
						t4.date_fin = 'N.D.'
					ORDER BY t2.lot_id";
				$res_info_lots = $bdd->prepare($rq_info_lots);
				$res_info_lots->execute(array('entite_af_id'=>$result['entite_af_id'][$e], 'session_af_id'=>$session_af_id));
				$info_lots = $res_info_lots->fetchall();
				For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 
					$result['entite_af_code'][$e] .= "
							<tr>
								<td class='no-border' style='margin-top:5px;'>
									<label class='label'>
										<u>Lot ID :</u> ".$info_lots[$l]['lot_id']."
									</label>
								</td>
								<td>
									<img id='interloc_btn_detail-".$info_lots[$l]['lot_id']."' src='".ROOT_PATH."images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick=\"showDetail('interloc', '".$info_lots[$l]['lot_id']."');\">															
								</td>
							</tr>
							<tr>
								<td id='interloc_td_detail-".$info_lots[$l]['lot_id']."' style='display:none' colspan='2'>
									<table class='no-border'>
										<tr>
											<td class='no-border'>
												<label class='label'>
													<u>Contenance :</u> ".conv_are($info_lots[$l]['dcntlo'])."
												</label>
											</td>
										</tr>
										<tr>
											<td class='no-border'>
												<label class='label'>
													<u>Utilisation du sol :</u> 
												</label>
										</tr>
										<tr>
											<td style='padding-left:10px;'>
												<label class='label'>
													".retour_ligne($info_lots[$l]['utilissol'], 40)."
												</label>
											</td>
										</tr>
										<tr>
											<td class='no-border'>";
													$rq_nat_cult = 
													"SELECT DISTINCT
														d_dsgrpf.natcult_ssgrp,
														d_dsgrpf.cgrnum,
														lots_natcult_cen.dcntsf as surf_nat_cult,
														CASE WHEN d_cnatsp.natcult_natspe IS NOT NULL THEN d_cnatsp.natcult_natspe ELSE 'N.D.' END as natcult_natspe,
														d_dsgrpf.dsgrpf
													FROM
														cadastre.lots_cen t2	
														JOIN cadastre.lots_natcult_cen USING (lot_id)
														LEFT JOIN cadastre.d_cnatsp USING (cnatsp)
														JOIN cadastre.d_dsgrpf USING (dsgrpf)
													WHERE t2.lot_id = ?
													ORDER BY surf_nat_cult DESC";
													$res_nat_cult = $bdd->prepare($rq_nat_cult);
													$res_nat_cult->execute(array($info_lots[$l]['lot_id']));					
													$nat_cult = $res_nat_cult->fetchall();
													
												$result['entite_af_code'][$e] .= "
												<table style='border:0 solid black'>
													<tr>
														<td style='vertical-align:top'>
															<label class='label' style='margin-left:-3px'><u>Natures de culture : </u></label> 
														</td>
													</tr>
													<tr>
														<td>
															<table CELLSPACING = '0' CELLPADDING ='0' style='border:0 solid black;margin-left:5px;'>";
														If (count($nat_cult) == 0) {
															$result['entite_af_code'][$e] .= "<td><label class='label'>N.D.</label></td>";
														} Else {
															For ($n=0; $n<count($nat_cult); $n++) { 
																$result['entite_af_code'][$e] .= "
																<tr>
																	<td style='text-align:left'>
																		<label class='label'>
																			".retour_ligne($nat_cult[$n]['natcult_ssgrp'] . ' : ' . conv_are($nat_cult[$n]['surf_nat_cult']) . ' (' . round(($nat_cult[$n]['surf_nat_cult']*100)/$info_lots[$l]['dcntlo'], 0) . '%)', 40)."
																		</label>
																	</td>
																</tr>";
																If ($nat_cult[$n]['natcult_natspe'] != 'N.D.') { 
																$result['entite_af_code'][$e] .= "
																<tr>
																	<td style='text-align:left;'>
																		<label class='label' style='font-size:9pt;font-weight:normal;'>
																			".'(' . $nat_cult[$n]['natcult_natspe'] . ')'."
																		</label>
																	</td>
																</tr>";
																} 
															}
														} 
															$result['entite_af_code'][$e] .= "
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class='no-border'>
												<label class='label'>
													<u>Priorit&eacute; d'animation :</u> ".($info_lots[$l]['paf_id'])."
												</label>
											</td>
										</tr>
									</table>
								</td>
							</tr>";		
					}
						$result['entite_af_code'][$e] .= "
						</table>
					</td>
					<td style='border-bottom: 4px solid #ccc;'>
						<center>
							<table style='float:none;' CELLSPACING = '5' CELLPADDING ='5' class='no-border'>
								<tr>															
									<td>
										<table cellspacing='0' cellpadding='8' id='tbl_".$result['entite_af_id'][$e]."' class='no-border'>";
										
										$result['entite_af_code'][$e] .= fct_rq_lst_interlocs($session_af_id, $result['entite_af_id'][$e], $infos_eaf['tbl_eaf_lot_id'], $infos_eaf['tbl_rel_saf_foncier_id']);
			}
		}
		
		$bdd = null;		
		print json_encode($result);
	}
	
	// Cette fonction permet de supprimer un interlocuteur associé à un propriétaire depuis la page fiche_saf.php
	function fct_deleteInterlocConfirm($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		$tbl_rel_saf_foncier_interlocs_id = $data['params']['var_tbl_rel_saf_foncier_interlocs_id']; //Identifiant de la ligne à supprimer depuis rel_af_proprios_interlocs
		$interloc_id = $data['params']['var_interloc_id']; //Identifiant de l'interlocuteur
		$session_af_id = $data['params']['var_session_af_id']; //Identifiant de la session d'animation
		$entite_af_id = $data['params']['var_entite_af_id']; //Identifiant du compte de propriété
		$type_interloc_id = $data['params']['var_type_interloc_id']; //Identifiant du compte de propriété
		$groupe_interloc_id = $data['params']['var_groupe_interloc_id']; //Identifiant du compte de propriété
		
		$rq_delete_ref_interloc = "
		WITH t1 AS 
		(
			SELECT 
				rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				session_af_id = :session_af_id AND
				entite_af_id = :entite_af_id AND
				interloc_id = :interloc_id AND
				type_interloc_id = :type_interloc_id
		)
		DELETE FROM animation_fonciere.interloc_referent 
		WHERE 
			rel_saf_foncier_interlocs_id_ref IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1) OR
			rel_saf_foncier_interlocs_id IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1)";
		// echo $rq_delete_ref_interloc;
		$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
		$res_delete_ref_interloc->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'interloc_id'=>$interloc_id, 'type_interloc_id'=>$type_interloc_id));	
		$res_delete_ref_interloc->closeCursor();	
		
		$rq_delete_interloc_substitution = "
		WITH t1 AS 
		(
			SELECT 
				rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				session_af_id = :session_af_id AND
				entite_af_id = :entite_af_id AND
				interloc_id = :interloc_id AND
				type_interloc_id = :type_interloc_id
		)
		DELETE FROM animation_fonciere.interloc_substitution 
		WHERE 
			rel_saf_foncier_interlocs_id_vp IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1) OR 
			rel_saf_foncier_interlocs_id_is IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1)";
		// echo $rq_delete_ref_interloc;
		$res_delete_interloc_substitution = $bdd->prepare($rq_delete_interloc_substitution);
		$res_delete_interloc_substitution->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'interloc_id'=>$interloc_id, 'type_interloc_id'=>$type_interloc_id));	
		$res_delete_interloc_substitution->closeCursor();
		
		$rq_delete_interloc = "
		DELETE FROM animation_fonciere.rel_saf_foncier_interlocs 
		WHERE 
			rel_saf_foncier_interlocs_id IN 
				(
					SELECT 
						rel_saf_foncier_interlocs_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						session_af_id = :session_af_id AND
						entite_af_id = :entite_af_id AND
						interloc_id = :interloc_id AND
						type_interloc_id = :type_interloc_id
				)";
		// echo $rq_delete_interloc;
		$res_delete_interloc = $bdd->prepare($rq_delete_interloc);
		$res_delete_interloc->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'interloc_id'=>$interloc_id, 'type_interloc_id'=>$type_interloc_id));	
		$res_delete_interloc->closeCursor();		
		
		$result['entite_af_id'][] = $entite_af_id;
		If ($groupe_interloc_id == 1) {
			//Il faut maintenant contrôler les EAF et proceder au réassemblage si besoin
			//On recherche une EAF qui correspond au nouveau groupement de propriétaires
			$rq_new_entite_af_id = "
			SELECT
				t2.entite_af_id as new_entite_af_id
			FROM
				(SELECT DISTINCT 
					interloc_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				WHERE
					entite_af_id = $entite_af_id AND interloc_id != $interloc_id AND groupe_interloc_id = 1) t1,
				(SELECT 
					entite_af_id, 
					array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
				FROM
					(SELECT DISTINCT
						entite_af_id,
						interloc_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE groupe_interloc_id = 1 AND entite_af_id != $entite_af_id) t2
				GROUP BY entite_af_id) t2
			GROUP BY t2.entite_af_id, t2.lst_interloc_id
			HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)";
			$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
			// echo $rq_new_entite_af_id;
			$res_new_entite_af_id->execute();
			$new_entite_af_id_temp = $res_new_entite_af_id->fetchall();
			
			// Si on obtient un résultat, on met à jour l'EAF sinon il faut en créer une nouvelle
			If ($res_new_entite_af_id->rowCount() == 1) {
				$new_entite_af_id = $new_entite_af_id_temp['new_entite_af_id'];

				$rq_check_ref_unique = "
				SELECT DISTINCT
					rel_saf_foncier_interlocs.interloc_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id) 
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id) 
					JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
				WHERE
					session_af_id = $session_af_id
					AND entite_af_id = $new_entite_af_id";
				$res_check_ref_unique = $bdd->prepare($rq_check_ref_unique);
				// echo $rq_check_ref_unique;
				$res_check_ref_unique->execute();
				$check_ref_unique = $res_check_ref_unique->fetch();
				
				If ($res_check_ref_unique->rowCount() == 1 && $new_entite_af_id != $entite_af_id) {
					$rq_delete_ref_interloc = "
					DELETE FROM animation_fonciere.interloc_referent 
					WHERE 
						rel_saf_foncier_interlocs_id_ref IN (
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							session_af_id = $session_af_id AND entite_af_id = $entite_af_id)";
					// echo $rq_delete_ref_interloc;
					$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
					$res_delete_ref_interloc->execute();	
					$res_delete_ref_interloc->closeCursor();
					
					$rq_insert_interloc_referent = "
					INSERT INTO animation_fonciere.interloc_referent (
						SELECT
							t1.rel_saf_foncier_interlocs_id,
							t2.rel_saf_foncier_interlocs_id
						FROM
							(SELECT DISTINCT	
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id) 
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = $session_af_id
								AND entite_af_id = $entite_af_id
								AND interloc_id = ".$check_ref_unique['interloc_id'].") t1,						
							JOIN
							(SELECT DISTINCT	
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id) 
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = $session_af_id
								AND entite_af_id = $entite_af_id
								AND interloc_id != ".$check_ref_unique['interloc_id'].") t2 USING (rel_saf_foncier_id)
					)";
					// echo $rq_insert_interloc_referent;
					$res_insert_interloc_referent = $bdd->prepare($rq_insert_interloc_referent);
					$res_insert_interloc_referent->execute();
					$res_insert_interloc_referent->closeCursor();
				} Else If ($res_check_ref_unique->rowCount() > 1 && $new_entite_af_id != $entite_af_id) {
					$rq_delete_ref_interloc = "
					DELETE FROM animation_fonciere.interloc_referent 
					WHERE 
						rel_saf_foncier_interlocs_id_ref IN (
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id) OR
						rel_saf_foncier_interlocs_id IN (
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id))";
					// echo $rq_delete_ref_interloc;
					$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
					$res_delete_ref_interloc->execute();	
					$res_delete_ref_interloc->closeCursor();
				}
			} Else If ($res_new_entite_af_id->rowCount() > 1) {
				//Si on entre dans cette boucle, une erreur dans le système a doublonner ou plus une même EAF. Il faut rétablir la situation
				$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];
				$lst_eaf_to_update = array_shift(array_values(array_column($new_entite_af_id_temp, 'new_entite_af_id')));
				$rq_update_bad_eaf = "
				UPDATE
					animation_fonciere.rel_eaf_foncier 
				SET 
					entite_af_id = $new_entite_af_id
				WHERE
					entite_af_id IN (".implode(',', $lst_eaf_to_update).")";
				// echo $rq_update_bad_eaf;
				$res_update_bad_eaf = $bdd->prepare($rq_update_bad_eaf);
				$res_update_bad_eaf->execute();	
				$res_update_bad_eaf->closeCursor();
				
				$rq_reset_prix_eaf = "
				UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
					(
						SELECT 
							rel_eaf_foncier_id
						FROM
							animation_fonciere.rel_eaf_foncier
							JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id OR entite_af_id IN (".implode(',', $lst_eaf_to_update)."))
					)";
				$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
				// echo $rq_reset_prix_eaf.'<br>';
				$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
				$res_reset_prix_eaf->closeCursor();
			} Else {
				$rq_check_parc_saf = "
				SELECT
					'complete'
				FROM
					(SELECT DISTINCT
						cad_site_id
					FROM
						animation_fonciere.rel_eaf_foncier
					WHERE
						entite_af_id = $entite_af_id) t1,
					(SELECT DISTINCT
						cad_site_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						entite_af_id = $entite_af_id AND session_af_id = $session_af_id) t2
				HAVING array_agg(t1.cad_site_id ORDER BY t1.cad_site_id) = array_agg(t2.cad_site_id ORDER BY t2.cad_site_id)";
				$res_check_parc_saf = $bdd->prepare($rq_check_parc_saf);
				// echo $rq_check_parc_saf;
				$res_check_parc_saf->execute();
			
				If ($res_check_parc_saf->rowCount() == 1) {
					$new_entite_af_id = $entite_af_id;						
				} Else {
					$rq_new_entite_af_id = "SELECT nextval('animation_fonciere.entite_af_id_seq'::regclass)";
					$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
					// echo $rq_new_entite_af_id;
					$res_new_entite_af_id->execute();
					$new_entite_af_id_temp = $res_new_entite_af_id->fetch();
					$new_entite_af_id = $new_entite_af_id_temp[0];	
					
					//on intègre la nouvelle EAF
					$rq_insert_entite_eaf = "INSERT INTO animation_fonciere.entite_af (entite_af_id) VALUES ($new_entite_af_id)";
					$res_insert_entite_eaf = $bdd->prepare($rq_insert_entite_eaf);
					// echo $rq_insert_entite_eaf;
					$res_insert_entite_eaf->execute();
					$res_insert_entite_eaf->closeCursor();					
				}
			}
			$res_new_entite_af_id->closeCursor();
			
			$result['entite_af_id'][] = $new_entite_af_id;
			
			If ($new_entite_af_id != $entite_af_id) {
				//On reset les prix par eaf car la liste de parcelles a changé
				$rq_reset_prix_eaf = "
				UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
					(
						SELECT 
							rel_eaf_foncier_id
						FROM
							animation_fonciere.rel_eaf_foncier
							JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id)
					)";
				$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
				// echo $rq_reset_prix_eaf.'<br>';
				$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
				$res_reset_prix_eaf->closeCursor();	
				
				$rq_update_rel_eaf_foncier = "
				UPDATE 
					animation_fonciere.rel_eaf_foncier 
				SET 
					entite_af_id = $new_entite_af_id
				WHERE
					entite_af_id = $entite_af_id AND
					rel_eaf_foncier_id IN (
						SELECT 
							rel_eaf_foncier_id
						FROM 
							animation_fonciere.rel_saf_foncier
						WHERE session_af_id = $session_af_id)";
				// echo $rq_update_rel_eaf_foncier;
				$res_update_rel_eaf_foncier = $bdd->prepare($rq_update_rel_eaf_foncier);
				$res_update_rel_eaf_foncier->execute();	
				$res_update_rel_eaf_foncier->closeCursor();
			}
				
			//On peut supprimer les eaf qui ne sont plus utilisés dans la table entite_af
			$rq_delete_entite_af = "DELETE FROM animation_fonciere.entite_af WHERE entite_af_id NOT IN (SELECT entite_af_id FROM animation_fonciere.rel_eaf_foncier)";
			$res_delete_entite_af = $bdd->prepare($rq_delete_entite_af);
			// echo $rq_delete_entite_af;
			$res_delete_entite_af->execute();
			$res_delete_entite_af->closeCursor();	
		}	
		
		//on simplifie le tableau des entite_af_id pour eliminer les doublons lorsque l'EAF identifiée est l'EAF d'origine
		$result['entite_af_id'] = array_values(array_unique($result['entite_af_id']));
		// Pour toutes les EAF modifiées ou crées il faut mettre à jour la page avec AJAX.
		For ($e=0; $e<count($result['entite_af_id']); $e++) {
			$result['entite_af_code'][$e] = 'N.D.';
			$rq_infos_eaf = "
			SELECT DISTINCT 
				array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
				array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
				array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
				t1.nbr_echanges
			FROM 		
				animation_fonciere.rel_eaf_foncier
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.cadastre_site t4 USING (cad_site_id)
				JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
				LEFT JOIN 
					(SELECT DISTINCT 
						entite_af_id,
						count(echange_af_id) as nbr_echanges
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
					GROUP BY entite_af_id
					) t1 USING (entite_af_id)
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND 
				entite_af_id= :entite_af_id AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'
			GROUP BY 
				entite_af_id,
				t1.nbr_echanges";											
			$res_infos_eaf = $bdd->prepare($rq_infos_eaf);
			// echo $rq_infos_eaf;
			$res_infos_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$result['entite_af_id'][$e]));
			$infos_eaf = $res_infos_eaf->fetch();
			If ($res_infos_eaf->rowCount() > 0) {
				$result['entite_af_code'][$e] = "
				<td style='border-right: 1px solid #ccc; border-bottom: 4px solid #ccc;'>
					<table id='interloc_tbl_lots_".$result['entite_af_id'][$e]."' CELLSPACING = '1' CELLPADDING ='0' class='no-border'>";
						
				$rq_info_lots = "
					SELECT DISTINCT
						t4.cad_site_id,
						t2.lot_id,
						t2.dcntlo,
						COALESCE(rel_eaf_foncier.utilissol, 'N.D.') AS utilissol,
						rel_saf_foncier.paf_id,
						d_paf.paf_lib
					FROM
						cadastre.lots_cen t2
						JOIN cadastre.cadastre_cen t3 USING (lot_id)
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.d_paf USING (paf_id)
					WHERE 
						entite_af_id = :entite_af_id AND 
						rel_saf_foncier.session_af_id = :session_af_id AND
						t3.date_fin = 'N.D.' AND
						t4.date_fin = 'N.D.'
					ORDER BY t2.lot_id";
				$res_info_lots = $bdd->prepare($rq_info_lots);
				$res_info_lots->execute(array('entite_af_id'=>$result['entite_af_id'][$e], 'session_af_id'=>$session_af_id));
				$info_lots = $res_info_lots->fetchall();
				For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 
					$result['entite_af_code'][$e] .= "
							<tr>
								<td class='no-border' style='margin-top:5px;'>
									<label class='label'>
										<u>Lot ID :</u> ".$info_lots[$l]['lot_id']."
									</label>
								</td>
								<td>
									<img id='interloc_btn_detail-".$info_lots[$l]['lot_id']."' src='".ROOT_PATH."images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick=\"showDetail('interloc', '".$info_lots[$l]['lot_id']."');\">															
								</td>
							</tr>
							<tr>
								<td id='interloc_td_detail-".$info_lots[$l]['lot_id']."' style='display:none' colspan='2'>
									<table class='no-border'>
										<tr>
											<td class='no-border'>
												<label class='label'>
													<u>Contenance :</u> ".conv_are($info_lots[$l]['dcntlo'])."
												</label>
											</td>
										</tr>
										<tr>
											<td class='no-border'>
												<label class='label'>
													<u>Utilisation du sol :</u> 
												</label>
										</tr>
										<tr>
											<td style='padding-left:10px;'>
												<label class='label'>
													".retour_ligne($info_lots[$l]['utilissol'], 40)."
												</label>
											</td>
										</tr>
										<tr>
											<td class='no-border'>";
													$rq_nat_cult = 
													"SELECT DISTINCT
														d_dsgrpf.natcult_ssgrp,
														d_dsgrpf.cgrnum,
														lots_natcult_cen.dcntsf as surf_nat_cult,
														CASE WHEN d_cnatsp.natcult_natspe IS NOT NULL THEN d_cnatsp.natcult_natspe ELSE 'N.D.' END as natcult_natspe,
														d_dsgrpf.dsgrpf
													FROM
														cadastre.lots_cen t2	
														JOIN cadastre.lots_natcult_cen USING (lot_id)
														LEFT JOIN cadastre.d_cnatsp USING (cnatsp)
														JOIN cadastre.d_dsgrpf USING (dsgrpf)
													WHERE t2.lot_id = ?
													ORDER BY surf_nat_cult DESC";
													$res_nat_cult = $bdd->prepare($rq_nat_cult);
													$res_nat_cult->execute(array($info_lots[$l]['lot_id']));					
													$nat_cult = $res_nat_cult->fetchall();
													
												$result['entite_af_code'][$e] .= "
												<table style='border:0 solid black'>
													<tr>
														<td style='vertical-align:top'>
															<label class='label' style='margin-left:-3px'><u>Natures de culture : </u></label> 
														</td>
													</tr>
													<tr>
														<td>
															<table CELLSPACING = '0' CELLPADDING ='0' style='border:0 solid black;margin-left:5px;'>";
														If (count($nat_cult) == 0) {
															$result['entite_af_code'][$e] .= "<td><label class='label'>N.D.</label></td>";
														} Else {
															For ($n=0; $n<count($nat_cult); $n++) { 
																$result['entite_af_code'][$e] .= "
																<tr>
																	<td style='text-align:left'>
																		<label class='label'>
																			".retour_ligne($nat_cult[$n]['natcult_ssgrp'] . ' : ' . conv_are($nat_cult[$n]['surf_nat_cult']) . ' (' . round(($nat_cult[$n]['surf_nat_cult']*100)/$info_lots[$l]['dcntlo'], 0) . '%)', 40)."
																		</label>
																	</td>
																</tr>";
																If ($nat_cult[$n]['natcult_natspe'] != 'N.D.') { 
																$result['entite_af_code'][$e] .= "
																<tr>
																	<td style='text-align:left;'>
																		<label class='label' style='font-size:9pt;font-weight:normal;'>
																			".'(' . $nat_cult[$n]['natcult_natspe'] . ')'."
																		</label>
																	</td>
																</tr>";
																} 
															}
														} 
															$result['entite_af_code'][$e] .= "
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class='no-border'>
												<label class='label'>
													<u>Priorit&eacute; d'animation :</u> ".($info_lots[$l]['paf_id'])."
												</label>
											</td>
										</tr>
									</table>
								</td>
							</tr>";		
					}
						$result['entite_af_code'][$e] .= "
						</table>
					</td>
					<td style='border-bottom: 4px solid #ccc;'>
						<center>
							<table style='float:none;' CELLSPACING = '5' CELLPADDING ='5' class='no-border'>
								<tr>															
									<td>
										<table cellspacing='0' cellpadding='8' id='tbl_".$result['entite_af_id'][$e]."' class='no-border'>";
										$result['entite_af_code'][$e] .= fct_rq_lst_interlocs($session_af_id, $result['entite_af_id'][$e], $infos_eaf['tbl_eaf_lot_id'], $infos_eaf['tbl_rel_saf_foncier_id']);
			}		
		}
		
		$bdd = null;
		print json_encode($result);
	}
	
	// Cette fonction permet d'afficher un résumé des informations liées à un échange avant de confirmer la suppression 
	function fct_deleteEchangeUpdateLabel($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$interloc_id = $data['params']['var_interloc_id']; //Identifiant de l'interlocuteur
		$session_af_id = $data['params']['var_session_af_id']; //Identifiant de la session d'animation
		$entite_af_id = $data['params']['var_entite_af_id']; //Identifiant du compte de propriété
		$echange_af_id = $data['params']['var_echange_af_id']; //Identifiant du compte de propriété
		$echange_direct = $data['params']['var_echange_direct']; //Identifiant du compte de propriété
		
		$rq_info_echange = "
		SELECT DISTINCT
			count(DISTINCT interloc_id) as nbr_interloc,
			entite_af_id
		FROM
			animation_fonciere.rel_af_echanges
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
		WHERE
			echange_af_id = :echange_af_id AND
			session_af_id = :session_af_id
		GROUP BY entite_af_id";
		// echo $rq_info_echange;
		$res_info_echange = $bdd->prepare($rq_info_echange);
		$res_info_echange->execute(array('echange_af_id'=>$echange_af_id, 'session_af_id'=>$session_af_id));
		$info_echange = $res_info_echange->fetchAll();
		
		$result['nbr_interloc_current_eaf'] = 0;
		$result['nbr_interloc_other_eaf'] = 0;
		$result['nbr_eaf'] = 0;
		
		For ($i=0; $i<$res_info_echange->rowCount(); $i++) {
			If ($info_echange[$i]['entite_af_id'] == $entite_af_id) {
				$result['nbr_interloc_current_eaf'] = $info_echange[$i]['nbr_interloc'];
			} Else {
				$result['nbr_interloc_other_eaf'] = $result['nbr_interloc_other_eaf'] + $info_echange[$i]['nbr_interloc'];
				$result['nbr_eaf'] = $result['nbr_eaf'] + $info_echange[$i]['nbr_interloc'];
			}
		}
		
		$bdd = null;
		print json_encode($result);		
	}
	
	// Cette fonction permet supprimer un échange
	function fct_deleteEchangeConfirm($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$mode = $data['params']['var_mode']; //Identifiant de la ligne à supprimer depuis rel_af_proprios_interlocs
		$interloc_id = $data['params']['var_interloc_id']; //Identifiant de l'interlocuteur
		$session_af_id = $data['params']['var_session_af_id']; //Identifiant de la session d'animation
		$entite_af_id = $data['params']['var_entite_af_id']; //Identifiant du compte de propriété
		$echange_af_id = $data['params']['var_echange_af_id']; //Identifiant du compte de propriété
		$echange_direct = $data['params']['var_echange_direct']; //Identifiant du compte de propriété
		
		$rq_info_echange = "
		SELECT DISTINCT
			interlocuteurs.interloc_id,
			interlocuteurs.interloc_id as subst_interloc_id,
			interlocuteurs.nom_usage,
			interlocuteurs.ddenom,
			entite_af_id
		FROM
			animation_fonciere.rel_af_echanges
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
			JOIN animation_fonciere.interlocuteurs USING (interloc_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
		WHERE
			echange_af_id = :echange_af_id AND
			session_af_id = :session_af_id";
		If ($mode == 'one') {
			$rq_info_echange .= " AND interlocuteurs.interloc_id = $interloc_id AND entite_af_id = $entite_af_id";
		}
		$rq_info_echange .= "
		UNION
		SELECT DISTINCT
			interlocuteurs.interloc_id,
			t_temp_subst.interloc_id as subst_interloc_id,
			interlocuteurs.nom_usage,
			interlocuteurs.ddenom,
			entite_af_id
		FROM
			animation_fonciere.rel_af_echanges
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
			JOIN animation_fonciere.interlocuteurs USING (interloc_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp
		= rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
		WHERE
			echange_af_id = :echange_af_id AND
			session_af_id = :session_af_id";
		If ($mode == 'one') {
			$rq_info_echange .= " AND t_temp_subst.interloc_id = $interloc_id AND entite_af_id = $entite_af_id";
		}
		// echo $rq_info_echange;
		$res_info_echange = $bdd->prepare($rq_info_echange);
		$res_info_echange->execute(array('echange_af_id'=>$echange_af_id, 'session_af_id'=>$session_af_id));
		$info_echange = $res_info_echange->fetchAll();
		For ($i=0; $i<$res_info_echange->rowCount(); $i++) {
			$result['tbl_echange_id'][$i] = $info_echange[$i]['subst_interloc_id'].'_'.$info_echange[$i]['entite_af_id'].'_'.$echange_af_id;
			
			$rq_delete_echange = "
			DELETE FROM animation_fonciere.rel_af_echanges 
			WHERE
				rel_af_echanges_id IN 
					(
						SELECT 
							rel_af_echanges_id
						FROM
							animation_fonciere.rel_af_echanges
							JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							echange_af_id = :echange_af_id AND
							session_af_id = :session_af_id AND
							entite_af_id = :entite_af_id AND
							interloc_id = :interloc_id
					)";
			$res_delete_echange = $bdd->prepare($rq_delete_echange);
			$res_delete_echange->execute(array('echange_af_id'=>$echange_af_id, 'session_af_id'=>$session_af_id, 'entite_af_id'=>$info_echange[$i]['entite_af_id'], 'interloc_id'=>$info_echange[$i]['interloc_id']));
		}
		
		$rq_clean_unused_echange = "
		DELETE FROM animation_fonciere.echanges_af
		WHERE
			echange_af_id NOT IN 
				(
					SELECT DISTINCT
						echange_af_id
					FROM
						animation_fonciere.rel_af_echanges
				)";
		$res_clean_unused_echange = $bdd->prepare($rq_clean_unused_echange);
		$res_clean_unused_echange->execute();
		
		$bdd = null;
		print json_encode($result);		
	}
	
	// Cette fonction est utilisée dans la page fiche_saf.php pour supprimer tout un compte de prorpiété d'une saf
	function fct_deleteEAF($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		
		$rq_delete_ref_interloc = "
		WITH t1 AS 
		(
			SELECT 
				rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				session_af_id = :session_af_id AND
				entite_af_id = :entite_af_id
		)
		DELETE FROM animation_fonciere.interloc_referent 
		WHERE 
			rel_saf_foncier_interlocs_id_ref IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1) OR
			rel_saf_foncier_interlocs_id IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1)";
		// echo $rq_delete_ref_interloc;
		$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
		$res_delete_ref_interloc->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));	
		$res_delete_ref_interloc->closeCursor();	
		
		$rq_delete_interloc_substitution = "
		WITH t1 AS 
		(
			SELECT 
				rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				session_af_id = :session_af_id AND
				entite_af_id = :entite_af_id
		)
		DELETE FROM animation_fonciere.interloc_substitution 
		WHERE 
			rel_saf_foncier_interlocs_id_vp IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1) OR 
			rel_saf_foncier_interlocs_id_is IN (SELECT t1.rel_saf_foncier_interlocs_id FROM t1)";
		// echo $rq_delete_ref_interloc;
		$res_delete_interloc_substitution = $bdd->prepare($rq_delete_interloc_substitution);
		$res_delete_interloc_substitution->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));	
		$res_delete_interloc_substitution->closeCursor();
		
		$rq_delete_rel_saf_foncier_interlocs = "
		DELETE FROM animation_fonciere.rel_saf_foncier_interlocs WHERE rel_saf_foncier_interlocs_id IN 
		(
			SELECT 
				rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE 
				session_af_id = $session_af_id AND 
				entite_af_id = $entite_af_id
		)";
		// echo $rq_delete_rel_saf_foncier_interlocs;
		$res_delete_rel_saf_foncier_interlocs = $bdd->prepare($rq_delete_rel_saf_foncier_interlocs);
		$res_delete_rel_saf_foncier_interlocs->execute();	
		$res_delete_rel_saf_foncier_interlocs->closeCursor();

		$rq_delete_rel_saf_foncier = "
		DELETE FROM animation_fonciere.rel_saf_foncier WHERE rel_saf_foncier_id IN 
		(
			SELECT 
				rel_saf_foncier_id
			FROM
				animation_fonciere.rel_saf_foncier
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE 
				session_af_id = $session_af_id AND 
				entite_af_id = $entite_af_id
		)";
		// echo $rq_delete_rel_af_foncier;
		$res_delete_rel_saf_foncier = $bdd->prepare($rq_delete_rel_saf_foncier);
		$res_delete_rel_saf_foncier->execute();	
		$res_delete_rel_saf_foncier->closeCursor();	

		$rq_update_saf_geom = "
		UPDATE animation_fonciere.session_af SET 
			geom = (
				SELECT 
					st_multi(st_union(t1.geom)) as geom
				FROM
					cadastre.parcelles_cen t1
					JOIN cadastre.lots_cen t2 USING (par_id) 
					JOIN cadastre.cadastre_cen t3 USING (lot_id) 
					JOIN foncier.cadastre_site t4 USING (cad_cen_id) 
					JOIN animation_fonciere.rel_saf_foncier USING (cad_site_id)
				WHERE rel_saf_foncier.session_af_id = :session_af_id)
		WHERE 
			session_af.session_af_id = :session_af_id AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'";
		$res_update_saf_geom = $bdd->prepare($rq_update_saf_geom);
		$res_update_saf_geom->execute(array('session_af_id'=>$session_af_id));	
		$res_update_saf_geom->closeCursor();	
		
		$rq_info_parcelle =
		"SELECT 
			coalesce(count(distinct t1.par_id),0) as nbr_parcelle, 
			coalesce(count(*),0) as nbr_lot, coalesce(sum(dcntlo),0) as surf_total
		FROM 
			cadastre.parcelles_cen t1 
			JOIN cadastre.lots_cen t2 USING (par_id) 
			JOIN cadastre.cadastre_cen t3 USING (lot_id) 
			JOIN foncier.cadastre_site t4 USING (cad_cen_id) 
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		WHERE 
			rel_saf_foncier.session_af_id = ? AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'";
		$res_info_parcelle = $bdd->prepare($rq_info_parcelle);
		$res_info_parcelle->execute(array($session_af_id));
		$info_parcelle = $res_info_parcelle->fetch();
		If ($res_info_parcelle->rowCount() == 0){
			include('../includes/prob_tech.php');
		}

		$rq_info_proprio =
		"SELECT 
			count(distinct t6.dnupro) as nbr_compte, 
			count(distinct t6.dnuper) as nbr_proprio
		FROM 
			cadastre.cadastre_cen t3 
			JOIN foncier.cadastre_site t4 USING (cad_cen_id) 
			JOIN cadastre.cptprop_cen t5 USING(dnupro) 
			JOIN cadastre.r_prop_cptprop_cen t6 USING (dnupro)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		WHERE 
			rel_saf_foncier.session_af_id = ? AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'";
		$res_info_proprio = $bdd->prepare($rq_info_proprio);
		$res_info_proprio->execute(array($session_af_id));
		$info_proprio = $res_info_proprio->fetch();
		If ($res_info_proprio->rowCount() == 0){
			include('../includes/prob_tech.php');
		}
		
		$rq_bilan_animation = "
		SELECT DISTINCT 
			'Lots acquis' as lib,
			count(t3.lot_id) as nbr_lot,
			-2 as ordre
		FROM 
			cadastre.cadastre_cen t3
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN foncier.r_cad_site_mf USING (cad_site_id)
			JOIN foncier.mf_acquisitions USING (mf_id)
			JOIN foncier.r_mfstatut USING (mf_id),
			(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			r_mfstatut.date = tt1.statut_date AND 
			r_mfstatut.mf_id = tt1.mf_id AND 
			r_mfstatut.statutmf_id = '1' AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'
		UNION
		SELECT DISTINCT 
			'Lots conventionn&eacute;s' as lib,
			count(t3.lot_id) as nbr_lot,
			-1 as ordre
		FROM 
			cadastre.cadastre_cen t3
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN foncier.r_cad_site_mu USING (cad_site_id)
			JOIN foncier.mu_conventions USING (mu_id)
			JOIN foncier.r_mustatut USING (mu_id),
			(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			r_mustatut.date = tt2.statut_date AND 
			r_mustatut.mu_id = tt2.mu_id AND 
			r_mustatut.statutmu_id = '1' AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'
		UNION
		SELECT 
			t_objectif.objectif_af_lib as lib,
			COALESCE(t_af.nbr_lot_objectif_acquisition, 0) AS nbr_lot,
			t_objectif.objectif_af_id as ordre
		FROM
			(SELECT DISTINCT d_objectif_af.objectif_af_id, d_objectif_af.objectif_af_lib FROM animation_fonciere.d_objectif_af ORDER BY d_objectif_af.objectif_af_id) t_objectif
			LEFT JOIN
			(SELECT 
				rel_saf_foncier.objectif_af_id,
				count(DISTINCT t3.lot_id) as nbr_lot_objectif_acquisition
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			WHERE 
				t3.lot_id NOT IN 
				(
					SELECT DISTINCT 
						t3.lot_id
					FROM 
						cadastre.cadastre_cen t3
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN foncier.r_cad_site_mf USING (cad_site_id)
						JOIN foncier.mf_acquisitions USING (mf_id)
						JOIN foncier.r_mfstatut USING (mf_id),
						(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
					WHERE 
						rel_saf_foncier.session_af_id = :session_af_id AND 
						r_mfstatut.date = tt1.statut_date AND 
						r_mfstatut.mf_id = tt1.mf_id AND 
						r_mfstatut.statutmf_id = '1' AND
						t3.date_fin = 'N.D.' AND
						t4.date_fin = 'N.D.'
					UNION
					SELECT DISTINCT 
						t3.lot_id
					FROM 
						cadastre.cadastre_cen t3
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN foncier.r_cad_site_mu USING (cad_site_id)
						JOIN foncier.mu_conventions USING (mu_id)
						JOIN foncier.r_mustatut USING (mu_id),
						(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
					WHERE 
						rel_saf_foncier.session_af_id = :session_af_id AND 
						r_mustatut.date = tt2.statut_date AND 
						r_mustatut.mu_id = tt2.mu_id AND 
						r_mustatut.statutmu_id = '1' AND
						t3.date_fin = 'N.D.' AND
						t4.date_fin = 'N.D.'
				) AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'
			GROUP BY rel_saf_foncier.objectif_af_id
			) t_af
			ON t_objectif.objectif_af_id = t_af.objectif_af_id
		ORDER BY ordre";
		// echo $rq_bilan_animation;
		$res_bilan_animation = $bdd->prepare($rq_bilan_animation);
		$res_bilan_animation->execute(array('session_af_id'=>$session_af_id));
		$bilan_animation = $res_bilan_animation->fetchAll();
		
		$lots_a_determiner = $info_parcelle['nbr_lot'] - array_sum(array_column($bilan_animation, 'nbr_lot'));
		
		$result['tbl_situation_cadastre'] = "
		<tr>
			<td colspan='2' class='bilan-af'>
				<label class='label'>
					<u>Superficie cadastrale  :</u> " . conv_are($info_parcelle['surf_total']) . " ha.a.ca
				</label>
			</td>
		</tr>
		<tr>
			<td class='bilan-af'>
				<label class='label'>
					<u>Situation cadastrale  :</u>
				</label>
			</td>
			<td class='bilan-af'>
				<li>
					<label class='label'>".$info_parcelle['nbr_parcelle']." parcelle";
					If ($info_parcelle['nbr_parcelle']>1) {
						$result['tbl_situation_cadastre'] .= "s";
					} 
		$result['tbl_situation_cadastre'] .= "
					</label>
				</li>
				<li>
					<label class='label'>".$info_parcelle['nbr_lot']." lot";
					If ($info_parcelle['nbr_lot']>1) {
						$result['tbl_situation_cadastre'] .= "s";
					}
		$result['tbl_situation_cadastre'] .= "
					</label>
				</li>
				<li>
					<label class='label'>".$info_proprio['nbr_compte']." compte";
					If ($info_proprio['nbr_compte']>1) {
						$result['tbl_situation_cadastre'] .= "s";
					}
		$result['tbl_situation_cadastre'] .= "			
					de propri&eacute;t&eacute;</label>
				</li>
				<li>
					<label class='label'>".$info_proprio['nbr_proprio']." propri&eacute;taire";
					If ($info_proprio['nbr_proprio']>1) {
						$result['tbl_situation_cadastre'] .= "s";
					} 
		$result['tbl_situation_cadastre'] .= "
					</label>
				</li>
			</td>
		</tr>";

		$result['tbl_bilan_animation'] = "	
		<tr>
			<td class='bilan-af' style='vertical-align:top;text-align:left'>
				<label class='label' style='margin-left:-40px;'><u>Bilan de l'animation  :</u></label>
			</td>
		</tr>
		<tr>
			<td style='text-align:left'>
				<table CELLSPACING = '0' CELLPADDING ='0' class='bilan-af'>";
				For ($i=0; $i<$res_bilan_animation->rowCount(); $i++) {
					$result['tbl_bilan_animation'] .= "	
					<tr>
						<td class='bilan-af' style='vertical-align:top; text-align:right'>
							<li class='label' style='text-align:right'>".$bilan_animation[$i]['lib']." : </li>
						</td>
						<td class='bilan-af' style='padding-bottom:0px'>
							<li style='margin:0' class='label'>".$bilan_animation[$i]['nbr_lot']." lot";
							If ($bilan_animation[$i]['nbr_lot']>1) {
								$result['tbl_bilan_animation'] .= "s";
							} 
					$result['tbl_bilan_animation'] .=
								 " (".round(($bilan_animation[$i]['nbr_lot']*100)/($info_parcelle['nbr_lot']), 0)."%)
							</li>
						</td>
					</tr>";
				}
				$result['tbl_bilan_animation'] .= "	
					<tr>
						<td class='bilan-af' style='vertical-align:top; text-align:right'>
							<li class='label'style='text-align:right;color:#ff0000;'>A d&eacute;terminer : </li>
						</td>
						<td class='bilan-af' style='padding-bottom:0px'>
							<li style='margin:0;color:#ff0000;' class='label'>".$lots_a_determiner." lot";
							If ($lots_a_determiner>1) {
								$result['tbl_bilan_animation'] .= "s";
							} 
						$result['tbl_bilan_animation'] .=	
								" (".round((($lots_a_determiner)*100)/($info_parcelle['nbr_lot']), 0)."%)
							</li>
						</td>
					</tr>
				</table>
			</td>
		</tr>";
		
		print json_encode($result);
	}
	
	function fct_addupdateEvenement($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure	
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$date_evenement = $data['params']['var_date_evenement'];
		$type_evenement = $data['params']['var_type_evenement'];
		$description = $data['params']['var_description'];
		$tbl_evenement_presents = $data['params']['var_tbl_evenement_presents'];
		$tbl_commentaire_interloc_id = $data['params']['var_tbl_commentaire_interloc_id'];
		$tbl_commentaire_interloc_txt = $data['params']['var_tbl_commentaire_interloc_txt'];
		$evenement_af_id = $data['params']['var_evenement_af_id'];
		
		If ($evenement_af_id != -1) {
			$rq_update_evenement =
			"UPDATE animation_fonciere.evenements_af SET 
				evenement_af_date = '".date_transform_inverse($date_evenement)."',
				type_evenements_af_id = $type_evenement,
				evenement_af_description = '$description'
			WHERE 
				evenement_af_id = $evenement_af_id";
			$res_update_evenement = $bdd->prepare($rq_update_evenement);
			// echo $rq_update_evenement;
			$res_update_evenement->execute();
			$res_update_evenement->closeCursor();
			
			$rq_delete_evenement = "
			DELETE FROM animation_fonciere.rel_af_evenements
			WHERE evenement_af_id = $evenement_af_id";
			$res_delete_evenement = $bdd->prepare($rq_delete_evenement);
			// echo $rq_delete_evenement;
			$res_delete_evenement->execute();
			$res_delete_evenement->closeCursor();
		} Else {
			$rq_new_evenement_id = "SELECT nextval('animation_fonciere.evenement_af_id_seq'::regclass)";
			$res_new_evenement_id = $bdd->prepare($rq_new_evenement_id);
			$res_new_evenement_id->execute();
			$new_evenement_id_temp = $res_new_evenement_id->fetch();
			$evenement_af_id = $new_evenement_id_temp[0];
			$res_new_evenement_id->closeCursor();
					
			$rq_insert_evenement =
			"INSERT INTO animation_fonciere.evenements_af
				(evenement_af_id,
				evenement_af_date,
				type_evenements_af_id,
				evenement_af_description)
			VALUES 
				($evenement_af_id,
				'".date_transform_inverse($date_evenement)."',
				$type_evenement,
				'".str_replace("'", "''", $description)."')";
			$res_insert_evenement = $bdd->prepare($rq_insert_evenement);
			// echo $rq_insert_evenement;
			$res_insert_evenement->execute();
			$res_insert_evenement->closeCursor();
		}
		
		Foreach ($tbl_evenement_presents as $id) {
			$rq_insert_evenement = "INSERT INTO animation_fonciere.rel_af_evenements (session_af_id, evenement_af_id, interloc_id) VALUES ($session_af_id, $evenement_af_id, $id)";
			$res_insert_evenement = $bdd->prepare($rq_insert_evenement);
			// echo $rq_insert_evenement;
			$res_insert_evenement->execute();
			$res_insert_evenement->closeCursor();
		}
		
		$i=0;
		Foreach ($tbl_commentaire_interloc_id as $id) {
			$rq_update_interloc_com = "
			UPDATE animation_fonciere.interlocuteurs SET commentaires = '".str_replace("'", "''", $tbl_commentaire_interloc_txt[$i])."' WHERE interloc_id = $id";
			$res_update_interloc_com = $bdd->prepare($rq_update_interloc_com);
			// echo $rq_update_interloc_com;
			$res_update_interloc_com->execute();
			$res_update_interloc_com->closeCursor();
			$i++;
		}
		
		$rq_evt_globaux = "
		SELECT DISTINCT
			evenements_af.evenement_af_id, 
			evenements_af.evenement_af_date, 
			d_types_evenements_af.type_evenements_af_lib
		FROM 
			animation_fonciere.evenements_af
			JOIN animation_fonciere.d_types_evenements_af USING (type_evenements_af_id)
			JOIN animation_fonciere.rel_af_evenements USING (evenement_af_id)
		WHERE 
			rel_af_evenements.session_af_id = ?
		ORDER BY evenement_af_date DESC";
		$res_evt_globaux = $bdd->prepare($rq_evt_globaux);
		$res_evt_globaux->execute(array($session_af_id));
		$lst_evt_globaux = $res_evt_globaux->fetchall();	
		$result['td_lst_evenements'] = '';
		For ($i=0; $i<$res_evt_globaux->rowCount(); $i++) { 	
			$result['td_lst_evenements'] .= "
			<li>
				<label class='label_simple' style='margin-left:10px;'>le ".date_transform($lst_evt_globaux[$i]['evenement_af_date'])." : ". $lst_evt_globaux[$i]['type_evenements_af_lib']."</label>
				<img onclick=\"showFormEvenement(this, ".$session_af_id.", ".$lst_evt_globaux[$i]['evenement_af_id'].");\" src='".ROOT_PATH."images/edit.png' style='cursor:pointer;padding-left:5px;'>
			</li>";
		}
		
		print json_encode($result);
	}
	
	function fct_addupdateEchange($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure	
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$interloc_lib = $data['params']['var_interloc_lib'];
		$referent = $data['params']['var_referent'];
		$interloc_id = $data['params']['var_interloc_id'];
		$gtoper = $data['params']['var_gtoper'];
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		$date_echange = $data['params']['var_date_echange'];
		$type_echange = $data['params']['var_type_echange'];
		$description = $data['params']['var_description'];
		$echange_referent = $data['params']['var_echange_referent'];
		$date_echange_recontact = $data['params']['var_date_echange_recontact'];
		$motif_echange_recontact = $data['params']['var_motif_echange_recontact'];		
		$rappel_traite = $data['params']['var_rappel_traite'];		
		$result_echange_af_id = $data['params']['var_result_echange_af_id'];
		$tbl_modif_infos_interloc = $data['params']['var_tbl_modif_infos_interloc'];
		$maitrise_parc = $data['params']['var_maitrise_parc'];
		$tbl_maitrise_parc = $data['params']['var_tbl_maitrise_parc'];
		$tbl_prix_nego_parc = $data['params']['var_tbl_prix_nego_parc'];
		$tbl_parc_echange = $data['params']['var_tbl_parc_echange'];
		$echange_af_id = $data['params']['var_echange_af_id'];
		$representant = $data['params']['var_representant'];
		$prix_eaf_negocie = $data['params']['var_prix_eaf_negocie'];
		
		If ($result_echange_af_id == '3') {
			$min_crit = 0; //déclaration de la variable $min_crit à 0 correspondant à la position du premier caractère de la première variable
			$max_crit = strpos($tbl_modif_infos_interloc, ':'); //déclaration de la variable $max_crit correspondant au nombre de caractère de la première variable en l'occurrence il s'agit de la position du premier ':'
			$min_val = strpos($tbl_modif_infos_interloc, ':') + 1; //déclaration de la variable $min_val correspondant à la position du premier caractère de la valeur de la première variable, en l'occurence il s'agit de la position du premier ':' + 1 (on aurait pu ecrire $min_val = $max_crit + 1
			$max_val = strpos($tbl_modif_infos_interloc, ';') - $min_val; //déclaration de la variable $max_val correspondant au nombre de caractère de la valeur de la première variable			
			For ($p=0; $p<substr_count($tbl_modif_infos_interloc, ':'); $p++) { //on boucle sur la variable GET 'd' pour créer les différentes variables PHP
				$crit = substr($tbl_modif_infos_interloc, $min_crit, $max_crit); //déclaration du nom de la variable PHP
				$value = substr($tbl_modif_infos_interloc, $min_val, $max_val); //déclaration de la valeur de la variable PHP
				$$crit = $value; //création de la variable PHP
				$min_crit = strpos($tbl_modif_infos_interloc, ';', $min_crit) + 1; //recalcul de la variable $min_crit pour la prochaine variable
				$max_crit= strpos($tbl_modif_infos_interloc, ':', $min_crit) - $min_crit; //recalcul de la variable $max_crit pour la prochaine variable
				$min_val = $min_crit + $max_crit + 1; //recalcul de la variable $min_val pour la valeur de la prochaine variable
				$max_val = strpos($tbl_modif_infos_interloc, ';', $min_val) - $min_val; //recalcul de la variable $max_val pour la valeur de la prochaine variable
			}
			
			//Ajout de l'interlocuteur à partir du formaulaire prévu à cet effet
			If ($gtoper == '1') {
				$rq_update_interloc =
				"UPDATE animation_fonciere.interlocuteurs SET 
					ccoqua = $particule_id,
					ddenom = '".strtoupper($nom.'/'.$prenom)."',
					nom_usage = '".strtoupper($nom)."',
					nom_jeunefille = CASE WHEN '".strtoupper($nom_jeune_fille)."' != '' THEN '".strtoupper($nom_jeune_fille)."' ELSE CASE WHEN $particule_id = 1 THEN 'N.P.' ELSE 'N.P.' END END,
					prenom_usage = '".strtoupper($prenom)."',
					nom_conjoint =  CASE WHEN '".strtoupper($nom_conjoint)."' != '' THEN '".strtoupper($nom_conjoint)."' ELSE 'N.D.' END,
					prenom_conjoint = CASE WHEN '".strtoupper($prenom_conjoint)."' != '' THEN '".strtoupper($prenom_conjoint)."' ELSE 'N.D.' END,
					jdatnss = CASE WHEN LENGTH('".$date_naissance."') = 10 THEN '".substr($date_naissance, 6, 4) . substr($date_naissance, 3, 2) . substr($date_naissance, 0,2)."' ELSE 'N.D.' END,
					dldnss =  CASE WHEN '".strtoupper($lieu_naissance)."' != '' THEN '".strtoupper($lieu_naissance)."' ELSE 'N.D.' END,";
			} Else {
				$rq_update_interloc =
				"UPDATE animation_fonciere.interlocuteurs SET 
					ccoqua = NULL,
					ddenom = '".strtoupper($ddenom)."',
					nom_usage = 'N.P.',
					nom_jeunefille = 'N.P.',
					prenom_usage = 'N.P.',
					nom_conjoint =  'N.P.',
					prenom_conjoint = 'N.P.',
					jdatnss = 'N.P.',
					dldnss = 'N.P.',";
			}
			$rq_update_interloc .= "
				dlign3 = CASE WHEN '$adresse1' != '' THEN '$adresse1' ELSE 'N.D.' END,
				dlign4 = CASE WHEN '$adresse2' != '' THEN '$adresse2' ELSE 'N.D.' END,
				dlign5 = CASE WHEN '$adresse3' != '' THEN '$adresse3' ELSE 'N.D.' END,
				dlign6 = CASE WHEN '$adresse4' != '' THEN '$adresse4' ELSE 'N.D.' END,
				email = CASE WHEN '$mail' != '' THEN '$mail' ELSE 'N.D.' END,
				fixe_domicile = CASE WHEN '$tel_fixe1' != '' THEN '$tel_fixe1' ELSE 'N.D.' END,
				fixe_travail = CASE WHEN '$tel_fixe2' != '' THEN '$tel_fixe2' ELSE 'N.D.' END,
				portable_domicile = CASE WHEN '$tel_portable1' != '' THEN '$tel_portable1' ELSE 'N.D.' END,
				portable_travail = CASE WHEN '$tel_portable2' != '' THEN '$tel_portable2' ELSE 'N.D.' END,
				fax = CASE WHEN '$fax' != '' THEN '$fax' ELSE 'N.D.' END,
				gtoper = $gtoper
			WHERE interloc_id = $interloc_id";			
			$res_update_interloc = $bdd->prepare($rq_update_interloc);
			// echo $rq_update_interloc;
			$res_update_interloc->execute();
			$res_update_interloc->closeCursor();
		}		
		
		$rq_update_prix_eaf_negocie = "
		UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_negocie = $prix_eaf_negocie WHERE rel_eaf_foncier_id IN 
			(
				SELECT 
					rel_eaf_foncier_id
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				WHERE
					entite_af_id = :entite_af_id
			)";
		$res_update_prix_eaf_negocie = $bdd->prepare($rq_update_prix_eaf_negocie);
		// echo $rq_update_prix_eaf_negocie;
		$res_update_prix_eaf_negocie->execute(array('entite_af_id'=>$entite_af_id));
		$res_update_prix_eaf_negocie->closeCursor();
		
		
		If ($maitrise_parc == 'oui' && count($tbl_maitrise_parc) > 0) {
			$min_crit = 0; //déclaration de la variable $min_crit à 0 correspondant à la position du premier caractère de la première variable
			$max_crit = strpos($tbl_prix_nego_parc, ':'); //déclaration de la variable $max_crit correspondant au nombre de caractère de la première variable en l'occurrence il s'agit de la position du premier ':'
			$min_val = strpos($tbl_prix_nego_parc, ':') + 1; //déclaration de la variable $min_val correspondant à la position du premier caractère de la valeur de la première variable, en l'occurence il s'agit de la position du premier ':' + 1 (on aurait pu ecrire $min_val = $max_crit + 1
			$max_val = strpos($tbl_prix_nego_parc, ';') - $min_val; //déclaration de la variable $max_val correspondant au nombre de caractère de la valeur de la première variable			
			For ($p=0; $p<substr_count($tbl_prix_nego_parc, ':'); $p++) { //on boucle sur la variable GET 'd' pour créer les différentes variables PHP
				$crit = substr($tbl_prix_nego_parc, $min_crit, $max_crit); //déclaration du nom de la variable PHP
				$value = substr($tbl_prix_nego_parc, $min_val, $max_val); //déclaration de la valeur de la variable PHP
				$$crit = $value; //création de la variable PHP
				$min_crit = strpos($tbl_prix_nego_parc, ';', $min_crit) + 1; //recalcul de la variable $min_crit pour la prochaine variable
				$max_crit= strpos($tbl_prix_nego_parc, ':', $min_crit) - $min_crit; //recalcul de la variable $max_crit pour la prochaine variable
				$min_val = $min_crit + $max_crit + 1; //recalcul de la variable $min_val pour la valeur de la prochaine variable
				$max_val = strpos($tbl_prix_nego_parc, ';', $min_val) - $min_val; //recalcul de la variable $max_val pour la valeur de la prochaine variable
			}
			
			$tbl_rel_saf_foncier_interlocs_id_check = array();			
			Foreach ($tbl_maitrise_parc as $id) {
				$tbl_temp = explode('_', $id);
				$tbl_temp2 = explode('-', $tbl_temp[1]);
				Foreach ($tbl_temp2 as $id2) {					
					$tbl_parc_maitrise['parc'.$id2] = $tbl_temp[0];
					If(isset($$id) && $$id != 'N.P.') {
						$tbl_parc_prix_nego['parc'.$id2] = $$id;
					}
					array_push($tbl_rel_saf_foncier_interlocs_id_check, $id2);
				}
			}			
		}
		
		If ($echange_af_id != 'N.D.') {
			$rq_update_echange =
			"UPDATE animation_fonciere.echanges_af SET 
				echange_af_date = '".date_transform_inverse($date_echange)."',
				type_echanges_af_id = $type_echange,
				echange_af_description = '$description',
				result_echange_af_id = $result_echange_af_id,
				echange_referent = '$echange_referent'
			WHERE 
				echange_af_id = $echange_af_id";
			$res_update_echange = $bdd->prepare($rq_update_echange);
			// echo $rq_update_echange;
			$res_update_echange->execute();
			$res_update_echange->closeCursor();
			
			$rq_lst_eaf_update = "
			SELECT 
				interloc_id || '_' || entite_af_id as id_to_update
			FROM
				animation_fonciere.rel_af_echanges
				JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				echange_af_id = :echange_af_id
			GROUP BY
				entite_af_id";
			// echo $rq_lst_eaf_update;
			$res_lst_eaf_update = $bdd->prepare($rq_lst_eaf_update);
			$res_lst_eaf_update->execute(array('echange_af_id'=>$echange_af_id));
			$lst_eaf_update = $res_lst_eaf_update->fetchall();
			
			$rq_delete_rel_af_echanges = "
			DELETE
			FROM 
				animation_fonciere.rel_af_echanges
			WHERE 
				echange_af_id = $echange_af_id ";
			If (isset($tbl_parc_echange) && $tbl_parc_echange != 'NULL') {
				$rq_delete_rel_af_echanges .= "AND (rel_saf_foncier_interlocs_id IN (".str_replace('-', ',', implode(',', $tbl_parc_echange)).") OR rel_saf_foncier_interlocs_id IN (SELECT rel_saf_foncier_interlocs_id_is FROM animation_fonciere.interloc_substitution WHERE rel_saf_foncier_interlocs_id_vp IN (". str_replace('-', ',', implode(',', $tbl_parc_echange)).")))";
			}
			$res_delete_rel_af_echanges = $bdd->prepare($rq_delete_rel_af_echanges);
			// echo $rq_delete_rel_af_echanges;
			$res_delete_rel_af_echanges->execute();
			$res_delete_rel_af_echanges->closeCursor();
		} Else {
			$lst_eaf_update = array();
			
			$rq_new_echange_id = "SELECT nextval('animation_fonciere.echange_af_id_seq'::regclass)";
			$res_new_echange_id = $bdd->prepare($rq_new_echange_id);
			$res_new_echange_id->execute();
			$new_echange_id_temp = $res_new_echange_id->fetch();
			$echange_af_id = $new_echange_id_temp[0];
			$res_new_echange_id->closeCursor();
					
			$rq_insert_echange =
			"INSERT INTO animation_fonciere.echanges_af
				(echange_af_id,
				echange_af_date,
				type_echanges_af_id,
				echange_af_description,
				result_echange_af_id,
				echange_referent)
			VALUES 
				($echange_af_id,
				'".date_transform_inverse($date_echange)."',
				$type_echange,
				'".str_replace("'", "''", $description)."',
				$result_echange_af_id,
				'$echange_referent')";
			$res_insert_echange = $bdd->prepare($rq_insert_echange);
			// echo $rq_insert_echange;
			$res_insert_echange->execute();
			$res_insert_echange->closeCursor();
		}		
		
		$rq_rel_saf_foncier_interlocs_id = "
		SELECT DISTINCT
			COALESCE(rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id, t_temp_subst.rel_saf_foncier_interlocs_id) as rel_saf_foncier_interlocs_id,
			CASE WHEN rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id IN 
				(
					SELECT 
						rel_saf_foncier_interlocs_id_vp 
					FROM 
						animation_fonciere.interloc_substitution
						JOIN animation_fonciere.rel_saf_foncier_interlocs ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					WHERE
						groupe_interloc_id != 3
				) THEN t_temp_subst.interloc_id ELSE rel_saf_foncier_interlocs.interloc_id END as interloc_id,
			CASE WHEN rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id IN 
				(
					SELECT 
						rel_saf_foncier_interlocs_id_vp 
					FROM 
						animation_fonciere.interloc_substitution
						JOIN animation_fonciere.rel_saf_foncier_interlocs ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					WHERE
						groupe_interloc_id != 3
				) THEN t_temp_subst.interloc_id || '_' || entite_af_id ELSE rel_saf_foncier_interlocs.interloc_id || '_' || entite_af_id END as id_to_update,
			CASE WHEN rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id IN 
				(
					SELECT 
						rel_saf_foncier_interlocs_id_vp 
					FROM 
						animation_fonciere.interloc_substitution
						JOIN animation_fonciere.rel_saf_foncier_interlocs ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
		= interloc_substitution.rel_saf_foncier_interlocs_id_is)
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					WHERE
						groupe_interloc_id != 3
				) THEN t_temp_subst.type_interloc_id ELSE rel_saf_foncier_interlocs.type_interloc_id END AS type_interloc_id,
				can_be_subst::text,
				insert
		FROM
			animation_fonciere.rel_saf_foncier_interlocs
			JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
			JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			JOIN
				(";
					If (isset($tbl_rel_saf_foncier_interlocs_id_check)) {
						$rq_rel_saf_foncier_interlocs_id .= "
						SELECT DISTINCT
							interloc_id,
							type_interloc_id,
							'true' as insert
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						WHERE
							rel_saf_foncier_interlocs_id IN (".implode(',', $tbl_rel_saf_foncier_interlocs_id_check).") AND
							(can_nego = 't' OR avis_favorable = 't')
						UNION
						";
					} 
					If ($representant == 'false') {
						$rq_rel_saf_foncier_interlocs_id .= "
						SELECT DISTINCT
							interloc_id,
							type_interloc_id,
							'true' as insert
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND
							entite_af_id = :entite_af_id AND
							interloc_id = :interloc_id AND
							can_be_subst = 'f' AND
							(can_nego = 't' OR avis_favorable = 't')
							
						UNION";
					}
					$rq_rel_saf_foncier_interlocs_id .= "
					SELECT 
						rel_saf_foncier_interlocs.interloc_id,
						rel_saf_foncier_interlocs.type_interloc_id,
						'true' as insert
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
					WHERE
						session_af_id = :session_af_id AND
						entite_af_id = :entite_af_id AND
						t_temp_subst.interloc_id = :interloc_id AND
						can_be_subst = 'f' AND
						(can_nego = 't' OR avis_favorable = 't')	
					";
					
					$rq_rel_saf_foncier_interlocs_id .= "
					UNION

					SELECT DISTINCT
						t_temp_ref.interloc_id,
						t_temp_ref.type_interloc_id,
						CASE WHEN '".$echange_referent."' = 't' THEN 'true' ELSE 'false' END as insert
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (interloc_referent.rel_saf_foncier_interlocs_id = t_temp_ref.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.d_type_interloc ON (d_type_interloc.type_interloc_id = t_temp_ref.type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					WHERE
						rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
						session_af_id = :session_af_id AND
						entite_af_id = :entite_af_id AND
						can_be_subst = 'f' AND
						(can_nego = 't' OR avis_favorable = 't') AND 
						t_temp_ref.rel_saf_foncier_interlocs_id NOT IN 
							(
								SELECT DISTINCT
									rel_saf_foncier_interlocs_id_vp
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
								WHERE
									session_af_id = :session_af_id AND
									entite_af_id = :entite_af_id
							)

					UNION

					SELECT DISTINCT
						t_temp_subst.interloc_id,
						t_temp_subst.type_interloc_id,
						CASE WHEN '".$echange_referent."' = 't' THEN 'true' ELSE 'false' END as insert
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (interloc_referent.rel_saf_foncier_interlocs_id = t_temp_ref.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.d_type_interloc ON (d_type_interloc.type_interloc_id = t_temp_ref.type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp = t_temp_ref.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
					WHERE
						rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
						session_af_id = :session_af_id AND
						entite_af_id = :entite_af_id AND
						can_be_subst = 'f' AND
						(can_nego = 't' OR avis_favorable = 't')
				) t1 ON (t1.interloc_id = rel_saf_foncier_interlocs.interloc_id AND t1.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)";
			If ($representant == 'false') {
				$rq_rel_saf_foncier_interlocs_id .= "
				LEFT JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
				LEFT JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)";
			} Else {
				$rq_rel_saf_foncier_interlocs_id .= "
				JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
				JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)"; 
			}
			
		$rq_rel_saf_foncier_interlocs_id .= "
		WHERE
			session_af_id = :session_af_id AND
			entite_af_id = :entite_af_id AND
			groupe_interloc_id != 5
		ORDER BY 
			id_to_update,
			type_interloc_id";
			
		// echo $rq_rel_saf_foncier_interlocs_id;
		$res_rel_saf_foncier_interlocs_id = $bdd->prepare($rq_rel_saf_foncier_interlocs_id);
		$res_rel_saf_foncier_interlocs_id->execute(array('interloc_id'=>$interloc_id, 'session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$lst_rel_saf_foncier_interlocs_id = $res_rel_saf_foncier_interlocs_id->fetchall();
		
		For($i=0; $i<$res_rel_saf_foncier_interlocs_id->rowCount(); $i++) {
			If (isset($tbl_parc_maitrise['parc'.$lst_rel_saf_foncier_interlocs_id[$i]['rel_saf_foncier_interlocs_id']])) {
				$nego_af_id = $tbl_parc_maitrise['parc'.$lst_rel_saf_foncier_interlocs_id[$i]['rel_saf_foncier_interlocs_id']];
			} Else {
				$nego_af_id = 'Null';
			}
			If (isset($tbl_parc_prix_nego['parc'.$lst_rel_saf_foncier_interlocs_id[$i]['rel_saf_foncier_interlocs_id']]) && $tbl_parc_prix_nego['parc'.$lst_rel_saf_foncier_interlocs_id[$i]['rel_saf_foncier_interlocs_id']] != "") {
				$nego_af_prix = str_replace(',', '.', $tbl_parc_prix_nego['parc'.$lst_rel_saf_foncier_interlocs_id[$i]['rel_saf_foncier_interlocs_id']]);
			} Else {
				$nego_af_prix = 'Null';
			}
			If ($lst_rel_saf_foncier_interlocs_id[$i]['insert'] == 'true') {	
				$rq_insert_rel_af_echanges = 
				"INSERT INTO animation_fonciere.rel_af_echanges
					(echange_af_id,
					rel_saf_foncier_interlocs_id,
					rel_saf_foncier_representant_id,
					nego_af_id,
					nego_af_prix,
					date_recontact,
					commentaires,
					rappel_traite,
					echange_direct)
				VALUES
					($echange_af_id,
					".$lst_rel_saf_foncier_interlocs_id[$i]['rel_saf_foncier_interlocs_id'].",";
					If ($representant == 'false') {
						$rq_insert_rel_af_echanges .= "NULL,";
					} Else {
						$rq_insert_rel_af_echanges .= "(SELECT rel_saf_foncier_interlocs_id_is FROM animation_fonciere.interloc_substitution WHERE rel_saf_foncier_interlocs_id_vp = ".$lst_rel_saf_foncier_interlocs_id[$i]['rel_saf_foncier_interlocs_id']."),";
					}
					$rq_insert_rel_af_echanges .= "
					$nego_af_id,
					$nego_af_prix,";
					If ($lst_rel_saf_foncier_interlocs_id[$i]['interloc_id'] == $interloc_id || $representant == 'true') {
						$rq_insert_rel_af_echanges .= "
						'".date_transform_inverse($date_echange_recontact)."',
						'".str_replace("'", "''", $motif_echange_recontact)."',
						'$rappel_traite',";
					} Else {
						$rq_insert_rel_af_echanges .= "
						'N.D.',
						'N.D.',
						'f',";
					}
					If ($lst_rel_saf_foncier_interlocs_id[$i]['id_to_update'] == $interloc_id.'_'.$entite_af_id) {
						$rq_insert_rel_af_echanges .= "
						't')";
					} Else {
						$rq_insert_rel_af_echanges .= "
						'f')";
					}				
					
				$res_insert_rel_af_echanges = $bdd->prepare($rq_insert_rel_af_echanges);
				// echo $rq_insert_rel_af_echanges;
				$res_insert_rel_af_echanges->execute();
				$res_insert_rel_af_echanges->closeCursor();	
			}
		}
		
		$result['code'] = '';
		$tbl_eaf_interloc_update = array_values(array_unique(array_merge(array_column($lst_rel_saf_foncier_interlocs_id, 'id_to_update'), array_column($lst_eaf_update, 'id_to_update'))));
		
		For ($i=0; $i<count($tbl_eaf_interloc_update); $i++) {
			$tbl_temp = explode('_', $tbl_eaf_interloc_update[$i]);
			
			$rq_echanges = "
			SELECT DISTINCT
				'echange' as categorie,
				st2.interloc_id,
				representant,
				show_nego,
				interlocuteurs.dnuper,
				d_ccoqua.ccoqua as particule_id,
				d_ccoqua.particule_lib,
				interlocuteurs.ddenom,
				interlocuteurs.nom_usage,
				interlocuteurs.prenom_usage,
				echange_af_id,
				echange_af_date,
				echange_af_description,
				echange_referent::text,
				CASE WHEN nbr_representant > 0 THEN 'true' ELSE 'false' END echange_representant,
				type_echanges_af_lib,
				date_recontact,
				st2.commentaires,
				rappel_traite,
				echange_direct,
				result_echange_af_id,
				result_echange_af_lib,
				array_to_string(array_agg(rel_saf_foncier_representant_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_representant_id,	
				array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,	
				array_to_string(array_agg(t1.ccosec || t1.dnupla ORDER BY lot_id), ',') as tbl_par_lib,
				array_to_string(array_agg(COALESCE(t2.dnulot, 'N.D.') ORDER BY lot_id), ',') as tbl_dnulot,
				array_to_string(array_agg(COALESCE(d_nego_af.nego_af_lib, 'N.D.') ORDER BY lot_id), ',') as tbl_resultat,
				array_to_string(array_agg(COALESCE(nego_af_prix, -1) ORDER BY lot_id), ',') as tbl_resultat_prix
			FROM
				animation_fonciere.echanges_af
				LEFT JOIN animation_fonciere.d_result_echanges_af USING (result_echange_af_id)
				JOIN animation_fonciere.d_types_echanges_af USING (type_echanges_af_id)
				JOIN
					(
						SELECT DISTINCT
							count(rel_saf_foncier_representant_id) as nbr_representant,
							CASE WHEN groupe_interloc_id = 3 THEN 'true' ELSE 'false' END representant,
							CASE WHEN can_nego = 't' OR avis_favorable = 't' THEN 'true' ELSE 'false' END show_nego,
							rel_af_echanges.date_recontact,
							rel_af_echanges.commentaires,
							rel_af_echanges.rappel_traite,
							rel_af_echanges.echange_direct,
							rel_af_echanges.nego_af_prix,
							echange_af_id,
							nego_af_id,
							rel_saf_foncier_id,
							rel_saf_foncier_interlocs.interloc_id,
							--rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
							rel_saf_foncier_representant_id
						FROM
							animation_fonciere.rel_af_echanges
							JOIN 
								(
									SELECT 
										rel_saf_foncier_interlocs_id,
										interloc_id,
										rel_saf_foncier_id,
										type_interloc_id,
										groupe_interloc_id,
										can_nego,
										avis_favorable
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									WHERE
										interloc_id = :interloc_id AND 
										session_af_id = :session_af_id AND
										entite_af_id = :entite_af_id

									UNION

									SELECT 
										rel_saf_foncier_interlocs_id_vp,
										interloc_id,
										rel_saf_foncier_id,
										type_interloc_id,
										groupe_interloc_id,
										can_nego,
										avis_favorable
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
									WHERE
										interloc_id = :interloc_id AND 
										session_af_id = :session_af_id AND
										entite_af_id = :entite_af_id
								) rel_saf_foncier_interlocs  USING (rel_saf_foncier_interlocs_id)
							JOIN
								(";
							If (isset($tbl_rel_saf_foncier_interlocs_id_check)) {
								$rq_echanges .= "
								SELECT DISTINCT
									interloc_id,
									type_interloc_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
								WHERE
									rel_saf_foncier_interlocs_id IN (".implode(',', $tbl_rel_saf_foncier_interlocs_id_check).")
									
								UNION	
								";
							}
							
							$rq_echanges .= "
							SELECT DISTINCT
								interloc_id,
								type_interloc_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = :session_af_id AND
								entite_af_id = :entite_af_id AND
								interloc_id = :interloc_id AND
								(can_nego = 't' OR avis_favorable = 't')
								
							UNION

							SELECT 
								rel_saf_foncier_interlocs.interloc_id,
								rel_saf_foncier_interlocs.type_interloc_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
								JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
							WHERE
								session_af_id = :session_af_id AND
								entite_af_id = :entite_af_id AND
								t_temp_subst.interloc_id = :interloc_id AND
								(can_nego = 't' OR avis_favorable = 't')";
							
							If ($echange_referent == 't') {
								$rq_echanges .= "
								UNION

								SELECT DISTINCT
									t_temp_ref.interloc_id,
									t_temp_ref.type_interloc_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (interloc_referent.rel_saf_foncier_interlocs_id = t_temp_ref.rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.d_type_interloc ON (d_type_interloc.type_interloc_id = t_temp_ref.type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
								WHERE
									rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
									session_af_id = :session_af_id AND
									entite_af_id = :entite_af_id AND
									(can_nego = 't' OR avis_favorable = 't') AND 
									t_temp_ref.rel_saf_foncier_interlocs_id NOT IN 
										(
											SELECT DISTINCT
												rel_saf_foncier_interlocs_id_vp
											FROM
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
											WHERE
												session_af_id = :session_af_id AND
												entite_af_id = :entite_af_id
										)

								UNION

								SELECT DISTINCT
									t_temp_subst.interloc_id,
									t_temp_subst.type_interloc_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (interloc_referent.rel_saf_foncier_interlocs_id = t_temp_ref.rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.d_type_interloc ON (d_type_interloc.type_interloc_id = t_temp_ref.type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
									JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp = t_temp_ref.rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
								WHERE
									rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
									session_af_id = :session_af_id AND
									entite_af_id = :entite_af_id AND
									--can_be_subst = 'f' AND
									(can_nego = 't' OR avis_favorable = 't')";
							}
							$rq_echanges .= "
							) st1 ON (st1.interloc_id = rel_saf_foncier_interlocs.interloc_id AND st1.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
						GROUP BY
							representant,
							show_nego,
							rel_af_echanges.date_recontact,
							rel_af_echanges.commentaires,
							rel_af_echanges.rappel_traite,
							rel_af_echanges.echange_direct,
							rel_af_echanges.nego_af_prix,
							echange_af_id,
							nego_af_id,
							rel_saf_foncier_id,
							rel_saf_foncier_interlocs.interloc_id,
							rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
							rel_saf_foncier_representant_id
					) st2 USING (echange_af_id)
				LEFT JOIN animation_fonciere.d_nego_af USING (nego_af_id)
				JOIN animation_fonciere.interlocuteurs USING (interloc_id)
				LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.cadastre_site t4 USING (cad_site_id) 
				JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
				JOIN cadastre.lots_cen t2 USING (lot_id)
				JOIN cadastre.parcelles_cen t1 USING (par_id)
			WHERE
				interloc_id = :interloc_id AND 	
				session_af_id = :session_af_id AND
				entite_af_id = :entite_af_id
			GROUP BY
				st2.interloc_id,
				representant,
				echange_representant,
				show_nego,
				interlocuteurs.dnuper,
				particule_id,
				interlocuteurs.ddenom,
				interlocuteurs.nom_usage,
				interlocuteurs.prenom_usage,
				echange_af_id,
				echange_af_date,
				echange_af_description,
				echange_referent,
				type_echanges_af_lib,
				date_recontact,
				st2.commentaires,
				rappel_traite,
				echange_direct,
				result_echange_af_id,
				result_echange_af_lib
			
			UNION

			SELECT DISTINCT
				'evenement' as categorie,
				interloc_id,
				'false' as representant,
				'false' as show_nego,
				'N.D.' as dnuper,
				0 as particule_id,
				'N.D.' as particule_lib,
				'N.D.' as ddenom,
				'N.D.' as nom_usage,
				'N.D.' as prenom_usage,
				evenement_af_id as echange_af_id,
				evenement_af_date as echange_af_date,
				evenement_af_description as echange_af_description,
				'false' as echange_referent,
				'false' as echange_representant,
				type_evenements_af_lib as type_echanges_af_lib,
				'N.D.' as date_recontact,
				'N.D.' as commentaires,
				FALSE as rappel_traite,
				FALSE as echange_direct,
				0 as result_echange_af_id,
				NULL as result_echange_af_lib,
				NULL as tbl_rel_saf_foncier_representant_id,	
				NULL as tbl_rel_saf_foncier_id,	
				NULL as tbl_par_lib,
				NULL as tbl_dnulot,
				'N.D.' as tbl_resultat,
				NULL as tbl_resultat_prix
			FROM
				animation_fonciere.evenements_af
				JOIN animation_fonciere.d_types_evenements_af USING (type_evenements_af_id)
				JOIN animation_fonciere.rel_af_evenements USING (evenement_af_id)
			WHERE 
				interloc_id = :interloc_id AND
				session_af_id = :session_af_id				
				
			ORDER BY 
				echange_af_date DESC,
				echange_af_id";
			// echo $rq_echanges;
			$res_echanges = $bdd->prepare($rq_echanges);
			$res_echanges->execute(array('interloc_id'=>$tbl_temp[0], 'session_af_id'=>$session_af_id, 'entite_af_id'=>$tbl_temp[1]));
			$lst_echanges = $res_echanges->fetchall();			
			
			$tbl_index_same_lines = array_keys(array_values(array_column($lst_rel_saf_foncier_interlocs_id, 'id_to_update')), $tbl_temp[0].'_'.$tbl_temp[1]);
			$id_table_lst_type_interloc = array();
			For ($index_type = 0; $index_type < count($tbl_index_same_lines); $index_type++) {
				array_push($id_table_lst_type_interloc, $lst_rel_saf_foncier_interlocs_id[$tbl_index_same_lines[$index_type]]['type_interloc_id']);		
			}
			sort($id_table_lst_type_interloc);
			$result['entite_af_id'][$i] = "tbl_echange_".$tbl_temp[0]."_".$tbl_temp[1]."_".implode('-', array_unique($id_table_lst_type_interloc));
						
			$result['entite_af_code'][$i] = "";
			For ($e=0; $e<$res_echanges->rowCount(); $e++) {
				$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_echanges[$e]['particule_lib'], $lst_echanges[$e]['nom_usage'], $lst_echanges[$e]['prenom_usage'], $lst_echanges[$e]['ddenom']));
				
				$rq_echange_referent = "
				SELECT DISTINCT
					interloc_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
					
				WHERE
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id IN (	
						SELECT DISTINCT
							rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
						WHERE
							echange_af_id = :echange_af_id
						)";
				$res_echange_referent = $bdd->prepare($rq_echange_referent);
				$res_echange_referent->execute(array('echange_af_id'=>$lst_echanges[$e]['echange_af_id']));
				$echange_referent = $res_echange_referent->fetch();
				
				If ($e == 0 || (isset($lst_echanges[$e-1]['echange_af_id']) && $lst_echanges[$e-1]['echange_af_id'] != $lst_echanges[$e]['echange_af_id'])) {
					If ($lst_echanges[$e]['echange_representant'] == 'true') {
						$rq_infos_representant = "
						SELECT DISTINCT
							interloc_id,
							particule_lib,
							nom_usage,
							prenom_usage,
							ddenom
						FROM
							animation_fonciere.interlocuteurs
							LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
							JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
						WHERE
							rel_saf_foncier_interlocs_id IN (".$lst_echanges[$e]['tbl_rel_saf_foncier_representant_id'].")";
						// echo $rq_infos_representant;
						$res_infos_representant = $bdd->prepare($rq_infos_representant);
						$res_infos_representant->execute();
						$infos_representant = $res_infos_representant->fetch();	
						$interloc_lib = str_replace('   ', ' ', proprio_lib($infos_representant['particule_lib'], $infos_representant['nom_usage'], $infos_representant['prenom_usage'], $infos_representant['ddenom']));
						$echange_interloc_id = $infos_representant['interloc_id'];
					} Else {
						$echange_interloc_id = $lst_echanges[$e]['interloc_id'];
					}
					$result['entite_af_code'][$i] .= "
					<tr id='tr_echange_info_".$tbl_temp[0]."_".$tbl_temp[1]."_".$lst_echanges[$e]['echange_af_id']."' class='tr_echange_".$lst_echanges[$e]['echange_af_id']."'>
						<td style='padding-top:10px;vertical-align:top;text-align:right;'>
							<label class='label'>".date_transform($lst_echanges[$e]['echange_af_date'])."<br>".$lst_echanges[$e]['type_echanges_af_lib']."</label>";
							If ($lst_echanges[$e]['result_echange_af_id'] == 1 || $lst_echanges[$e]['result_echange_af_id'] == 2) {
								$result['entite_af_code'][$i] .= "
								<br>
								<label class='label' style='color:#fe0000;'>".$lst_echanges[$e]['result_echange_af_lib']."</label>";
							}
						$result['entite_af_code'][$i] .="
						</td>
						<td style='padding-top:10px;vertical-align:top;text-align:right;'>";
						If ($lst_echanges[$e]['echange_af_description'] != 'N.D.') {
							$result['entite_af_code'][$i] .="
							<textarea id='description_echange_".$lst_echanges[$e]['echange_af_id']."_".$lst_echanges[$e]['interloc_id']."_".$tbl_temp[1]."' class='label textarea_com_edit' cols='120' rows='1' style='font-weight:normal;' READONLY>".$lst_echanges[$e]['echange_af_description']."</textarea>";
						}
						If ($lst_echanges[$e]['echange_representant'] == 'true') {
							$result['entite_af_code'][$i] .= "
							<br>
							<label class='label_simple'>Echange avec ".$interloc_lib."</label>";
						} ElseIf ($lst_echanges[$e]['echange_referent'] == 'true' && $echange_referent['interloc_id'] != $lst_echanges[$e]['interloc_id']) {
							$result['entite_af_code'][$i] .= "
							<br>
							<label class='label_simple'>Echange avec le référent</label>";
						}					
						$result['entite_af_code'][$i] .= "	
						</td>
						<td style='padding-top:10px;vertical-align:middle;text-align:right;'>";
						If ($lst_echanges[$e]['categorie'] == 'echange') {
							If ($res_echange_referent->rowCount() == 0 || $echange_referent['interloc_id'] == $lst_echanges[$e]['interloc_id']) {
								$result['entite_af_code'][$i] .= "
								<img width='15px' style='cursor:pointer;padding-left:5px;' src='".ROOT_PATH."images/edit.png' onclick=\"showFormEchange(this, '".str_replace("'", "\'", $interloc_lib)."', '";
								If ($lst_echanges[$e]['echange_representant'] == 'true') {
									If ($infos_representant['nom_usage'] != 'N.P.') {
										$result['entite_af_code'][$i] .= str_replace("'", "\'", $infos_representant['nom_usage']);
									} Else {
										$result['entite_af_code'][$i] .= str_replace("'", "\'", $infos_representant['ddenom']);
									}
								} Else {
									If ($lst_echanges[$e]['nom_usage'] != 'N.P.') {
										$result['entite_af_code'][$i] .= str_replace("'", "\'", $lst_echanges[$e]['nom_usage']);
									} Else {
										$result['entite_af_code'][$i] .= str_replace("'", "\'", $lst_echanges[$e]['ddenom']);
									}
								}
								$result['entite_af_code'][$i] .= "', '".$tbl_temp[0]."', ".$echange_interloc_id.", '".$lst_echanges[$e]['show_nego']."', ".$session_af_id.", ".$tbl_temp[1].", ".$lst_echanges[$e]['echange_af_id'].", '".$lst_echanges[$e]['echange_representant']."', 'tbl_echange_".$tbl_temp[0]."_".$tbl_temp[1]."_".implode('-', array_unique($id_table_lst_type_interloc))."', '".$gtoper."');\">
								</br>";
							}
							$result['entite_af_code'][$i] .= "
								<img width='15px' style='cursor:pointer;padding-left:5px;' src='".ROOT_PATH."images/supprimer.png' onclick=\"deleteEchange(this, ".$lst_echanges[$e]['interloc_id'].", ".$session_af_id.", ".$tbl_temp[1].", ".$lst_echanges[$e]['echange_af_id'].", '".$lst_echanges[$e]['echange_direct']."');\">";
						}
					$result['entite_af_code'][$i] .= "		
						</td>
					</tr>";
					If ($lst_echanges[$e]['date_recontact'] != 'N.D.') {
						If ($lst_echanges[$e]['rappel_traite'] == 't') {
							$suite_color = '#537a13';
						} Else {
							$suite_color = '#fe0000';
						}
						$result['entite_af_code'][$i] .=											
						"<tr id='tr_echange_rappel_".$tbl_temp[0]."_".$tbl_temp[1]."_".$lst_echanges[$e]['echange_af_id']."' class='tr_echange_".$lst_echanges[$e]['echange_af_id']."'>
							<td style='text-align:right;vertical-align:top;padding-left:25px;'>
								<label class='label' style='margin:0 4px; white-space:pre-line;color:".$suite_color.";'>A recontacter le ".date_transform($lst_echanges[$e]['date_recontact'])." : </label>
							</td>
							<td style='vertical-align:top;text-align:right;'>
								<textarea id='description_rappel_".$lst_echanges[$e]['echange_af_id']."_".$lst_echanges[$e]['interloc_id']."_".$tbl_temp[1]."' class='label textarea_com_edit' cols='120' rows='1' style='font-weight:normal;min-height:5px;color:".$suite_color.";background:none;padding:0;' READONLY>".$lst_echanges[$e]['commentaires']."</textarea>
							</td>
						</tr>";
					}
				}
				
				$lst_resultat = explode(',', $lst_echanges[$e]['tbl_resultat']);
				$lst_resultat_prix = explode(',', $lst_echanges[$e]['tbl_resultat_prix']);
				If (count($lst_resultat) > 0) {
					$lst_par_lib = explode(',', $lst_echanges[$e]['tbl_par_lib']);
					$lst_dnulot = explode(',', $lst_echanges[$e]['tbl_dnulot']);		
					$result['entite_af_code'][$i] .="
					<tr id='tr_echange_nego_".$tbl_temp[0]."_".$tbl_temp[1]."_".$lst_echanges[$e]['echange_af_id']."' class='tr_echange_".$lst_echanges[$e]['echange_af_id']."'>
						<td>&nbsp;</td>
						<td class='bilan-af'>
							<ul style='margin:0;'>";
							For ($a=0; $a<count($lst_resultat); $a++) {
								switch (substr($lst_resultat[$a], 0, 5)) {
									case 'Refus':
									case 'Défa':
										$color='#fe0000';
										break;
									case 'Accor':
									case 'Favor':
										$color='#2b933e';
										break;
									case 'Persp':
										$color='#004494';
										break;
									default:
									   $color='#004494';
								}
								If ($lst_resultat[$a] != 'N.D.') {
									$result['entite_af_code'][$i] .= "<li class='label_simple' style='color:$color;'>".$lst_par_lib[$a];
									If ($lst_dnulot[$a] != 'N.D.') {
										$result['entite_af_code'][$i] .= " ".$lst_dnulot[$a];
									}
									$result['entite_af_code'][$i] .= " : ".$lst_resultat[$a];
									If ($lst_resultat_prix[$a] != -1) { 
										$result['entite_af_code'][$i] .= " à ".$lst_resultat_prix[$a]." €";
									}
									$result['entite_af_code'][$i] .= "</li>";
								}
							}
					$result['entite_af_code'][$i] .= "
							</ul>
						</td>
					</tr>";
				}										
			}
		}
		
		print json_encode($result);
	}
	
	function fct_showFormEvenement($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure	
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres		
		$evenement_af_id = $data['params']['var_evenement_af_id'];
		$session_af_id = $data['params']['var_session_af_id'];
		
		If($evenement_af_id != -1) {
			$rq_evt_globaux = "
			SELECT DISTINCT
				evenements_af.evenement_af_date, 
				evenements_af.type_evenements_af_id,
				evenements_af.evenement_af_description
			FROM 
				animation_fonciere.evenements_af
			WHERE 
				evenements_af.evenement_af_id = :evenement_af_id";
			$res_evt_globaux = $bdd->prepare($rq_evt_globaux);
			$res_evt_globaux->execute(array('evenement_af_id'=>$evenement_af_id));
			$infos_evt_globaux = $res_evt_globaux->fetch();
			$result['date_evenement'] = date_transform($infos_evt_globaux['evenement_af_date']);
			$result['lst_type_evenement'] = $infos_evt_globaux['type_evenements_af_id'];
			$result['description_evenement'] = $infos_evt_globaux['evenement_af_description'];
		} Else {
			$result['date_evenement'] = 'N.D.';
			$result['lst_type_evenement'] = 'N.D.';
			$result['description_evenement'] = 'N.D.';
		}
		
		$rq_lst_interlocs = "
		SELECT DISTINCT
			interlocuteurs.interloc_id,
			tevenement.interloc_id_present,
			interlocuteurs.dnuper,
			d_ccoqua.particule_lib,
			interlocuteurs.ddenom,
			interlocuteurs.nom_usage,
			interlocuteurs.nom_jeunefille,
			CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
				CASE WHEN strpos(interlocuteurs.prenom_usage, ' ') > 0 THEN
					substring(interlocuteurs.prenom_usage from 0 for  strpos(interlocuteurs.prenom_usage, ' ')) 
				ELSE 
					interlocuteurs.prenom_usage
				END
			ELSE	
				'N.P.' 
			END as prenom_usage,
			CASE WHEN nom_usage != 'N.P.' THEN nom_usage ELSE ddenom END as ordre,
			interlocuteurs.commentaires
		FROM
			animation_fonciere.interlocuteurs 
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
			JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			LEFT JOIN 
				(
					SELECT 
						rel_af_evenements.interloc_id interloc_id_present
					FROM
						animation_fonciere.rel_af_evenements
					WHERE
						rel_af_evenements.session_af_id = :session_af_id
						AND rel_af_evenements.evenement_af_id = :evenement_af_id
				) tevenement ON (interlocuteurs.interloc_id = tevenement.interloc_id_present)
		WHERE
			rel_saf_foncier.session_af_id = :session_af_id
		ORDER BY ordre";
		$res_lst_interlocs = $bdd->prepare($rq_lst_interlocs);
		$res_lst_interlocs->execute(array('session_af_id'=>$session_af_id, 'evenement_af_id'=>$evenement_af_id));
		$lst_interlocs = $res_lst_interlocs->fetchall();
		$result['tbody_tbl_evenement_presents'] = '';
		For ($i=0; $i<$res_lst_interlocs->rowCount(); $i++) {
			$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_interlocs[$i]['particule_lib'], $lst_interlocs[$i]['nom_usage'], $lst_interlocs[$i]['prenom_usage'], $lst_interlocs[$i]['ddenom']));
			$result['tbody_tbl_evenement_presents'] .= "
			<tr>
				<td>
					<input type='checkbox' id='event_present_".$lst_interlocs[$i]['interloc_id']."' class='event_present'"; If ($evenement_af_id == -1 OR $lst_interlocs[$i]['interloc_id'] == $lst_interlocs[$i]['interloc_id_present'] ) {$result['tbody_tbl_evenement_presents'] .= " checked ";} $result['tbody_tbl_evenement_presents'] .= ">
				</td>
				<td style='text-align:left;'>
					<label class='label'>".$interloc_lib."</label>
					<img width='15px' style='cursor:pointer;padding-left:5px;' src='".ROOT_PATH."images/edit.png' onclick=\"$('#tr_evt_com_".$lst_interlocs[$i]['interloc_id']."').toggle('display');\">
				</td>
			</tr>
			<tr id='tr_evt_com_".$lst_interlocs[$i]['interloc_id']."' style='display:none'>
				<td colspan='2'>
					<textarea id='txt_evt_com_".$lst_interlocs[$i]['interloc_id']."' class='label textarea_com_edit' cols='120' style='font-weight:normal;' onkeyup=\"resizeTextarea('txt_evt_com_".$lst_interlocs[$i]['interloc_id']."');\" onchange=\"resizeTextarea('txt_evt_com_".$lst_interlocs[$i]['interloc_id']."');\">".$lst_interlocs[$i]['commentaires']."</textarea>
				</td>
			</tr>";
		}		
		
		$res_lst_interlocs->closeCursor();
		
		print json_encode($result);
	}
	
	function fct_showFormUpdateEchange($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure	
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$interloc_id = $data['params']['var_interloc_id'];
		$echange_af_id = $data['params']['var_echange_af_id'];
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];		
		$representant = $data['params']['var_representant'];		
		
		$rq_echange_af = "
		SELECT DISTINCT
			echange_af_date,
			echange_af_description,
			type_echanges_af_id,
			echange_referent::text,
			CASE WHEN rel_af_echanges.date_recontact = 'N.D.' THEN Null ELSE rel_af_echanges.date_recontact END as date_recontact,
			CASE WHEN rel_af_echanges.commentaires = 'N.D.' THEN Null ELSE rel_af_echanges.commentaires END as commentaires,
			rel_af_echanges.rappel_traite::text,
			result_echange_af_id,
			count(nego_af_id) as nbr_parc_maitrise
		FROM
			animation_fonciere.echanges_af
			JOIN animation_fonciere.rel_af_echanges USING (echange_af_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
		WHERE 
			echanges_af.echange_af_id = $echange_af_id
		GROUP BY 
			echanges_af.echange_af_date,
			echange_af_description,
			type_echanges_af_id,
			echange_referent,
			rel_af_echanges.date_recontact,
			rel_af_echanges.commentaires,
			rappel_traite,
			result_echange_af_id";
		// echo $rq_echange_af;
		$res_echange_af = $bdd->prepare($rq_echange_af);
		$res_echange_af->execute();
		$lst_res_echange_af = $res_echange_af->fetch();
		$result['date_echange'] = date_transform($lst_res_echange_af['echange_af_date']);
		$result['description_echange'] = $lst_res_echange_af['echange_af_description'];
		$result['lst_type_echange'] = $lst_res_echange_af['type_echanges_af_id'];
		If ($lst_res_echange_af['date_recontact'] == Null) {
			$result['date_echange_recontact'] = Null;
		} Else {
			$result['date_echange_recontact'] = date_transform($lst_res_echange_af['date_recontact']);
		}		
		$result['motif_echange_recontact'] = $lst_res_echange_af['commentaires'];
		If ($lst_res_echange_af['rappel_traite'] == 'true' && $lst_res_echange_af['date_recontact'] != Null) {
			$result['echange_chk_rappel_traite'] = array('display'=>'', 'checked'=>'checked', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
		} ElseIf ($lst_res_echange_af['rappel_traite'] == 'false' && $lst_res_echange_af['date_recontact'] != Null) {
			$result['echange_chk_rappel_traite'] = array('display'=>'', 'checked'=>'', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
		} Else {
			$result['echange_chk_rappel_traite'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#004494');
		}
		
		$rq_check_referent = "
		SELECT
			interloc_id
		FROM
			animation_fonciere.interloc_referent
			JOIN animation_fonciere.rel_saf_foncier_interlocs ON (interloc_referent.rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
		WHERE
			rel_saf_foncier_interlocs.interloc_id = $interloc_id
			AND session_af_id = $session_af_id
			AND entite_af_id = $entite_af_id";
		$res_check_referent = $bdd->prepare($rq_check_referent);
		$res_check_referent->execute();
		
		If ($res_check_referent->rowCount() > 0) {
			If ($lst_res_echange_af['echange_referent'] == 'true') {
				$result['chk_echange_referent'] = array('display'=>'', 'checked'=>'checked', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
				$display_tbl_parc_maitrise_other_eaf = '';
			} Else {
				$result['chk_echange_referent'] = array('display'=>'', 'checked'=>'', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
				$display_tbl_parc_maitrise_other_eaf = 'none';
			}			
		} Else {
			$result['chk_echange_referent'] = array('display'=>'none', 'checked'=>'', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
			$display_tbl_parc_maitrise_other_eaf = 'none';
		}
		
		Switch ($lst_res_echange_af['result_echange_af_id']) {
			case 1 :
				$result['chk_attente_reponse'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
				$result['chk_interloc_injoignable'] = array('display'=>'', 'checked'=>'checked', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
				$result['chk_modif_infos_interloc'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
				$result['chk_maitrise_parc'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
				break;
			case 2 :
				$result['chk_attente_reponse'] = array('display'=>'', 'checked'=>'checked', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
				$result['chk_interloc_injoignable'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
				$result['chk_modif_infos_interloc'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
				$result['chk_maitrise_parc'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
				break;
			default :
				If ($lst_res_echange_af['nbr_parc_maitrise'] > 0) {
					$result['chk_attente_reponse'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
					$result['chk_interloc_injoignable'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
					$result['chk_modif_infos_interloc'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
					$result['chk_maitrise_parc'] = array('display'=>'', 'checked'=>'checked', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
				} Else {
					$result['chk_attente_reponse'] = array('display'=>'', 'checked'=>'', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
					$result['chk_interloc_injoignable'] = array('display'=>'', 'checked'=>'', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
					$result['chk_modif_infos_interloc'] = array('display'=>'', 'checked'=>'', 'readonly'=>true, 'disabled'=>true, 'color'=>'#6a6a6a');
					$result['chk_maitrise_parc'] = array('display'=>'', 'checked'=>'', 'readonly'=>false, 'disabled'=>false, 'color'=>'#004494');
				}
				break;
		}
		
		$result['tbody_tbl_maitrise_parc'] = fct_echange_tbl_maitrise_parc($session_af_id, $entite_af_id, $interloc_id, $echange_af_id, $display_tbl_parc_maitrise_other_eaf, $representant);
		
		$rq_prix_eaf_negocie = "
		SELECT DISTINCT
			prix_eaf_negocie
		FROM
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		WHERE
			entite_af_id = :entite_af_id";
		$res_prix_eaf_negocie = $bdd->prepare($rq_prix_eaf_negocie);
		// echo $rq_prix_eaf_negocie;
		$res_prix_eaf_negocie->execute(array('entite_af_id'=>$entite_af_id));
		If ($res_prix_eaf_negocie->rowCount() > 1) {
			$rq_delete = "
			UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
				(
					SELECT 
						rel_eaf_foncier_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						entite_af_id = :entite_af_id
				)";
			$res_delete = $bdd->prepare($rq_delete);
			// echo $rq_delete.'<br>';
			$res_delete->execute(array('entite_af_id'=>$entite_af_id));
			$res_delete->closeCursor();
			$result['prix_eaf_negocie'] = '';
		} Else {
			$prix_eaf_negocie = $res_prix_eaf_negocie->fetch();
			$result['prix_eaf_negocie'] = $prix_eaf_negocie['prix_eaf_negocie'];
		}
		
		print json_encode($result);
	}
		
	function fct_showFormAddInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		
		$rq_info_lots = "
			SELECT DISTINCT
				rel_saf_foncier_id,
				communes.nom,
				t4.cad_site_id,
				t1.dnupla,
				t1.ccosec,
				COALESCE(t2.dnulot, 'N.D.') as dnulot,
				t2.lot_id,
				t2.dcntlo,
				COALESCE(rel_eaf_foncier.utilissol, 'N.D.') AS utilissol,
				rel_saf_foncier.paf_id,
				d_paf.paf_lib,
				COALESCE(d_objectif_af.objectif_af_lib, 'N.D.') as objectif_af_lib
			FROM
				administratif.communes
				JOIN cadastre.parcelles_cen t1 ON (code_insee = codcom)
				JOIN cadastre.lots_cen t2 USING (par_id)
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.d_paf USING (paf_id)
				LEFT JOIN animation_fonciere.d_objectif_af USING (objectif_af_id)
			WHERE 
				entite_af_id = :entite_af_id AND 
				rel_saf_foncier.session_af_id = :session_af_id AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'
			ORDER BY t2.lot_id";
		$res_info_lots = $bdd->prepare($rq_info_lots);
		$res_info_lots->execute(array('entite_af_id'=>$entite_af_id, 'session_af_id'=>$session_af_id));
		$info_lots = $res_info_lots->fetchall();
		$result['code'] = "";
		For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 
			$result['code'] .= "
			<tr>
				<td class='no-border' style='margin-top:5px;width:1%;'>
					<input id='chk_".$info_lots[$l]['rel_saf_foncier_id']."' class='chk_parc' checked value='".$info_lots[$l]['rel_saf_foncier_id']."'  type='checkbox'>
					<label class='label'>
						".$info_lots[$l]['nom']." - ".$info_lots[$l]['ccosec']." - ".$info_lots[$l]['dnupla'];
					If ($info_lots[$l]['dnulot'] != 'N.D.') {
						$result['code'] .= " - " . $info_lots[$l]['dnulot'];
					}
					$result['code'] .= "
					</label>
				</td>
				<td class='only_proprio'>
					<label class='btn_combobox'> 
						<select id='new_interloc_ccodem-".$info_lots[$l]['rel_saf_foncier_id']."' class='combobox'>
							<option value='-1'></option>";
							$rq_lst_ccodem = "SELECT DISTINCT ccodem, dqualp FROM cadastre.d_ccodem ORDER BY dqualp";
							$res_lst_ccodem = $bdd->prepare($rq_lst_ccodem);
							$res_lst_ccodem->execute();
							While ($donnees = $res_lst_ccodem->fetch())  {
								$result['code'] .= "
								<option value='$donnees[ccodem]'>$donnees[dqualp]</option>";
							}
						$result['code'] .= "	
						</select>
					</label>
				</td>
				<td class='only_proprio'>
					<label class='btn_combobox'> 
						<select id='new_interloc_ccodro-".$info_lots[$l]['rel_saf_foncier_id']."' class='combobox'>
							<option value='-1'></option>";
							$rq_lst_ccodro = "SELECT DISTINCT ccodro, droit_lib FROM cadastre.d_ccodro WHERE ccodro IN ('P', 'U', 'N', 'A', 'E', 'G') ORDER BY droit_lib";
							$res_lst_ccodro = $bdd->prepare($rq_lst_ccodro);
							$res_lst_ccodro->execute();
							While ($donnees = $res_lst_ccodro->fetch())  {
								$result['code'] .= "
								<option value='$donnees[ccodro]'>$donnees[droit_lib]</option>";
							}
						$result['code'] .= "
						</select>
					</label>
				</td>
			</tr>";
		}
		
		print json_encode($result);
	}
	
	function fct_showFormUpdateInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$interloc_id = $data['params']['var_interloc_id'];
		$type_interloc_id = $data['params']['var_type_interloc_id'];
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		
		$rq_info_interlocs = "
		SELECT DISTINCT
			COALESCE(d_ccoqua.ccoqua::text, 'N.D.') as particule_id,
			COALESCE(interlocuteurs.ddenom, 'N.D.') as ddenom,
			COALESCE(interlocuteurs.nom_usage, 'N.D.') as nom_usage,
			COALESCE(interlocuteurs.nom_jeunefille, 'N.D.') as nom_jeunefille,
			COALESCE(interlocuteurs.prenom_usage, 'N.D.') as prenom_usage,
			COALESCE(interlocuteurs.nom_conjoint, 'N.D.') as nom_conjoint,
			COALESCE(interlocuteurs.prenom_conjoint, 'N.D.') as prenom_conjoint,
			COALESCE(REPLACE(interlocuteurs.jdatnss, '-', ''), 'N.D.') as jdatnss,
			COALESCE(interlocuteurs.dldnss, 'N.D.') as dldnss,
			COALESCE(interlocuteurs.dlign3, 'N.D.') as dlign3,
			COALESCE(interlocuteurs.dlign4, 'N.D.') as dlign4,
			COALESCE(interlocuteurs.dlign5, 'N.D.') as dlign5,
			COALESCE(interlocuteurs.dlign6, 'N.D.') as dlign6,
			COALESCE(interlocuteurs.email, 'N.D.') as email,
			COALESCE(interlocuteurs.fixe_domicile, 'N.D.') as fixe_domicile,
			COALESCE(interlocuteurs.fixe_travail, 'N.D.') as fixe_travail,
			COALESCE(interlocuteurs.portable_domicile, 'N.D.') as portable_domicile,
			COALESCE(interlocuteurs.portable_travail, 'N.D.') as portable_travail,
			COALESCE(interlocuteurs.fax, 'N.D.') as fax,
			d_type_interloc.type_interloc_id, 
			etat_interloc_id, 
			interlocuteurs.commentaires,
			COALESCE(d_ccodem.ccodem::text, 'N.D.') as ccodem,
			COALESCE(d_ccodro.ccodro::text, 'N.D.') as ccodro
		FROM 
			animation_fonciere.interlocuteurs
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
			LEFT JOIN cadastre.d_ccodem USING (ccodem)
			LEFT JOIN cadastre.d_ccodro USING (ccodro)
			JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
		WHERE 
			interlocuteurs.interloc_id = ".$interloc_id." AND type_interloc_id = ".$type_interloc_id." AND session_af_id = ".$session_af_id." AND entite_af_id = " .$entite_af_id;
		// echo $rq_info_interlocs;
		$res_info_interlocs = $bdd->prepare($rq_info_interlocs);
		$res_info_interlocs->execute();
		$lst_interlocs = $res_info_interlocs->fetch();				
		$result['update_type_interloc'] = $lst_interlocs['type_interloc_id'];
		$result['update_etat_interloc'] = $lst_interlocs['etat_interloc_id'];
		$result['update_interloc_ccodem'] = $lst_interlocs['ccodem'];
		$result['update_interloc_ccodro'] = $lst_interlocs['ccodro'];
		$result['update_interloc_lst_particule'] = $lst_interlocs['particule_id'];
		$result['update_interloc_ddenom'] = $lst_interlocs['ddenom'];
		$result['update_interloc_nom_usage'] = $lst_interlocs['nom_usage'];
		$result['update_interloc_prenom_usage'] = $lst_interlocs['prenom_usage'];
		If ($lst_interlocs['jdatnss'] != 'N.D.') {
			$result['update_interloc_date_naissance'] = substr($lst_interlocs['jdatnss'], 6, 2) . '/' . substr($lst_interlocs['jdatnss'], 4, 2) . '/' . substr($lst_interlocs['jdatnss'], 0,4);
		} Else {
			$result['update_interloc_date_naissance'] = $lst_interlocs['jdatnss'];
		}			
		$result['update_interloc_lieu_naissance'] = $lst_interlocs['dldnss'];
		$result['update_interloc_nom_conjoint'] = $lst_interlocs['nom_conjoint'];
		$result['update_interloc_prenom_conjoint'] = $lst_interlocs['prenom_conjoint'];
		$result['update_interloc_nom_jeunefille'] = $lst_interlocs['nom_jeunefille'];
		$result['update_interloc_adresse1'] = $lst_interlocs['dlign3'];
		$result['update_interloc_adresse2'] = $lst_interlocs['dlign4'];
		$result['update_interloc_adresse3'] = $lst_interlocs['dlign5'];
		$result['update_interloc_adresse4'] = $lst_interlocs['dlign6'];
		$result['update_interloc_tel_fix1'] = $lst_interlocs['fixe_domicile'];
		$result['update_interloc_tel_fix2'] = $lst_interlocs['fixe_travail'];
		$result['update_interloc_tel_portable1'] = $lst_interlocs['portable_domicile'];
		$result['update_interloc_tel_portable2'] = $lst_interlocs['portable_travail'];
		$result['update_interloc_tel_fax'] = $lst_interlocs['fax'];
		$result['update_interloc_mail'] = $lst_interlocs['email'];
		$result['update_interloc_commentaires'] = $lst_interlocs['commentaires'];
		
		$rq_parc_interloc = "
		SELECT DISTINCT
			t3.lot_id,
			rel_saf_foncier.rel_saf_foncier_id,
			communes.nom,
			t4.cad_site_id,
			t1.dnupla,
			t1.ccosec,
			COALESCE(t2.dnulot, 'N.D.') as dnulot,
			CASE WHEN rel_saf_foncier_id IN 
				(
					SELECT 
						rel_saf_foncier_id 
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs 
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE 
						rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
						rel_saf_foncier_interlocs.type_interloc_id = :type_interloc_id AND
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id
				)
			THEN 
				'oui' 
			ELSE 
				'non' 
			END as parc_interloc,
			CASE WHEN rel_saf_foncier_id IN 
				(
					SELECT DISTINCT
						rel_saf_foncier_id 
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs 
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
					WHERE 
						rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
						rel_saf_foncier_interlocs.type_interloc_id = :type_interloc_id AND
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id
						
					UNION

					SELECT DISTINCT
						rel_saf_foncier_id 
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs 
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_af_echanges ON (rel_af_echanges.rel_saf_foncier_representant_id = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
					WHERE 
						rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
						rel_saf_foncier_interlocs.type_interloc_id = :type_interloc_id AND
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id
				)
			THEN 
				'oui' 
			ELSE 
				'non' 
			END as parc_echange,
			ccodro,
			ccodem
		FROM 
			animation_fonciere.rel_saf_foncier	
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			JOIN foncier.cadastre_site t4 USING (cad_site_id)	
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.lots_cen t2 USING (lot_id)
			JOIN cadastre.parcelles_cen t1 USING (par_id)
			JOIN administratif.communes ON (code_insee = codcom)
			LEFT JOIN 
				(
					SELECT
						rel_saf_foncier_id,
						ccodro,
						ccodem
					FROM
						animation_fonciere.rel_saf_foncier_interlocs			 
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
						rel_saf_foncier_interlocs.type_interloc_id = :type_interloc_id AND
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id
				) t_droit USING (rel_saf_foncier_id)
		WHERE 
			session_af_id = :session_af_id AND
			entite_af_id = :entite_af_id AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'
		ORDER BY lot_id";
		// echo $rq_parc_interloc;
		$res_parc_interloc = $bdd->prepare($rq_parc_interloc);
		$res_parc_interloc->execute(array('interloc_id'=>$interloc_id, 'type_interloc_id'=>$type_interloc_id, 'session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$lst_parc_interloc = $res_parc_interloc->fetchall();
		$result['nbr_parc_interloc'] = $res_parc_interloc->rowCount();
		$result['parc_interloc'] = '';
		$tbl_parc_interloc = [];
		
		For ($i=0; $i<$res_parc_interloc->rowCount(); $i++) {
			$tbl_parc_interloc[] = $lst_parc_interloc[$i]['rel_saf_foncier_id'];
			$result['parc_interloc'] .= "
			<tr>
				<td>
					<input type='checkbox' ";
					If ($lst_parc_interloc[$i]['parc_interloc'] == 'oui') {
						$result['parc_interloc'] .= "checked ";
					}
					$result['parc_interloc'] .= "
					class='update_chk_parc ";
					If ($lst_parc_interloc[$i]['parc_echange'] == 'oui') {
						$result['parc_interloc'] .= "with_echange";
					}
					$result['parc_interloc'] .= "' id='update_chk_".$lst_parc_interloc[$i]['rel_saf_foncier_id']."' value='".$lst_parc_interloc[$i]['rel_saf_foncier_id']."' onclick=\"checkEchange('update');\">
					<label id='interloc_lbl_lot_id-".$lst_parc_interloc[$i]['rel_saf_foncier_id']."' class='label'>
						".$lst_parc_interloc[$i]['nom']." - ".$lst_parc_interloc[$i]['ccosec']." - ".$lst_parc_interloc[$i]['dnupla'];
					If ($lst_parc_interloc[$i]['dnulot'] != 'N.D.') {
						$result['parc_interloc'] .= " - " . $lst_parc_interloc[$i]['dnulot'];
					}
					$result['parc_interloc'] .= "
					</label>
					<br><label id='msg_update_chk_".$lst_parc_interloc[$i]['rel_saf_foncier_id']."' class='last_maj' style='color:#fe0000;display:none;'><i>Attention, cette action supprimera des échanges</i></label>
				</td>
				<td class='only_proprio'"; If ($lst_interlocs['type_interloc_id'] != 1) {$result['parc_interloc'] .= "style='opacity:0.5;'";} $result['parc_interloc'] .= ">
					<label class='btn_combobox'> 
						<select id='update_interloc_ccodem-".$lst_parc_interloc[$i]['rel_saf_foncier_id']."' class='combobox'";
						If ($lst_interlocs['type_interloc_id'] != 1) {$result['parc_interloc'] .= " readonly disabled";}
						$result['parc_interloc'] .= ">
							<option value='-1'></option>";
							$rq_lst_ccodem = "SELECT DISTINCT ccodem, dqualp FROM cadastre.d_ccodem ORDER BY dqualp";
							$res_lst_ccodem = $bdd->prepare($rq_lst_ccodem);
							$res_lst_ccodem->execute();
							While ($donnees = $res_lst_ccodem->fetch())  {
								$result['parc_interloc'] .= "
								<option ";
								If ($donnees['ccodem'] == $lst_parc_interloc[$i]['ccodem']) {
									$result['parc_interloc'] .= "selected ";
								}
								$result['parc_interloc'] .= "value='$donnees[ccodem]'>$donnees[dqualp]</option>";
							}
						$result['parc_interloc'] .= "	
						</select>
					</label>
				</td>
				<td class='only_proprio'"; If ($lst_interlocs['type_interloc_id'] != 1) {$result['parc_interloc'] .= "style='opacity:0.5;'";} $result['parc_interloc'] .= ">
					<label class='btn_combobox'> 
						<select id='update_interloc_ccodro-".$lst_parc_interloc[$i]['rel_saf_foncier_id']."' class='combobox'";
						If ($lst_interlocs['type_interloc_id'] != 1) {$result['parc_interloc'] .= " readonly disabled";}
						$result['parc_interloc'] .= ">
							<option value='-1'></option>";
							$rq_lst_ccodro = "SELECT DISTINCT ccodro, droit_lib FROM cadastre.d_ccodro WHERE ccodro IN ('P', 'U', 'N', 'A', 'E', 'G') ORDER BY droit_lib";
							$res_lst_ccodro = $bdd->prepare($rq_lst_ccodro);
							$res_lst_ccodro->execute();
							While ($donnees = $res_lst_ccodro->fetch())  {
								$result['parc_interloc'] .= "
								<option ";
								If ($donnees['ccodro'] == $lst_parc_interloc[$i]['ccodro']) {
									$result['parc_interloc'] .= "selected ";
								}
								$result['parc_interloc'] .= "value='$donnees[ccodro]'>$donnees[droit_lib]</option>";
							}
						$result['parc_interloc'] .= "
						</select>
					</label>
				</td>
			</tr>";
		}
		$result['tbl_parc_interloc'] = implode("_", $tbl_parc_interloc);
		print json_encode($result);
	}
	
	function fct_showTblModifInfosInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$interloc_id = $data['params']['var_interloc_id'];
		
		$rq_info_interloc = "
		SELECT DISTINCT
			COALESCE(d_ccoqua.ccoqua::text, 'N.D.') as particule_id,
			COALESCE(interlocuteurs.ddenom, 'N.D.') as ddenom,
			COALESCE(interlocuteurs.nom_usage, 'N.D.') as nom_usage,
			COALESCE(interlocuteurs.nom_jeunefille, 'N.D.') as nom_jeunefille,
			COALESCE(interlocuteurs.prenom_usage, 'N.D.') as prenom_usage,
			COALESCE(interlocuteurs.nom_conjoint, 'N.D.') as nom_conjoint,
			COALESCE(interlocuteurs.prenom_conjoint, 'N.D.') as prenom_conjoint,
			COALESCE(REPLACE(interlocuteurs.jdatnss, '-', ''), 'N.D.') as jdatnss,
			COALESCE(interlocuteurs.dldnss, 'N.D.') as dldnss,
			COALESCE(interlocuteurs.dlign3, 'N.D.') as dlign3,
			COALESCE(interlocuteurs.dlign4, 'N.D.') as dlign4,
			COALESCE(interlocuteurs.dlign5, 'N.D.') as dlign5,
			COALESCE(interlocuteurs.dlign6, 'N.D.') as dlign6,
			COALESCE(interlocuteurs.email, 'N.D.') as email,
			COALESCE(interlocuteurs.fixe_domicile, 'N.D.') as fixe_domicile,
			COALESCE(interlocuteurs.fixe_travail, 'N.D.') as fixe_travail,
			COALESCE(interlocuteurs.portable_domicile, 'N.D.') as portable_domicile,
			COALESCE(interlocuteurs.portable_travail, 'N.D.') as portable_travail,
			COALESCE(interlocuteurs.fax, 'N.D.') as fax,
			gtoper
		FROM 
			animation_fonciere.interlocuteurs 
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
		WHERE 
			interlocuteurs.interloc_id = :interloc_id";
		// echo $rq_info_interlocs;
		$res_info_interloc = $bdd->prepare($rq_info_interloc);
		$res_info_interloc->execute(array('interloc_id'=>$interloc_id));
		$lst_interloc = $res_info_interloc->fetch();				
		$result['update_interloc_lst_particule'] = $lst_interloc['particule_id'];
		$result['update_interloc_ddenom'] = $lst_interloc['ddenom'];
		$result['update_interloc_nom_usage'] = $lst_interloc['nom_usage'];
		$result['update_interloc_prenom_usage'] = $lst_interloc['prenom_usage'];		
		If ($lst_interloc['gtoper'] != 1 && $lst_interloc['gtoper'] != 2) {
			If ($lst_interloc['nom_usage'] == 'N.P.' && substr_count($lst_interloc['ddenom'], '/') > 0) {
				$result['update_interloc_nom_usage'] = substr($lst_interloc['ddenom'], 0 ,strpos($lst_interloc['ddenom'], '/'));
			}
			If ($lst_interloc['prenom_usage'] == 'N.P.' && substr_count($lst_interloc['ddenom'], '/') > 0) {
				$result['update_interloc_prenom_usage'] = ucwords(strtolower(substr($lst_interloc['ddenom'], strpos($lst_interloc['ddenom'], '/')+1 , strlen($lst_interloc['ddenom']))));
				Foreach (array('-', '\'') as $delimiter) {
					If (strpos($result['update_interloc_prenom_usage'], $delimiter)!==false) {
						$result['update_interloc_prenom_usage'] = implode($delimiter, array_map('ucfirst', explode($delimiter, $prenom_usage)));
					}
				}
			}
		}
		If ($lst_interloc['jdatnss'] != 'N.D.' && $lst_interloc['jdatnss'] != 'N.P.') {
			$result['update_interloc_date_naissance'] = substr($lst_interloc['jdatnss'], 6, 2) . '/' . substr($lst_interloc['jdatnss'], 4, 2) . '/' . substr($lst_interloc['jdatnss'], 0,4);
		} Else {
			$result['update_interloc_date_naissance'] = $lst_interloc['jdatnss'];
		}			
		$result['update_interloc_lieu_naissance'] = $lst_interloc['dldnss'];
		$result['update_interloc_nom_conjoint'] = $lst_interloc['nom_conjoint'];
		$result['update_interloc_prenom_conjoint'] = $lst_interloc['prenom_conjoint'];
		$result['update_interloc_nom_jeunefille'] = $lst_interloc['nom_jeunefille'];
		$result['update_interloc_adresse1'] = $lst_interloc['dlign3'];
		$result['update_interloc_adresse2'] = $lst_interloc['dlign4'];
		$result['update_interloc_adresse3'] = $lst_interloc['dlign5'];
		$result['update_interloc_adresse4'] = $lst_interloc['dlign6'];
		$result['update_interloc_tel_fix1'] = $lst_interloc['fixe_domicile'];
		$result['update_interloc_tel_fix2'] = $lst_interloc['fixe_travail'];
		$result['update_interloc_tel_portable1'] = $lst_interloc['portable_domicile'];
		$result['update_interloc_tel_portable2'] = $lst_interloc['portable_travail'];
		$result['update_interloc_tel_fax'] = $lst_interloc['fax'];
		$result['update_interloc_mail'] = $lst_interloc['email'];
		
		print json_encode($result);
	}
	
	// Cette fonction permet de mettre à jour la table interlocuteurs depuis la page fiche_saf.php
	function fct_updateInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		$interloc_id = $data['params']['var_interloc_id'];
		$tbl_rel_saf_foncier_interlocs_id = $data['params']['var_tbl_rel_saf_foncier_interlocs_id'];
		$type_interloc = $data['params']['type_interloc']; //type d'interlocuteur
		$etat_interloc = $data['params']['etat_interloc']; //type d'interlocuteur
		$bad_caract = array("-","–","’", "'");
		$good_caract = array("–","-","''", "''");
		$nom = str_replace($bad_caract, $good_caract, $data['params']['nom']);
		$ddenom = str_replace($bad_caract, $good_caract, $data['params']['ddenom']);
		$adresse1 = str_replace($bad_caract, $good_caract, $data['params']['adresse1']);
		$adresse2 = str_replace($bad_caract, $good_caract, $data['params']['adresse2']);
		$adresse3 = str_replace($bad_caract, $good_caract, $data['params']['adresse3']);
		$adresse4 = str_replace($bad_caract, $good_caract, $data['params']['adresse4']);
		$tel_fixe1 = str_replace($bad_caract, $good_caract, $data['params']['tel_fixe1']);
		$tel_fixe2 = str_replace($bad_caract, $good_caract, $data['params']['tel_fixe2']);
		$tel_portable1 = str_replace($bad_caract, $good_caract, $data['params']['tel_portable1']);
		$tel_portable2 = str_replace($bad_caract, $good_caract, $data['params']['tel_portable2']);
		$fax = str_replace($bad_caract, $good_caract, $data['params']['fax']);
		$mail = str_replace($bad_caract, $good_caract, $data['params']['mail']);		
		$commentaires = str_replace($bad_caract, $good_caract, $data['params']['commentaires']);		
		$tbl_rel_saf_foncier_id_assoc = $data['params']['var_tbl_rel_saf_foncier_id_assoc'];
		$tbl_rel_saf_foncier_id_dissoc = $data['params']['var_tbl_rel_saf_foncier_id_dissoc'];
		$gtoper = $data['params']['var_gtoper'];
			
		//Ajout de l'interlocuteur à partir du formaulaire prévu à cet effet
		If ($gtoper == '1') {
			$particule_id = str_replace($bad_caract, $good_caract, $data['params']['particule_id']);
			$prenom = str_replace($bad_caract, $good_caract, $data['params']['prenom']);
			$date_naissance = str_replace($bad_caract, $good_caract, $data['params']['date_naissance']);
			$lieu_naissance = str_replace($bad_caract, $good_caract, $data['params']['lieu_naissance']);
			$nom_conjoint = str_replace($bad_caract, $good_caract, $data['params']['nom_conjoint']);
			$prenom_conjoint = str_replace($bad_caract, $good_caract, $data['params']['prenom_conjoint']);
			$nom_jeune_fille = str_replace($bad_caract, $good_caract, $data['params']['nom_jeune_fille']);
			$rq_update_interloc =
			"UPDATE animation_fonciere.interlocuteurs SET 
				ccoqua = $particule_id,
				ddenom = '".strtoupper($nom.'/'.$prenom)."',
				nom_usage = '".strtoupper($nom)."',
				nom_jeunefille = CASE WHEN '".strtoupper($nom_jeune_fille)."' != '' THEN '".strtoupper($nom_jeune_fille)."' ELSE CASE WHEN $particule_id = 1 THEN 'N.P.' ELSE 'N.P.' END END,
				prenom_usage = '".strtoupper($prenom)."',
				nom_conjoint =  CASE WHEN '".strtoupper($nom_conjoint)."' != '' THEN '".strtoupper($nom_conjoint)."' ELSE 'N.D.' END,
				prenom_conjoint = CASE WHEN '".strtoupper($prenom_conjoint)."' != '' THEN '".strtoupper($prenom_conjoint)."' ELSE 'N.D.' END,
				jdatnss = CASE WHEN LENGTH('".$date_naissance."') = 10 THEN '".substr($date_naissance, 6, 4) . substr($date_naissance, 3, 2) . substr($date_naissance, 0,2)."' ELSE 'N.D.' END,
				dldnss = '".strtoupper($lieu_naissance)."',";
		} Else {
			$rq_update_interloc =
			"UPDATE animation_fonciere.interlocuteurs SET 
				ccoqua = NULL,
				ddenom = '".strtoupper($ddenom)."',
				nom_usage = 'N.P.',
				nom_jeunefille = 'N.P.',
				prenom_usage = 'N.P.',
				nom_conjoint =  'N.P.',
				prenom_conjoint = 'N.P.',
				jdatnss = 'N.P.',
				dldnss = 'N.P.',";
		}
		
		$rq_update_interloc .= "
			etat_interloc_id = $etat_interloc,
			commentaires = CASE WHEN '$commentaires' != '' THEN '$commentaires' ELSE NULL END,
			dlign3 = CASE WHEN '$adresse1' != '' THEN '$adresse1' ELSE 'N.D.' END,
			dlign4 = CASE WHEN '$adresse2' != '' THEN '$adresse2' ELSE 'N.D.' END,
			dlign5 = CASE WHEN '$adresse3' != '' THEN '$adresse3' ELSE 'N.D.' END,
			dlign6 = CASE WHEN '$adresse4' != '' THEN '$adresse4' ELSE 'N.D.' END,
			email = CASE WHEN '$mail' != '' THEN '$mail' ELSE 'N.D.' END,
			fixe_domicile = CASE WHEN '$tel_fixe1' != '' THEN '$tel_fixe1' ELSE 'N.D.' END,
			fixe_travail = CASE WHEN '$tel_fixe2' != '' THEN '$tel_fixe2' ELSE 'N.D.' END,
			portable_domicile = CASE WHEN '$tel_portable1' != '' THEN '$tel_portable1' ELSE 'N.D.' END,
			portable_travail = CASE WHEN '$tel_portable2' != '' THEN '$tel_portable2' ELSE 'N.D.' END,
			fax = CASE WHEN '$fax' != '' THEN '$fax' ELSE 'N.D.' END
		WHERE interloc_id = $interloc_id";			
		$res_update_interloc = $bdd->prepare($rq_update_interloc);
		// echo $rq_update_interloc;
		$res_update_interloc->execute();
		$res_update_interloc->closeCursor();
		
		$rq_delete_ref_inactif = "
		DELETE FROM animation_fonciere.interloc_referent 
		WHERE 
			rel_saf_foncier_interlocs_id_ref IN 
				(
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.interlocuteurs
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
					WHERE 
						etat_interloc_id != 1
				) OR 
			rel_saf_foncier_interlocs_id IN 
				(
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.interlocuteurs
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
					WHERE 
						etat_interloc_id != 1
				)";
		// echo $rq_delete_ref_inactif;
		$res_delete_ref_inactif = $bdd->prepare($rq_delete_ref_inactif);
		$res_delete_ref_inactif->execute();	
		$res_delete_ref_inactif->closeCursor();
				
		$rq_delete_subst_inactif = "
		DELETE FROM animation_fonciere.interloc_substitution 
		WHERE 			
			rel_saf_foncier_interlocs_id_is IN 
				(
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.interlocuteurs
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
					WHERE 
						etat_interloc_id != 1
				)";
		// echo $rq_delete_subst_inactif;
		$res_delete_subst_inactif = $bdd->prepare($rq_delete_subst_inactif);
		$res_delete_subst_inactif->execute();	
		$res_delete_subst_inactif->closeCursor();			
		
		$rq_check_modif = "
			SELECT 
				type_interloc_id::text as check_type_interloc,
				array_to_string(array_agg(rel_saf_foncier_id ORDER BY rel_saf_foncier_id), ',') as check_tbl_rel_saf_foncier_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
			WHERE 
				rel_saf_foncier_interlocs_id IN (" . str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id) . ")
			GROUP BY type_interloc_id, ccodro, ccodem";
		$res_check_modif = $bdd->prepare($rq_check_modif);
		// echo $rq_check_modif;
		$res_check_modif->execute();
		$check_modif = $res_check_modif->fetch();		
		$res_check_modif->closeCursor();
		
		//Si des parcelles ont été dissociées il faut supprimer les lignes relatives à ces parcelles et ce propriétaire dans les différentes tables où elles sont utilisées
		If ($tbl_rel_saf_foncier_id_dissoc != 'N.D.') {
			//Dans le cas où le propriétaire est référent il faut nettoyer la table correspondante
			$rq_delete_ref_interloc = "
			DELETE FROM animation_fonciere.interloc_referent 
			WHERE 
				rel_saf_foncier_interlocs_id_ref IN 
					(
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
						WHERE 
							rel_saf_foncier_interlocs_id IN (".str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id).")
					) OR 
				rel_saf_foncier_interlocs_id IN 
					(
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
						WHERE 
							rel_saf_foncier_interlocs_id IN (".str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id).") AND 
							rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
					)";
			// echo $rq_delete_ref_interloc;
			$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
			$res_delete_ref_interloc->execute();	
			$res_delete_ref_interloc->closeCursor();
			
			//Dans le cas où le propriétaire est un interlocuteur de substitution il faut également nettoyer la table correspondante
			$rq_delete_interloc_substitution = "
			DELETE FROM animation_fonciere.interloc_substitution 
			WHERE 
				rel_saf_foncier_interlocs_id_vp IN 
					(
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
						WHERE 
							rel_saf_foncier_interlocs_id IN (".str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id).") AND 
							rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
					) OR
				rel_saf_foncier_interlocs_id_is IN 
					(
						SELECT rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
						WHERE 
							rel_saf_foncier_interlocs_id IN (".str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id).") AND 
							rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
					)";
			// echo $rq_delete_interloc_substitution;
			$res_delete_interloc_substitution = $bdd->prepare($rq_delete_interloc_substitution);
			$res_delete_interloc_substitution->execute();	
			$res_delete_interloc_substitution->closeCursor();
			
			//Si des échanges sont concernés par les parcelles supprimées, il faut les éliminées y compris ceux passés avec des représentants
			$rq_delete_rel_af_echanges = "
			WITH t1 AS
			(
				SELECT rel_saf_foncier_interlocs_id
				FROM 
					animation_fonciere.rel_saf_foncier_interlocs
				WHERE 
					rel_saf_foncier_interlocs_id IN (".str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id).") AND rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
			)
			DELETE FROM animation_fonciere.rel_af_echanges
			WHERE 
				rel_saf_foncier_interlocs_id IN (SELECT rel_saf_foncier_interlocs_id FROM t1) OR
				rel_saf_foncier_representant_id IN (SELECT rel_saf_foncier_interlocs_id FROM t1)";
			// echo $rq_delete_rel_af_echanges;
			$res_delete_rel_af_echanges = $bdd->prepare($rq_delete_rel_af_echanges);
			$res_delete_rel_af_echanges->execute();	
			$res_delete_rel_af_echanges->closeCursor();
			
			//On peut maintenant nettoyer la table de relation interlocuteur/parcelle d'une SAF
			$rq_delete_rel_saf_foncier_interlocs = "
			DELETE FROM 
				animation_fonciere.rel_saf_foncier_interlocs 
			WHERE rel_saf_foncier_interlocs_id IN 
				(
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
					WHERE 
						rel_saf_foncier_interlocs_id IN (".str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id).") AND rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
				)";
			// echo $rq_delete_rel_saf_foncier_interlocs;
			$res_delete_rel_saf_foncier_interlocs = $bdd->prepare($rq_delete_rel_saf_foncier_interlocs);
			$res_delete_rel_saf_foncier_interlocs->execute();
			$res_delete_rel_saf_foncier_interlocs->closeCursor();
		}		
		
		$lst_rel_saf_foncier_id = '';
		For ($i=0; $i<count($tbl_rel_saf_foncier_id_assoc) ; $i++) {
			$info_parc_interloc = explode(':', $tbl_rel_saf_foncier_id_assoc[$i]);
			$lst_rel_saf_foncier_id .= $info_parc_interloc[0].',';
			
			// On met à jour les données "simples" relatives au propriétaire
			$rq_update_rel_saf_foncier_interlocs = "		
			UPDATE animation_fonciere.rel_saf_foncier_interlocs 
			SET
				type_interloc_id = $type_interloc,
				ccodro = CASE WHEN '".$info_parc_interloc[2]."' = '-1' THEN NULL ELSE '".$info_parc_interloc[2]."' END,
				ccodem = CASE WHEN '".$info_parc_interloc[1]."' = '-1' THEN NULL ELSE '".$info_parc_interloc[1]."' END
			WHERE
				rel_saf_foncier_interlocs_id IN 
					(
						SELECT
							rel_saf_foncier_interlocs_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND
							entite_af_id = :entite_af_id AND
							interloc_id = :interloc_id AND
							type_interloc_id = :type_interloc_id
					)
				AND rel_saf_foncier_id = ".$info_parc_interloc[0];
			// echo $rq_update_rel_saf_foncier_interlocs;
			$res_update_rel_saf_foncier_interlocs = $bdd->prepare($rq_update_rel_saf_foncier_interlocs);
			$res_update_rel_saf_foncier_interlocs->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'interloc_id'=>$interloc_id, 'type_interloc_id'=>$check_modif['check_type_interloc']));
			$res_update_rel_saf_foncier_interlocs->closeCursor();
		}
		$lst_rel_saf_foncier_id = substr($lst_rel_saf_foncier_id, 0, -1);		
		
		// Dans certains cas, les modifications apportées au propriétaire entrainent une révision des informations relatives aux référents et aux interlocuteurs de substitution. En effet, le propriétaire peut ne plus pouvoir être l'un ou l'autre
		
		//Nettoyage des interlocuteurs de substitution
		$rq_delete_interlocuteur_substitution = "
		WITH t1 AS
		(
			--On récupère les lignes correspondant à des interlocuteurs de substitution dont le type ne permet pas de l'être
			SELECT
				rel_saf_foncier_interlocs_id_is,
				rel_saf_foncier_interlocs_id_vp
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id_is = rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_subst = 'f'

			UNION
			
			--On récupère les lignes correspondant à des interlocuteurs représentés dont le type ne permet pas de l'être
			SELECT
				rel_saf_foncier_interlocs_id_is,
				rel_saf_foncier_interlocs_id_vp
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_under_subst = 'f'
		)
		DELETE FROM animation_fonciere.interloc_substitution
		WHERE 
			rel_saf_foncier_interlocs_id_is IN (SELECT rel_saf_foncier_interlocs_id_is FROM t1) AND
			rel_saf_foncier_interlocs_id_vp IN (SELECT rel_saf_foncier_interlocs_id_vp FROM t1)";
		// echo $rq_delete_interlocuteur_substitution;
		$res_delete_interlocuteur_substitution = $bdd->prepare($rq_delete_interlocuteur_substitution);
		$res_delete_interlocuteur_substitution->execute();
		$res_delete_interlocuteur_substitution->closeCursor();
		
		//Nettoyage des interlocuteurs de substitution
		$rq_delete_interlocuteur_referent = "
		WITH t1 AS
		(
			--On récupère les lignes correspondant à des interlocuteurs de substitution dont le type ne permet pas de l'être
			SELECT
				interloc_referent.rel_saf_foncier_interlocs_id_ref,
				interloc_referent.rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_ref = 'f'

			UNION

			--On récupère les lignes correspondant à des interlocuteurs représentés dont le type ne permet pas de l'être
			SELECT
				interloc_referent.rel_saf_foncier_interlocs_id_ref,
				interloc_referent.rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_under_ref = 'f'
		)
		DELETE FROM animation_fonciere.interloc_referent
		WHERE 
			rel_saf_foncier_interlocs_id_ref IN (SELECT rel_saf_foncier_interlocs_id_ref FROM t1) AND
			rel_saf_foncier_interlocs_id IN (SELECT rel_saf_foncier_interlocs_id FROM t1)";
		// echo $rq_delete_interlocuteur_referent;
		$res_delete_interlocuteur_referent = $bdd->prepare($rq_delete_interlocuteur_referent);
		$res_delete_interlocuteur_referent->execute();
		$res_delete_interlocuteur_referent->closeCursor();
		
		//Le nouveau type permet d'être représenté par quelqu'un et c'est déjà le cas sur un autre type
		$rq_insert_interloc_substitution = "
		WITH t1 AS
		(
			SELECT
				rel_saf_foncier_interlocs_id_vp,
				rel_saf_foncier_interlocs_id_is,
				rel_saf_foncier_interlocs.rel_saf_foncier_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_under_subst = 't'
		)
		INSERT INTO animation_fonciere.interloc_substitution
			(
				SELECT
					rel_saf_foncier_interlocs_id,
					t1.rel_saf_foncier_interlocs_id_is
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN t1 USING (rel_saf_foncier_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					interloc_id = $interloc_id AND
					CASE WHEN 
					(
						SELECT all_eaf 
						FROM 
							animation_fonciere.d_type_interloc
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						WHERE type_interloc_id = $type_interloc
					) = 'f' THEN type_interloc_id = $type_interloc END AND
					can_be_under_subst = 't' AND
					rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id_vp FROM t1)
			)";
		$res_insert_interloc_substitution = $bdd->prepare($rq_insert_interloc_substitution);
		$res_insert_interloc_substitution->execute();
		$res_insert_interloc_substitution->closeCursor();
		
		//Le nouveau type permet d'être référencé par quelqu'un et c'est déjà le cas sur un autre type
		$rq_insert_interloc_referent = "
		WITH t1 AS
		(
			SELECT DISTINCT
				rel_saf_foncier_interlocs_id,
				rel_saf_foncier_interlocs_id_ref,
				rel_saf_foncier_interlocs.rel_saf_foncier_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_under_ref = 't'
		)

		INSERT INTO animation_fonciere.interloc_referent
			(
				SELECT DISTINCT
					t1.rel_saf_foncier_interlocs_id_ref,
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN t1 USING (rel_saf_foncier_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					interloc_id = $interloc_id AND
					can_be_under_ref = 't' AND
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id FROM t1)
			)";
		$res_insert_interloc_referent = $bdd->prepare($rq_insert_interloc_referent);
		$res_insert_interloc_referent->execute();
		$res_insert_interloc_referent->closeCursor();
		
		//Il faut ajouter les nouvelles parcelles associées. Il y en a seulement si à l'origine il s'agissait d'un interlocuteur de substitution
		//Il faudra également s'assurer de la mise à jour des table interlocuteur_referent et interlocuteur_substitution
		//On commence par récupérer la liste des rel_saf_foncier à intégrer
		$rq_lst_new_rel_saf_foncier_id = "
		SELECT DISTINCT	
			rel_saf_foncier_id
		FROM 
			animation_fonciere.rel_saf_foncier_interlocs
		WHERE 
			rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id.")
			AND rel_saf_foncier_id NOT IN 
				(
					SELECT rel_saf_foncier_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
					WHERE 
						rel_saf_foncier_interlocs_id IN (".str_replace('_', ',', $tbl_rel_saf_foncier_interlocs_id).")
				)";
		// echo $rq_lst_new_rel_saf_foncier_id;		
		$res_lst_new_rel_saf_foncier_id = $bdd->prepare($rq_lst_new_rel_saf_foncier_id);
		$res_lst_new_rel_saf_foncier_id->execute();
		$lst_new_rel_saf_foncier_id = $res_lst_new_rel_saf_foncier_id->fetchall();
		For ($i=0; $i<$res_lst_new_rel_saf_foncier_id->rowCount(); $i++) { 
			$rq_new_rel_saf_foncier_interlocs_id = "SELECT nextval('animation_fonciere.rel_saf_foncier_interlocs_id_seq'::regclass)";
			$res_new_rel_saf_foncier_interlocs_id = $bdd->prepare($rq_new_rel_saf_foncier_interlocs_id);
			$res_new_rel_saf_foncier_interlocs_id->execute();
			$new_rel_saf_foncier_interlocs_id_temp = $res_new_rel_saf_foncier_interlocs_id->fetch();
			$new_rel_saf_foncier_interlocs_id = $new_rel_saf_foncier_interlocs_id_temp[0];
			$res_new_rel_saf_foncier_interlocs_id->closeCursor();
		
			$rq_insert_rel_saf_foncier_interlocs = "
			INSERT INTO animation_fonciere.rel_saf_foncier_interlocs (
				rel_saf_foncier_interlocs_id,
				interloc_id,
				rel_saf_foncier_id,
				type_interloc_id)
			VALUES (
				$new_rel_saf_foncier_interlocs_id,
				$interloc_id,
				".$lst_new_rel_saf_foncier_id[$i]['rel_saf_foncier_id'].",
				$type_interloc)";
			// echo $rq_insert_rel_saf_foncier_interlocs;
			$res_insert_rel_saf_foncier_interlocs = $bdd->prepare($rq_insert_rel_saf_foncier_interlocs);
			$res_insert_rel_saf_foncier_interlocs->execute();
			$res_insert_rel_saf_foncier_interlocs->closeCursor();
		}
		For ($i=0; $i<count($tbl_rel_saf_foncier_id_assoc) ; $i++) {
			$info_parc_interloc = explode(':', $tbl_rel_saf_foncier_id_assoc[$i]);
			$lst_rel_saf_foncier_id .= $info_parc_interloc[0].',';
			
			// On met à jour les données "simples" relatives au propriétaire
			$rq_update_rel_saf_foncier_interlocs = "		
			UPDATE animation_fonciere.rel_saf_foncier_interlocs 
			SET
				type_interloc_id = $type_interloc,
				ccodro = CASE WHEN '".$info_parc_interloc[2]."' = '-1' THEN NULL ELSE '".$info_parc_interloc[2]."' END,
				ccodem = CASE WHEN '".$info_parc_interloc[1]."' = '-1' THEN NULL ELSE '".$info_parc_interloc[1]."' END
			WHERE
				rel_saf_foncier_interlocs_id IN 
					(
						SELECT
							rel_saf_foncier_interlocs_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND
							entite_af_id = :entite_af_id AND
							interloc_id = :interloc_id AND
							type_interloc_id = :type_interloc_id
					)
				AND rel_saf_foncier_id = ".$info_parc_interloc[0];
			// echo $rq_update_rel_saf_foncier_interlocs;
			$res_update_rel_saf_foncier_interlocs = $bdd->prepare($rq_update_rel_saf_foncier_interlocs);
			$res_update_rel_saf_foncier_interlocs->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'interloc_id'=>$interloc_id, 'type_interloc_id'=>$type_interloc));
			$res_update_rel_saf_foncier_interlocs->closeCursor();
		}
		
		//Après avoir éventuellement ajouter des parcelles, on vérifie ses relations au sein de l'EAF pour mettre à jour automatiquement les table interloc_referent et interloc_substitution
		//L'interlocuteur est représenté par quelqu'un
		$rq_insert_interloc_substitution = "
		WITH t1 AS
		(
			SELECT
				rel_saf_foncier_interlocs_id_vp,
				rel_saf_foncier_interlocs_id_is,
				rel_saf_foncier_interlocs.rel_saf_foncier_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_under_subst = 't'
		)
		INSERT INTO animation_fonciere.interloc_substitution
			(
				SELECT
					rel_saf_foncier_interlocs_id,
					t1.rel_saf_foncier_interlocs_id_is
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN t1 USING (rel_saf_foncier_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					interloc_id = $interloc_id AND
					can_be_under_subst = 't' AND
					CASE WHEN 
					(
						SELECT all_eaf 
						FROM 
							animation_fonciere.d_type_interloc
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						WHERE type_interloc_id = $type_interloc
					) = 'f' THEN type_interloc_id = $type_interloc END AND
					rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id_vp FROM t1)
			)";
		$res_insert_interloc_substitution = $bdd->prepare($rq_insert_interloc_substitution);
		$res_insert_interloc_substitution->execute();
		$res_insert_interloc_substitution->closeCursor();
		
		//L'interlocuteur représente quelqu'un
		$rq_insert_interloc_substitution = "
		WITH t1 AS
		(
			SELECT DISTINCT
				st1.rel_saf_foncier_interlocs_id,
				st1.rel_saf_foncier_id,
				st1.interloc_id,
				st2.interloc_id_under_subst,
				st2.rel_saf_foncier_interlocs_id_under_subst
			FROM
				(
					SELECT
						rel_saf_foncier_interlocs_id,
						rel_saf_foncier_id,
						interloc_id,
						type_interloc_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND
						can_be_subst = 'f' AND
						can_be_under_subst = 't' AND
						rel_saf_foncier_interlocs_id NOT IN
							(
								SELECT
									rel_saf_foncier_interlocs_id_vp
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id_is = rel_saf_foncier_interlocs_id)
								WHERE
									session_af_id = $session_af_id AND
									entite_af_id = $entite_af_id
							)
				) st1 
			JOIN
				(
					SELECT
						t_subst_temp.interloc_id as interloc_id,
						t_subst_temp.type_interloc_id as type_interloc_id,
						rel_saf_foncier_interlocs.interloc_id as interloc_id_under_subst,
						rel_saf_foncier_interlocs.rel_saf_foncier_id as rel_saf_foncier_id_under_subst,
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id as rel_saf_foncier_interlocs_id_under_subst
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id_is = rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs t_subst_temp ON (t_subst_temp.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_vp)
					WHERE
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND
						rel_saf_foncier_interlocs.interloc_id = $interloc_id AND
						groupe_interloc_id = 3
				) st2 ON (st1.interloc_id = st2.interloc_id AND st1.type_interloc_id = st2.type_interloc_id)
		)
		INSERT INTO animation_fonciere.interloc_substitution
			(
				SELECT DISTINCT
					t1.rel_saf_foncier_interlocs_id,
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN t1 ON (t1.interloc_id_under_subst = rel_saf_foncier_interlocs.interloc_id AND t1.rel_saf_foncier_id = rel_saf_foncier_interlocs.rel_saf_foncier_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					t1.interloc_id_under_subst = $interloc_id AND
					groupe_interloc_id = 3 AND
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id FROM t1)
				ORDER BY t1.rel_saf_foncier_interlocs_id
			)";
		$res_insert_interloc_substitution = $bdd->prepare($rq_insert_interloc_substitution);
		$res_insert_interloc_substitution->execute();
		$res_insert_interloc_substitution->closeCursor();
		
		//L'interlocuteur est référencé par quelqu'un
		$rq_insert_interloc_referent = "
		WITH t1 AS
		(
			SELECT DISTINCT
				rel_saf_foncier_interlocs_id,
				rel_saf_foncier_interlocs_id_ref,
				rel_saf_foncier_interlocs.rel_saf_foncier_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND
				can_be_under_ref = 't'
		)

		INSERT INTO animation_fonciere.interloc_referent
			(
				SELECT DISTINCT
					t1.rel_saf_foncier_interlocs_id_ref,
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN t1 USING (rel_saf_foncier_id)
				WHERE
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id AND
					interloc_id = $interloc_id AND
					can_be_under_ref = 't' AND
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id NOT IN (SELECT rel_saf_foncier_interlocs_id FROM t1)
			)";
		$res_insert_interloc_referent = $bdd->prepare($rq_insert_interloc_referent);
		$res_insert_interloc_referent->execute();
		$res_insert_interloc_referent->closeCursor();
		
		//Des modification ont été apportées en rapport avec les représentants, il faut mettre à jour la table rel_af_echanges
		$rq_update_rel_af_echanges ="
		WITH t1 AS
		(
			SELECT 
				rel_saf_foncier_representant_id
			FROM
				animation_fonciere.rel_af_echanges
				JOIN animation_fonciere.rel_saf_foncier_interlocs ON (rel_af_echanges.rel_saf_foncier_representant_id = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				session_af_id = $session_af_id AND
				entite_af_id = $entite_af_id AND
				interloc_id = $interloc_id AND	
				groupe_interloc_id != 3
		)
		UPDATE animation_fonciere.rel_af_echanges
		SET rel_saf_foncier_representant_id = NULL
		WHERE rel_saf_foncier_representant_id IN (SELECT rel_saf_foncier_representant_id FROM t1)";
		$res_update_rel_af_echanges = $bdd->prepare($rq_update_rel_af_echanges);
		$res_update_rel_af_echanges->execute();
		$res_update_rel_af_echanges->closeCursor();
		
		$result['entite_af_id'] = array($entite_af_id);
		//Il faut maintenant mettre à jour les EAF en fonction des modifications effectuées si on a un propriétaire en plus ou en moins ou si les parcelles d'un véritable propriétaire ont changées
		//On recherchera l'EAF pour les parcelles associées puis pour les parcelles dissociées.
		//En fonction des résultats on pourra soit mettre a jour une EAF existante soit en créer une soit laisser tel quel
		//Il faudra ensuite pour chaque EAF identifiée mettre à jour la page en AJAX
		If (($check_modif['check_type_interloc'] != $type_interloc && ($type_interloc == 1 || $check_modif['check_type_interloc'] == 1)) OR ($check_modif['check_type_interloc'] == $type_interloc && $type_interloc == 1 && $tbl_rel_saf_foncier_id_dissoc != 'N.D.')) {
		
			// 1. Un propriétaire ne l'est plus :
			If ($check_modif['check_type_interloc'] != $type_interloc && $check_modif['check_type_interloc'] == 1) {
				//Il faut rechercher une autre EAF avec la nouvelle liste de propriétaires sur l'ensemble des parcelles associées et dissociées.
				//Soit elle existe et on met à jour l'actuelle avec celle identifiée soit elle n'existe pas et on ne fait rien, elle "s'adapte" toute seule!
				$rq_new_entite_af_id = "
				SELECT
					t2.entite_af_id as new_entite_af_id
				FROM
					(SELECT DISTINCT 
						interloc_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						groupe_interloc_id = 1 AND
						rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id;
						If ($tbl_rel_saf_foncier_id_dissoc != 'N.D.') {
							$rq_new_entite_af_id .= ", ".implode(',', $tbl_rel_saf_foncier_id_dissoc);
						}
					$rq_new_entite_af_id .= "	
					)) t1,
					(SELECT 
						entite_af_id, 
						array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
					FROM
						(SELECT DISTINCT
							entite_af_id,
							interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							groupe_interloc_id = 1 
							AND entite_af_id != $entite_af_id
						) t2
					GROUP BY entite_af_id) t2
				GROUP BY t2.entite_af_id, t2.lst_interloc_id
				HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)";
				$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
				// echo $rq_new_entite_af_id;
				$res_new_entite_af_id->execute();
				$new_entite_af_id_temp = $res_new_entite_af_id->fetchall();
				
				// Si on obtient un résultat, on met à jour l'EAF
				If ($res_new_entite_af_id->rowCount() == 1) {
					$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];			
				} Else If ($res_new_entite_af_id->rowCount() > 1) {
					//Si on entre dans cette boucle, une erreur dans le système a doublonner ou plus une même EAF. Il faut rétablir la situation
					$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];
					$lst_eaf_to_update = array_shift(array_values(array_column($new_entite_af_id_temp, 'new_entite_af_id')));
					$rq_update_bad_eaf = "
					UPDATE
						animation_fonciere.rel_eaf_foncier 
					SET 
						entite_af_id = $new_entite_af_id
					WHERE
						entite_af_id IN (".implode(',', $lst_eaf_to_update).")";
					// echo $rq_update_bad_eaf;
					$res_update_bad_eaf = $bdd->prepare($rq_update_bad_eaf);
					$res_update_bad_eaf->execute();	
					$res_update_bad_eaf->closeCursor();
					
					$rq_reset_prix_eaf = "
					UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
						(
							SELECT 
								rel_eaf_foncier_id
							FROM
								animation_fonciere.rel_eaf_foncier
								JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id OR entite_af_id IN (".implode(',', $lst_eaf_to_update)."))
						)";
					$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
					// echo $rq_reset_prix_eaf.'<br>';
					$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
					$res_reset_prix_eaf->closeCursor();
				} Else {
					$new_entite_af_id = $entite_af_id;
				}
			// 2. Un nouveau propriétaire est identifié : 	
			} Else If ($check_modif['check_type_interloc'] != $type_interloc && $type_interloc == 1) {
				//Soit il est associé à l'ensemble des parcelles de l'EAF en cours. Dans ce cas seule l'EAF en cours est à adapter.
				If ($tbl_rel_saf_foncier_id_dissoc == 'N.D.') {
					//On recherche donc une EAF avec la nouvelle liste de propriétaires. Si on en trouve une on l'utilise pour mettre à jour l'EAF actuelle.
					//Si on en trouve pas, l'EAF "s'adapte" toute seule!
					$rq_new_entite_af_id = "
					SELECT
						t2.entite_af_id as new_entite_af_id
					FROM
						(SELECT DISTINCT 
							interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							groupe_interloc_id = 1 AND
							rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id.")
						) t1,
						(SELECT 
							entite_af_id, 
							array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
						FROM
							(SELECT DISTINCT
								entite_af_id,
								interloc_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE 
								groupe_interloc_id = 1 
								AND entite_af_id != $entite_af_id
							) t2
						GROUP BY entite_af_id) t2
					GROUP BY t2.entite_af_id, t2.lst_interloc_id
					HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)";
					$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
					// echo $rq_new_entite_af_id;
					$res_new_entite_af_id->execute();
					$new_entite_af_id_temp = $res_new_entite_af_id->fetchall();
					
					// Si on obtient un résultat, on met à jour l'EAF
					If ($res_new_entite_af_id->rowCount() == 1) {
						$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];			
					} Else If ($res_new_entite_af_id->rowCount() > 1) {
						//Si on entre dans cette boucle, une erreur dans le système a doublonner ou plus une même EAF. Il faut rétablir la situation
						$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];
						$lst_eaf_to_update = array_shift(array_values(array_column($new_entite_af_id_temp, 'new_entite_af_id')));
						$rq_update_bad_eaf = "
						UPDATE
							animation_fonciere.rel_eaf_foncier 
						SET 
							entite_af_id = $new_entite_af_id
						WHERE
							entite_af_id IN (".implode(',', $lst_eaf_to_update).")";
						// echo $rq_update_bad_eaf;
						$res_update_bad_eaf = $bdd->prepare($rq_update_bad_eaf);
						$res_update_bad_eaf->execute();	
						$res_update_bad_eaf->closeCursor();
						
						$rq_reset_prix_eaf = "
						UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
							(
								SELECT 
									rel_eaf_foncier_id
								FROM
									animation_fonciere.rel_eaf_foncier
									JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
								WHERE
									session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id OR entite_af_id IN (".implode(',', $lst_eaf_to_update)."))
							)";
						$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
						// echo $rq_reset_prix_eaf.'<br>';
						$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
						$res_reset_prix_eaf->closeCursor();
					} Else {
						$new_entite_af_id = $entite_af_id;
					}
				//Soit il est associé à une partie seulement des parcelles.	
				} Else {
					//Dans ce cas il faut identifer si les parcelles concernées sont dans une EAF.
					//Si on en identife une, on l'utilise pour mettre à jour l'EAF sur les parcelles associés.
					//Si on en trouve pas, il faut la créer.
					$rq_new_entite_af_id = "
					SELECT
						t2.entite_af_id as new_entite_af_id
					FROM
						(SELECT DISTINCT 
							interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							groupe_interloc_id = 1 AND
							rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id.")
						) t1,
						(SELECT 
							entite_af_id, 
							array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
						FROM
							(SELECT DISTINCT
								entite_af_id,
								interloc_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE 
								groupe_interloc_id = 1 
								AND entite_af_id != $entite_af_id
							) t2
						GROUP BY entite_af_id) t2
					GROUP BY t2.entite_af_id, t2.lst_interloc_id
					HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)";
					$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
					// echo $rq_new_entite_af_id;
					$res_new_entite_af_id->execute();
					$new_entite_af_id_temp = $res_new_entite_af_id->fetchall();
					
					// Si on obtient un résultat, on met à jour l'EAF sinon il faut en créer une nouvelle
					If ($res_new_entite_af_id->rowCount() == 1) {
						$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];			
					} Else If ($res_new_entite_af_id->rowCount() > 1) {
						//Si on entre dans cette boucle, une erreur dans le système a doublonner ou plus une même EAF. Il faut rétablir la situation
						$new_entite_af_id = $new_entite_af_id_temp[0]['new_entite_af_id'];
						$lst_eaf_to_update = array_shift(array_values(array_column($new_entite_af_id_temp, 'new_entite_af_id')));
						$rq_update_bad_eaf = "
						UPDATE
							animation_fonciere.rel_eaf_foncier 
						SET 
							entite_af_id = $new_entite_af_id
						WHERE
							entite_af_id IN (".implode(',', $lst_eaf_to_update).")";
						// echo $rq_update_bad_eaf;
						$res_update_bad_eaf = $bdd->prepare($rq_update_bad_eaf);
						$res_update_bad_eaf->execute();	
						$res_update_bad_eaf->closeCursor();
						
						$rq_reset_prix_eaf = "
						UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
							(
								SELECT 
									rel_eaf_foncier_id
								FROM
									animation_fonciere.rel_eaf_foncier
									JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
								WHERE
									session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id OR entite_af_id IN (".implode(',', $lst_eaf_to_update)."))
							)";
						$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
						// echo $rq_reset_prix_eaf.'<br>';
						$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
						$res_reset_prix_eaf->closeCursor();
					} Else {
						$rq_new_entite_af_id = "SELECT nextval('animation_fonciere.entite_af_id_seq'::regclass)";
						$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
						// echo $rq_new_entite_af_id;
						$res_new_entite_af_id->execute();
						$new_entite_af_id_temp = $res_new_entite_af_id->fetch();
						$new_entite_af_id = $new_entite_af_id_temp[0];	
						
						//on intègre la nouvelle EAF
						$rq_insert_entite_eaf = "INSERT INTO animation_fonciere.entite_af (entite_af_id) VALUES ($new_entite_af_id)";
						$res_insert_entite_eaf = $bdd->prepare($rq_insert_entite_eaf);
						// echo $rq_insert_entite_eaf;
						$res_insert_entite_eaf->execute();
						$res_insert_entite_eaf->closeCursor();				
					}
					
					//Il faut également mettre à jour l'EAF en cours sur les autres parcelles donc dissociées. On recherche une EAF avec ces parcelles dissociées.
					//Si on en trouve une on l'utilise pour mettre à jour l'EAF en cours.
					//Si on en trouve pas, l'EAF "s'adapte" toute seule!
					$rq_parc_dissoc_entite_af_id = "
					SELECT
						t2.entite_af_id as parc_dissoc_entite_af_id
					FROM
						(SELECT DISTINCT 
							interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							groupe_interloc_id = 1 AND
							rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
						) t1,
						(SELECT 
							entite_af_id, 
							array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
						FROM
							(SELECT DISTINCT
								entite_af_id,
								interloc_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE 
								groupe_interloc_id = 1 
								AND entite_af_id != $entite_af_id
							) t2
						GROUP BY entite_af_id) t2
					GROUP BY t2.entite_af_id, t2.lst_interloc_id
					HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)";
					$res_parc_dissoc_entite_af_id = $bdd->prepare($rq_parc_dissoc_entite_af_id);
					// echo $rq_parc_dissoc_entite_af_id;
					$res_parc_dissoc_entite_af_id->execute();
					$parc_dissoc_entite_af_id_temp = $res_parc_dissoc_entite_af_id->fetchall();
					
					// Si on obtient un résultat, on met à jour l'EAF sinon il faut en créer une nouvelle
					If ($res_parc_dissoc_entite_af_id->rowCount() == 1) {
						$parc_dissoc_entite_af_id = $parc_dissoc_entite_af_id_temp[0]['parc_dissoc_entite_af_id'];			
					} Else If ($res_parc_dissoc_entite_af_id->rowCount() > 1) {
						//Si on entre dans cette boucle, une erreur dans le système a doublonner ou plus une même EAF. Il faut rétablir la situation
						$parc_dissoc_entite_af_id = $parc_dissoc_entite_af_id_temp[0]['parc_dissoc_entite_af_id'];
						$lst_parc_dissoc_eaf_to_update = array_shift(array_values(array_column($parc_dissoc_entite_af_id_temp, 'parc_dissoc_entite_af_id')));
						$rq_update_bad_eaf = "
						UPDATE
							animation_fonciere.rel_eaf_foncier 
						SET 
							entite_af_id = $parc_dissoc_entite_af_id
						WHERE
							entite_af_id IN (".implode(',', $lst_parc_dissoc_eaf_to_update).")";
						// echo $rq_update_bad_eaf;
						$res_update_bad_eaf = $bdd->prepare($rq_update_bad_eaf);
						$res_update_bad_eaf->execute();	
						$res_update_bad_eaf->closeCursor();
						
						$rq_reset_prix_eaf = "
						UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
							(
								SELECT 
									rel_eaf_foncier_id
								FROM
									animation_fonciere.rel_eaf_foncier
									JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
								WHERE
									session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id OR entite_af_id IN (".implode(',', $lst_eaf_to_update)."))
							)";
						$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
						// echo $rq_reset_prix_eaf.'<br>';
						$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$parc_dissoc_entite_af_id));
						$res_reset_prix_eaf->closeCursor();
					} Else {
						$rq_parc_dissoc_entite_af_id = "SELECT nextval('animation_fonciere.entite_af_id_seq'::regclass)";
						$res_parc_dissoc_entite_af_id = $bdd->prepare($rq_parc_dissoc_entite_af_id);
						// echo $rq_parc_dissoc_entite_af_id;
						$res_parc_dissoc_entite_af_id->execute();
						$parc_dissoc_entite_af_id_temp = $res_parc_dissoc_entite_af_id->fetch();
						$parc_dissoc_entite_af_id = $parc_dissoc_entite_af_id_temp[0];	
						
						//on intègre la nouvelle EAF
						$rq_insert_entite_eaf = "INSERT INTO animation_fonciere.entite_af (entite_af_id) VALUES ($parc_dissoc_entite_af_id)";
						$res_insert_entite_eaf = $bdd->prepare($rq_insert_entite_eaf);
						// echo $rq_insert_entite_eaf;
						$res_insert_entite_eaf->execute();
						$res_insert_entite_eaf->closeCursor();
					}
					$result['entite_af_id'][] = $parc_dissoc_entite_af_id;	
					
					//On reset les prix par eaf car la liste de parcelles a changé
					$rq_reset_prix_eaf = "
					UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
						(
							SELECT 
								rel_eaf_foncier_id
							FROM
								animation_fonciere.rel_eaf_foncier
								JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = :session_af_id AND 
								(
									entite_af_id = :entite_af_id OR
									rel_eaf_foncier_id IN 
										(SELECT 
											rel_eaf_foncier_id
										FROM 
											animation_fonciere.rel_saf_foncier
										WHERE rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc)."))
								)
						)";
					$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
					// echo $rq_reset_prix_eaf.'<br>';
					$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$new_entite_af_id));
					$res_reset_prix_eaf->closeCursor();
					
					$rq_update_parc_dissoc_rel_eaf_foncier = "
					UPDATE 
						animation_fonciere.rel_eaf_foncier 
					SET 
						entite_af_id = $parc_dissoc_entite_af_id
					WHERE
						entite_af_id = $entite_af_id AND
						rel_eaf_foncier_id IN (
							SELECT 
								rel_eaf_foncier_id
							FROM 
								animation_fonciere.rel_saf_foncier
							WHERE rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
							)";
					// echo $rq_update_rel_eaf_foncier;
					$res_update_parc_dissoc_rel_eaf_foncier = $bdd->prepare($rq_update_parc_dissoc_rel_eaf_foncier);
					$res_update_parc_dissoc_rel_eaf_foncier->execute();	
					$res_update_parc_dissoc_rel_eaf_foncier->closeCursor();
				}	
			// 3. Les parcelles d'un véritable propriétaire ont changées	
			} Else If ($check_modif['check_type_interloc'] == $type_interloc && $type_interloc == 1 && $tbl_rel_saf_foncier_id_dissoc != 'N.D.') {
				//Il faut mettre à jour l'EAF en cours sur les parcelles dissociées. On recherche une EAF avec ces parcelles dissociées.
				//Si on en trouve une on l'utilise pour mettre à jour l'EAF en cours.
				//Si on en trouve pas il faudra en créer une
				$rq_parc_dissoc_entite_af_id = "
				SELECT
					t2.entite_af_id as parc_dissoc_entite_af_id
				FROM
					(
						SELECT DISTINCT 
							interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							groupe_interloc_id = 1 AND
							rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
					) t1,
					(
						SELECT 
							entite_af_id, 
							array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
						FROM
							(
								SELECT DISTINCT
									entite_af_id,
									interloc_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								WHERE 
									groupe_interloc_id = 1 
									AND entite_af_id != $entite_af_id
							) st2
						GROUP BY entite_af_id
					) t2
				GROUP BY t2.entite_af_id, t2.lst_interloc_id
				HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)";
				$res_parc_dissoc_entite_af_id = $bdd->prepare($rq_parc_dissoc_entite_af_id);
				// echo $rq_parc_dissoc_entite_af_id;
				$res_parc_dissoc_entite_af_id->execute();
				$parc_dissoc_entite_af_id_temp = $res_parc_dissoc_entite_af_id->fetchall();
				
				// Si on obtient un résultat, on met à jour l'EAF sinon il faut en créer une nouvelle
				If ($res_parc_dissoc_entite_af_id->rowCount() == 1) {
					$parc_dissoc_entite_af_id = $parc_dissoc_entite_af_id_temp[0]['parc_dissoc_entite_af_id'];			
				} Else If ($res_parc_dissoc_entite_af_id->rowCount() > 1) {
					//Si on entre dans cette boucle, une erreur dans le système a doublonner ou plus une même EAF. Il faut rétablir la situation
					$parc_dissoc_entite_af_id = $parc_dissoc_entite_af_id_temp[0]['parc_dissoc_entite_af_id'];
					$lst_parc_dissoc_eaf_to_update = array_shift(array_values(array_column($parc_dissoc_entite_af_id_temp, 'parc_dissoc_entite_af_id')));
					$rq_update_bad_eaf = "
					UPDATE
						animation_fonciere.rel_eaf_foncier 
					SET 
						entite_af_id = $parc_dissoc_entite_af_id
					WHERE
						entite_af_id IN (".implode(',', $lst_parc_dissoc_eaf_to_update).")";
					// echo $rq_update_bad_eaf;
					$res_update_bad_eaf = $bdd->prepare($rq_update_bad_eaf);
					$res_update_bad_eaf->execute();	
					$res_update_bad_eaf->closeCursor();
					
					$rq_reset_prix_eaf = "
					UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
						(
							SELECT 
								rel_eaf_foncier_id
							FROM
								animation_fonciere.rel_eaf_foncier
								JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
							WHERE
								session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id OR entite_af_id IN (".implode(',', $lst_eaf_to_update)."))
						)";
					$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
					// echo $rq_reset_prix_eaf.'<br>';
					$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
					$res_reset_prix_eaf->closeCursor();
				} Else {
					$rq_parc_dissoc_new_entite_af_id = "SELECT nextval('animation_fonciere.entite_af_id_seq'::regclass)";
					$res_parc_dissoc_new_entite_af_id = $bdd->prepare($rq_parc_dissoc_new_entite_af_id);
					// echo $rq_parc_dissoc_entite_af_id;
					$res_parc_dissoc_new_entite_af_id->execute();
					$parc_parc_dissoc_new_entite_af_id_temp = $res_parc_dissoc_new_entite_af_id->fetch();
					$parc_dissoc_entite_af_id = $parc_parc_dissoc_new_entite_af_id_temp[0];	
					
					//on intègre la nouvelle EAF
					$rq_insert_entite_eaf = "INSERT INTO animation_fonciere.entite_af (entite_af_id) VALUES ($parc_dissoc_entite_af_id)";
					$res_insert_entite_eaf = $bdd->prepare($rq_insert_entite_eaf);
					// echo $rq_insert_entite_eaf;
					$res_insert_entite_eaf->execute();
					$res_insert_entite_eaf->closeCursor();
				}
				$result['entite_af_id'][] = $parc_dissoc_entite_af_id;
				$new_entite_af_id = $entite_af_id;
				
				$rq_update_parc_dissoc_rel_eaf_foncier = "
				UPDATE 
					animation_fonciere.rel_eaf_foncier 
				SET 
					entite_af_id = $parc_dissoc_entite_af_id
				WHERE
					entite_af_id = $entite_af_id AND
					rel_eaf_foncier_id IN (
						SELECT 
							rel_eaf_foncier_id
						FROM 
							animation_fonciere.rel_saf_foncier
						WHERE rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
						)";
				// echo $rq_update_rel_eaf_foncier;
				$res_update_parc_dissoc_rel_eaf_foncier = $bdd->prepare($rq_update_parc_dissoc_rel_eaf_foncier);
				$res_update_parc_dissoc_rel_eaf_foncier->execute();	
				$res_update_parc_dissoc_rel_eaf_foncier->closeCursor();
				
				$rq_update_parc_dissoc_rel_eaf_foncier = "
				UPDATE 
					animation_fonciere.rel_eaf_foncier 
				SET 
					entite_af_id = $parc_dissoc_entite_af_id
				WHERE
					entite_af_id = $entite_af_id AND
					rel_eaf_foncier_id IN (
						SELECT 
							rel_eaf_foncier_id
						FROM 
							animation_fonciere.rel_saf_foncier
						WHERE rel_saf_foncier_id IN (".implode(',', $tbl_rel_saf_foncier_id_dissoc).")
					)";
				// echo $rq_update_parc_dissoc_rel_eaf_foncier;
				$res_update_parc_dissoc_rel_eaf_foncier = $bdd->prepare($rq_update_parc_dissoc_rel_eaf_foncier);
				$res_update_parc_dissoc_rel_eaf_foncier->execute();	
				$res_update_parc_dissoc_rel_eaf_foncier->closeCursor();
			}
			//On stock l'id de l'EAF pour la requête d'actualisation de la page
			$result['entite_af_id'][] = $new_entite_af_id;
			
			//Si une nouvelle EAF est identifiée, on met à jour l'actuelle et on réinitialise toutes les infos relatives aux référents et aux interlocuteurs de substitution
			If ($new_entite_af_id != $entite_af_id) {
				//On reset les prix par eaf car la liste de parcelles a changé
				$rq_reset_prix_eaf = "
				UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
					(
						SELECT 
							rel_eaf_foncier_id
						FROM
							animation_fonciere.rel_eaf_foncier
							JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = :session_af_id AND (entite_af_id = :entite_af_id OR entite_af_id = :new_entite_af_id)
					)";
				$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
				// echo $rq_reset_prix_eaf.'<br>';
				$res_reset_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'new_entite_af_id'=>$new_entite_af_id));
				$res_reset_prix_eaf->closeCursor();					
				
				$rq_update_rel_eaf_foncier = "
				UPDATE 
					animation_fonciere.rel_eaf_foncier 
				SET 
					entite_af_id = $new_entite_af_id
				WHERE
					entite_af_id = $entite_af_id AND
					rel_eaf_foncier_id IN (
						SELECT 
							rel_eaf_foncier_id
						FROM 
							animation_fonciere.rel_saf_foncier
						WHERE rel_saf_foncier_id IN (".$lst_rel_saf_foncier_id.")
						)";
				// echo $rq_update_rel_eaf_foncier;
				$res_update_rel_eaf_foncier = $bdd->prepare($rq_update_rel_eaf_foncier);
				$res_update_rel_eaf_foncier->execute();	
				$res_update_rel_eaf_foncier->closeCursor();
								
				$rq_delete_ref_interloc = "
				DELETE FROM animation_fonciere.interloc_referent 
				WHERE 
					rel_saf_foncier_interlocs_id_ref IN (
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE 
						session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id) OR
					rel_saf_foncier_interlocs_id IN (
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE 
						session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id))";
				// echo $rq_delete_ref_interloc;
				$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
				$res_delete_ref_interloc->execute();	
				$res_delete_ref_interloc->closeCursor();
				
				$rq_delete_interloc_substitution = "
				DELETE FROM animation_fonciere.interloc_substitution 
				WHERE 
					rel_saf_foncier_interlocs_id_vp IN (
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE 
						session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id)) OR
					rel_saf_foncier_interlocs_id_is IN (
					SELECT rel_saf_foncier_interlocs_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE 
						session_af_id = $session_af_id AND (entite_af_id = $entite_af_id OR entite_af_id = $new_entite_af_id))";
				// echo $rq_delete_interloc_substitution;
				$res_delete_interloc_substitution = $bdd->prepare($rq_delete_interloc_substitution);
				$res_delete_interloc_substitution->execute();	
				$res_delete_interloc_substitution->closeCursor();			
			}
		} 
			
		//on simplifie le tableau des entite_af_id pour eliminer les doublons lorsque l'EAF identifiée est l'EAF d'origine
		$result['entite_af_id'] = array_values(array_unique(array_filter($result['entite_af_id'])));
		
		//On peut supprimer les eaf qui ne sont plus utilisés dans la table entite_af
		$rq_delete_entite_af = "DELETE FROM animation_fonciere.entite_af WHERE entite_af_id NOT IN (SELECT entite_af_id FROM animation_fonciere.rel_eaf_foncier)";
		$res_delete_entite_af = $bdd->prepare($rq_delete_entite_af);
		// echo $rq_delete_entite_af;
		$res_delete_entite_af->execute();
		$res_delete_entite_af->closeCursor();	
			
		// Pour toutes les EAF modifiées ou crées il faut mettre à jour la page avec AJAX.	
		For ($e=0; $e<count($result['entite_af_id']); $e++) {
			$result['entite_af_code'][$e] = 'N.D.';			
			$rq_infos_eaf = "
			SELECT DISTINCT 
				array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
				array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
				array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
				t1.nbr_echanges
			FROM 		
				animation_fonciere.rel_eaf_foncier
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.cadastre_site t4 USING (cad_site_id)
				JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
				JOIN 
					(SELECT DISTINCT 
						entite_af_id,
						count(echange_af_id) as nbr_echanges
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
					GROUP BY entite_af_id
					) t1 USING (entite_af_id)
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND 
				entite_af_id= :entite_af_id AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'
			GROUP BY 
				entite_af_id,
				t1.nbr_echanges";											
			$res_infos_eaf = $bdd->prepare($rq_infos_eaf);
			// echo $rq_infos_eaf;
			$res_infos_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$result['entite_af_id'][$e]));
			$infos_eaf = $res_infos_eaf->fetch();
			
			If ($infos_eaf['tbl_eaf_lot_id'] != '') {
				$result['entite_af_code'][$e] = "
				<td style='border-right: 1px solid #ccc; border-bottom: 4px solid #ccc;'>
					<table id='interloc_tbl_lots_".$result['entite_af_id'][$e]."' CELLSPACING = '1' CELLPADDING ='0' class='no-border'>";
				
				$rq_info_lots = "
				SELECT DISTINCT
					t4.cad_site_id,
					t2.lot_id,
					t2.dcntlo,
					COALESCE(rel_eaf_foncier.utilissol, 'N.D.') AS utilissol,
					rel_saf_foncier.paf_id,
					d_paf.paf_lib
				FROM
					cadastre.lots_cen t2
					JOIN cadastre.cadastre_cen t3 USING (lot_id)
					JOIN foncier.cadastre_site t4 USING (cad_cen_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.d_paf USING (paf_id)
				WHERE 
					entite_af_id = :entite_af_id AND 
					rel_saf_foncier.session_af_id = :session_af_id AND
					t3.date_fin = 'N.D.' AND
					t4.date_fin = 'N.D.'
				ORDER BY t2.lot_id";
				$res_info_lots = $bdd->prepare($rq_info_lots);
				$res_info_lots->execute(array('entite_af_id'=>$result['entite_af_id'][$e], 'session_af_id'=>$session_af_id));
				$info_lots = $res_info_lots->fetchall();
				For ($l=0; $l<$res_info_lots->rowCount(); $l++) { 
					$result['entite_af_code'][$e] .= "
						<tr>
							<td class='no-border' style='margin-top:5px;'>
								<label class='label'>
									<u>Lot ID :</u> ".$info_lots[$l]['lot_id']."
								</label>
							</td>
							<td>
								<img id='interloc_btn_detail-".$info_lots[$l]['lot_id']."' src='".ROOT_PATH."images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick=\"showDetail('interloc', '".$info_lots[$l]['lot_id']."');\">															
							</td>
						</tr>
						<tr>
							<td id='interloc_td_detail-".$info_lots[$l]['lot_id']."' style='display:none' colspan='2'>
								<table class='no-border'>
									<tr>
										<td class='no-border'>
											<label class='label'>
												<u>Contenance :</u> ".conv_are($info_lots[$l]['dcntlo'])."
											</label>
										</td>
									</tr>
									<tr>
										<td class='no-border'>
											<label class='label'>
												<u>Utilisation du sol :</u> 
											</label>
									</tr>
									<tr>
										<td style='padding-left:10px;'>
											<label class='label'>
												".retour_ligne($info_lots[$l]['utilissol'], 40)."
											</label>
										</td>
									</tr>
									<tr>
										<td class='no-border'>";
												$rq_nat_cult = 
												"SELECT DISTINCT
													d_dsgrpf.natcult_ssgrp,
													d_dsgrpf.cgrnum,
													lots_natcult_cen.dcntsf as surf_nat_cult,
													CASE WHEN d_cnatsp.natcult_natspe IS NOT NULL THEN d_cnatsp.natcult_natspe ELSE 'N.D.' END as natcult_natspe,
													d_dsgrpf.dsgrpf
												FROM
													cadastre.lots_cen t2	
													JOIN cadastre.lots_natcult_cen USING (lot_id)
													LEFT JOIN cadastre.d_cnatsp USING (cnatsp)
													JOIN cadastre.d_dsgrpf USING (dsgrpf)
												WHERE t2.lot_id = ?
												ORDER BY surf_nat_cult DESC";
												$res_nat_cult = $bdd->prepare($rq_nat_cult);
												$res_nat_cult->execute(array($info_lots[$l]['lot_id']));					
												$nat_cult = $res_nat_cult->fetchall();
												
											$result['entite_af_code'][$e] .= "
											<table style='border:0 solid black'>
												<tr>
													<td style='vertical-align:top'>
														<label class='label' style='margin-left:-3px'><u>Natures de culture : </u></label> 
													</td>
												</tr>
												<tr>
													<td>
														<table CELLSPACING = '0' CELLPADDING ='0' style='border:0 solid black;margin-left:5px;'>";
													If (count($nat_cult) == 0) {
														$result['entite_af_code'][$e] .= "<td><label class='label'>N.D.</label></td>";
													} Else {
														For ($n=0; $n<count($nat_cult); $n++) { 
															$result['entite_af_code'][$e] .= "
															<tr>
																<td style='text-align:left'>
																	<label class='label'>
																		".retour_ligne($nat_cult[$n]['natcult_ssgrp'] . ' : ' . conv_are($nat_cult[$n]['surf_nat_cult']) . ' (' . round(($nat_cult[$n]['surf_nat_cult']*100)/$info_lots[$l]['dcntlo'], 0) . '%)', 40)."
																	</label>
																</td>
															</tr>";
															If ($nat_cult[$n]['natcult_natspe'] != 'N.D.') { 
															$result['entite_af_code'][$e] .= "
															<tr>
																<td style='text-align:left;'>
																	<label class='label' style='font-size:9pt;font-weight:normal;'>
																		".'(' . $nat_cult[$n]['natcult_natspe'] . ')'."
																	</label>
																</td>
															</tr>";
															} 
														}
													} 
														$result['entite_af_code'][$e] .= "
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class='no-border'>
											<label class='label'>
												<u>Priorit&eacute; d'animation :</u> ".($info_lots[$l]['paf_id'])."
											</label>
										</td>
									</tr>
								</table>
							</td>
						</tr>";		
					}
					$result['entite_af_code'][$e] .= "
					</table>
				</td>
				<td style='border-bottom: 4px solid #ccc'>
					<center>
						<table id='tbl_".$result['entite_af_id'][$e]."' style='float:none;' CELLSPACING = '5' CELLPADDING ='5' class='no-border'>
							<tr>															
								<td>
									<table cellspacing='0' cellpadding='8' class='no-border'>";
										$result['entite_af_code'][$e] .= fct_rq_lst_interlocs($session_af_id, $result['entite_af_id'][$e], $infos_eaf['tbl_eaf_lot_id'], $infos_eaf['tbl_rel_saf_foncier_id']);
										$result['entite_af_code'][$e] .= "
									</table>
								</td>
							</tr>
						</table>
					</center>
				</td>";										
			} Else {
				$result['entite_af_code'][$e] = "hide";
			}
		}
		$bdd = null;
		
		print json_encode($result);
	}
	
	function fct_createTblMaitriseParc($data) {
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		$interloc_id = $data['params']['var_interloc_id'];
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		$representant = $data['params']['var_representant'];
		$chk_echange_referent = $data['params']['var_chk_echange_referent'];
		If ($chk_echange_referent == 'true') {
			$display_tbl_parc_maitrise_other_eaf = '';
		} Else {
			$display_tbl_parc_maitrise_other_eaf = 'none';
		}
		
		$result['code'] = fct_echange_tbl_maitrise_parc($session_af_id, $entite_af_id, $interloc_id, 'N.D.', $display_tbl_parc_maitrise_other_eaf, $representant);
		
		$rq_prix_eaf_negocie = "
		SELECT DISTINCT
			prix_eaf_negocie
		FROM
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
		WHERE
			entite_af_id = :entite_af_id";
		$res_prix_eaf_negocie = $bdd->prepare($rq_prix_eaf_negocie);
		// echo $rq_prix_eaf_negocie;
		$res_prix_eaf_negocie->execute(array('entite_af_id'=>$entite_af_id));
		If ($res_prix_eaf_negocie->rowCount() > 1) {
			$rq_delete = "
			DELETE FROM animation_fonciere.rel_saf_foncier WHERE rel_eaf_foncier_id IN 
				(
					SELECT 
						rel_eaf_foncier_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						entite_af_id = :entite_af_id
				)";
			$res_delete = $bdd->prepare($rq_delete);
			// echo $rq_delete.'<br>';
			$res_delete->execute(array('entite_af_id'=>$entite_af_id));
			$res_delete->closeCursor();
			$result['prix_eaf_negocie'] = '';
		} Else {
			$prix_eaf_negocie = $res_prix_eaf_negocie->fetch();
			$result['prix_eaf_negocie'] = $prix_eaf_negocie['prix_eaf_negocie'];
		}
				
		print json_encode($result);
	}
	
	function fct_majRefInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres		
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		$interloc_id = $data['params']['var_interloc_id'];
		$ref_interloc_id = $data['params']['var_ref_interloc_id'];
		
		If ($interloc_id != 'ref') {
			$rq_delete = "
			DELETE FROM animation_fonciere.interloc_referent WHERE rel_saf_foncier_interlocs_id IN 
				(
					SELECT 
						rel_saf_foncier_interlocs_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						interloc_id = ".$interloc_id." AND
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND
						can_be_ref = 't'
						
					UNION
					
					SELECT DISTINCT
						rel_saf_foncier_interlocs_id_vp
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
					WHERE
						interloc_id = ".$interloc_id." AND
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id
				)";
			$res_delete = $bdd->prepare($rq_delete);
			// echo $rq_delete.'<br>';
			$res_delete->execute();
			$res_delete->closeCursor();
		}		
		
		
		If ($ref_interloc_id != 'ref') {
			$rq_delete = "
			DELETE FROM animation_fonciere.interloc_referent
			WHERE 
				rel_saf_foncier_interlocs_id_ref IN 
					(
						--on supprime les id correspondant à des référents dont le type ne le permet pas
						SELECT 
							rel_saf_foncier_interlocs_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = $session_af_id AND
							entite_af_id = $entite_af_id AND
							can_be_ref = 'f'
							
						UNION
						
						--on supprime les id correspondant à des référents représentés
						SELECT DISTINCT
							rel_saf_foncier_interlocs_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = $session_af_id AND
							entite_af_id = $entite_af_id AND
							interloc_id IN 
								(
									SELECT DISTINCT
										interloc_id
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
									WHERE
										session_af_id = $session_af_id AND
										entite_af_id = $entite_af_id AND
										interloc_id = ".$ref_interloc_id."
								)
					) OR
				rel_saf_foncier_interlocs_id IN 
					(
						--on supprime les id correspondant à des référencés dont le type ne le permet pas
						SELECT 
							rel_saf_foncier_interlocs_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = $session_af_id AND
							entite_af_id = $entite_af_id AND
							can_be_under_ref = 'f'
					)
				";
			$res_delete = $bdd->prepare($rq_delete);
			// echo $rq_delete.'<br>';
			$res_delete->execute();
			$res_delete->closeCursor();
		
			$rq_insert = "
			INSERT INTO animation_fonciere.interloc_referent 
				(
					SELECT DISTINCT
						t2.rel_saf_foncier_interlocs_id as rel_saf_foncier_interlocs_id,
						t1.rel_saf_foncier_interlocs_id as foncier_interloc
					FROM
						(
							SELECT DISTINCT
								interloc_id,
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								interloc_id = ".$interloc_id." AND
								session_af_id = $session_af_id AND
								entite_af_id = $entite_af_id AND
								can_be_under_ref = 't'

							UNION

							SELECT DISTINCT
								t_temp_subst.interloc_id,
								rel_saf_foncier_interlocs_id_vp,
								t_temp_subst.rel_saf_foncier_id			
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc ON (d_type_interloc.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
								JOIN animation_fonciere.d_groupe_interloc ON (d_groupe_interloc.groupe_interloc_id = d_type_interloc.groupe_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
								JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
								JOIN animation_fonciere.d_type_interloc t_temp_subst_type ON (t_temp_subst_type.type_interloc_id = t_temp_subst.type_interloc_id)
								JOIN animation_fonciere.d_groupe_interloc t_temp_subst_groupe ON (t_temp_subst_groupe.groupe_interloc_id = t_temp_subst_type.groupe_interloc_id)
								
							WHERE 
								session_af_id = $session_af_id AND 
								entite_af_id = $entite_af_id AND
								rel_saf_foncier_interlocs.interloc_id = ".$interloc_id." AND
								d_groupe_interloc.can_be_under_ref = 't'
						) t1
						JOIN 
						(
							SELECT 
								interloc_id,
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								interloc_id = ".$ref_interloc_id." AND
								session_af_id = $session_af_id AND
								entite_af_id = $entite_af_id AND
								can_be_ref = 't'
						) t2 ON (t1.rel_saf_foncier_id = t2.rel_saf_foncier_id AND t1.interloc_id != t2.interloc_id)
				)
			";
			$res_insert = $bdd->prepare($rq_insert);
			// echo $rq_insert;
			$res_insert->execute();
			$res_insert->closeCursor();
		}
		
		$rq_infos_eaf = "
		SELECT DISTINCT 
			array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
			array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
			array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
			t1.nbr_echanges
		FROM 		
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN 
				(SELECT DISTINCT 
					entite_af_id,
					count(echange_af_id) as nbr_echanges
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
				GROUP BY entite_af_id
				) t1 USING (entite_af_id)										
			
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			entite_af_id= :entite_af_id AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'
		GROUP BY 
			entite_af_id,
			t1.nbr_echanges";											
		$res_infos_eaf = $bdd->prepare($rq_infos_eaf);
		// echo $rq_infos_eaf;
		$res_infos_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$infos_eaf = $res_infos_eaf->fetch();
		
		$result['new_eaf_code'] = fct_rq_lst_interlocs($session_af_id, $entite_af_id, $infos_eaf['tbl_eaf_lot_id'], $infos_eaf['tbl_rel_saf_foncier_id']);
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_majSubstInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		$subst_interloc = explode('_', $data['params']['var_subst_interloc']);
		
		$rq_delete = "
		DELETE FROM animation_fonciere.interloc_substitution 
		WHERE 1=1 AND ";
		If ($data['params']['var_interloc'] != 'subst') {
			$interloc = explode('_', $data['params']['var_interloc']);
			$rq_delete .= "
			rel_saf_foncier_interlocs_id_vp IN (
				SELECT rel_saf_foncier_interlocs_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				WHERE
					interloc_id = ".$interloc[0]." AND
					type_interloc_id = ".$interloc[1]." AND
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id
				)
				AND ";
		}
		If ($data['params']['var_interloc_origin'] != 'subst') {
			$interloc_origin = explode('_', $data['params']['var_interloc_origin']);
			$rq_delete .= "
			rel_saf_foncier_interlocs_id_vp IN (
				SELECT 
					rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = interloc_substitution.rel_saf_foncier_interlocs_id_is)
					JOIN animation_fonciere.d_type_interloc ON (d_type_interloc.type_interloc_id = t_temp_subst.type_interloc_id)
					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				WHERE
					rel_saf_foncier_interlocs.interloc_id = ".$interloc_origin[0]." AND
					t_temp_subst.type_interloc_id = ".$subst_interloc[1]." AND
					can_be_subst = 't' AND
					can_be_under_ref = 't' AND
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id
					
				UNION
				
				SELECT rel_saf_foncier_interlocs_id
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
					JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				WHERE
					rel_saf_foncier_interlocs.interloc_id = ".$interloc_origin[0]." AND
					type_interloc_id = ".$interloc_origin[1]." AND
					session_af_id = $session_af_id AND
					entite_af_id = $entite_af_id
				)
				AND ";
		}
		$rq_delete .= "
		rel_saf_foncier_interlocs_id_is IN (
			SELECT rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				interloc_id = ".$subst_interloc[0]." AND
				type_interloc_id = ".$subst_interloc[1]." AND
				session_af_id = $session_af_id AND
				can_be_subst = 't' AND
				entite_af_id = $entite_af_id
			)";
		
		$res_delete = $bdd->prepare($rq_delete);
		// echo $rq_delete.'<br>';
		$res_delete->execute();
		$res_delete->closeCursor();	
		
		If ($data['params']['var_subst_interloc'] != 'subst' && $data['params']['var_interloc'] != 'subst') {
			$rq_insert = "
			INSERT INTO animation_fonciere.interloc_substitution 
				(
					SELECT DISTINCT
						t1.rel_saf_foncier_interlocs_id as rel_saf_foncier_interlocs_id,
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id as foncier_interloc
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN (
							SELECT 
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								interloc_id = ".$interloc[0]." AND
								type_interloc_id = ".$interloc[1]." AND
								session_af_id = $session_af_id AND
								entite_af_id = $entite_af_id
						) t1 ON (t1.rel_saf_foncier_id = rel_saf_foncier_interlocs.rel_saf_foncier_id)
					WHERE
						interloc_id = ".$subst_interloc[0]." AND
						type_interloc_id = ".$subst_interloc[1]." AND
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND 
						can_be_subst = 't'
						
					UNION
					
					SELECT DISTINCT
						t1.rel_saf_foncier_interlocs_id as rel_saf_foncier_interlocs_id,
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id as foncier_interloc
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN (
							SELECT 
								rel_saf_foncier_interlocs_id,
								rel_saf_foncier_id
							FROM
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
								JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							WHERE
								interloc_id = ".$interloc[0]." AND
								session_af_id = $session_af_id AND
								entite_af_id = $entite_af_id AND
								can_be_under_subst = 't'
						) t1 ON (t1.rel_saf_foncier_id = rel_saf_foncier_interlocs.rel_saf_foncier_id)
					WHERE
						interloc_id = ".$subst_interloc[0]." AND
						session_af_id = $session_af_id AND
						entite_af_id = $entite_af_id AND
						type_interloc_id = ".$subst_interloc[1]." AND
						can_be_subst = 't' AND
						can_be_under_ref = 't'					
				)";
			$res_insert = $bdd->prepare($rq_insert);
			// echo $rq_insert;
			$res_insert->execute();
			$res_insert->closeCursor();
			
			//Il faut mettre à jour la table interlocuteur_referent si l'interlocuteur représenté était référent, il ne peut plus l'être
			$rq_delete_interlocuteur_referent = "	
			DELETE FROM animation_fonciere.interloc_referent
			WHERE 
				rel_saf_foncier_interlocs_id_ref IN 
					(
						SELECT 
							rel_saf_foncier_interlocs_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = $session_af_id AND
							entite_af_id = $entite_af_id AND
							interloc_id = ".$interloc[0]."
					)
			";
			$res_delete_interlocuteur_referent = $bdd->prepare($rq_delete_interlocuteur_referent);
			// echo $rq_delete_interlocuteur_referent;
			$res_delete_interlocuteur_referent->execute();
			$res_delete_interlocuteur_referent->closeCursor();
			
			//Il faut mettre à jour la table interlocuteur_substitution si le représentant était représenté il ne peut plus l'être
			$rq_delete_interlocuteur_substitution = "	
			DELETE FROM animation_fonciere.interloc_substitution
			WHERE 
				rel_saf_foncier_interlocs_id_vp IN 
					(
						SELECT 
							rel_saf_foncier_interlocs_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE
							session_af_id = $session_af_id AND
							entite_af_id = $entite_af_id AND
							interloc_id = ".$subst_interloc[0]."
					)
			";
			$res_delete_interlocuteur_substitution = $bdd->prepare($rq_delete_interlocuteur_substitution);
			// echo $rq_delete_interlocuteur_referent;
			$res_delete_interlocuteur_substitution->execute();
			$res_delete_interlocuteur_substitution->closeCursor();
		}		
		
		$rq_infos_eaf = "
		SELECT DISTINCT 
			array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
			array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
			array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
			t1.nbr_echanges
		FROM 		
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN 
				(SELECT DISTINCT 
					entite_af_id,
					count(echange_af_id) as nbr_echanges
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
				GROUP BY entite_af_id
				) t1 USING (entite_af_id)										
			
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			entite_af_id= :entite_af_id AND
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'
		GROUP BY 
			entite_af_id,
			t1.nbr_echanges";											
		$res_infos_eaf = $bdd->prepare($rq_infos_eaf);
		// echo $rq_infos_eaf;
		$res_infos_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$infos_eaf = $res_infos_eaf->fetch();
		
		$result['new_eaf_code'] = fct_rq_lst_interlocs($session_af_id, $entite_af_id, $infos_eaf['tbl_eaf_lot_id'], $infos_eaf['tbl_rel_saf_foncier_id']);
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_showFormGestref($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		
		$result['code'] = "
		<tr>
			<td style='text-align:center;'>
				<center>
					<div id='dropper_ref' class='dropper' style='cursor:default;float:unset'>	
						<div class='drop_title'>Référent</div>";
						$rq_gest_interloc = "
						SELECT
							t1.interloc_id,
							t1.dnuper,
							t1.particule_id,
							t1.particule_lib,
							t1.ddenom,
							t1.nom_usage,
							t1.prenom_usage,
							array_to_string(array_agg(DISTINCT t1.type_interloc_id), ',') as tbl_type_interloc_id,
							array_to_string(array_agg(DISTINCT t1.type_interloc_lib), '<br>') as lst_type_interloc_lib,	
							array_to_string(array_agg(DISTINCT t1.can_be_ref), ',') as tbl_can_be_ref,
							array_to_string(array_agg(DISTINCT t1.can_be_under_ref), ',') as tbl_can_be_under_ref,
							array_to_string(array_agg(DISTINCT t1.can_be_subst), ',') as tbl_can_be_subst,
							array_to_string(array_agg(DISTINCT CASE WHEN t1.tbl_interloc_ref IS NOT NULL THEN t1.tbl_interloc_ref END), ',') as tbl_interloc_ref
						FROM 
							(
								SELECT DISTINCT
									interlocuteurs.interloc_id,
									interlocuteurs.dnuper,
									d_ccoqua.ccoqua as particule_id,
									d_ccoqua.particule_lib,
									interlocuteurs.ddenom,
									interlocuteurs.nom_usage,
									CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
										CASE WHEN strpos(interlocuteurs.prenom_usage, ' ') > 0 THEN
											substring(interlocuteurs.prenom_usage from 0 for  strpos(interlocuteurs.prenom_usage, ' ')) 
										ELSE 
											interlocuteurs.prenom_usage
										END
									ELSE	
										'N.P.' 
									END as prenom_usage,
									ref2.tbl_interloc_ref as tbl_interloc_ref,
									CASE WHEN array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY rel_saf_foncier_id), ',') = (
										SELECT 
											array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY rel_saf_foncier_id), ',') 
										FROM  
											animation_fonciere.rel_saf_foncier
											JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										WHERE
											session_af_id = :session_af_id AND 
											entite_af_id = :entite_af_id)
									AND count(rel_saf_foncier_interlocs_id_vp) = 0 THEN
										'can_be_ref'
									ELSE
										'cant_be_ref'
									END as check_ref,						
									d_type_interloc.type_interloc_lib,
									rel_saf_foncier_interlocs.type_interloc_id,
									d_type_interloc.groupe_interloc_id,
									CASE WHEN count(interloc_substitution.rel_saf_foncier_interlocs_id_vp) > 0 THEN 'never' ELSE d_groupe_interloc.can_be_ref::text END AS can_be_ref,
									d_groupe_interloc.can_be_under_ref::text,
									d_groupe_interloc.can_be_subst::text
								FROM 
									animation_fonciere.interlocuteurs
									LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
									JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									LEFT JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
									LEFT JOIN 
										(
											SELECT DISTINCT 
												t1.interloc_id as tbl_interloc_ref,
												interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_id
											FROM 
												(
													SELECT
														rel_saf_foncier_interlocs_id,
														interloc_id,
														type_interloc_id
													FROM
														animation_fonciere.rel_saf_foncier_interlocs
														JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
														JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													WHERE
														session_af_id = :session_af_id AND
														entite_af_id = :entite_af_id AND
														rel_saf_foncier_interlocs_id NOT IN 
															(
																SELECT DISTINCT
																	rel_saf_foncier_interlocs_id_vp
																FROM
																	animation_fonciere.rel_saf_foncier_interlocs
																	JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
																	JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id) 
																	JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
																	JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
																	JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
																WHERE
																	session_af_id = :session_af_id AND
																	entite_af_id = :entite_af_id AND
																	can_be_subst = 't' AND
																	can_be_under_ref = 't'															
															)													
											
													UNION
													
													SELECT DISTINCT
														rel_saf_foncier_interlocs_id_vp,
														interloc_id,
														type_interloc_id
													FROM
														animation_fonciere.rel_saf_foncier_interlocs
														JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
														JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
														JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
														JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
														JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
													WHERE
														session_af_id = :session_af_id AND
														entite_af_id = :entite_af_id AND
														can_be_subst = 't' AND
														can_be_under_ref = 't'
												) t1
												JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
										) ref2 ON (ref2.foncier_interloc_id = rel_saf_foncier_interlocs_id)
								WHERE 
									session_af_id = :session_af_id AND 
									entite_af_id = :entite_af_id AND
									(can_be_ref = 't' OR can_be_under_ref = 't') AND 
									interloc_id NOT IN 
										(
											SELECT DISTINCT 
												rel_saf_foncier_interlocs.interloc_id
											FROM 
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN 
													(
														SELECT
															rel_saf_foncier_interlocs_id_vp,
															interloc_id
														FROM
															animation_fonciere.rel_saf_foncier_interlocs
															JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
															JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
															JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
														WHERE
															can_be_subst = 't' AND
															can_be_under_ref = 't'
													) t1 ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
											WHERE 
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id
												
											UNION

											SELECT DISTINCT
												t1.interloc_id as interloc_ref_vp
											FROM
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN 
													(
														SELECT
															interloc_id,
															rel_saf_foncier_interlocs_id,
															rel_saf_foncier_interlocs_id_vp,
															rel_saf_foncier_interlocs_id_is
														FROM
															animation_fonciere.rel_saf_foncier_interlocs
															JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
															JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
															JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
														WHERE
															can_be_subst = 't' AND
															can_be_under_ref = 't'									
													) t1 ON (t1.rel_saf_foncier_interlocs_id_is = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
											WHERE 
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id AND 
												rel_saf_foncier_interlocs.interloc_id IN 
													(
														SELECT DISTINCT
															interloc_id
														FROM
															animation_fonciere.rel_saf_foncier_interlocs
															JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
															JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
															JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
															JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
														WHERE 
															session_af_id = :session_af_id AND 
															entite_af_id = :entite_af_id AND 
															groupe_interloc_id = 1
													)	
										)
									AND rel_saf_foncier_interlocs_id NOT IN
										(
											SELECT DISTINCT 
												rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id
											FROM 
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
												JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)

											WHERE
												can_be_subst = 't' AND
												rel_saf_foncier_interlocs_id NOT IN
													(
														SELECT rel_saf_foncier_interlocs_id_is
														FROM animation_fonciere.interloc_substitution
													)
										)
									AND etat_interloc_id = 1
								GROUP BY 
									interlocuteurs.interloc_id, 
									d_ccoqua.ccoqua, 
									type_interloc_lib, 
									type_interloc_id, 
									groupe_interloc_id, 
									can_be_ref,
									can_be_under_ref,
									can_be_subst,
									tbl_interloc_ref
								HAVING (count(rel_saf_foncier_id) = 
									(
										SELECT count(rel_saf_foncier_id) 
										FROM 
											animation_fonciere.rel_saf_foncier
											JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										WHERE 
											session_af_id = :session_af_id AND 
											entite_af_id = :entite_af_id
									)
								)	
								ORDER BY tbl_interloc_ref DESC
							) t1
						GROUP BY
							t1.interloc_id,
							t1.dnuper,
							t1.particule_id,
							t1.particule_lib,
							t1.ddenom,
							t1.nom_usage,
							t1.prenom_usage
						";
						$res_gest_interloc = $bdd->prepare($rq_gest_interloc);
						// echo $rq_gest_interloc;
						$res_gest_interloc->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
						$gest_interlocs = $res_gest_interloc->fetchall();	
						
						$tbl_interloc_not_ref = '';
						For ($j=0; $j<$res_gest_interloc->rowCount(); $j++) { 
							$tbl_interloc_not_ref .= $gest_interlocs[$j]['tbl_interloc_ref'] . ',';
						}
						$tbl_interloc_not_ref = preg_split("/,/", $tbl_interloc_not_ref);
						$tbl_interloc_not_ref = array_filter($tbl_interloc_not_ref);
						
						For ($j=0; $j<$res_gest_interloc->rowCount(); $j++) { 
							$tbl_interloc_ref = preg_split("/,/", $gest_interlocs[$j]['tbl_interloc_ref']);
							
							$interloc_lib = str_replace('   ', ' ', proprio_lib($gest_interlocs[$j]['particule_lib'], $gest_interlocs[$j]['nom_usage'], $gest_interlocs[$j]['prenom_usage'], $gest_interlocs[$j]['ddenom']));
							
							If ($tbl_interloc_ref[0] == Null && !in_array($gest_interlocs[$j]['interloc_id'], $tbl_interloc_not_ref)) {
								$result['code'] .= "
								<div id='div_".$gest_interlocs[$j]['interloc_id']."' ";
								If (substr_count($gest_interlocs[$j]['tbl_can_be_under_ref'], 'true') == 0) {
									$result['code'] .= "class='not_draggable never' draggable='false'>";
								} Else {
									$result['code'] .= "class='draggable' draggable='true'>";
								}
									$result['code'] .= "
									<input id='ref_".$gest_interlocs[$j]['interloc_id']."' type='checkbox' class='checkRef' style='float:left;'";
									If (substr_count($gest_interlocs[$j]['tbl_can_be_ref'], 'true') == 0 || substr_count($gest_interlocs[$j]['tbl_can_be_ref'], 'never') > 0) {
										$result['code'] .= "readonly disabled";
									}
									$result['code'] .= "
									>
										$interloc_lib
										<br>
										<label class='last_maj'>".$gest_interlocs[$j]['lst_type_interloc_lib']."</label>
								</div>";
							} Else If (!in_array($gest_interlocs[$j]['interloc_id'], $tbl_interloc_not_ref) && ((count(array_keys(array_column($gest_interlocs, 'interloc_id'), $gest_interlocs[$j]['interloc_id'])) > 1 && $gest_interlocs[$j]['can_be_subst'] == 'false') || count(array_keys(array_column($gest_interlocs, 'interloc_id'), $gest_interlocs[$j]['interloc_id'])) == 1)) {
								$result['code'] .= "
								<div id='div_".$gest_interlocs[$j]['interloc_id']."' class='not_draggable' draggable='false'>
									<input id='ref_".$gest_interlocs[$j]['interloc_id']."' type='checkbox' checked class='checkRef' style='float:left;'";
									If (substr_count($gest_interlocs[$j]['tbl_can_be_ref'], 'true') == 0 || substr_count($gest_interlocs[$j]['tbl_can_be_ref'], 'never') > 0) {$result['code'] .= "readonly disabled";}
									$result['code'] .= "
									>
										$interloc_lib
										<br>
										<label class='last_maj'>".$gest_interlocs[$j]['lst_type_interloc_lib']."</label>
								</div>";
							}							
						}
					$result['code'] .= "
					</div>
				</center>
			</td>
		</tr>
		<tr>
			<td style='text-align:center;'>
				<center>";	
				For ($j=0; $j<$res_gest_interloc->rowCount(); $j++) { 
					$tbl_interloc_ref = explode(',', $gest_interlocs[$j]['tbl_interloc_ref']);				
					$interloc_lib = str_replace('   ', ' ', proprio_lib($gest_interlocs[$j]['particule_lib'], $gest_interlocs[$j]['nom_usage'], $gest_interlocs[$j]['prenom_usage'], $gest_interlocs[$j]['ddenom']));
					If ($tbl_interloc_ref[0] == Null) {
						$result['code'] .= "
						<div id='dropper_".$gest_interlocs[$j]['interloc_id']."' class='dropper' style='display:none'>
							<div class='drop_title'>
								$interloc_lib
									<br>
									<label class='last_maj'>".$gest_interlocs[$j]['lst_type_interloc_lib']."</label>
							</div>
						</div>";
					} Else {
						$result['code'] .= "
						<div id='dropper_".$gest_interlocs[$j]['interloc_id']."' class='dropper'>
							<div class='drop_title'>
								$interloc_lib
									<br>
									<label class='last_maj'>".$gest_interlocs[$j]['lst_type_interloc_lib']."</label>
							</div>";
						For ($t=0; $t<count($tbl_interloc_ref); $t++) {
							$interloc_index =  array_search($tbl_interloc_ref[$t], array_values(array_column($gest_interlocs, 'interloc_id')));
							If (is_numeric($interloc_index) && $interloc_index >= 0) {
								$interloc_lib = str_replace('   ', ' ', proprio_lib($gest_interlocs[$interloc_index]['particule_lib'], $gest_interlocs[$interloc_index]['nom_usage'], $gest_interlocs[$interloc_index]['prenom_usage'], $gest_interlocs[$interloc_index]['ddenom']));
								$result['code'] .= "
								<div id='div_".$tbl_interloc_ref[$t]."' class='draggable' draggable='true'>
									<input id='ref_".$tbl_interloc_ref[$t]."' type='checkbox' class='checkRef' style='float:left;display:none;'>
										$interloc_lib
										<br>
										<label class='last_maj'>".$gest_interlocs[$interloc_index]['lst_type_interloc_lib']."</label>								
								</div>";
							}
						}
						$result['code'] .= "
						</div>";
					}					
				}
				$result['code'] .= "
				</center>
			</td>
		</tr>";
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_showFormGestsubst($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		
		$result['code'] = "
		<tr>
			<td style='text-align:center;'>
				<center>
					<div id='dropper_subst' class='dropper' style='cursor:default;float:unset'>	
						<div class='drop_title'>Représentants identifiés</div>";
					
						$rq_gest_interloc = "
						SELECT
							t1.id,
							t1.interloc_id,
							t1.dnuper,
							t1.particule_id,
							t1.particule_lib,
							t1.ddenom,
							t1.nom_usage,
							t1.type_interloc_id,
							t1.type_interloc_lib,
							t1.groupe_interloc_id,
							t1.can_be_subst,
							t1.can_be_under_subst,
							t1.prenom_usage,
							array_to_string(array_agg(CASE WHEN t1.tbl_interloc_subst != '' THEN t1.tbl_interloc_subst END), ',') as tbl_interloc_subst,
							t1.referent,
							t1.etat_interloc_id
						FROM 
							(
								SELECT DISTINCT
									interlocuteurs.interloc_id || '_' || rel_saf_foncier_interlocs.type_interloc_id as id,
									interlocuteurs.interloc_id,
									interlocuteurs.dnuper,
									d_ccoqua.ccoqua as particule_id,
									d_ccoqua.particule_lib,
									interlocuteurs.ddenom,
									interlocuteurs.nom_usage,
									rel_saf_foncier_interlocs.type_interloc_id,
									d_type_interloc.type_interloc_lib,
									d_type_interloc.groupe_interloc_id,
									d_groupe_interloc.can_be_subst::text,
									d_groupe_interloc.can_be_under_subst::text,
									CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
										CASE WHEN strpos(interlocuteurs.prenom_usage, ' ') > 0 THEN
											substring(interlocuteurs.prenom_usage from 0 for  strpos(interlocuteurs.prenom_usage, ' ')) 
										ELSE 
											interlocuteurs.prenom_usage
										END
									ELSE	
										'N.P.' 
									END as prenom_usage,
									array_to_string(array_agg(DISTINCT subst2.id), ',') as tbl_interloc_subst,
									CASE WHEN ref2.nbr_under_ref > 0 THEN 'is_ref' ELSE 'is_not_ref' END as referent,
									etat_interloc_id
								FROM 
									animation_fonciere.interlocuteurs
									LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
									JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									LEFT JOIN 
										(
											SELECT DISTINCT 
												COALESCE(rel_saf_foncier_interlocs.interloc_id || '_' || rel_saf_foncier_interlocs.type_interloc_id, 'N.D.') as id, 
												interloc_substitution.rel_saf_foncier_interlocs_id_vp as foncier_interloc_id
											FROM 
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is = rel_saf_foncier_interlocs_id)
											WHERE 
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id
										) subst2 ON (subst2.foncier_interloc_id = rel_saf_foncier_interlocs_id)
									LEFT JOIN 
										(
											SELECT DISTINCT 
												rel_saf_foncier_interlocs_id_ref,
												count(rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id) as nbr_under_ref
											FROM 
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
											WHERE 
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id
											GROUP BY rel_saf_foncier_interlocs_id_ref
										) ref2 ON (ref2.rel_saf_foncier_interlocs_id_ref = rel_saf_foncier_interlocs_id)
								WHERE 
									session_af_id = :session_af_id AND 
									entite_af_id = :entite_af_id AND
									(can_be_subst = 't' OR can_be_under_subst = 't')
								GROUP BY 
									id,
									interlocuteurs.interloc_id, 
									d_ccoqua.ccoqua, 
									rel_saf_foncier_interlocs.type_interloc_id,
									d_type_interloc.type_interloc_lib,
									d_type_interloc.groupe_interloc_id,
									d_groupe_interloc.can_be_subst,
									d_groupe_interloc.can_be_under_subst,
									referent
								ORDER BY tbl_interloc_subst DESC
							) t1
						GROUP BY
							t1.id,
							t1.interloc_id,
							t1.dnuper,
							t1.particule_id,
							t1.particule_lib,
							t1.ddenom,
							t1.nom_usage,
							t1.type_interloc_id,
							t1.type_interloc_lib,
							t1.groupe_interloc_id,
							t1.can_be_subst,
							t1.can_be_under_subst,
							t1.prenom_usage,
							t1.referent,
							t1.etat_interloc_id
						ORDER BY id";
						$res_gest_interloc = $bdd->prepare($rq_gest_interloc);
						$res_gest_interloc->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
						$gest_interlocs = $res_gest_interloc->fetchall();	
						
						For ($j=0; $j<$res_gest_interloc->rowCount(); $j++) { 
							If ($gest_interlocs[$j]['can_be_subst'] == 'true' && $gest_interlocs[$j]['etat_interloc_id'] == 1) {
								$interloc_lib = str_replace('   ', ' ', proprio_lib($gest_interlocs[$j]['particule_lib'], $gest_interlocs[$j]['nom_usage'], $gest_interlocs[$j]['prenom_usage'], $gest_interlocs[$j]['ddenom']));
								$result['code'] .= "
								<div id='div_".$gest_interlocs[$j]['id']."' class='draggable ".$gest_interlocs[$j]['groupe_interloc_id']."' draggable='true'>
									$interloc_lib
									<br>
									<label class='last_maj'>".$gest_interlocs[$j]['type_interloc_lib']."</label>
								</div>";
							}						
						}
					$result['code'] .= "
					</div>
				</center>
			</td>
		</tr>
		<tr>
			<td style='text-align:center;'>
				<center>";					
				For ($j=0; $j<$res_gest_interloc->rowCount(); $j++) { 
					$can_be_under_subst = 'true';	
					Foreach (array_keys(array_values(array_column($gest_interlocs, 'interloc_id')), $gest_interlocs[$j]['interloc_id']) as $key) {
						If ($gest_interlocs[$key]['can_be_under_subst'] == 'false' || $gest_interlocs[$key]['referent'] == 'is_ref') {
							$can_be_under_subst = 'false';
						}
					}
					If ($gest_interlocs[$j]['can_be_subst'] == 'false' && $gest_interlocs[$j]['can_be_under_subst'] == 'true' && $can_be_under_subst == 'true') {
						$tbl_interloc_subst = preg_split("/,/", $gest_interlocs[$j]['tbl_interloc_subst']);				
						$interloc_lib = str_replace('   ', ' ', proprio_lib($gest_interlocs[$j]['particule_lib'], $gest_interlocs[$j]['nom_usage'], $gest_interlocs[$j]['prenom_usage'], $gest_interlocs[$j]['ddenom']));
						If ($tbl_interloc_subst[0] != Null && $gest_interlocs[array_search($tbl_interloc_subst[0], array_values(array_column($gest_interlocs, 'id')))]['groupe_interloc_id'] == 4) {
							$dropper_class = '';
						} Else {
							$dropper_class = '';
						}
						$result['code'] .= "
						<div id='dropper_".$gest_interlocs[$j]['id']."' class='dropper".$dropper_class."'>
							<div class='drop_title'>
								$interloc_lib
								<br>
								<label class='last_maj'>".$gest_interlocs[$j]['type_interloc_lib']."</label>
							</div>";
						If ($tbl_interloc_subst[0] != Null) {
							For ($t=0; $t<count($tbl_interloc_subst); $t++) {
								$interloc_index =  array_search($tbl_interloc_subst[$t], array_values(array_column($gest_interlocs, 'id')));
								$interloc_lib = str_replace('   ', ' ', proprio_lib($gest_interlocs[$interloc_index]['particule_lib'], $gest_interlocs[$interloc_index]['nom_usage'], $gest_interlocs[$interloc_index]['prenom_usage'], $gest_interlocs[$interloc_index]['ddenom']));
								$result['code'] .= "
								<div id='div_".$tbl_interloc_subst[$t]."' class='draggable ".$gest_interlocs[$interloc_index]['groupe_interloc_id']."' draggable='true'>
									$interloc_lib
									<br>
									<label class='last_maj'>".$gest_interlocs[$interloc_index]['type_interloc_lib']."</label>
								</div>";
							}
						}
						$result['code'] .= "
						</div>";
					}
				}
				$result['code'] .= "
				</center>
			</td>
		</tr>";
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_RemoveRefInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$old_interloc_ref_id = $data['params']['var_old_interloc_ref_id'];
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		
		$rq_delete_ref_interloc = "
		DELETE FROM animation_fonciere.interloc_referent 
		WHERE 
			rel_saf_foncier_interlocs_id_ref IN (
			SELECT rel_saf_foncier_interlocs_id
			FROM 
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE 
				session_af_id = $session_af_id AND 
				entite_af_id = $entite_af_id AND 
				interloc_id = ".$old_interloc_ref_id."
				)";
		// echo $rq_delete_ref_interloc;
		$res_delete_ref_interloc = $bdd->prepare($rq_delete_ref_interloc);
		$res_delete_ref_interloc->execute();	
		$res_delete_ref_interloc->closeCursor();
		
		$rq_infos_eaf = "
		SELECT DISTINCT 
			array_to_string(array_agg(t3.lot_id ORDER BY lot_id), ',') as tbl_eaf_lot_id,
			array_to_string(array_agg(rel_eaf_foncier.cad_site_id ORDER BY lot_id), ',') as tbl_eaf_cad_site_id,
			array_to_string(array_agg(rel_saf_foncier.rel_saf_foncier_id ORDER BY lot_id), ',') as tbl_rel_saf_foncier_id,
			t1.nbr_echanges
		FROM 		
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN 
				(SELECT DISTINCT 
					entite_af_id,
					count(echange_af_id) as nbr_echanges
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
				GROUP BY entite_af_id
				) t1 USING (entite_af_id)										
			
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			entite_af_id= :entite_af_id AND 
			t3.date_fin = 'N.D.' AND
			t4.date_fin = 'N.D.'
		GROUP BY 
			entite_af_id,
			t1.nbr_echanges";											
		$res_infos_eaf = $bdd->prepare($rq_infos_eaf);
		// echo $rq_infos_eaf;
		$res_infos_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$infos_eaf = $res_infos_eaf->fetch();
		
		$result['new_eaf_code'] = fct_rq_lst_interlocs($session_af_id, $entite_af_id, $infos_eaf['tbl_eaf_lot_id'], $infos_eaf['tbl_rel_saf_foncier_id']);		
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_showFormPrixOccsol($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		
		$rq_types_occsol = "
		SELECT DISTINCT
			type_occsol_af_id,
			type_occsol_af_lib
		FROM 
			animation_fonciere.rel_saf_foncier 
			JOIN animation_fonciere.rel_saf_foncier_occsol USING (rel_saf_foncier_id)
			JOIN animation_fonciere.d_type_occsol_af USING (type_occsol_af_id)
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id
		ORDER BY type_occsol_af_id";	
		// echo $rq_types_occsol;
		$res_types_occsol = $bdd->prepare($rq_types_occsol);
		$res_types_occsol->execute(array($session_af_id));
		$types_occsol = $res_types_occsol->fetchall();
		$result['code'] = '';
		For ($i=0; $i<$res_types_occsol->rowCount(); $i++) { 
			$result['code'] .= "
			<tr>
				<td style='text-align:right; margin-right:5px;'>
					<label class='label_simple'>".$types_occsol[$i]['type_occsol_af_lib']."</label>
				</td>
				<td style='text-align:left; margin-right:5px;'>
					<input type='text' size='5' class='editbox' style='text-align:right;' id='prix_occsol-".$types_occsol[$i]['type_occsol_af_id']."'><label class='label_simple'> €</label>
				</td>
			</tr>";
		}
		$bdd = null;
		print json_encode($result);
	}	
	
	function fct_showFormRegroupInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$flt_interloc_regroup = $data['params']['var_flt_interloc_regroup'];
		
		$rq_info_interlocs = "
		SELECT DISTINCT
			interlocuteurs.interloc_id,
			d_ccoqua.particule_lib,
			interlocuteurs.ddenom,
			interlocuteurs.nom_usage,
			interlocuteurs.nom_jeunefille,
			CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
				interlocuteurs.prenom_usage
			ELSE	
				'N.P.' 
			END as prenom_usage
		FROM 
			animation_fonciere.interlocuteurs 
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
		WHERE 
			rel_saf_foncier.session_af_id = ?";
		If ($flt_interloc_regroup <> 'Filtrer sur le nom' AND $flt_interloc_regroup <> '') {
			$rq_info_interlocs .= " AND CASE WHEN interlocuteurs.nom_usage IS NOT NULL AND interlocuteurs.nom_usage != 'N.P.' THEN interlocuteurs.nom_usage ILIKE '" . str_replace("'", "''", $flt_interloc_regroup) . "%' ELSE interlocuteurs.ddenom ILIKE '" . str_replace("'", "''", $flt_interloc_regroup) . "%' END";
		}
		$rq_info_interlocs .= "			
		GROUP BY 
			interlocuteurs.interloc_id, 
			d_ccoqua.particule_lib
		ORDER BY nom_usage, ddenom";	
		// echo $rq_info_interlocs;
		$res_info_interlocs = $bdd->prepare($rq_info_interlocs);
		$res_info_interlocs->execute(array($session_af_id));
		$lst_interlocs = $res_info_interlocs->fetchall();
		$result['interloc_regroup'] = "<option value='-1'>--- Sélectionnez un interlocuteur ---</option>";
		For ($j=0; $j<$res_info_interlocs->rowCount(); $j++) { 
			$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_interlocs[$j]['particule_lib'], $lst_interlocs[$j]['nom_usage'], $lst_interlocs[$j]['prenom_usage'], $lst_interlocs[$j]['ddenom']));
			$result['interloc_regroup'] .= "<option value='".$lst_interlocs[$j]['interloc_id']."'>".$interloc_lib."</option>";
		}
		$bdd = null;
		print json_encode($result);
	}	
	
	function fct_updateFormRegroupInterloc($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$interloc_id = $data['params']['var_interloc_id'];
		$nom = $data['params']['var_nom'];
		$mode_proprio = $data['params']['var_mode_proprio'];
		$bad_caract = array("-","–","’", "'");
		$good_caract = array("–","-","''", "''");
		
		$rq_infos_interloc = "
		SELECT DISTINCT
			ccoqua,
			d_ccoqua.particule_lib,
			interlocuteurs.ddenom,
			interlocuteurs.nom_usage,
			interlocuteurs.nom_jeunefille,
			CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
				interlocuteurs.prenom_usage
			ELSE	
				'N.P.' 
			END as prenom_usage,
			jdatnss,
			dldnss,
			dlign3,
			dlign4,
			dlign5,
			dlign6,
			gtoper
		FROM
			animation_fonciere.interlocuteurs
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
		WHERE
			interloc_id = ?";
		$res_infos_interloc = $bdd->prepare($rq_infos_interloc);
		$res_infos_interloc->execute(array($interloc_id));
		$infos_interloc = $res_infos_interloc->fetch();
		$levenshtein_nom = str_replace("'", "''", $infos_interloc['ddenom']);
		$levenshtein_naissance = $infos_interloc['jdatnss'].$infos_interloc['dldnss'];
		$levenshtein_adresse = $infos_interloc['dlign3'].$infos_interloc['dlign4'].$infos_interloc['dlign5'].$infos_interloc['dlign6'];
		
		If ($mode_proprio == 'SAF') {
			$rq_info_proprios_dispos = "
			SELECT 
				dnuper,
				particule_lib,
				ddenom,
				nom_usage,
				nom_jeunefille,
				prenom_usage,
				CASE WHEN nom_usage != 'N.P.' THEN
					levenshtein(nom_usage::text, '".$infos_interloc['nom_usage']."'::text)
					+
					levenshtein(jdatnss||dldnss::text, '".$levenshtein_naissance."'::text)*100
					+
					levenshtein(dlign3||dlign4||dlign5||dlign6::text, '".$levenshtein_adresse."'::text)*10000
				ELSE
					levenshtein(ddenom::text, '". str_replace($bad_caract, $good_caract, $infos_interloc['ddenom'])."'::text)
					+
					levenshtein(jdatnss||dldnss::text, '".$levenshtein_naissance."'::text)*100
					+
					levenshtein(dlign3||dlign4||dlign5||dlign6::text, '".$levenshtein_adresse."'::text)*10000
				END as ordre				
			FROM	
				(SELECT DISTINCT
					proprios_cen.dnuper,
					d_ccoqua.particule_lib,
					proprios_cen.ddenom,
					proprios_cen.nom_usage,
					proprios_cen.nom_jeunefille,
					CASE WHEN proprios_cen.prenom_usage IS NOT NULL AND proprios_cen.prenom_usage != 'N.P.' THEN 
						proprios_cen.prenom_usage
					ELSE	
						'N.P.' 
					END as prenom_usage,
					COALESCE(proprios_cen.jdatnss, '') as jdatnss,
					COALESCE(proprios_cen.dldnss, '') as dldnss,
					COALESCE(proprios_cen.dlign3, '') as dlign3,
					COALESCE(proprios_cen.dlign4, '') as dlign4,
					COALESCE(proprios_cen.dlign5, '') as dlign5,
					COALESCE(proprios_cen.dlign6, '') as dlign6
				FROM 
					cadastre.proprios_cen
					LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
					JOIN animation_fonciere.rel_proprio_interloc USING (dnuper)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				WHERE
					interloc_id != $interloc_id 
					AND session_af_id = $session_af_id ";
				If ($infos_interloc['ccoqua'] != '') {
					$rq_info_proprios_dispos .= "
					AND (proprios_cen.ccoqua = ".$infos_interloc['ccoqua']." OR proprios_cen.ccoqua IS NULL)";
				}
				If ($infos_interloc['gtoper'] != '') {
					$rq_info_proprios_dispos .= "
					AND (proprios_cen.gtoper = ".$infos_interloc['gtoper']." OR proprios_cen.gtoper IS NULL)";
				}
				If ($infos_interloc['gtoper'] == '2') {
					$rq_info_proprios_dispos .= "
					AND (proprios_cen.ccoqua IS NULL)";
				}
				If ($nom <> 'Filtrer sur le nom' AND $nom <> '') {
					$rq_info_proprios_dispos .= " AND CASE WHEN proprios_cen.nom_usage IS NOT NULL AND proprios_cen.nom_usage != 'N.P.' THEN proprios_cen.nom_usage ILIKE '" . str_replace("'", "''", $nom) . "%' ELSE proprios_cen.ddenom ILIKE '" . str_replace("'", "''", $nom) . "%' END";
				}
				$rq_info_proprios_dispos .= "
				) t1				
			ORDER BY ddenom";
		} Else If ($mode_proprio == 'sites') {	
			$rq_info_proprios_dispos = "
			SELECT 
				dnuper,
				particule_lib,
				ddenom,
				nom_usage,
				nom_jeunefille,
				prenom_usage,
				CASE WHEN levenshtein(ddenom||nom_usage||nom_jeunefille||prenom_usage||jdatnss||dlign3::text, '".$levenshtein_nom."'::text) < length('".$levenshtein_nom."'::text)/1.5 THEN levenshtein(ddenom||nom_usage||nom_jeunefille||prenom_usage||jdatnss||dlign3::text, '".$levenshtein_nom."'::text) ELSE length('".$levenshtein_nom."'::text) END as ordre
			FROM	
				(SELECT DISTINCT
					proprios_cen.dnuper,
					d_ccoqua.particule_lib,
					proprios_cen.ddenom,
					proprios_cen.nom_usage,
					proprios_cen.nom_jeunefille,
					CASE WHEN proprios_cen.prenom_usage IS NOT NULL AND proprios_cen.prenom_usage != 'N.P.' THEN 
						proprios_cen.prenom_usage
					ELSE	
						'N.P.' 
					END as prenom_usage,
					COALESCE(proprios_cen.jdatnss, '') as jdatnss,
					COALESCE(proprios_cen.dldnss, '') as dldnss,
					COALESCE(proprios_cen.dlign3, '') as dlign3,
					COALESCE(proprios_cen.dlign4, '') as dlign4,
					COALESCE(proprios_cen.dlign5, '') as dlign5,
					COALESCE(proprios_cen.dlign6, '') as dlign6
				FROM 
					cadastre.proprios_cen
					LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
					LEFT JOIN animation_fonciere.rel_proprio_interloc USING (dnuper)
				WHERE
					interloc_id IS NULL ";
				If ($infos_interloc['ccoqua'] != '') {
					$rq_info_proprios_dispos .= "
					AND (ccoqua = ".$infos_interloc['ccoqua']." OR ccoqua IS NULL)";
				}
				If ($infos_interloc['gtoper'] != '') {
					$rq_info_proprios_dispos .= "
					AND (gtoper = ".$infos_interloc['gtoper']." OR gtoper IS NULL)";
				}
				If ($infos_interloc['gtoper'] == '2') {
					$rq_info_proprios_dispos .= "
					AND (proprios_cen.ccoqua IS NULL)";
				}
				If ($nom <> 'Filtrer sur le nom' AND $nom <> '') {
					$rq_info_proprios_dispos .= " AND CASE WHEN nom_usage IS NOT NULL AND nom_usage != 'N.P.' THEN nom_usage ILIKE '" . str_replace("'", "''", $nom) . "%' ELSE ddenom ILIKE '" . str_replace("'", "''", $nom) . "%' END";
				}	
				$rq_info_proprios_dispos .= "
				) t1
			ORDER BY ddenom";
		}
		// echo $rq_info_proprios_dispos;
		$res_info_proprios_dispos = $bdd->prepare($rq_info_proprios_dispos);
		$res_info_proprios_dispos->execute(array());
		$lst_proprios_dispos = $res_info_proprios_dispos->fetchall();		
		$result['proprios_dispos'] = '';
		If ($interloc_id != -1) {
			For ($j=0; $j<$res_info_proprios_dispos->rowCount(); $j++) { 
				$proprio_lib = str_replace('   ', ' ', proprio_lib($lst_proprios_dispos[$j]['particule_lib'], $lst_proprios_dispos[$j]['nom_usage'], $lst_proprios_dispos[$j]['prenom_usage'], $lst_proprios_dispos[$j]['ddenom']));
				$result['proprios_dispos'] .= "<option id='".$lst_proprios_dispos[$j]['dnuper']."' style='font-weight:normal;' value='".$lst_proprios_dispos[$j]['dnuper']."'>".$proprio_lib;
				If ($lst_proprios_dispos[$j]['nom_usage'] != $lst_proprios_dispos[$j]['nom_jeunefille'] AND $lst_proprios_dispos[$j]['nom_jeunefille'] != 'N.P.')
					$result['proprios_dispos'] .= " (".$lst_proprios_dispos[$j]['nom_jeunefille'].")";
				$result['proprios_dispos'] .= "</option>";
			}
		}
		
		$rq_info_proprios_associes = "
		SELECT DISTINCT
			proprios_cen.dnuper,
			d_ccoqua.particule_lib,
			proprios_cen.ddenom,
			proprios_cen.nom_usage,
			CASE WHEN proprios_cen.prenom_usage IS NOT NULL AND proprios_cen.prenom_usage != 'N.P.' THEN 
				proprios_cen.prenom_usage
			ELSE	
				'N.P.' 
			END as prenom_usage
		FROM 
			cadastre.proprios_cen
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
			JOIN animation_fonciere.rel_proprio_interloc USING (dnuper)
		WHERE
			interloc_id = ?
		ORDER BY nom_usage, ddenom";	
		// echo $rq_info_interlocs;
		$res_info_proprios_associes = $bdd->prepare($rq_info_proprios_associes);
		$res_info_proprios_associes->execute(array($interloc_id));
		$lst_proprios_associes = $res_info_proprios_associes->fetchall();
		$result['proprios_associes'] = '';
		For ($j=0; $j<$res_info_proprios_associes->rowCount(); $j++) { 
			$proprio_lib = str_replace('   ', ' ', proprio_lib($lst_proprios_associes[$j]['particule_lib'], $lst_proprios_associes[$j]['nom_usage'], $lst_proprios_associes[$j]['prenom_usage'], $lst_proprios_associes[$j]['ddenom']));
			$result['proprios_associes'] .= "<option id='".$lst_proprios_associes[$j]['dnuper']."' style='font-weight:normal;' value='".$lst_proprios_associes[$j]['dnuper']."'>".$proprio_lib."</option>";
		}
		$bdd = null;
		print json_encode($result);
	}

	function fct_prixOccsol($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$tbl_prix_occsol = $data['params']['var_tbl_prix_occsol'];
		
		For ($i = 0; $i<count($tbl_prix_occsol); $i++) {
			$prix_occsol = explode(':', $tbl_prix_occsol[$i]);
			$rq_update = "
			UPDATE animation_fonciere.rel_saf_foncier_occsol SET montant = ".$prix_occsol[1]."
			WHERE 
				rel_saf_foncier_id IN (SELECT rel_saf_foncier_id FROM animation_fonciere.rel_saf_foncier WHERE session_af_id = $session_af_id) AND
				type_occsol_af_id = ".$prix_occsol[0]." AND
				montant IS NULL";
			$res_update = $bdd->prepare($rq_update);
			// echo $rq_update;
			$res_update->execute();
		}
				
		$bdd = null;
	}
	
	function fct_regroupeProprios($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$interloc_id = $data['params']['var_interloc_id'];
		If (isset($data['params']['var_tbl_proprios_assoc'])) {
			$tbl_proprios_assoc = $data['params']['var_tbl_proprios_assoc'];
			$lst_proprios_assoc = implode(',', $tbl_proprios_assoc);
		} Else {
			$tbl_proprios_assoc = array();
			$lst_proprios_assoc = '';
		}
		If (isset($data['params']['var_tbl_proprios_dissoc'])) {
			$tbl_proprios_dissoc = $data['params']['var_tbl_proprios_dissoc'];		
			$lst_proprios_dissoc = implode(',', $tbl_proprios_dissoc);
		} Else {
			$tbl_proprios_dissoc = array();		
			$lst_proprios_dissoc = '';
		}

		//La mise à jour de ces données est complexe et doit se repercuter sur les EAF.
		//Il faut donc commencer par récupérer une liste de parcelles (cad_site_id) affectées par les modifications à venir de manière à mettre à jour les données.
		If (count($tbl_proprios_assoc) > 0 || count($tbl_proprios_dissoc) > 0) {
			$rq_lst_cad_site_id = "
			SELECT 
				array_to_string(array_agg(cad_site_id), ',') as tbl_cad_site_id
			FROM
				(SELECT DISTINCT 
					cad_site_id
				FROM
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					JOIN animation_fonciere.interlocuteurs USING (interloc_id)
					JOIN animation_fonciere.rel_proprio_interloc USING (interloc_id)
				WHERE rel_proprio_interloc.dnuper IN ($lst_proprios_assoc) OR rel_proprio_interloc.dnuper IN ($lst_proprios_assoc)) t1";
			$res_lst_cad_site_id = $bdd->prepare($rq_lst_cad_site_id);
			// echo $rq_lst_cad_site_id;
			$res_lst_cad_site_id->execute();
			$tbl_cad_site_id = $res_lst_cad_site_id->fetch();
			$lst_cad_site_id = preg_split("/,/", $tbl_cad_site_id['tbl_cad_site_id']);
		} Else {
			$lst_cad_site_id = array();
		}
		
		//ON TRAITE D'ABORD LES ASSOCIATIONS
		//On commence par mettre à jour les interlocuteurs qui ont été associé
		$rq_update_rel_saf_foncier_interlocs = "
		UPDATE 
			animation_fonciere.rel_saf_foncier_interlocs SET interloc_id = $interloc_id 
		WHERE rel_saf_foncier_interlocs_id IN (
			SELECT 
				rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.interlocuteurs USING (interloc_id)
				JOIN animation_fonciere.rel_proprio_interloc USING (interloc_id)
			WHERE rel_proprio_interloc.dnuper IN ($lst_proprios_assoc))";
		$res_update_rel_saf_foncier_interlocs = $bdd->prepare($rq_update_rel_saf_foncier_interlocs);
		// echo $rq_update_rel_saf_foncier_interlocs;
		$res_update_rel_saf_foncier_interlocs->execute();
		$res_update_rel_saf_foncier_interlocs->closeCursor();
		
		//On poursuit en mettant à jour la table d'association rel_propio_interloc
		$rq_update_rel_proprio_interloc = "
		UPDATE 
			animation_fonciere.rel_proprio_interloc SET interloc_id = $interloc_id 
		WHERE dnuper IN ($lst_proprios_assoc)";
		$res_update_rel_proprio_interloc = $bdd->prepare($rq_update_rel_proprio_interloc);
		// echo $rq_update_rel_proprio_interloc;
		$res_update_rel_proprio_interloc->execute();
		$res_update_rel_proprio_interloc->closeCursor();	

		//Il faut également ajouter ce qui n'a pas été mis à jour car absent de la table rel_propio_interloc
		$rq_insert_rel_proprio_interloc = "
		INSERT INTO animation_fonciere.rel_proprio_interloc (
			SELECT 
				dnuper,
				" . $interloc_id . "
			FROM 
				cadastre.proprios_cen t1 
			WHERE 
				dnuper IN ($lst_proprios_assoc)
				AND dnuper NOT IN (
					SELECT 
						dnuper
					FROM 
						animation_fonciere.rel_proprio_interloc
					WHERE
						dnuper IN ($lst_proprios_assoc)
					)
			)";
		$res_insert_rel_proprio_interloc = $bdd->prepare($rq_insert_rel_proprio_interloc);
		// echo $rq_insert_rel_proprio_interloc;
		$res_insert_rel_proprio_interloc->execute();
		$res_insert_rel_proprio_interloc->closeCursor();	
				
		//ON TRAITE MAINTENANT LES DISSOCIATIONS
		If (count($tbl_proprios_dissoc) > 0) {
			//on récupère si besoin le nouveau entite_af_id à créer
			$rq_new_entite_af_id = "SELECT nextval('animation_fonciere.entite_af_id_seq'::regclass)";
			$res_new_entite_af_id = $bdd->prepare($rq_new_entite_af_id);
			$res_new_entite_af_id->execute();
			$new_entite_af_id_temp = $res_new_entite_af_id->fetch();
			$new_entite_af_id = $new_entite_af_id_temp[0];
			$res_new_entite_af_id->closeCursor();
			
			//on intègre la nouvelle EAF
			$rq_insert_entite_eaf = "INSERT INTO animation_fonciere.entite_af (entite_af_id) VALUES ($new_entite_af_id)";
			$res_insert_entite_eaf = $bdd->prepare($rq_insert_entite_eaf);
			// echo $rq_insert_entite_eaf;
			$res_insert_entite_eaf->execute();
			$res_insert_entite_eaf->closeCursor();
		}
		
		For ($i = 0; $i<count($tbl_proprios_dissoc); $i++) {
			//on récupère l'interloc_id qu'il faudra supprimer
			$rq_old_interlocs_id = "SELECT interloc_id FROM animation_fonciere.rel_proprio_interloc WHERE dnuper = ".$tbl_proprios_dissoc[$i];
			$res_old_interlocs_id = $bdd->prepare($rq_old_interlocs_id);
			// echo $rq_old_interlocs_id;
			$res_old_interlocs_id->execute();
			$old_interlocs_id = $res_old_interlocs_id->fetch();
			
			//on récupère le nouveau interloc_id à créer
			$rq_new_interloc_id = "SELECT nextval('animation_fonciere.interloc_id_seq'::regclass)";
			$res_new_interloc_id = $bdd->prepare($rq_new_interloc_id);
			$res_new_interloc_id->execute();
			$new_interloc_id_temp = $res_new_interloc_id->fetch();
			$new_interloc_id = $new_interloc_id_temp[0];
			$res_new_interloc_id->closeCursor();
			
			//on ajoute le nouvel interlocuteur
			$rq_insert_interloc = "
			INSERT INTO animation_fonciere.interlocuteurs (
				interloc_id,
				etat_interloc_id,
				dnuper,
				ccoqua,
				ddenom,
				nom_usage,
				nom_jeunefille,
				prenom_usage,
				nom_conjoint,
				prenom_conjoint,
				dnomlp,
				dprnlp,
				epxnee,
				dnomcp,
				dprncp,
				jdatnss,
				dldnss,
				dlign3,
				dlign4,
				dlign5,
				dlign6,
				email,
				fixe_domicile,
				fixe_travail,
				portable_domicile,
				portable_travail,
				fax,
				gtoper,
				ccogrm,
				dnatpr,
				dsglpm,
				commentaires,
				annee_matrice,
				maj_user,
				maj_date)
			(SELECT 
				".$new_interloc_id.", 
				1, 
				t1.dnuper,
				t1.ccoqua,
				t1.ddenom,
				t1.nom_usage,
				t1.nom_jeunefille,
				t1.prenom_usage,
				t1.nom_conjoint,
				t1.prenom_conjoint,
				t1.dnomlp,
				t1.dprnlp,
				t1.epxnee,
				t1.dnomcp,
				t1.dprncp,
				t1.jdatnss,
				t1.dldnss,
				t1.dlign3,
				t1.dlign4,
				t1.dlign5,
				t1.dlign6,
				t1.email,
				t1.fixe_domicile,
				t1.fixe_travail,
				t1.portable_domicile,
				t1.portable_travail,
				t1.fax,
				t1.gtoper,
				t1.ccogrm,
				t1.dnatpr,
				t1.dsglpm,
				t1.commentaires,
				t1.annee_matrice,
				t1.maj_user,
				t1.maj_date 
			FROM cadastre.proprios_cen t1 
			WHERE t1.dnuper = ".$tbl_proprios_dissoc[$i].")";
			$res_insert_interloc = $bdd->prepare($rq_insert_interloc);
			// echo $rq_insert_interloc;
			$res_insert_interloc->execute();
			$res_insert_interloc->closeCursor();
			
			//On récupère maintenant les parcelles concernées par ce propriétaire via le foncier et pas l'AF
			$rq_lst_cad_site_id_prorpio = "
			SELECT DISTINCT cad_site_id
			FROM
				foncier.cadastre_site t4
				JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
				JOIN cadastre.cptprop_cen USING (dnupro)
				JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
			WHERE
				dnuper = ".$tbl_proprios_dissoc[$i]." AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'";			
			
			$rq_reset_prix_eaf = "
			UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
				(
					SELECT 
						rel_eaf_foncier_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						cad_site_id IN (".$rq_lst_cad_site_id_prorpio.")
				)";
			$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
			// echo $rq_reset_prix_eaf.'<br>';
			$res_reset_prix_eaf->execute();
			$res_reset_prix_eaf->closeCursor();
			
			//on met à jour l'entite_af_id pour les parcelle concernées
			$rq_update_rel_eaf_foncier = "
			UPDATE animation_fonciere.rel_eaf_foncier 
			SET entite_af_id = ".$new_entite_af_id." 
			WHERE cad_site_id IN (".$rq_lst_cad_site_id_prorpio.")";			
			$res_update_rel_eaf_foncier = $bdd->prepare($rq_update_rel_eaf_foncier);
			// echo $rq_update_rel_eaf_foncier;
			$res_update_rel_eaf_foncier->execute();
			$res_update_rel_eaf_foncier->closeCursor();	

			$rq_reset_prix_eaf = "
			UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = NULL, prix_eaf_negocie = NULL WHERE rel_eaf_foncier_id IN 
				(
					SELECT 
						rel_eaf_foncier_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						entite_af_id = :entite_af_id
				)";
			$res_reset_prix_eaf = $bdd->prepare($rq_reset_prix_eaf);
			// echo $rq_reset_prix_eaf.'<br>';
			$res_reset_prix_eaf->execute(array('entite_af_id'=>$new_entite_af_id));
			$res_reset_prix_eaf->closeCursor();
			
			//On met à jour la table rel_saf_foncier_interlocs avec le nouvel interlocuteur
			$rq_update_rel_saf_foncier_interlocs = "
			UPDATE animation_fonciere.rel_saf_foncier_interlocs 
			SET interloc_id = ".$new_interloc_id." 
			WHERE 
				interloc_id = ".$old_interlocs_id['interloc_id']."
				AND rel_saf_foncier_id IN 
					(SELECT rel_saf_foncier_id 
					FROM 
						animation_fonciere.rel_saf_foncier
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE cad_site_id IN (".$rq_lst_cad_site_id_prorpio.")
					)";
			$res_update_rel_saf_foncier_interlocs = $bdd->prepare($rq_update_rel_saf_foncier_interlocs);
			// echo $rq_update_rel_saf_foncier_interlocs;
			$res_update_rel_saf_foncier_interlocs->execute();
			$res_update_rel_saf_foncier_interlocs->closeCursor();
			
			//On poursuit en mettant à jour la table d'association rel_propio_interloc
			$rq_update_rel_proprio_interloc = "UPDATE animation_fonciere.rel_proprio_interloc SET interloc_id = ".$new_interloc_id." WHERE interloc_id = ".$old_interlocs_id['interloc_id']." AND dnuper = ".$tbl_proprios_dissoc[$i];
			$res_update_rel_proprio_interloc = $bdd->prepare($rq_update_rel_proprio_interloc);
			// echo $rq_update_rel_proprio_interloc;
			$res_update_rel_proprio_interloc->execute();
			$res_update_rel_proprio_interloc->closeCursor();			
		}
		
		//On peut supprimer les interlocuteurs qui ne sont plus utilisés dans la table interlocuteurs
		$rq_delete_interlocuteurs = "
		DELETE FROM animation_fonciere.interlocuteurs WHERE interloc_id NOT IN (SELECT interloc_id FROM animation_fonciere.rel_saf_foncier_interlocs UNION SELECT interloc_id FROM animation_fonciere.rel_proprio_interloc)";
		$res_delete_interlocuteurs = $bdd->prepare($rq_delete_interlocuteurs);
		// echo $rq_delete_interlocuteurs;
		$res_delete_interlocuteurs->execute();
		$res_delete_interlocuteurs->closeCursor();	
		
		//ON TRAITE MAINTENANT LA MISE A JOUR DES EAF
		For ($i=0; $i<count($lst_cad_site_id); $i++) {
			//On met à jour l'EAF en fonction des nouveaux interlocuteurs
			$rq_update_rel_eaf_foncier = "
			WITH twith as
			(
				SELECT
					t2.entite_af_id
				FROM
					(SELECT DISTINCT 
						interloc_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						cad_site_id = ".$lst_cad_site_id[$i]." AND type_interloc_id = 1) t1,
					(SELECT 
						entite_af_id, 
						array_agg(interloc_id ORDER BY interloc_id) as lst_interloc_id
					FROM
						(SELECT DISTINCT
							entite_af_id,
							interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE type_interloc_id = 1) t2
					GROUP BY entite_af_id) t2
				GROUP BY t2.entite_af_id, t2.lst_interloc_id
				HAVING t2.lst_interloc_id = array_agg(interloc_id ORDER BY interloc_id)
			)
			UPDATE 
				animation_fonciere.rel_eaf_foncier 
			SET 
				entite_af_id = (SELECT entite_af_id FROM twith LIMIT 1)
			FROM
				twith
			WHERE
				rel_eaf_foncier.entite_af_id IN (twith.entite_af_id)";
			$res_update_rel_eaf_foncier = $bdd->prepare($rq_update_rel_eaf_foncier);
			// echo $rq_update_rel_eaf_foncier;
			$res_update_rel_eaf_foncier->execute();
			$res_update_rel_eaf_foncier->closeCursor();	
		}
		
		//On peut supprimer les eaf qui ne sont plus utilisés dans la table entite_af
		$rq_delete_entite_af = "
		DELETE FROM animation_fonciere.entite_af WHERE entite_af_id NOT IN (SELECT entite_af_id FROM animation_fonciere.rel_eaf_foncier)";
		$res_delete_entite_af = $bdd->prepare($rq_delete_entite_af);
		// echo $rq_delete_entite_af;
		$res_delete_entite_af->execute();
		$res_delete_entite_af->closeCursor();

		//Il faut maintenant une requète pour mettre a jour toute la page
		$result['code'] = '';
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_showInfosEAF($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];
		$result['lbl_title_infos_eaf'] = "Informations sur une Entit&eacute; d'Animation Fonci&egrave;re";
		$result['code'] = "";
		
		$rq_tbl_infos_proprios_eaf = "
		SELECT
			t_interloc.interloc_id,
			array_to_string(array_agg(dnuper ORDER BY dnuper), ',') as tbl_dnuper
		FROM
			(SELECT DISTINCT
				rel_saf_foncier_interlocs.interloc_id		
			FROM
				animation_fonciere.rel_saf_foncier_interlocs
				JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			WHERE
				type_interloc_id = 1
				AND entite_af_id = :entite_af_id
				AND session_af_id = :session_af_id
			) t_interloc
			LEFT JOIN animation_fonciere.rel_proprio_interloc USING (interloc_id)
		GROUP BY
			interloc_id";		
		$res_tbl_infos_proprios_eaf = $bdd->prepare($rq_tbl_infos_proprios_eaf);
		// echo $rq_tbl_infos_proprios_eaf;
		$res_tbl_infos_proprios_eaf->execute(array('session_af_id' => $session_af_id, 'entite_af_id' => $entite_af_id));
		$tbl_infos_proprios_eaf = $res_tbl_infos_proprios_eaf->fetchall();		
	
		$data_where = array();
		For ($i=0; $i<$res_tbl_infos_proprios_eaf->rowCount(); $i++) {
			array_push($data_where, explode(',', $tbl_infos_proprios_eaf[$i]['tbl_dnuper']));
		}
		$combos = combos($data_where);
		$where_cpt = '';
		foreach ($combos as $tab) {
			sort($tab);
			$where_cpt .= "t1.lst_dnuper = '".implode($tab, ',')."' OR ";
		}
		$where_cpt = substr($where_cpt, 0, -4);
		$rq_lst_dnupro = "
		SELECT
			dnupro
		FROM
			(
				SELECT 
					dnupro,
					array_to_string(array_agg(dnuper ORDER BY dnuper), ',') as lst_dnuper
				FROM cadastre.r_prop_cptprop_cen
				GROUP BY dnupro
			) t1
		WHERE " . $where_cpt;
		
		$rq_sites_eaf = "
		SELECT DISTINCT
			site_id,
			site_nom
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
		WHERE
			entite_af_id = $entite_af_id
			AND t4.date_fin = 'N.D.'
		
		UNION
		
		SELECT DISTINCT
			site_id,
			site_nom
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
		WHERE
			dnupro IN (".$rq_lst_dnupro.")
			AND t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'";
		$res_sites_eaf = $bdd->prepare($rq_sites_eaf);
		// echo $rq_sites_eaf;
		$res_sites_eaf->execute();
		$sites_eaf = $res_sites_eaf->fetchall();
		$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Cette EAF est présente sur les sites suivants :</label>
		";
		For ($i=0; $i<$res_sites_eaf->rowCount(); $i++) {
			$lst_site[] = "'".$sites_eaf[$i]['site_id']."'";
			$vget = "site_nom:".$sites_eaf[$i]['site_nom'].";site_id:".$sites_eaf[$i]['site_id'].";";
			$vget_crypt = base64_encode($vget);
			$lien_site = ROOT_PATH . "foncier/fiche_site.php?d=$vget_crypt#general";
			$result['code'] .= "			
			<li style='margin-left:25px;'>
				<label class='label'>".$sites_eaf[$i]['site_nom']." (".$sites_eaf[$i]['site_id'].")</label>
				<a href='".$lien_site."'>
					<img style='margin:-5px 0;' src='".ROOT_PATH."images/fiche_site.png' width='15px' onmouseover='tooltip.show(this)' onmouseout='tooltip.hide(this)' title='Fiche site'>
				</a>
			</li>";
		}
		$result['code'] .= "					
				</td>
			</tr>
		";
		
		$rq_echanges_eaf = "
		SELECT 
			session_af_id,
			session_af_lib,
			count(DISTINCT echange_af_id) as nbr_echanges
		FROM
			animation_fonciere.session_af
			JOIN animation_fonciere.rel_saf_foncier USING (session_af_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
			LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
		WHERE
			entite_af_id = :entite_af_id AND
			session_af_id != :session_af_id
		GROUP BY
			session_af_id,
			session_af_lib";
		$res_echanges_eaf = $bdd->prepare($rq_echanges_eaf);
		// echo $rq_echanges_eaf;
		$res_echanges_eaf->execute(array('session_af_id'=>$session_af_id,'entite_af_id'=>$entite_af_id));
		$echanges_eaf = $res_echanges_eaf->fetchall();
		$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>";					
			If ($res_echanges_eaf->rowCount() == 0) {
				$result['code'] .= "<label class='label'>Cette EAF n'est présente sur aucune autre Session d'Animation Foncière</label>";
			} Else {
				$result['code'] .= "<label class='label'>Cette EAF est présente sur les Sessions d'Animation Foncière suivantes :</label>";
				For ($i=0; $i<$res_echanges_eaf->rowCount(); $i++) {
					$vget = "mode:consult;session_af_id:".$session_af_id.";session_af_lib:".$echanges_eaf[$i]['session_af_lib'].";";
					$vget_crypt = base64_encode($vget);
					$lien_saf = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_saf.php?d=$vget_crypt#general";				
					$result['code'] .= "			
					<li style='margin-left:25px;'>
						<label class='label'>".$echanges_eaf[$i]['session_af_lib'];
					If ($echanges_eaf[$i]['nbr_echanges'] == 0) {
						$result['code'] .= " mais aucun échange n'a été réalisé à ce jour";
					} Else {
						$result['code'] .= " et des échanges ont déjà été réalisés";
					}
					$result['code'] .= "
						</label>
						<a href='".$lien_saf."'>
							<img style='margin:-5px 0;' src='".ROOT_PATH."images/fiche_site.png' width='15px' onmouseover='tooltip.show(this)' onmouseout='tooltip.hide(this)' title='Fiche SAF'>
						</a>
					</li>";
				}
			}
			$result['code'] .= "					
				</td>
			</tr>
		";
		
		If ($res_tbl_infos_proprios_eaf->rowCount() == 1) {
			$rq_actes_saf = "
			SELECT DISTINCT 
				NULL::integer as session_af_id,
				site_id,
				site_nom,
				'Lots acquis' as lib,
				count(t3.lot_id) as nbr_lot,
				1 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN cadastre.cptprop_cen USING (dnupro)
				JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
				JOIN foncier.r_cad_site_mf USING (cad_site_id)
				JOIN foncier.mf_acquisitions USING (mf_id)
				JOIN foncier.r_mfstatut USING (mf_id)
				LEFT JOIN sites.sites USING (site_id),
				(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
				
			WHERE 
				r_mfstatut.date = tt1.statut_date 
				AND r_mfstatut.mf_id = tt1.mf_id 
				AND r_mfstatut.statutmf_id = '1'
				AND dnuper IN (SELECT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = :interloc_id)
				AND t4.date_fin = 'N.D.'
				AND t3.date_fin = 'N.D.'
			GROUP BY
				site_id,
				site_nom

			UNION

			SELECT DISTINCT 
				NULL::integer as session_af_id,
				site_id,
				site_nom,
				'Lots conventionn&eacute;s' as lib,
				count(t3.lot_id) as nbr_lot,
				2 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN foncier.r_cad_site_mu USING (cad_site_id)	
				JOIN cadastre.cptprop_cen USING (dnupro)
				JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
				JOIN sites.sites USING (site_id)
				JOIN foncier.mu_conventions USING (mu_id)
				JOIN foncier.r_mustatut USING (mu_id),
				(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut
			.mu_id) as tt2
			WHERE 
				r_mustatut.date = tt2.statut_date 
				AND r_mustatut.mu_id = tt2.mu_id 
				AND r_mustatut.statutmu_id = '1'	
				AND dnuper IN (SELECT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = :interloc_id)
				AND t4.date_fin = 'N.D.'
				AND t3.date_fin = 'N.D.'
			GROUP BY
				site_id,
				site_nom

			UNION

			SELECT DISTINCT
				session_af_id,	
				site_id,
				site_nom,
				CASE WHEN tnegoprix.nbr_all_nego = nbr_interloc_vp THEN
					tbl_nego[1]
				ELSE
					tbl_nego[1] || ' (A confirmer)'
				END as lib,
				count(t2.lot_id) as nbr_lot,
				3 as ordre	
			FROM
				cadastre.lots_cen t2
				JOIN cadastre.parcelles_cen USING (par_id)
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN sites.sites USING (site_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
				JOIN (
					SELECT
						rel_saf_foncier_id,
						count(interloc_id) as nbr_interloc_vp
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
					GROUP BY
						rel_saf_foncier_id
				) st1 USING (rel_saf_foncier_id)
				LEFT JOIN (
					SELECT DISTINCT
						rel_saf_foncier_id, 
						array_agg(DISTINCT nego_af_lib) as tbl_nego,
						array_length(array_agg(DISTINCT nego_af_id), 1) as nbr_nego,
						array_length(array_agg(nego_af_id), 1) as nbr_all_nego
					FROM 
						animation_fonciere.rel_af_echanges
						JOIN animation_fonciere.d_nego_af USING (nego_af_id)
						JOIN animation_fonciere.echanges_af USING (echange_af_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN (
							SELECT
							rel_saf_foncier_interlocs_id,
							max(echange_af_date) as max_date
							FROM 
							animation_fonciere.rel_af_echanges
							JOIN animation_fonciere.echanges_af USING (echange_af_id)
							WHERE 
							nego_af_id IS NOT NULL
							GROUP BY
							rel_saf_foncier_interlocs_id
							) st2 ON (echanges_af.echange_af_date = st2.max_date AND rel_af_echanges.rel_saf_foncier_interlocs_id
			= st2.rel_saf_foncier_interlocs_id)
							WHERE 
							nego_af_id IS NOT NULL
							AND interloc_id = :interloc_id
					GROUP BY
						rel_saf_foncier_id
						) tnegoprix USING (rel_saf_foncier_id)
			WHERE 
				interloc_id = :interloc_id
				AND t4.date_fin = 'N.D.'
				AND t3.date_fin = 'N.D.'
			GROUP BY
				session_af_id,
				nbr_all_nego,
				nbr_interloc_vp,
				tbl_nego,
				site_id,
				site_nom
			HAVING 
				tbl_nego[1] IS NOT NULL
			ORDER BY 
				site_id,
				ordre,
				lib";		
			// echo $rq_actes_saf;
			$res_actes_saf = $bdd->prepare($rq_actes_saf);
			$res_actes_saf->execute(array('interloc_id'=>$tbl_infos_proprios_eaf[0]['interloc_id']));
			$actes_saf = $res_actes_saf->fetchAll();
			If ($res_actes_saf->rowCount() > 0) {
				$result['code'] .= "
				<tr>
					<td class='bilan-af' style='text-align:left;'>
						<label class='label'>Bilan des actes en cours et à venir avec ce propriétaire :</label>";
					For ($i=0; $i<$res_actes_saf->rowCount(); $i++) {
						If ($actes_saf[$i]['nbr_lot'] > 0) {				
							$result['code'] .= "			
							<li style='margin-left:25px;'>
								<label class='label'>".$actes_saf[$i]['lib']." pour ".$actes_saf[$i]['nbr_lot'] ." lot(s) sur le site ".$actes_saf[$i]['site_nom']. " (".$actes_saf[$i]['site_id'].")</label>
							</li>";
						} 
					}			
				$result['code'] .= "					
					</td>
				</tr>";
			} Else {
				$result['code'] .= "
				<tr>
					<td class='bilan-af' style='text-align:left;'>
						<label class='label'>Aucun acte en cours ou à venir avec ce propriétaire</label>
					</td>
				</tr>";
			}
		} Else {
		
			$rq_actes_saf = "
			SELECT DISTINCT 
				NULL::integer as session_af_id,
				site_id,
				site_nom,
				'Lots acquis' as lib,
				count(t3.lot_id) as nbr_lot,
				1 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN foncier.r_cad_site_mf USING (cad_site_id)
				JOIN foncier.mf_acquisitions USING (mf_id)
				JOIN foncier.r_mfstatut USING (mf_id)
				LEFT JOIN sites.sites USING (site_id),
				(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1				
			WHERE 
				r_mfstatut.date = tt1.statut_date 
				AND r_mfstatut.mf_id = tt1.mf_id 
				AND r_mfstatut.statutmf_id = '1'
				AND dnupro IN (".$rq_lst_dnupro.")
				AND t4.date_fin = 'N.D.'
				AND t3.date_fin = 'N.D.'
			GROUP BY
				site_id,
				site_nom

			UNION

			SELECT DISTINCT 
				NULL::integer as session_af_id,
				site_id,
				site_nom,
				'Lots conventionn&eacute;s' as lib,
				count(t3.lot_id) as nbr_lot,
				2 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN foncier.r_cad_site_mu USING (cad_site_id)
				JOIN sites.sites USING (site_id)
				JOIN foncier.mu_conventions USING (mu_id)
				JOIN foncier.r_mustatut USING (mu_id),
				(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
			WHERE 
				r_mustatut.date = tt2.statut_date 
				AND r_mustatut.mu_id = tt2.mu_id 
				AND r_mustatut.statutmu_id = '1'	
				AND dnupro IN (".$rq_lst_dnupro.")
				AND t4.date_fin = 'N.D.'
				AND t3.date_fin = 'N.D.'
			GROUP BY
				site_id,
				site_nom

			UNION

			SELECT DISTINCT
				session_af_id,	
				site_id,
				site_nom,
				CASE WHEN tnegoprix.nbr_all_nego = nbr_interloc_vp THEN
					tbl_nego[1]
				END as lib,
				count(t2.lot_id) as nbr_lot,
				3 as ordre	
			FROM
				cadastre.lots_cen t2
				JOIN cadastre.parcelles_cen USING (par_id)
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN sites.sites USING (site_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN (
					SELECT
						rel_saf_foncier_id,
						count(interloc_id) as nbr_interloc_vp
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
					GROUP BY
						rel_saf_foncier_id
				) st1 USING (rel_saf_foncier_id)
				LEFT JOIN (
					SELECT DISTINCT
						rel_saf_foncier_id, 
						array_agg(DISTINCT nego_af_lib) as tbl_nego,
						array_length(array_agg(DISTINCT nego_af_id), 1) as nbr_nego,
						array_length(array_agg(nego_af_id), 1) as nbr_all_nego
					FROM 
						animation_fonciere.rel_af_echanges
						JOIN animation_fonciere.d_nego_af USING (nego_af_id)
						JOIN animation_fonciere.echanges_af USING (echange_af_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN (
							SELECT
							rel_saf_foncier_interlocs_id,
							max(echange_af_date) as max_date
							FROM 
							animation_fonciere.rel_af_echanges
							JOIN animation_fonciere.echanges_af USING (echange_af_id)
							WHERE 
							nego_af_id IS NOT NULL
							GROUP BY
							rel_saf_foncier_interlocs_id
							) st2 ON (echanges_af.echange_af_date = st2.max_date AND rel_af_echanges.rel_saf_foncier_interlocs_id = st2.rel_saf_foncier_interlocs_id)
							WHERE 
							nego_af_id IS NOT NULL
					GROUP BY
						rel_saf_foncier_id
						) tnegoprix USING (rel_saf_foncier_id)
			WHERE 
				entite_af_id = :entite_af_id 
				AND	session_af_id != :session_af_id
				AND t4.date_fin = 'N.D.'
				AND t3.date_fin = 'N.D.'
			GROUP BY
				session_af_id,
				nbr_all_nego,
				nbr_interloc_vp,
				tbl_nego,
				site_id,
				site_nom
			HAVING 
				tbl_nego[1] IS NOT NULL AND 
				tnegoprix.nbr_all_nego = nbr_interloc_vp
			ORDER BY 
				site_id,
				ordre,
				lib";		
			// echo $rq_actes_saf;
			$res_actes_saf = $bdd->prepare($rq_actes_saf);
			$res_actes_saf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
			$actes_saf = $res_actes_saf->fetchAll();
			If ($res_actes_saf->rowCount() > 0) {
				$result['code'] .= "
				<tr>
					<td class='bilan-af' style='text-align:left;'>
						<label class='label'>Bilan des actes en cours et à venir avec cette EAF :</label>";
					For ($i=0; $i<$res_actes_saf->rowCount(); $i++) {
						If ($actes_saf[$i]['nbr_lot'] > 0) {				
							$result['code'] .= "			
							<li style='margin-left:25px;'>
								<label class='label'>".$actes_saf[$i]['lib']." pour ".$actes_saf[$i]['nbr_lot'] ." lot(s) sur le site ".$actes_saf[$i]['site_nom']. " (".$actes_saf[$i]['site_id'].")</label>
							</li>";
						} 
					}			
				$result['code'] .= "					
					</td>
				</tr>";
			} Else {
				$result['code'] .= "
				<tr>
					<td class='bilan-af' style='text-align:left;'>
						<label class='label'>Aucun acte en cours ou à venir avec cette EAF</label>
					</td>
				</tr>";
			}
		}
		
		If ($res_tbl_infos_proprios_eaf->rowCount() == 1) {
			$rq_other_eaf_proprio = "
			SELECT DISTINCT
				session_af_id,
				session_af_lib,
				array_to_string(array_agg(DISTINCT 'site_id:'||site_id||';site_nom:'||site_nom||';'), ',') as lst_site_infos,
				array_to_string(array_agg(DISTINCT entite_af_id), ',') as lst_entite_af_id
			FROM 
				animation_fonciere.rel_eaf_foncier
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.session_af USING (session_af_id)
				JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
				JOIN foncier.cadastre_site t4 USING (cad_site_id)
				JOIN sites.sites USING (site_id)
			WHERE
				interloc_id = :interloc_id
				AND entite_af_id || '_' || session_af_id != :entite_af_id_session_af_id
				AND t4.date_fin = 'N.D.'
			GROUP BY
				session_af_id,
				session_af_lib";
			$res_other_eaf_proprio = $bdd->prepare($rq_other_eaf_proprio);
			// echo $rq_other_eaf_proprio;
			$res_other_eaf_proprio->execute(array('interloc_id'=>$tbl_infos_proprios_eaf[0]['interloc_id'], 'entite_af_id_session_af_id'=>$entite_af_id.'_'.$session_af_id));
			// print_r(array('interloc_id'=>$tbl_infos_proprios_eaf[0]['interloc_id'], 'entite_af_id_session_af_id'=>$entite_af_id.'_'.$session_af_id));
			$other_eaf_proprio = $res_other_eaf_proprio->fetchAll();
			
			If ($res_other_eaf_proprio->rowCount() > 0) {				
				$result['code'] .= "
				<tr>
					<td class='bilan-af' style='text-align:left;'>
						<label class='label'>Le Propriétaire de cette EAF est présent sur :</label>";				
				For ($i=0; $i<$res_other_eaf_proprio->rowCount(); $i++) {
					$vget = "mode:consult;session_af_id:".$other_eaf_proprio[$i]['session_af_id'].";session_af_lib:".$other_eaf_proprio[$i]['session_af_lib'].";";
					$vget_crypt = base64_encode($vget);
					$lien_saf = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_saf.php?d=$vget_crypt#general";
					If ($session_af_id == $other_eaf_proprio[$i]['session_af_id']) {
						$result['code'] .= "			
						<li style='margin-left:25px;'>
							<label class='label'>".count(explode(',', $other_eaf_proprio[$i]['lst_entite_af_id'])) ." autre(s) EAF sur cette SAF</label>
						</li>";
					} Else {
						$result['code'] .= "			
						<li style='margin-left:25px;'>
							<label class='label'>".count(explode(',', $other_eaf_proprio[$i]['lst_entite_af_id'])) ." EAF sur la session : ".$other_eaf_proprio[$i]['session_af_lib']."</label>
						</li>";
					}
				}
				$result['code'] .= "
					</td>
				</tr>";
			} Else {
				$result['code'] .= "
				<tr>
					<td class='bilan-af' style='text-align:left;'>
						<label class='label'>Ce Propriétaire n'est présent sur aucune autre EAF</label>
					</td>
				</tr>";
			}			
		}
		
		$rq_other_lots_eaf = "
		SELECT DISTINCT
			site_id,
			site_nom,
			array_to_string(array_agg(DISTINCT 'session_af_id:'||session_af_id||';session_af_lib:'||session_af_lib||';'), ',') as lst_saf_infos,
			count(cad_site_id) as nbr_lots
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.session_af USING (session_af_id)
		WHERE
			entite_af_id = :entite_af_id
			AND t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'
			AND	cad_site_id NOT IN (
				SELECT 
					cad_site_id
				FROM 
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				WHERE 
					entite_af_id = :entite_af_id
					AND session_af_id = :session_af_id)
		GROUP BY
			site_id,
			site_nom
		
		UNION
		
		SELECT DISTINCT
			site_id,
			site_nom,
			'HORS SAF' as lst_saf_infos,
			count(cad_site_id) as nbr_lots
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
		WHERE
			dnupro IN (".$rq_lst_dnupro.") 
			AND t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'
			AND	cad_site_id NOT IN (
				SELECT 
					cad_site_id
				FROM 
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				WHERE 
					entite_af_id = :entite_af_id
					AND session_af_id = :session_af_id)
		GROUP BY
			site_id,
			site_nom";
		$res_other_lots_eaf = $bdd->prepare($rq_other_lots_eaf);
		// echo $rq_other_lots_eaf;
		// print_r(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$res_other_lots_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$other_lots_eaf = $res_other_lots_eaf->fetchall();
		If ($res_other_lots_eaf->rowCount() > 0) {				
			$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Cette EAF intègre des parcelles sur les sites suivants :</label>";
			For ($i=0; $i<$res_other_lots_eaf->rowCount(); $i++) {
				$lst_site[] = "'".$other_lots_eaf[$i]['site_id']."'";
				$vget = "site_nom:".$other_lots_eaf[$i]['site_nom'].";site_id:".$other_lots_eaf[$i]['site_id'].";";
				$vget_crypt = base64_encode($vget);
				$lien_site = ROOT_PATH . "foncier/fiche_site.php?d=$vget_crypt#general";
				$result['code'] .= "			
				<li style='margin-left:25px;'>
					<label class='label'>".$other_lots_eaf[$i]['nbr_lots']. " lots ";
					If ($other_lots_eaf[$i]['lst_saf_infos'] == 'HORS SAF') {
						$result['code'] .= " hors SAF";
					} Else {
						$result['code'] .= " sur d'autres SAFs";
					}
				$result['code'] .= "	
					sur ".$other_lots_eaf[$i]['site_nom']." (".$other_lots_eaf[$i]['site_id'].")</label>
					<a href='".$lien_site."'>
						<img style='margin:-5px 0;' src='".ROOT_PATH."images/fiche_site.png' width='15px' onmouseover='tooltip.show(this)' onmouseout='tooltip.hide(this)' title='Fiche site'>
					</a>";				
				$result['code'] .= "	
				</li>";
			}
			$result['code'] .= "					
					</td>
				</tr>
			";
		}
		
		If ($res_tbl_infos_proprios_eaf->rowCount() == 1) {
			$rq_other_lots_proprio = "
			SELECT DISTINCT
				site_id,
				site_nom,
				array_to_string(array_agg(DISTINCT 'session_af_id:'||session_af_id||';session_af_lib:'||session_af_lib||';'), ',') as lst_saf_infos,
				count(cad_site_id) as nbr_lots
			FROM
				sites.sites
				JOIN foncier.cadastre_site t4 USING (site_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
				JOIN animation_fonciere.session_af USING (session_af_id)
			WHERE
				interloc_id = :interloc_id
				AND t4.date_fin = 'N.D.'
				AND	cad_site_id NOT IN (
					SELECT 
						cad_site_id
					FROM 
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					WHERE 
						interloc_id = :interloc_id
						AND session_af_id = :session_af_id)
			GROUP BY
				site_id,
				site_nom

			UNION

			SELECT DISTINCT
				site_id,
				site_nom,
				'HORS SAF' as lst_saf_infos,
				count(cad_site_id) as nbr_lots
			FROM
				sites.sites
				JOIN foncier.cadastre_site t4 USING (site_id)
				JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
				JOIN cadastre.cptprop_cen USING (dnupro)
				JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
			WHERE
				dnuper IN (SELECT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = :interloc_id) 
				AND t4.date_fin = 'N.D.'
				AND t3.date_fin = 'N.D.'
				AND	cad_site_id NOT IN (
					SELECT 
						cad_site_id
					FROM 
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
					WHERE 
						interloc_id = :interloc_id
						AND session_af_id = :session_af_id)
			GROUP BY
				site_id,
				site_nom
			ORDER BY 
				site_id,
				lst_saf_infos";
			$res_other_lots_proprio = $bdd->prepare($rq_other_lots_proprio);
			$res_other_lots_proprio->execute(array('session_af_id'=>$session_af_id, 'interloc_id'=>$tbl_infos_proprios_eaf[0]['interloc_id']));
			$other_lots_proprio = $res_other_lots_proprio->fetchall();
			If ($res_other_lots_proprio->rowCount() > 0) {				
				$result['code'] .= "
				<tr>
					<td class='bilan-af' style='text-align:left;'>
						<label class='label'>Ce propriétaire possède également des parcelles sur les sites suivants :</label>";
				For ($i=0; $i<$res_other_lots_proprio->rowCount(); $i++) {
					$lst_site[] = "'".$other_lots_proprio[$i]['site_id']."'";
					$vget = "site_nom:".$other_lots_proprio[$i]['site_nom'].";site_id:".$other_lots_proprio[$i]['site_id'].";";
					$vget_crypt = base64_encode($vget);
					$lien_site = ROOT_PATH . "foncier/fiche_site.php?d=$vget_crypt#general";
					$result['code'] .= "			
					<li style='margin-left:25px;'>
						<label class='label'>".$other_lots_proprio[$i]['nbr_lots']. " lots ";
						If ($other_lots_proprio[$i]['lst_saf_infos'] == 'HORS SAF') {
							$result['code'] .= " hors SAF";
						} Else {
							$result['code'] .= " sur d'autres SAFs";
						}
					$result['code'] .= "	
						sur ".$other_lots_proprio[$i]['site_nom']." (".$other_lots_proprio[$i]['site_id'].")</label>
						<a href='".$lien_site."'>
							<img style='margin:-5px 0;' src='".ROOT_PATH."images/fiche_site.png' width='15px' onmouseover='tooltip.show(this)' onmouseout='tooltip.hide(this)' title='Fiche site'>
						</a>";				
					$result['code'] .= "	
					</li>";
				}
				$result['code'] .= "					
						</td>
					</tr>
				";
			}
		}
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_showInfosProprio($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$session_af_id = $data['params']['var_session_af_id'];
		$entite_af_id = $data['params']['var_entite_af_id'];		
		$interloc_id = $data['params']['var_interloc_id'];		
		$interloc_lib = $data['params']['var_interloc_lib'];		
		$result['lbl_title_infos_eaf'] = "Informations sur $interloc_lib";
		$result['code'] = '';
		
		$rq_other_eaf_proprio = "
		SELECT DISTINCT
			session_af_id,
			session_af_lib,
			array_to_string(array_agg(DISTINCT 'site_id:'||site_id||';site_nom:'||site_nom||';'), ',') as lst_site_infos,
			array_to_string(array_agg(DISTINCT entite_af_id), ',') as lst_entite_af_id
		FROM 
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.session_af USING (session_af_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
			JOIN foncier.cadastre_site t4 USING (cad_site_id)
			JOIN sites.sites USING (site_id)
		WHERE
			interloc_id = :interloc_id
			AND entite_af_id != :entite_af_id
			AND t4.date_fin = 'N.D.'
		GROUP BY
			session_af_id,
			session_af_lib";
		$res_other_eaf_proprio = $bdd->prepare($rq_other_eaf_proprio);
		$res_other_eaf_proprio->execute(array('interloc_id'=>$interloc_id, 'entite_af_id'=>$entite_af_id));
		$other_eaf_proprio = $res_other_eaf_proprio->fetchAll();
		
		If ($res_other_eaf_proprio->rowCount() > 0) {				
			$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Ce propriétaire est présent sur :</label>";				
			For ($i=0; $i<$res_other_eaf_proprio->rowCount(); $i++) {
				$vget = "mode:consult;session_af_id:".$other_eaf_proprio[$i]['session_af_id'].";session_af_lib:".$other_eaf_proprio[$i]['session_af_lib'].";";
				$vget_crypt = base64_encode($vget);
				$lien_saf = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_saf.php?d=$vget_crypt#general";
				If ($session_af_id == $other_eaf_proprio[$i]['session_af_id']) {
					$result['code'] .= "			
					<li style='margin-left:25px;'>
						<label class='label'>".count(explode(',', $other_eaf_proprio[$i]['lst_entite_af_id'])) ." autre(s) EAF sur cette SAF</label>
					</li>";
				} Else {
					$result['code'] .= "			
					<li style='margin-left:25px;'>
						<label class='label'>".count(explode(',', $other_eaf_proprio[$i]['lst_entite_af_id'])) ." autre(s) EAF sur la session : ".$other_eaf_proprio[$i]['session_af_lib']."</label>
					</li>";
				}
			}
			$result['code'] .= "
				</td>
			</tr>";
		} Else {
			$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Ce Propriétaire n'est présent sur aucune autre EAF</label>
				</td>
			</tr>";
		}
		
		$rq_sites_eaf = "
		SELECT DISTINCT
			site_id,
			site_nom
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
		WHERE
			entite_af_id = $entite_af_id
			AND t4.date_fin = 'N.D.'
		
		UNION
		
		SELECT DISTINCT
			site_id,
			site_nom
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.cptprop_cen USING (dnupro)
			JOIN cadastre.r_prop_cptprop_cen USING (dnupro)			
		WHERE
			dnuper IN (SELECT DISTINCT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = $interloc_id)
			AND t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'";
		$res_sites_eaf = $bdd->prepare($rq_sites_eaf);
		// echo $rq_sites_eaf;
		$res_sites_eaf->execute();
		$sites_eaf = $res_sites_eaf->fetchall();
		$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Ce propriétaire est présent sur les sites suivants :</label>
		";
		For ($i=0; $i<$res_sites_eaf->rowCount(); $i++) {
			$lst_site[] = "'".$sites_eaf[$i]['site_id']."'";
			$vget = "site_nom:".$sites_eaf[$i]['site_nom'].";site_id:".$sites_eaf[$i]['site_id'].";";
			$vget_crypt = base64_encode($vget);
			$lien_site = ROOT_PATH . "foncier/fiche_site.php?d=$vget_crypt#general";
			$result['code'] .= "			
			<li style='margin-left:25px;'>
				<label class='label'>".$sites_eaf[$i]['site_nom']." (".$sites_eaf[$i]['site_id'].")</label>
				<a href='".$lien_site."'>
					<img style='margin:-5px 0;' src='".ROOT_PATH."images/fiche_site.png' width='15px' onmouseover='tooltip.show(this)' onmouseout='tooltip.hide(this)' title='Fiche site'>
				</a>
			</li>";
		}
		$result['code'] .= "					
				</td>
			</tr>
		";
		
		$rq_actes_saf = "
		SELECT DISTINCT 
			NULL::integer as session_af_id,
			site_id,
			site_nom,
			'Lots acquis' as lib,
			count(t3.lot_id) as nbr_lot,
			1 as ordre
		FROM 
			cadastre.cadastre_cen t3
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN cadastre.cptprop_cen USING (dnupro)
			JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
			JOIN foncier.r_cad_site_mf USING (cad_site_id)
			JOIN foncier.mf_acquisitions USING (mf_id)
			JOIN foncier.r_mfstatut USING (mf_id)
			LEFT JOIN sites.sites USING (site_id),
			(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
			
		WHERE 
			r_mfstatut.date = tt1.statut_date 
			AND r_mfstatut.mf_id = tt1.mf_id 
			AND r_mfstatut.statutmf_id = '1'
			AND dnuper IN (SELECT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = :interloc_id)
			AND t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'
		GROUP BY
			site_id,
			site_nom

		UNION

		SELECT DISTINCT 
			NULL::integer as session_af_id,
			site_id,
			site_nom,
			'Lots conventionn&eacute;s' as lib,
			count(t3.lot_id) as nbr_lot,
			2 as ordre
		FROM 
			cadastre.cadastre_cen t3
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN foncier.r_cad_site_mu USING (cad_site_id)	
			JOIN cadastre.cptprop_cen USING (dnupro)
			JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
			JOIN sites.sites USING (site_id)
			JOIN foncier.mu_conventions USING (mu_id)
			JOIN foncier.r_mustatut USING (mu_id),
			(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut
		.mu_id) as tt2
		WHERE 
			r_mustatut.date = tt2.statut_date 
			AND r_mustatut.mu_id = tt2.mu_id 
			AND r_mustatut.statutmu_id = '1'	
			AND dnuper IN (SELECT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = :interloc_id)
			AND t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'
		GROUP BY
			site_id,
			site_nom

		UNION

		SELECT DISTINCT
			session_af_id,	
			site_id,
			site_nom,
			CASE WHEN tnegoprix.nbr_all_nego = nbr_interloc_vp THEN
				tbl_nego[1]
			ELSE
				tbl_nego[1] || ' (A confirmer)'
			END as lib,
			count(t2.lot_id) as nbr_lot,
			3 as ordre	
		FROM
			cadastre.lots_cen t2
			JOIN cadastre.parcelles_cen USING (par_id)
			JOIN cadastre.cadastre_cen t3 USING (lot_id)
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN sites.sites USING (site_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
			JOIN (
				SELECT
					rel_saf_foncier_id,
					count(interloc_id) as nbr_interloc_vp
				FROM
					animation_fonciere.rel_saf_foncier_interlocs
				GROUP BY
					rel_saf_foncier_id
			) st1 USING (rel_saf_foncier_id)
			LEFT JOIN (
				SELECT DISTINCT
					rel_saf_foncier_id, 
					array_agg(DISTINCT nego_af_lib) as tbl_nego,
					array_length(array_agg(DISTINCT nego_af_id), 1) as nbr_nego,
					array_length(array_agg(nego_af_id), 1) as nbr_all_nego
				FROM 
					animation_fonciere.rel_af_echanges
					JOIN animation_fonciere.d_nego_af USING (nego_af_id)
					JOIN animation_fonciere.echanges_af USING (echange_af_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_interlocs_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
					JOIN (
						SELECT
						rel_saf_foncier_interlocs_id,
						max(echange_af_date) as max_date
						FROM 
						animation_fonciere.rel_af_echanges
						JOIN animation_fonciere.echanges_af USING (echange_af_id)
						WHERE 
						nego_af_id IS NOT NULL
						GROUP BY
						rel_saf_foncier_interlocs_id
						) st2 ON (echanges_af.echange_af_date = st2.max_date AND rel_af_echanges.rel_saf_foncier_interlocs_id
		= st2.rel_saf_foncier_interlocs_id)
						WHERE 
						nego_af_id IS NOT NULL
						AND interloc_id = :interloc_id
				GROUP BY
					rel_saf_foncier_id
					) tnegoprix USING (rel_saf_foncier_id)
		WHERE 
			interloc_id = :interloc_id
			AND t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'
		GROUP BY
			session_af_id,
			nbr_all_nego,
			nbr_interloc_vp,
			tbl_nego,
			site_id,
			site_nom
		HAVING 
			tbl_nego[1] IS NOT NULL
		ORDER BY 
			site_id,
			ordre,
			lib";		
		// echo $rq_actes_saf;
		$res_actes_saf = $bdd->prepare($rq_actes_saf);
		$res_actes_saf->execute(array('interloc_id'=>$interloc_id));
		$actes_saf = $res_actes_saf->fetchAll();
		If ($res_actes_saf->rowCount() > 0) {
			$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Bilan des actes en cours et à venir avec ce propriétaire :</label>";
				For ($i=0; $i<$res_actes_saf->rowCount(); $i++) {
					If ($actes_saf[$i]['nbr_lot'] > 0) {				
						$result['code'] .= "			
						<li style='margin-left:25px;'>
							<label class='label'>".$actes_saf[$i]['lib']." pour ".$actes_saf[$i]['nbr_lot'] ." lot(s) sur le site ".$actes_saf[$i]['site_nom']. " (".$actes_saf[$i]['site_id'].")</label>
						</li>";
					} 
				}			
			$result['code'] .= "					
				</td>
			</tr>";
		} Else {
			$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Aucun acte en cours ou à venir avec ce propriétaire</label>
				</td>
			</tr>";
		}
		
		$rq_other_lots_proprio = "
		SELECT DISTINCT
			site_id,
			site_nom,
			array_to_string(array_agg(DISTINCT 'session_af_id:'||session_af_id||';session_af_lib:'||session_af_lib||';'), ',') as lst_saf_infos,
			count(cad_site_id) as nbr_lots
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
			JOIN animation_fonciere.session_af USING (session_af_id)
		WHERE
			interloc_id = :interloc_id
			AND t4.date_fin = 'N.D.'
			AND cad_site_id NOT IN (
				SELECT 
					cad_site_id
				FROM 
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
				WHERE 
					interloc_id = :interloc_id
					AND session_af_id = :session_af_id)
		GROUP BY
			site_id,
			site_nom

		UNION

		SELECT DISTINCT
			site_id,
			site_nom,
			'HORS SAF' as lst_saf_infos,
			count(cad_site_id) as nbr_lots
		FROM
			sites.sites
			JOIN foncier.cadastre_site t4 USING (site_id)
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.cptprop_cen USING (dnupro)
			JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
		WHERE
			dnuper IN (SELECT dnuper FROM animation_fonciere.rel_proprio_interloc WHERE interloc_id = :interloc_id)
			AND	t4.date_fin = 'N.D.'
			AND t3.date_fin = 'N.D.'
			AND cad_site_id NOT IN (
				SELECT 
					cad_site_id
				FROM 
					animation_fonciere.rel_eaf_foncier
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
				WHERE 
					interloc_id = :interloc_id
					AND session_af_id = :session_af_id)
		GROUP BY
			site_id,
			site_nom
		ORDER BY 
			site_id,
			lst_saf_infos";
		$res_other_lots_proprio = $bdd->prepare($rq_other_lots_proprio);
		$res_other_lots_proprio->execute(array('session_af_id'=>$session_af_id, 'interloc_id'=>$interloc_id));
		$other_lots_proprio = $res_other_lots_proprio->fetchall();
		If ($res_other_lots_proprio->rowCount() > 0) {				
			$result['code'] .= "
			<tr>
				<td class='bilan-af' style='text-align:left;'>
					<label class='label'>Ce propriétaire possède également des parcelles sur les sites suivants :</label>";
			For ($i=0; $i<$res_other_lots_proprio->rowCount(); $i++) {
				$lst_site[] = "'".$other_lots_proprio[$i]['site_id']."'";
				$vget = "site_nom:".$other_lots_proprio[$i]['site_nom'].";site_id:".$other_lots_proprio[$i]['site_id'].";";
				$vget_crypt = base64_encode($vget);
				$lien_site = ROOT_PATH . "foncier/fiche_site.php?d=$vget_crypt#general";
				$result['code'] .= "			
				<li style='margin-left:25px;'>
					<label class='label'>".$other_lots_proprio[$i]['nbr_lots']. " lots ";
					If ($other_lots_proprio[$i]['lst_saf_infos'] == 'HORS SAF') {
						$result['code'] .= " hors SAF";
					} Else {
						$result['code'] .= " sur d'autres SAFs";
					}
				$result['code'] .= "	
					sur ".$other_lots_proprio[$i]['site_nom']." (".$other_lots_proprio[$i]['site_id'].")</label>
					<a href='".$lien_site."'>
						<img style='margin:-5px 0;' src='".ROOT_PATH."images/fiche_site.png' width='15px' onmouseover='tooltip.show(this)' onmouseout='tooltip.hide(this)' title='Fiche site'>
					</a>";				
				$result['code'] .= "	
				</li>";
			}
			$result['code'] .= "					
					</td>
				</tr>
			";
		}
		
		$bdd = null;
		print json_encode($result);
	}
	
	function fct_updateLot($data) {
		include('../foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		include('af_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$id = $data['params']['var_id'];
		$value = $data['params']['var_value'];		
		$champ = substr($id, 0, strpos($id, '-')); //On récupère l'interloc_id depuis la clé vue précédement
		$champ_id = substr($id, strpos($id, '-')+1); //On récupère l'interloc_id depuis la clé vue précédement
		switch ($champ) {
			case 'commentaires_eaf':
				$rq_updateLot = "UPDATE animation_fonciere.entite_af SET commentaires = '".str_replace("'", "''", $value)."' WHERE entite_af_id = $champ_id";
				break;
			case 'montant_eaf':
				$rq_updateLot = "
				UPDATE animation_fonciere.rel_saf_foncier SET prix_eaf_arrondi = $value
				WHERE rel_eaf_foncier_id IN (
					SELECT 
						rel_eaf_foncier_id
					FROM
						animation_fonciere.rel_eaf_foncier
					WHERE
						entite_af_id = $champ_id)
				";
				break;
			case 'occsol':
				$rq_delete = "DELETE FROM animation_fonciere.rel_saf_foncier_occsol WHERE rel_saf_foncier_id = $champ_id";
				$res_delete = $bdd->prepare($rq_delete);
				$res_delete->execute();
				$res_delete->closeCursor();
				
				$rq_updateLot = "INSERT INTO animation_fonciere.rel_saf_foncier_occsol (rel_saf_foncier_id, type_occsol_af_id, pourcentage, montant) VALUES ";
				$lst_occsol = explode(';', $value);
				
				For ($a=0; $a<count($lst_occsol)-1; $a++) {
					$lst_occsol_info = explode(':', $lst_occsol[$a]);
					$rq_updateLot .= "($champ_id, ".$lst_occsol_info[0].", ".$lst_occsol_info[1].", ".$lst_occsol_info[2]."),";
				}
				$rq_updateLot = substr($rq_updateLot, 0, strlen($rq_updateLot)-1).';'; //suppression de la dernière virgule
				break;
			case 'prix':
				$rq_delete_prix_detail = "DELETE FROM animation_fonciere.rel_saf_foncier_prix WHERE rel_saf_foncier_id = $champ_id";
				$res_delete_prix_detail = $bdd->prepare($rq_delete_prix_detail);
				$res_delete_prix_detail->execute();
				$res_delete_prix_detail->closeCursor();
				
				$rq_updateLot = "INSERT INTO animation_fonciere.rel_saf_foncier_prix (rel_saf_foncier_id, prix_lib, montant, quantite) VALUES ";
				$lst_prix = explode(';', $value);
				
				For ($a=0; $a<count($lst_prix)-1; $a++) {
					$lst_prix_info = explode(':', $lst_prix[$a]);
					If ($lst_prix_info[0] == '*$*parc_arrondi_prix*$*' && $lst_prix_info[2] == '*$*parc_arrondi_prix*$*') {
						$rq_updatePrix_parc_arrondi = "UPDATE animation_fonciere.rel_saf_foncier SET prix_parc_arrondi = ".$lst_prix_info[1]." WHERE rel_saf_foncier_id = $champ_id";
						$res_updatePrix_parc_arrondi = $bdd->prepare($rq_updatePrix_parc_arrondi);
						// echo $rq_updatePrix_parc_arrondi;
						$res_updatePrix_parc_arrondi->execute();
						$res_updatePrix_parc_arrondi->closeCursor();
					} Else {
						$rq_updateLot .= "($champ_id, '".str_replace("'", "''", $lst_prix_info[0])."', ".$lst_prix_info[1].", ".$lst_prix_info[2]."),";
					}
				}
				$rq_updateLot = substr($rq_updateLot, 0, strlen($rq_updateLot)-1).';'; //suppression de la dernière virgule
				break;	
			case 'utilissol':
				$rq_updateLot = "
				UPDATE animation_fonciere.rel_eaf_foncier SET utilissol = '".str_replace("'", "''", $value)."'
				WHERE rel_eaf_foncier_id IN (
					SELECT 
						rel_eaf_foncier_id
					FROM
						animation_fonciere.rel_eaf_foncier
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
					WHERE
						rel_saf_foncier_id = $champ_id)
				";
				break;
			case 'commentaires':
				$rq_updateLot = "
				UPDATE animation_fonciere.rel_saf_foncier SET commentaires = '".str_replace("'", "''", $value)."' WHERE rel_saf_foncier_id = $champ_id";
				break;
			case 'paf':
				$rq_updateLot = "
				UPDATE animation_fonciere.rel_saf_foncier SET paf_id = '$value' WHERE rel_saf_foncier_id = $champ_id";
				break;
			case 'obj':
				$rq_updateLot = "
				UPDATE animation_fonciere.rel_saf_foncier SET objectif_af_id = '$value' WHERE rel_saf_foncier_id = $champ_id";
				
				break;
			case 'lot_bloque':
				$rq_updateLot = "
				UPDATE animation_fonciere.rel_saf_foncier SET lot_bloque = '$value' WHERE rel_saf_foncier_id = $champ_id";
				break;	
			default:
				$rq_updateLot = "";
		}
		$res_update = $bdd->prepare($rq_updateLot);
		// echo $rq_updateLot;
		$res_update->execute();
		$res_update->closeCursor();
		
		$rq_infos_parc = "
		SELECT 
			session_af_id,
			entite_af_id,
			count(rel_af_echanges.echange_af_id) as nbr_echanges,
			prix_parc_arrondi,
			prix_fond_occsol			
		FROM
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
			LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)
			LEFT JOIN 
				(
					SELECT 
						rel_saf_foncier_id,
						CASE WHEN sum(pourcentage) = 100 THEN
							CASE WHEN rel_saf_foncier.geom IS NULL THEN 
								sum(((rel_saf_foncier_occsol.pourcentage*dcntlo)/100)*rel_saf_foncier_occsol.montant)
							ELSE 
								sum(((rel_saf_foncier_occsol.pourcentage*st_area(rel_saf_foncier.geom))/100)*rel_saf_foncier_occsol.montant)
							END					
						ELSE
							-1
						END as prix_fond_occsol
					FROM
						cadastre.lots_cen t2
						JOIN cadastre.cadastre_cen t3 USING (lot_id)
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_occsol USING (rel_saf_foncier_id)
					WHERE 
						rel_saf_foncier_id = :rel_saf_foncier_id
					GROUP BY 
						rel_saf_foncier_id
				) as t_occsol USING (rel_saf_foncier_id)
		WHERE 
			rel_saf_foncier_id = :rel_saf_foncier_id		
		GROUP BY 
			session_af_id,
			entite_af_id,
			prix_parc_arrondi,
			rel_saf_foncier.geom,
			prix_fond_occsol";
		$res_infos_parc = $bdd->prepare($rq_infos_parc);
		// echo $rq_updateLot;
		$res_infos_parc->execute(array('rel_saf_foncier_id'=>$champ_id));
		$infos_parc = $res_infos_parc->fetch();
		If ($champ == 'occsol') {
			$result['tbl_parc'] = fct_rq_occsol_parc($champ_id, $infos_parc['session_af_id'], $infos_parc['entite_af_id'], $infos_parc['nbr_echanges']);
			$result['prix_total_occsol'] = number_format($infos_parc['prix_fond_occsol'],2, '.', ' ');			
		} Else If ($champ == 'obj') {
			$result['td_prix_eaf'] = "<label class='label'><i>Prix d'acquisition de l'EAF => </i></label>";
			$result['td_prix_eaf'] .= fct_rq_prix_eaf($infos_parc['session_af_id'], $infos_parc['entite_af_id'], $infos_parc['nbr_echanges']);
		} Else If ($champ == 'prix') {
			$result['tbl_parc'] = "
			<tr>
				<th style='text-align:center;margin-left:10px;'>
					<label class='last_maj'>Libellé</label>
				</th>
				<th style='text-align:center;margin-left:10px;'>
					<label class='last_maj'>Montant</label>
				</th>
				<th style='text-align:center;margin-left:10px;'>
					<label class='last_maj' style='text-align:center;margin-left:10px;'>Quantité</label>
				</th>
			</tr>";
			$result['tbl_parc'] .= fct_rq_prix_parc($champ_id, $infos_parc['session_af_id'], $infos_parc['entite_af_id'], $infos_parc['nbr_echanges']);
			$result['tbl_parc'] .= "
			<tr>
				<td colspan='4' style='text-align:right;'>
					<label class='label'><u>Prix d'acquisition total opérateur :</u></label>
				</td>
				<td>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox parc_arrondi_prix' type='text' id='parc_arrondi_prix-".$champ_id."' size='8' value='".$infos_parc['prix_parc_arrondi']."' onkeyup=\"isUpdatable(this, 'prix', ".$champ_id.", ".$infos_parc['entite_af_id'].", ".$infos_parc['session_af_id'].", ".$infos_parc['nbr_echanges'].");\"><label class='label'>€</label>
				</td>
			</tr>";
			$result['td_prix_eaf'] = "<label class='label'><i>Prix d'acquisition de l'EAF => </i></label>";
			$result['td_prix_eaf'] .= fct_rq_prix_eaf($infos_parc['session_af_id'], $infos_parc['entite_af_id'], $infos_parc['nbr_echanges']);
		} Else {
			$result['tbl_parc'] = '';
		}
		
		$res_infos_parc->closeCursor();	
		
		$bdd = null;
		print json_encode($result);
	}
?>