<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/explorateur.css">		
		<link rel="stylesheet" type="text/css" href= "onglet_af.css">
		<link rel="stylesheet" type="text/css" href="af.css">
		<link rel="stylesheet" type="text/css" href="jquery-ui-af.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="tooltip-af.js"></script><div id="tooltip-af" class='tooltip_help' style='display:none;'></div>
		<script type="text/javascript" src="../foncier.js"></script>
		<script type="text/javascript" src="af_help.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script>
			$(document).ready( function () {
				setTimeout(function(){loadQuestionMark();showQuestionMark('general');}, 2000);				
				$('#glossaire_content').height($('#main').height() - 80);
			} ) ;
		</script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<div id='body2' style='width:100%;height:100%;position:fixed;display:none;z-index:20;' onclick="showHelp(this);"></div>		
		<?php 
			include ('../../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig', 'grp_foncier'));
			$h2 ='Foncier';
			include('../../includes/header.php');
			include('../../includes/breadcrumb.php');
			//include('../includes/construction.php');
			include('../foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
			include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure				
		?>
		
		<section id="main">
			<h3>Fiche session d'animation foncière : mode AIDE</h3>
			<img id="img_arrow" src="images/help/pointer.png" style="display:none;position:fixed;" width="20px">			
			<img id="img_help_general_1" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_general_temp' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Pour en savoir plus sur les événements">			
			<img id="img_help_interloc_1" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_interloc' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Pour en savoir plus sur les filtres">
			<img id="img_help_interloc_2" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_interloc' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Ce bouton permet de résoudre le problème des propriétaires du cadastre présents avec plusieurs identifiants">
			<img id="img_help_interloc_3" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_interloc' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Comment doit-on remplir une fiche<br>'<i>Ajouter un interlocuteur</i>' ?">
			<img id="img_help_interloc_4" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_interloc' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Les interlocuteurs sont organisés ? Oui, mais comment?">
			<img id="img_help_interloc_5" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_interloc' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Comment gérer les référents ?">
			<img id="img_help_interloc_6" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_interloc' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Comment gérer les interlocuteurs de substitution ?">
			<img id="img_tip_off_interloc_1" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_interloc' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Un m&ecirc;me interlocuteur peut être présent plusieurs fois sur une EAF mais avec des types différents">
			<img id="img_tip_on_interloc_1" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_interloc' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Un m&ecirc;me interlocuteur peut être présent plusieurs fois sur une EAF mais avec des types différents">
			<img id="img_tip_off_interloc_2" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_interloc'>
			<img id="img_tip_on_interloc_2" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_interloc' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lorsque aucun véritable propriétaire n'est identifié, l'EAF n'est pas présente dans les autres onglets.">
			
			<img id="img_help_parcelle_1" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_parcelle' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Comment saisir l'occupation du sol ?">
			<img id="img_help_parcelle_2" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_parcelle' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Comment saisir les prix d'acquisition ?">
			<img id="img_help_parcelle_3" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_parcelle' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="A quoi correspond ces informations ?">
			<img id="img_help_parcelle_4" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_parcelle' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Pour saisir les prix de toutes les occupations du sol en une fois">
			<img id="img_tip_off_parcelle_1" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Une parcelle supprim&eacute;e signifie qu'elle n'existe plus suite &agrave; la mise &agrave; jour du cadastre">
			<img id="img_tip_on_parcelle_1" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Une parcelle supprim&eacute;e signifie qu'elle n'existe plus suite &agrave; la mise &agrave; jour du cadastre">
			<img id="img_tip_off_parcelle_2" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="<div style='margin-top:10px;'><label>On bloque un lot lorsque, pour une raison ou pour une autre, aucune négociation n'est possible.</label></div><div style='margin-top:10px;'><label>Lorqsqu'un lot est bloqué :</label></div><div style='margin-left:20px;'><label>- On ne peut plus saisir d'information le concernant</label></div><div style='margin-left:20px;'><label>- Il apparait en grisé dans tous les onglets.</label></div><div style='margin-left:20px;'><label>- Aucune négociation ne peut être renseignée</label></div><div style='margin-left:20px;'><label>- Lorsque il est unique dans une EAF, aucun échange ne peut être saisi</label></div>">
			<img id="img_tip_on_parcelle_2" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="<div style='margin-top:10px;'><label>On bloque un lot lorsque, pour une raison ou pour une autre, aucune négociation n'est possible.</label></div><div style='margin-top:10px;'><label>Lorqsqu'un lot est bloqué :</label></div><div style='margin-left:20px;'><label>- On ne peut plus saisir d'information le concernant</label></div><div style='margin-left:20px;'><label>- Il apparait en grisé dans tous les onglets.</label></div><div style='margin-left:20px;'><label>- Aucune négociation ne peut être renseignée</label></div><div style='margin-left:20px;'><label>- Lorsque il est unique dans une EAF, aucun échange ne peut être saisi</label></div>">
			<img id="img_tip_off_parcelle_3" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="<div style='margin-top:10px;'><label>Etat actuel de la négociation.</label></div><div style='margin-top:10px;'><label>Le message varie en fonction des cas. On peut trouver entre autre :</label></div><div style='margin-left:20px;'><label>- Accord pour une acquisition à XXX €</label></div><div style='margin-left:20px;'><label>- Accord pour une acquisition mais le prix reste à clarifier</label></div><div style='margin-left:20px;'><label>- Certains propriétaires se sont mis d'accord mais ils ne se sont pas encore tous prononcés</label></div><div style='margin-left:20px;'><label>- Les propriétaires ne se sont pas encore mis d'accord</label></div>">
			<img id="img_tip_on_parcelle_3" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="<div style='margin-top:10px;'><label>Etat actuel de la négociation.</label></div><div style='margin-top:10px;'><label>Le message varie en fonction des cas. On peut trouver entre autre :</label></div><div style='margin-left:20px;'><label>- Accord pour une acquisition à XXX €</label></div><div style='margin-left:20px;'><label>- Accord pour une acquisition mais le prix reste à clarifier</label></div><div style='margin-left:20px;'><label>- Certains propriétaires se sont mis d'accord mais ils ne se sont pas encore tous prononcés</label></div><div style='margin-left:20px;'><label>- Les propriétaires ne se sont pas encore mis d'accord</label></div>">
			<img id="img_tip_off_parcelle_4" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="<div style='margin-top:10px;'><label>Ici, la convention ne concerne qu'une partie de la parcelle.</label></div><div style='margin-top:10px;'><label>Plusieurs remarques concernant les conventions pour partie :</label></div><div style='margin-left:20px;'><label>- Elles ne concernent que les parcelles constituées d'un seul lot. Donc les BND ne peuvent pas être conventionnés pour partie.</label></div><div style='margin-left:20px;'><label>- La surface initiale, lorsqu'elle est indiquée, précise que la surface à conventionner a évolué et renseigne sur la surface de départ souhaitée.</label></div><div style='margin-left:20px;'><label>- Pour saisir une convention pour partie, il faut éditer la vue (ou couche) <i>parcelles_saf_pour_partie</i> dans QGis et remodeler la parcelle.</label></div><div style='margin-left:20px;'></div>">
			<img id="img_tip_on_parcelle_4" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_parcelle' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="<div style='margin-top:10px;'><label>Ici, la convention ne concerne qu'une partie de la parcelle.</label></div><div style='margin-top:10px;'><label>Plusieurs remarques concernant les conventions pour partie :</label></div><div style='margin-left:20px;'><label>- Elles ne concernent que les parcelles constituées d'un seul lot. Donc les BND ne peuvent pas être conventionnés pour partie.</label></div><div style='margin-left:20px;'><label>- La surface initiale, lorsqu'elle est indiquée, précise que la surface à conventionner a évolué et renseigne sur la surface de départ souhaitée.</label></div><div style='margin-left:20px;'><label>- Pour saisir une convention pour partie, il faut éditer la vue (ou couche) <i>parcelles_saf_pour_partie</i> dans QGis et remodeler la parcelle.</label></div><div style='margin-left:20px;'></div>">
			
			
			<img id="img_help_echanges_1" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_echanges' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Comment saisir ou éditer un échange ? Zoom sur la partie négociation.">
			<img id="img_help_echanges_2" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_echanges' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Comment saisir ou éditer un échange ? Zoom sur l'option Réduire et sur les rappels.">
			<img id="img_tip_off_echanges_1" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_echanges' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cela parait évident, mais on ne peut pas saisir d'échange avec un interlocuteur décédé">
			<img id="img_tip_on_echanges_1" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_echanges' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cela parait évident, mais on ne peut pas saisir d'échange avec un interlocuteur décédé">
			<img id="img_tip_off_echanges_2" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_echanges' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="A l'exception du type d'interlocuteur : Représentant, lorsqu'un véritable propriétaire est représenté par un interlocuteur de substitution, on ne peut passer un échange qu'avec ce dernier.">
			<img id="img_tip_on_echanges_2" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_echanges' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="A l'exception du type d'interlocuteur : Représentant, lorsqu'un véritable propriétaire est représenté par un interlocuteur de substitution, on ne peut passer un échange qu'avec ce dernier.">
			<img id="img_tip_off_echanges_3" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_echanges' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cet échange ne peut pas être édité car il a été passé avec le référent. Pour le modifier il faut éditer celui passé avec le référent.">
			<img id="img_tip_on_echanges_3" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_echanges' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cet échange ne peut pas être édité car il a été passé avec le référent. Pour le modifier il faut éditer celui passé avec le référent.">
			
			<img id="img_help_synthese_1" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_synthese' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Au survol d'un interlocuteur : KIKADIKWOI ?">
			<img id="img_help_synthese_2" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_synthese' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Pour en savoir plus sur les pops-up d'information.">
			<img id="img_help_synthese_3" src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_synthese' onclick="showHelp(this);" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Pour en savoir plus sur le code couleur.">
			<img id="img_tip_off_synthese_1" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_synthese' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="S'il existe et commun pour tous les interlocuteurs, il s'agit du prix issu des négociations. Sinon, il peut s'agir du prix d'acquisition arrondi renseigné dans l'onglet <i>Parcelles/ Lots</i>.">
			<img id="img_tip_on_synthese_1" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_synthese' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="S'il existe et commun pour tous les interlocuteurs, il s'agit du prix issu des négociations. Sinon, il peut s'agir du prix d'acquisition arrondi renseigné dans l'onglet <i>Parcelles/ Lots</i>.">
			<img id="img_tip_off_synthese_2" src="images/help/tip_off.png" class='img_tip img_tip_off img_tip_synthese' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="S'il a été renseigné, il s'agit du <i>Prix d'acquisition de l'EAF en cours de négociation</i> saisi dans la section négociation d'un échange. Sinon, il peut s'agir du prix d'acquisition arrondi dans l'onglet <i>Parcelles/ Lots</i>.">
			<img id="img_tip_on_synthese_2" src="images/help/tip_on.png" class='img_tip img_tip_on img_tip_synthese' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="S'il a été renseigné, il s'agit du <i>Prix d'acquisition de l'EAF en cours de négociation</i> saisi dans la section négociation d'un échange. Sinon, il peut s'agir du prix d'acquisition arrondi dans l'onglet <i>Parcelles/ Lots</i>.">
			
			<div id='help_msg' style='display:none;position:absolute;padding:15px;background:rgba(190, 190, 190, 0.8);border:1px solid gray;z-index:50;'></div>
			<div id='help_evidence' style='display:none;border:2px dashed gray;position:absolute;z-index:100;border-radius:15px;'></div>
			<div id="onglet" class="list" style='opacity:0.3;'>				
				<img id='glossaire' src='images/help/glossaire.png' width='40px' style='position:absolute;right:40px;margin-left:10px;cursor:pointer;' onclick="showHelp(this);">
				<ul class="tabs-nav">
					<li class="">
						<a href="#general" onclick="showQuestionMark('general');"><span>Général</span></a>
					</li>
					<li class="">
						<a href="#interloc" onclick="showQuestionMark('interloc');"><span>Interlocuteurs</span></a>
					</li>
					<li class="">
						<a href="#parcelle" onclick="showQuestionMark('parcelle');"><span>Parcelles/ Lots</span></a>
					</li>
					<li class="">
						<a href="#echanges" onclick="showQuestionMark('echanges');"><span>Echanges</span></a>
					</li>
					<li class="tabs-selected">
						<a href="#synthese" onclick="showQuestionMark('synthese');"><span>Synthèse</span></a>
					</li>
				</ul>
				<div id='filigrane' class='filigrane_help'>
					<label>MODE AIDE</label>
				</div>
				<div id="div_11_10_move" class="draggable 4 anime_help" style='display:none;position:absolute;z-index:5;'>
					M. DUCK Donald<br><label class="last_maj">Curateur</label>
				</div>
				<div id="div_40_9_move" class="draggable 4 anime_help" style='display:none;position:absolute;z-index:5;'>
					Mme. POPPINS Mary<br><label class="last_maj">Mandataire spécial</label>
				</div>
				<div id="div_11_move" class="draggable 4 anime_help" style='display:none;position:absolute;z-index:5;'>
					<input class="checkRef" style="float:left;" type="checkbox">
					M. DUCK Donald<br><label class="last_maj">Curateur</label>								
				</div>
				<div id="div_30_move" class="draggable 4 anime_help" style='display:none;position:absolute;z-index:5;'>
					<input class="checkRef" style="float:left;" type="checkbox">
					M. FUDD Elmer<br><label class="last_maj">Chasseur (bail de chasse)	</label>								
				</div>
				<div id="div_39_move" class="draggable 4 anime_help" style='display:none;position:absolute;z-index:5;'>
					<input id='chk_39_move' class="checkRef" style="float:left;" type="checkbox">
					Mme. DUCK Daisy<br><label class="last_maj">Véritable propriétaire</label>								
				</div>
				
				<div id="general" class="tabs-container tabs-hide" style="min-width: 0px;">					
					<table style="border:0px solid black" width="100%" cellspacing="5" cellpadding="5">
						<tbody>
							<tr id='general_referent'>
								<td style="text-align:right" colspan="3">
									<label class="last_maj"><u>Référent foncier</u> : Gandalf</label>
								</td>
							</tr>
							<tr id='general_resume'>
								<td style="vertical-align:top">
									<table class="no-border" cellspacing="5" cellpadding="5">
										<tbody>
											<tr>
												<td class="bilan-af" style="text-align:right;">
													<label class="label">
														<u>Site :</u>
													</label>
												</td>
												<td class="bilan-af">
													<li>
														<label class="label">Les Champs du Pelennor (CHPE)</label>
														<img style="margin:-5px 0;" src="<?php echo ROOT_PATH; ?>images/fiche_site.png" width="15px">
													</li>
												</td>
											</tr>
											<tr>
												<td class="bilan-af">
													<label class="label">
														<u>Communes :</u>
													</label>
												</td>
												<td class="bilan-af">
													<li>
														<label class="label">Osgiliath</label>
													</li>
													<li>
														<label class="label">Minas Tirith</label>
													</li>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td style="vertical-align:top">
									<table id="tbl_situation_cadastre" class="bilan-af" cellspacing="5" cellpadding="5">
										<tbody>
											<tr>
												<td colspan="2" class="bilan-af">
													<label class="label">
														<u>Superficie cadastrale  :</u> 5.61.74 ha.a.ca
													</label>
												</td>
											</tr>
											<tr>
												<td class="bilan-af">
													<label class="label">
														<u>Situation cadastrale  :</u>
													</label>
												</td>
												<td class="bilan-af">
													<li>
														<label class="label">5 parcelles</label>
													</li>
													<li>
														<label class="label">6 lots</label>
													</li>
													<li>
														<label class="label">3 comptes de propriété</label>
													</li>
													<li>
														<label class="label">8 propriétaires</label>
													</li>
												</td>
											</tr>									
										</tbody>
									</table>
								</td>
								<td style="vertical-align:top">
									<table id="tbl_obj_animation" class="bilan-af" cellspacing="5" cellpadding="5">
										<tbody>
											<tr>
												<td class="bilan-af" style="vertical-align:top;text-align:left">
													<label class="label" style='margin-left:-40px;'><u>Objectif de l'animation  :</u></label>
												</td>
											</tr>
											<tr>
												<td style="text-align:left">
													<table class="bilan-af" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Objectif d'acquisition : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		4 lots (100%)
																	</li>
																</td>
															</tr>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Objectif de convention : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		0 lot (0%)
																	</li>
																</td>
															</tr>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Pas d'objectif : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		0 lot (0%)
																	</li>
																</td>
															</tr>									
														</tbody>
													</table>
												</td>
											</tr>												
										</tbody>
									</table>
								</td>
							</tr>
							<tr id='tr_bilan_animation' style="vertical-align:top;text-align:right;">
								<td colspan="2">
									<img style="border-radius:15px;box-shadow:2px 2px 3px 2px #6a6a6a;" src="images/help/pie_bilan_help_af.png" alt="pie_bilan_af">
								</td>
								<td style="vertical-align:top;text-align:right;">
									<table id="tbl_bilan_animation" class="bilan-af" style="float:right;" cellspacing="5" cellpadding="5">
										<tbody>
											<tr>									
												<td class="bilan-af" style="vertical-align:top;text-align:left">
													<label class="label"><u>Bilan de l'animation  :</u></label>
												</td>
											</tr>
											<tr>
												<td style="text-align:left;padding-left:40px;">
													<table class="bilan-af" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Lots acquis : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		0 lot (0%)
																	</li>
																</td>
															</tr>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Lots en projet d'acquisition : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		0 lot (0%)
																	</li>
																</td>
															</tr>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Lots conventionnés : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		1 lots (42%)
																	</li>
																</td>
															</tr>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Lots en projet de convention : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		0 lot (0%)
																	</li>
																</td>
															</tr>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right">Lots bloqués : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0" class="label">
																		0 lot (0%)
																	</li>
																</td>
															</tr>
															<tr>
																<td class="bilan-af" style="vertical-align:top; text-align:right">
																	<li class="label" style="text-align:right;color:#ff0000;">A déterminer : </li>
																</td>
																<td class="bilan-af" style="padding-bottom:0px">
																	<li style="margin:0;color:#ff0000;" class="label">
																		3 lots (58%)
																	</li>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>												
										</tbody>
									</table>
								</td>							
							</tr>
							<tr>
								<td colspan="3">
									<table class="no-border" width="100%">
										<tbody>
											<tr>
												<td width="1%">
													<label class="label">
														Evénements globaux :
													</label>
													<br>
													<input class="btn_frm_help" value="Saisir un événement" type="button">
												</td>	
												<td id="td_lst_evenements" class="bilan-af">
													<li>
														<label class="label_simple" style="margin-left:10px;">le 03/08/2017 : Réunion de lancement</label>
														<img id='evt_edit' src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">
													</li>
												</td>
											</tr>
										</tbody>
									</table>
								</td>							
							</tr>					
						</tbody>
					</table>
				</div>
				
				
				
				<div id="interloc" class="tabs-container tabs-hide" style="min-width: 0px;">					
					<table style="border:0px solid black" width="100%" cellspacing="10" cellpadding="10">
						<tbody>
							<tr>
								<td>
									<center>
									<table class="no-border" id="tbl_interloc" width="100%" cellspacing="0" cellpadding="5" border="0" align="CENTER">
										<tbody id="tbl_interlocs_entete">							
											<tr>
												<td class="filtre" style="text-align:left;width:1px;border-bottom:4px solid #004494;">
													<table class="no-border" width="100%" border="0" align="CENTER">
														<tbody>
															<tr>
																<td class="filtre" style="text-align:left;" width="90px">
																	<input style="text-align:center;margin-left:40px;border:0" class="editbox filtre_section help_filtre" id="filtre_interloc_section" size="8" placeholder="SECTION" onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';" type="text" disabled>
																	<input style="text-align:center;border:0" class="editbox filtre_par_num help_filtre" id="filtre_interloc_par_num" size="8" placeholder="PAR NUM" onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';" type="text" disabled>
																	<img class='help_filtre' src="<?php echo ROOT_PATH; ?>images/supprimer.png">
																</td>	
															</tr>
														</tbody>
													</table>	
												</td>
												<td id="td_filtre_lst_interlocs_nom" style="text-align: left; width: 1px; border-bottom: 4px solid rgb(0, 68, 148);">
													<table class="no-border" width="100%" cellspacing="0" cellpadding="0" border="0" align="CENTER">
														<tbody>
															<tr>													
																<td class="filtre" align="left">
																	<input class="editbox filtre_nom help_filtre" style="border:0;" id="filtre_interloc_nom" size="40" placeholder="Interlocuteur" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';" type="text">
																	<img class='help_filtre' src="<?php echo ROOT_PATH; ?>images/supprimer.png">
																</td>														
																<td style="text-align:right;">															
																	<input id="btn_regroup_interloc" class="btn_frm_help" value="Regrouper des propriétaires" type="button">
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
										<tbody id="tbody_interlocs_corps">
											<tr id="tr_22" style="">
												<td style="border-right: 1px solid #ccc; border-bottom: 4px solid #ccc;width:1%;">
													<table id="interloc_tbl_lots_22" class="no-border" cellspacing="1" cellpadding="0">
														<tbody>
															<tr id="interloc_tr_lot-11">
																<td class="no-border " style="margin-top:5px;width:1%;">
																	<label class="label">
																		<u>Lot :</u>
																	</label>
																	<label id="interloc_lbl_lot_id-11" class="label">
																		Minas Tirith - 0B - 913
																	</label>														
																	<img id="interloc_btn_detail-11" src="<?php echo ROOT_PATH; ?>images/plus.png" style="padding-top:10px;"  width="15px">
																</td>
															</tr>															
															<tr id="interloc_tr_lot-15">
																<td class="no-border " style="margin-top:5px;width:1%;">
																	<label class="label">
																		<u>Lot :</u>
																	</label>
																	<label id="interloc_lbl_lot_id-15" class="label">
																		Minas Tirith - 0B - 914 - 00A0004
																	</label>
																	<img id="interloc_btn_detail-15" src="<?php echo ROOT_PATH; ?>images/plus.png" style="padding-top:10px;" width="15px">
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td style="border-bottom: 4px solid #ccc;">
													<center>
														<table id="tbl_22" style="float:none;" class="no-border" cellspacing="0" cellpadding="8">
															<tbody>
																<tr id="tr_51_52_53" style="">			
																	<td style="padding-left:5px;">
																		<div class="22-41 no-breathing">
																			<label id="lbl_22-41-1" style="color:#004494; " class="label">
																			 ASSOCIATION DES GEEKS ANONYMES
																			</label>	
																		</div>
																		<label class="label" style="font-weight:normal;color:#004494;">	
																			Véritable propriétaire
																		</label>
																	</td>
																	<td style="padding-left:5px;">
																		<img src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">
																	</td>
																	<td style="padding-left:5px;"></td>
																</tr>	
																<tr id="tr_51_52_59" style="">			
																	<td style="padding-left:5px;">
																		<div class="22-41 no-breathing">
																			<label id="lbl_22-41-1" style="color:#2b5824;" class="label">
																			 ASSOCIATION DES GEEKS ANONYMES
																			</label>	
																		</div>
																		<label class="label" style="font-weight:normal;color:#2b5824;">	
																			Agriculteur (contrat d'exploitation)
																		</label>
																	</td>
																	<td style="padding-left:5px;">
																		<img src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">
																	</td>
																	<td style="padding-left:5px;"></td>
																</tr>																	
																<tr id="tr_add_interloc_22">
																	<td id="td_22" style="text-align:center;" colspan="3">
																		<label class="label">																	
																			<input id="btn_22" class="btn_frm_help" value="Ajouter un interlocuteur" type="button">
																		</label>
																	</td>
																</tr>																
															</tbody>
														</table>
													</center>
												</td>
											</tr>
											<tr id="tr_18" style="">
												<td style="border-right: 1px solid #ccc; border-bottom: 4px solid #ccc;width:1%;">
													<table id="interloc_tbl_lots_18" class="no-border" cellspacing="1" cellpadding="0">
														<tbody>
															<tr id="interloc_tr_lot-25">
																<td class="no-border " style="margin-top:5px;width:1%;">
																	<label class="label">
																		<u>Lot :</u>
																	</label>
																	<label id="interloc_lbl_lot_id-25" class="label">
																		Minas Tirith - 0B - 918 - 00A0001
																	</label>
																	<img id="interloc_btn_detail-25" src="<?php echo ROOT_PATH; ?>images/plus.png" style="padding-top:10px;"  width="15px">															
																</td>
															</tr>
															<tr id="interloc_tr_lot-25">
																<td class="no-border " style="margin-top:5px;width:1%;">
																	<label class="label">
																		<u>Lot :</u>
																	</label>
																	<label id="interloc_lbl_lot_id-25" class="label">
																		Minas Tirith - 0B - 918 - 00A0002
																	</label>
																	<img id="interloc_btn_detail-25" src="<?php echo ROOT_PATH; ?>images/plus.png" style="padding-top:10px;"  width="15px">															
																</td>
															</tr>
															<tr id="interloc_tr_lot-25">
																<td class="no-border " style="margin-top:5px;width:1%;">
																	<label class="label">
																		<u>Lot :</u>
																	</label>
																	<label id="interloc_lbl_lot_id-25" class="label">
																		Minas Tirith - 0B - 918 - 00A0003
																	</label>
																	<img id="interloc_btn_detail-25" src="<?php echo ROOT_PATH; ?>images/plus.png" style="padding-top:10px;"  width="15px">															
																</td>
															</tr>																			
														</tbody>
													</table>
												</td>
												<td style="border-bottom: 4px solid #ccc;">
													<center>
														<table id="tbl_18" style="float:none;" class="no-border" cellspacing="0" cellpadding="8">
															<tbody>																
																<tr id="tr_no_vp_18">
																	<td style="text-align:center;">
																		<label class="label" style="color:red;">	
																			Attention il n'y a pas de véritable propriétaire identifié
																		</label>
																	</td>
																</tr>
																<tr id="tr_add_interloc_18">
																	<td id="td_18" style="text-align:center;" colspan="3">
																		<label class="label">																	
																			<input id="btn_18" class="btn_frm_help" value="Ajouter un interlocuteur" type="button">
																		</label>
																	</td>
																</tr>
																<tr id="tr_gestref_18">
																	<td style="text-align:center;" colspan="3">
																	</td>
																</tr>
															</tbody>
														</table>
													</center>
												</td>
											</tr>
											<tr id="tr_16" style="">
												<td style="border-right: 1px solid #ccc; border-bottom: 4px solid #ccc;width:1%;">
													<table id="interloc_tbl_lots_16" class="no-border" cellspacing="1" cellpadding="0">
														<tbody>
															<tr id="interloc_tr_lot-22">
																<td class="no-border " style="margin-top:5px;width:1%;">
																	<label class="label">
																		<u>Lot :</u>
																	</label>
																	<label id="interloc_lbl_lot_id-22" class="label">
																		Minas Tirith - 0B - 917 - 00A0001
																	</label>
																
																	<img id="interloc_btn_detail-22" src="<?php echo ROOT_PATH; ?>images/moins.png" style="padding-top:10px;" width="15px">															
																</td>
															</tr>
															<tr>
																<td class="" id="interloc_td_detail-22" colspan="2">
																	<table class="no-border">
																		<tbody><tr>
																			<td class="no-border">
																				<label class="label">
																					<u>ID :</u> 2490000B091700A0001
																				</label>
																			</td>
																		</tr>
																		<tr>
																			<td class="no-border">
																				<label class="label">
																					<u>Contenance :</u> 0.18.90
																				</label>
																			</td>
																		</tr>		
																		<tr>
																			<td class="no-border">
																				<label class="label">
																					<u>Priorité d'animation :</u> 1
																				</label>
																			</td>
																		</tr>
																		<tr>
																			<td class="no-border">
																				<label class="label">
																					<u>Objectif :</u> Objectif d'acquisition
																				</label>
																			</td>
																		</tr>
																		<tr>
																			<td class="no-border">
																				<table style="border:0 solid black">
																					<tbody>
																						<tr>
																							<td style="vertical-align:top">
																								<label class="label" style="margin-left:-3px"><u>Natures de culture : </u></label> 
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table style="border:0 solid black;margin-left:5px;" cellspacing="0" cellpadding="0">
																									<tbody>
																										<tr>
																											<td style="text-align:left">
																												<label class="label">
																													Prés : 0.18.90 (100%)
																												</label>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>									
																	</tbody></table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td style="border-bottom: 4px solid #ccc;">
													<center>
														<table id="tbl_16" style="float:none;" class="no-border" cellspacing="0" cellpadding="8">
															<tbody>
																<tr id="tr_30" style="">			
																	<td style="padding-left:5px;">
																		<div class="16-17 no-breathing">
																			<label id="lbl_16-17-1" style="color:#004494; " class="label referent">
																			M. PICSOU Balthazar
																			</label>	
																		</div>
																		<label class="label" style="font-weight:normal;color:#004494;">	
																			Véritable propriétaire
																		</label>
																	</td>
																	<td style="padding-left:5px;">
																		<img src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">
																	</td>
																	<td style="padding-left:5px;">																		
																	</td>
																</tr>																
																<tr id="tr_31" style="">			
																	<td style="padding-left:5px;">
																		<div class="16-22 no-breathing">
																			<label id="lbl_16-22-1" style="color:#004494; " class="label">
																			Mme. BOOP Betty
																			</label>	
																		</div>
																		<label class="label" style="font-weight:normal;color:#004494;">	
																			Véritable propriétaire
																		</label>
																	</td>
																	<td style="padding-left:5px;">
																		<img style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png">
																		<img id="img_delete_31" src="<?php echo ROOT_PATH; ?>images/supprimer.png">
																	</td>
																	<td style="padding-left:5px;">
																		<table class="no-border">
																				<tbody><tr>			
																					<td style="padding-left:15px;">
																						<div class="16-11 no-breathing">
																							<label id="lbl_16-11-10" style="color:#7500be;" class="label">
																								M. DUCK Donald
																							</label>	
																						</div>
																						<label class="label" style="color:#7500be;font-weight:normal;">	
																							Curateur
																						</label>	
																					</td>
																					<td style="padding-left:15px">
																						<img src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">
																					</td> 	
																			</tr>
																		</tbody></table>
																	</td>
																</tr>
																<tr id='tr_32'style="">			
																	<td style="padding-left:5px;">
																		<div class="16-30 no-breathing">
																			<label style="color:#004494; " class="label">
																			Mme. DUCK Daisy
																			</label>	
																		</div>
																		<label class="label" style="font-weight:normal;color:#004494;">	
																			Véritable propriétaire
																		</label>
																	</td>
																	<td style="padding-left:5px;">
																		<img src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">
																	</td>
																	<td style="padding-left:5px;">
																		<table class="no-border"> 	
																			
																		</table>
																	</td>
																</tr>
																<tr id="tr_33" style="">			
																	<td style="border-bottom:1px solid #004494; padding-left:5px;">
																		<div class="16-30 no-breathing">
																			<label id="lbl_16-30-1" style="color:#2b5824; " class="label">
																			M. FUDD Elmer
																			</label>	
																		</div>
																		<label class="label" style="font-weight:normal;color:#2b5824;">	
																			Chasseur (bail de chasse)	
																		</label>
																	</td>
																	<td style="border-bottom:1px solid #004494; padding-left:5px;">
																		<img src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">
																	</td>
																	<td style="border-bottom:1px solid #004494; padding-left:5px;">
																		<table class="no-border"> 	
																			
																		</table>
																	</td>
																</tr>
																<tr id="tr_49" style="">			
																	<td style="padding-left:5px;">
																		<div class="16-40 no-breathing">
																			<label id="lbl_16-40-1" style="color:#ccc; " class="label">
																			Mme. POPPINS Mary
																			</label>	
																		</div>
																		<label class="label" style="font-weight:normal;color:#ccc;">	
																			Véritable propriétaire
																		</label>
																		<br>
																		<label class="last_maj" style="color:red;">	
																			(Décédé)
																		</label>
																	</td>
																	<td style="padding-left:5px;">
																		<img src="<?php echo ROOT_PATH; ?>images/edit.png" style="padding-left:5px;">												
																		<img id="img_delete_49" src="<?php echo ROOT_PATH; ?>images/supprimer.png">
																	</td>
																	<td style="padding-left:5px;">
																		<table class="no-border"> 	
																			
																		</table>
																	</td>
																</tr>
																<tr id="tr_add_interloc_16">
																	<td id="td_16" style="text-align:center;" colspan="3">
																		<label class="label">																	
																			<input id="btn_16" class="btn_frm_help" value="Ajouter un interlocuteur" type="button">
																		</label>
																	</td>
																</tr>
																<tr id="tr_gestref_16">
																	<td style="text-align:center;" colspan="3">
																		<input id="btn_gestref_16" class="btn_frm_help" value="Gestion des référents" type="button">
																		<input id="btn_gestsubst_16" class="btn_frm_help" value="Gestion des représentants" type="button">
																	</td>
																</tr>
															</tbody>
														</table>
													</center>
												</td>
											</tr>
										</tbody>
									</table>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div id="parcelle" class="tabs-container tabs-hide" style="min-width: 0px;">
					<table style="border:0px solid black" width="100%" cellspacing="10" cellpadding="10">
						<tbody>
							<tr>
								<td>
									<center>
									<table class="no-border" id="tbl_parc" width="100%" cellspacing="0" cellpadding="4" border="0" align="CENTER">
										<tbody id="tbody_parc_entete">
											<tr>
												<td style="text-align:left;width:1px;border-bottom:4px solid #004494;">
													<table class="no-border" width="1px">
														<tbody>
															<tr>
																<td style="text-align:left;">
																	<input class="editbox filtre_nom help_filtre" style="border:0;" id="filtre_parcelle_nom" size="40" placeholder="Interlocuteur" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';" type="text" disabled>
																	<img src="<?php echo ROOT_PATH; ?>images/supprimer.png" class='help_filtre'>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td style="text-align:left;border-bottom:4px solid #004494;">
													<table class="no-border" width="100%">
														<tbody>
															<tr>
																<td class="filtre" style="text-align:left;" width="90px">
																	<input style="text-align:center;margin-left:40px;border:0" class="editbox filtre_section help_filtre" id="filtre_parcelle_section" size="8" placeholder="SECTION" onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';" type="text" disabled>
																	<input style="text-align:center;border:0" class="editbox filtre_par_num help_filtre" id="filtre_parcelle_par_num" size="8" placeholder="PAR NUM" onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';" type="text" disabled>
																	<img src="<?php echo ROOT_PATH; ?>images/supprimer.png" class='help_filtre'>
																</td>
																<td style="text-align:center;width:100%;">															
																	<input id="btn_prix_occsol" class="btn_frm_help" style="display:nne;" value="Saisir des prix au m² par occupation du sol" type="button">
																</td>
																<td id='td_onglet_parc_ALL' style="text-align:right;margin:10px 10px 10px -8px;width:100%;">
																	<img id="btn_onglet_parc_ALL" src="<?php echo ROOT_PATH; ?>images/moins.png" width="15px">	
																	<label id="lbl_onglet_parc_ALL" class="label">
																		Réduire tout
																	</label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
										<tbody id="tbody_parc_corps">
											<tr>
												<td colspan="2" style="background:#fff;">
													&nbsp;
												</td>
											</tr>
											<tr id="tr_parc_prix_eaf-16" style="">
												<td colspan="2" style="border-color:#ccc;border-width:4px 4px 1px 4px;border-style:solid;">
													<table class="no-border" cellspacing="0" cellpadding="0" width='100%'>
														<tbody>
															<tr>
																<td style="text-align:left;">
																	<label class="label" style='margin-left:25px;'>
																		<i>Prix d'acquisition de l'EAF =&gt; </i>
																	</label>
																	<label id="lbl_montant_eaf_calcule-18" class="label_simple" style='margin-left:25px;'>
																		<i><b>calculé</b> : 13 350.50 €</i>
																	</label>																
																	<label class="label_simple" style='margin-left:25px;'>
																		<i><b>opérateur</b> :</i>						
																		<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_eaf-16" size="12" value="" type="text" disabled> €
																		<img id="img_save_montant_eaf-16" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;" width="15px">	
																	</label>
																	<label id="lbl_montant_eaf_calcule-18" class="label_simple" style='margin-left:25px;'>
																		<i><b>en cours de négociation</b> : 850.00 €</i>
																	</label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="tr_parc_infos-16" style="">
												<td id='td_parc_interloc-16' style="border-right: 1px solid #ccc; border-left: 4px solid #ccc;">
													<table id="tbl_parc_interloc-16" class="no-border" cellspacing="0" cellpadding="8">									
														<tbody>
															<tr style="">			
																<td class="bilan-af" style="padding-bottom:2px;padding-left:15px;">
																	<label id="lbl_16-17" class="label referent" style="color:#004494">
																		M. PICSOU Balthazar
																	</label>
																	<br>	
																	<label class="label" style="font-weight:normal;color:#004494;padding-left:20px;">Véritable propriétaire	
																	</label>	
																</td>
															</tr>									
															<tr style="">			
																<td class="bilan-af" style="padding-bottom:2px;padding-left:15px;padding-top:0; ">
																	<label id="lbl_16-11" class="label" style="color:#7500be">
																		M. DUCK Donald
																	</label>
																	<br>
																	<label class="label" style="font-weight:normal;color:#7500be;padding-left:20px;">Curateur	
																	</label>															
																	<li class="label_simple" style="padding-left:60px;">
																		<label id="lbl_16-22" class="label" style="color:#004494">
																			Mme. BOOP Betty
																		</label>
																		<br>
																			<label class="label_simple" style="color:#004494;padding-left:20px;">
																				Véritable propriétaire
																			</label>
																		<br>
																	</li>	
																</td>
															</tr>									
																<tr style="">			
																	<td class="bilan-af" style="padding-bottom:2px;padding-left:15px;">
																		<label id="lbl_16-30" class="label" style="color:#2b5824">
																			M. FUDD Elmer
																		</label>
																		<br>
																		<label class="label" style="font-weight:normal;color:#2b5824;padding-left:20px;">Chasseur (bail de chasse)	
																		</label>	
																	</td>
																</tr>			
																<tr style="">			
																	<td class="bilan-af" style="padding-bottom:2px;padding-left:15px;">
																		<label class="label" style="color:#004494">
																			Mme. DUCK Daisy
																		</label>
																		<br>
																		<label class="label" style="font-weight:normal;color:#004494;padding-left:20px;">Véritable propriétaire	
																		</label>	
																	</td>
																</tr>											
																<tr style="">			
																	<td class="bilan-af" style="padding-bottom:2px;padding-left:15px;padding-top:20px ">
																		<label id="lbl_16-4" class="label" style="color:#ccc">
																			Mme. POPPINS Mary
																		</label>
																		<br>	
																		<label class="label" style="font-weight:normal;color:#ccc;padding-left:20px;">Véritable propriétaire	
																		</label>
																		<br>
																		<label class="last_maj" style="color:red;padding-left:20px;">	
																			(Décédé)
																		</label>	
																	</td>
																</tr>
														</tbody>
													</table>
												</td>
												<td style="border-right: 4px solid #ccc;">
													<table id="parc_tbl_lots_16" class="no-border" width="100%" cellspacing="1" cellpadding="0">
														<tbody>
															<tr>
																<td id="parc_td_general-22">									
																	<table class="no-border" width="100%">
																		<tbody>
																			<tr>
																				<td style="text-align:left;width:15px;padding-right:15px;" rowspan="3">
																					<img id="parc_btn_detail-22" src="<?php echo ROOT_PATH; ?>images/moins.png" width="15px">	
																				</td>
																				<td style="text-align:left;" colspan="2">									
																					<label id="parc_lbl_lot_id-22" class="label">
																						<u>Lot :</u>Minas Tirith - 0B - 917 - 00A0001
																					</label>
																				</td>
																				
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lot ID :</u> 2490000B091700A0001
																					</label>
																				</td>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lieu-dit :</u> AUX LITIERES                  
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Contenance :</u> 0.18.90
																					</label>
																				</td>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Surface à maitriser :</u> 0.18.90
																					</label>	
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td id="parc_td_detail-22" class="td_detail_parc " style="">
																	<table class="no-border" width="100%">
																		<tbody>
																			<tr id='tr_objectif-22'>
																				<td class="no-border" width="100%">
																					<fieldset id="fieldset_parc_objectif" class="fieldset_af">
																						<legend>Objectifs et négociations</legend>
																						<table style="border:0 solid black;width:100%">
																							<tbody>
																								<tr>
																									<td class="no-border" style="text-align:left;width:50%;">
																										<label class="label">
																											<u>Priorité :</u>
																										</label>
																										<label class="btn_combobox" style="margin-left:5px;">			
																											<select name="paf-22" id="paf-22" class="combobox paf" style="text-align:left" disabled><option value="0">Pas dans le paf</option><option selected="" value="1">Priorité d'animation 1</option><option value="2">Priorité d'animation 2</option><option value="3">Priorité d'animation 3</option>
																											</select>
																										</label>
																									</td>
																									<td class="no-border" style="text-align:right;width:50%;">
																										<label class="label">
																											<u>Objectif interne :</u>
																										</label>
																										<label class="btn_combobox" style="margin-left:5px;">			
																											<select name="obj-22" id="obj-22" class="combobox" style="text-align:left" disabled><option selected="" value="1">Objectif d'acquisition</option><option value="2">Objectif de convention</option><option value="3">Pas d'objectif</option>
																											</select>
																										</label>
																									</td>
																								</tr>
																								<tr>
																									<td class="no-border">
																										<label class="label" style="vertical-align:top;">
																											<u>Négociations :</u> 
																										</label>
																										<label id='lbl_cur_nego-22' class="label_simple" style="vertical-align:top;">Les propriétaires ne se sont pas encore mis d'accord</label>
																									</td>
																									<td class="no-border" style="text-align:right;width:50%;">
																										<label class="label_simple"><i></i></label>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</fieldset>
																				</td>
																			</tr>
																			<tr id='tr_pratique_sol-22'>
																				<td class="no-border" width="100%">
																					<fieldset id="fieldset_parc_sol" class="fieldset_af">
																						<legend>Pratiques du sol</legend>
																						<table style="border:0 solid black;">
																							<tbody>
																								<tr id='tr_nature_culture-22'>
																									<td style="vertical-align:top">
																										<label class="label" style="margin-left:-3px"><u>Natures de culture : </u></label> 
																									</td>
																									<td>
																										<table style="border:0 solid black;margin-left:5px;" cellspacing="0" cellpadding="0">
																											<tbody><tr>
																												<td style="text-align:left">
																													<label class="label">
																														Prés : 0.18.90 (100%)
																													</label>
																												</td>
																											</tr>
																											<tr>
																												<td style="text-align:left;">
																													<label class="label" style="font-size:9pt;font-weight:normal;">
																														(Pré marais)
																													</label>
																												</td>
																											</tr>
																										</tbody></table>
																									</td>
																								</tr>
																								<tr id='occsol-22'>
																									<td class="no-border">
																										<label class="label" style="vertical-align:top;">
																											<u>Occupation du sol :</u> 
																										</label>
																									</td>
																									<td>
																										<table style="border:0;" cellspacing="0" cellpadding="0">
																											<tbody>
																												<tr>
																													<td>
																														<table id="tbl_parc_occsol-22" class="tbl_parc_occsol" style="border:0;" cellspacing="0" cellpadding="0">
																															<tbody>
																																<tr>
																																	<td>
																																		<label class="btn_combobox" style="margin-left:5px;">			
																																			<select id="type_occsol-25-0" class="combobox type_occsol" style="text-align:left;width:154px;" disabled>
																																				<option value="-1"></option>
																																				<option selected="" value="0">Prairie</option>			
																																			</select>
																																		</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox pourcentage_occsol" id="pourc_occsol-25-0" size="3" value="20" type="text"><label class="label_simple">%</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox prix_occsol" id="prix_occsol-25-0" size="3" value="0.2" type="text"><label class="label_simple">€ le m²</label>
																																	</td>
																																</tr>
																																<tr>
																																	<td>
																																		<label class="btn_combobox" style="margin-left:5px;">			
																																			<select id="type_occsol-25-1" class="combobox type_occsol" style="text-align:left;width:154px;" disabled>
																																				<option value="-1"></option>
																																				<option selected="" value="2">Marais</option>		
																																			</select>
																																		</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox pourcentage_occsol" id="pourc_occsol-25-1" size="3" value="20" type="text"><label class="label_simple">%</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox prix_occsol" id="prix_occsol-25-1" size="3" value="0.5" type="text"><label class="label_simple">€ le m²</label>
																																	</td>
																																</tr>
																																<tr>
																																	<td>
																																		<label class="btn_combobox" style="margin-left:5px;">			
																																			<select id="type_occsol-25-2" class="combobox type_occsol" style="text-align:left;width:154px;" disabled>
																																				<option value="-1"></option>
																																				<option selected="" value="3">Pelouse sèche</option>
																																			</select>
																																		</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox pourcentage_occsol" id="pourc_occsol-25-2" size="3" value="20" type="text"><label class="label_simple">%</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox prix_occsol" id="prix_occsol-25-2" size="3" value="0.35" type="text"><label class="label_simple">€ le m²</label>
																																	</td>
																																</tr>	
																																<tr>
																																	<td>
																																		<label class="btn_combobox" style="margin-left:5px;">			
																																			<select id="type_occsol-25-3" class="combobox occsol" style="text-align:left;" disabled>
																																				<option value="-1"></option>
																																				<option value="1">Culture</option>
																																				<option selected value="4">Boisement</option>
																																				<option value="5">Plantation</option>
																																				<option value="6">Lande/ friche</option>
																																				<option value="7">Eau libre</option>
																																				<option value="8">Milieu artificialisé</option>
																																			</select>
																																		</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox pourcentage_occsol" id="pourc_occsol-25-3" size="3" value="40" type="text"><label class="label_simple">%</label>
																																		<input style="text-align:right;margin-left:10px;border:0" class="editbox prix_occsol" id="prix_occsol-25-3" size="3" value="0.6" type="text"><label class="label_simple">€ le m²</label>
																																	</td>
																																</tr>													
																															</tbody>
																														</table>
																													</td>
																													<td>
																														<img id="img_save_occsol-22" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;margin-left:10px;" width="15px">	
																													</td>
																													<td>
																														<div class="lib_alerte" id="occsol_prob_saisie-22" style="display:none;white-space:pre-wrap;padding:10px">Veuillez vérifier votre saisie</div>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr id='utilisol-22'>
																									<td class="no-border">
																										<label class="label" style="vertical-align:top;">
																											<u>Utilisation du sol :</u> 
																										</label>
																									</td>
																									<td>
																										<textarea name="utilissol_22" class="label textarea_com_edit" rows="2" cols="1000" disabled>N.D.</textarea>
																										<input id="hid_utilissol-22" value="N.D." type="hidden">
																									</td>
																									<td>
																										<img id="img_save_utilissol-22" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;" width="15px">	
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</fieldset>
																				</td>
																			</tr>	
																			<tr id='tr_prix-22'>
																				<td class="no-border" width="100%">
																					<fieldset id="fieldset_parc_prix" class="fieldset_af">
																						<legend>Prix d'acquisition</legend>
																						<div class="no-border" style="width:100%;text-align:right;">
																							<label id="lbl_prix_total_occsol-22" class="label" style="vertical-align: top;">
																								<u>Prix estimé du fond à partir de l'occupation du sol :</u> 850.50 €
																							</label>
																						</div>
																						<table style="border:0;background:transparent;" cellspacing="0" cellpadding="0">
																							<tbody>
																								<tr>
																									<td>
																										<table id="tbl_parc_prix-25" style="border:0;background:transparent;" cellspacing="0" cellpadding="2">
																											<tbody>
																												<tr>
																													<th style="text-align:center;margin-left:10px;">
																														<label class="last_maj">Libellé</label>
																													</th>
																													<th style="text-align:center;margin-left:10px;">
																														<label class="last_maj">Montant</label>
																													</th>
																													<th style="text-align:center;margin-left:10px;">
																														<label class="last_maj" style="text-align:center;margin-left:10px;">Quantité</label>
																													</th>
																												</tr>
																												<tr id='tr_prix_fond_total' class="prix_detail">
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="lib_prix-25-0" size="40" value="Prix du fond total" type="text" disabled>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_prix-25-0" size="8" value="850.50" type="text" disabled><label class="label_simple">€</label>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="quantite_prix-25-0" size="8" value="1" type="text" disabled>
																													</td>
																													<td>
																														<label class="label"> = </label>
																													</td>
																													<td style="text-align:right;">
																														<label class="label">850.50 €</label>
																													</td>
																												</tr>
																												<tr id='tr_prix_fond_m2' class="prix_detail"style='display:none;'>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="lib_prix-25-0" size="40" value="Prix du fond au m²" type="text" disabled>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_prix-25-0" size="8" value="0.45" type="text" disabled><label class="label_simple">€</label>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="quantite_prix-25-0" size="8" value="1890" type="text" disabled>
																													</td>
																													<td>
																														<label class="label"> = </label>
																													</td>
																													<td style="text-align:right;">
																														<label class="label">850.50 €</label>
																													</td>
																												</tr>
																												<tr class="prix_detail">
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="lib_prix-25-1" size="40" value="Prix du bois sur pied (m3)" type="text" disabled>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_prix-25-1" size="8" value="50.00" type="text" disabled><label class="label_simple">€</label>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="quantite_prix-25-1" size="8" value="250" type="text" disabled>
																													</td>
																													<td>
																														<label class="label"> = </label>
																													</td>
																													<td style="text-align:right;">
																														<label class="label">12 500.00 €</label>
																													</td>
																												</tr>	
																												<tr class="prix_detail">
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="lib_prix-25-2" size="40" type="text" disabled>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_prix-25-2" size="8" type="text" disabled><label class="label_simple">€</label>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="quantite_prix-25-2" size="8" value="1" type="text" disabled>
																													</td>
																													<td colspan="2">
																														&nbsp;
																													</td>
																												</tr>
																												<tr>
																													<td colspan="4">
																														&nbsp;				
																													</td>
																													<td style="border-top:1px solid #ccc;text-align:right;">
																														<label class="label">13 350.50 €</label>
																													</td>
																												</tr>
																												<tr id='tr_prix_total_op-22'>
																													<td colspan="4" style="text-align:right;">
																														<label class="label"><u>Prix d'acquisition total opérateur :</u></label>
																													</td>
																													<td>
																														<input style="text-align:right;margin-left:10px;border:0" class="editbox parc_arrondi_prix" id="parc_arrondi_prix-25" size="8" value="13 350.50" type="text" disabled><label class="label">€</label>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																									<td>
																										<img id="img_save_prix-22" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;margin-left:10px;" width="15px">	
																									</td>
																									<td>
																										<div class="lib_alerte" id="prix_prob_saisie-22" style="display:none;white-space:pre-wrap;padding:10px">Veuillez vérifier votre saisie</div>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</fieldset>	
																				</td>
																			</tr>
																			<tr id='tr_divers-22'>
																				<td class="no-border" width="100%">
																					<fieldset id="fieldset_parc_divers" class="fieldset_af">
																						<legend>Divers</legend>
																						<table style="border:0 solid black">
																							<tbody>
																								<tr>
																									<td class="no-border" colspan="3">
																										<input id="lot_bloque-22" class="chk_lot_bloque" type="checkbox" disabled>
																										<label id="lbl_lot_bloque-22" class="label"><u>Lot bloqué</u></label>
																									</td>
																								</tr>
																								<tr id='tr_comm-22'>
																									<td class="no-border">
																										<label class="label" style="vertical-align:top;">
																											<u>Commentaires :</u> 
																										</label>
																									</td>
																									<td>
																										<textarea id="commentaires-22" name="commentaires-22" class="label textarea_com_edit" rows="2" cols="1000" disabled></textarea>
																									</td>
																									<td>
																										<img id="img_save_commentaires-22" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;" width="15px">	
																									</td>
																								</tr>	
																							</tbody>
																						</table>
																					</fieldset>	
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="tr_parc_comm_eaf-16" style="">
												<td colspan="2" style="border-color:#ccc;border-width:1px 4px 4px 4px;border-style:solid;">
													<table class="no-border" cellspacing="0" cellpadding="2">
														<tbody><tr>
															<td style="width:33%;text-align:center;">
																<label class="label"><i>Commentaires sur l'EAF : </i></label>
															</td>
															<td style="text-align:center;white-space:nowrap;">
																<textarea id="commentaires_eaf-16" class="label textarea_com_edit" rows="1" cols="60"  disabled></textarea>								
															</td>
															<td style="width:15px;text-align:center;">
																<img id="img_save_commentaires_eaf-16" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;" width="15px">	
															</td>
														</tr>
													</tbody></table>
												</td>
											</tr>			
											<tr>
												<td colspan="2" style="background:#fff;">
													&nbsp;
												</td>
											</tr>
											<tr id="tr_parc_prix_eaf-9" style="">
												<td colspan="2" style="border-color:#ccc;border-width:4px 4px 1px 4px;border-style:solid;">
													<table class="no-border" cellspacing="0" cellpadding="0" width='100%'>
														<tbody>
															<tr>
																<td style="text-align:left;">
																	<label class="label" style='margin-left:25px;'>
																		<i>Prix d'acquisition de l'EAF =&gt; </i>
																	</label>																
																	<label id="lbl_montant_eaf_calcule-19" class="label_simple" style='margin-left:25px;'>
																		<i>calculé : 3 340.00 €</i>
																	</label>
																	<label class="label_simple" style='margin-left:25px;'><i>opérateur :</i>						
																		<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_eaf-16" size="12" value="3300" type="text" disabled> €
																		<img id="img_save_montant_eaf-16" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;" width="15px">
																	</label>																		
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="tr_parc_infos-9" style="">
												<td style="border-right: 1px solid #ccc; border-left: 4px solid #ccc;">
													<table id="tbl_9" class="no-border" cellspacing="0" cellpadding="8">									
														<tbody>
															<tr style="">			
																<td class="bilan-af" style="padding-bottom:2px;padding-left:15px;">
																	<label class="label" style="color:#004494">
																		ASSOCIATION DES GEEKS ANONYMES
																	</label>
																	<br>	
																	<label class="label" style="font-weight:normal;color:#004494;padding-left:20px;">Véritable propriétaire	
																	</label>	
																</td>
															</tr>
															<tr style="">			
																<td class="bilan-af" style="padding-bottom:2px;padding-left:15px;">
																	<label class="label" style="color:#2b5824">
																		ASSOCIATION DES GEEKS ANONYMES
																	</label>
																	<br>	
																	<label class="label" style="font-weight:normal;color:#2b5824;padding-left:20px;">Agriculteur (contrat d'exploitation)
																	</label>	
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td style="border-right: 4px solid #ccc;">
													<table id="parc_tbl_lots_9" class="no-border" width="100%" cellspacing="1" cellpadding="0">
														<tbody>
															<tr>
																<td id="parc_td_general-10" class="">									
																	<table class="no-border" width="100%">
																		<tbody>
																			<tr>
																				<td style="text-align:left;width:15px;padding-right:15px;" rowspan="3">
																					<img id="parc_btn_detail-10" src="<?php echo ROOT_PATH; ?>images/plus.png" width="15px">	
																				</td>
																				<td style="text-align:left;" colspan="2">									
																					<label id="parc_lbl_lot_id-10" class="label ">
																						<u>Lot :</u>Minas Tirith - 0B - 913
																					</label>
																				</td>
																				
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lot ID :</u> 2490000B0913
																					</label>
																				</td>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lieu-dit :</u> AUX LITIERES                  
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Contenance :</u> 0.39.56
																					</label>
																				</td>
																				<td id='td_conv_partie' style="text-align:left;">
																					<label class="label ">
																						<u>Surface à maitriser :</u> 0.30.16 <label class='lib_alerte'>pour partie</label>
																					</label>	
																					<label class='label_simple ".$parc_class."'>
																						(Surface initiale : 0.35.10)
																					</label>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>													
															<tr id='tr_prix-10' style='display:none'>
																<td class="no-border" width="100%">
																	<fieldset id="fieldset_parc_prix" class="fieldset_af">
																		<legend>Prix d'acquisition</legend>																		
																		<table style="border:0;background:transparent;" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td>
																						<table id="tbl_parc_prix-10" style="border:0;background:transparent;" cellspacing="0" cellpadding="2">
																							<tbody>
																								<tr>
																									<th style="text-align:center;margin-left:10px;">
																										<label class="last_maj">Libellé</label>
																									</th>
																									<th style="text-align:center;margin-left:10px;">
																										<label class="last_maj">Montant</label>
																									</th>
																									<th style="text-align:center;margin-left:10px;">
																										<label class="last_maj" style="text-align:center;margin-left:10px;">Quantité</label>
																									</th>
																								</tr>
																								<tr class="prix_detail">
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="lib_prix-10-0" size="40" value="Prix du fond total" type="text" disabled>
																									</td>
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_prix-10-0" size="8" value="1740.50" type="text" disabled><label class="label_simple">€</label>
																									</td>
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="quantite_prix-10-0" size="8" value="1" type="text" disabled>
																									</td>
																									<td>
																										<label class="label"> = </label>
																									</td>
																									<td style="text-align:right;">
																										<label class="label">1 740.50 €</label>
																									</td>
																								</tr>																								
																								<tr>
																									<td colspan="4">
																										&nbsp;				
																									</td>
																									<td style="border-top:1px solid #ccc;text-align:right;">
																										<label class="label">1 740.50 €</label>
																									</td>
																								</tr>
																								<tr>
																									<td colspan="4" style="text-align:right;">
																										<label class="label"><u>Prix d'acquisition total opérateur :</u></label>
																									</td>
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox parc_arrondi_prix" id="parc_arrondi_prix-10" size="8" value="1 740.00" type="text" disabled><label class="label">€</label>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																					<td>
																						<img id="img_save_prix-22" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;margin-left:10px;" width="15px">	
																					</td>
																					<td>
																						<div class="lib_alerte" id="prix_prob_saisie-22" style="display:none;white-space:pre-wrap;padding:10px">Veuillez vérifier votre saisie</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</fieldset>	
																</td>
															</tr>																	
															<tr>
																<td style="border-bottom:1px dashed #004494;" colspan="3">
																	&nbsp;
																</td>
															</tr>
															<tr>
																<td id="parc_td_general-17" class="">									
																	<table class="no-border" width="100%">
																		<tbody>
																			<tr>
																				<td style="text-align:left;width:15px;padding-right:15px;" rowspan="3">
																					<img id="parc_btn_detail-17" src="<?php echo ROOT_PATH; ?>images/plus.png" width="15px">	
																				</td>
																				<td style="text-align:left;" colspan="2">									
																					<label id="parc_lbl_lot_id-17" class="label ">
																						<u>Lot :</u>Minas Tirith - 0B - 914 - 00A0004
																					</label>
																				</td>
																				
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lot ID :</u> 2490000B091400A0004
																					</label>
																				</td>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lieu-dit :</u> AUX LITIERES                  
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Contenance :</u> 0.31.86
																					</label>
																				</td>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Surface à maitriser :</u> 0.31.86
																					</label>	
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>										
															<tr id='tr_prix-11' style='display:none'>
																<td class="no-border" width="100%">
																	<fieldset id="fieldset_parc_prix" class="fieldset_af">
																		<legend>Prix d'acquisition</legend>																		
																		<table style="border:0;background:transparent;" cellspacing="0" cellpadding="0">
																			<tbody>
																				<tr>
																					<td>
																						<table id="tbl_parc_prix-11" style="border:0;background:transparent;" cellspacing="0" cellpadding="2">
																							<tbody>
																								<tr>
																									<th style="text-align:center;margin-left:10px;">
																										<label class="last_maj">Libellé</label>
																									</th>
																									<th style="text-align:center;margin-left:10px;">
																										<label class="last_maj">Montant</label>
																									</th>
																									<th style="text-align:center;margin-left:10px;">
																										<label class="last_maj" style="text-align:center;margin-left:10px;">Quantité</label>
																									</th>
																								</tr>
																								<tr class="prix_detail">
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="lib_prix-11-0" size="40" value="Prix du fond total" type="text" disabled>
																									</td>
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="montant_prix-11-0" size="8" value="1610.20" type="text" disabled><label class="label_simple">€</label>
																									</td>
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox" id="quantite_prix-11-0" size="8" value="1" type="text" disabled>
																									</td>
																									<td>
																										<label class="label"> = </label>
																									</td>
																									<td style="text-align:right;">
																										<label class="label">1 610.20 €</label>
																									</td>
																								</tr>																								
																								<tr>
																									<td colspan="4">
																										&nbsp;				
																									</td>
																									<td style="border-top:1px solid #ccc;text-align:right;">
																										<label class="label">1 610.20 €</label>
																									</td>
																								</tr>
																								<tr>
																									<td colspan="4" style="text-align:right;">
																										<label class="label"><u>Prix d'acquisition total opérateur :</u></label>
																									</td>
																									<td>
																										<input style="text-align:right;margin-left:10px;border:0" class="editbox parc_arrondi_prix" id="parc_arrondi_prix-11" size="8" value="1 600.00" type="text" disabled><label class="label">€</label>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																					<td>
																						<img id="img_save_prix-22" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;margin-left:10px;" width="15px">	
																					</td>
																					<td>
																						<div class="lib_alerte" id="prix_prob_saisie-22" style="display:none;white-space:pre-wrap;padding:10px">Veuillez vérifier votre saisie</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</fieldset>	
																</td>
															</tr>
															<tr>
																<td style="border-bottom:1px dashed #004494;" colspan="3">
																	&nbsp;
																</td>
															</tr>
															<tr>
																<td id="parc_td_general-17" class="lot_supprime">									
																	<table class="no-border" width="100%">
																		<tbody>
																			<tr>
																				<td style="text-align:center;" colspan="3">		
																					<label id='lbl_lot_suppr' class="label lot_supprime" style="margin-right:25px;font-size:11pt">LOT SUPPRIMÉ</label>
																				</td>
																			</tr>
																			<tr>
																				<td style="text-align:left;width:15px;padding-right:15px;" rowspan="3">
																					<img id="parc_btn_detail-17" src="<?php echo ROOT_PATH; ?>images/plus.png" width="15px">	
																				</td>
																				<td style="text-align:left;" colspan="2">									
																					<label id="parc_lbl_lot_id-17" class="label ">
																						<u>Lot :</u>Minas Tirith - 0B - 915 - 00A0001
																					</label>
																				</td>
																				
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lot ID :</u> 2490000B091500A0001
																					</label>
																				</td>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Lieu-dit :</u> AUX LITIERES                  
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Contenance :</u> 0.12.22
																					</label>
																				</td>
																				<td style="text-align:left;">
																					<label class="label ">
																						<u>Surface à maitriser :</u> 0.12.22
																					</label>	
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>																
														</tbody>
													</table>
												</td>
											</tr>
											<tr id="tr_parc_comm_eaf-9" style="">
												<td colspan="2" style="border-color:#ccc;border-width:1px 4px 4px 4px;border-style:solid;">
													<table class="no-border" cellspacing="0" cellpadding="2">
														<tbody>
															<tr>
																<td style="width:33%;text-align:center;">
																	<label class="label"><i>Commentaires sur l'EAF : </i></label>
																</td>
																<td style="text-align:center;white-space:nowrap;">
																	<textarea class="label textarea_com_edit" rows="1" cols="60" disabled></textarea>
																</td>
																<td style="width:15px;text-align:center;">
																	<img id="img_save_commentaires_eaf-9" src="<?php echo ROOT_PATH; ?>images/enregistrer.png" style="cursor:auto;opacity:0.3;" width="15px">	
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
								
				<div id="echanges" class="tabs-container tabs-hide" style="min-width: 0px;">
					<table style="border:0px solid black" width="100%" cellspacing="10" cellpadding="10">
						<tbody>
							<tr>
								<td>
									<center>
									<table style="border-collapse:collapse;" class="no-border" id="tbl_echanges" width="100%" cellspacing="5" cellpadding="5" border="0" align="CENTER">
										<tbody id="tbl_echanges_entete">
											<tr>
												<td class="filtre" style="text-align:left;width:1px;border-bottom:4px solid #004494;" colspan="2">
													<input style="text-align:center;border:0" class="editbox filtre_section" id="filtre_echange_section" size="8" placeholder="SECTION" onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';" type="text" disabled>
													<input style="text-align:center;border:0" class="editbox filtre_par_num" id="filtre_echange_par_num" size="8" placeholder="PAR NUM" onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';" type="text" disabled>
													<input class="editbox filtre_nom" style="border:0;margin-left:40px;" id="filtre_echange_nom" size="40" placeholder="Interlocuteur" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';" type="text" disabled>
													<img src="<?php echo ROOT_PATH; ?>images/supprimer.png">
												</td>
											</tr>
										</tbody>
										<tbody id="tbody_echanges_corps">				
											<tr id='tr_echange-1' style="">
												<td id='td_parc_echange-1' style="border-right: 1px solid #ccc; border-bottom: 4px solid #ccc">
													<table class="no-border" cellspacing="1" cellpadding="0">
														<tbody>
															<tr id='tr_echange_parc-1'>
																<td class="no-border " style="margin-top:5px;">
																	<label class="label">
																		0B-917-00A0001
																	</label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>			
												<td style="border-bottom: 4px solid #ccc">
													<table class="no-border" width="100%" cellspacing="1" cellpadding="0">
														<tbody>
															<tr id='tr_echange_ech-1'>
																<td>
																	<table class="no-border" style="border-collapse:collapse;" width="100%" cellspacing="0" cellpadding="5">
																		<tbody>
																			<tr>
																				<td id='td_interloc-1_echange-1' style="vertical-align:top;border-top:4px solid #f0c08d; border-left:4px solid #f0c08d; " width="30%">
																					<table id="tbl_17_16" class="no-border">
																						<tbody>
																							<tr id="tr_17_16" style="">
																								<td style="vertical-align:top;">
																									<img id='img_ech_new' style="margin:-10px 0;" src="images/echange.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Saisir un échange" width="30px">
																									<label class="label referent" style="color:#004494;">
																										M. PICSOU Balthazar 
																									</label>
																									<img id="echange_btn_detail-17_1_16" src="<?php echo ROOT_PATH; ?>images/plus.png" style="margin-left:3px;" width="12px">
																								</td>
																							</tr>																						
																							<tr>
																								<td>
																									<table class="no-border" cellspacing="0" cellpadding="0">
																										<tbody><tr>
																											<td style="padding-left:40px;">
																												<label class="label_simple" style="color:#004494;">
																													Véritable propriétaire
																												</label>
																											</td>
																										</tr>
																									</tbody></table>
																								</td>
																							</tr>	
																						</tbody>
																					</table>
																				</td>
																				<td style="border-bottom: solid #004494 1px; border-top:4px solid #f0c08d;border-right:4px solid #f0c08d; " width="70%">
																					<table class="no-border" id="tbl_echange_17_16_1">
																						<tbody>
																							<tr id="tr_echange_info_17_16_10" class="tr_echange_10">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">06/09/2017<br>Téléphone</label>			
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_10_17_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Retour sur le groupe et discussion avec M. Picsou sur les évolutions possibles.</textarea>									
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																									<img id='img_ech_edit' style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																									<br>
																									<img style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px">
																								</td>
																							</tr>
																							<tr id="tr_echange_nego_17_16_10" class="tr_echange_10">
																								<td>&nbsp;</td>
																								<td class="bilan-af">
																									<ul style="margin:0;"><li class="label_simple" style="color:#2b933e;">0B917 00A0001 : Accord pour une acquisition à 900 €</li>
																									</ul>
																								</td>
																							</tr>																							
																							<tr id="tr_echange_info_17_16_6" class="tr_echange_6">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">01/08/2017<br>Courrier individuel</label>
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_6_17_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Envoi d'informations concernant le projet d'AF. M. Picsou s'occupe de transmettre le projet aux personnes dont il est le référent.</textarea>
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																									<img style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																									<br>
																									<img style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px">
																								</td>
																							</tr>
																							<tr id="tr_echange_rappel_17_16_6" class="tr_echange_6">
																								<td style="text-align:right;vertical-align:top;padding-left:25px;">
																									<label class="label" style="margin:0 4px; white-space:pre-line;color:#fe0000;">A recontacter le<br>06/09/2017 : </label>
																								</td>
																								<td style="vertical-align:top;text-align:right;">
																									<textarea id="description_rappel_7_17_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;color:#fe0000;background:none;padding:0;" readonly="">Prendre connaissance des retours sur le projet du groupe référencé par M. Picsou.</textarea>
																								</td>
																							</tr>
																							<tr id="tr_echange_info_17_16_1" class="tr_echange_1">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">03/07/2017<br>Réunion de lancement</label>
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_1_17_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Bonne participation et grand intérêt de la part des personnes présentes</textarea>	
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td style="vertical-align:top;border-top: solid #004494 1px; border-left:4px solid #f0c08d; " width="30%">
																					<table id="tbl_11_16" class="no-border">
																						<tbody>
																							<tr id="tr_11_16" style="">
																								<td style="vertical-align:top;">
																									<img style="margin:-10px 0;" src="images/echange.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Saisir un échange" width="30px">
																									<label class="label " style="color:#7500be;">
																										M. DUCK Donald 
																									</label>
																									<img id="echange_btn_detail-11_10_16" src="<?php echo ROOT_PATH; ?>images/plus.png" style="margin-left:3px;" width="12px">
																								</td>
																							</tr>
																							<tr>
																								<td style="padding-top:0;padding-left:40px;">
																									<table class="no-border" cellspacing="0">
																										<tbody>
																											<tr>
																												<td>
																													<label class="label_simple" style="color:#7500be;">Curateur de :</label>
																												</td>
																											</tr>
																											<tr>
																												<td style="vertical-align:top;">
																													<table id="tbl_22_16" class="no-border" cellspacing="0">
																														<tbody>
																															<tr>
																																<td style="vertical-align:top;padding-top:0;padding-left:40px;">
																																	<label class="label" style="color:#004494;">
																																		Mme. BOOP Betty
																																	</label>
																																	<img id="echange_btn_detail-11_22_16" src="<?php echo ROOT_PATH; ?>images/plus.png"  width="12px">
																																</td>
																															</tr>
																															<tr>
																																<td style="vertical-align:top;padding-top:0;padding-left:50px;">
																																	<label class="label_simple" style="color:#004494;">
																																		Véritable propriétaire
																																	</label>
																																	<br>
																																</td>
																															</tr>									
																														</tbody>
																													</table>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>	
																						</tbody>
																					</table>
																				</td>
																				<td style="border-bottom: solid #004494 1px; border-right:4px solid #f0c08d; " width="70%">
																					<table class="no-border" id="tbl_echange_11_16_10">
																						<tbody>
																							<tr id="tr_echange_info_11_16_10" class="tr_echange_10">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">06/09/2017<br>Téléphone</label>																
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_10_11_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Retour sur le groupe et discussion avec M. Picsou sur les évolutions possibles.</textarea>
																									<br>
																									<label class="label_simple">Echange avec le référent</label>	
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																									<img style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px">
																								</td>
																							</tr>
																							<tr id="tr_echange_nego_11_16_10" class="tr_echange_10">
																								<td>&nbsp;</td>
																								<td class="bilan-af">
																									<ul style="margin:0;"><li class="label_simple" style="color:#004494;">0B917 00A0001 : Perspective d'acquisition à 1000 €</li>
																									</ul>
																								</td>
																							</tr>
																							<tr id="tr_echange_info_11_16_1" class="tr_echange_1">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">03/07/2017<br>Réunion de lancement</label>																
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_1_11_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Bonne participation et grand intérêt de la part des personnes présentes</textarea>	
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																								</td>
																							</tr>																							
																						</tbody>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td style="vertical-align:top;border-top: solid #004494 1px; border-left:4px solid #f0c08d; " width="30%">
																					<table id="tbl_12_16" class="no-border">
																						<tbody>
																							<tr id="tr_12_16" style="">
																								<td style="vertical-align:top;">
																									<img style="margin:-10px 0;" src="images/echange.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Saisir un échange" width="30px">
																									<label class="label " style="color:#004494;">
																										Mme. DUCK Daisy 
																									</label>
																									<img id="echange_btn_detail-12_1_16" src="<?php echo ROOT_PATH; ?>images/plus.png" style="margin-left:3px;" width="12px">
																								</td>
																							</tr>																					
																							<tr>
																								<td>
																									<table class="no-border" cellspacing="0" cellpadding="0">
																										<tbody>
																											<tr>
																												<td style="padding-left:40px;">
																													<label class="label_simple" style="color:#004494;">
																														Véritable propriétaire
																													</label>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>	
																						</tbody>
																					</table>
																				</td>
																				<td style="border-bottom: solid #004494 1px; border-right:4px solid #f0c08d; " width="70%">
																					<table class="no-border" id="tbl_echange_12_16_1">
																						<tbody>
																							<tr id="tr_echange_info_12_16_10" class="tr_echange_10">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">06/09/2017<br>Téléphone</label>																
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_10_12_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Retour sur le groupe et discussion avec M. Picsou sur les évolutions possibles.</textarea>	
																									<br>
																									<label class="label_simple">Echange avec le référent</label>
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																									<img style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px">
																								</td>
																							</tr>
																							<tr id="tr_echange_nego_12_16_10" class="tr_echange_10">
																								<td>&nbsp;</td>
																								<td class="bilan-af">
																									<ul style="margin:0;"><li class="label_simple" style="color:#2b933e;">0B917 00A0001 : Accord pour une acquisition à 900 €</li>
																									</ul>
																								</td>
																							</tr>		
																							<tr id="tr_echange_info_12_16_1" class="tr_echange_1">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">03/07/2017<br>Réunion de lancement</label>
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_1_12_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Bonne participation et grand intérêt de la part des personnes présentes</textarea>	
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																								</td>
																							</tr>																							
																						</tbody>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td style="vertical-align:top;border-top: solid #004494 1px; border-left:4px solid #f0c08d; border-bottom:4px solid #f0c08d;" width="30%">
																					<table id="tbl_30_16" class="no-border">
																						<tbody>
																							<tr id="tr_30_16" style="">
																								<td style="vertical-align:top;">
																									<img style="margin:-10px 0;" src="images/echange.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Saisir un échange" width="30px">
																									<label class="label " style="color:#2b5824;">
																										M. FUDD Elmer
																									</label>
																									<img id="echange_btn_detail-30_1_16" src="<?php echo ROOT_PATH; ?>images/plus.png" style="margin-left:3px;" width="12px">
																								</td>
																							</tr>
																							<tr>
																								<td>
																									<table class="no-border" cellspacing="0" cellpadding="0">
																										<tbody>
																											<tr>
																												<td style="padding-left:40px;">
																													<label class="label_simple" style="color:#2b5824;">
																														Chasseur (bail de chasse)	
																													</label>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>	
																						</tbody>
																					</table>
																				</td>
																				<td style="border-bottom: solid #004494 1px; border-right:4px solid #f0c08d; border-bottom:4px solid #f0c08d;" width="70%">
																					<table class="no-border" id="tbl_echange_30_16_1">
																						<tbody>
																							<tr id="tr_echange_info_30_16_10" class="tr_echange_10">
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<label class="label">06/09/2017<br>Téléphone</label>																
																								</td>
																								<td style="padding-top:10px;vertical-align:top;text-align:right;">
																									<textarea id="description_echange_10_30_16" class="label textarea_com_edit" cols="120" rows="1" style="font-weight:normal;min-height:5px;" readonly="">Retour sur le groupe et discussion avec M. Picsou sur les évolutions possibles.</textarea>	
																									<br>
																									<label class="label_simple">Echange avec le référent</label>
																								</td>
																								<td style="width:20px;padding-top:10px;vertical-align:middle;text-align:right;">
																									<img style="padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px">
																								</td>
																							</tr>
																							<tr id="tr_echange_nego_30_16_10" class="tr_echange_10">
																								<td>&nbsp;</td>
																								<td class="bilan-af">
																									<ul style="margin:0;"><li class="label_simple" style="color:#2b933e;">0B917 00A0001 : Favorable</li>
																									</ul>
																								</td>
																							</tr>	
																						</tbody>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td style="vertical-align:top;border-top:4px solid #f0c08d; " width="30%">
																					<table id="tbl_40_16" class="no-border">
																						<tbody>
																						<tr id="tr_40_16" style="">
																							<td style="vertical-align:top;">
																								<label class="label " style="color:#ccc;">
																									Mme. POPPINS Mary 
																								</label>
																									<label class="label_simple" style="color:#ccc;">(79 ans)</label>
																								<img id="echange_btn_detail-40_1_16" src="<?php echo ROOT_PATH; ?>images/plus.png" style="margin-left:3px;" width="12px">
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<label class="last_maj" style="color:red;">	
																									(Décédé)
																								</label>
																							</td>
																						</tr>																					
																						<tr>
																							<td>
																								<table class="no-border" cellspacing="0" cellpadding="0">
																									<tbody><tr>
																										<td style="padding-left:40px;">
																											<label class="label_simple" style="color:#004494;">
																												Véritable propriétaire
																											</label>
																										</td>
																									</tr>
																								</tbody></table>
																							</td>
																						</tr>	
																					</tbody></table>
																				</td>
																				<td width="70%">
																					<table class="no-border" id="tbl_echange_40_16_1">
																					</table>
																				</td>
																			</tr>			
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											
											<tr id='tr_echange-2' style="">
												<td style="border-right: 1px solid #ccc; border-bottom: 4px solid #ccc">
													<table class="no-border" cellspacing="1" cellpadding="0">
														<tbody>
															<tr>
																<td class="no-border " style="margin-top:5px;">
																	<label class="label">
																		0B-913
																	</label>
																</td>
															</tr>
															<tr>
																<td class="no-border " style="margin-top:5px;">
																	<label class="label">
																		0B-914-00A0004
																	</label>
																</td>
															</tr>
															<tr>
																<td class="no-border lot_supprime" style="margin-top:5px;">
																	<label class="label">
																		0B-915-00A0001
																	</label>
																</td>
															</tr>
														</tbody>
													</table>
												</td>			
												<td style="border-bottom: 4px solid #ccc">
													<table class="no-border" width="100%" cellspacing="1" cellpadding="0">
														<tbody>
															<tr>
																<td>
																	<table class="no-border" style="border-collapse:collapse;" width="100%" cellspacing="0" cellpadding="5">
																		<tbody>
																			<tr>
																				<td style="vertical-align:top;" width="30%">
																					<table id="tbl_41_10" class="no-border">
																						<tbody><tr id="tr_41_10" style="">
																							<td style="vertical-align:top;">
																								<img style="margin:-10px 0;" src="images/echange.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Saisir un échange" width="30px">
																								<label class="label " style="color:#2b5824;">
																									 ASSOCIATION DES GEEKS ANONYMES 
																								</label>
																								<img id="echange_btn_detail-41_3_10" src="<?php echo ROOT_PATH; ?>images/plus.png" style="margin-left:3px;" width="12px">
																							</td>
																						</tr>																					
																						<tr>
																							<td>
																								<table class="no-border" cellspacing="0" cellpadding="0">
																									<tbody>
																										<tr>
																											<td style="padding-left:40px;">
																												<label class="label_simple" style="color:#004494;">
																													Véritable propriétaire
																												</label>
																											</td>
																										</tr>
																										<tr>
																											<td style="padding-left:40px;">
																												<label class="label_simple" style="color:#2b5824;">
																													Agriculteur (contrat d'exploitation)
																												</label>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>	
																					</tbody></table>
																				</td>
																				<td width="70%">
																					
																				</td>
																			</tr>		
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div id="synthese" class="tabs-container">
					<table style="border:0px solid black" width="100%" cellspacing="10" cellpadding="10">
						<tbody><tr>
							<td>
								<center>
								<table class="no-border" id="tbl_synthese" width="100%" cellspacing="0" cellpadding="4" border="0" align="CENTER">
									<tbody id="tbody_synthese_evt_globaux">
										
									</tbody>
									<tbody id="tbody_synthese_bilan_af">
								
									</tbody>
									<tbody id="tbody_synthese_parc">
										<tr>
											<td>
												<table class="no-border" style="border-collapse:collapse;" width="100%">											
													<tbody id="tbody_synthese_parc_entete">														
														<tr>															
															<td class="filtre" style="text-align:left;width:1px;border-bottom:4px solid #004494;" colspan="2" width="90px">
																<input style="text-align:center;margin-left:40px;border:0" class="editbox filtre_section" id="filtre_synthese_section" size="8" placeholder="SECTION" onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';" type="text" disabled>
																<input style="text-align:center;border:0" class="editbox filtre_par_num" id="filtre_synthese_par_num" size="8" placeholder="PAR NUM" onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';" type="text" disabled>
																<img src="<?php echo ROOT_PATH; ?>images/supprimer.png">
															</td>
															<td style="text-align:left;width:1px;border-bottom:4px solid #004494;">
																&nbsp;
															</td>
															<td class="filtre" style="text-align:left;width:1px;border-bottom:4px solid #004494;" colspan="2">
																<input class="editbox filtre_nom" style="border:0;" id="filtre_synthese_nom" size="40" placeholder="Interlocuteur" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';" type="text" disabled>
																<img src="<?php echo ROOT_PATH; ?>images/supprimer.png">
															</td>
														</tr>
													</tbody>
													<tbody id="tbody_synthese_parc_corps">
														<tr id='tr_synthese_entete_prix'>
															<td colspan='2' style='text-align:right;'>
																<label id='lbl_synthese_prix_lot' class="last_maj" style="margin-right:15px;">Prix d'acquisition<br>total du lot</label>
															</td>
															<td style='text-align:center;'>
																<label id='lbl_synthese_prix_eaf' class="last_maj" style="margin-left:15px;">Prix d'acquisition<br>de l'EAF</label>
															</td>
															<td colspan='2'>
															</td>
														</tr>
														<tr id="tr_synthese_16">
															<td colspan="2" style="border-bottom:1px solid #004494;">
																<table id="tbl_parc_16" class="no-border">
																	<tbody>
																		<tr>
																			<td id="td_objectif_22" colspan="1" class="filtre_objectif" style="background:#747474;min-width:44px;border-radius:8px;text-align:center;">
																				<label id="label_objectif_22" class="label_simple" style="color:#ffffff;font-size:8pt;padding:0;">&nbsp;</label>
																				<input id="hid_label_objectif_22" value="N.D." type="hidden">
																				<input id="hid_color_objectif_22" value="#747474" type="hidden">
																			</td>
																			<td colspan="1" class="filtre_lot">
																				<label id="label_22" class="label_simple" style="margin-left:15px;color:#00b050;"><b>Minas Tirith - 0B - 917 - 00A0001</b>&nbsp;&nbsp;0.18.90
																				</label>
																				<input id="hid_color_paf_22" value="#00b050" type="hidden">
																			</td>
																			<td style="width:75px;height:25px;">
																				<img id="agri_22" style="display:none;" src="images/tracteur.png" width="24px">
																			
																				<img id="chasseur_22" style="display:none;" src="images/fusil.png" width="20px">
																			
																				<img id="pecheur_22" style="display:none;" src="images/canne-a-peche.png" width="25px">
																			</td>
																			<td id='td_synthese_prix_lot-1'style='text-align:right;'>
																				<label class="label_simple" style="margin-left:15px;">13 350.50 €</label>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td style="border-bottom:1px solid #004494;">																
															</td>
															<td style="border-bottom:1px solid #004494; vertical-align:middle;">
																<img id='img_info_eaf-1' src="<?php echo ROOT_PATH; ?>images/info.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Infos sur l'EAF" width="15px">
															</td>
															<td style="border-bottom:1px solid #004494;width:100%;">
																<table id="tbl_interloc_16" class="no-border" width="100%">
																	<tbody>
																		<tr>
																			<td id='td_synthese_interloc-1' class="filtre_proprio" width="1px">
																				<label class="label" style="margin-left:15px;color:#000000;">M. PICSOU Balthazar</label>					
																				<img id='img_info_interloc-1' src="<?php echo ROOT_PATH; ?>images/info.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Infos sur le propriétaire" width="15px">				
																			</td>
																			<td>
																				
																			</td>
																		</tr>
																		<tr>
																			<td id='td_synthese_interloc-2' class="filtre_proprio" width="1px">
																				<label class="label_simple" style="margin-left:30px;color:#000000;">M. DUCK Donald</label>					
																				<img src="<?php echo ROOT_PATH; ?>images/info.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Infos sur le propriétaire" width="15px">				
																			</td>
																			<td>
																				<table class="no-border" width="100%">
																				</table>	
																			</td>
																		</tr>
																		<tr>
																			<td id='td_synthese_interloc-3' class="filtre_proprio" width="1px">
																				<label class="label_simple" style="margin-left:30px;color:#000000;">Mme. DUCK Daisy</label>					
																				<img src="<?php echo ROOT_PATH; ?>images/info.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Infos sur le propriétaire" width="15px">				
																			</td>
																			<td>
																				<table class="no-border" width="100%">
																				</table>	
																			</td>
																		</tr>
																		<tr>
																			<td id='td_synthese_interloc-4' class="filtre_proprio" width="1px">
																				<label class="label_simple" style="margin-left:30px;color:#000000;">M. FUDD Elmer</label>					
																				<img src="<?php echo ROOT_PATH; ?>images/info.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Infos sur le propriétaire" width="15px">				
																			</td>
																			<td>
																				<table class="no-border" width="100%">
																				</table>	
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														<tr id="tr_synthese_22">
															<td colspan="2" style="border-bottom:1px solid #004494;">
																<table id="tbl_parc_22" class="no-border">
																	<tbody>
																		<tr>
																			<td id="td_objectif_11" colspan="1" class="filtre_objectif" style="background:#747474;min-width:44px;border-radius:8px;text-align:center;">
																				<label id="label_objectif_11" class="label_simple" style="color:#ffffff;font-size:8pt;padding:0;">&nbsp;</label>
																				<input id="hid_label_objectif_11" value="N.D." type="hidden">
																				<input id="hid_color_objectif_11" value="#747474" type="hidden">
																			</td>
																			<td colspan="1" class="filtre_lot">
																				<label id="label_11" class="label_simple" style="margin-left:15px;color:#00b050;"><b>Minas Tirith - 0B - 913</b>&nbsp;&nbsp;0.28.00
																				</label>
																				<input id="hid_color_paf_11" value="#00b050" type="hidden">
																			</td>
																			<td style="width:75px;height:25px;">
																				<img id="agri_11" style="display:none;" src="images/tracteur.png" width="24px">
																			
																				<img id="chasseur_11" style="display:none;" src="images/fusil.png" width="20px">
																			
																				<img id="pecheur_11" style="display:none;" src="images/canne-a-peche.png" width="25px">
																			</td>
																		</tr>
																		<tr>
																			<td id="td_objectif_15" colspan="1" class="filtre_objectif" style="background:#4472c4;min-width:44px;border-radius:8px;text-align:center;">
																				<label id="label_objectif_15" class="label_simple" style="color:#ffffff;font-size:8pt;padding:0;">&nbsp;</label>
																				<input id="hid_label_objectif_15" value="N.D." type="hidden">
																				<input id="hid_color_objectif_15" value="#4472c4" type="hidden">
																			</td>
																			<td colspan="1" class="filtre_lot">
																				<label id="label_15" class="label_simple" style="margin-left:15px;color:#00b050;"><b>Minas Tirith - 0B - 914 - 00A0004</b>&nbsp;&nbsp;0.04.00
																				</label>
																				<input id="hid_color_paf_15" value="#00b050" type="hidden">
																			</td>
																			<td style="width:75px;height:25px;">
																				<img id="agri_15" style="display:none;" src="images/tracteur.png" width="24px">
																			
																				<img id="chasseur_15" style="display:none;" src="images/fusil.png" width="20px">
																			
																				<img id="pecheur_15" style="display:none;" src="images/canne-a-peche.png" width="25px">
																			</td>	
																			<td>
																				
																			</td>
																		</tr>
																		<tr>
																			<td id="td_objectif_18" colspan="1" class="filtre_objectif" style="background:#747474;min-width:44px;border-radius:8px;text-align:center;">
																				<label id="label_objectif_18" class="label_simple" style="color:#ffffff;font-size:8pt;padding:0;">&nbsp;</label>
																				<input id="hid_label_objectif_18" value="N.D." type="hidden">
																				<input id="hid_color_objectif_18" value="#747474" type="hidden">
																			</td>
																			<td colspan="1" class="filtre_lot">
																				<label id="label_18" class="label_simple" style="margin-left:15px;color:#00b050;"><b>Minas Tirith - 0B - 915 - 00A0001</b>&nbsp;&nbsp;0.12.22
																				</label>
																				<input id="hid_color_paf_18" value="#00b050" type="hidden">
																			</td>
																			<td style="width:75px;height:25px;">
																				<img id="agri_18" style="display:none;" src="images/tracteur.png" width="24px">
																			
																				<img id="chasseur_18" style="display:none;" src="images/fusil.png" width="20px">
																			
																				<img id="pecheur_18" style="display:none;" src="images/canne-a-peche.png" width="25px">
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td id='td_synthese_prix_eaf-2'style="border-bottom:1px solid #004494;text-align:center;">
																<label class="label_simple">3 300.00 €</label>
															</td>
															<td style="border-bottom:1px solid #004494; vertical-align:middle;">
																<img id='img_info_eaf-2' src="<?php echo ROOT_PATH; ?>images/info.png" title="Infos sur l'EAF" width="15px">
															</td>
															<td style="border-bottom:1px solid #004494;">
																<table id="tbl_interloc_22" class="no-border" width="100%">
																	<tbody><tr>
																		<td class="filtre_proprio" width="1px">
																			<label class="label_simple" style="margin-left:15px;color:#000000;"> ASSOCIATION DES GEEKS ANONYMES</label>				
																		</td><td>
																			<table class="no-border" width="100%">
																			</table>	
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>													
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>	
								</table>
								</center>
							</td>
						</tr>
					</tbody></table>
					<div id="lgd_synthese" style="background-color: rgb(204, 204, 204) !important; border-color: rgb(106, 106, 106); border-radius: 10px; border-style: solid; border-width: 1px !important; box-shadow: rgb(106, 106, 106) 2px 1px 3px 0px; white-space: nowrap; position: fixed; right: 45px; bottom: 120px;">
						<div style="text-align:right;margin: 5px">
							<img  src="images/legend-add.png" width="20px">
						</div>
						<div id="ul_lgd_synthese" style="display:none;width:30px;height:35px;">
							<label class="label">Perspective</label>
							<ul style="list-style-type:none;margin:0;">
								<li style="background:#747474;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;">
									<label class="label_simple" style="color:#ffffff;">Pas de perspective</label>
								</li>
								<li style="background:rgba(0, 0, 0, 0) repeating-linear-gradient(45deg, #70ad47, #70ad47 5px, #4472c4 5px, #4472c4 10px) repeat scroll 0 0;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;">
									<label class="label_simple" style="color:#ffffff;">Perspective d'acte</label>
								</li>
								<li style="background:#70ad47;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;">
									<label class="label_simple" style="color:#ffffff;">Perspective de convention</label>
								</li>
								<li style="background:#4472c4;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;">
									<label class="label_simple" style="color:#ffffff;">Perspective d'acquisition</label>
								</li>
								<li style="background:#fe0000;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;">
									<label class="label_simple" style="color:#ffffff;">Refus</label>
								</li>
							</ul>
							<br>
							<label class="label">Priorité</label>
							<ul style="list-style-type:none;margin:0;">
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#fe0000;'>Forte</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#ffa025;'>Moyenne</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#00b050;'>Faible</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#000000;'>Pas de priorit&eacute;</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#747474;'>Lot bloqu&eacute;</label>
								</li>			
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<div id="explorateur_interloc_masque" class="explorateur_span" style="display: none;top:10px"></div>
			
			<div id="explorateur_evenement" class="explorateur_span" style="display:none;top:170px;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_evenement_sousdiv" class="no-border" style="text-align:right;position:relative;">
									<form class="form" onsubmit="return valide_form('frm_evenement')" name="frm_evenement" id="frm_evenement" method="POST" style="display:inline-block">
										<table style="width:590px;" class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="2" border="0" align="center">
											<tbody id="tbody_evenement_entete">
												<tr>
													<td colspan="2">
														<table class="no-border" width="100%">
															<tbody><tr>
																<td style="text-align:center;margin-left:20px">
																	<label id="evenement_lib" class="label" style="text-align:center;margin-left:20px">Nouvel événement</label>
																</td>
																<td style="text-align:right;width:25px;">	
																	<img src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
																</td>																	
															</tr>
														</tbody></table>
													</td>
											</tr></tbody>
											<tbody id="tbody_evenement_contenu">
												<tr>
													<td style="text-align:right">
														<label class="label"><u>Date de l'événement :</u></label>
													</td>
													<td style="text-align:left">
														<input size="10" class="datepicker hasDatepicker" id="date_evenement" name="date_evenement" type="text" disabled>
													</td>										
												</tr>
												<tr>
													<td style="text-align:right">
														<label class="label"><u>Type d'événement :</u></label>
													</td>
													<td style="text-align:left">															
														<label class="btn_combobox">			
															<select name="lst_type_evenement" id="lst_type_evenement" class="combobox" style="text-align:left">
																<option value="-1">- - - Type - - -</option>
																<option value="1">Réunion de lancement</option><option value="2">Réunion courante</option><option value="3">Courrier collectif</option><option value="4">Rencontre collective</option><option value="5">Autre</option>				
															</select>
														</label>													
													</td>									
												</tr>
												<tr>
													<td style="text-align:right">
														<label class="label"><u>Commentaire :</u></label>
													</td>
													<td style="text-align:left">	
														<textarea id="description_evenement" name="description_evenement" class="label textarea_com_edit" rows="1" cols="10"></textarea>
													</td>
												</tr>		
											</tbody>
											<tbody id="tbody_evenement_presents">	
												<tr>
													<td colspan="2" style="border-top:2px dashed #004494;padding-top:20px;text-align:center;">
														<label class="label">Personnes présentes</label>
													</td>
												</tr>
												<tr>
													<td colspan="2">
														<fieldset id="fld_evenement_presents" class="fieldset_af">
															<table style="border:0px solid black" width="100%" cellspacing="0" cellpadding="5" align="CENTER">
																<tbody id="tbody_tbl_evenement_tous-aucun">
																	<tr>
																		<td>
																			<input id="event_present_ALL" checked="" type="checkbox">
																		</td>
																		<td style="text-align:left;">
																			<label class="label">Tous/ Aucun</label>
																		</td>
																	</tr>
																</tbody>
																<tbody id="tbody_tbl_evenement_presents">
																	<tr>
																		<td>
																			<input id="event_present_40" class="event_present" checked="" type="checkbox">
																		</td>
																		<td style="text-align:left;">
																			<label class="label">ASSOCIATION DES GEEKS ANONYMES</label>
																			<img id='interloc_edit'style="cursor:pointer;padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																		</td>
																	</tr>
																	<tr id="tr_evt_com_40" style="display:none">
																		<td colspan="2">
																			<textarea id="txt_evt_com_40" class="label textarea_com_edit" cols="10" style="font-weight:normal;">R.A.S.</textarea>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<input id="event_present_8" class="event_present" checked="" type="checkbox">
																		</td>
																		<td style="text-align:left;">
																			<label class="label">Mme. BOOP Betty</label>
																			<img style="cursor:pointer;padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																		</td>
																	</tr>																	
																	<tr>
																		<td>
																			<input id="event_present_41" class="event_present" checked="" type="checkbox">
																		</td>
																		<td style="text-align:left;">
																			<label class="label">Mme. DUCK Daisy</label>
																			<img style="cursor:pointer;padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																		</td>
																	</tr>
																	
																	<tr>
																		<td>
																			<input id="event_present_7" class="event_present" checked="" type="checkbox">
																		</td>
																		<td style="text-align:left;">
																			<label class="label">M. DUCK Donald</label>
																			<img style="cursor:pointer;padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																		</td>
																	</tr>																	
																	<tr>
																		<td>
																			<input id="event_present_27" class="event_present" checked="" type="checkbox">
																		</td>
																		<td style="text-align:left;">
																			<label class="label">M. FUDD Elmer</label>
																			<img style="cursor:pointer;padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																		</td>
																	</tr>																	
																	<tr>
																		<td>
																			<input id="event_present_14" class="event_present" checked="" type="checkbox">
																		</td>
																		<td style="text-align:left;">
																			<label class="label">M. PICSOU Balthazar</label>
																			<img style="cursor:pointer;padding-left:5px;" src="<?php echo ROOT_PATH; ?>images/edit.png" width="15px">
																		</td>
																	</tr>																	
																</tbody>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>											
											<tbody id="tbody_evenement_pied">
												<tr>
													<td>
														<div class="lib_alerte" id="evenement_prob_saisie" style="width:580px;display:none;">Veuillez compléter votre saisie</div>
													</td>
													<td class="save">
														<input name="btn_valide_evenement" id="btn_valide_evenement" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer" type="button">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="confirmDeleteEAF" class="explorateur_span" style="display:none;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="confirmDeleteEAF_sousdiv" class="no-border" style="text-align:center;">
									<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="5" border="0" align="center">
										<tbody>
											<tr>
												<td colspan="2">
													<label id="msg_confirmDeleteEAF" class="label"></label>
												</td>
											</tr>
											<tr>
												<td style="padding-right:20px;text-align:right;">
													<input id="btn_confirmDeleteEAF_oui" class="btn_frm_help" style="width:60px"  value="Oui" type="button">
												</td>
												<td style="padding-left:20px;text-align:left;">
													<input id="btn_confirmDeleteEAF_non" class="btn_frm_help" style="width:60px" value="Non" type="button">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="confirmDeleteInterloc" class="explorateur_span" style="display:none;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="confirmDeleteInterloc_sousdiv" class="no-border" style="text-align:center;">
									<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="5" border="0" align="center">
										<tbody>
											<tr>
												<td colspan="2">
													<label class="label">Veuillez confirmer la suppression d'un interlocuteur</label>
												</td>
											</tr>
											<tr>
												<td style="padding-right:20px;text-align:right;">
													<input id="btn_confirmDeleteInterloc_oui" class="btn_frm_help" style="width:60px" value="Oui" type="button">
												</td>
												<td style="padding-left:20px;text-align:left;">
													<input id="btn_confirmDeleteInterloc_non" class="btn_frm_help" style="width:60px" value="Non" type="button">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_glossaire" class="explorateur_span" style="display:none;top:180px;position:fixed;z-index:40;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div style="text-align:center;top:0;height:100%">
									<table class="explorateur_contenu" style='width:100%;border-bottom:0;' cellspacing="1" cellpadding="5" border="0" align="center">
										<tbody>
											<tr>
												<td style="text-align:center;">
													<label class="label">
														Glossaire
													</label>
												</td>
												<td style="text-align:right;width:25px;">
													<img onclick="cacheMess('explorateur_glossaire');changeOpac(100, 'onglet');$('#onglet').css('height', 'auto');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
												</td>
											</tr>
											<tr>
												<td style="text-align:left;" colspan='2'>
													<label class="label">
														Recherche :
													</label>
													<input id='glossaire_search' size='60' type='text' oninput="fltGlossary('find');">
													<img onclick="fltGlossary('reset');" src="<?php echo ROOT_PATH; ?>images/supprimer.png">
												</td>
											</tr>
										</tbody>
									</table>
									<table id='glossaire_content' class="explorateur_contenu" style='width:100%;overflow-y:scroll;border-top:0;display:block;' cellspacing="1" cellpadding="5" border="0" align="center">
										<tbody id='tbody_saf' class='glossaire_tbody'>
											<tr>				
												<td style="text-align:left;white-space:normal;">
													<label id='title_saf' class="glossaire_titre label">
														Session d'Animation Foncière (SAF)
													</label>
													<br>
													<label class='label_simple' style="text-align:left;white-space:normal;">
														Session de travail définie par une date de lancement et une origine de création.<br>
														Elle regroupe un ensemble de parcelles favorables à une maîtrise foncière. Seules les parcelles déjà acquises ne peuvent pas être concernées par une SAF.<br>
														Une SAF peut être considérée comme complète ou aboutie lorsque TOUTES les parcelles qui ne sont pas en échec de maîtrise sont effectivement maîtrisées dans la Base de Données Foncières.
													</label>
												</td>	
											</tr>
										</tbody>
										<tbody id='tbody_eaf' class='glossaire_tbody'>											
											<tr>												
												<td style="text-align:left;white-space:normal;">
													<label id='title_eaf' class="glossaire_titre label">
														Entité d'Animation Foncière (EAF)
													</label>
													<br>
													<label class='label_simple' style="text-align:left;white-space:normal;">
														Ensemble de parcelles qui partagent rigoureusement la même liste d'interlocuteurs propriétaires.<br>
														Peut être comparée au compte de propriété à la grande différence qu'elle s'affranchie des limites administratives comme des démembrements ou des indivisions.<br>
														Une EAF est en perpétuelle évolution suivant les modifications apportées aux propriétaires.<br>
														Pour aller plus loin, au sein d'une même SAF, on peut très bien ne trouver qu'une partie des parcelles d'une EAF.
													</label>
												</td>
											</tr>
										</tbody>
										<tbody id='tbody_interloc' class='glossaire_tbody'>
											<tr>												
												<td style="text-align:left;white-space:normal;">
													<label id='title_interloc' class="glossaire_titre label">
														Interlocuteurs
													</label>
													<br>
													<label class='label_simple' style="text-align:left;white-space:normal;">
														Toutes personnes physiques ou morales pouvant être contactées pour faire évoluer de quelques manières que ce soit une SAF.<br>
														Il peut s'agir d'un véritable propriétaire ou d'un agriculteur exploitant une parcelle ou même d'un généalogiste permettant de retrouver un propriétaire inconnu.<br>
														Les interlocuteurs se répartissent en différents types permettant de décrire le rôle de celui-ci dans la SAF. A chaque type correspond des droits en termes de négociation et de représentation par rapport aux interlocuteurs au sein d'une EAF.<br>
														Pour aller plus loin, un même interlocuteur peut avoir plusieurs types au sein d'une même EAF et un même interlocuteur peut avoir des types différents sur des EAF ou des SAF différentes.<br>
														Quelques précisions concernant l'organisation des interlocuteurs au sein d'une EAF :
														<li style='margin-left:30px;'> le <b>référent</b> : Il s'agit d'un interlocuteur en capacité de négocier chargé de faire remonter les informations aux interlocuteurs qu'il référence. Son intérêt est de simplifier le travail de l'AF en limitant les échanges.</li>
														<li style='margin-left:30px;'>L'interlocuteur de <b>substitution</b> : il s'agit d'un individu pouvant s'exprimer au nom d'un autre. On distingue des interlocuteurs de substitution d'ordre "<i>juridique</i>" qui "<i>prennent</i>" la place du véritable propriétaire, des représentants qui ne peuvent que porter le message de l'interlocuteur qu'ils représentent.</li>
														<li style='margin-left:30px;'>Certains interlocuteurs (la plupart des ayants-droit) n'ont pas à donner légalement leur accord pour une négociation mais donnent un avis favorable ou non sur le projet proposé par l'animation foncière. Ceci dans l'objectif de maintenir un bon relationnel avec toutes les personnes liées à une activité sur les parcelles.
														</li><br>
														<table border='1px solid #004494' cellspacing="0" cellpadding="4" align='center'>
															<tr>
																<td colspan='2' style='text-align:center'><b>Groupes et/ou Types</td>
																<td style='text-align:center'><b>Négociation ou Avis</b></td>
																<td style='text-align:center'><b>Peut être référent</b></td>
																<td style='text-align:center'><b>Peut être référencé</b></td>
																<td style='text-align:center'><b>Peut substituer</b></td>
																<td style='text-align:center'><b>Peut être substitué</b></td>
															</tr>
															<tr>
																<td colspan='2'>Véritable propriétaire</td>
																<td style='text-align:center;'><label class='label_simple'>Négocie</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#2b5824' rowspan='6'>Ayants-droit</td>
																<td style='color:#2b5824'>Agriculteur (bail rural)</td>
																<td style='text-align:center;'><label class='label_simple'>Négocie</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#2b5824'>Agriculteur (contrat d'exploitation)</td>
																<td style='text-align:center;'><label class='label_simple'>Donne un avis</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#2b5824'>Agriculteur sans droit ni titre</td>
																<td style='text-align:center;'><label class='label_simple'>Donne un avis</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#2b5824'>Agriculteur sans contrat identifié</td>
																<td style='text-align:center;'><label class='label_simple'>Donne un avis</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#2b5824'>Chasseur (bail de chasse)</td>
																<td style='text-align:center;'><label class='label_simple'>Donne un avis</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#2b5824'>Pêcheur (bail de pêche)</td>
																<td style='text-align:center;'><label class='label_simple'>Donne un avis</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
															</tr>
															<tr>
																<td rowspan='5'>Interlocuteurs<br>de substitution</td>
																<td style='color:#000000;'>Représentant</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#7500be;'>Mandataire spécial</td>
																<td style='text-align:center;'><label class='label_simple'>Négocie</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#7500be;'>Curateur</td>
																<td style='text-align:center;'><label class='label_simple'>Négocie</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#7500be;'>Tuteur</td>
																<td style='text-align:center;'><label class='label_simple'>Négocie</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#7500be;'>Mandataire</td>
																<td style='text-align:center;'><label class='label_simple'>Négocie</label></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/valider.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;' rowspan='8'>Intermédiaires</td>
																<td style='color:#5a5a5a;'>Notaire</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;'>Généalogiste</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;'>Service de la Publicité Foncière</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;'>Archives cantonales (CH)</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;'>Contrôle des habitants (CH)</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;'>Office cantonal de la population et des migrations (CH)</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;'>Registre foncier (CH)</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
															<tr>
																<td style='color:#5a5a5a;'>Justice de paix (CH)</td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
																<td style='text-align:center;'><img src="<?php echo ROOT_PATH; ?>images/supprimer.png" width="15px"></td>
															</tr>
														</table>														
													</label>
												</td>
											</tr>
										</tbody>
										<tbody id='tbody_prix' class='glossaire_tbody'>											
											<tr>												
												<td style="text-align:left;white-space:normal;">
													<label id='title_prix' class="glossaire_titre label">
														Prix
													</label>
													<br>
													<label class='label_simple' style="text-align:left;white-space:normal;">
														Le système permet de gérer pas moins de 8 prix différents :
														<ul>
															<li>
																<u>Les prix au m² par type d'occupation du sol :</u><br>
																Il s'agit d'un prix d'acquisition au m² estimé en fonction du type d'occupation du sol.<br>
																Ce prix peut varier d'une parcelle à l'autre mais les variations se constatent plutôt sur des secteurs plus vastes.<br>
																Une prochaine version du module proposera, afin d'aider la saisie, une moyenne des prix sur le secteur concerné par la SAF.<br>
																<br>
																Cette information est à saisir dans le module pour chacune des parcelles dans l'onglet "<i>Parcelles/ Lots</i>" section "<i>Occupation du sol</i>".<br>
																Toutefois l'option "<i>Saisir des prix au m² par occupation du sol</i>" permet de renseigner en une fois un montant pour chaque occupation du sol présente dans l'EAF.<br>
																<br>
																Cette information permet de proposer le prix estimé du fond à partir de l'occupation du sol.
															</li>
															<br>
															<li>
																<u>Le prix estimé du fond à partir de l'occupation du sol :</u><br>
																A partir des données expliquées ci-dessus, un prix estimé du fond est calculé et indiqué automatiquement dès que la saisie de l'occupation du sol est complète.<br>
																L'idée étant d'aider la saisie d'un prix du fond dans la section "<i>Prix d'acquisition</i>".
															</li>
															<br>
															<li>
																<u>Les prix d'acquisition :</u><br>
																Il est possible de saisir une infinité de ligne mais il est fortement conseillé d'en inscrire au moins correspondant au prix du fond.<br>
																A noter qu'il est possible de préciser l'unité uniquement dans le libellé. 
															</li>
															<br>
															<li>
																<u>Le prix d'acquisition total calculé:</u><br>
																Calcul automatique de la somme des prix d'acquisition décrits ci-dessus.
															</li>
															<br>
															<li>
																<u>Le prix d'acquisition total opérateur :</u><br>
																Permet de renseigner un montant arrondi du prix d'acquisition total.
															</li>
															<br>
															<li>
																<u>Le prix d'acquisition total de l'EAF calculé :</u><br>
																Il s'agit de la somme des prix d'acquisition total de chaque lot de l'EAF.<br>
																S'il existe on somme en priorité le prix d'acquisition total opérateur, sinon on prend le prix d'acquisition total calculé.<br>
																Cette information n'est pas présente s'il manque un prix d'acquisition total sur au moins un lot dont l'objectif interne est un objectif d'acquisition. 
															</li>
															<br>
															<li>
																<u>Le prix d'acquisition total de l'EAF opérateur :</u><br>
																Permet de renseigner un montant arrondi du prix d'acquisition total de l'EAF.<br>
																<span style='color:#fe0000;'>Attention une EAF n'est pas figée dans le temps. A chaque évolution cette information est réinitialisée, donc perdue.</span>
															</li>
															<br>
															<li>
																<u>Prix d'acquisition de l'EAF en cours de négociation :</u><br>
																Il s'agit d'un prix d'acquisition total de l'EAF saisi manuellement lors d'un échange.<br>
																C'est une valeur fixe par EAF qui peut être modifiée à la volée sur n'importe quelle fiche d'échange.<br>
																Cette information est censée permettre une visualisation directe de ce qu'on est supposé proposer comme prix d'acquisition à un interlocuteur pour l'ensemble de l'EAF lors d'un échange direct, téléphonique par exemple.<br>
																<span style='color:#fe0000;'>Attention une EAF n'est pas figée dans le temps. A chaque évolution cette information est réinitialisée, donc perdue.</span>
															</li>
														</ul>
														<br>
														Remarque du développeur : "<i>J'ai averti à plusieurs reprises que tous ces prix risquent de rendre l'outil inutilisable. Une usine à gaz ne permet pas de travailler efficacement. Il a été proposé de n'avoir que des prix d'acquisition au m² et de mettre à disposition divers prix calculés automatiquement à partir de ceux-ci. J'ai prévenu sur les risques de stocker dans la base de données des prix relatifs aux EAF mais je n'ai pas été pris au serieux. Lorqu'un outil est créé pour de nombreuses personnes aux methodes de travail différentes, il faut savoir s'harmoniser et trouver le meilleur compromis. De mon point de vue, ça n'a pas été le cas lors des reflexions sur la conception de ce module. Déception...</i>"
													</label>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_interloc" class="explorateur_span" style="display:none;top:170px;left:0;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_new_interloc" style="text-align:left;margin-left:25px;">
									<form class="form" onsubmit="return valide_form('frm_new_interloc')" name="frm_new_interloc" id="frm_new_interloc" method="POST" style="display:inline-block">
										<input id="need_session_af_id" name="need_session_af_id" type="hidden">
										<input id="need_entite_af_id" name="need_entite_af_id" type="hidden">
										<input id="hid_parc_interloc" name="hid_parc_interloc" type="hidden">
										<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="5" border="0" align="center">
											<tbody>
												<tr>
													<td style="text-align:center;">
														<label class="label">
															Ajouter un interlocuteur
														</label>
													</td>
													<td style="text-align:right;">
														<img src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>		
													<td colspan="2">
														<fieldset id="fld_new_interloc_eaf" class="fieldset_af">
															<legend>Type d'interlocuteur pour l'EAF</legend>
															<table style="border:0px solid black" width="100%" cellspacing="2" cellpadding="5" align="CENTER">
																<tbody>
																	<tr>												
																		<td id="td_new_type_interloc" class="normal" style="text-align:left;white-space:nowrap;" colspan="2">
																			<label class="label">
																				En tant que :
																			</label>	
																			<label class="btn_combobox"> 
																				<select name="new_type_interloc" id="new_type_interloc" class="combobox" disabled>
																					<option value="-1">- - - Type d'interlocuteur - - -</option>				
																				</select>
																			</label>
																		</td>
																	</tr>
																</tbody>
															</table>
														</fieldset>
													</td>
												</tr>
											</tbody>
											<tbody>	
												<tr>
													<td style="text-align:left;white-space:nowrap;" colspan="2">
														<fieldset id="fld_import_proprio" class="fieldset_af">
															<legend>Sélectionnez un interlocuteur</legend>
															<table style="border:0px solid black" width="100%" cellspacing="0" cellpadding="5" align="CENTER">
																<tbody><tr>
																	<td style="text-align:center;">
																		<input size="32" class="editbox" name="flt_import_nom_usage" id="flt_import_nom_usage" placeholder="Filtrer sur le nom" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Filtrer sur le nom';" type="text" disabled>
																	
																		<input size="32" class="editbox" name="flt_import_prenom_usage" id="flt_import_prenom_usage" placeholder="Filtrer sur le prénom" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Filtrer sur le prénom';" type="text" disabled>
																	
																		<img id="img_flt_lst_proprio_import" name="img_flt_lst_proprio_import" src="<?php echo ROOT_PATH; ?>images/filtre.png" style="width:20px">
																	</td>
																</tr>	
																<tr>
																	<td id="td_import_proprio_lst_import_proprio" class="normal" colspan="3" style="text-align:center;">
																		<label class="btn_combobox"> 
																			<select name="lst_import_proprio" id="lst_import_proprio" class="combobox" style="width:300px;">
																			</select>
																		</label>
																	</td>
																</tr>
															</tbody></table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td style="text-align:center;" colspan="2">
														<label class="label">
															OU
														</label>
													</td>
												</tr>
											</tbody>
											<tbody>	
												<tr>
													<td colspan="2">
														<fieldset id="fld_new_interloc" class="fieldset_af">
															<legend>Ajouter un nouvel interlocuteur via le formulaire dédié</legend>
															<table style="border:0px solid black" width="100%" cellspacing="0" cellpadding="0" align="CENTER">
																<tbody><tr>
																	<td colspan="2">
																		<label class="last_maj">Personne physique</label>
																	</td>
																</tr>
																<tr>
																	<td colspan="2">
																		<table style="border:0px;float:none;" cellspacing="0" cellpadding="0" align="CENTER">
																			<tbody>
																				<tr>
																					<td id="td_new_interloc_lst_particule" class="normal" style="text-align:center;white-space:nowrap;">
																						<label class="btn_combobox">
																							<select name="new_interloc_lst_particule" id="new_interloc_lst_particule" class="combobox" disabled>
																								<option value="-1">---Particule---</option>
																							</select>
																						</label>
																						<input size="25" class="editbox" name="new_interloc_nom_usage" id="new_interloc_nom_usage" oninput="disableNewInterloc(this);" placeholder="Nom" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom';" type="text" disabled>
																						<input size="25" class="editbox" name="new_interloc_prenom_usage" id="new_interloc_prenom_usage" placeholder="Prénom" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Prénom';" type="text" disabled>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan="2" style="padding-top:10px;">
																		<label class="last_maj">Personne morale</label>
																	</td>
																</tr>
																<tr>
																	<td colspan="2">
																		<table style="border:0px;float:none;" cellspacing="0" cellpadding="0" align="CENTER">
																			<tbody><tr>
																				<td id="td_new_interloc_lst_particule" class="normal" style="text-align:center;white-space:nowrap;">
																					<input size="64" class="editbox" name="new_interloc_ddenom" id="new_interloc_ddenom" placeholder="Nom" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom';" type="text" disabled>				
																				</td>
																			</tr>
																		</tbody></table>
																	</td>
																</tr>
															</tbody></table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody id="tbody_new_parc_interloc">	
												<tr>
													<td style="text-align:left;white-space:nowrap;" colspan="2">
														<fieldset id="fld_new_parc_interloc" class="fieldset_af">
															<legend>Quelles sont les parcelles concernées ?</legend>
															<table style="border:0px solid black" width="100%" cellspacing="0" cellpadding="0" align="CENTER">
																<tbody>
																	<tr>
																		<td>
																			<input id="new_check_all_parc" checked="" type="checkbox">
																			<label class="last_maj">
																				Tous/ Aucun
																			</label>
																		</td>
																		<td class="only_proprio" style="text-align:center;">
																			<label class="last_maj">
																				Démembrement/ Indivision
																			</label>
																		</td>
																		<td class="only_proprio" style="text-align:center;">
																			<label class="last_maj">
																				Droit réel ou particulier
																			</label>
																		</td>
																	</tr>
																</tbody>
																<tbody id="tbody_check_parc_interloc">
																	<tr>
																		<td class="no-border" style="margin-top:5px;width:1%;">
																			<input id="chk_23" class="chk_parc" checked="" value="23" type="checkbox">
																			<label class="label">
																				Minas Tirith - 0B - 918 - 00A0001
																			</label>
																		</td>
																		<td class="only_proprio">
																			<label class="btn_combobox"> 
																				<select id="new_interloc_ccodem-23" class="combobox" style='width:160px' disabled>
																					
																				</select>
																			</label>
																		</td>
																		<td class="only_proprio">
																			<label class="btn_combobox"> 
																				<select id="new_interloc_ccodro-23" class="combobox" style='width:160px' disabled>
																					
																				</select>
																			</label>
																		</td>
																	</tr>
																	<tr>
																		<td class="no-border" style="margin-top:5px;width:1%;">
																			<input id="chk_27" class="chk_parc" checked="" value="27" type="checkbox">
																			<label class="label">
																				Minas Tirith - 0B - 918 - 00A0002
																			</label>
																		</td>
																		<td class="only_proprio">
																			<label class="btn_combobox"> 
																				<select id="new_interloc_ccodem-27" class="combobox" style='width:160px' disabled>
																					
																				</select>
																			</label>
																		</td>
																		<td class="only_proprio">
																			<label class="btn_combobox"> 
																				<select id="new_interloc_ccodro-27" class="combobox" style='width:160px' disabled>
																					
																				</select>
																			</label>
																		</td>
																	</tr>
																	<tr>
																		<td class="no-border" style="margin-top:5px;width:1%;">
																			<input id="chk_28" class="chk_parc" checked="" value="28" type="checkbox">
																			<label class="label">
																				Minas Tirith - 0B - 918 - 00A0003
																			</label>
																		</td>
																		<td class="only_proprio">
																			<label class="btn_combobox"> 
																				<select id="new_interloc_ccodem-28" class="combobox" style='width:160px' disabled>
																					
																				</select>
																			</label>
																		</td>
																		<td class="only_proprio">
																			<label class="btn_combobox"> 
																				<select id="new_interloc_ccodro-28" class="combobox" style='width:160px' disabled>
																					
																				</select>
																			</label>
																		</td>
																	</tr>																	
																</tbody>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td>
														<div class="lib_alerte" id="new_interloc_prob_saisie" style="width:580px;display:none;">Veuillez compléter votre saisie</div>
													</td>
													<td class="save">
														<input onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer" type="button">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_echange" class="explorateur_span" style="display:none;width:auto;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_echange_sousdiv" class="no-border" style="text-align:left;">
									<form class="form" onsubmit="return valide_form('frm_echange')" name="frm_echange" id="frm_echange" method="POST" style="display:inline-block">
										<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="2" border="0" align="center" style='padding:5px;'>
											<tbody id="tbody_echange_entete">
												<tr>
													<td colspan="2">
														<table class="no-border" width="100%">
															<tbody><tr>		
																<td style="text-align:left;width:25px;">	
																	<img id="img_reduce_explorateur_echange" src="<?php echo ROOT_PATH; ?>images/reduire.png" style="cursor:pointer" width="20px">
																	<input id="hid_echange_ddenom" value="" type="hidden">
																</td>
																<td style="text-align:center;margin-left:20px">
																	<label id="echange_interloc_lib" class="label" style="text-align:center;margin-left:20px"></label>
																</td>
																<td style="text-align:right;width:25px;">	
																	<img src="<?php echo ROOT_PATH; ?>images/quitter.png" width="15px">
																</td>																	
															</tr>
														</tbody></table>
													</td>
											</tr></tbody>
											<tbody id="tbody_echange_contenu">
												<tr>
													<td style="text-align:right">
														<label class="label"><u>Date de l'échange :</u></label>
													</td>
													<td style="text-align:left">
														<input size="10" class="datepicker hasDatepicker" id="date_echange" name="date_echange" type="text" value='06/09/2017' disabled>
													</td>										
												</tr>
												<tr>
													<td style="text-align:right">
														<label class="label"><u>Type d'échange :</u></label>
													</td>
													<td style="text-align:left">															
														<label class="btn_combobox">			
															<select name="lst_type_echange" id="lst_type_echange" class="combobox" style="text-align:left" disabled>
																<option value="-1">- - - Type - - -</option>
																<option value="1">Courrier individuel</option>
																<option value="2">Mail</option>
																<option selected value="3">Téléphone</option>
																<option value="4">Rencontre individuelle</option>				
															</select>
														</label>													
													</td>									
												</tr>
												<tr>
													<td style="text-align:right">
														<label class="label"><u>Description :</u></label>
													</td>
													<td style="text-align:left">	
														<textarea id="description_echange" name="description_echange" class="label textarea_com_edit" rows="1" cols="60">Retour sur le groupe et discussion avec M. Picsou sur les évolutions possibles.</textarea>
													</td>
												</tr>												
												<tr id="tr_chk_echange_referent" style="">
													<td colspan="2" style="text-align:center;">
														<input class="editbox" style="border-color:#fff;-moz-transform: scale(1.2);" id="chk_echange_referent" name="chk_echange_referent" type="checkbox" width="15px" checked>
														<label class="label"><u>Cet échange concerne tous les interlocuteurs référencés</u></label>
													</td>
												</tr>
												<tr id='tr_ech_rappel' style='display:none;'>
													<td colspan="2" style="padding:20px 0 0 0">
														<table class="no-border">
															<tbody>
																<tr>
																	<td style="text-align:right:padding-top:15px">
																		<label class="label"><u>A recontacter le :</u></label>
																	</td>
																	<td style="text-align:left">
																		<input size="10" style="border-color:#fff;" class="datepicker hasDatepicker" id="date_echange_recontact" name="date_echange_recontact" type="text" value='06/09/2017' disabled>
																	</td>	
																	<td style="text-align:right">
																		<label class="label"><u>Motif :</u></label>
																	</td>
																	<td style="text-align:left">	
																		<textarea id="motif_echange_recontact" name="motif_echange_recontact" class="label textarea_com_edit" rows="1" cols="30">Prendre connaissance des retours sur le projet du groupe référencé par M. Picsou.</textarea>
																	</td>
																	<td id="td_echange_rappel_traite" style="text-align:right">
																		<label id="echange_lbl_rappel_traite" class="label"><u>Traité :</u></label><input class="editbox" style="border-color:#fff;-moz-transform: scale(1.2);" id="echange_chk_rappel_traite" name="echange_chk_rappel_traite" type="checkbox" width="15px">
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
											<tbody id="tbody_echange_resultat">	
												<tr>
													<td colspan="2" style="border-top:2px dashed #004494;padding-top:10px;text-align:center;">
														<label class="label">Résultat de l'échange</label>
													</td>
												</tr>
												<tr style='display:none;'>
													<td colspan="2">
														<table>
															<tbody><tr>
																<td style="text-align:left;">
																	<input id="chk_attente_reponse" type="checkbox"><label id="lbl_attente_reponse" class="label">En attente d'une réponse</label>
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
												<tr style='display:none;'>
													<td colspan="2">
														<table>
															<tbody><tr>
																<td style="text-align:left;">
																	<input id="chk_interloc_injoignable" type="checkbox"><label id="lbl_interloc_injoignable" class="label">Interlocuteur injoignable</label>
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
												<tr style='display:none;'>
													<td colspan="2">
														<table>
															<tbody>
																<tr>
																	<td style="text-align:left;">
																		<input id="chk_modif_infos_interloc" type="checkbox"><label id="lbl_modif_infos_interloc" class="label">Modification des informations personnelles</label>
																	</td>
																</tr>
															</tbody>
															<tbody id="tbody_modif_infos_interloc" style='display:none;'>	
																<tr>
																	<td>
																		<fieldset id="fld_modif_infos_interloc" class="fieldset_af">			
																			<table style="border:0px solid black" width="100%" cellspacing="2" cellpadding="5" align="CENTER">
																				<tbody>
																					<tr id="tr_ech_gtoper-1" style="">
																						<td colspan="2">
																							<table style="padding-top:10px;border:0px;float:none;" cellspacing="0" cellpadding="0" align="CENTER">
																								<tbody><tr>
																									<td id="td_ech_update_interloc_lst_particule" class="normal" style="text-align:center;white-space:nowrap;">
																										<label class="btn_combobox">
																											<select name="ech_update_interloc_lst_particule" id="ech_update_interloc_lst_particule" class="combobox">
																												<option value="-1">---Particule---</option>
																												<option value="1">M.</option><option value="2">Mme.</option><option value="3">Mlle.</option>																										</select>
																										</label>
																										<input size="32" class="editbox" name="ech_update_interloc_nom_usage" id="ech_update_interloc_nom_usage" placeholder="Nom" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom';" type="text" disabled>
																										<input size="32" class="editbox" name="ech_update_interloc_prenom_usage" id="ech_update_interloc_prenom_usage" placeholder="Prénom" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Prénom';" type="text" disabled>
																									</td>
																								</tr>
																								<tr>
																									<td style="text-align:left;">
																										<label class="label">
																											Né(e) le : <input value="" size="10" placeholder="jj/mm/aaaa" onfocus="this.placeholder = '';" onblur="this.placeholder = 'jj/mm/aaaa';" class="editbox" name="ech_update_interloc_date_naissance" id="ech_update_interloc_date_naissance" type="text" disabled>
																										</label>
																									</td>	
																								</tr>
																								<tr style="margin-top:15px">
																									<td colspan="2">
																										<label class="label">
																											Epoux (se) de <input size="32" placeholder="Nom du conjoint" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du conjoint';" class="editbox" name="ech_update_interloc_nom_conjoint" id="ech_update_interloc_nom_conjoint" type="text" disabled>
																											<input size="32" placeholder="Prénom du conjoint" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Prénom du conjoint';" class="editbox" name="ech_update_interloc_prenom_conjoint" id="ech_update_interloc_prenom_conjoint" type="text" disabled>
																										</label>
																									</td>
																								</tr>
																								<tr>
																									<td>
																										<label class="label">
																											Nom de jeune fille : <input value="" size="32" placeholder="Nom de jeune fille" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom de jeune fille';" class="editbox" name="ech_update_interloc_nom_jeunefille" id="ech_update_interloc_nom_jeunefille" type="text" disabled>
																										</label>
																									</td>
																								</tr>		
																							</tbody></table>
																						</td>
																					</tr>
																					<tr id="tr_ech_gtoper-2" style="display:none;">
																						<td colspan="2">
																							<table style="padding-top:10px;border:0px;float:none;" cellspacing="0" cellpadding="0" align="CENTER">
																								<tbody><tr>
																									<td id="td_update_interloc_lst_particule" class="normal" style="text-align:center;white-space:nowrap;">
																										<input size="90" class="editbox" name="ech_update_interloc_nom_usage" id="ech_update_interloc_ddenom" type="text" disabled>
																									</td>
																								</tr>
																							</tbody></table>
																						</td>
																					</tr>
																					<tr>
																						<td>
																							<table style="padding-top:0px;border:0px; padding-left:50px" cellspacing="0" cellpadding="0" align="CENTER">
																								<tbody><tr>
																									<td colspan="2">
																										<table style="padding-top:0px;border:0px" cellspacing="0" cellpadding="0" align="CENTER">
																											<tbody><tr>
																												<td rowspan="4" style="vertical-align:top;">
																													<img src="<?php echo ROOT_PATH; ?>images/adresse.gif" width="20px">
																												</td>
																												<td>
																													<input size="60" class="editbox" id="ech_update_interloc_adresse1" name="ech_update_interloc_adresse1" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / Bâtiment" type="text" disabled>
																												</td>
																											</tr>
																											<tr>
																												<td style="text-align:right;">
																													<input size="60" class="editbox" id="ech_update_interloc_adresse2" name="ech_update_interloc_adresse2" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N°, Rue, Boite postale" type="text" disabled>
																												</td>	
																											</tr>
																											<tr>
																												<td style="text-align:right;">
																													<input size="60" class="editbox" id="ech_update_interloc_adresse3" name="ech_update_interloc_adresse3" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Complément" type="text" disabled>
																												</td>	
																											</tr>
																											<tr>
																												<td style="text-align:right;">
																													<input size="60" class="editbox" id="ech_update_interloc_adresse4" name="ech_update_interloc_adresse4" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal &amp; Ville" type="text" disabled>
																												</td>	
																											</tr>
																										</tbody></table>
																									</td>
																								</tr>
																								<tr>
																									<td style="vertical-align:bottom;">
																										<img src="<?php echo ROOT_PATH; ?>images/mail.png" width="20px">
																									</td>
																									<td style="vertical-align:middle;padding-top:20px;">
																										<input size="60" class="editbox" name="ech_update_interloc_mail" id="ech_update_interloc_mail" type="text" disabled>
																									</td>
																								</tr>
																							</tbody></table>
																						</td>
																						<td>
																							<table style="padding-top:0px;border:0px; padding-right:50px" cellspacing="0" cellpadding="0" align="CENTER">
																								<tbody><tr>
																									<td style="text-align:right">
																										<img src="<?php echo ROOT_PATH; ?>images/tel_fix.gif" width="20px">
																									</td>
																									<td style="white-space:nowrap;text-align:left;">
																										<input size="15" class="editbox" name="ech_update_interloc_tel_fix1" id="ech_update_interloc_tel_fix1" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(perso)</label>
																									</td>	
																								</tr>
																								<tr>
																									<td style="text-align:right">
																										<img src="<?php echo ROOT_PATH; ?>images/tel_fix.gif" width="20px">
																									</td>
																									<td style="white-space:nowrap;text-align:left;">
																										<input size="15" class="editbox" name="ech_update_interloc_tel_fix2" id="ech_update_interloc_tel_fix2" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(pro)</label>
																									</td>	
																								</tr>
																								<tr>
																									<td style="text-align:right">
																										<img src="<?php echo ROOT_PATH; ?>images/tel_portable.png" style="align:right" width="12px">
																									</td>
																									<td style="white-space:nowrap;text-align:left;">
																										<input size="15" class="editbox" name="ech_update_interloc_tel_portable1" id="ech_update_interloc_tel_portable1" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(perso)</label>
																									</td>	
																								</tr>
																								<tr>
																									<td style="text-align:right">
																										<img src="<?php echo ROOT_PATH; ?>images/tel_portable.png" width="12px">
																									</td>
																									<td style="white-space:nowrap;text-align:left;">
																										<input size="15" class="editbox" name="ech_update_interloc_tel_portable2" id="ech_update_interloc_tel_portable2" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(pro)</label>
																									</td>	
																								</tr>
																								<tr>
																									<td style="text-align:right">
																										<img src="<?php echo ROOT_PATH; ?>images/fax.png" width="20px">
																									</td>
																									<td style="white-space:nowrap;text-align:left;">
																										<input size="15" class="editbox" name="ech_update_interloc_tel_fax" id="ech_update_interloc_tel_fax" type="text" disabled>
																									</td>	
																								</tr>
																							</tbody></table>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</fieldset>		
																	</td>
																</tr>
															</tbody>
														</table>
													</td>													
												</tr>
												<tr>
													<td colspan="2">
														<table style="width:100%;text-align:center;">
															<tbody>
																<tr>
																	<td style="text-align:left;">
																		<input id="chk_maitrise_parc" type="checkbox" checked disabled><label id="lbl_maitrise_parc" class="label">Négociation sur la maîtrise des parcelles</label>
																	</td>
																</tr>
															</tbody>
															<tbody id="tbody_maitrise_parc" style="">
																<tr>
																	<td style="text-align:right;padding-right:15px;">																		
																		<label class="label_simple"><i>Prix d'acquisition de l'EAF en cours de négociation : </i></label>
																		<input id="ech_prix_eaf_nego" size="8" class="editbox" style="text-align:right;" type="text" value='850' disabled>
																		<label class="label">€</label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<fieldset id="fld_maitrise_parc" class="fieldset_af">
																			<table style="border:0px solid black" width="100%" cellspacing="0" cellpadding="5" align="CENTER">
																				<tbody>
																					<tr>
																						<td rowspan="2" style="border-bottom:1px solid #004494">
																							<label class="label_simple">PAR ID</label>
																						</td>
																						<td rowspan="2" style="border-bottom:1px solid #004494;border-right:2px solid #004494">
																							<label class="label_simple">LOT ID</label>
																						</td>
																						<td rowspan="2" style="border-bottom:1px solid #004494;border-right:2px solid #004494;">
																							<label class="label_simple">Refus</label>
																						</td>
																						<td colspan="3" style="border-right:2px solid #004494">
																							<label class="label_simple">Perspective</label>
																						</td>
																						<td colspan="2" style="border-right:2px solid #004494;">
																							<label class="label_simple">Accord</label>
																						</td>
																					</tr>
																					<tr>
																						<td style="border-bottom:1px solid #004494;">
																							<label class="label_simple">Acte</label>
																						</td>
																						<td style="border-bottom:1px solid #004494">
																							<label class="label_simple">Convention</label>
																						</td>
																						<td style="border-bottom:1px solid #004494;border-right:2px solid #004494">
																							<label class="label_simple">Acquisition</label>
																						</td>																	
																						<td style="border-bottom:1px solid #004494">
																							<label class="label_simple">Convention</label>
																						</td>
																						<td style="border-bottom:1px solid #004494;border-right:2px solid #004494;">
																							<label class="label_simple">Acquisition</label>
																						</td>	
																					</tr>
																				</tbody>
																				<tbody id="tbody_tbl_maitrise_parc">
																					<tr>
																						<td colspan="8">
																							<label class="label">M. PICSOU Balthazar  -  <i>Véritable propriétaire</i></label>
																						</td>
																					</tr>
																					<tr id="tr_31">
																						<td rowspan="1" style="border-bottom: 1px solid #004494;">
																							<label class="label_simple">0B917</label>
																						</td>
																						<td style="border-right:2px solid #004494;border-bottom: 1px solid #004494;">
																							<label class="label_simple">00A0001</label>
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_31 chk_referent ech_0" id="rad_result_ech_0_31" name="rad_result_ech_0_31" value="0_31" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_31 chk_referent ech_1" id="rad_result_ech_1_31" name="rad_result_ech_1_31" value="1_31" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_31 chk_referent ech_2" id="rad_result_ech_2_31" name="rad_result_ech_2_31" value="2_31" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_31 chk_referent ech_3" id="rad_result_ech_3_31" name="rad_result_ech_3_31" value="3_31" type="checkbox" disabled>
																							<input id="prix_result_ech_3_31" name="prix_result_ech_3_31" class="editbox chk_referent ech_3" style="text-align:center; border:0px none; background:#ccc; padding:1px; opacity:0.2;" readonly disabled size="8" value="" type="text"><label class="label_simple">€</label>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_31 chk_referent ech_4" id="rad_result_ech_4_31" name="rad_result_ech_4_31" value="4_31" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_31 chk_referent ech_5" id="rad_result_ech_5_31" name="rad_result_ech_5_31" value="5_31" type="checkbox" checked disabled>
																							<input id="prix_result_ech_5_31" name="prix_result_ech_5_31" class="editbox chk_referent ech_5" style="text-align:center; border:0px none; background:#ccc; padding:1px; opacity:1;" readonly disabled size="8" value="900" type="text"><label class="label_simple">€</label>	
																						</td>
																					</tr>
																					<tr class="display_tbl_parc_maitrise_other_eaf">
																						<td colspan="8" style="font-variant:petite-caps;padding-top:10px;border-top:4px solid #ccc;">
																							<label class="label"><u>Interlocuteurs référencés par M. PICSOU Balthazar</u></label>
																						</td>
																					</tr>
																					<tr class="display_tbl_parc_maitrise_other_eaf">
																						<td colspan="8">
																							<label class="label">M. DUCK Donald   -  <i>Curateur</i></label>
																						</td>
																					</tr>
																					<tr id="tr_34" class="display_tbl_parc_maitrise_other_eaf">
																						<td style="border-bottom: 1px solid #004494;">
																							<label class="label_simple">0B917</label>
																						</td>
																						<td style="border-right:2px solid #004494;border-bottom: 1px solid #004494;">
																							<label class="label_simple">00A0001</label>
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_34 chk_reference  ech_0" value="0_34" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_34 chk_reference  ech_1" value="1_34" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_34 chk_reference  ech_2" value="2_34" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_34 chk_reference  ech_3" value="3_34" type="checkbox" checked disabled>
																							<input id="prix_result_ech_3_34" name="prix_result_ech_3_34" class="editbox chk_reference ech_3" style="text-align:center; border:0px none; background:#ccc; padding:1px;" readonly disabled size="8" value="1000" type="text"><label class="label_simple">€</label>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_34 chk_reference  ech_4" value="4_34" type="checkbox" disabled>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_34 chk_reference  ech_5" value="5_34" type="checkbox" disabled>
																							<input class="editbox chk_reference ech_5" style="text-align:center; border:0px none; background:#ccc; padding:1px; opacity:0.2;" readonly disabled size="8" value="" type="text"><label class="label_simple">€</label>	
																						</td>
																					</tr>
																					<tr class="display_tbl_parc_maitrise_other_eaf">
																						<td colspan="8">
																							<label class="label">Mme. DUCK Daisy  -  <i>Véritable propriétaire</i>
																							</label>
																						</td>
																					</tr>
																					<tr id="tr_29" class="display_tbl_parc_maitrise_other_eaf">
																						<td style="border-bottom: 1px solid #004494;">
																							<label class="label_simple">0B917</label>
																						</td>
																						<td style="border-right:2px solid #004494;border-bottom: 1px solid #004494;">
																							<label class="label_simple">00A0001</label>
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_29 chk_reference  ech_0" value="0_29" type="checkbox">	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_29 chk_reference  ech_1" value="1_29" type="checkbox">	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_29 chk_reference  ech_2" value="2_29" type="checkbox">	
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_29 chk_reference  ech_3" value="3_29" type="checkbox">
																							<input class="editbox chk_reference ech_3" style="text-align:center; border:0px none; background:#ccc; padding:1px; opacity:0.2;" readonly disabled size="8" value="" type="text"><label class="label_simple">€</label>	
																						</td>
																						<td style="border-bottom: 1px solid #004494;">
																							<input class="rad_29 chk_reference  ech_4" value="4_29" type="checkbox">	
																						</td>
																						<td style="border-bottom: 1px solid #004494;border-right:2px solid #004494;">
																							<input class="rad_29 chk_reference  ech_5" value="5_29" type="checkbox" checked disabled>
																							<input id="prix_result_ech_5_29" name="prix_result_ech_5_29" class="editbox chk_reference ech_5" style="text-align:center; border:0px none; background:#ccc; padding:1px;" readonly disabled size="8" value="900" type="text"><label class="label_simple">€</label>	
																						</td>
																					</tr>
																					<tr class="display_tbl_parc_maitrise_other_eaf">
																						<td colspan="8">
																							<label class="label">M. FUDD Elmer  -  <i>Chasseur (bail de chasse) </i>
																							</label>
																						</td>
																					</tr>
																					<tr id="tr_8" style="" class="display_tbl_parc_maitrise_other_eaf">
																						<td style="border-bottom: 1px solid #004494;">
																							<label class="label_simple">0B917</label>
																						</td>
																						<td style="border-right:2px solid #004494;border-bottom: 1px solid #004494;">
																							<label class="label_simple">00A0001</label>
																						</td>
																						<td style="border-bottom: 1px solid #004494;" colspan="4">
																							&nbsp;
																						</td>
																						<td style="border-bottom: 1px solid #004494; border-right:2px solid #004494;" colspan="2">
																							<input class="rad_8 chk_reference ech_6" id="rad_result_ech_6_8" name="rad_result_ech_6_8" value="6_8" type="checkbox" checked disabled>
																							<label class="label_simple">Favorable</label>
																							<input class="rad_8 chk_reference ech_7" id="rad_result_ech_7_8" name="rad_result_ech_7_8" value="7_8"type="checkbox" disabled>
																							<label class="label_simple">Défavorable</label>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</fieldset>	
																	</td>
																</tr>
															</tbody>
														</table>
													</td>		
												</tr>
											</tbody>											
											<tbody id="tbody_echange_pied">
												<tr>
													<td>
														<div class="lib_alerte" id="echange_prob_saisie" style="width:580px;display:none;">Veuillez compléter votre saisie</div>
													</td>
													<td class="save">
														<input name="btn_valide_echange" id="btn_valide_echange" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer" type="button">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="confirmDeleteEchange" class="explorateur_span" style="display:none;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="confirmDeleteEchange_sousdiv" class="no-border" style="text-align:center;">
									<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="5" border="0" align="center">
										<tbody>
											<tr>
												<td>
													<label class="label">Veuillez confirmer la suppression d'un échange</label>
												</td>
											</tr>
											<tr>
												<td>
													<label class="label_simple" id="lbl_resume_delete_echange"></label>
												</td>
											</tr>
											<tr>
												<td style="padding-right:20px;text-align:center;">
													<input id="btn_confirmDeleteEchange_one" class="btn_frm_help" style="width:60px" value="Oui" type="button">
													<input id="btn_confirmDeleteEchange_all" class="btn_frm_help" style="width:120px" value="Oui pour tous" type="button">
													<input id="btn_confirmDeleteEchange_non" class="btn_frm_help" style="width:60px" value="Non" type="button">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_update_interloc" class="explorateur_span" style="display: none;top:220px">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_sousdiv_update_interloc" style="text-align:center;">
									<form class="form" onsubmit="return false" name="frm_update_interloc" id="frm_update_interloc" method="POST" style="display:inline-block">
										<input id="update_interloc_id" name="update_interloc_id" type="hidden">
										<input id="hid_update_parc_interloc" name="hid_update_parc_interloc" type="hidden">
										<input id="hid_update_entite_af_id" name="hid_update_entite_af_id" type="hidden">
										<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="5" border="0" align="center">
											<tbody>
												<tr>
													<td style="text-align:center;">
														<label class="label" id="update_interloc_interloc_lib"></label>
													</td>
													<td style="text-align:right;width:25px;">
														<img src="<?php echo ROOT_PATH; ?>images/quitter.png" width="15px">
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td colspan="2">
														<fieldset id="fld_update_interloc_eaf" class="fieldset_af">
															<legend>Type d'interlocuteur pour l'EAF</legend>
															<table style="border:0px solid black" width="100%" cellspacing="2" cellpadding="5" align="CENTER">
																<tbody><tr>
																	<td style="text-align:right;white-space:nowrap;">
																		<label class="label">
																			En tant que :
																		</label>	
																	</td>
																	<td id="td_update_type_interloc" class="normal" style="text-align:left;white-space:nowrap;" colspan="2">
																		<label class="btn_combobox"> 
																			<select name="update_type_interloc" id="update_type_interloc" class="combobox">
																				<option value="-1">- - - Type d'interlocuteur - - -</option>
																				<option class="not_all_eaf" value="1">Véritable propriétaire</option><option class="not_all_eaf" value="2">Agriculteur (bail rural)</option><option class="not_all_eaf" value="3">Agriculteur (contrat d'exploitation)</option><option class="not_all_eaf" value="4">Agriculteur sans droit ni titre</option><option class="not_all_eaf" value="5">Agriculteur sans contrat identifié</option><option class="not_all_eaf" value="6">Chasseur (bail de chasse)</option><option class="not_all_eaf" value="7">Pêcheur (bail de pêche)</option><option class="not_all_eaf" value="8">Représentant</option><option class="all_eaf" value="9">Mandataire spécial</option><option class="all_eaf" value="10">Curateur</option><option class="all_eaf" value="11">Tuteur</option><option class="all_eaf" value="12">Mandataire</option><option class="all_eaf" value="13">Notaire</option><option class="all_eaf" value="14">Généalogiste</option><option class="all_eaf" value="15">Service de la Publicité Foncière</option><option class="all_eaf" value="16">Archives cantonales (CH)</option><option class="all_eaf" value="17">Contrôle des habitants (CH)</option><option class="all_eaf" value="18">Office cantonal de la population et des migrations (CH)</option><option class="all_eaf" value="19">Registre foncier (CH)</option><option class="all_eaf" value="20">Justice de paix (CH)</option>																			</select>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td style="text-align:right;white-space:nowrap;">
																		<label class="label">
																			Etat :
																		</label>	
																	</td>
																	<td id="td_update_etat_interloc" class="normal" style="text-align:left;white-space:nowrap;">
																		<label class="btn_combobox"> 
																			<select id="update_etat_interloc" class="combobox">											
																				<option value="1">Actif</option><option value="2">Introuvable</option><option value="3">Injoignable</option><option value="4">Décédé</option>																			</select>
																		</label>
																	</td>
																	<td style="text-align:left;width:100%;">
																		<label class="lib_alerte" style="font-weight:normal;">La modification de cette information peut entraîner<br>des changements dans <b>toutes</b> les SAF en cours</label>
																	</td>
																</tr>
															</tbody></table>	
														</fieldset>
													</td>
												</tr>
											</tbody>										
											<tbody>	
												<tr>
													<td colspan="2">
														<fieldset id="fld_update_interloc" class="fieldset_af">
															<legend>Données personnelles de l'interlocuteur</legend>
															<table style="border:0px solid black" width="100%" cellspacing="2" cellpadding="0" align="CENTER">
																<tbody><tr id="tr_gtoper-1" style="">
																	<td colspan="2">
																		<table style="padding-top:10px;border:0px;float:none;" cellspacing="2" cellpadding="0" align="CENTER">
																			<tbody><tr>
																				<td id="td_update_interloc_lst_particule" class="normal" style="text-align:center;white-space:nowrap;">
																					<label class="btn_combobox">
																						<select name="update_interloc_lst_particule" id="update_interloc_lst_particule" class="combobox">
																							<option value="-1">---Particule---</option>
																							<option value="1">M.</option><option value="2">Mme.</option><option value="3">Mlle.</option>																						</select>
																					</label>
																					<input size="32" class="editbox" name="update_interloc_nom_usage" id="update_interloc_nom_usage" type="text" disabled>
																					<input size="32" class="editbox" name="update_interloc_prenom_usage" id="update_interloc_prenom_usage" type="text" disabled>
																				</td>
																			</tr>
																			<tr>
																				<td style="text-align:left;">
																					<label class="label">
																						Né(e) le : <input value="" size="10" placeholder="jj/mm/aaaa" onfocus="this.placeholder = '';" onblur="this.placeholder = 'jj/mm/aaaa';" class="editbox" name="update_interloc_date_naissance" id="update_interloc_date_naissance" type="text" disabled>
																					</label>
																					<label class='label'>
																						&agrave; : <input type='text' value="" size='40' class='editbox' name='update_interloc_lieu_naissance' id='update_interloc_lieu_naissance'>
																					</label>
																				</td>	
																			</tr>
																			<tr style="margin-top:15px">
																				<td colspan="2">
																					<label class="label">
																						Epoux (se) de <input size="32" placeholder="Nom du conjoint" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du conjoint';" class="editbox" name="update_interloc_nom_conjoint" id="update_interloc_nom_conjoint" type="text" disabled>
																						<input size="32" placeholder="Prénom du conjoint" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Prénom du conjoint';" class="editbox" name="update_interloc_prenom_conjoint" id="update_interloc_prenom_conjoint" type="text" disabled>
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<label class="label">
																						Nom de jeune fille : <input value="" size="32" placeholder="Nom de jeune fille" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom de jeune fille';" class="editbox" name="update_interloc_nom_jeunefille" id="update_interloc_nom_jeunefille" type="text" disabled>
																					</label>
																				</td>
																			</tr>		
																		</tbody></table>
																	</td>
																</tr>
																<tr id="tr_gtoper-2" style="display:none;">
																	<td colspan="2">
																		<table style="padding-top:10px;border:0px;float:none;" cellspacing="2" cellpadding="0" align="CENTER">
																			<tbody><tr>
																				<td class="normal" style="text-align:center;white-space:nowrap;">
																					<input size="90" class="editbox" name="update_interloc_ddenom" id="update_interloc_ddenom" type="text" disabled>
																				</td>
																			</tr>
																		</tbody></table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table style="padding-top:0px;border:0px; padding-left:50px" cellspacing="0" cellpadding="0" align="CENTER">
																			<tbody><tr>
																				<td colspan="2">
																					<table style="padding-top:0px;border:0px" cellspacing="2" cellpadding="0" align="CENTER">
																						<tbody><tr>
																							<td rowspan="4" style="vertical-align:top;">
																								<img src="<?php echo ROOT_PATH; ?>images/adresse.gif" width="20px">
																							</td>
																							<td>
																								<input size="60" class="editbox" id="update_interloc_adresse1" name="update_interloc_adresse1" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / Bâtiment" type="text" disabled>
																							</td>
																						</tr>
																						<tr>
																							<td style="text-align:right;">
																								<input size="60" class="editbox" id="update_interloc_adresse2" name="update_interloc_adresse2" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N°, Rue, Boite postale" type="text" disabled>
																							</td>	
																						</tr>
																						<tr>
																							<td style="text-align:right;">
																								<input size="60" class="editbox" id="update_interloc_adresse3" name="update_interloc_adresse3" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Complément" type="text" disabled>
																							</td>	
																						</tr>
																						<tr>
																							<td style="text-align:right;">
																								<input size="60" class="editbox" id="update_interloc_adresse4" name="update_interloc_adresse4" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal &amp; Ville" type="text" disabled>
																							</td>	
																						</tr>
																					</tbody></table>
																				</td>
																			</tr>
																			<tr>
																				<td style="vertical-align:bottom;">
																					<img src="<?php echo ROOT_PATH; ?>images/mail.png" width="20px">
																				</td>
																				<td style="vertical-align:middle;padding-top:20px;">
																					<input size="60" class="editbox" name="update_interloc_mail" id="update_interloc_mail" type="text" disabled>
																				</td>
																			</tr>
																		</tbody></table>
																	</td>
																	<td>
																		<table style="padding-top:0px;border:0px; padding-right:50px" cellspacing="2" cellpadding="0" align="CENTER">
																			<tbody><tr>
																				<td style="text-align:right">
																					<img src="<?php echo ROOT_PATH; ?>images/tel_fix.gif" width="20px">
																				</td>
																				<td style="white-space:nowrap;text-align:left;">
																					<input size="15" class="editbox" name="update_interloc_tel_fix1" id="update_interloc_tel_fix1" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(perso)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style="text-align:right">
																					<img src="<?php echo ROOT_PATH; ?>images/tel_fix.gif" width="20px">
																				</td>
																				<td style="white-space:nowrap;text-align:left;">
																					<input size="15" class="editbox" name="update_interloc_tel_fix2" id="update_interloc_tel_fix2" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(pro)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style="text-align:right">
																					<img src="<?php echo ROOT_PATH; ?>images/tel_portable.png" style="align:right" width="12px">
																				</td>
																				<td style="white-space:nowrap;text-align:left;">
																					<input size="15" class="editbox" name="update_interloc_tel_portable1" id="update_interloc_tel_portable1" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(perso)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style="text-align:right">
																					<img src="<?php echo ROOT_PATH; ?>images/tel_portable.png" width="12px">
																				</td>
																				<td style="white-space:nowrap;text-align:left;">
																					<input size="15" class="editbox" name="update_interloc_tel_portable2" id="update_interloc_tel_portable2" type="text" disabled><label class="label" style="font-weight:normal;font-size:8pt;">(pro)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style="text-align:right">
																					<img src="<?php echo ROOT_PATH; ?>images/fax.png" width="20px">
																				</td>
																				<td style="white-space:nowrap;text-align:left;">
																					<input size="15" class="editbox" name="update_interloc_tel_fax" id="update_interloc_tel_fax" type="text" disabled>
																				</td>	
																			</tr>
																		</tbody></table>
																	</td>
																</tr>
																<tr>
																	<td colspan="2">
																		<table style="padding-top:10px;border:0px;float:none;" cellspacing="2" cellpadding="0" align="CENTER">
																			<tbody><tr>
																				<td style="text-align:right;white-space:nowrap;">
																					<label class="label">
																						Commentaires :
																					</label>	
																				</td>
																				<td style="text-align:center;white-space:nowrap;">
																					<textarea id="update_interloc_commentaires" class="label textarea_com_edit" rows="1" cols="60"></textarea>
																				</td>
																			</tr>
																		</tbody></table>
																	</td>
																</tr>
															</tbody></table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody id="tbody_update_parc_interloc">	
												<tr>
													<td style="text-align:left;white-space:nowrap;" colspan="2">
														<fieldset id="fld_update_parc_interloc" class="fieldset_af">
															<legend>Parcelles associées à l'interlocuteur</legend>
															<table style="border:0px solid black" width="100%" cellspacing="0" cellpadding="0" align="CENTER">
																<tbody>
																	<tr>
																		<td>
																			<input id="update_check_all_parc" checked="" type="checkbox" disabled>
																			<label class="last_maj">
																				Tous/ Aucun
																			</label>
																		</td>
																		<td class="only_proprio" style="text-align:center;">
																			<label class="last_maj">
																				Démembrement/ Indivision
																			</label>
																		</td>
																		<td class="only_proprio" style="text-align:center;">
																			<label class="last_maj">
																				Droit réel ou particulier
																			</label>
																		</td>
																	</tr>
																</tbody>
																<tbody id="tbody_update_check_parc_interloc">
																
																</tbody>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td>
														<div class="lib_alerte" id="update_interloc_prob_saisie" style="width:580px;display:none;">Veuillez compléter votre saisie</div>
													</td>
													<td class="save">
														<input name="btn_valide_update_interloc" id="btn_valide_update_interloc" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer" type="button">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_gestion_referent_sousdiv" style="text-align:center;width:750px;display:none;position:absolute;top:200px;left:80px;">
				<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="2" border="0" align="center">
					<tbody>
						<tr>		
							<td style="text-align:left;">	
								<label class="last_maj">
									Pour passer un interlocuteur en référent vous devez cochez la case correspondante.<br>
									Vous pouvez ensuite paramétrer les interlocuteurs par cliquer/ déplacer.
								</label>
								<br>
								<label class="last_maj" style="color:red;">
									Attention seuls les interlocuteurs associés à l'ensemble des parcelles de l'EAF peuvent être référent.
								</label>
							</td>												
							<td style="text-align:right;width:25px;vertical-align:top;">	
								<img src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table id="tbl_gestRef" width="100%">
									<tr>
										<td style="text-align:center;">
											<center>
												<div id="dropper_ref" class="dropper" style="cursor:default;float:unset">	
													<div class="drop_title">Référent</div>
													<div id="div_11" class="draggable">
														<input id="ref_11" class="checkRef" style="float:left;" type="checkbox">
															M. DUCK Donald
															<br>
															<label class="last_maj">Curateur</label>
													</div>
													<div id="div_17" class="draggable">
														<input id="ref_17" class="checkRef" style="float:left;" type="checkbox">
															M. PICSOU Balthazar
															<br>
															<label class="last_maj">Véritable propriétaire</label>
													</div>
													<div id="div_30" class="draggable">
														<input id="ref_30" class="checkRef" style="float:left;" type="checkbox">
															M. FUDD Elmer
															<br>
															<label class="last_maj">Chasseur (bail de chasse)</label>
													</div>
													<div id="div_39" class="draggable">
														<input id="ref_30" class="checkRef" style="float:left;" type="checkbox">
															Mme. DUCK Daisy
															<br>
															<label class="last_maj">Véritable propriétaire</label>
													</div>																			
												</div>
											</center>
										</td>
									</tr>
									<tr>
										<td style="text-align:center;">
											<center>
													<div id="dropper_11" class="dropper" style="display:none">
														<div class="drop_title">
															M. DUCK Donald
															<br>
															<label class="last_maj">Curateur</label>
														</div>
													</div>
													<div id="dropper_17" class="dropper" style="display:none">
														<div class="drop_title">
															M. PICSOU Balthazar
															<br>
															<label class="last_maj">Véritable propriétaire</label>
														</div>
														<div id="div_11" class="draggable" style='display:none;'>
															M. DUCK Donald
															<br>
															<label class="last_maj">Curateur</label>								
														</div>
														<div id="div_30" class="draggable" style='display:none;'>
															M. FUDD Elmer
															<br>
															<label class="last_maj">Chasseur (bail de chasse)</label>								
														</div>
														<div id="div_40" class="draggable" style='display:none;'>
															Mme. DUCK Daisy
															<br>
															<label class="last_maj">Véritable propriétaire</label>								
														</div>
													</div>
													<div id="dropper_30" class="dropper" style="display:none">
														<div class="drop_title">
															M. FUDD Elmer
															<br>
															<label class="last_maj">Chasseur (bail de chasse)</label>
														</div>
													</div>
													<div id="dropper_39" class="dropper" style="display:none">
														<div class="drop_title">
															Mme. DUCK Daisy
															<br>
															<label class="last_maj">Véritable propriétaire</label>
														</div>
													</div>
											</center>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:right;">
								<label class="lib_alerte">Les modifications sont enregistrées automatiquement dans le système</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_gestion_substitution_sousdiv" style="text-align:center;width:750px;display:none;position:absolute;top:200px;left:80px;">
				<table class="explorateur_contenu" frame="box" rules="NONE" width="100%" cellspacing="0" cellpadding="2" border="0" align="center">
					<tbody>
						<tr>		
							<td style="text-align:left;">	
								<label class="last_maj">
									Vous pouvez associer des représentants par cliquer/ déplacer.
								</label>
							</td>
							<td style="text-align:right;width:25px;vertical-align:top;">	
								<img src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table id="tbl_gestSubst" width="100%">
									<tr>
										<td style="text-align:center;">
											<center>
												<div id="dropper_subst" class="dropper" style="cursor:default;float:unset">	
													<div class="drop_title">Représentants identifiés</div>
													<div id="div_11_10" class="draggable 4" draggable="true">
														M. DUCK Donald
														<br>
														<label class="last_maj">Curateur</label>
													</div>															
													<div id="div_40_91" class="draggable 4" draggable="true">
														Mme. POPPINS Mary
														<br>
														<label class="last_maj">Mandataire spécial</label>
													</div>
												</div>
											</center>
										</td>
									</tr>
									<tr>
										<td style="text-align:center;">
											<center>
											<div id="dropper_12_1" class="dropper">
												<div class="drop_title">
													Mme. DUCK Daisy
													<br>
													<label class="last_maj">Véritable propriétaire</label>
												</div>															
											</div>
											<div id="dropper_22_1" class="dropper">
												<div class="drop_title">
													Mme. BOOP Betty
													<br>
													<label class="last_maj">Véritable propriétaire</label>
												</div>
												<div id="div_11_10" class="draggable 4 anime_help" draggable="true">
													M. DUCK Donald
													<br>
													<label class="last_maj">Curateur</label>
												</div>
											</div>
											<div id="dropper_30_1" class="dropper">
												<div class="drop_title">
													M. FUDD Elmer
													<br>
													<label class="last_maj">Chasseur (bail de chasse)</label>
												</div>
												<div id="div_40_9" class="draggable 4" draggable="true">
													Mme. POPPINS Mary
													<br>
													<label class="last_maj">Mandataire spécial</label>
												</div>
											</div>
											</center>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:right;">
								<label class="lib_alerte">Les modifications sont enregistrées automatiquement dans le système</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>			
			
			<div id="explorateur_infos_eaf" class="explorateur_span" style="display:none;width:90%;">
				<table class="explorateur_fond" frame="box" rules="NONE" width="100%" cellspacing="0" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_infos_eaf_sousdiv" class="no-border" style="text-align:center;">
									<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="6" cellpadding="6" border="0" align="center">
										<tbody id="tbody_infos_eaf_entete">
											<tr>
												<td>
													<table class="no-border" width="100%">
														<tbody><tr>
															<td>
																<label class="label">Informations sur une Entité d'Animation Foncière</label>
															</td>
															<td style="text-align:right;width:25px;">																	
																<img src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
															</td>																	
														</tr>
													</tbody></table>
												</td>
											</tr>
										</tbody>
										<tbody id="tbody_infos_eaf_contenu">
											<tr>
												<td class="bilan-af" style="text-align:left;">
													<label class="label">Cette EAF est présente sur les sites suivants :</label>													
													<li style="margin-left:25px;">
														<label class="label">Les Champs du Pelennor (CHPE)</label>
														<img style="margin:-5px 0;" src="<?php echo ROOT_PATH; ?>images/fiche_site.png" width="15px">
													</li>
													<li style="margin-left:25px;">
														<label class="label">La Forêt de Fangorne (FOFA)</label>
														<img style="margin:-5px 0;" src="<?php echo ROOT_PATH; ?>images/fiche_site.png" width="15px">
													</li>													
												</td>
											</tr>
										
											<tr>
												<td class="bilan-af" style="text-align:left;">
													<label class='label'>Cette EAF est présente sur les Sessions d'Animation Foncière suivantes :</label>
													<li style='margin-left:25px;'>
														<label class='label'>SAF_IMAGINAIRE_01 mais aucun échange n'a été réalisé à ce jour.</label>
														<a href='".$lien_saf."'>
															<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px'>
														</a>
													</li>
													<li style='margin-left:25px;'>
														<label class='label'>SAF_IMAGINAIRE_02 et des échanges ont déjà été réalisés.</label>
														<a href='".$lien_saf."'>
															<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px'>
														</a>
													</li>
												</td>
											</tr>											
											<tr>
												<td class="bilan-af" style="text-align:left;">
													<label class="label">Aucun acte en cours ou à venir avec cette EAF</label>
												</td>
											</tr>
											<tr>
												<td class='bilan-af' style='text-align:left;'>
													<label class='label'>Cette EAF intègre des parcelles sur les sites suivants :</label>														
													<li style='margin-left:25px;'>
														<label class='label'>5 lots sur d'autres SAFs sur La Forêt de Fangorne (FOFA)</label>
														<a href='".$lien_site."'>
															<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px'>
														</a>
													</li>
													<li style='margin-left:25px;'>
														<label class='label'>1 lot hors SAF sur La Forêt de Fangorne (FOFA)</label>
														<a href='".$lien_site."'>
															<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px'>
														</a>
													</li>	
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
			
			<div id="explorateur_infos_interloc" class="explorateur_span" style="display:none;width:90%;">
				<table class="explorateur_fond" frame="box" rules="NONE" width="100%" cellspacing="0" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_infos_interloc_sousdiv" class="no-border" style="text-align:center;">
									<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="6" cellpadding="6" border="0" align="center">
										<tbody id="tbody_infos_interloc_entete">
											<tr>
												<td>
													<table class="no-border" width="100%">
														<tbody><tr>
															<td>
																<label class="label">Informations sur M. PICSOU Balthazar</label>
															</td>
															<td style="text-align:right;width:25px;">																	
																<img src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
															</td>																	
														</tr>
													</tbody></table>
												</td>
											</tr>
										</tbody>
										<tbody id="tbody_infos_interloc_contenu">
											<tr>
												<td class='bilan-af' style='text-align:left;'>
													<label class='label'>Ce propriétaire est présent sur :</label>														
													<li style='margin-left:25px;'>
														<label class='label'>2 autre(s) EAF sur la session : SAF_IMAGINAIRE_02</label>
													</li>
												</td>
											</tr>										
											<tr>
												<td class='bilan-af' style='text-align:left;'>
													<label class='label'>Ce propriétaire est présent sur les sites suivants :</label>										
													<li style='margin-left:25px;'>
														<label class='label'>La Forêt de Fangorne (FOFA)</label>
														<a href='".$lien_site."'>
															<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px'>
														</a>
													</li>			
												</td>
											</tr>											
											<tr>
												<td class='bilan-af' style='text-align:left;'>
													<label class='label'>Bilan des actes en cours et à venir avec ce propriétaire :</label>													
													<li style='margin-left:25px;'>
														<label class='label'>Accord pour une acquisition (A confirmler)<br>pour 1 lot(s) sur le site Les Champs du Pelennor (CHPE)</label>
													</li>
													<li style='margin-left:25px;'>
														<label class='label'>Accord pour une convention pour 5 lot(s)<br>sur le site La Forêt de Fangorne (FOFA)</label>
													</li>
												</td>
											</tr>
											<tr>
												<td class='bilan-af' style='text-align:left;'>
													<label class='label'>Ce propriétaire possède également des parcelles sur les sites suivants :</label>
													<li style='margin-left:25px;'>
														<label class='label'>5 lots sur d'autres SAFs sur La Forêt de Fangorne (FOFA)</label>
														<a href='".$lien_site."'>
															<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px'>
														</a>
													</li>
													<li style='margin-left:25px;'>
														<label class='label'>2 lots hors SAF sur La Forêt de Fangorne (FOFA)</label>
														<a href='".$lien_site."'>
															<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px'>
														</a>
													</li>				
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
			
			<div id="explorateur_prix_occsol" class="explorateur_span" style="display:none;width:90%;top:350px">
				<table class="explorateur_fond" frame="box" rules="NONE" width="100%" cellspacing="0" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_prix_occsol_sousdiv" class="no-border" style="text-align:center;">
									<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="6" cellpadding="6" border="0" align="center">
										<tbody id="tbody_prix_occsol_entete">
											<tr>
												<td>
													<table class="no-border" width="100%">
														<tbody><tr>
															<td>
																<label class="label">Saisie des prix au m² par occupation du sol</label>
															</td>
															<td style="text-align:right;width:25px;">																	
																<img src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer" width="15px">
															</td>																	
														</tr>
													</tbody></table>
												</td>
											</tr>
										</tbody>
										<tbody id="tbody_prix_occsol_contenu">
											<tr>
												<td>
													<table id="tbl_prix_occsol" class="no-border" width="100%">
														<tr>
															<td style="text-align:right; margin-right:5px;">
																<label class="label_simple">Prairie</label>
															</td>
															<td style="text-align:left; margin-right:5px;">
																<input size="8" class="editbox" style="text-align:right;" id="prix_occsol-0" type="text"><label class="label_simple"> €</label>
															</td>
														</tr>
														<tr>
															<td style="text-align:right; margin-right:5px;">
																<label class="label_simple">Marais</label>
															</td>
															<td style="text-align:left; margin-right:5px;">
																<input size="8" class="editbox" style="text-align:right;" id="prix_occsol-2" type="text"><label class="label_simple"> €</label>
															</td>
														</tr>
														<tr>
															<td style="text-align:right; margin-right:5px;">
																<label class="label_simple">Pelouse sèche</label>
															</td>
															<td style="text-align:left; margin-right:5px;">
																<input size="8" class="editbox" style="text-align:right;" id="prix_occsol-3" type="text"><label class="label_simple"> €</label>
															</td>
														</tr><tr>
															<td style="text-align:right; margin-right:5px;">
																<label class="label_simple">Boisement</label>
															</td>
															<td style="text-align:left; margin-right:5px;">
																<input size="8" class="editbox" style="text-align:right;" id="prix_occsol-4" type="text"><label class="label_simple"> €</label>
															</td>
														</tr>														
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<label class="last_maj">Ce tableau permet de renseigner un prix par occupation du sol pour chaque parcelle concernée.<br>Seules les informations manquantes seront renseignées.</label>
												</td>
											</tr>
										</tbody>
										<tbody id="tbody_prix_occsol_pied">
											<tr>
												<td>
													<label id="lbl_prob_prix_occsol" class="lib_alerte" style="display:none;">Veuillez compléter votre saisie</label>
												</td>
											</tr>
											<tr>
												<td class="save">
													<input name="btn_valide_prix_occsol" id="btn_valide_prix_occsol" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer" type="button">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
			
			<div id="msg_accueil" class="explorateur_span">
				<table class="explorateur_fond" frame="box" rules="NONE" width="100%" cellspacing="0" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div class="no-border" style="text-align:center;top:50px;">
									<table class="explorateur_contenu" frame="box" rules="NONE" cellspacing="6" cellpadding="6" border="0" align="center">
										<thead>
											<tr>
												<td>
													<label class='label'>Bienvenue sur le mode AIDE de l'animation foncière
												</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td style='text-align:left;'>
													<label class='label_simple'>
														Vous trouverez sur ces pages des informations pour vous aider à bien comprendre et à utiliser au mieux ce module.<br>
														Ici, aucun élément de formulaire ne peut être utilisé ou saisi. Il s'agit d'une session d'animation fictive. Vous ne risquez donc pas d'altérer vos véritables animations foncières.<br>
														<br>
														<img src="images/help/tip_off.png" style='position:absolute;' class='img_tip_off'>
														<img src="images/help/tip_on.png" style='position:absolute;' class='img_tip_on'>
														<span style='margin-left:30px;'>Survolez ce symbole pour des explications ou des précisions sur un élément.</span><br>
														<br>
														<img src="<?php echo ROOT_PATH; ?>images/help.png" class='img_help img_help_keep' style='padding-top:10px'><span style='margin-left:30px;'>Cliquez sur ce symbole pour ouvrir une pop-up d'explication via du texte, des captures d'écran et même des animations.</span><br><span style='margin-left:30px;'>Pour quitter la pop-up, il suffit de cliquer en dehors de celle-ci.</span><br>
														<br>
														<img src="images/help/voir.png" style='padding-top:10px;' width='20px;'><span style='margin-left:10px;'>Survolez ce symbole pour mettre en évidence un élément et mieux comprendre les explications fournies.</span><br>
														<br>
														<img src='images/help/glossaire.png' width='40px'><div style='margin-left:45px;margin-top:-35px;width:auto;'>Ce bouton donne accès à un glossaire dans lequel vous avez la possibilité d'effectuer une recherche.</div>
													</label>
												</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td style='text-align:right;'>
													<label class='last_maj' style='cursor:pointer;' onclick="skipMsg()">Skip...</label>
												</td>
											</tr>	
										</tfoot>
									</table>
								</div>
							</td>
						</tr>
					</tbody>					
				</table>
			</div>
			
			<div id="loading" class="loading" style="display: none;">
				<img src="<?php echo ROOT_PATH; ?>images/ajax-loader.gif">
			</div>
		</section>
		
			<div id="explorateur_echange_reduit" class="explorateur_span" style="display:none;">
				<table class="explorateur_fond" frame="box" rules="NONE" cellspacing="5" cellpadding="5" border="0">
					<tbody>
						<tr>
							<td>
								<div class="no-border" style="text-align: center; position: fixed; left: 15px; bottom: 15px;">
									<form class="form" onsubmit="return valide_form('frm_echange')" method="POST" style="display:inline-block">
										<table style="width: 890px; background-color: rgb(204, 204, 204); border-color: rgb(254, 0, 0); border-width: 3px;" class="explorateur_contenu" frame="box" rules="NONE" cellspacing="0" cellpadding="2" border="0" align="center">
											<tbody>
												<tr>
													<td colspan="2">
														<table class="no-border" width="100%">
															<tbody>
																<tr>		
																	<td style="text-align:left;width:25px;">	
																		<img id='img_ech_maximize' src="<?php echo ROOT_PATH; ?>images/agrandir.png" style="cursor:pointer" width="20px">
																	</td>
																	<td style="text-align:center;margin-left:20px">
																		<label  class="label" style="text-align:center;margin-left:20px">Saisie d'un échange avec M. PICSOU Balthazar</label>
																	</td>
																	<td style="text-align:right;width:25px;">	
																		
																	</td>																	
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>											
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		
		<?php 			
			include("../../includes/footer.php");
		?>
	</body>
</html>