<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	/* Fonction permettant de mettre à jour tout ou partie des EAF de l'onglet "Interlocuteurs"
	La liste des EAF concernées par la MAJ est contenue dans la variable $tbl_rel_saf_foncier_id */
	function fct_rq_lst_interlocs($session_af_id, $entite_af_id, $tbl_eaf_lot_id, $tbl_rel_saf_foncier_id) {
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../../includes/prob_tech.php'));
		}
		
		$rq_info_interlocs = "
		SELECT DISTINCT
			interlocuteurs.interloc_id,
			interlocuteurs.dnuper,
			d_ccoqua.particule_lib,
			interlocuteurs.ddenom,
			interlocuteurs.nom_usage,
			interlocuteurs.nom_jeunefille,
			CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
				CASE WHEN strpos(interlocuteurs.prenom_usage, ' ') > 0 THEN
					substring(interlocuteurs.prenom_usage from 0 for  strpos(interlocuteurs.prenom_usage, ' ')) 
				ELSE 
					interlocuteurs.prenom_usage
				END
			ELSE	
				'N.P.' 
			END as prenom_usage,
			interlocuteurs.nom_conjoint,
			interlocuteurs.prenom_conjoint,
			interlocuteurs.jdatnss,
			interlocuteurs.dlign3,
			interlocuteurs.dlign4,
			interlocuteurs.dlign5,
			interlocuteurs.dlign6,
			interlocuteurs.email,
			interlocuteurs.fixe_domicile,
			interlocuteurs.fixe_travail,
			interlocuteurs.portable_domicile,
			interlocuteurs.portable_travail,
			interlocuteurs.fax,
			COALESCE(interlocuteurs.gtoper, 1) as gtoper,
			d_type_interloc.type_interloc_lib,
			d_type_interloc.type_interloc_id,
			d_type_interloc.groupe_interloc_id,
			d_groupe_interloc.groupe_interloc_color,
			d_groupe_interloc.can_be_subst::text,
			d_groupe_interloc.can_be_ref::text,
			d_etat_interloc.etat_interloc_lib,
			d_etat_interloc.etat_interloc_id,
			array_to_string(array_agg(DISTINCT rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id), '_') as tbl_rel_saf_foncier_interlocs_id,
			count(rel_af_echanges.echange_af_id) as nbr_echanges,
			CASE WHEN ref1.interloc_id IS NOT NULL THEN 
				'1ref:'||ref1.interloc_ref_id || '-a-' || CASE WHEN ref1.interloc_id = ref1.interloc_ref_id THEN 0 ELSE 2 END
			ELSE
				CASE WHEN ref2.interloc_id IS NOT NULL THEN 
					'1ref:'||ref2.interloc_ref_id || '-b-' || ref2.interloc_id
				ELSE
					'2notref:'||interlocuteurs.interloc_id || '-a-1'
				END
			END as ordre
		FROM 
			animation_fonciere.interlocuteurs 
			JOIN animation_fonciere.d_etat_interloc USING (etat_interloc_id) 
			LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
			JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
			JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.session_af USING (session_af_id)
			LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)									
			LEFT JOIN 
				(
					SELECT
						st1.interloc_id,
						st1.type_interloc_id,
						rel_saf_foncier_interlocs.interloc_id as interloc_ref_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN
							(SELECT DISTINCT 
								rel_saf_foncier_interlocs.interloc_id,
								rel_saf_foncier_interlocs.type_interloc_id,
								interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
							FROM 
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
							WHERE 
								session_af_id = :session_af_id AND 
								entite_af_id = :entite_af_id
							
							UNION

							SELECT
								t_subst2.interloc_id,
								t_subst2.type_interloc_id,
								t_subst2.rel_saf_foncier_interlocs_id_is as foncier_interloc_ref_id
							FROM								
								(
									SELECT DISTINCT
										rel_saf_foncier_interlocs.interloc_id,
										rel_saf_foncier_interlocs.type_interloc_id,
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
										rel_saf_foncier_interlocs_id_is
									FROM
										animation_fonciere.interlocuteurs
										JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
										JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
										JOIN animation_fonciere.d_type_interloc ON (t_temp_subst.type_interloc_id = d_type_interloc.type_interloc_id)
										JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id) 
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id AND
										groupe_interloc_id != 3 AND
										t_temp_subst.interloc_id IN 
											(
												SELECT
													interloc_id
												FROM
													animation_fonciere.rel_saf_foncier_interlocs
													JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
													JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref) 
												WHERE 
													session_af_id = :session_af_id AND 
													entite_af_id = :entite_af_id
											)
								) t_subst2 									
							) st1 ON st1.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
				) ref1 ON (ref1.interloc_id = rel_saf_foncier_interlocs.interloc_id AND ref1.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
			LEFT JOIN 
				(
					SELECT
						st2.interloc_id,
						st2.type_interloc_id,
						rel_saf_foncier_interlocs.interloc_id as interloc_ref_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN
							(SELECT DISTINCT 
								rel_saf_foncier_interlocs.interloc_id, 
								rel_saf_foncier_interlocs.type_interloc_id,
								interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
							FROM 
								animation_fonciere.rel_saf_foncier_interlocs
								JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
							WHERE 
								session_af_id = :session_af_id AND 
								entite_af_id = :entite_af_id
								
							UNION

							SELECT DISTINCT
								t_subst.interloc_id,
								t_subst.type_interloc_id,
								t_ref.rel_saf_foncier_interlocs_id
								
							FROM
								(
									SELECT DISTINCT
										t_temp_subst.interloc_id,
										t_temp_subst.type_interloc_id,
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
										rel_saf_foncier_interlocs_id_vp
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
										JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_is)
										JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_subst ON (t_temp_subst.rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp)
										
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id
								) t_subst
								JOIN
								(
									SELECT DISTINCT
										rel_saf_foncier_interlocs.interloc_id,
										rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,
										interloc_referent.rel_saf_foncier_interlocs_id as under_ref
									FROM
										animation_fonciere.interlocuteurs
										JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id)
										JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									WHERE 
										session_af_id = :session_af_id AND 
										entite_af_id = :entite_af_id
								) t_ref ON (t_subst.rel_saf_foncier_interlocs_id = t_ref.under_ref)
								
								
							) st2 ON st2.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
				) ref2 ON (ref2.interloc_id = rel_saf_foncier_interlocs.interloc_id AND ref2.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
		WHERE 
			rel_saf_foncier.session_af_id = :session_af_id AND 
			entite_af_id = :entite_af_id AND
			(
				(d_groupe_interloc.can_be_subst = 'f' OR rel_saf_foncier_interlocs.interloc_id NOT IN 
					(SELECT DISTINCT 					
						interloc_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
					WHERE 
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id)
				)
			OR
				d_groupe_interloc.can_be_subst = 't' AND rel_saf_foncier_interlocs.interloc_id || '_' || rel_saf_foncier_interlocs.type_interloc_id NOT IN 
					(SELECT DISTINCT 					
						interloc_id || '_' || type_interloc_id
					FROM 
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
					WHERE 
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id
					)
			)
		GROUP BY 
			interlocuteurs.interloc_id, 
			d_ccoqua.particule_lib, 
			d_type_interloc.type_interloc_lib,
			d_type_interloc.type_interloc_id,
			d_type_interloc.groupe_interloc_id,
			d_groupe_interloc.groupe_interloc_color,
			d_groupe_interloc.can_be_subst,
			d_groupe_interloc.can_be_ref,
			d_etat_interloc.etat_interloc_lib,
			d_etat_interloc.etat_interloc_id,
			ordre
		ORDER BY ordre";
		// echo $rq_info_interlocs;
		$res_info_interlocs = $bdd->prepare($rq_info_interlocs);
		$res_info_interlocs->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$lst_interlocs = $res_info_interlocs->fetchall();
		$result['code'] = '';
		For ($j=0; $j<$res_info_interlocs->rowCount(); $j++) { 
			$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_interlocs[$j]['particule_lib'], $lst_interlocs[$j]['nom_usage'], $lst_interlocs[$j]['prenom_usage'], $lst_interlocs[$j]['ddenom']));
			If ($lst_interlocs[$j]['etat_interloc_id'] == 1) {	
				$color_font_interloc = $lst_interlocs[$j]['groupe_interloc_color'];
			} Else {
				$color_font_interloc = '#ccc';
			}
			$result['code'] .= "
			<tr id='tr_".$lst_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id']."'>			
				<td style='"; If ($j>0 && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-'))) {$result['code'] .= "border-top:1px solid #004494; ";}
				$result['code'] .= "padding-left:5px;'>
					<div class='".$entite_af_id.'-'.$lst_interlocs[$j]['interloc_id']." no-breathing' onmouseover=\"overInterloc(this, 'tbl_".$entite_af_id."', '".$entite_af_id.'-'.$lst_interlocs[$j]['interloc_id']."');\" onmouseout=\"outInterloc(this, 'tbl_".$entite_af_id."', '".$entite_af_id.'-'.$lst_interlocs[$j]['interloc_id']."');\">
						<label id='lbl_".$entite_af_id.'-'.$lst_interlocs[$j]['interloc_id'].'-'.$lst_interlocs[$j]['type_interloc_id']."' style='color:$color_font_interloc; 'class='label";
					If (count(array_keys(array_values(array_column($lst_interlocs, 'ordre')), '1ref:'.$lst_interlocs[$j]['interloc_id'].'-a-0')) > 0 && $lst_interlocs[$j]['can_be_ref'] == 'true') {
						$result['code'] .= " referent"; 
					} 
					$result['code'] .= "'>
						".retour_ligne($interloc_lib, 40)."
						</label>	
					</div>
					<label class='label' style='font-weight:normal;color:$color_font_interloc;'>	
						".$lst_interlocs[$j]['type_interloc_lib']."
					</label>";
				If ($lst_interlocs[$j]['etat_interloc_id'] != 1) {
					$result['code'] .= "
					<br>
					<label class='last_maj' style='color:red;'>	
						(".$lst_interlocs[$j]['etat_interloc_lib'].")
					</label>";
				}
				If ($lst_interlocs[$j]['can_be_subst'] == 'true') {
					$result['code'] .= "
					<br>
					<label class='last_maj' style='color:red;'>	
						A associer
					</label>";
				}
				$result['code'] .= "
				</td>
				<td style='"; If ($j>0 && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-'))) {$result['code'] .= "border-top:1px solid #004494; ";}
				$result['code'] .= "padding-left:5px;'>
					<img onclick=\"showFormUpdateInterloc(this, ".$lst_interlocs[$j]['interloc_id'].", ".$lst_interlocs[$j]['type_interloc_id'].", '".str_replace("'", "\'", $interloc_lib)."', ".$session_af_id.", '".$entite_af_id."', '".$lst_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id']."', '".$lst_interlocs[$j]['gtoper']."');\" src='".ROOT_PATH."images/edit.png' style='cursor:pointer;padding-left:5px;'>";											
				If ($lst_interlocs[$j]['nbr_echanges'] == 0) {
					$result['code'] .= "												
					<img id='img_delete_".$lst_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"deleteInterloc('".$lst_interlocs[$j]['tbl_rel_saf_foncier_interlocs_id']."', '".$lst_interlocs[$j]['interloc_id']."', '".$session_af_id."', '".$entite_af_id."', '".$lst_interlocs[$j]['type_interloc_id']."', '".$lst_interlocs[$j]['groupe_interloc_id']."');\" src='".ROOT_PATH."images/supprimer.png'>";
				}
				$result['code'] .= "
				</td>
				<td style='"; If ($j>0 && $lst_interlocs[$j]['ordre'] != '' && substr($lst_interlocs[$j]['ordre'], 0, strpos($lst_interlocs[$j]['ordre'], '-')) != substr($lst_interlocs[$j-1]['ordre'], 0, strpos($lst_interlocs[$j-1]['ordre'], '-'))) {$result['code'] .= "border-top:1px solid #004494; ";}
				$result['code'] .= "padding-left:5px;'>
					<table class='no-border'>";
						$rq_info_interlocs_subst = "
						SELECT DISTINCT
							interlocuteurs.interloc_id,							
							d_etat_interloc.etat_interloc_lib,
							d_etat_interloc.etat_interloc_id,
							interlocuteurs.dnuper,
							d_ccoqua.particule_lib,
							interlocuteurs.ddenom,
							interlocuteurs.nom_usage,
							interlocuteurs.nom_jeunefille,
							CASE WHEN interlocuteurs.prenom_usage IS NOT NULL AND interlocuteurs.prenom_usage != 'N.P.' THEN 
								CASE WHEN strpos(interlocuteurs.prenom_usage, ' ') > 0 THEN
									substring(interlocuteurs.prenom_usage from 0 for  strpos(interlocuteurs.prenom_usage, ' ')) 
								ELSE 
									interlocuteurs.prenom_usage
								END
							ELSE	
								'N.P.' 
							END as prenom_usage,
							interlocuteurs.nom_conjoint,
							interlocuteurs.prenom_conjoint,
							interlocuteurs.jdatnss,
							interlocuteurs.dlign3,
							interlocuteurs.dlign4,
							interlocuteurs.dlign5,
							interlocuteurs.dlign6,
							interlocuteurs.email,
							interlocuteurs.fixe_domicile,
							interlocuteurs.fixe_travail,
							interlocuteurs.portable_domicile,
							interlocuteurs.portable_travail,
							interlocuteurs.fax,
							d_type_interloc.type_interloc_id,
							d_type_interloc.type_interloc_lib,
							d_groupe_interloc.groupe_interloc_id,
							d_groupe_interloc.groupe_interloc_color,
							d_groupe_interloc.can_be_ref::text,
							array_to_string(array_agg(DISTINCT rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id), '_') as tbl_rel_saf_foncier_interlocs_id,
							count(rel_af_echanges.echange_af_id) as nbr_echanges,
							CASE WHEN ref1.interloc_id IS NOT NULL THEN 
								ref1.interloc_ref_id || '-a-'
							ELSE
								ref2.interloc_ref_id || '-b-' || ref2.interloc_id
							END as ordre
						FROM 
							animation_fonciere.interlocuteurs 
							JOIN animation_fonciere.d_etat_interloc USING (etat_interloc_id) 
							LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
							JOIN animation_fonciere.rel_saf_foncier_interlocs USING (interloc_id) 
							JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
							JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							JOIN animation_fonciere.session_af USING (session_af_id)
							LEFT JOIN animation_fonciere.rel_af_echanges USING (rel_saf_foncier_interlocs_id)									
							LEFT JOIN 
								(
									SELECT
										st1.interloc_id,
										rel_saf_foncier_interlocs.interloc_id as interloc_ref_id,
										type_interloc_id
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN
											(SELECT DISTINCT 
												rel_saf_foncier_interlocs.interloc_id, 
												interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
											FROM 
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN animation_fonciere.interloc_referent ON (interloc_referent.rel_saf_foncier_interlocs_id_ref =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
											WHERE 
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id
											) st1 ON st1.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
								) ref1 ON (ref1.interloc_id = rel_saf_foncier_interlocs.interloc_id AND ref1.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
							LEFT JOIN 
								(
									SELECT
										st2.interloc_id,
										rel_saf_foncier_interlocs.interloc_id as interloc_ref_id,
										type_interloc_id
									FROM
										animation_fonciere.rel_saf_foncier_interlocs
										JOIN
											(SELECT DISTINCT 
												rel_saf_foncier_interlocs.interloc_id, 
												interloc_referent.rel_saf_foncier_interlocs_id_ref as foncier_interloc_ref_id
											FROM 
												animation_fonciere.rel_saf_foncier_interlocs
												JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
												JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
												JOIN animation_fonciere.interloc_referent USING (rel_saf_foncier_interlocs_id)
											WHERE 
												session_af_id = :session_af_id AND 
												entite_af_id = :entite_af_id
											) st2 ON st2.foncier_interloc_ref_id = rel_saf_foncier_interlocs_id
								) ref2 ON (ref2.interloc_id = rel_saf_foncier_interlocs.interloc_id AND ref2.type_interloc_id = rel_saf_foncier_interlocs.type_interloc_id)
						WHERE 
							rel_saf_foncier.session_af_id = :session_af_id AND 
							entite_af_id = :entite_af_id AND
							rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id IN (
								SELECT 
									rel_saf_foncier_interlocs_id_is 
								FROM
									animation_fonciere.interloc_substitution
									JOIN animation_fonciere.rel_saf_foncier_interlocs ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								WHERE
									rel_saf_foncier.session_af_id = :session_af_id AND 
									entite_af_id = :entite_af_id AND
									interloc_id = ".$lst_interlocs[$j]['interloc_id']." AND
									type_interloc_id = ".$lst_interlocs[$j]['type_interloc_id']."
								)
						GROUP BY 
							interlocuteurs.interloc_id, 
							d_ccoqua.particule_lib, 
							d_type_interloc.type_interloc_id,
							d_type_interloc.type_interloc_lib,
							d_groupe_interloc.groupe_interloc_id,
							d_groupe_interloc.groupe_interloc_color,
							d_groupe_interloc.can_be_ref,
							d_etat_interloc.etat_interloc_lib,
							d_etat_interloc.etat_interloc_id,
							ordre
						ORDER BY ordre";
						// echo $rq_info_interlocs;
						$res_info_interlocs_subst = $bdd->prepare($rq_info_interlocs_subst);
						$res_info_interlocs_subst->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
						$lst_interlocs_subst = $res_info_interlocs_subst->fetchall();
						For ($s=0; $s<$res_info_interlocs_subst->rowCount(); $s++) { 
							$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_interlocs_subst[$s]['particule_lib'], $lst_interlocs_subst[$s]['nom_usage'], $lst_interlocs_subst[$s]['prenom_usage'], $lst_interlocs_subst[$s]['ddenom']));		
							If ($lst_interlocs_subst[$s]['etat_interloc_id'] == 1) {	
								$color_font_interloc_subst = $lst_interlocs_subst[$s]['groupe_interloc_color'];
							} Else {
								$color_font_interloc_subst = '#ccc';
							}
							$result['code'] .= "
							<tr>			
								<td style='padding-left:15px;'>
									<div class='".$entite_af_id.'-'.$lst_interlocs_subst[$s]['interloc_id']." no-breathing' onmouseover=\"overInterloc(this, 'tbl_".$entite_af_id."', '".$entite_af_id.'-'.$lst_interlocs_subst[$s]['interloc_id']."');\" onmouseout=\"outInterloc(this, 'tbl_".$entite_af_id."', '".$entite_af_id.'-'.$lst_interlocs_subst[$s]['interloc_id']."');\">
										<label id='lbl_".$entite_af_id.'-'.$lst_interlocs_subst[$s]['interloc_id'].'-'.$lst_interlocs_subst[$s]['type_interloc_id']."' style='color:".$color_font_interloc_subst.";' class='label";
										If (count(array_keys(array_values(array_column($lst_interlocs, 'ordre')), $lst_interlocs_subst[$s]['interloc_id'].'-a-0')) > 0 && $lst_interlocs_subst[$s]['can_be_ref'] == 'true') {
											$result['code'] .= " referent"; 
										} 
										$result['code'] .= "'>
											".retour_ligne($interloc_lib, 40)."
										</label>	
									</div>
									<label class='label' style='color:".$color_font_interloc_subst.";font-weight:normal;'>	
										".$lst_interlocs_subst[$s]['type_interloc_lib']."
									</label>";
								If ($lst_interlocs_subst[$s]['etat_interloc_id'] != 1) {
									$result['code'] .= "
									<br>
									<label class='last_maj' style='color:red;'>	
										(".$lst_interlocs_subst[$s]['etat_interloc_lib'].")
									</label>";
								}
								$result['code'] .= "	
								</td>
								<td style='padding-left:15px'>
									<img onclick=\"showFormUpdateInterloc(this, ".$lst_interlocs_subst[$s]['interloc_id'].", ".$lst_interlocs_subst[$s]['type_interloc_id'].", '".str_replace("'", "\'", $interloc_lib)."', ".$session_af_id.", '".$entite_af_id."', '".$lst_interlocs_subst[$s]['tbl_rel_saf_foncier_interlocs_id']."', '".$lst_interlocs[$j]['gtoper']."');\" src='".ROOT_PATH."images/edit.png' style='cursor:pointer;padding-left:5px;'>";											
								If ($lst_interlocs_subst[$s]['nbr_echanges'] == 0) {
									$result['code'] .= "												
									<img id='img_delete_".$lst_interlocs_subst[$s]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"deleteInterloc('".$lst_interlocs_subst[$s]['tbl_rel_saf_foncier_interlocs_id']."', '".$lst_interlocs_subst[$s]['interloc_id']."', '".$session_af_id."', '".$entite_af_id."', '".$lst_interlocs_subst[$s]['type_interloc_id']."', '".$lst_interlocs_subst[$s]['groupe_interloc_id']."');\" src='".ROOT_PATH."images/supprimer.png'>";
								}
								$result['code'] .= "
								</td>";
						}
						$result['code'] .= " 	
						</tr>
					</table>
				</td>
			</tr>";
		}
		
		$rq_bilan_type_interlocs = "
			SELECT DISTINCT												
				d_groupe_interloc.groupe_interloc_id,
				can_be_subst,
				count(DISTINCT interloc_id) as nbr_interloc
			FROM
				animation_fonciere.d_groupe_interloc
				JOIN animation_fonciere.d_type_interloc USING (groupe_interloc_id)
				LEFT JOIN 
					(
						SELECT DISTINCT 
							interloc_id, 
							type_interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							entite_af_id = :entite_af_id AND 
							session_af_id = :session_af_id
					) t1  USING (type_interloc_id)
			GROUP BY d_groupe_interloc.groupe_interloc_id
			ORDER BY d_groupe_interloc.groupe_interloc_id";
		$res_bilan_type_interlocs = $bdd->prepare($rq_bilan_type_interlocs);
		$res_bilan_type_interlocs->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$bilan_type_interlocs = $res_bilan_type_interlocs->fetchall();
		
		$rq_gestion_referent = "
			SELECT 
				count(DISTINCT interloc_id) as nbr_interloc_can_be_referent
			FROM
				(
					SELECT DISTINCT 
						interloc_id, 
						type_interloc_id,
						nbr_lots_eaf,
						count(rel_saf_foncier_interlocs_id) as nbr_lots,
						count(rel_saf_foncier_interlocs_id_is) as nbr_substitution
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						LEFT JOIN animation_fonciere.interloc_substitution ON (rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs_id_vp),
						(
							SELECT 
								count(rel_saf_foncier_id) as nbr_lots_eaf
							FROM 
								animation_fonciere.rel_saf_foncier
								JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
							WHERE
								rel_saf_foncier.session_af_id = :session_af_id AND
								rel_eaf_foncier.entite_af_id = :entite_af_id
							GROUP BY
								entite_af_id
						) t_lot_eaf
						
					WHERE 
						rel_eaf_foncier.entite_af_id = :entite_af_id AND 
						rel_saf_foncier.session_af_id = :session_af_id AND
						groupe_interloc_id != 4
					GROUP BY 
						interloc_id, 
						type_interloc_id,
						nbr_lots_eaf
				) t_synthese_referent
			WHERE
				t_synthese_referent.nbr_lots_eaf = t_synthese_referent.nbr_lots AND
				t_synthese_referent.nbr_substitution = 0";
		$res_gestion_referent = $bdd->prepare($rq_gestion_referent);
		$res_gestion_referent->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$gestion_referent = $res_gestion_referent->fetch();
		
		$rq_gestion_subst = "
			SELECT DISTINCT												
				interloc_id,
				can_be_subst,
				can_be_under_subst,
				count(DISTINCT interloc_id) as nbr_interloc
			FROM
				animation_fonciere.d_groupe_interloc
				JOIN animation_fonciere.d_type_interloc USING (groupe_interloc_id)
				JOIN 
					(
						SELECT DISTINCT 
							interloc_id, 
							type_interloc_id
						FROM
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							entite_af_id = :entite_af_id AND 
							session_af_id = :session_af_id
					) t1  USING (type_interloc_id)
			GROUP BY interloc_id, can_be_subst, can_be_under_subst
			ORDER BY interloc_id";
		$res_gestion_subst = $bdd->prepare($rq_gestion_subst);
		$res_gestion_subst->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$gestion_subst = $res_gestion_subst->fetchall();
		
		$result['code'] .= "
			<tr id ='tr_no_vp_".$entite_af_id."' style='";
			If ($bilan_type_interlocs[0]['nbr_interloc'] != 0) {				
				$result['code'] .= "display:none;";
			} 
			$result['code'] .= "'>
				<td style='text-align:center;'>
					<label class='label' style='color:red;'>	
						Attention il n'y a pas de véritable propriétaire identifié
					</label>
				</td>
			</tr>
			<tr id ='tr_add_interloc_".$entite_af_id."'>
				<td id='td_".$entite_af_id."' style='text-align:center;' colspan='3'>
					<label class='label'>																	
						<input id='btn_".$entite_af_id."' type='button' class='btn_frm' onclick=\"showFormAddInterloc('".$entite_af_id."', ".$session_af_id.", '".$tbl_eaf_lot_id."', '".$tbl_rel_saf_foncier_id."')\" value='Ajouter un interlocuteur'>
					</label>
				</td>
			</tr>
			<tr id ='tr_gestref_".$entite_af_id."'>
				<td style='text-align:center;' colspan='3'>";
				If ($gestion_referent['nbr_interloc_can_be_referent'] > 1) {
					$result['code'] .= "
					<input id='btn_gestref_".$entite_af_id."' type='button' class='btn_frm' onclick=\"showFormGestref(".$entite_af_id.", ".$session_af_id.")\" value='Gestion des r&eacute;f&eacute;rents'>";
				} 
				$nbr_subst = 0;
				$nbr_under_subst = 0;
				Foreach (array_keys(array_values(array_column($gestion_subst, 'can_be_subst')), 'true') as $key1) {
					$nbr_subst += $gestion_subst[$key1]['nbr_interloc'];
					Foreach (array_keys(array_values(array_column($gestion_subst, 'can_be_under_subst')), 'true') as $key2) {
						If ($gestion_subst[$key1]['interloc_id'] != $gestion_subst[$key2]['interloc_id']) {
							$nbr_under_subst += $gestion_subst[$key2]['nbr_interloc'];
						}
					}					
				}
				If ($nbr_subst > 0 && $nbr_under_subst > 0) {
					$result['code'] .= "
					<input id='btn_gestsubst_".$entite_af_id."' type='button' class='btn_frm' onclick=\"showFormGestsubst('".$entite_af_id."', ".$session_af_id.", '".$tbl_rel_saf_foncier_id."')\" value='Gestion des représentants'>";
				}
				$result['code'] .= "
				</td>
			</tr>";
		
		return $result['code'];
	}	
	
	/* Fonction pour afficher le tableau relatif à la négociation des parcelles d'un échange. Elle prend en compte les référents et les référencés, ainsi que les parcelles d'un interlocuteur dans les autres EAF de la SAF. */
	function fct_echange_tbl_maitrise_parc($session_af_id, $entite_af_id, $interloc_id, $echange_af_id, $display_tbl_parc_maitrise_other_eaf, $representant) {
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../../includes/prob_tech.php'));
		}
		
		If ($representant == 'true') {
			$rq_lst_parcelles = "
			SELECT DISTINCT
				interloc_id,
				particule_lib,
				nom_usage,
				prenom_usage,
				ddenom,
				array_to_string(array_agg(rel_saf_foncier_interlocs_id ORDER BY dnulot), '-') as tbl_rel_saf_foncier_interlocs_id,	
				par_lib,
				count(st1.dnulot) as nbr_lot,
				st1.dnulot,
				array_to_string(array_agg(DISTINCT type_interloc_id), ',') as tbl_type_interloc_id,
				array_to_string(array_agg(DISTINCT type_interloc_lib), ' / ') as tbl_type_interloc_lib,
				avis_favorable::text,
				ordre_eaf as ordre_eaf,
				MIN(type_interloc_id) as ordre_type,
				entite_af_id,
				array_to_string(array_agg(COALESCE(nego_af_id, -1) ORDER BY lot_id), ',') as tbl_resultat,
				array_to_string(array_agg(COALESCE(nego_af_prix, -1) ORDER BY lot_id), ',') as tbl_resultat_prix,
				can_be_ref,
				interloc_id || '_' || par_lib || '_' || ordre_eaf || '_' || avis_favorable || '_' || can_be_ref as var_calc_nbr_lot
			FROM
				(
					SELECT DISTINCT
						interloc_id,
						particule_lib,
						nom_usage,
						prenom_usage,
						ddenom,
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,	
						t1.ccosec || t1.dnupla as par_lib,
						t2.dnulot,
						lot_id,
						d_type_interloc.type_interloc_id,
						type_interloc_lib,
						d_type_interloc.avis_favorable::text,
						CASE WHEN entite_af_id = :entite_af_id THEN 1 ELSE 2 END as ordre_eaf,
						entite_af_id,
						nego_af_id,
						nego_af_prix,
						can_be_ref::text
					FROM
						cadastre.parcelles_cen t1
						JOIN cadastre.lots_cen t2 USING (par_id)
						JOIN cadastre.cadastre_cen t3 USING (lot_id)
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING(groupe_interloc_id)
						JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_vp =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
						LEFT JOIN 
							(
								SELECT
									rel_saf_foncier_interlocs_id,
									nego_af_id,
									nego_af_prix
								FROM
									animation_fonciere.rel_af_echanges
								WHERE
									echange_af_id::text = :echange_af_id::text
								
							) t_nego USING (rel_saf_foncier_interlocs_id)
					WHERE 
						session_af_id = :session_af_id AND 
						entite_af_id = :entite_af_id AND
						t3.date_fin = 'N.D.' AND
						t4.date_fin = 'N.D.' AND
						lot_bloque = 'f' AND 
						rel_saf_foncier.paf_id!= 0 AND
						d_type_interloc.groupe_interloc_id != 3 AND
						(d_type_interloc.can_nego = 't' OR d_type_interloc.avis_favorable = 't') AND
						rel_saf_foncier_interlocs_id_is IN
							(
								SELECT
									rel_saf_foncier_interlocs_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs						
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
								WHERE
									interloc_id = :interloc_id AND
									groupe_interloc_id = 3 AND
									session_af_id = :session_af_id AND 
									entite_af_id = :entite_af_id
							)
				) st1

			GROUP BY 
				interloc_id,
				particule_lib,
				nom_usage,
				prenom_usage,
				ddenom,
				par_lib,
				st1.dnulot,
				avis_favorable,
				ordre_eaf,
				entite_af_id,
				can_be_ref,
				var_calc_nbr_lot
			ORDER BY 
				ordre_eaf,
				ordre_type,
				entite_af_id,
				interloc_id,
				avis_favorable,
				can_be_ref,
				par_lib";
		} Else {				
			$rq_lst_parcelles = "
			SELECT DISTINCT
				interloc_id_subst as interloc_id,
				particule_lib,
				nom_usage,
				prenom_usage,
				ddenom,
				array_to_string(array_agg(rel_saf_foncier_interlocs_id ORDER BY dnulot), '-') as tbl_rel_saf_foncier_interlocs_id,	
				par_lib,
				count(st1.dnulot) as nbr_lot,
				st1.dnulot,
				CASE WHEN array_to_string(array_agg(DISTINCT st2.tbl_type_interloc_id), ',') = '' THEN
					array_to_string(array_agg(DISTINCT st1.type_interloc_id), ',')
				ELSE
					array_to_string(array_agg(DISTINCT st2.tbl_type_interloc_id), ',')
				END as tbl_type_interloc_id,
				CASE WHEN array_to_string(array_agg(DISTINCT st2.tbl_type_interloc_lib), ' / ') = '' THEN
					array_to_string(array_agg(DISTINCT st1.type_interloc_lib), ',')
				ELSE
					array_to_string(array_agg(DISTINCT st2.tbl_type_interloc_lib), ' / ')
				END as tbl_type_interloc_lib,
				st1.avis_favorable::text,
				ordre_eaf as ordre_eaf,
				MIN(type_interloc_id) as ordre_type,
				st1.entite_af_id,
				array_to_string(array_agg(COALESCE(nego_af_id, -1) ORDER BY lot_id), ',') as tbl_resultat,
				array_to_string(array_agg(COALESCE(nego_af_prix, -1) ORDER BY lot_id), ',') as tbl_resultat_prix,
				st1.can_be_ref,
				interloc_id_subst || '_' || par_lib || '_' || ordre_eaf || '_' || st1.avis_favorable || '_' || st1.can_be_ref as var_calc_nbr_lot,
				referent_id
			FROM
				(
					SELECT DISTINCT
						interloc_id as interloc_id_subst,
						particule_lib,
						nom_usage,
						prenom_usage,
						ddenom,
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,	
						t1.ccosec || t1.dnupla as par_lib,
						t2.dnulot,
						lot_id,
						type_interloc_id,
						type_interloc_lib,
						avis_favorable::text,
						CASE WHEN entite_af_id = :entite_af_id THEN 1 ELSE 2 END as ordre_eaf,
						entite_af_id,
						nego_af_id,
						nego_af_prix,
						can_be_ref::text,
						referent_id,
						can_nego::text,
						all_eaf::text,
						can_be_under_ref::text,
						can_be_subst::text,
						can_be_under_subst::text
					FROM
						cadastre.parcelles_cen t1
						JOIN cadastre.lots_cen t2 USING (par_id)
						JOIN cadastre.cadastre_cen t3 USING (lot_id)
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.interlocuteurs USING (interloc_id)
						LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
						LEFT JOIN 
							(
								SELECT
									rel_saf_foncier_interlocs_id,
									nego_af_id,
									nego_af_prix
								FROM
									animation_fonciere.rel_af_echanges
								WHERE
									echange_af_id::text = :echange_af_id::text
								
							) t_nego USING (rel_saf_foncier_interlocs_id)
							LEFT JOIN 
							(
								SELECT
									interloc_referent.rel_saf_foncier_interlocs_id,
									interloc_id as referent_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)
							) t_referent USING (rel_saf_foncier_interlocs_id)
							
					WHERE 
						session_af_id = :session_af_id AND 
						t3.date_fin = 'N.D.' AND
						t4.date_fin = 'N.D.' AND
						lot_bloque = 'f' AND 
						rel_saf_foncier.paf_id!= 0 AND 
						interloc_id = :interloc_id AND
						can_be_subst = 'f' AND
						(can_nego = 't' OR avis_favorable = 't') AND
						groupe_interloc_id != 3 AND
						rel_saf_foncier_interlocs_id NOT IN
							(
								SELECT 
									rel_saf_foncier_interlocs_id_vp
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
									JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
								WHERE 
									session_af_id = :session_af_id AND 
									groupe_interloc_id = 4
							)

					UNION

					SELECT DISTINCT
						interloc_id_subst,
						particule_lib,
						nom_usage,
						prenom_usage,
						ddenom,
						rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id,	
						t1.ccosec || t1.dnupla as par_lib,
						t2.dnulot,
						lot_id,
						t_subst.type_interloc_id as subst_type_interloc_id,
						t_subst.type_interloc_lib as subst_type_interloc_lib,
						d_type_interloc.avis_favorable::text,
						CASE WHEN entite_af_id = :entite_af_id THEN 1 ELSE 2 END as ordre_eaf,
						entite_af_id,
						nego_af_id,
						nego_af_prix,
						t_subst.can_be_ref::text,
						referent_id,
						d_type_interloc.can_nego::text,
						all_eaf::text,
						can_be_under_ref::text,
						can_be_subst::text,
						can_be_under_subst::text
					FROM
						cadastre.parcelles_cen t1
						JOIN cadastre.lots_cen t2 USING (par_id)
						JOIN cadastre.cadastre_cen t3 USING (lot_id)
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
						JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
						JOIN animation_fonciere.d_type_interloc ON (rel_saf_foncier_interlocs.type_interloc_id = d_type_interloc.type_interloc_id)
						JOIN
							(
								SELECT DISTINCT 
									rel_saf_foncier_interlocs.interloc_id as interloc_id_subst,
									rel_saf_foncier_interlocs_id_vp,
									d_type_interloc.type_interloc_id,
									d_type_interloc.type_interloc_lib,
									d_groupe_interloc.groupe_interloc_id,
									can_nego::text,
									avis_favorable::text,
									can_be_ref::text,
									all_eaf::text,
									can_be_under_ref::text,
									can_be_subst::text,
									can_be_under_subst::text
								FROM 
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
									JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
									JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
									JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
									JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is =  rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
								WHERE 
									session_af_id = :session_af_id AND 
									rel_saf_foncier_interlocs.interloc_id = :interloc_id AND
									groupe_interloc_id != 3
							) t_subst ON (t_subst.rel_saf_foncier_interlocs_id_vp = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
						JOIN animation_fonciere.interlocuteurs ON (t_subst.interloc_id_subst = interlocuteurs.interloc_id)
						LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
						LEFT JOIN 
							(
								SELECT
									rel_saf_foncier_interlocs_id,
									nego_af_id,
									nego_af_prix
								FROM
									animation_fonciere.rel_af_echanges
								WHERE
									echange_af_id::text = :echange_af_id::text
								
							) t_nego USING (rel_saf_foncier_interlocs_id)						
						LEFT JOIN 
							(
								SELECT
									interloc_referent.rel_saf_foncier_interlocs_id,
									interloc_id as referent_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs
									JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)

								UNION

								SELECT
									interloc_substitution.rel_saf_foncier_interlocs_id_vp,
									rel_saf_foncier_interlocs.interloc_id as referent_id
								FROM
									animation_fonciere.rel_saf_foncier_interlocs						
									JOIN animation_fonciere.interloc_referent ON (rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id_ref)
									JOIN animation_fonciere.rel_saf_foncier_interlocs t_temp_ref ON (t_temp_ref.rel_saf_foncier_interlocs_id = interloc_referent.rel_saf_foncier_interlocs_id)
									JOIN animation_fonciere.interloc_substitution ON (interloc_substitution.rel_saf_foncier_interlocs_id_is = t_temp_ref.rel_saf_foncier_interlocs_id)
							) t_referent USING (rel_saf_foncier_interlocs_id)	
					WHERE 
						session_af_id = :session_af_id AND 
						rel_saf_foncier.paf_id!= 0 AND
						d_type_interloc.groupe_interloc_id != 3 AND
						(d_type_interloc.can_nego = 't' OR d_type_interloc.avis_favorable = 't') AND
						t3.date_fin = 'N.D.' AND
						t4.date_fin = 'N.D.'
				) st1
				
				LEFT JOIN (
					SELECT DISTINCT
						array_to_string(array_agg(DISTINCT type_interloc_id), ',') as tbl_type_interloc_id,
						array_to_string(array_agg(DISTINCT type_interloc_lib), ' / ') as tbl_type_interloc_lib,
						entite_af_id,
						interloc_id,
						avis_favorable::text,
						can_nego::text,
						all_eaf::text,
						can_be_ref::text,
						can_be_under_ref::text,
						can_be_subst::text,
						can_be_under_subst::text,
						groupe_interloc_id
					FROM
						animation_fonciere.rel_saf_foncier_interlocs
						JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
						JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
						JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
					WHERE
						session_af_id = :session_af_id AND
						interloc_id = :interloc_id
					GROUP BY
						entite_af_id,
						interloc_id,
						avis_favorable,
						can_nego,
						all_eaf,
						can_be_ref,
						can_be_under_ref,
						can_be_subst,
						can_be_under_subst,
						groupe_interloc_id
				) st2 ON (st1.entite_af_id = st2.entite_af_id AND st1.interloc_id_subst = st2.interloc_id AND CASE WHEN groupe_interloc_id != 3 THEN st1.avis_favorable = st2.avis_favorable AND st1.can_nego = st2.can_nego AND st1.all_eaf = st2.all_eaf AND st1.can_be_ref = st2.can_be_ref AND st1.can_be_under_ref = st2.can_be_under_ref AND st1.can_be_subst = st2.can_be_subst AND st1.can_be_under_subst = st2.can_be_under_subst END)
				
			GROUP BY 
				interloc_id_subst,
				particule_lib,
				nom_usage,
				prenom_usage,
				ddenom,
				par_lib,
				st1.dnulot,
				st1.avis_favorable,
				ordre_eaf,
				st1.entite_af_id,
				st1.can_be_ref,
				var_calc_nbr_lot,
				referent_id
			ORDER BY 
				ordre_eaf,
				ordre_type,
				entite_af_id,
				interloc_id,
				avis_favorable,
				can_be_ref,
				par_lib";	
		}
		// echo $rq_lst_parcelles;	
		$res_lst_parcelles = $bdd->prepare($rq_lst_parcelles);
		$res_lst_parcelles->execute(array('session_af_id' => $session_af_id, 'entite_af_id' => $entite_af_id, 'interloc_id' => $interloc_id, 'echange_af_id' => $echange_af_id));
		$lst_parcelles = $res_lst_parcelles->fetchall();
		
		$result['code'] =  "";
		For ($p=0; $p<$res_lst_parcelles->rowCount(); $p++) {
			$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_parcelles[$p]['particule_lib'], $lst_parcelles[$p]['nom_usage'], $lst_parcelles[$p]['prenom_usage'], $lst_parcelles[$p]['ddenom']));
			
			If (isset($lst_parcelles[$p-1]['ordre_eaf']) && $lst_parcelles[$p-1]['ordre_eaf'] != $lst_parcelles[$p]['ordre_eaf'] && $representant == 'false') {
				$result['code'] .=  "
				<tr>
					<td colspan='8' style='font-variant:petite-caps;padding-top:20px;border-top:2px solid #004494;'>
						<label class='label'><u>Lot(s) présent dans d'autres EAF</u></label>
					</td>
				</tr>";
			}
			
			If ($p == 0 || (isset($lst_parcelles[$p-1]['interloc_id']) && $lst_parcelles[$p-1]['interloc_id'].'_'.$lst_parcelles[$p-1]['avis_favorable'].'_'.$lst_parcelles[$p-1]['can_be_ref'] != $lst_parcelles[$p]['interloc_id'].'_'.$lst_parcelles[$p]['avis_favorable'].'_'.$lst_parcelles[$p]['can_be_ref'])) {	
				$result['code'] .=  "
				<tr>
					<td colspan='8'>
						<label class='label'>";						
						$result['code'] .=  $interloc_lib;
						$result['code'] .= "  -  <i>".$lst_parcelles[$p]['tbl_type_interloc_lib']."</i>";
						$result['code'] .=  "
						</label>
					</td>
				</tr>";				
			}
							
			$maitrise = explode(',', $lst_parcelles[$p]['tbl_resultat']);
			$nego_prix = explode(',', $lst_parcelles[$p]['tbl_resultat_prix']);
			
			$result['code'] .= "
			<tr id='tr_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' class='tr_nego'>";
			If ($p == 0 || (isset($lst_parcelles[$p-1]['par_lib']) && $lst_parcelles[$p-1]['par_lib'].'_'.$lst_parcelles[$p-1]['avis_favorable'].'_'.$lst_parcelles[$p-1]['can_be_ref'] != $lst_parcelles[$p]['par_lib'].'_'.$lst_parcelles[$p]['avis_favorable'].'_'.$lst_parcelles[$p]['can_be_ref'])) {
				$rowspan = array_keys(array_values(array_column($lst_parcelles, 'var_calc_nbr_lot')), $lst_parcelles[$p]['var_calc_nbr_lot']);
				$result['code'] .= "
				<td rowspan='".count($rowspan)."' style='border-bottom: 1px solid #004494;'>
					<label class='label_simple'>".$lst_parcelles[$p]['par_lib']."</label>
				</td>";
			}
				$result['code'] .= "
				<td style='border-right:2px solid #004494;border-bottom: 1px solid #004494;'>
					<label class='label_simple'>".$lst_parcelles[$p]['dnulot']."</label>
				</td>";
			If ($lst_parcelles[$p]['avis_favorable'] == 'false') {
				For ($r=0; $r<6; $r++) {
					$result['code'] .= "
					<td style='border-bottom: 1px solid #004494;";
					If ($r==0 || $r==3 || $r==5) {
						$result['code'] .= "border-right:2px solid #004494;";
					}
					$result['code'] .= "'>
						<input type='checkbox' "; If ($r==$lst_parcelles[$p]['tbl_resultat']) {$result['code'] .= "checked ";} $result['code'] .= "class='rad_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']." chk_referent ech_".$r."' id='rad_result_ech_".$r."_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' name='rad_result_ech_".$r."_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' value='".$r."_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"updateRadMaitrise('rad_result_ech_".$r."_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."', 'rad');\">";
					If ($r==3 || $r==5) {
						$result['code'] .= "
						<input type='text' id='prix_result_ech_".$r."_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' name='prix_result_ech_".$r."_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' onBlur=\";updateRadMaitrise('prix_result_ech_".$r."_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."', 'prix');\" class='editbox chk_referent ech_".$r."' style='text-align:center; border:0px none; background:#ccc; padding:1px; "; If ($r==$lst_parcelles[$p]['tbl_resultat']) {$result['code'] .= "opacity:1' ";} Else {$result['code'] .= "opacity:0.2;' readonly disabled ";} $result['code'] .= " size='8' value='";
						If ($r==$lst_parcelles[$p]['tbl_resultat'] && $lst_parcelles[$p]['tbl_resultat_prix'] != -1) {$result['code'] .= $lst_parcelles[$p]['tbl_resultat_prix'];} $result['code'] .= "'><label class='label_simple'>€</label>";
					}
					$result['code'] .= "	
					</td>";
				}
			} Else {
				$result['code'] .= "
				<td style='border-bottom: 1px solid #004494;' colspan='4'>
					&nbsp;
				</td>
				<td style='border-bottom: 1px solid #004494; border-right:2px solid #004494;' colspan='2'>
					<input type='checkbox' "; If ($lst_parcelles[$p]['tbl_resultat']==6) {$result['code'] .= "checked ";} $result['code'] .= "class='rad_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']." chk_referent ech_6' id='rad_result_ech_6_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' name='rad_result_ech_6_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' value='6_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"updateRadMaitrise('rad_result_ech_6_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."', 'rad');\">
					<label class='label_simple'>Favorable</label>
					<input type='checkbox' "; If ($lst_parcelles[$p]['tbl_resultat']==7) {$result['code'] .= "checked ";} $result['code'] .= "class='rad_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']." chk_referent ech_7' id='rad_result_ech_7_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' name='rad_result_ech_7_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' value='7_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"updateRadMaitrise('rad_result_ech_7_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."', 'rad');\">
					<label class='label_simple'>Défavorable</label>
				</td>";
			}
				
			$result['code'] .= "
			</tr>";
		}
		
		If ($representant == 'false') {
			$rq_lst_interlocs_referent = "
			SELECT
				interloc_id,
				particule_lib,
				nom_usage,
				prenom_usage,
				ddenom,
				array_to_string(array_agg(DISTINCT interloc_referent.rel_saf_foncier_interlocs_id), ',') as tbl_rel_saf_foncier_interlocs_id
			FROM
				animation_fonciere.interloc_referent
				JOIN animation_fonciere.rel_saf_foncier_interlocs ON (interloc_referent.rel_saf_foncier_interlocs_id = rel_saf_foncier_interlocs.rel_saf_foncier_interlocs_id)
				JOIN animation_fonciere.d_type_interloc USING (type_interloc_id)
				JOIN animation_fonciere.interlocuteurs USING (interloc_id)
				LEFT JOIN cadastre.d_ccoqua USING (ccoqua)
			WHERE
				rel_saf_foncier_interlocs_id_ref IN 
					(
						SELECT 
							rel_saf_foncier_interlocs_id
						FROM 
							animation_fonciere.rel_saf_foncier_interlocs
							JOIN animation_fonciere.rel_saf_foncier USING (rel_saf_foncier_id)
							JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
						WHERE 
							interloc_id = :interloc_id
							AND session_af_id = :session_af_id
							AND entite_af_id = :entite_af_id
					)
			GROUP BY 
				interloc_id,
				particule_lib,
				nom_usage,
				prenom_usage,
				ddenom";
			$res_lst_interlocs_referent = $bdd->prepare($rq_lst_interlocs_referent);
			$res_lst_interlocs_referent->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id, 'interloc_id'=>$interloc_id));
			$lst_interlocs_referent = $res_lst_interlocs_referent->fetchall();
			
			If ($res_lst_interlocs_referent->rowCount() > 0) {
				$result['code'] .=  "
				<tr style='display:$display_tbl_parc_maitrise_other_eaf;' class='display_tbl_parc_maitrise_other_eaf'>
					<td colspan='8' style='font-variant:petite-caps;padding-top:30px;border-top:4px solid #ccc;'>
						<label class='label'><u>Interlocuteurs référencés par $interloc_lib</u></label>
					</td>
				</tr>";
			}
			For ($j=0; $j<$res_lst_interlocs_referent->rowCount(); $j++) {
				$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_interlocs_referent[$j]['particule_lib'], $lst_interlocs_referent[$j]['nom_usage'], $lst_interlocs_referent[$j]['prenom_usage'], $lst_interlocs_referent[$j]['ddenom']));
				
				$res_lst_parcelles->execute(array('session_af_id' => $session_af_id, 'entite_af_id' => $entite_af_id, 'interloc_id' => $lst_interlocs_referent[$j]['interloc_id'], 'echange_af_id' => $echange_af_id));
				$lst_parcelles = $res_lst_parcelles->fetchall();
				$tbl_index_referent = array_keys(array_values(array_column($lst_parcelles, 'referent_id')), $interloc_id);
				For ($p=0; $p<count($tbl_index_referent); $p++) {
					$interloc_lib = str_replace('   ', ' ', proprio_lib($lst_parcelles[$tbl_index_referent[$p]]['particule_lib'], $lst_parcelles[$tbl_index_referent[$p]]['nom_usage'], $lst_parcelles[$tbl_index_referent[$p]]['prenom_usage'], $lst_parcelles[$tbl_index_referent[$p]]['ddenom']));
					
					If (isset($tbl_index_referent[$p-1]) && $lst_parcelles[$tbl_index_referent[$p-1]]['ordre_eaf'] != $lst_parcelles[$tbl_index_referent[$p]]['ordre_eaf']) {
						$result['code'] .=  "
						<tr style='display:$display_tbl_parc_maitrise_other_eaf;' class='display_tbl_parc_maitrise_other_eaf'>
							<td colspan='8' style='font-variant:petite-caps;padding-top:20px;border-top:2px solid #004494;'>
								<label class='label'><u>Lot(s) présent dans d'autres EAF</u></label>
							</td>
						</tr>";
					}
				
					If ($p == 0 || (isset($lst_parcelles[$tbl_index_referent[$p-1]]['interloc_id']) && $lst_parcelles[$tbl_index_referent[$p-1]]['interloc_id'].'_'.$lst_parcelles[$tbl_index_referent[$p-1]]['entite_af_id'].'_'.$lst_parcelles[$tbl_index_referent[$p-1]]['avis_favorable'].'_'.$lst_parcelles[$tbl_index_referent[$p-1]]['can_be_ref'] != $lst_parcelles[$tbl_index_referent[$p]]['interloc_id'].'_'.$lst_parcelles[$tbl_index_referent[$p]]['entite_af_id'].'_'.$lst_parcelles[$tbl_index_referent[$p]]['avis_favorable'].'_'.$lst_parcelles[$tbl_index_referent[$p]]['can_be_ref'])) {		
						$result['code'] .=  "
						<tr style='display:$display_tbl_parc_maitrise_other_eaf;' class='display_tbl_parc_maitrise_other_eaf'>
							<td colspan='8'>
								<label class='label'>";						
								$result['code'] .=  $interloc_lib;
								$result['code'] .= "  -  <i>".$lst_parcelles[$tbl_index_referent[$p]]['tbl_type_interloc_lib']."</i>";
								$result['code'] .=  "
								</label>
							</td>
						</tr>";				
					}
									
					$maitrise = explode(',', $lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat']);
					$nego_prix = explode(',', $lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat_prix']);
					
					$result['code'] .= "
					<tr id='tr_".$lst_parcelles[$p]['tbl_rel_saf_foncier_interlocs_id']."' style='display:$display_tbl_parc_maitrise_other_eaf;' class='tr_nego display_tbl_parc_maitrise_other_eaf'>";
					If ($p == 0 || (isset($lst_parcelles[$tbl_index_referent[$p-1]]['par_lib']) && $lst_parcelles[$tbl_index_referent[$p-1]]['par_lib'].'_'.$lst_parcelles[$tbl_index_referent[$p-1]]['avis_favorable'].'_'.$lst_parcelles[$tbl_index_referent[$p-1]]['can_be_ref'] != $lst_parcelles[$tbl_index_referent[$p]]['par_lib'].'_'.$lst_parcelles[$tbl_index_referent[$p]]['avis_favorable'].'_'.$lst_parcelles[$tbl_index_referent[$p]]['can_be_ref'])) {
						$rowspan = array_keys(array_values(array_column($lst_parcelles, 'var_calc_nbr_lot')), $lst_parcelles[$p]['var_calc_nbr_lot']);
						$result['code'] .= "
						<td rowspan='".count($rowspan)."' style='border-bottom: 1px solid #004494;'>
							<label class='label_simple'>".$lst_parcelles[$p]['par_lib']."</label>
						</td>";
					}
						$result['code'] .= "
						<td style='border-right:2px solid #004494;border-bottom: 1px solid #004494;'>
							<label class='label_simple'>".$lst_parcelles[$tbl_index_referent[$p]]['dnulot']."</label>
						</td>";
					If ($lst_parcelles[$tbl_index_referent[$p]]['avis_favorable'] == 'false') {
						For ($r=0; $r<6; $r++) {
							$result['code'] .= "
							<td style='border-bottom: 1px solid #004494;";
							If ($r==0 || $r==3 || $r==5) {
								$result['code'] .= "border-right:2px solid #004494;";
							}
							$result['code'] .= "'>
								<input type='checkbox' "; If ($r==$lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat']) {$result['code'] .= "checked ";} $result['code'] .= "class='rad_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']." chk_reference  ech_".$r."' id='rad_result_ech_".$r."_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' name='rad_result_ech_".$r."_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' value='".$r."_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"updateRadMaitrise('rad_result_ech_".$r."_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."', 'rad');\">";
							If ($r==3 || $r==5) {
								$result['code'] .= "
								<input type='text' id='prix_result_ech_".$r."_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' name='prix_result_ech_".$r."_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' onBlur=\";updateRadMaitrise('prix_result_ech_".$r."_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."', 'prix');\" class='editbox chk_reference ech_".$r."' style='text-align:center; border:0px none; background:#ccc; padding:1px; "; If ($r==$lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat']) {$result['code'] .= "opacity:1' ";} Else {$result['code'] .= "opacity:0.2;' readonly disabled ";} $result['code'] .= " size='8' value='";
								If ($r==$lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat'] && $lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat_prix'] != -1) {$result['code'] .= $lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat_prix'];} $result['code'] .= "'><label class='label_simple'>€</label>";
							}
							$result['code'] .= "	
							</td>";
						}
					} Else {
						$result['code'] .= "
						<td style='border-bottom: 1px solid #004494;' colspan='4'>
							&nbsp;
						</td>
						<td style='border-bottom: 1px solid #004494; border-right:2px solid #004494;' colspan='2'>
							<input type='checkbox' "; If ($lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat']==6) {$result['code'] .= "checked ";} $result['code'] .= "class='rad_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']." chk_reference ech_6' id='rad_result_ech_6_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' name='rad_result_ech_6_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' value='6_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"updateRadMaitrise('rad_result_ech_6_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."', 'rad');\">
							<label class='label_simple'>Favorable</label>
							<input type='checkbox' "; If ($lst_parcelles[$tbl_index_referent[$p]]['tbl_resultat']==7) {$result['code'] .= "checked ";} $result['code'] .= "class='rad_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']." chk_reference ech_7' id='rad_result_ech_7_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' name='rad_result_ech_7_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' value='7_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."' onclick=\"updateRadMaitrise('rad_result_ech_7_".$lst_parcelles[$tbl_index_referent[$p]]['tbl_rel_saf_foncier_interlocs_id']."', 'rad');\">
							<label class='label_simple'>Défavorable</label>
						</td>";
					}
						
					$result['code'] .= "
					</tr>";
				}
			}
		}
		$res_lst_parcelles->closeCursor();
		$bdd = null;
		
		return $result['code'];
	}
	
	/* Fonction permettant d'afficher et de mettre à jour la section "Occupation du sol" de l'onglet "Parcelles/ Lots". */
	function fct_rq_occsol_parc($rel_saf_foncier_id, $session_af_id, $entite_af_id, $nbr_echanges) {
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../../includes/prob_tech.php'));
		}
	
		$rq_occsol = 
		"SELECT DISTINCT
			type_occsol_af_id,
			type_occsol_af_lib,
			pourcentage,
			montant,
			commentaire
		FROM
			animation_fonciere.rel_saf_foncier_occsol
			JOIN animation_fonciere.d_type_occsol_af USING (type_occsol_af_id)
		WHERE rel_saf_foncier_id = ?
		ORDER BY type_occsol_af_id";
		$res_occsol = $bdd->prepare($rq_occsol);
		$res_occsol->execute(array($rel_saf_foncier_id));					
		$occsol = $res_occsol->fetchall();
		
		$result['code'] = '';
		
		For ($o=0; $o<$res_occsol->rowCount(); $o++) {
			$result['code'] .= "
			<tr>
				<td>
					<label class='btn_combobox' style='margin-left:5px;'>			
						<select id='type_occsol-".$rel_saf_foncier_id."-".$o."' class='combobox type_occsol' style='text-align:left;' onchange=\"isUpdatable(this, 'occsol', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\">";																	
							$rq_lst_type_occsol = "
							SELECT 
								type_occsol_af_id as id,
								type_occsol_af_lib as lib
							FROM 
								animation_fonciere.d_type_occsol_af
							WHERE
								type_occsol_af_id NOT IN (
									SELECT DISTINCT
										type_occsol_af_id
									FROM
										animation_fonciere.rel_saf_foncier_occsol
									WHERE 
										rel_saf_foncier_id = :rel_saf_foncier_id AND
										type_occsol_af_id != :id)";
							$res_lst_type_occsol = $bdd->prepare($rq_lst_type_occsol);
							$res_lst_type_occsol->execute(array('rel_saf_foncier_id'=>$rel_saf_foncier_id, 'id'=>$occsol[$o]['type_occsol_af_id']));
							$result['code'] .= "<option value='-1'></option>";
							While ($donnees = $res_lst_type_occsol->fetch()) {
								$result['code'] .= "<option ";
								If ($res_occsol->rowCount() > 0 && $donnees['id'] == $occsol[$o]['type_occsol_af_id']) {
									$result['code'] .= "selected ";
								}
								$result['code'] .= "value='$donnees[id]'>$donnees[lib]</option>";
							}
						$result['code'] .= "					
						</select>
					</label>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox pourcentage_occsol' type='text' id='pourc_occsol-".$rel_saf_foncier_id."-".$o."' size='3' onkeyup=\"isUpdatable(this, 'occsol', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" value='".$occsol[$o]['pourcentage']."'><label class='label_simple'>%</label>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox prix_occsol' type='text' id='prix_occsol-".$rel_saf_foncier_id."-".$o."' size='3' onkeyup=\"isUpdatable(this, 'occsol', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" value='".$occsol[$o]['montant']."'><label class='label_simple'>€ le m²</label>
				</td>
			</tr>";	
		}
		If (array_sum(array_column($occsol, 'pourcentage')) < 100) {
			$result['code'] .= "	
			<tr>
				<td>
					<label class='btn_combobox' style='margin-left:5px;'>			
						<select id='type_occsol-".$rel_saf_foncier_id."-".$o."' class='combobox occsol' style='text-align:left;' onchange=\"isUpdatable(this, 'occsol', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\">";																	
							$rq_lst_type_occsol = "
							SELECT 
								type_occsol_af_id as id,
								type_occsol_af_lib as lib
							FROM 
								animation_fonciere.d_type_occsol_af
							WHERE
								type_occsol_af_id NOT IN (
									SELECT DISTINCT
										type_occsol_af_id
									FROM
										animation_fonciere.rel_saf_foncier_occsol
									WHERE 
										rel_saf_foncier_id = :rel_saf_foncier_id)";
							$res_lst_type_occsol = $bdd->prepare($rq_lst_type_occsol);
							$res_lst_type_occsol->execute(array('rel_saf_foncier_id'=>$rel_saf_foncier_id));	
							$result['code'] .= "<option value='-1'></option>";
							While ($donnees = $res_lst_type_occsol->fetch()) {
								$result['code'] .= "<option value='$donnees[id]'>$donnees[lib]</option>";
							}
						$result['code'] .= "					
						</select>
					</label>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox pourcentage_occsol' type='text' id='pourc_occsol-".$rel_saf_foncier_id."-".$o."' onkeyup=\"isUpdatable(this, 'occsol', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" size='3'><label class='label_simple'>%</label>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox prix_occsol' type='text' id='prix_occsol-".$rel_saf_foncier_id."-".$o."' onkeyup=\"isUpdatable(this, 'occsol', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" size='3'><label class='label_simple'>€ le m²</label>
				</td>
			</tr>";			
		}
		
		return $result['code'];
	}
	
	/* Fonction permettant d'afficher et de mettre à jour la section "Prix d'acquisition" de l'onglet "Parcelles/ Lots". */
	function fct_rq_prix_parc($rel_saf_foncier_id, $session_af_id, $entite_af_id, $nbr_echanges) {
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../../includes/prob_tech.php'));
		}
	
		$rq_prix = 
		"SELECT DISTINCT
			prix_lib,
			montant,
			quantite
		FROM
			animation_fonciere.rel_saf_foncier_prix
		WHERE rel_saf_foncier_id = ?
		ORDER BY montant DESC";
		$res_prix = $bdd->prepare($rq_prix);
		$res_prix->execute(array($rel_saf_foncier_id));					
		$prix = $res_prix->fetchall();
		
		$result['code'] = '';
		$prix_acquisition = 0;
		For ($o=0; $o<$res_prix->rowCount(); $o++) {
			$result['code'] .= "
			<tr class='prix_detail'>
				<td>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox' type='text' id='lib_prix-".$rel_saf_foncier_id."-".$o."' size='40' onkeyup=\"isUpdatable(this, 'prix', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" value='".$prix[$o]['prix_lib']."'>
				</td>
				<td>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox' type='text' id='montant_prix-".$rel_saf_foncier_id."-".$o."' size='5' onkeyup=\"isUpdatable(this, 'prix', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" value='".number_format($prix[$o]['montant'],2, '.', '')."'><label class='label_simple'>€</label>
				</td>
				<td>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox' type='text' id='quantite_prix-".$rel_saf_foncier_id."-".$o."' size='5' onkeyup=\"isUpdatable(this, 'prix', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" value='".$prix[$o]['quantite']."'>
				</td>
				<td>
					<label class='label'> = </label>
				</td>
				<td style='text-align:right;'>
					<label class='label'>".number_format($prix[$o]['quantite']*$prix[$o]['montant'],2, '.', ' ')." €</label>
				</td>
			</tr>";	
			$prix_acquisition += $prix[$o]['quantite']*$prix[$o]['montant'];
		}
		
			$result['code'] .= "	
			<tr class='prix_detail'>
				<td>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox' type='text' id='lib_prix-".$rel_saf_foncier_id."-".$o."' size='40' onkeyup=\"isUpdatable(this, 'prix', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\">
				</td>
				<td>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox' type='text' id='montant_prix-".$rel_saf_foncier_id."-".$o."' size='5' onkeyup=\"isUpdatable(this, 'prix', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\"><label class='label_simple'>€</label>
				</td>
				<td>
					<input style='text-align:right;margin-left:10px;border:0' class='editbox' type='text' id='quantite_prix-".$rel_saf_foncier_id."-".$o."' size='5' value='1' onkeyup=\"isUpdatable(this, 'prix', ".$rel_saf_foncier_id.", ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\">
				</td>
				<td colspan='2'>
					&nbsp;
				</td>
			</tr>";
		If ($res_prix->rowCount()>0) {	
			$result['code'] .= "
			<tr>
				<td colspan='4'>
					&nbsp;				
				</td>
				<td style='border-top:1px solid #ccc;text-align:right;'>
					<label class='label'>".number_format($prix_acquisition,2, '.', ' ')." €</label>
				</td>
			</tr>";			
		}
		
		return $result['code'];
	}
	
	/* Fonction permettant d'afficher et de mettre à jour la section "Prix d'acquisition" de l'onglet "Parcelles/ Lots". */
	function fct_rq_prix_eaf($session_af_id, $entite_af_id, $nbr_echanges) {
		include ('../../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../../includes/prob_tech.php'));
		}
	
		$rq_prix_eaf = "
		SELECT DISTINCT 
			rel_saf_foncier_id,
			CASE WHEN COALESCE(prix_parc_arrondi, -1) > 0 THEN prix_parc_arrondi ELSE CASE WHEN COALESCE(SUM(montant*quantite), -1) > 0 THEN SUM(montant*quantite) ELSE -1 END END as prix_parc,
			COALESCE(prix_eaf_arrondi, NULL) as prix_eaf_arrondi,
			COALESCE(prix_eaf_negocie, -1) as prix_eaf_negocie	
		FROM 		
			animation_fonciere.entite_af
			JOIN animation_fonciere.rel_eaf_foncier USING (entite_af_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			LEFT JOIN animation_fonciere.rel_saf_foncier_prix USING (rel_saf_foncier_id)
		WHERE
			session_af_id = :session_af_id AND 
			entite_af_id = :entite_af_id AND
			objectif_af_id = 1
		GROUP BY
			rel_saf_foncier_id";	
		$res_prix_eaf = $bdd->prepare($rq_prix_eaf);
		// echo $rq_prix_eaf;
		$res_prix_eaf->execute(array('session_af_id'=>$session_af_id, 'entite_af_id'=>$entite_af_id));
		$prix_eaf = $res_prix_eaf->fetchall();
		$result['code'] = '';
		If ($res_prix_eaf->rowCount() > 0) {
			If (!in_array(-1, array_column($prix_eaf, 'prix_parc'))) {
				$result['code'] .= "							
				<label id='lbl_montant_eaf_calcule-".$entite_af_id."' class='label_simple' style='margin-left:25px;'><i>calcul&eacute; : ".number_format(array_sum(array_column($prix_eaf, 'prix_parc')),2, '.', ' ')." €</i></label>";
			}
			$result['code'] .= "
			<label class='label_simple' style='margin-left:25px;'><i>opérateur :</i>							
				<input style='text-align:right;margin-left:10px;border:0' class='editbox' type='text' id='montant_eaf-".$entite_af_id."' size='8' onkeyup=\"isUpdatable(this, 'montant_eaf', 'ALL', ".$entite_af_id.", ".$session_af_id.", ".$nbr_echanges.");\" value='".$prix_eaf[0]['prix_eaf_arrondi']."'> €
				<img id='img_save_montant_eaf-".$entite_af_id."' src='".ROOT_PATH."images/enregistrer.png' width='15px' style='cursor:auto;opacity:0.3;' onclick=''>
			</label>
			";
			If ($prix_eaf[0]['prix_eaf_negocie'] > 0) {
				$result['code'] .= "							
				<label class='label_simple' style='margin-left:25px;'><i>en cours de négociation : ".$prix_eaf[0]['prix_eaf_negocie']." €</i></label>
				";
			}			
		} Else {
			$result['code'] .= "							
			<label id='lbl_eaf_no_acqu-".$entite_af_id."' class='label_simple' style='margin-left:25px;'>Aucun objectif d'acquisition sur cette EAF</label>
			";
		}
		
		return $result['code'];
	}
?>