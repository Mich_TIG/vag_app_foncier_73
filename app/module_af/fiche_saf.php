<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include('../../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/explorateur.css">		
		<link rel="stylesheet" type="text/css" href= "onglet_af.css">
		<link rel="stylesheet" type="text/css" href="af.css">
		<link rel="stylesheet" type="text/css" href="jquery-ui-af.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui-1.8.custom.min.js"></script>
		<script type="text/javascript" src="tooltip-af.js"></script><div id="tooltip-af"></div>		
		<script type="text/javascript" src="af.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script>
			jQuery(function($){
				$.datepicker.setDefaults($.datepicker.regional['fr']);
				$('.datepicker').datepicker({
					dateFormat : 'dd/mm/yy',
					changeMonth: true,
					changeYear: true
				});
			});
		</script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			include ('../../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig', 'grp_foncier'));
			$h2 ='Foncier';
			include('../../includes/header.php');
			include('../../includes/breadcrumb.php');
			//include('../../includes/construction.php');
			include('../foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
			include('../../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure		
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../../includes/prob_tech.php'));
			}		
			
			If (isset($_GET['d']) && substr_count(base64_decode($_GET['d']), ':') > 0 && substr_count(base64_decode($_GET['d']), ';') > 0) {
				$d= base64_decode($_GET['d']);
				$min_crit = 0;
				$max_crit = strpos($d, ':');
				$min_val = strpos($d, ':') + 1;
				$max_val = strpos($d, ';') - $min_val;
				
				For ($p=0; $p<substr_count($d, ':'); $p++) {
					$crit = substr($d, $min_crit, $max_crit);
					$value = substr($d, $min_val, $max_val);
					$$crit = $value;
					$min_crit = strpos($d, ';', $min_crit) + 1;
					$max_crit= strpos($d, ':', $min_crit) - $min_crit;
					$min_val = $min_crit + $max_crit + 1;
					$max_val = strpos($d, ';', $min_val) - $min_val;
				}
				If (!isset($session_af_id)) {
					$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
					include('../../includes/prob_tech.php');
				}
			} Else {
				$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
				include('../../includes/prob_tech.php');
			}
			
			$rq_info_parcelle =
			"SELECT 
				coalesce(count(distinct t1.par_id),0) as nbr_parcelle, 
				coalesce(count(*),0) as nbr_lot, coalesce(sum(dcntlo),0) as surf_total
			FROM 
				cadastre.parcelles_cen t1 
				JOIN cadastre.lots_cen t2 USING (par_id) 
				JOIN cadastre.cadastre_cen t3 USING (lot_id) 
				JOIN foncier.cadastre_site t4 USING (cad_cen_id) 
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			WHERE 
				rel_saf_foncier.session_af_id = ? AND
				t3.date_fin = 'N.D.' AND
				t4.date_fin = 'N.D.'";
			$res_info_parcelle = $bdd->prepare($rq_info_parcelle);
			$res_info_parcelle->execute(array($session_af_id));
			$info_parcelle = $res_info_parcelle->fetch();
			If ($res_info_parcelle->rowCount() == 0){
				include('../includes/prob_tech.php');
			}

			$rq_info_proprio =
			"SELECT 
				count(distinct t6.dnupro) as nbr_compte, 
				count(distinct t6.dnuper) as nbr_proprio
			FROM 
				cadastre.cadastre_cen t3 
				JOIN foncier.cadastre_site t4 USING (cad_cen_id) 
				JOIN cadastre.cptprop_cen t5 USING(dnupro) 
				JOIN cadastre.r_prop_cptprop_cen t6 USING (dnupro)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			WHERE rel_saf_foncier.session_af_id = ?";
			$res_info_proprio = $bdd->prepare($rq_info_proprio);
			$res_info_proprio->execute(array($session_af_id));
			$info_proprio = $res_info_proprio->fetch();
			If ($res_info_proprio->rowCount() == 0){
				include('../../includes/prob_tech.php');
			}
								
			$rq_obj_animation = "			
			SELECT 
				t_objectif.objectif_af_lib as lib,
				COALESCE(t_af.nbr_lot_objectif_acquisition, 0) AS nbr_lot,
				t_objectif.objectif_af_id as ordre
			FROM
				(SELECT DISTINCT d_objectif_af.objectif_af_id, d_objectif_af.objectif_af_lib FROM animation_fonciere.d_objectif_af ORDER BY d_objectif_af.objectif_af_id) t_objectif
				LEFT JOIN
				(SELECT 
					rel_saf_foncier.objectif_af_id,
					count(DISTINCT t3.lot_id) as nbr_lot_objectif_acquisition
				FROM 
					cadastre.cadastre_cen t3
					JOIN foncier.cadastre_site t4 USING (cad_cen_id)
					JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
					JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				WHERE 
					lot_bloque = 'f' AND
					rel_saf_foncier.session_af_id = :session_af_id
				GROUP BY rel_saf_foncier.objectif_af_id
				) t_af
				ON t_objectif.objectif_af_id = t_af.objectif_af_id
			ORDER BY ordre";
			// echo $rq_obj_animation;
			$res_obj_animation = $bdd->prepare($rq_obj_animation);
			$res_obj_animation->execute(array('session_af_id'=>$session_af_id));
			$obj_animation = $res_obj_animation->fetchAll();
			
			$rq_bilan_animation = "
			SELECT DISTINCT 
				'Lots acquis' as lib,
				count(t3.lot_id) as nbr_lot,
				-5 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.r_cad_site_mf USING (cad_site_id)
				JOIN foncier.mf_acquisitions USING (mf_id)
				JOIN foncier.r_mfstatut USING (mf_id),
				(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id) AND r_mfstatut.statutmf_id = '1'
			UNION
			SELECT DISTINCT 
				'Lots en projet d''acquisition' as lib,
				count(t3.lot_id) as nbr_lot,
				-4 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.r_cad_site_mf USING (cad_site_id)
				JOIN foncier.mf_acquisitions USING (mf_id)
				JOIN foncier.r_mfstatut USING (mf_id),
				(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id) AND r_mfstatut.statutmf_id = '0'
			UNION
			SELECT DISTINCT 
				'Lots conventionn&eacute;s' as lib,
				count(t3.lot_id) as nbr_lot,
				-3 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.r_cad_site_mu USING (cad_site_id)
				JOIN foncier.mu_conventions USING (mu_id)
				JOIN foncier.r_mustatut USING (mu_id),
				(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND (r_mustatut.date = tt2.statut_date AND r_mustatut.mu_id = tt2.mu_id) AND r_mustatut.statutmu_id = '1'
			UNION
			SELECT DISTINCT 
				'Lots en projet de convention' as lib,
				count(t3.lot_id) as nbr_lot,
				-2 as ordre
			FROM 
				cadastre.cadastre_cen t3
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.r_cad_site_mu USING (cad_site_id)
				JOIN foncier.mu_conventions USING (mu_id)
				JOIN foncier.r_mustatut USING (mu_id),
				(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt2
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND (r_mustatut.date = tt2.statut_date AND r_mustatut.mu_id = tt2.mu_id) AND r_mustatut.statutmu_id = '0'
			UNION
			SELECT DISTINCT 
				'Lots bloqu&eacute;s' as lib,
				count(rel_saf_foncier_id) as nbr_lot,
				-1 as ordre
			FROM 
				animation_fonciere.rel_saf_foncier				
			WHERE 
				rel_saf_foncier.session_af_id = :session_af_id AND 
				lot_bloque = 't'			
			ORDER BY ordre";
			// echo $rq_bilan_animation;
			$res_bilan_animation = $bdd->prepare($rq_bilan_animation);
			$res_bilan_animation->execute(array('session_af_id'=>$session_af_id));
			$bilan_animation = $res_bilan_animation->fetchAll();
			
			$rq_lst_sites =
			"SELECT DISTINCT
				sites.site_id,
				sites.site_nom
			FROM 
				sites.sites
				JOIN foncier.cadastre_site t4 USING (site_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
				JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			WHERE 
				rel_saf_foncier.session_af_id = ? AND 
				t3.date_fin = 'N.D.' AND 
				t4.date_fin = 'N.D.';";
			$res_lst_sites = $bdd->prepare($rq_lst_sites);
			$res_lst_sites->execute(array($session_af_id));
			$lst_sites = $res_lst_sites->fetchall();
			
			$rq_lst_communes =
			"SELECT 
				array_to_string(array_agg(DISTINCT t0.nom),',') as communes
			FROM 
				administratif.communes t0
				JOIN cadastre.parcelles_cen t1 ON (t0.code_insee = t1.codcom)
				JOIN cadastre.lots_cen t2 USING (par_id) 
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
				JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			WHERE 
				t0.actif = TRUE AND 
				rel_saf_foncier.session_af_id = ? AND 
				t3.date_fin = 'N.D.' AND 
				t4.date_fin = 'N.D.';";
			$res_lst_communes = $bdd->prepare($rq_lst_communes);
			$res_lst_communes->execute(array($session_af_id));
			$lst_communes = $res_lst_communes->fetch();
			$lst_communes = preg_split("/,/", $lst_communes['communes']);
			
			$rq_ref_foncier = "
			SELECT
				individu_id,
				prenom,
				nom
			FROM
				animation_fonciere.session_af
				JOIN admin_sig.utilisateurs ON (utilisateur = utilisateur_id)
				JOIN personnes.d_individu USING (individu_id)
			WHERE
				session_af_id = :session_af_id";
			$res_ref_foncier = $bdd->prepare($rq_ref_foncier);
			$res_ref_foncier->execute(array($session_af_id));
			$ref_foncier = $res_ref_foncier->fetch();			
		?>
		
		<section id='main'>
			<h3>Fiche session d'animation fonci&egrave;re : <?php echo $session_af_lib; ?></h3>
			<div id='onglet' class='list'>
				<a href="fiche_saf_help.php#general" target="_blank">
				<img id='img_help_thema' src='<?php echo ROOT_PATH; ?>images/help.png' width='30px' style='position:absolute;right:40px;margin-left:10px;cursor:pointer;' ></a>
				<ul>
					<li>
						<a href="#general"><span>G&eacute;n&eacute;ral</span></a>
					</li>
					<li>
						<a href="#interloc" onclick="create_lst_interlocs(0, '<?php echo $session_af_id; ?>', 0);"><span><?php echo utf8_encode("Interlocuteurs"); ?></span></a>
					</li>
					<li>
						<a href="#parcelle" onclick="create_lst_parc(0, '<?php echo $session_af_id; ?>', 0);"><span><?php echo utf8_encode("Parcelles/ Lots"); ?></span></a>
					</li>
					<li>
						<a href="#echanges" onclick="create_lst_echanges(0, '<?php echo $session_af_id; ?>', 0);"><span><?php echo utf8_encode("Echanges"); ?></span></a>
					</li>
					<li>
						<a href="#synthese" onclick="create_synthese('charge', 0, '<?php echo $session_af_id; ?>', 0);"><span><?php echo utf8_encode("Synth&egrave;se"); ?></span></a>
					</li>
				</ul>
				
				<div id='general'>					
					<table width = '100%' CELLSPACING = '5' CELLPADDING ='5' style='border:0px solid black'>
						<tr>
							<td style='text-align:right' colspan='3'>
								<label class="last_maj">
									<u>R&eacute;f&eacute;rent foncier  :</u> 
									<?php 
										If ($ref_foncier['individu_id'] > 0) {
											echo $ref_foncier['prenom'].' '.$ref_foncier['nom']; 
										} Else {
											echo 'non d&eacute;fini';
										}
									?>
								</label>
							</td>
						</tr>
						<tr>
							<td style='vertical-align:top'>
								<table CELLSPACING = '5' CELLPADDING ='5' class='no-border'>
									<tr>
										<td class='bilan-af' style='text-align:right;'>
											<label class="label">
												<u>Site<?php If ($res_lst_sites->rowCount() > 1) {echo "s";} ?> :</u>
											</label>
										</td>
										<td class='bilan-af'>
										<?php 
											For ($i=0; $i<$res_lst_sites->rowCount(); $i++) {
												$lst_site[] = "'".$lst_sites[$i]['site_id']."'";
												$vget = "site_nom:".$lst_sites[$i]['site_nom'].";site_id:".$lst_sites[$i]['site_id'].";";
												$vget_crypt = base64_encode($vget);
												$lien_site = ROOT_PATH . "foncier/fiche_site.php?d=$vget_crypt#general";
										?>
											<li>
												<label class='label'><?php echo $lst_sites[$i]['site_nom']; ?> (<?php echo $lst_sites[$i]['site_id']; ?>)</label>
												<a href='<?php echo $lien_site; ?>'>
													<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche site">
												</a>
											</li>
										<?php 
											}
										?>
										</td>
									</tr>
									<tr>
										<td class='bilan-af'>
											<label class="label">
												<u>Commune<?php If (count($lst_communes)>1) {echo "s";} ?> :</u>
											</label>
										</td>
										<td class='bilan-af'>
											<?php For ($i=0; $i<count($lst_communes); $i++) { ?>
											<li>
												<label class='label'><?php echo $lst_communes[$i]; ?></label>
											</li>
											<?php } ?>
										</td>
									</tr>
								</table>
							</td>
							<td style='vertical-align:top'>
								<table id='tbl_situation_cadastre' CELLSPACING = '5' CELLPADDING ='5' class='bilan-af'>
									<tr>
										<td colspan='2' class='bilan-af'>
											<label class="label">
												<u>Superficie cadastrale  :</u> <?php echo conv_are($info_parcelle['surf_total']) ?> ha.a.ca
											</label>
										</td>
									</tr>
									<tr>
										<td class='bilan-af'>
											<label class="label">
												<u>Situation cadastrale  :</u>
											</label>
										</td>
										<td class='bilan-af'>
											<li>
												<label class="label"><?php echo $info_parcelle['nbr_parcelle'];?> parcelle<?php If ($info_parcelle['nbr_parcelle']>1) {echo "s";} ?></label>
											</li>
											<li>
												<label class="label"><?php echo $info_parcelle['nbr_lot'];?> lot<?php If ($info_parcelle['nbr_lot']>1) {echo "s";} ?></label>
											</li>
											<li>
												<label class="label"><?php echo $info_proprio['nbr_compte'];?> compte<?php If ($info_proprio['nbr_compte']>1) {echo "s";} ?> de propri&eacute;t&eacute;</label>
											</li>
											<li>
												<label class="label"><?php echo $info_proprio['nbr_proprio'];?> propri&eacute;taire<?php If ($info_proprio['nbr_proprio']>1) {echo "s";} ?></label>
											</li>
										</td>
									</tr>									
								</table>
							</td>
							<td style='vertical-align:top'>
								<table id='tbl_obj_animation' CELLSPACING = '5' CELLPADDING ='5' class='bilan-af'>
									<tr>
										<td class='bilan-af' style='vertical-align:top;text-align:left'>
											<label class="label" style='margin-left:-40px;'><u>Objectif de l'animation  :</u></label>
										</td>
									</tr>
									<tr>
										<td style='text-align:left'>
											<table CELLSPACING = '0' CELLPADDING ='0' class='bilan-af'>
												<?php For ($i=0; $i<$res_obj_animation->rowCount(); $i++) { ?>
												<tr>
													<td class='bilan-af' style='vertical-align:top; text-align:right'>
														<li class='label'style="text-align:right"><?php echo $obj_animation[$i]['lib']; ?> : </li>
													</td>
													<td class='bilan-af' style='padding-bottom:0px'>
														<li style='margin:0' class='label'>
															<?php echo $obj_animation[$i]['nbr_lot']; ?> lot<?php If ($obj_animation[$i]['nbr_lot']>1) {echo "s";} 
															echo " (".round(($obj_animation[$i]['nbr_lot']*100)/($info_parcelle['nbr_lot']), 0)."%)";?>
														</li>
													</td>
												</tr>
												<?php } ?>												
											</table>
										</td>
									</tr>												
								</table>
							</td>
						</tr>
						<tr style='vertical-align:top;text-align:right;'>
							<td colspan='2'>							
								<img style='border-radius:15px;box-shadow:2px 2px 3px 2px #6a6a6a;' src="charts/pie_bilan_af.php?session_af_id=<?php echo $session_af_id; ?>" alt='pie_bilan_af'/>							
							</td>
							<td style='vertical-align:top;text-align:right;'>
								<table id='tbl_bilan_animation' CELLSPACING = '5' CELLPADDING ='5' class='bilan-af' style='float:right;'>
									<tr>
									
										<td class='bilan-af' style='vertical-align:top;text-align:left'>
											<label class="label"><u>Bilan de l'animation  :</u></label>
										</td>
									
									
									</tr>
									<tr>
										<td style='text-align:left;padding-left:40px;'>
											<table CELLSPACING = '0' CELLPADDING ='0' class='bilan-af'>
												<?php For ($i=0; $i<$res_bilan_animation->rowCount(); $i++) { ?>
												<tr>
													<td class='bilan-af' style='vertical-align:top; text-align:right'>
														<li class='label'style="text-align:right"><?php echo $bilan_animation[$i]['lib']; ?> : </li>
													</td>
													<td class='bilan-af' style='padding-bottom:0px'>
														<li style='margin:0' class='label'>
															<?php echo $bilan_animation[$i]['nbr_lot']; ?> lot<?php If ($bilan_animation[$i]['nbr_lot']>1) {echo "s";} 
															echo " (".round(($bilan_animation[$i]['nbr_lot']*100)/($info_parcelle['nbr_lot']), 0)."%)";?>
														</li>
													</td>
												</tr>
												<?php } ?>
												<tr>
													<td class='bilan-af' style='vertical-align:top; text-align:right'>
														<li class='label'style="text-align:right;color:#ff0000;">A d&eacute;terminer : </li>
													</td>
													<td class='bilan-af' style='padding-bottom:0px'>
														<li style='margin:0;color:#ff0000;' class='label'>
															<?php echo $info_parcelle['nbr_lot'] - array_sum(array_column($bilan_animation, 'nbr_lot')); ?> lot<?php If ($info_parcelle['nbr_lot'] - array_sum(array_column($bilan_animation, 'nbr_lot'))>1) {echo "s";} 
															echo " (".round((($info_parcelle['nbr_lot'] - array_sum(array_column($bilan_animation, 'nbr_lot')))*100)/($info_parcelle['nbr_lot']), 0)."%)";?>
														</li>
													</td>
												</tr>
											</table>
										</td>
									</tr>												
								</table>
							</td>							
						</tr>
						<tr>
							<td colspan='3'>
								<table class='no-border' width='100%'>
									<tr>
									<?php
										$rq_evt_globaux = "
										SELECT DISTINCT
											evenements_af.evenement_af_id, 
											evenements_af.evenement_af_date, 
											d_types_evenements_af.type_evenements_af_lib
										FROM 
											animation_fonciere.evenements_af
											JOIN animation_fonciere.d_types_evenements_af USING (type_evenements_af_id)
											JOIN animation_fonciere.rel_af_evenements USING (evenement_af_id)
										WHERE 
											rel_af_evenements.session_af_id = ?
										ORDER BY evenement_af_date DESC";
										$res_evt_globaux = $bdd->prepare($rq_evt_globaux);
										$res_evt_globaux->execute(array($session_af_id));
										$lst_evt_globaux = $res_evt_globaux->fetchall();
									?>	
										<td align='left' width='1%'>
											<label class='label'>
												Ev&eacute;nements globaux :
											</label>
											<br>
											<input class="btn_frm" onclick="showFormEvenement(this, <?php echo $session_af_id; ?>, -1);" value="Saisir un événement" type="button">
										</td>	
										<td id='td_lst_evenements' class='bilan-af' align='left'>
										<?php
											If ($res_evt_globaux->rowCount() > 0) {
												For ($i=0; $i<$res_evt_globaux->rowCount(); $i++) { 	
										?>
												<li>
													<label class='label_simple' style='margin-left:10px;'>le <?php echo date_transform($lst_evt_globaux[$i]['evenement_af_date']); ?> : <?php echo $lst_evt_globaux[$i]['type_evenements_af_lib']; ?></label>
													<img onclick="showFormEvenement(this, <?php echo $session_af_id; ?>, <?php echo $lst_evt_globaux[$i]['evenement_af_id']; ?>);" src='<?php echo ROOT_PATH; ?>images/edit.png' style='cursor:pointer;padding-left:5px;'>
												</li>
										<?php
												}
											} Else {
										?>
												<li>
													<label class='label_simple' style='margin-left:10px;'>Aucun</label>
												</li>
										<?php
											}
										?>											
										</td>
									<?php
										
									
										$rq_lst_sessions = "
										SELECT DISTINCT
											session_af_id,
											session_af_lib													
										FROM 
											animation_fonciere.session_af
											JOIN animation_fonciere.rel_saf_foncier USING (session_af_id)
											JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)											
											JOIN foncier.cadastre_site t4 USING (cad_site_id)
										WHERE 
											session_af_id != ? AND
											site_id IN (".implode(', ', $lst_site).")";
										// echo $rq_lst_sessions;
										$res_lst_sessions = $bdd->prepare($rq_lst_sessions);
										$res_lst_sessions->execute(array($session_af_id));
										$lst_sessions = $res_lst_sessions->fetchall();
										If ($res_lst_sessions->rowCount() > 0) { 	
									?>
										<td align='right'>
											<label class='label'>
												Autres sessions d'animation sur le même secteur :
											</label>
										</td>	
										<td class='bilan-af' align='left' width='1%' style='vertical-align:middle;'>
											<?php
												For ($i=0; $i<$res_lst_sessions->rowCount(); $i++) { 	
													$vget = "mode:consult;session_af_id:".$lst_sessions[$i]['session_af_id'].";session_af_lib:".$lst_sessions[$i]['session_af_lib'].";";
													$vget_crypt = base64_encode($vget);
													$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_saf.php?d=$vget_crypt#general";
											?>
												<li>
													<a class='label_simple' style='margin-left:10px' href='<?php echo $lien; ?>'><?php echo $lst_sessions[$i]['session_af_lib']; ?></a>
												</li>
											<?php
												}
											?>
										</td>
									<?php
										}
									?>	
									</tr>
								</table>
							</td>							
						</tr>
					</table>
				</div id='general'>
				
				
				
				<div id='interloc'>	
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<center>
								<table width = '100%' class='no-border' id='tbl_interloc' align='center' CELLSPACING='0' CELLPADDING='5' border='0'>
									<tbody id='tbl_interlocs_entete'>							
										<tr>
											<td class='filtre' style='text-align:left;width:1px;border-bottom:4px solid #004494;'>
												<table width='100%' class='no-border' align='center' border='0'>
													<tr>
														<td class='filtre' style='text-align:left;' width='90px'>
															<input style='text-align:center;margin-left:40px;border:0' class='editbox filtre_section' oninput="create_lst_interlocs(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_interloc_section' size='8' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';">
															<input style='text-align:center;border:0' class='editbox filtre_par_num' oninput="create_lst_interlocs(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_interloc_par_num' size='8' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';">
															<img onclick="$('.filtre_section').val(''); $('.filtre_par_num').val(''); create_lst_interlocs(0, '<?php echo $session_af_id; ?>', 0);" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
														</td>	
													</tr>
												</table>	
											</td>
											<td id='td_filtre_lst_interlocs_nom' style='display:none;text-align:left;width:1px;border-bottom:4px solid #004494;'>
												<table width='100%' class='no-border' align='center' CELLSPACING='0' CELLPADDING='0' border='0'>
													<tr>													
														<td class='filtre' align='left'>
															<input class='editbox filtre_nom' style="border:0;" oninput="create_lst_interlocs(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_interloc_nom' size='40' placeholder='Interlocuteur' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';">
															<img onclick="$('.filtre_nom').val(''); create_lst_interlocs(0, '<?php echo $session_af_id; ?>', 0);" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
														</td>														
														<td style='text-align:right;'>															
															<input id='btn_regroup_interloc' type='button' class='btn_frm' onclick="showFormRegroupInterloc('charge', <?php echo $session_af_id?>)" value='Regrouper des propriétaires'>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</tbody>
									<tbody id='tbody_interlocs_corps'>
								
									</tbody>
								</table>
								</center>
							</td>
						</tr>
					</table>
				</div>
				
				<div id='parcelle'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<center>
								<table width = '100%' class='no-border' id='tbl_parc' align='center' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody id='tbody_parc_entete'>
										<tr>
											<td style='text-align:left;width:1px;border-bottom:4px solid #004494;'>
												<table class='no-border' width='1px'>
													<tr>
														<td style='text-align:left;'>
															<input class='editbox filtre_nom' style="border:0;" oninput="create_lst_parc(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_parcelle_nom' size='40' placeholder='Interlocuteur' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';">
															<img onclick="$('.filtre_nom').val(''); create_lst_parc(0, '<?php echo $session_af_id; ?>', 0);" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
														</td>
													</tr>
												</table>
											</td>
											<td style='text-align:left;border-bottom:4px solid #004494;'>
												<table class='no-border' width='100%'>
													<tr>
														<td class='filtre' style='text-align:left;' width='90px'>
															<input style='text-align:center;margin-left:40px;border:0' class='editbox filtre_section' oninput="create_lst_parc(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_parcelle_section' size='8' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';">
															<input style='text-align:center;border:0' class='editbox filtre_par_num' oninput="create_lst_parc(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_parcelle_par_num' size='8' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';">
															<img onclick="$('.filtre_section').val(''); $('.filtre_par_num').val(''); create_lst_parc(0, '<?php echo $session_af_id; ?>', 0);" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
														</td>
													<?php
														$rq_verif_saisie_occsol1 = "
														SELECT 
															rel_saf_foncier_id,
															sum(COALESCE(pourcentage, 0)) as sum_pourc
														FROM 
															animation_fonciere.rel_saf_foncier 
															LEFT JOIN animation_fonciere.rel_saf_foncier_occsol USING (rel_saf_foncier_id)
														WHERE session_af_id = :session_af_id
														GROUP BY rel_saf_foncier_id
														HAVING sum(COALESCE(pourcentage, 0)) < 100";
														$res_verif_saisie_occsol1 = $bdd->prepare($rq_verif_saisie_occsol1);
														$res_verif_saisie_occsol1->execute(array('session_af_id'=>$session_af_id));
														
														$rq_verif_saisie_occsol2 = "
														SELECT 
															rel_saf_foncier_id,
															count(rel_saf_foncier_id)
														FROM 
															animation_fonciere.rel_saf_foncier 
															JOIN animation_fonciere.rel_saf_foncier_occsol USING (rel_saf_foncier_id)
														WHERE session_af_id = :session_af_id AND montant IS NULL
														GROUP BY rel_saf_foncier_id";
														$res_verif_saisie_occsol2 = $bdd->prepare($rq_verif_saisie_occsol2);
														$res_verif_saisie_occsol2->execute(array('session_af_id'=>$session_af_id));
														
														If ($res_verif_saisie_occsol1->rowCount() == 0 && $res_verif_saisie_occsol2->rowCount() > 0) {
															$display_occsol = '';
														} Else {
															$display_occsol = 'none';
														}
													?>
														<td style='text-align:center;width:100%;'>															
															<input id='btn_prix_occsol' type='button' class='btn_frm' style='display:<?php echo $display_occsol; ?>;' onclick="showFormPrixOccsol(<?php echo $session_af_id?>)" value='Saisir des prix au m² par occupation du sol'>
														</td>
														<td style='text-align:right;width:100%;'>
															<img id='btn_onglet_parc_ALL' src='<?php echo ROOT_PATH; ?>images/moins.png' width='15px' style='cursor:pointer;' onclick="showDetail('parc', 'onglet_parc_ALL');">	
															<label id='lbl_onglet_parc_ALL' class='label'>
																Réduire tout
															</label>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</tbody>
									<tbody id='tbody_parc_corps'>
										
									</tbody>
									
								</table>
								</center>
							</td>
						</tr>
					</table>
				</div>
								
				<div id='echanges'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<center>
								<table style='border-collapse:collapse;' width = '100%' class='no-border' id='tbl_echanges' align='center' CELLSPACING='5' CELLPADDING='5' border='0'>
									<tbody id='tbl_echanges_entete'>
										<tr>
											<td class='filtre' style='text-align:left;width:1px;border-bottom:4px solid #004494;' colspan='2'>
												<input style='text-align:center;border:0' class='editbox filtre_section' oninput="create_lst_echanges(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_echange_section' size='8' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';">
												<input style='text-align:center;border:0' class='editbox filtre_par_num' oninput="create_lst_echanges(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_echange_par_num' size='8' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';">
												<input class='editbox filtre_nom' style="border:0;margin-left:40px;" oninput="create_lst_echanges(0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_echange_nom' size='40' placeholder='Interlocuteur' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';">
												<img onclick="$('.filtre_section').val(''); $('.filtre_par_num').val('');$('.filtre_nom').val('');create_lst_echanges(0, '<?php echo $session_af_id; ?>', 0);" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
											</td>
										</tr>
									</tbody>
									<tbody id='tbody_echanges_corps'>
								
									</tbody>
								</table>
								</center>
							</td>
						</tr>
					</table>
				</div>
				
				<div id='synthese'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<center>
								<table width = '100%' class='no-border' id='tbl_synthese' align='center' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody id='tbody_synthese_evt_globaux'>
										
									</tbody>
									<tbody id='tbody_synthese_bilan_af'>
								
									</tbody>
									<tbody id='tbody_synthese_parc'>
										<tr>
											<td>
												<table class='no-border' style='border-collapse:collapse;' width='100%'>											
													<tbody id='tbody_synthese_parc_entete'>														
														<tr>															
															<td class='filtre' style='text-align:left;width:1px;border-bottom:4px solid #004494;' width='90px' colspan='1'>
																<input style='text-align:center;margin-left:40px;border:0' class='editbox filtre_section' oninput="create_synthese('modif', 0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_synthese_section' size='8' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';">
																<input style='text-align:center;border:0' class='editbox filtre_par_num' oninput="create_synthese('modif', 0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_synthese_par_num' size='8' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';">
																<img onclick="$('.filtre_par_num').val(''); $('.filtre_section').val(''); create_synthese('modif', 0, '<?php echo $session_af_id; ?>', 0);" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
															</td>
															<td style='text-align:left;width:1px;border-bottom:4px solid #004494;'>
																&nbsp;
															</td>
															<td class='filtre' style='text-align:left;width:1px;border-bottom:4px solid #004494;' colspan='2'>
																<input class='editbox filtre_nom' style="border:0;" oninput="create_synthese('modif', 0, '<?php echo $session_af_id; ?>', 0);" type='text' id='filtre_synthese_nom' size='40' placeholder='Interlocuteur' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Interlocuteur';">
																<img onclick="$('.filtre_nom').val(''); create_synthese('modif', 0, '<?php echo $session_af_id; ?>', 0);" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
															</td>
														</tr>
													</tbody>
													<tbody id='tbody_synthese_parc_corps'>
													
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>	
								</table>
								</center>
							</td>
						</tr>
					</table>
					<div id="lgd_synthese" style="display:none;background-color:#ccc !important; border-color:#6a6a6a; border-radius:10px; border-style:solid; border-width:1px !important; box-shadow: 2px 1px 3px 0 #6a6a6a;white-space: nowrap;position:fixed;right:45px;bottom:120px">
						<div style="text-align:right;margin: 5px">
							<img width="20px" style='cursor:pointer;' src="<?php echo ROOT_PATH; ?>foncier/module_af/images/legend-add.png" onclick="affLgd();">
						</div>
						<div id='ul_lgd_synthese' style='display:none;width:30px;height:35px;'>
							<label class='label'>Perspective</label>
							<ul style='list-style-type:none;margin:0;'>
								<li style='background:#747474;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;'>
									<label class='label_simple' style='color:#ffffff;'>Pas de perspective</label>
								</li>
								<li style='background:rgba(0, 0, 0, 0) repeating-linear-gradient(45deg, #70ad47, #70ad47 5px, #4472c4 5px, #4472c4 10px) repeat scroll 0 0;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;'>
									<label class='label_simple' style='color:#ffffff;'>Perspective d'acte</label>
								</li>
								<li style='background:#70ad47;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;'>
									<label class='label_simple' style='color:#ffffff;'>Perspective de convention</label>
								</li>
								<li style='background:#4472c4;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;'>
									<label class='label_simple' style='color:#ffffff;'>Perspective d'acquisition</label>
								</li>
								<li style='background:#fe0000;border-radius:8px;margin-left:-20px;margin-right:10px;padding:5px;'>
									<label class='label_simple' style='color:#ffffff;'>Refus</label>
								</li>
							</ul>
							<br>
							<label class='label'>Priorit&eacute;</label>
							<ul style='list-style-type:none;margin:0;'>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#fe0000;'>Forte</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#ffa025;'>Moyenne</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#00b050;'>Faible</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#000000;'>Pas de priorit&eacute;</label>
								</li>
								<li style='margin-left:-20px;margin-right:10px;'>
									<label class='label' style='color:#747474;'>Lot bloqu&eacute;</label>
								</li>								
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<div id="explorateur_interloc_masque" class="explorateur_span" style="display:none;top:10px"></div>
			
			<div id="explorateur_evenement" class="explorateur_span" style='display:none;'>
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_evenement_sousdiv" class='no-border' style='text-align:center;position:relative;'>
									<form class='form' onsubmit="return valide_form('frm_evenement')" name='frm_evenement' id='frm_evenement' method='POST' style='display:inline-block'>
										<table style='width:890px;' class="explorateur_contenu" cellspacing="0" cellpadding="2" border="0" align="center" frame="box" rules="NONE">
											<tbody id='tbody_evenement_entete'>
												<tr>
													<td colspan='2'>
														<table class='no-border' width='100%'>
															<tr>
																<td style="text-align:center;margin-left:20px">
																	<label id='evenement_lib' class='label' style="text-align:center;margin-left:20px">Nouvel événement</label>
																</td>
																<td style="text-align:right;width:25px;">	
																	<img width="15px" onclick="cacheMess('explorateur_evenement');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
																</td>																	
															</tr>
														</table>
													</td>
											</tbody>
											<tbody id='tbody_evenement_contenu'>
												<tr>
													<td style='text-align:right'>
														<label class='label'><u>Date de l'&eacute;v&eacute;nement :</u></label>
													</td>
													<td style='text-align:left'>
														<input type='text' size='10' class='datepicker' id='date_evenement' name='date_evenement'>
													</td>										
												</tr>
												<tr>
													<td style='text-align:right'>
														<label class="label"><u>Type d'&eacute;v&eacute;nement :</u></label>
													</td>
													<td style='text-align:left'>															
														<label class="btn_combobox">			
															<select name="lst_type_evenement" id="lst_type_evenement" class="combobox" style='text-align:left'>
																<option value='-1'>- - - Type - - -</option>
																<?php
																$rq_lst_type = "SELECT type_evenements_af_id as id, type_evenements_af_lib as lib FROM animation_fonciere.d_types_evenements_af";
																$res_lst_type = $bdd->prepare($rq_lst_type);
																$res_lst_type->execute();																
																While ($donnees = $res_lst_type->fetch()) {
																	echo "<option value='$donnees[id]'>$donnees[lib]</option>";
																}
																?>				
															</select>
														</label>													
													</td>									
												</tr>
												<tr>
													<td style='text-align:right'>
														<label class="label"><u>Commentaire :</u></label>
													</td>
													<td style='text-align:left'>	
														<textarea onkeyup="resizeTextarea('description_evenement');" onchange="resizeTextarea('description_evenement');" id='description_evenement' name='description_evenement' class="label textarea_com_edit" rows='1' cols='60'></textarea>
													</td>
												</tr>		
											</tbody>
											<tbody id='tbody_evenement_presents'>	
												<tr>
													<td colspan='2' style='border-top:2px dashed #004494;padding-top:20px;text-align:center;'>
														<label class='label'>Personnes présentes</label>
													</td>
												</tr>
												<tr>
													<td colspan='2'>
														<fieldset id='fld_evenement_presents' class='fieldset_af'>
															<table width = '100%' CELLSPACING = '0' CELLPADDING ='5' style='border:0px solid black' align='center'>
																<tbody id='tbody_tbl_evenement_tous-aucun'>
																	<tr>
																		<td>
																			<input id="event_present_ALL" checked="" type="checkbox" onclick="$('.event_present').prop('checked', $(this).prop('checked'));">
																		</td>
																		<td style='text-align:left;'>
																			<label class="label">Tous/ Aucun</label>
																		</td>
																	</tr>
																</tbody>
																<tbody id='tbody_tbl_evenement_presents'>
															
																</tbody>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>											
											<tbody id='tbody_evenement_pied'>
												<tr>
													<td>
														<div class='lib_alerte' id='evenement_prob_saisie' style='width:580px;display:none;'>Veuillez compl&eacuteter votre saisie</div>
													</td>
													<td class='save'>
														<input type='button' onclick="" name='btn_valide_evenement' id='btn_valide_evenement' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="confirmDeleteEAF" class="explorateur_span" style='display:none;'>
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="confirmDeleteEAF_sousdiv" class='no-border' style='text-align:center;'>
									<table class="explorateur_contenu" cellspacing="0" cellpadding="5" border="0" align="center" frame="box" rules="NONE">
										<tbody>
											<tr>
												<td colspan='2'>
													<label id='msg_confirmDeleteEAF' class='label'></label>
												</td>
											</tr>
											<tr>
												<td style='padding-right:20px;text-align:right;'>
													<input id='btn_confirmDeleteEAF_oui' type='button' class='btn_frm' style='width:60px' onclick="" value='Oui'>
												</td>
												<td style='padding-left:20px;text-align:left;'>
													<input id='btn_confirmDeleteEAF_non' type='button' class='btn_frm' style='width:60px' onclick="cacheMess('confirmDeleteEAF');changeOpac(100, 'onglet');" value='Non'>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="confirmDeleteInterloc" class="explorateur_span" style='display:none;'>
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="confirmDeleteInterloc_sousdiv" class='no-border' style='text-align:center;'>
									<table class="explorateur_contenu" cellspacing="0" cellpadding="5" border="0" align="center" frame="box" rules="NONE">
										<tbody>
											<tr>
												<td colspan='2'>
													<label class='label'>Veuillez confirmer la suppression d'un interlocuteur</label>
												</td>
											</tr>
											<tr>
												<td style='padding-right:20px;text-align:right;'>
													<input id='btn_confirmDeleteInterloc_oui' type='button' class='btn_frm' style='width:60px' onclick="" value='Oui'>
												</td>
												<td style='padding-left:20px;text-align:left;'>
													<input id='btn_confirmDeleteInterloc_non' type='button' class='btn_frm' style='width:60px' onclick="cacheMess('confirmDeleteInterloc');changeOpac(100, 'onglet');" value='Non'>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_interloc" class="explorateur_span" style="display: none;top:220px">
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_new_interloc" style='text-align:center;'>
									<form class='form' onsubmit="return valide_form('frm_new_interloc')" name='frm_new_interloc' id='frm_new_interloc' method='POST' style='display:inline-block'>
										<input type='hidden' id='need_session_af_id' name='need_session_af_id'>
										<input type='hidden' id='need_entite_af_id' name='need_entite_af_id'>
										<input type='hidden' id='hid_parc_interloc' name='hid_parc_interloc'>
										<table class="explorateur_contenu" cellspacing="0" cellpadding="5" border="0" align="center" frame="box" rules="NONE">
											<tbody>
												<tr>
													<td style="text-align:center;">
														<label class='label' style='margin-left:150px;'>
															Ajouter un interlocuteur
														</label>
													</td>
													<td style="text-align:right;">
														<img width="15px" onclick="cacheMess('explorateur_interloc');changeOpac(100, 'onglet');$('#onglet').css('height', 'auto');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>		
													<td colspan='2'>
														<fieldset id='fld_new_interloc_eaf' class='fieldset_af'>
															<legend>Type d'interlocuteur pour l'EAF</legend>
															<table width = '100%' CELLSPACING = '2' CELLPADDING ='5' style='border:0px solid black' align='center'>
																<tr>												
																	<td id="td_new_type_interloc" class="normal" style='text-align:left;white-space:nowrap;' colspan='2'>
																		<label class='label'>
																			En tant que :
																		</label>	
																		<label class="btn_combobox"> 
																			<select name="new_type_interloc" id="new_type_interloc" class="combobox" onchange="adaptListParc(this, 'new_');">
																				<option value='-1'>- - - Type d'interlocuteur - - -</option>
																				<?php
																				$rq_lst_type_interloc = "
																					SELECT DISTINCT 
																						type_interloc_id, 
																						type_interloc_lib, 
																						all_eaf 
																					FROM 
																						animation_fonciere.d_type_interloc
																						JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
																					ORDER BY type_interloc_id";
																				$res_lst_type_interloc = $bdd->prepare($rq_lst_type_interloc);
																				$res_lst_type_interloc->execute();
																				While ($donnees = $res_lst_type_interloc->fetch())  {
																					If ($donnees['all_eaf'] == 't') {
																						$all_eaf = 'all_eaf';
																					} Else {
																						$all_eaf = 'not_all_eaf';
																					}
																					echo "<option class='".$all_eaf."' value='$donnees[type_interloc_id]'>$donnees[type_interloc_lib]</option>";
																				}
																				?>
																			</select>
																		</label>
																	</td>
																</tr>
															</table>
														</fieldset>
													</td>
												</tr>
											</tbody>
											<tbody>	
												<tr>
													<td style='text-align:left;white-space:nowrap;' colspan='2'>
														<fieldset id='fld_import_proprio' class='fieldset_af'>
															<legend>S&eacute;lectionnez un interlocuteur</legend>
															<table width = '100%' CELLSPACING = '0' CELLPADDING ='5' style='border:0px solid black' align='center'>
																<tr>
																	<td style='text-align:center;'>
																		<input type='text' size='32' class='editbox' name='flt_import_nom_usage' id='flt_import_nom_usage' placeholder='Filtrer sur le nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Filtrer sur le nom';">
																	
																		<input type='text' size='32' class='editbox' name='flt_import_prenom_usage' id='flt_import_prenom_usage' placeholder='Filtrer sur le pr&eacute;nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Filtrer sur le pr&eacute;nom';">
																	
																		<img id='img_flt_lst_proprio_import' name='img_flt_lst_proprio_import' src="<?php echo ROOT_PATH; ?>images/filtre.png" style='cursor:pointer;width:20px' onclick="flt_lst_proprio_import('null');">
																	</td>
																</tr>	
																<tr>
																	<td id="td_import_proprio_lst_import_proprio" class="normal" colspan='3' style='text-align:center;'>
																		<label class="btn_combobox"> 
																			<select name="lst_import_proprio" id="lst_import_proprio" class="combobox" style='width:550px;' onchange="disableNewInterloc(this);">
																			</select>
																		</label>
																		<br>
																		<label class='last_maj'>
																			Utilisez les filtres pour une liste plus complète
																		</label>
																	</td>
																</tr>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td style='text-align:center;' colspan='2'>
														<label class='label'>
															OU
														</label>
													</td>
												</tr>
											</tbody>
											<tbody>	
												<tr>
													<td colspan='2'>
														<fieldset id='fld_new_interloc' class='fieldset_af'>
															<legend>Ajouter un nouvel interlocuteur via le formulaire d&eacute;di&eacute;</legend>
															<table width = '100%' CELLSPACING = '0' CELLPADDING ='0' style='border:0px solid black' align='center'>
																<tr>
																	<td colspan='2'>
																		<label class='last_maj'>Personne physique</label>
																	</td>
																</tr>
																<tr>
																	<td colspan='2'>
																		<table align='center' CELLSPACING='0' CELLPADDING='0' style="border:0px;float:none;">
																			<tr>
																				<td id="td_new_interloc_lst_particule" class="normal" style='text-align:center;white-space:nowrap;'>
																					<label class="btn_combobox">
																						<select name="new_interloc_lst_particule" id="new_interloc_lst_particule" class="combobox">
																							<option value='-1'>---Particule---</option>
																							<?php
																							$rq_lst_particules = "SELECT DISTINCT ccoqua, particule_lib FROM cadastre.d_ccoqua ORDER BY ccoqua";
																							$res_lst_particules = $bdd->prepare($rq_lst_particules);
																							$res_lst_particules->execute();
																							While ($donnees = $res_lst_particules->fetch())  {
																								echo "<option value='$donnees[ccoqua]'>$donnees[particule_lib]</option>";
																							}
																							?>
																						</select>
																					</label>
																					<input type='text' size='32' class='editbox' name='new_interloc_nom_usage' id='new_interloc_nom_usage' oninput="disableNewInterloc(this);" placeholder='Nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom';">
																					<input type='text' size='32' class='editbox' name='new_interloc_prenom_usage' id='new_interloc_prenom_usage' placeholder='Pr&eacute;nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Pr&eacute;nom';">
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='2' style='padding-top:10px;'>
																		<label class='last_maj'>Personne morale</label>
																	</td>
																</tr>
																<tr>
																	<td colspan='2'>
																		<table align='center' CELLSPACING='0' CELLPADDING='0' style="border:0px;float:none;">
																			<tr>
																				<td id="td_new_interloc_lst_particule" class="normal" style='text-align:center;white-space:nowrap;'>
																					<input type='text' size='64' class='editbox' name='new_interloc_ddenom' id='new_interloc_ddenom' oninput="disableNewInterloc(this);" placeholder='Nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom';">				
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody id='tbody_new_parc_interloc'>	
												<tr>
													<td style='text-align:left;white-space:nowrap;' colspan='2'>
														<fieldset id='fld_new_parc_interloc' class='fieldset_af'>
															<legend>Quelles sont les parcelles concern&eacute;es ?</legend>
															<table width = '100%' CELLSPACING = '0' CELLPADDING ='0' style='border:0px solid black' align='center'>
																<tbody>
																	<tr>
																		<td>
																			<input id='new_check_all_parc' type='checkbox' checked onclick="$('.chk_parc').prop('checked', $('#new_check_all_parc').prop('checked'));checkEchange('new');">
																			<label class='last_maj'>
																				Tous/ Aucun
																			</label>
																		</td>
																		<td class='only_proprio' style='text-align:center;'>
																			<label class='last_maj'>
																				D&eacute;membrement/ Indivision
																			</label>
																		</td>
																		<td class='only_proprio' style='text-align:center;'>
																			<label class='last_maj'>
																				Droit r&eacute;el ou particulier
																			</label>
																		</td>
																	</tr>
																</tbody>
																<tbody id='tbody_check_parc_interloc'>
																
																</tbody>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td>
														<div class='lib_alerte' id='new_interloc_prob_saisie' style='width:580px;display:none;'>Veuillez compl&eacuteter votre saisie</div>
													</td>
													<td class='save'>
														<input type='button' onclick="addInterloc();" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_echange" class="explorateur_span" style='display:none;'>
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_echange_sousdiv" class='no-border' style='text-align:center;'>
									<form class='form' onsubmit="return valide_form('frm_echange')" name='frm_echange' id='frm_echange' method='POST' style='display:inline-block'>
										<table style='width:890px;' class="explorateur_contenu" cellspacing="0" cellpadding="2" border="0" align="center" frame="box" rules="NONE">
											<tbody id='tbody_echange_entete'>
												<tr>
													<td colspan='2'>
														<table class='no-border' width='100%'>
															<tr>		
																<td style="text-align:left;width:25px;">	
																	<img id='img_reduce_explorateur_echange' width="20px" onclick="" src='<?php echo ROOT_PATH; ?>images/reduire.png' style="cursor:pointer">
																	<input type='hidden' id='hid_echange_ddenom' value=''>
																</td>
																<td style="text-align:center;margin-left:20px">
																	<label id='echange_interloc_lib' class='label' style="text-align:center;margin-left:20px"></label>
																</td>
																<td style="text-align:right;width:25px;">	
																	<img id='btn_ech_close' width="15px" onclick="cacheMess('explorateur_echange');changeOpac(100, 'onglet');$('#hid_echange_ddenom').val(null);$('#tbody_modif_infos_interloc').css('display', 'none' );$('#tbody_maitrise_parc').css('display', 'none' );" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
																</td>																	
															</tr>
														</table>
													</td>
											</tbody>
											<tbody id='tbody_echange_contenu'>
												<tr>
													<td style='text-align:right;'>
														<label class='label'><u>Date de l'&eacute;change :</u></label>
													</td>
													<td style='text-align:left;'>
														<input type='text' size='10' class='datepicker' id='date_echange' name='date_echange'>
													</td>										
												</tr>
												<tr>
													<td style='text-align:right;'>
														<label class="label"><u>Type d'&eacute;change :</u></label>
													</td>
													<td style='text-align:left;'>															
														<label class="btn_combobox">			
															<select name="lst_type_echange" id="lst_type_echange" class="combobox" style='text-align:left;'>
																<option value='-1'>- - - Type - - -</option>
																<?php
																$rq_lst_type = "SELECT type_echanges_af_id as id, type_echanges_af_lib as lib FROM animation_fonciere.d_types_echanges_af";
																$res_lst_type = $bdd->prepare($rq_lst_type);
																$res_lst_type->execute();																
																While ($donnees = $res_lst_type->fetch()) {
																	echo "<option value='$donnees[id]'>$donnees[lib]</option>";
																}
																?>				
															</select>
														</label>													
													</td>									
												</tr>
												<tr>
													<td style='text-align:right;'>
														<label class="label"><u>Description :</u></label>
													</td>
													<td style='text-align:left;'>	
														<textarea onkeyup="resizeTextarea('description_echange');" onchange="resizeTextarea('description_echange');" id='description_echange' name='description_echange' class="label textarea_com_edit" rows='1' cols='60'></textarea>
													</td>
												</tr>												
												<tr id='tr_chk_echange_referent' style='display:block;'>
													<td colspan='2'>
														<input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='chk_echange_referent' name='chk_echange_referent' onclick='updateTblMaitriseParc()'>
														<label class='label'><u>Cet &eacute;change concerne tous les interlocuteurs r&eacute;f&eacute;renc&eacute;s</u></label>
													</td>
												</tr>
												<tr>
													<td colspan='2'style="padding:20px 0 0 100px">
														<table class='no-border'>
															<tr>
																<td style='text-align:right;padding-top:15px'>
																	<label class='label'><u>A recontacter le :</u></label>
																</td>
																<td style='text-align:left;'>
																	<input type='text' size='10' style='border-color:#a2c282;' class='datepicker' id='date_echange_recontact' name='date_echange_recontact'>
																</td>	
																<td style='text-align:right;'>
																	<label class="label"><u>Motif :</u></label>
																</td>
																<td style='text-align:left;'>	
																	<textarea onkeyup="resizeTextarea('motif_echange_recontact');" onchange="resizeTextarea('motif_echange_recontact');" id='motif_echange_recontact' name='motif_echange_recontact' class="label textarea_com_edit" rows='1' cols='30'>
																	</textarea>
																</td>
																<td id='td_echange_rappel_traite' style='text-align:right;'>
																	<label id='echange_lbl_rappel_traite' class="label"><u>Traité :</u></label><input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='echange_chk_rappel_traite' name='echange_chk_rappel_traite' onclick=''>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</tbody>
											<tbody id='tbody_echange_resultat'>	
												<tr>
													<td colspan='2' style='border-top:2px dashed #004494;padding-top:20px;text-align:center;'>
														<label class='label'>R&eacute;sultat de l'&eacute;change</label>
													</td>
												</tr>
												<tr>
													<td colspan='2'>
														<table>
															<tr>
																<td style='text-align:left;'>
																	<input type='checkbox' id='chk_attente_reponse'  onclick="if ( $('#chk_attente_reponse').is(':checked')) {$('#chk_interloc_injoignable').prop('readonly', true );$('#chk_interloc_injoignable').prop('disabled', true);$('#lbl_interloc_injoignable').css('color', '#6a6a6a');$('#chk_modif_infos_interloc').prop('readonly', true );$('#chk_modif_infos_interloc').prop('disabled', true );$('#lbl_modif_infos_interloc').css('color', '#6a6a6a');$('#chk_maitrise_parc').prop('readonly', true );$('#chk_maitrise_parc').prop('disabled', true );$('#lbl_maitrise_parc').css('color', '#6a6a6a');} else {$('#chk_interloc_injoignable').prop('readonly', false );$('#chk_interloc_injoignable').prop('disabled', false);$('#lbl_interloc_injoignable').css('color', '#004494');$('#chk_modif_infos_interloc').prop('readonly', false );$('#chk_modif_infos_interloc').prop('disabled', false);$('#lbl_modif_infos_interloc').css('color', '#004494');$('#chk_maitrise_parc').prop('disabled', false);$('#chk_maitrise_parc').prop('readonly', false );$('#lbl_maitrise_parc').css('color', '#004494');}"><label id='lbl_attente_reponse' class='label'>En attente d'une réponse</label>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td colspan='2'>
														<table>
															<tr>
																<td style='text-align:left;'>
																	<input type='checkbox' id='chk_interloc_injoignable' onclick="if ( $('#chk_interloc_injoignable').is(':checked')) {$('#chk_attente_reponse').prop('readonly', true );$('#chk_attente_reponse').prop('disabled', true);$('#lbl_attente_reponse').css('color', '#6a6a6a');$('#chk_modif_infos_interloc').prop('readonly', true );$('#chk_modif_infos_interloc').prop('disabled', true );$('#lbl_modif_infos_interloc').css('color', '#6a6a6a');$('#chk_maitrise_parc').prop('readonly', true );$('#chk_maitrise_parc').prop('disabled', true );$('#lbl_maitrise_parc').css('color', '#6a6a6a');} else {$('#chk_attente_reponse').prop('readonly', false );$('#chk_attente_reponse').prop('disabled', false);$('#lbl_attente_reponse').css('color', '#004494');$('#chk_modif_infos_interloc').prop('readonly', false );$('#chk_modif_infos_interloc').prop('disabled', false);$('#lbl_modif_infos_interloc').css('color', '#004494');$('#chk_maitrise_parc').prop('disabled', false);$('#chk_maitrise_parc').prop('readonly', false );$('#lbl_maitrise_parc').css('color', '#004494');}"><label id='lbl_interloc_injoignable' class='label'>Interlocuteur injoignable</label>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td colspan='2'>
														<table>
															<tr>
																<td style='text-align:left;'>
																	<input type='checkbox' id='chk_modif_infos_interloc' onclick=""><label id='lbl_modif_infos_interloc' class='label'>Modification des informations personnelles</label>
																</td>
															</tr>
															<tbody id='tbody_modif_infos_interloc' style='display:none;'>	
																<tr>
																	<td>
																		<fieldset id='fld_modif_infos_interloc' class='fieldset_af'>			
																			<table width = '100%' CELLSPACING = '2' CELLPADDING ='5' style='border:0px solid black' align='center'>
																				<tr id='tr_ech_gtoper' style='display:none;'>
																					<td colspan='2'>
																						<input type='radio' name='rad_gtoper' id='rad_gtoper1' value='1' onclick='gtoperUpdate();'><label class='last_maj'>Personne physique</label>
																						<input type='radio' name='rad_gtoper' id='rad_gtoper2' value='2' onclick='gtoperUpdate();'><label class='last_maj'>Personne moral</label>
																					</td>
																				</tr>
																				<tr id='tr_ech_gtoper-1' style='display:block;'>
																					<td colspan='2'>
																						<table align='center' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px;float:none;">
																							<tr>
																								<td id="td_ech_update_interloc_lst_particule" class="normal" style='text-align:center;white-space:nowrap;'>
																									<label class="btn_combobox">
																										<select name="ech_update_interloc_lst_particule" id="ech_update_interloc_lst_particule" class="combobox">
																											<option value='-1'>---Particule---</option>
																											<?php
																											$rq_lst_particules = "SELECT DISTINCT ccoqua, particule_lib FROM cadastre.d_ccoqua ORDER BY ccoqua";
																											$res_lst_particules = $bdd->prepare($rq_lst_particules);
																											$res_lst_particules->execute();
																											While ($donnees = $res_lst_particules->fetch())  {
																												echo "<option value='$donnees[ccoqua]'>$donnees[particule_lib]</option>";
																											}
																											?>
																										</select>
																									</label>
																									<input type='text' size='32' class='editbox' name='ech_update_interloc_nom_usage' id='ech_update_interloc_nom_usage' placeholder='Nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom';">
																									<input type='text' size='32' class='editbox' name='ech_update_interloc_prenom_usage' id='ech_update_interloc_prenom_usage'placeholder='Pr&eacute;nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Pr&eacute;nom';">
																								</td>
																							</tr>
																							<tr>
																								<td style='text-align:left;'>
																									<label class='label'>
																										N&eacute;(e) le : <input type='text' value="" size='10' placeholder='jj/mm/aaaa' onfocus="this.placeholder = '';" onblur="this.placeholder = 'jj/mm/aaaa';" class='editbox' name='ech_update_interloc_date_naissance' id='ech_update_interloc_date_naissance'>
																									</label>
																									<label class='label'>
																										&agrave; : <input type='text' value="" size='40' class='editbox' name='ech_update_interloc_lieu_naissance' id='ech_update_interloc_lieu_naissance'>
																									</label>
																								</td>	
																							</tr>
																							<tr style='margin-top:15px'>
																								<td colspan='2'>
																									<label class='label'>
																										Epoux (se) de <input type='text' size='32' placeholder='Nom du conjoint' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du conjoint';" class='editbox' name='ech_update_interloc_nom_conjoint' id='ech_update_interloc_nom_conjoint'>
																										<input type='text' size='32' placeholder='Pr&eacute;nom du conjoint' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Pr&eacute;nom du conjoint';" class='editbox' name='ech_update_interloc_prenom_conjoint' id='ech_update_interloc_prenom_conjoint'>
																									</label>
																								</td>
																							</tr>
																							<tr>
																								<td>
																									<label class='label'>
																										Nom de jeune fille : <input type='text' value="" size='32' placeholder='Nom de jeune fille' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom de jeune fille';" class='editbox' name='ech_update_interloc_nom_jeunefille' id='ech_update_interloc_nom_jeunefille'>
																									</label>
																								</td>
																							</tr>		
																						</table>
																					</td>
																				</tr>
																				<tr id='tr_ech_gtoper-2' style='display:none;'>
																					<td colspan='2'>
																						<table align='center' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px;float:none;">
																							<tr>
																								<td id="td_update_interloc_lst_particule" class="normal" style='text-align:center;white-space:nowrap;'>
																									<input type='text' size='90' class='editbox' name='ech_update_interloc_nom_usage' id='ech_update_interloc_ddenom'>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<table align='center' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
																							<tr>
																								<td colspan='2'>
																									<table align='center' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
																										<tr>
																											<td rowspan='4' style="vertical-align:top;">
																												<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
																											</td>
																											<td>
																												<input type='text' size='60' class='editbox' id='ech_update_interloc_adresse1' name='ech_update_interloc_adresse1' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / B&acirc;timent">
																											</td>
																										</tr>
																										<tr>
																											<td style='text-align:right;'>
																												<input type='text' size='60' class='editbox' id='ech_update_interloc_adresse2' name='ech_update_interloc_adresse2' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N&deg;, Rue, Boite postale">
																											</td>	
																										</tr>
																										<tr>
																											<td style='text-align:right;'>
																												<input type='text' size='60' class='editbox' id='ech_update_interloc_adresse3' name='ech_update_interloc_adresse3' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Compl&eacute;ment">
																											</td>	
																										</tr>
																										<tr>
																											<td style='text-align:right;'>
																												<input type='text' size='60' class='editbox' id='ech_update_interloc_adresse4' name='ech_update_interloc_adresse4' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal & Ville">
																											</td>	
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<td style='vertical-align:bottom;'>
																									<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
																								</td>
																								<td style='vertical-align:middle;padding-top:20px;'>
																									<input type='text' size='60' class='editbox' name='ech_update_interloc_mail' id='ech_update_interloc_mail'>
																								</td>
																							</tr>
																						</table>
																					</td>
																					<td>
																						<table align='center' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-right:50px">
																							<tr>
																								<td style='text-align:right;'>
																									<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
																								</td>
																								<td style='white-space:nowrap;text-align:left;'>
																									<input type='text' size='15' class='editbox' name='ech_update_interloc_tel_fix1' id='ech_update_interloc_tel_fix1'><label class='label' style='font-weight:normal;font-size:8pt;'>(perso)</label>
																								</td>	
																							</tr>
																							<tr>
																								<td style='text-align:right;'>
																									<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
																								</td>
																								<td style='white-space:nowrap;text-align:left;'>
																									<input type='text' size='15' class='editbox' name='ech_update_interloc_tel_fix2' id='ech_update_interloc_tel_fix2'><label class='label' style='font-weight:normal;font-size:8pt;'>(pro)</label>
																								</td>	
																							</tr>
																							<tr>
																								<td style='text-align:right;'>
																									<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
																								</td>
																								<td style='white-space:nowrap;text-align:left;'>
																									<input type='text' size='15' class='editbox' name='ech_update_interloc_tel_portable1' id='ech_update_interloc_tel_portable1'><label class='label' style='font-weight:normal;font-size:8pt;'>(perso)</label>
																								</td>	
																							</tr>
																							<tr>
																								<td style='text-align:right;'>
																									<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
																								</td>
																								<td style='white-space:nowrap;text-align:left;'>
																									<input type='text' size='15' class='editbox' name='ech_update_interloc_tel_portable2' id='ech_update_interloc_tel_portable2'><label class='label' style='font-weight:normal;font-size:8pt;'>(pro)</label>
																								</td>	
																							</tr>
																							<tr>
																								<td style='text-align:right;'>
																									<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
																								</td>
																								<td style='white-space:nowrap;text-align:left;'>
																									<input type='text' size='15' class='editbox' name='ech_update_interloc_tel_fax' id='ech_update_interloc_tel_fax'>
																								</td>	
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</fieldset>		
																	</td>
																</tr>
															</tbody>
														</table>
													</td>													
												</tr>
												<tr>
													<td colspan='2'>
														<table style='width:100%;'>
															<tr>
																<td style='text-align:left;'>
																	<input id='chk_maitrise_parc' type='checkbox' onclick='showTblMaitriseParc()'><label id='lbl_maitrise_parc' class='label'>Négociation sur la maîtrise des parcelles</label>
																</td>
															</tr>
															<tbody id='tbody_maitrise_parc' style='display:none;'>
																<tr>
																	<td style='text-align:right;padding-right:15px;'>
																		<label class='label_simple'><i>Prix d'acquisition de l'EAF en cours de négociation : </i></label>
																		<input id='ech_prix_eaf_nego' type='text' size='8' class='editbox' style='text-align:right;'>
																		<label class='label'> €</label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<fieldset id='fld_maitrise_parc' class='fieldset_af'>
																			<table width = '100%' CELLSPACING = '0' CELLPADDING ='5' style='border:0px solid black' align='center'>
																				<tbody>
																					<tr>
																						<td rowspan='2' style='border-bottom:1px solid #004494'>
																							<label class='label_simple'>PAR ID</label>
																						</td>
																						<td rowspan='2' style='border-bottom:1px solid #004494;border-right:2px solid #004494'>
																							<label class='label_simple'>LOT ID</label>
																						</td>
																						<td rowspan='2' style='border-bottom:1px solid #004494;border-right:2px solid #004494;'>
																							<label class='label_simple'>Refus</label>
																						</td>
																						<td colspan='3'style='border-right:2px solid #004494'>
																							<label class='label_simple'>Perspective</label>
																						</td>
																						<td colspan='2' style='border-right:2px solid #004494;'>
																							<label class='label_simple'>Accord</label>
																						</td>
																					</tr>
																					<tr>
																						<td style='border-bottom:1px solid #004494;'>
																							<label class='label_simple'>Acte</label>
																						</td>
																						<td style='border-bottom:1px solid #004494'>
																							<label class='label_simple'>Convention</label>
																						</td>
																						<td style='border-bottom:1px solid #004494;border-right:2px solid #004494'>
																							<label class='label_simple'>Acquisition</label>
																						</td>																	
																						<td style='border-bottom:1px solid #004494'>
																							<label class='label_simple'>Convention</label>
																						</td>
																						<td style='border-bottom:1px solid #004494;border-right:2px solid #004494;'>
																							<label class='label_simple'>Acquisition</label>
																						</td>	
																					</tr>
																				</tbody>
																				<tbody id='tbody_tbl_maitrise_parc'>
																			
																				</tbody>
																			</table>
																		</fieldset>	
																	</td>
																</tr>
															</tbody>
														</table>
													</td>		
												</tr>
											</tbody>											
											<tbody id='tbody_echange_pied'>
												<tr>
													<td>
														<div class='lib_alerte' id='echange_prob_saisie' style='width:580px;display:none;'>Veuillez compl&eacuteter votre saisie</div>
													</td>
													<td class='save'>
														<input type='button' onclick="" name='btn_valide_echange' id='btn_valide_echange' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="confirmDeleteEchange" class="explorateur_span" style='display:none;'>
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="confirmDeleteEchange_sousdiv" class='no-border' style='text-align:center;'>
									<table class="explorateur_contenu" cellspacing="0" cellpadding="5" border="0" align="center" frame="box" rules="NONE">
										<tbody>
											<tr>
												<td>
													<label class='label'>Veuillez confirmer la suppression d'un échange</label>
												</td>
											</tr>
											<tr>
												<td>
													<label class='label_simple' id='lbl_resume_delete_echange'></label>
												</td>
											</tr>
											<tr>
												<td style='padding-right:20px;text-align:center;'>
													<input id='btn_confirmDeleteEchange_one' type='button' class='btn_frm' style='width:60px' onclick="" value='Oui'>
													<input id='btn_confirmDeleteEchange_all' type='button' class='btn_frm' style='width:120px' onclick="" value='Oui pour tous'>
													<input id='btn_confirmDeleteEchange_non' type='button' class='btn_frm' style='width:60px' onclick="cacheMess('confirmDeleteEchange');changeOpac(100, 'onglet');" value='Non'>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_update_interloc" class="explorateur_span" style="display:none;top:220px">
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_sousdiv_update_interloc" style='text-align:center;'>
									<form class='form' onsubmit="return false" name='frm_update_interloc' id='frm_update_interloc' method='POST' style='display:inline-block'>
										<input type='hidden' id='update_interloc_id' name='update_interloc_id'>
										<input type='hidden' id='hid_update_parc_interloc' name='hid_update_parc_interloc'>
										<input type='hidden' id='hid_update_entite_af_id' name='hid_update_entite_af_id'>
										<table class="explorateur_contenu" cellspacing="0" cellpadding="5" border="0" align="center" frame="box" rules="NONE">
											<tbody>
												<tr>
													<td style="text-align:center;">
														<label class='label' id='update_interloc_interloc_lib'></label>
													</td>
													<td style="text-align:right;width:25px;">
														<img width="15px" onclick="cacheMess('explorateur_update_interloc');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td colspan='2'>
														<fieldset id='fld_update_interloc_eaf' class='fieldset_af'>
															<legend>Type d'interlocuteur pour l'EAF</legend>
															<table width = '100%' CELLSPACING = '2' CELLPADDING ='5' style='border:0px solid black' align='center'>
																<tr>
																	<td style='text-align:right;white-space:nowrap;'>
																		<label class='label'>
																			En tant que :
																		</label>	
																	</td>
																	<td id="td_update_type_interloc" class="normal" style='text-align:left;white-space:nowrap;' colspan='2'>
																		<label class="btn_combobox"> 
																			<select name="update_type_interloc" id="update_type_interloc" class="combobox" onchange="adaptListParc(this, 'update_');">
																				<option value='-1'>- - - Type d'interlocuteur - - -</option>
																				<?php
																				$rq_lst_type_interloc = "
																				SELECT DISTINCT 
																					type_interloc_id, 
																					type_interloc_lib, 
																					all_eaf 
																				FROM 
																					animation_fonciere.d_type_interloc
																					JOIN animation_fonciere.d_groupe_interloc USING (groupe_interloc_id)
																				ORDER BY type_interloc_id";
																				$res_lst_type_interloc = $bdd->prepare($rq_lst_type_interloc);
																				$res_lst_type_interloc->execute();
																				While ($donnees = $res_lst_type_interloc->fetch())  {
																					If ($donnees['all_eaf'] == 't') {
																						$all_eaf = 'all_eaf';
																					} Else {
																						$all_eaf = 'not_all_eaf';
																					}
																					echo "<option class='".$all_eaf."' value='$donnees[type_interloc_id]'>$donnees[type_interloc_lib]</option>";
																				}
																				?>
																			</select>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td style='text-align:right;white-space:nowrap;'>
																		<label class='label'>
																			Etat :
																		</label>	
																	</td>
																	<td id="td_update_etat_interloc" class="normal" style='text-align:left;white-space:nowrap;'>
																		<label class="btn_combobox"> 
																			<select id="update_etat_interloc" class="combobox">											
																				<?php
																				$rq_lst_etat_interloc = "
																				SELECT DISTINCT 
																					etat_interloc_id, 
																					etat_interloc_lib
																				FROM 
																					animation_fonciere.d_etat_interloc
																				ORDER BY etat_interloc_id";
																				$res_lst_etat_interloc = $bdd->prepare($rq_lst_etat_interloc);
																				$res_lst_etat_interloc->execute();
																				While ($donnees = $res_lst_etat_interloc->fetch())  {						
																					echo "<option value='$donnees[etat_interloc_id]'>$donnees[etat_interloc_lib]</option>";
																				}
																				?>
																			</select>
																		</label>
																	</td>
																	<td style='text-align:left;width:100%;'>
																		<label class='lib_alerte' style='font-weight:normal;'>La modification de cette information peut entraîner<br>des changements dans <b>toutes</b> les SAF en cours</label>
																	</td>
																</tr>
															</table>	
														</fieldset>
													</td>
												</tr>
											</tbody>										
											<tbody>	
												<tr>
													<td colspan='2'>
														<fieldset id='fld_update_interloc' class='fieldset_af'>
															<legend>Donn&eacute;es personnelles de l'interlocuteur</legend>
															<table width = '100%' CELLSPACING = '2' CELLPADDING ='0' style='border:0px solid black' align='center'>
																<tr id='tr_gtoper-1' style='display:block;'>
																	<td colspan='2'>
																		<table align='center' CELLSPACING='2' CELLPADDING='0' style="padding-top:10px;border:0px;float:none;">
																			<tr>
																				<td id="td_update_interloc_lst_particule" class="normal" style='text-align:center;white-space:nowrap;'>
																					<label class="btn_combobox">
																						<select name="update_interloc_lst_particule" id="update_interloc_lst_particule" class="combobox">
																							<option value='-1'>---Particule---</option>
																							<?php
																							$rq_lst_particules = "SELECT DISTINCT ccoqua, particule_lib FROM cadastre.d_ccoqua ORDER BY ccoqua";
																							$res_lst_particules = $bdd->prepare($rq_lst_particules);
																							$res_lst_particules->execute();
																							While ($donnees = $res_lst_particules->fetch())  {
																								echo "<option value='$donnees[ccoqua]'>$donnees[particule_lib]</option>";
																							}
																							?>
																						</select>
																					</label>
																					<input type='text' size='32' class='editbox' name='update_interloc_nom_usage' id='update_interloc_nom_usage'>
																					<input type='text' size='32' class='editbox' name='update_interloc_prenom_usage' id='update_interloc_prenom_usage'>
																				</td>
																			</tr>
																			<tr>
																				<td style='text-align:left;'>
																					<label class='label'>
																						N&eacute;(e) le : <input type='text' value="" size='10' placeholder='jj/mm/aaaa' onfocus="this.placeholder = '';" onblur="this.placeholder = 'jj/mm/aaaa';" class='editbox' name='update_interloc_date_naissance' id='update_interloc_date_naissance'>
																					</label>
																					<label class='label'>
																						&agrave; : <input type='text' value="" size='40' class='editbox' name='update_interloc_lieu_naissance' id='update_interloc_lieu_naissance'>
																					</label>
																				</td>	
																			</tr>
																			<tr style='margin-top:15px'>
																				<td colspan='2'>
																					<label class='label'>
																						Epoux (se) de <input type='text' size='32' placeholder='Nom du conjoint' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du conjoint';" class='editbox' name='update_interloc_nom_conjoint' id='update_interloc_nom_conjoint'>
																						<input type='text' size='32' placeholder='Pr&eacute;nom du conjoint' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Pr&eacute;nom du conjoint';" class='editbox' name='update_interloc_prenom_conjoint' id='update_interloc_prenom_conjoint'>
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<label class='label'>
																						Nom de jeune fille : <input type='text' value="" size='32' placeholder='Nom de jeune fille' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom de jeune fille';" class='editbox' name='update_interloc_nom_jeunefille' id='update_interloc_nom_jeunefille'>
																					</label>
																				</td>
																			</tr>		
																		</table>
																	</td>
																</tr>
																<tr id='tr_gtoper-2' style='display:none;'>
																	<td colspan='2'>
																		<table align='center' CELLSPACING='2' CELLPADDING='0' style="padding-top:10px;border:0px;float:none;">
																			<tr>
																				<td class="normal" style='text-align:center;white-space:nowrap;'>
																					<input type='text' size='90' class='editbox' name='update_interloc_ddenom' id='update_interloc_ddenom'>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table align='center' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
																			<tr>
																				<td colspan='2'>
																					<table align='center' CELLSPACING='2' CELLPADDING='0' style="padding-top:0px;border:0px">
																						<tr>
																							<td rowspan='4' style="vertical-align:top;">
																								<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
																							</td>
																							<td>
																								<input type='text' size='60' class='editbox' id='update_interloc_adresse1' name='update_interloc_adresse1' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / B&acirc;timent">
																							</td>
																						</tr>
																						<tr>
																							<td style='text-align:right;'>
																								<input type='text' size='60' class='editbox' id='update_interloc_adresse2' name='update_interloc_adresse2' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N&deg;, Rue, Boite postale">
																							</td>	
																						</tr>
																						<tr>
																							<td style='text-align:right;'>
																								<input type='text' size='60' class='editbox' id='update_interloc_adresse3' name='update_interloc_adresse3' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Compl&eacute;ment">
																							</td>	
																						</tr>
																						<tr>
																							<td style='text-align:right;'>
																								<input type='text' size='60' class='editbox' id='update_interloc_adresse4' name='update_interloc_adresse4' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal & Ville">
																							</td>	
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td style='vertical-align:bottom;'>
																					<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
																				</td>
																				<td style='vertical-align:middle;padding-top:20px;'>
																					<input type='text' size='60' class='editbox' name='update_interloc_mail' id='update_interloc_mail'>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td>
																		<table align='center' CELLSPACING='2' CELLPADDING='0' style="padding-top:0px;border:0px; padding-right:50px">
																			<tr>
																				<td style='text-align:right'>
																					<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
																				</td>
																				<td style='white-space:nowrap;text-align:left;'>
																					<input type='text' size='15' class='editbox' name='update_interloc_tel_fix1' id='update_interloc_tel_fix1'><label class='label' style='font-weight:normal;font-size:8pt;'>(perso)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style='text-align:right'>
																					<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
																				</td>
																				<td style='white-space:nowrap;text-align:left;'>
																					<input type='text' size='15' class='editbox' name='update_interloc_tel_fix2' id='update_interloc_tel_fix2'><label class='label' style='font-weight:normal;font-size:8pt;'>(pro)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style='text-align:right'>
																					<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
																				</td>
																				<td style='white-space:nowrap;text-align:left;'>
																					<input type='text' size='15' class='editbox' name='update_interloc_tel_portable1' id='update_interloc_tel_portable1'><label class='label' style='font-weight:normal;font-size:8pt;'>(perso)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style='text-align:right'>
																					<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
																				</td>
																				<td style='white-space:nowrap;text-align:left;'>
																					<input type='text' size='15' class='editbox' name='update_interloc_tel_portable2' id='update_interloc_tel_portable2'><label class='label' style='font-weight:normal;font-size:8pt;'>(pro)</label>
																				</td>	
																			</tr>
																			<tr>
																				<td style='text-align:right'>
																					<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
																				</td>
																				<td style='white-space:nowrap;text-align:left;'>
																					<input type='text' size='15' class='editbox' name='update_interloc_tel_fax' id='update_interloc_tel_fax'>
																				</td>	
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='2'>
																		<table align='center' CELLSPACING='2' CELLPADDING='0' style="padding-top:10px;border:0px;float:none;">
																			<tr>
																				<td style='text-align:right;white-space:nowrap;'>
																					<label class='label'>
																						Commentaires :
																					</label>	
																				</td>
																				<td style='text-align:center;white-space:nowrap;'>
																					<textarea onkeyup="resizeTextarea('update_interloc_commentaires');" onchange="resizeTextarea('update_interloc_commentaires');" id='update_interloc_commentaires' class="label textarea_com_edit" rows='1' cols='60'></textarea>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody id='tbody_update_parc_interloc'>	
												<tr>
													<td style='text-align:left;white-space:nowrap;' colspan='2'>
														<fieldset id='fld_update_parc_interloc' class='fieldset_af'>
															<legend>Parcelles associ&eacute;es &agrave; l'interlocuteur</legend>
															<table width = '100%' CELLSPACING = '0' CELLPADDING ='0' style='border:0px solid black' align='center'>
																<tbody>
																	<tr>
																		<td>
																			<input id='update_check_all_parc' type='checkbox' checked onclick="$('.update_chk_parc').prop('checked', $('#update_check_all_parc').prop('checked'));checkEchange('update');">
																			<label class='last_maj'>
																				Tous/ Aucun
																			</label>
																		</td>
																		<td class='only_proprio' style='text-align:center;'>
																			<label class='last_maj'>
																				D&eacute;membrement/ Indivision
																			</label>
																		</td>
																		<td class='only_proprio' style='text-align:center;'>
																			<label class='last_maj'>
																				Droit r&eacute;el ou particulier
																			</label>
																		</td>
																	</tr>
																</tbody>
																<tbody id='tbody_update_check_parc_interloc' >
																
																</tbody>
															</table>
														</fieldset>	
													</td>
												</tr>
											</tbody>
											<tbody>
												<tr>
													<td>
														<div class='lib_alerte' id='update_interloc_prob_saisie' style='width:580px;display:none;'>Veuillez compl&eacuteter votre saisie</div>
													</td>
													<td class='save'>
														<input type='button' onclick="" name='btn_valide_update_interloc' id='btn_valide_update_interloc' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
													</td>
												</tr>
											</tbody>
										</table>
									</form>	
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div id="explorateur_gestion_referent" class="explorateur_span" style='display:none;width:90%;'>
				<table cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE" width='100%'>
					<tbody>
						<tr>
							<td>
								<div id="explorateur_gestion_referent_sousdiv" class='no-border' style='text-align:center;width:100%;'>
									<table class="explorateur_contenu" cellspacing="0" cellpadding="2" border="0" align="center" frame="box" rules="NONE" width='100%'>
										<tbody>
											<tr>		
												<td style="text-align:left;">	
													<label class='last_maj'>
														Pour passer un interlocuteur en r&eacute;f&eacute;rent vous devez cochez la case correspondante.<br>
														Vous pouvez ensuite paramétrer les interlocuteurs par cliquer/ d&eacute;placer.
													</label>
													<br>
													<label class='last_maj' style='color:red;'>
														Attention seuls les interlocuteurs associés à l'ensemble des parcelles de l'EAF peuvent être référent.
													</label>
												</td>												
												<td style="text-align:right;width:25px;vertical-align:top;">	
													<img width="15px" onclick="cacheMess('explorateur_gestion_referent');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
												</td>
											</tr>
											<tr>
												<td colspan='2'>
													<table id='tbl_gestRef' width='100%'>
														
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' style='text-align:right;'>
													<label class='lib_alerte'>Les modifications sont enregistrées automatiquement dans le système</label>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>	
			
			<div id="explorateur_gestion_substitution" class="explorateur_span" style='display:none;width:90%;'>
				<table cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE" width='100%'>
					<tbody>
						<tr>
							<td>
								<div id="explorateur_gestion_substitution_sousdiv" class='no-border' style='text-align:center;width:100%;'>
									<table class="explorateur_contenu" cellspacing="0" cellpadding="2" border="0" align="center" frame="box" rules="NONE" width='100%'>
										<tbody>
											<tr>		
												<td style="text-align:left;">	
													<label class='last_maj'>
														Vous pouvez associer des représentants par cliquer/ d&eacute;placer.
													</label>
												</td>
												<td style="text-align:right;width:25px;vertical-align:top;">	
													<img width="15px" onclick="cacheMess('explorateur_gestion_substitution');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
												</td>
											</tr>
											<tr>
												<td colspan='2'>
													<table id='tbl_gestSubst' width='100%'>
														
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' style='text-align:right;'>
													<label class='lib_alerte'>Les modifications sont enregistrées automatiquement dans le système</label>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>	
			
			<div id="explorateur_regroupe_interloc" class="explorateur_span" style='display:none;width:90%;'>
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE" width='100%'>
					<tbody>
						<tr>
							<td>
								<div id="explorateur_regroupe_interloc_sousdiv" class='no-border' style='text-align:center;width:100%;'>
									<table class="explorateur_contenu" cellspacing="0" cellpadding="0" border="0" align="center" frame="box" rules="NONE">
										<tbody>
											<tr>
												<td style="text-align:right;">
													<img width="15px" onclick="cacheMess('explorateur_regroupe_interloc');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
												</td>
											</tr>
											<tr>
												<td>
													<input type='text' class='editbox' name='flt_lst_interloc_regroup' id='flt_lst_interloc_regroup' placeholder='Filtrer sur le nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Filtrer sur le nom';" oninput="showFormRegroupInterloc('update', <?php echo $session_af_id; ?>);">
													<img id='img_flt_lst_interloc_regroup' name='img_flt_lst_interloc_regroup' src="<?php echo ROOT_PATH; ?>images/supprimer.png" style='cursor:pointer;width:20px' onclick="$('#flt_lst_interloc_regroup').val('');showFormRegroupInterloc('update', <?php echo $session_af_id; ?>);">
												</td>
											</tr>
											<tr>
												<td>
													<label class='btn_combobox'>
														<select class='combobox' id='interloc_regroup' name='interloc_regroup' onchange="">
															<option value='-1'>--- Sélectionnez un interlocuteur ---</option>
														</select>
													</label>
												</td>
											</tr>
											<tr>
												<td class="label">
													<table CELLSPACING = '0' CELLPADDING ='3' border='0' align='center' style='margin-top:10px;'>
														<tr>
															<td style='text-align:center'>Propriétaires disponibles</td>
															<td>&nbsp;</td>
															<td style='text-align:center' rowspan='2'>Propriétaires associés à l'interlocuteur</td>
														</tr>
														<tr>
															<td>
																<input type='text' style='width:90%;' size='32' class='editbox' name='flt_proprios_dispos_nom' id='flt_proprios_dispos_nom' placeholder='Filtrer sur le nom' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Filtrer sur le nom';">
																<img id='img_flt_proprios_dispos_nom' name='img_flt_proprios_dispos_nom' src="<?php echo ROOT_PATH; ?>images/filtre.png" style='cursor:pointer;width:20px' onclick="">
															</td>
															<td colspan='2'>
																&nbsp;
															</td>
														</tr>
															<td width='380px'>
																<select style='width:100%;height:380px' class='info_contenu' multiple='multiple' id='proprios_dispos' name='proprios_dispos[]'>
																</select>
															</td>
															<td width='50px' align='center'>
																<label id='proprio_add' class='prec_suiv' onclick="adddelRegroupProprio('add')">
																	>>>
																</label>
																<br>
																<br>
																<br>
																<label id='proprio_del' class='prec_suiv' onclick="adddelRegroupProprio('del')">
																	<<<
																</label>																	
															</td>
															<td width='380px'>
																<select style='width:100%;height:380px' multiple='multiple' class='info_contenu' id='proprios_associes' name='proprios_associes[]'>
																</select>
															</td>
														</tr>
														<tr>
															<td>
																<table CELLSPACING = '0' CELLPADDING ='0' border='0' width='100%'>
																	<tr>
																		<td style='text-align:left;'>
																			<label style='cursor:pointer;' id='flt_proprios_saf' class="last_maj" onclick="">Propriétaires sur la SAF</label>
																		</td>
																		<td style='text-align:right;'>
																			<label style='cursor:pointer;' id='flt_proprios_sites' class="last_maj" onclick="">Propriétaires sur les sites</label>
																		</td>
																	</tr>
																</table>
															</td>
															<td colspan='2'>
																&nbsp;
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class='save'>
													<label id="lbl_regroupe_proprios" class="lib_alerte" style="display: none;">
														<b>Vous devez associer au moins un propriétaires</b>
													</label>
													<input id='btn_regroupe_proprios' name='btn_regroupe_proprios' type="button" onclick="regroupeProprios();"  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
			
			<div id="explorateur_infos_eaf" class="explorateur_span" style='display:none;width:90%;'>
				<table class="explorateur_fond" cellspacing="0" cellpadding="5" border="0" frame="box" rules="NONE" width='100%'>
					<tbody>
						<tr>
							<td>
								<div id="explorateur_infos_eaf_sousdiv" class='no-border' style='text-align:center;'>
									<table class="explorateur_contenu" cellspacing="6" cellpadding="6" border="0" align="center" frame="box" rules="NONE">
										<tbody id='tbody_infos_eaf_entete'>
											<tr>
												<td>
													<table class='no-border' width='100%'>
														<tr>
															<td>
																<label id ='lbl_title_infos_eaf' class="label">Informations sur une Entité d'Animation Foncière</label>
															</td>
															<td style="text-align:right;width:25px;">																	
																<img width="15px" onclick="cacheMess('explorateur_infos_eaf');changeOpac(100, 'onglet');$('#tbody_infos_eaf_contenu').html('');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
															</td>																	
														</tr>
													</table>
												</td>
											</tr>
										</tbody>
										<tbody id='tbody_infos_eaf_contenu'>
										
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
			
			<div id="explorateur_prix_occsol" class="explorateur_span" style='display:none;width:90%;'>
				<table class="explorateur_fond" cellspacing="0" cellpadding="5" border="0" frame="box" rules="NONE" width='100%'>
					<tbody>
						<tr>
							<td>
								<div id="explorateur_prix_occsol_sousdiv" class='no-border' style='text-align:center;'>
									<table class="explorateur_contenu" cellspacing="6" cellpadding="6" border="0" align="center" frame="box" rules="NONE">
										<tbody id='tbody_prix_occsol_entete'>
											<tr>
												<td>
													<table class='no-border' width='100%'>
														<tr>
															<td>
																<label class="label">Saisie des prix au m² par occupation du sol</label>
															</td>
															<td style="text-align:right;width:25px;">																	
																<img width="15px" onclick="cacheMess('explorateur_prix_occsol');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
															</td>																	
														</tr>
													</table>
												</td>
											</tr>
										</tbody>
										<tbody id='tbody_prix_occsol_contenu'>
											<tr>
												<td>
													<table id='tbl_prix_occsol' class='no-border' width='100%'>
												
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<label class='last_maj'>Ce tableau permet de renseigner un prix par occupation du sol pour chaque parcelle concernée.<br>Seules les informations manquantes seront renseignées.</label>
												</td>
											</tr>
										</tbody>
										<tbody id='tbody_prix_occsol_pied'>
											<tr>
												<td>
													<label id='lbl_prob_prix_occsol' class='lib_alerte' style='display:none;'>Veuillez compléter votre saisie</label>
												</td>
											</tr>
											<tr>
												<td class='save'>
													<input type='button' onclick="prixOccsol('<?php echo $session_af_id; ?>');" name='btn_valide_prix_occsol' id='btn_valide_prix_occsol' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
			
			<div id="explorateur_histo" class="explorateur_span" style="display:none;">
				<div id="explorateur_new">
					<table class="explorateur_contenu" cellspacing="0" cellpadding="4" border="0" align="center" frame="box" rules="NONE">
						<tbody id='tbody_histo'>
						</tbody>
					</table>
				</div>
			</div>

			<div id="loading" class="loading" style="display:none;">
				<img src="<?php echo ROOT_PATH; ?>images/ajax-loader.gif">
			</div>
		</section>
		
		<?php 
			$res_info_parcelle->closeCursor();
			$res_info_proprio->closeCursor();
			$res_bilan_animation->closeCursor();
			$res_lst_communes->closeCursor();
			$bdd = null;
			include("../../includes/footer.php");
		?>
	</body>
</html>