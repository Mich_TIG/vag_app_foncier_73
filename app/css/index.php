<?php 
	include ('../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/base_content.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<TITLE>Erreur 404 - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Vos outils test d&eacute;di&eacute;s';
			include("../includes/header.php");
		?>
		<nav>
			<table width='100%' class='breadcrumb'>
				<tr>
					<td style='width:30px;height:31px;cursor:pointer;'>
						<img id='img_back_page' src='<?php echo ROOT_PATH; ?>images/back_page.png' onclick="" width='25px'>
					</td>
					<td>
						<a href="<?php echo ROOT_PATH; ?>index.php#general">Accueil</a>
					</td>
				</tr>
			</table>
		</nav>		
		<section id='main'>
			<center><img src='<?php echo ROOT_PATH; ?>images/erreur404.png'></center>
		</section>
		<?php include("../includes/footer.php"); ?>
	</body>
</html>
