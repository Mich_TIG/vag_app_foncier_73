<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../../includes/configuration.php');
	include ('../../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/specific_content.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<TITLE>S.I.G. - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='S.I.G.';
			$h3 ='Création des utilisateurs PostgreSQL';
			include("../../includes/header.php");
			
			include ('../../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'), 'blocage');
			
			include('../../includes/breadcrumb.php');
			
			try
			{
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e)
			{
				die(include('../../includes/prob_tech.php'));
			}
			
			try
			{
				$sicen_bdd = new PDO("pgsql:host=".SICEN_HOST.";port=".SICEN_PORT.";dbname=".SICEN_DBNAME, $_SESSION['user'], $_SESSION['mdp2']);
				$sicen = 'ok';
			}
			catch (Exception $e)
			{
				$sicen = 'pasok';
			}
			
			$bad_caract = array("-","–","’", "'", "", "\t", "	");
			$good_caract = array("–","-","''", "''", "", "", "");
					
			If (isset($_GET['d']) && substr_count(base64_decode($_GET['d']), ':') > 0 && substr_count(base64_decode($_GET['d']), ';') > 0) {
				$d= base64_decode($_GET['d']);
				$min_crit = 0;
				$max_crit = strpos($d, ':');
				$min_val = strpos($d, ':') + 1;
				$max_val = strpos($d, ';') - $min_val;
				
				For ($p=0; $p<substr_count($d, ':'); $p++) {
					$crit = substr($d, $min_crit, $max_crit);
					$value = substr($d, $min_val, $max_val);
					$$crit = $value;
					$min_crit = strpos($d, ';', $min_crit) + 1;
					$max_crit= strpos($d, ':', $min_crit) - $min_crit;
					$min_val = $min_crit + $max_crit + 1;
					$max_val = strpos($d, ';', $min_val) - $min_val;
				}
			}
			
			If (isset($do) && $do == 'create' && $verif_role_acces == 'OUI') {
					
				$msg_creat_ok = false;
					
				If ($_POST['chk_bd_foncier'] == true) {
					$rq_select_individu = "
						SELECT d_individu.nom, d_individu.prenom
						FROM personnes.d_individu
						WHERE d_individu.nom = upper('$_POST[need_user_nom]') AND d_individu.prenom = '$_POST[need_user_prenom]';
					";
					$res_select_individu = $bdd->prepare($rq_select_individu); $res_select_individu->execute();
					If ($res_select_individu->rowCount() == 0) { // new user
						$rq_creat_user = "
							CREATE ROLE ".chr(34).$_POST['need_user_login'].chr(34).";
						";
						$res_creat_user = $bdd->prepare($rq_creat_user); $res_creat_user->execute();
						$rq_creat_user = "
							ALTER ROLE ".chr(34).$_POST['need_user_login'].chr(34)." WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN PASSWORD '".$_POST['need_user_mdp']."';
						";
						$res_creat_user = $bdd->prepare($rq_creat_user); $res_creat_user->execute();
						$user_mdp = base64_encode($_POST['need_user_mdp']);
						$user_mdp2 = base64_encode($_POST['need_user_mdp2']);
						$rq_creat_individu = "
							WITH t1 AS (
								INSERT INTO personnes.d_individu (nom, prenom) VALUES (upper('$_POST[need_user_nom]'), '$_POST[need_user_prenom]') RETURNING individu_id)
							INSERT INTO admin_sig.utilisateurs(utilisateur_id, individu_id, mdp_cen, mdp_ovh, mail)
							SELECT '$_POST[need_user_login]', t1.individu_id, '$user_mdp', '$user_mdp2', '$_POST[need_user_mail]' FROM t1;
						";
						$res_creat_individu = $bdd->prepare($rq_creat_individu); $res_creat_individu->execute();
						$rq_creat_individu = "
							WITH t1 AS (
								SELECT oid, rolname FROM pg_roles WHERE rolname = '$_POST[need_user_login]')
							UPDATE admin_sig.utilisateurs SET oid = t1.oid FROM t1 WHERE utilisateur_id = t1.rolname;
						";
						$res_creat_individu = $bdd->prepare($rq_creat_individu); $res_creat_individu->execute();
					}
					$count = count($_POST['lst_bd_foncier']);
					for ($i = 0; $i < $count; $i++) {
						$grpe = $_POST['lst_bd_foncier'][$i];
						$rq_grant_user = "
							GRANT $grpe TO ".chr(34).$_POST['need_user_login'].chr(34).";
						";
						$res_grant_user = $bdd->prepare($rq_grant_user); $res_grant_user->execute();
					}
					$msg_creat_ok = true;
				}

				If (isset($_POST['chk_sicen']) && $_POST['chk_sicen'] == true) {
					$rq_creat_user = "
						INSERT INTO ".SCH_PERSONNE.".personne (remarque, fax, portable, tel_pro, tel_perso, pays, ville, code_postal, adresse_1, prenom, nom, email, role, specialite, mot_de_passe, createur, titre, date_maj, id_structure) VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$_POST[need_user_prenom]', upper('$_POST[need_user_nom]'), '$_POST[need_user_mail]', '$_POST[lst_sicen]', NULL, md5('$_POST[need_user_mdp2]'), NULL, '$_POST[need_user_titre]', now(), $_POST[lst_sicen_struct]);
					";
					$res_creat_user = $sicen_bdd->prepare($rq_creat_user); $res_creat_user->execute();
					
					$msg_creat_ok = true;
				}
					
			}
		?>
		<section id='main'>
			<h3>Création des utilisateurs PostgreSQL</h3>
			<div id='div_main'>
				<?php
					$vget_maj = "do:create;";
					$vget_crypt_maj = base64_encode($vget_maj);
					$lien_maj= substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/creat_user.php?d=$vget_crypt_maj";
				?>
				<form class='form' name='frm_creat_user' onsubmit="return valide_form('frm_creat_user');" action='<?php echo $lien_maj; ?>' id='frm_creat_user' method='POST'>
					<center>					
						<div>
							<table border=0 style='border-collapse:collapse; width:auto;'>
								<tr>
									<?php 
										If ($sicen == 'ok') {
									?>
									<td>
										<label class="label">Titre :</label><br />
										<div><label class="btn_combobox">
											<select class='combobox' style='border-color:#a2c282;text-align:left;' id='need_user_titre' name='need_user_titre'>
												<option value='-1'>---</option>
												<?php
													$rq_lst_titre = "
														SELECT unnest(enum_range(null::md.enum_titre))::text as titre
														ORDER BY titre
														";
													$res_lst_titre = $sicen_bdd->prepare($rq_lst_titre);
													$res_lst_titre->execute();
													$lst_titre = $res_lst_titre->fetchAll();	
													For ($i=0; $i<$res_lst_titre->rowCount(); $i++) {
														echo "<option value='" . $lst_titre[$i]['titre'] . "'>" . $lst_titre[$i]['titre'] ."</option>";
													}
												?>
											</select>
										</label></div>
									</td>
									<?php 
										}
									?>
									<td>
										<label class="label">Prénom :</label><br />
										<input type='text' size='15' class='editbox' style='border-color:#a2c282;text-align:left;text-transform:capitalize;' id='need_user_prenom' name='need_user_prenom' value='' onkeyup='updLoginMail();'>
									</td>
									<td>
										<label class="label">Nom :</label><br />
										<input type='text' size='15' class='editbox' style='border-color:#a2c282;text-align:left;text-transform:uppercase;' id='need_user_nom' name='need_user_nom' value='' onkeyup='updLoginMail();'>
									</td>
									<td>
										<label class="label">E-mail :</label><br />
										<input type='text' size='25' class='editbox' style='border-color:#a2c282;text-align:left;' id='need_user_mail' name='need_user_mail' value='' placeholder='<?php echo STRUCTURE_MAIL.DOMAINE_MAIL; ?>'>
									</td>
								</tr>
								<tr>
									<?php 
										If ($sicen == 'ok') {
									?>
									<td>
										&nbsp;
									</td>
									<?php 
										}
									?>
									<td>
										<label class="label">Login :</label><br />
										<input type='text' size='15' class='editbox' style='border-color:#a2c282;text-align:left;' id='need_user_login' name='need_user_login' value='' placeholder='<?php echo USER_LOGIN; ?>'>
									</td>
									<td>
										<label class="label">Mot de passe :</label><br />
										<input type='text' size='15' class='editbox' style='border-color:#a2c282;text-align:left;' id='need_user_mdp' name='need_user_mdp' value=''>
									</td>
									<td>
										<label class="label">Mot de passe (sicen) :</label><br />
										<input type='text' size='15' class='editbox' style='border-color:#a2c282;text-align:left;' id='need_user_mdp2' name='need_user_mdp2' value=''>
										<input type='button' onclick="generatePwd('need_user_mdp'); document.getElementById('need_user_mdp2').value= document.getElementById('need_user_mdp').value;" title='Générer un mdp' style="background: #fff url(../../images/cle.png) no-repeat 5px center; background-size: 6px; cursor: pointer;"/>
									</td>
								</tr>
							</table>
							&nbsp;
							<table border=0 style='border-collapse:collapse; width:500px;'>
								<tr align=center>
									<center>
									<td style='width:250px;'>
										<input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='chk_bd_foncier' name='chk_bd_foncier' onclick="affGroupe('bd_foncier');">
										<label id='lbl_bd_foncier' class="label"><?php echo DBNAME; ?></label>
									</td>
									<!--SAVOIE-->
									<td style='width:250px;'>
										<input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='chk_sicen' name='chk_sicen' onclick="affGroupe('sicen');" <?php If ($sicen != 'ok') { echo 'disabled'; } ?> >
										<label id='lbl_sicen' class="label">sicen</label>
									</td>
									
									</center>
								</tr>
								<tr align=center>
									<td valign=top align=center>
										<select multiple id='lst_bd_foncier' size="10" name="lst_bd_foncier[]" style='display:none;'>
											<?php
												$rq_lst_grp = "
													SELECT groname
													FROM pg_catalog.pg_group
													WHERE left(groname,5) != 'sicen' --AND groname != 'grp_foncier'
													ORDER BY replace(replace(replace(groname,'sgrp_',''),'grp_',''),'cen_user','1')";
												$res_lst_grp = $bdd->prepare($rq_lst_grp);
												$res_lst_grp->execute();
												$lst_grp = $res_lst_grp->fetchAll();
												$ouverture = 'NON';
												For ($i=0; $i<$res_lst_grp->rowCount(); $i++) {
													If ((strpos($lst_grp[$i]['groname'], 'sgrp_foncier') === false) && $ouverture == 'OUI') {
														echo "</optgroup>";
													}
													If ($lst_grp[$i]['groname'] == 'grp_foncier') {
														echo "<optgroup label='" . $lst_grp[$i]['groname'] . "'>";
														$ouverture = 'OUI';
													} Else {
														echo "<option value='" . $lst_grp[$i]['groname'] . "'";
															If ($lst_grp[$i]['groname'] == 'cen_user') {
																echo "selected='selected' readonly";
															}
														echo ">" . $lst_grp[$i]['groname'] ."</option>";
													}
												}
											?>
										</select>
										<center>
											<label id='conseil_lst_bd_foncier' class="label" style="color:#d77b0e; text-decoration:none; cursor:pointer; font-size:9px; text-align: center; display:none;"> Pour une multi-sélection, appuyer sur Ctrl. </label>
										</center>
									</td>
									<?php 
										If ($sicen == 'ok') {
									?>
									<td valign=top>
										<select id='lst_sicen' size="6" name="lst_sicen" style='display:none;'>
											<?php
												$rq_lst_role = "
													SELECT  unnest(enum_range(null::md.enum_role))::text as role
													ORDER BY role
													";
												$res_lst_role = $sicen_bdd->prepare($rq_lst_role);
												$res_lst_role->execute();
												$lst_role = $res_lst_role->fetchAll();	
												For ($i=0; $i<$res_lst_role->rowCount(); $i++) {
													echo "<option value='" . $lst_role[$i]['role'] . "'>" . $lst_role[$i]['role'] ."</option>";
												}
											?>
										</select>
										<select id='lst_sicen_struct' size="6" name="lst_sicen_struct" style='display:none;'>
											<?php
												$rq_lst_struct = "
													SELECT  id_structure, nom_structure
													FROM ".SCH_PERSONNE.".structure
													ORDER BY nom_structure
													";
												$res_lst_struct = $sicen_bdd->prepare($rq_lst_struct);
												$res_lst_struct->execute();
												$lst_struct = $res_lst_struct->fetchAll();	
												For ($i=0; $i<$res_lst_struct->rowCount(); $i++) {
													echo "<option value='" . $lst_struct[$i]['id_structure'] . "'>" . $lst_struct[$i]['nom_structure'] ."</option>";
												}
											?>
										</select>
									</td>
									<?php 
										}
									?>
								</tr>
							</table>
						</div>
						<div>
							&nbsp;
							<table>
								<tr>
									<td class='processing'>
										<input type='submit' name='valid_form' id='valid_form' title="Lancer la création de l'utilisateur">
									</td>
								</tr>
							</table>
							<div class='lib_alerte' id='prob_saisie' style='display:none;'>Veuillez compl&eacuteter votre saisie</div>
							<?php 
								If (isset($do) && $do == 'create' && $verif_role_acces == 'OUI' && isset($msg_creat_ok) && $msg_creat_ok == true) {
									echo "<div class='label' id='ok_saisie' style='text-align:center;'>Utilisateur enregistré avec succès</div>";
								}
							?>
						</div>
					</center>
				</form>
			</div>
		</section>

		<?php include("../../includes/footer.php"); ?>
	</body>
</html>

<script type="text/javascript">

function updLoginMail() {
	
	$p = clear_accent(document.getElementById('need_user_prenom').value.toLowerCase().replace(/ /g,"").substring(0, 1));
	$prenom = clear_accent(document.getElementById('need_user_prenom').value.toLowerCase().replace(/ /g,""));
	$n = clear_accent(document.getElementById('need_user_nom').value.toLowerCase().replace(/ /g,"").substring(0, 1));
	$nom = clear_accent(document.getElementById('need_user_nom').value.toLowerCase().replace(/ /g,""));
	
	$user_login = '<?php echo USER_LOGIN; ?>';
	switch ($user_login) {
		case 'pnom':
			$login = $p + $nom; break;
		case 'prenomnom':
			$login = $prenom + $nom; break;
		case 'prenomn':
			$login = $prenom + $n; break;
		case 'p.nom':
			$login = $p + '.' + $nom; break;
		case 'prenom.nom':
			$login = $prenom + '.' + $nom; break;
		case 'prenom.n':
			$login = $prenom + '.' + $n; break;
		case 'p-nom': 
			$login = $p + '-' + $nom; break;
		case 'prenom-nom':
			$login = $prenom + '-' + $nom; break;
		case 'prenom-n':
			$login = $prenom + '-' + $n; break;
		case 'p_nom':
			$login = $p + '_' + $nom; break;
		case 'prenom_nom':
			$login = $prenom + '_' + $nom; break;
		case 'prenom_n':
			$login = $prenom + '_' + $n; break;
	}
	$structure_mail = '<?php echo STRUCTURE_MAIL; ?>';
	switch ($structure_mail) {
		case 'pnom':
			$mail = $p + $nom; break;
		case 'prenomnom':
			$mail = $prenom + $nom; break;
		case 'prenomn':
			$mail = $prenom + $n; break;
		case 'p.nom':
			$mail = $p + '.' + $nom; break;
		case 'prenom.nom':
			$mail = $prenom + '.' + $nom; break;
		case 'prenom.n':
			$mail = $prenom + '.' + $n; break;
		case 'p-nom':
			$mail = $p + '-' + $nom; break;
		case 'prenom-nom':
			$mail = $prenom + '-' + $nom ; break;
		case 'prenom-n':
			$mail = $prenom + '-' + $n; break;
		case 'p_nom':
			$mail = $p + '_' + $nom; break;
		case 'prenom_nom':
			$mail = $prenom + '_' + $nom; break;
		case 'prenom_n':
			$mail = $prenom + '_' + $n; break;
	}
	if ( $login != '.' && $login != '-' && $login != '_' ) {
		document.getElementById('need_user_login').value = $login;
	} else {
		document.getElementById('need_user_login').value = '';
	}
	if ( $mail != '.' && $mail != '-' && $mail != '_' ) {
		document.getElementById('need_user_mail').value = $mail + '<?php echo DOMAINE_MAIL; ?>';
	} else {
		document.getElementById('need_user_mail').value = '';
	}
}

function clear_accent(str){
	var accent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
	];
	var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
	for(var i = 0; i < accent.length; i++){
		str = str.replace(accent[i], noaccent[i]);
	}
	return str;
}

function affGroupe($bd) {
	id_chk = 'chk_' + $bd;
	id_lst = 'lst_' + $bd;
	id_lst_struct = 'lst_' + $bd + '_struct';
	if (document.getElementById(id_chk).checked == 1) {
		document.getElementById(id_lst).style.display = "inline"; 
		if (id_chk == 'chk_sicen') {
			document.getElementById(id_lst_struct).style.display = "inline";
		}
		if (id_chk == 'chk_bd_foncier') {
			document.getElementById('conseil_lst_bd_foncier').style.display = "block"; 
		}
	} else {
		document.getElementById(id_lst).style.display = "none"; 
		if (id_chk == 'chk_sicen') {
			document.getElementById(id_lst_struct).style.display = "none"; 
		}
		if (id_chk == 'chk_bd_foncier') {
			document.getElementById('conseil_lst_bd_foncier').style.display = "none"; 
		}
	}
}

</script>