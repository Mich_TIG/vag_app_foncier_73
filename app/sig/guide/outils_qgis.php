<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../../includes/configuration.php');
	include ('../../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>sig/guide/guide_sig_style.css" media="screen">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>sig/guide/guide_sig_style_print.css" media="print">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<TITLE>S.I.G. - <?php echo STRUCTURE; ?></TITLE>
		<script type="text/javascript">
			function showDetail(id) {
				heightDivDown = $('.div_detail:visible').height()
				$('.div_detail').slideUp(800)
				if ($('#'+id.replace('img', 'detail')).css('display') == 'none') {
					$('#'+id.replace('img', 'detail')).slideDown(800)
					if ($('#'+id).offset().top > heightDivDown && $('#'+id).offset().top-heightDivDown > 0 && heightDivDown > 0) {
						$('html,body').animate({scrollTop: $('#'+id).offset().top-heightDivDown-$('#'+id).height()},	'slow');
					} else {
						$('html,body').animate({scrollTop: $('#'+id).offset().top},	'slow');
					}
				}				
			}
		</script>
	</head>
	<body>
		<?php 
			$h2 ='S.I.G.';
			$h3 ='Aide sur les outils QGis du CEN Savoie';
			include("../../includes/header.php");
			include('../../includes/breadcrumb.php');	
			include ('../../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'));
		?>		
		<section id='main'>
			<h3><?php echo $h3; ?></h3>
			<table cellpadding='20'>
				<tr>
					<td style='width:50%;vertical-align:top;'>
						<div class='avant-propos' style='margin-top:0;'>Avant-propos</div>
						<p>
							Cette aide pr&eacute;sente les différents outils developpés par le service SIG du Cen Savoie.
						</p>
						<p>
							Il ne faut pas confondre un plugin et un formulaire d'attributs.<br>
						</p>
						<p>
							Un plugin sert à exécuter des tâches sur une <span class='evidence'>couche complète</span> ou une <span class='evidence'>sélection d'objets</span>. Il se lance à partir d'un bouton spécifique dans QGis.
						</p>
						<p>
							Un formulaire sert à renseigner les données attributaires d'<span class='evidence'>un seul</span> objet d'une couche. Il s'ouvre automatiquement à la création d'un objet ou grâce à l'outil '<i>Identifier les entités</i>'<img style='margin: 0 0 -6px 1px;' width="25" alt="" src="images/info_entity.png">.
						</p>
						<p>
							Regroupés par service, vous pouvez consulter leur fonctionnement en <span class='evidence'>cliquant sur l'icône</span> des plugins ou le titre des formulaires.
						</p>
					</td>
					<td style='vertical-align:top;'>
						<div class='avant-propos' style='margin-top:0;'>Rappel</div>
						<p>
							<span class='evidence'>PostgreSQL</span> est un système de gestion de base de données géographiques.<br>
							Il s'agit d'un système complexe que vous interrogez (peut être sans le savoir) avec QGis ou en naviguant sur l'Intranet.<br>
							Les données stockées par PostgreSQL <b><u>ne sont pas</u></b> consultables sur le serveur via un explorateur Windows.
						</p>	
						<p>
							Un <span class='evidence'>schéma</span> <img style='margin: 0 0 -4px 1px;' width="15" alt="" src="images/schema.png"> permet d'organiser les objets de la base de données en groupes logiques afin de faciliter leur gestion.</br>
							Les schémas sont comparables aux répertoires du système d'exploitation, à ceci près qu'ils ne peuvent pas être imbriqués.
						</p>
						<p>
							Une <span class='evidence'>vue</span> est une extraction d'une ou plusieurs tables de la base de données via une requête <a class="interwiki iw_wpfr" title="En savoir plus avec wikipedia" href="https://fr.wikipedia.org/wiki/Structured_Query_Language" target="_blank">SQL</a>. Une vue est comparable à une table classique.
						</p>
					</td>
				</tr>
			</table>
			<br>
			<div id='onglet' class='list'>
				<ul>
					<li>
						<a href="#plugins"><span style='line-height:1;font-family:verdana;font-size:15px;margin:0;'>Plugins</span></a>
					</li>
				</ul>
				
				<div id='plugins'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<div class='avant-propos'>Plugins Foncier</div>
								
								<div style='padding:15px 0;'>
									<img id='img_saf' onclick="showDetail(this.id);" width='50px' src="images/createSessionAF.png" alt="Image SAF" style='margin-right:15px;top:20px;position:relative;cursor:pointer;'>
									<span class='plugin_title'>Créer une Session d'Animation Foncière</span>				
									<div id='detail_saf' style='display:none;padding-left:80px;' class='div_detail'>
										<p>
											Ce plugin permet de créer une nouvelle session d'animation à partir d'une sélection de parcelles sur un ou plusieurs sites.<br>
										</p>
										<div class="notetip" style="vertical-align:middle">Le bouton est disponible après avoir sélectionné des objets depuis la vue '<i>parcelles_dispo_saf</i>' de '<i>PostgreSQL</i>'</div>										
										<div class='div_img_tuto' style='width:750px'>
											<img width='750px' src="images/tuto_saf.png" alt="Image TUTO SAF">
										</div>
									</div>
								</div>
								
								<div style='padding:15px 0;'>
									<img id='img_acte' onclick="showDetail(this.id);" width='50px' src="images/parcelleToActe.png" alt="Image ACTE" style='margin-right:15px;top:20px;position:relative;cursor:pointer;'>
									<span class='plugin_title'>Associer des parcelles à un acte</span>				
									<div id='detail_acte' style='display:none;padding-left:80px;' class='div_detail'>
										<p>
											Ce plugin permet d'associer des parcelles à un acte.
										</p>
										<div class="notetip" style="vertical-align:middle">Le bouton est disponible après avoir sélectionné des objets depuis la vue '<i>parcelles_dispo</i>' de '<i>PostgreSQL</i>'</div>
										<div class="notehelp" style="vertical-align:middle">Les parcelles sont à sélectionner depuis une vue affichant en temps réel les parcelles disponibles.</div>
										<div class="notewarning" style="vertical-align:middle">Les parcelles sélectionnées doivent appartenir au même site.</div>
										<div class='div_img_tuto' style='width:750px'>
											<img width='750px' src="images/tuto_acte.png" alt="Image TUTO ACTE">
										</div>
										<div class="notehelp" style="vertical-align:middle">Vous pouvez également ajouter ou supprimer des parcelles directement depuis l'Intranet.</div>
									</div>
								</div>		
							</td>
						</tr>
					</table>
				</div>
			</div>
		</section>
		<?php include("../../includes/footer.php"); ?>
		<?php 
			If (isset($_GET['title'])) {
		?>
		<script type="text/javascript">
			$(document).ready( function () {
				$('#<?php echo str_replace('title', 'detail', $_GET['title']); ?>').css('display', '')
				$('html,body').animate({scrollTop: $('#<?php echo $_GET['title']; ?>').offset().top},	'slow');
			});
		</script>
		<?php
			}
		?>
	</body>
</html>