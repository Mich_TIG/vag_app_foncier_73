<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	If ($browser <> "" && isset($browser) && str_replace(str_replace(array('http://', 'https://'), '', ROOT_PATH), '', $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']) <> 'index.php' && (!isset ($_SESSION['login']) or !isset ($_SESSION['mdp']) )) {		
		include ('configuration.php');
?>
		<script type="text/javascript">
			var JS_ROOT_PATH = '<?php echo ROOT_PATH; ?>';
		</script>
		<script type="text/javascript">
			window.onload = Init;
			var waitTime = 5; // Temps d'attente en seconde			
			var url = JS_ROOT_PATH+'index.php?do=new_cnx';     // Lien de destination
			var x;			
		</script>
		<?php include_once("header.php"); ?>
		<nav>
			<table width='100%' class='breadcrumb'>
				<tr>
					<td style='width:30px;height:31px;'>
						&nbsp;
					</td>
				</tr>
			</table>
		</nav>
		<section id='main'>
			<h3><?php If (isset($h3)) {echo $h3;} else {echo 'Bienvenue';}?></h3>
			<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/home.css">
			<center>
			<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_fond' width='100%'>
				<tr>
					<td>
						<center>
						<form action="<?php echo ROOT_PATH; ?>index.php#general" method="POST">
							<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='identification_contenu' height='150px'>
								<tr width='90%'>
									<td><img src='<?php echo ROOT_PATH; ?>images/exclamation.png' width='80px'></td>
									<td class='label'>Vous n'&ecirc;tes pas identif&eacute;</td>
								</tr>
								<tr width='90%'>
									<td colspan='2' class='label'><center>Vous allez maintenant &ecirc;tre redirig&eacute; dans <span id='compteur'>5</span>s.</center></td>
								</tr>
							</table>
						</form>
						</center>
					</td>
				</tr>
			</table>
			</center>
		</section>	
		<?php 
			include('footer.php'); 
			exit();
	}
	
	If ($_SESSION['bloqued_ip']) {
		?>
		<section id='main'>
			<h3>Attention</h3>
			<center>
			<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_fond' width='100%'>
				<tr>
					<td>
						<center>
							<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' height='150px'>
								<tr width='90%'>
									<td class='label'>
										<div class='lib_alerte'><b>Vous avez été banni</b></div>
									</td>
								</tr>
							</table>
						</form>
						</center>
					</td>
				</tr>
			</table>
			</center>
		</section>
		<?php 
			include("footer.php"); 
			exit();
	} Else If (isset($_SESSION['nombre']) && $_SESSION['nombre'] > 6) {		
		?>
		<section id='main'>
			<h3>Attention</h3>
			<center>
			<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_fond' width='100%'>
				<tr>
					<td>
						<center>
							<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' height='150px'>
								<tr width='90%'>
									<td class='label'>
										<div class='lib_alerte'>
											<b>Vous ne pouvez pas vous connecter pour le moment.</b>
											<br>
											Temps restant : <span id='compteur'><?php echo $_SESSION['timestamp_limite'] - time() ?></span>s.
										</div>
									</td>
								</tr>
							</table>
						</form>
						</center>
					</td>
				</tr>
			</table>
			</center>
		</section>
		<script type="text/javascript">
			var waitTime = <?php echo $_SESSION['timestamp_limite'] - time() ?>; // Temps d'attente en seconde
			var url = JS_ROOT_PATH+'index.php?do=new_cnx';     // Lien de destination
			Init();
		</script>
		<?php 
			include("footer.php"); 
			exit();
	} Else If ($browser <> "" && isset($browser) && str_replace(str_replace(array('http://', 'https://'), '', ROOT_PATH), '', $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']) == 'index.php' && (!isset ($_SESSION['login']) or !isset ($_SESSION['mdp']))) {		
		include_once('configuration.php');
		?>
		
		<section id='main'>
			<h3><?php If (isset($h3)) {echo $h3;} else {echo 'Bienvenue';}?></h3>
			<center>
			<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_fond' width='100%'>
				<tr>
					<td>
						<center>
						<form action="<?php echo ROOT_PATH; ?>index.php#general" method="POST">
							<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='identification_contenu' height='150px'>
								<tr width='90%'>
									<td class='label'>Identifiant : </td>
									<td><input type='text' class='editbox' id='login' name='login' size='20'></td>
								</tr>
								<tr width='90%'>
									<td class='label'>Mot de passe : </td>
									<td><input type='password' class='editbox' id='pwd' name='pwd' size='20'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<center>
										<input type='submit' class='valid'>
										</center>
									</td>
								</tr>
							</table>
						</form>
						</center>
					</td>
				</tr>
			</table>
			<?php If (isset($_POST['pwd'])) {echo "<div class='lib_alerte'><b>Echec de l'identification</b></div>";} ?>
			</center>
		</section>
		<?php 
			include("footer.php"); 
			exit();
	}
	
	If ($browser == "" or !isset($browser)) {		
		include('configuration.php');
		?>
		<section id='main'>
			<h3><?php If (isset($h3)) {echo $h3;} else {echo 'Bienvenue';}?></h3>
			<center>
			<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_fond' width='100%'>
				<tr>
					<td>
						<center>
							<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='identification_contenu' height='150px'>
								<tr width='90%'>
									<td class='label'><center>Veuillez utiliser Firefox</center></td>
								</tr>
								<tr width='90%'>
									<td class='label'><a href='https://www.mozilla.org/fr/firefox/desktop/'><img src='<?php echo ROOT_PATH; ?>images/firefox.png' width='300px'></a></td>
								</tr>
							</table>
						</center>
					</td>
				</tr>
			</table>
			</center>
		</section>
		<?php 
			include("footer.php"); 
			exit();
	}	

	function role_access($role, $mode='normal') {
		$role_acces = 'NON';
		foreach ($role as &$value) {
			if (in_array($value, $_SESSION['grp']) || in_array('grp_sig', $_SESSION['grp'])) {
				$role_acces = 'OUI';
				break;
			}
		}
		If ($role_acces == 'NON' && $mode == 'blocage') {
			die(include('prob_tech.php'));
		}
		return $role_acces;
	}
?>