<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	$tab_lien_breadcrumb = array (
		'accueil_foncier.php' => 'Accueil BD Foncier',
		'foncier_liste_site.php' => 'Recherche par site',
		'foncier_liste_parcelle.php' => 'Recherche par parcelle',
		'foncier_liste_proprio.php' => 'Recherche par propriétaire',
		'foncier_liste_acte.php' => 'Recherche par acte',
		'foncier_bilan.php' => 'Bilan complet',
		'foncier_admin.php' => 'Administration de la BD Foncier',
		'fiche_site.php' => 'Fiche site',
		'fiche_parcelle.php' => 'Fiche parcelle',
		'fiche_proprio.php' => 'Fiche propriétaire',
		'fiche_mfu.php' => 'Fiche maîtrise foncière et d\'usage',
		'fiche_new_mfu.php' => 'Fiche maîtrise foncière et d\'usage',
		'fiche_saf.php' => 'Fiche session d\'animation foncière',
		'fiche_saf_help.php' => 'Fiche session d\'animation foncière (mode AIDE)',
		'accueil_alertes.php' => 'Vos alertes en détail',
		'contacts_liste_all.php' => 'Les contacts du Conservatoire',
		'contact_select.php' => 'Fiche contact',
		'new_contact.php' => 'Nouveau contact',
		'codex.php' => 'Codex Geomatica',
		'creat_user.php' => 'Création des utilisateurs PostgreSQL',
		'outils_qgis.php' => 'Outils QGis',
		'../index.php#sig' => 'S.I.G.',
		'../../index.php#sig' => 'S.I.G.',
	);

	$tab_breadcrumb = array (
		'foncier' => array (
			'accueil_foncier.php' => array (),
			'foncier_liste_site.php' => array ('accueil_foncier.php'),
			'foncier_liste_parcelle.php' => array ('accueil_foncier.php'),
			'foncier_liste_proprio.php' => array ('accueil_foncier.php'),
			'foncier_liste_acte.php' => array ('accueil_foncier.php'),
			'foncier_bilan.php' => array ('accueil_foncier.php'),
			'foncier_admin.php' => array ('accueil_foncier.php'),
			'fiche_site.php' => array ('accueil_foncier.php', 'foncier_liste_site.php'),
			'fiche_parcelle.php' => array ('accueil_foncier.php', 'foncier_liste_parcelle.php'),
			'fiche_proprio.php' => array ('accueil_foncier.php', 'foncier_liste_proprio.php'),
			'fiche_mfu.php' => array ('accueil_foncier.php', 'foncier_liste_acte.php'),
			'fiche_new_mfu.php' => array ('accueil_foncier.php'),
			'fiche_saf.php' => array ('accueil_foncier.php'),
			'fiche_saf_help.php' => array ('accueil_foncier.php')
			),
		'contacts' => array (
			'contacts_liste_all.php' => array (),
			'contact_select.php' => array ('contacts_liste_all.php'),
			'new_contact.php' => array ('contacts_liste_all.php')
			),
		'sig' => array (
			'codex.php' => array ('../index.php#sig'),
			'creat_user.php' => array ('../index.php#sig'),
			'outils_qgis.php' => array ('../index.php#sig')
			),
		'alertes' => array (
			'accueil_alertes.php' => array ()
			)
	);
	
	If ($_SERVER['SERVER_PORT'] == 80) {
		$tab_url = preg_split("/[\\/]+/", str_replace(str_replace(array('http://'.$_SERVER['SERVER_NAME'], 'https://'.$_SERVER['SERVER_NAME']), '', ROOT_PATH), '', $_SERVER['PHP_SELF']));
	} Else {
		$tab_url = preg_split("/[\\/]+/", str_replace(str_replace(array('http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'], 'https://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT']), '', ROOT_PATH), '', $_SERVER['PHP_SELF']));
	}
	end($tab_url);
	$breadcrumb = array (prev($tab_url), end($tab_url));
	
?>

<nav>
	<table width='100%' class='breadcrumb'>
		<tr>
	<?php 
		If (isset($_SESSION['back']) && count($_SESSION['back']) > 0 && count(array_filter($breadcrumb)) > 1) { 
	?>
			<td style='width:30px;height:31px;cursor:pointer;'>
				<img id='img_back_page' src='<?php echo ROOT_PATH; ?>images/back_page.png' onclick="" width='25px'>
			</td>	
			<td>
				<a href="<?php echo ROOT_PATH; ?>index.php#general">Accueil</a>
	<?php 
		} Else {
	?>
			<td style='width:30px;height:31px;'>
				<img id='img_accueil' src='<?php echo ROOT_PATH; ?>images/accueil.png' onclick="" width='25px'>
			</td>
			<td>
				<a>Accueil</a>				
	<?php 
		}
		
		If (count(array_filter($breadcrumb)) > 1) {
	?>
		>
	<?php
			If (array_key_exists(reset($breadcrumb), $tab_breadcrumb)) {
				For($i=0; $i<count($tab_breadcrumb[reset($breadcrumb)][end($breadcrumb)]); $i++) {
					$lien_suffixe = '';
					For ($j=0; $j<(count($tab_url)-2); $j++) {
						$lien_suffixe .= $tab_url[$j].'/';
					}
	?>
				<span>
					<a href="<?php echo ROOT_PATH.$lien_suffixe.reset($breadcrumb).'/'.$tab_breadcrumb[reset($breadcrumb)][end($breadcrumb)][$i];?>"><?php echo $tab_lien_breadcrumb[$tab_breadcrumb[reset($breadcrumb)][end($breadcrumb)][$i]] ;?></a>
				</span>
				>					
	<?php
				}
			}				
	?>
		<span><?php echo $tab_lien_breadcrumb[end($breadcrumb)]; ?></span>
	<?php
		}
	?>
			</td>
		</tr>
	</table>
</nav>