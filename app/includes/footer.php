<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	$page = explode('/', $_SERVER['PHP_SELF'])[count(explode('/', $_SERVER['PHP_SELF']))-1];
	
	function base64url_encode( $data ){		
	  return rtrim( strtr( encode(($data) ), '+/', '-_'), '=');
	}
	function encode($data){
	  for($i = 0; $i < strlen($data); $i++){
		$data[$i] = ~ $data[$i];
	  }
	  $data = base64_encode($data);
	  return $data;
	}

	$url = 'https://extranet.cen-savoie.org/mail.php?d=';
	$url .= substr(base64url_encode(implode('', array_slice(array_reverse(array_keys(array_flip(array_unique(str_split(strtolower(implode('', array_slice((array_keys(get_defined_constants(true)['user'])), 0, 12)))))))), 0,10))), 0, (round((strlen(base64url_encode(implode('', array_slice(array_reverse(array_keys(array_flip(array_unique(str_split(strtolower(implode('', array_slice((array_keys(get_defined_constants(true)['user'])), 0, 12)))))))), 0,10))))/3)-0.4, 0))) . base64url_encode(ROOT_PATH) . substr(base64url_encode(implode('', array_slice(array_reverse(array_keys(array_flip(array_unique(str_split(strtolower(implode('', array_slice((array_keys(get_defined_constants(true)['user'])), 0, 12)))))))), 0,10))), (round((strlen(base64url_encode(implode('', array_slice(array_reverse(array_keys(array_flip(array_unique(str_split(strtolower(implode('', array_slice((array_keys(get_defined_constants(true)['user'])), 0, 12)))))))), 0,10))))/3)-0.4, 0)), strlen(base64url_encode(implode('', array_slice(array_reverse(array_keys(array_flip(array_unique(str_split(strtolower(implode('', array_slice((array_keys(get_defined_constants(true)['user'])), 0, 12)))))))), 0,10)))));
?>

<footer>
	<table width='100%' CELLSPACING='0' CELLPADDING='0'>
		<tr>
			<td style='border-bottom:solid 5px #7ab51d' valign='bottom'>
				<img src='<?php echo ROOT_PATH; ?>images/frise_left.png' align='left' height='83px'>
			</td>
			<td style='border-bottom:solid 5px #7ab51d'; width='100%'>
				<div class='footer' style='width:100%;'>R&eacutealisation : Service SI - CEN Savoie<br>Version 2.0 - Sous licence <u><a href='http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html' target='_blank'>CeCILL-C</a></u></div>
				<div class='footer' style='width:100%'>&copy; CEN Savoie<?php echo ' ' . date('Y'); ?></div>
			</td>
			<td style='border-bottom:solid 5px #7ab51d' valign='bottom'>
		<?php
			If (isset ($_SESSION['login']) && isset ($_SESSION['mdp']) ) {
		?>
				<img src='<?php echo ROOT_PATH; ?>images/frise_right_debug.png' align='right' height='83px' usemap="#map_frise_right_debug">
				<map name="map_frise_right_debug">					
					<area style='cursor:pointer;' shape="rect" coords="50,31,65,50" onclick="$('#explorateur_debug').css('display', '');" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Signaler un bug"/>
				</map>
		<?php
			} Else {
		?>
			<img src='<?php echo ROOT_PATH; ?>images/frise_right.png' align='right' height='83px'>
		<?php
			}
		?>
			</td>
		</tr>
	</table>
</footer>
<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/debug.css">

<div id="explorateur_debug" class="debug_span" style='display:none;'>
	<form action='<?php echo $url; ?>' method='POST'>	
		<input type='hidden' id='root_path' name='root_path' value='<?php echo ROOT_PATH; ?>'>
		<table class="debug_contenu" cellspacing="6" cellpadding="6" border="0" align="center" frame="box" rules="NONE">
			<tbody id='tbody_debug_entete'>
				<tr>
					<td>
						<table class='no-border' width='100%'>
							<tr>
								<td style='text-align:center;'>
									<label class="label">Signaler un bug</label>
								</td>
								<td style="text-align:right;width:25px;">																	
									<img width="15px" onclick="cacheMess('explorateur_debug');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
								</td>																	
							</tr>
						</table>
					</td>
				</tr>
			</tbody>
			<tbody id='tbody_debug_contenu'>
				<tr>
					<td>
						<div class='debug_form'><label class='label'>Structure :</label><input type='text' id='struct' name='struct' value='<?php echo STRUCTURE; ?>' readonly="readonly"></div>
						<div class='debug_form'><label class='label'>Utilisateur :</label><input type='text' id='user' name='user' value='<?php echo $_SESSION['login']; ?>' readonly="readonly"></div>
						<div class='debug_form'><label class='label'>Votre mail :</label><input type='text' id='mail' name='mail' value='<?php echo $_SESSION['email']; ?>' readonly="readonly"></div>
						<div class='debug_form'><label class='label'>Page web en cause :</label><input type='text' value='<?php echo $page; ?>' readonly="readonly"></div>
						<input type='hidden' id='page' name='page' value='<?php echo end($_SESSION['back']); ?>'>
						<div class='debug_form'><label class='label'>Décrivez votre problème :</label><br><textarea id='explain' name='explain' class="label textarea_com_edit" rows='5' cols='60'></textarea>
					</td>
				</tr>									
			</tbody>
			<tbody id='tbody_debug_pied'>
				<tr>
					<td class='debug_send'>
						<input type='submit' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Envoyer">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>