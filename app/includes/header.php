<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
?>	

<script type="text/javascript">
	var JS_ROOT_PATH = '<?php echo ROOT_PATH; ?>';
	$(document).ready( function () {
		// Permet de revenir en haut de la page au click sur le bouton prévu à cet effet
		$('#btn_top').click(function() {
			$('html,body').animate({scrollTop: 0}, 'slow');
		});	
		// Les procédures suivantes se lancent dès que l'ascenseur verticale est utilisé (y compris la molette de la souris)
		$(window).scroll(function(){	
			if($(window).scrollTop()<350) { // Si l'ascenseur masque moins de 350 pixels alors ...
				$('#btn_top').fadeOut(800); // On cache le bouton de retour haut de page			
			} else { // Si l'ascenseur masque plus de 350 pixels alors ...			
				$('#btn_top').fadeIn(800); // On affiche le bouton retour haut de page			
			}
		});
		if (window.location.href.indexOf('index.php?do=new_cnx') == -1) {
			$.ajax({ //On entre maintenant dans la partie ajax
				url       : '<?php echo ROOT_PATH; ?>includes/cen_fct_ajax.php', //variable permettant de savoir où se trouve la fonction php appelé plus loin
				type      : "post", //type de communication entre les différentes pages/fonctions/langages
				dataType : "json", //format de la réponse permettant notamment un retour en variable tableau
				data      :{ 
					fonction:'fct_majLinkBack', var_url: window.location.href//variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
				},
				success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
					if ($('#img_back_page').length && data['link_back'].length && data['link_back'] != 'Null') {
						$('#img_back_page').attr('onclick', "backPage('"+data['link_back']+"', '<?php echo ROOT_PATH; ?>');");
					}
					if ($('#img_accueil').length && data['link_back'].length && data['link_back'] != 'Null') {
						$('#img_accueil').attr('src', $('#img_accueil').attr('src').replace('accueil.png', 'back_page.png'));
						$('#img_accueil').attr('onclick', "backPage('"+data['link_back']+"', '<?php echo ROOT_PATH; ?>');");
					}
					if (data['tbl_back'] && data['link_back'].length > 0) {
						$('#explorateur_debug').find('#page').val(data['tbl_back'][data['tbl_back'].length-1])
					}
				}
			});
		}
	} ) ;
</script>

<header>
	<table bgcolor='#ffffff' width='100%'>
		<tr width='100%'>
			<td width='300px'>
				<a href='<?php echo ROOT_PATH; ?>index.php#general'>
					<img src='<?php echo LOGO_PATH; ?>' width='300px'>
				</a>
			</td>
			<td class='td_h1'>
				<h1><center><?php echo TITLE; ?></center></h1>
				<h2><center><?php If (isset($h2)) {echo $h2;} else {echo 'Vos outils d&eacute;di&eacute;s';}?></center></h2>
			</td>
			<?php if (isset($_SESSION['login']) && isset($_SESSION['mdp'])) { ?>
			<td class='deconnexion' width='300px' height='10%' onmouseover="document.getElementById('deco').style.display=''" onmouseout="document.getElementById('deco').style.display='none'">
				<span style="position: relative; right: 15px; top: 8px;">Bonjour<?php If (!empty($_SESSION['prenom'])) { echo ' '.$_SESSION['prenom']; } ?>,</span></br>
				<div id='deco' style="display:none;">
					<input class='btn_frm' type='button' value='Déconnexion' onclick="document.location.href='<?php echo ROOT_PATH; ?>index.php?do=new_cnx'" style='margin-bottom:5px;'/>
					<?php
						If (isset($verif_role_acces) && $verif_role_acces == 'OUI' && isset($mode) && isset($lien_cnx)) { 
							If ($mode=='consult') {
								echo "</br><input id='chg_mode' class='btn_frm' type='button' value='Mode édition' onclick=\"document.location.href='$lien_cnx'\" />";
							}
							ElseIf ($mode=='edit') {
								echo "</br><input id='chg_mode' class='btn_frm' type='button' value='Mode consultation' onclick=\"document.location.href='$lien_cnx'\" />";
							}
						} 
					?>
				</div>
			</td>
			<?php } else { ?>
			<td width='300px'>
			</td>
			<?php } ?>
		</tr>
	</table>
	<img width="30px" id="btn_top" class="backtotop" alt="Retour haut de page" onclick="return false;" src="<?php echo ROOT_PATH; ?>images/back_top.png" style="display:none;">
</header>