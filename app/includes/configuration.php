<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

/* OBLIGATOIRE */

	defined('MDP_FIRST_CNX') or define('MDP_FIRST_CNX', 'first_cnx.com');
	defined('MDP_FONCIER') or define('MDP_FONCIER', 'foncier.com');

    // Paramètres fixes de connexion à la base PostgreSQL
	defined('HOST') or define('HOST', '192.168.100.4');
    defined('PORT') or define('PORT', '5432');
	defined('DBNAME') or  define('DBNAME', 'bd_cen_savoie');
	
	// Paramètres de connexion automatique (client Windows) : permet une connexion automatique entre un client Windows et le serveur, les identifiants de l'AD et de connexion PostgreSQL doivent être identiques. inscrire non si le client est sous Linux
	defined('AUTO_CONN') or define('AUTO_CONN', 'oui'); // valeurs disponibles : oui non
		// si oui : LOC pour gérer une installation sur un serveur local, CEN pour gérer une installation distante
		defined('IPs_LOC') or define('IPs_LOC', '192.168');
		defined('IPs_CEN') or define('IPs_CEN', '193.252.211.105'); // si vous avez des antennes et comme PHP5 n'accepte pas les constantes "array", saisir les adresses autorisées en les séparant par un ;
	
	// Paramètres du serveur web
	defined('ROOT_PATH') or define('ROOT_PATH', 'http://192.168.100.4/Cen_Savoie/Intranet/');

	// Paramètres de personnalisation des pages
	defined('STRUCTURE') or define('STRUCTURE', 'CEN Savoie');
	defined('TITLE') or define('TITLE', 'Intranet '. STRUCTURE .'');
	defined('LOGO_PATH') or define('LOGO_PATH', ''. ROOT_PATH .'images/Logo_CEN_Savoie.jpg');
	
	// Paramètres de gestion des données
	defined('GEST_ACT') or define('GEST_ACT', 'web'); // valeurs disponibles : web file
		// si web : dossier à partir de ROOT_PATH dans lequel iront automatiquement les actes (ex : actes/). Ils seront stockés et nommés automatiquement et ils seront accessibles depuis le navigateur.
		// si file : racine du dossier pour retrouver les actes (ex : file://///nas/foncier/actes/ ou file:///N:/foncier/actes/). Tous les actes devront se situer au même endroit et le nom du pdf doit reprendre l'identifiant mu_id/mf_id de la base de données. Attention pour des raisons de sécurité, il faudra copier-coller vous-même le lien vers le pdf dans la barre du navigateur (clic droit, copier l'adresse du lien)
		defined('PATH_ACT') or define('PATH_ACT', 'actes/'); // file://///srv-cpns/Sig/Z_ADMIN/actes/
	defined('PARTINOT_ID') or define('PARTINOT_ID', 6); // id de la table personnes.d_particules pour les notaires 
	defined('STRUCTNOT_ID') or define('STRUCTNOT_ID', 6); // id de la table personnes.d_structures pour les notaires 
	defined('FONCTNOT_ID') or define('FONCTNOT_ID', 33); // id de la table personnes.d_fonctpers pour les notaires 
	
/* FACULTATIF */
	
	// Paramètres d'accès aux données inventaires ZH : permet d'afficher l'identifiant des zones humides sur les fiches parcelles (intersection)
	defined('NAME_SCHEMA_ZH') or define('NAME_SCHEMA_ZH', 'inventaires');
	defined('NAME_TABLE_ZH') or define('NAME_TABLE_ZH', 'suivi_zh');
	defined('ID_TABLE_ZH') or define('ID_TABLE_ZH', 'id_bdd');
	
	// Paramètres de personnalisation des utilisateurs (uniquement pour la pré-saisie lors de la création d'utilisateur) : permet d'utiliser l'outil de création d'utilisateur
	defined('USER_LOGIN') or define('USER_LOGIN', 'prenom_n'); // valeurs disponibles : p prenom n nom . - _
	defined('STRUCTURE_MAIL') or define('STRUCTURE_MAIL', 'p.nom'); // valeurs disponibles : p prenom n nom . - _
	defined('DOMAINE_MAIL') or define('DOMAINE_MAIL', '@cen-savoie.org');
	
	// Paramètres fixes de connexion à la base PostgreSQL SiCen (attention à l'utilisateur et au mdp... dans l'état, ils doivent être "identiques" entre la base PostgreSQL et PostgreSQL SiCen) : permet sur l'outil de création d'utilisateur de créer des comptes SiCen dans le même temps
	defined('SICEN_HOST') or define('SICEN_HOST', '91.134.194.222');
    defined('SICEN_PORT') or define('SICEN_PORT', '5432');
	defined('SICEN_DBNAME') or  define('SICEN_DBNAME', 'sicen');
	defined('SCH_PERSONNE') or  define('SCH_PERSONNE', 'md'); // schéma pour localiser la table personne
	
?>