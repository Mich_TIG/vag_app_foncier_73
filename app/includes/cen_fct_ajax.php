<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	session_start();
	$fonction = $_POST['fonction'];
	unset($_POST['fonction']);
	$fonction($_POST);
		
	function fct_backPage($data) {
		array_pop($_SESSION['back']);
		If (end($_SESSION['back']) == $data['var_url']) {
			array_pop($_SESSION['back']);
		}
		$result['link_back'] = end($_SESSION['back']);
		$result['tbl_back'] = $_SESSION['back'];
		print json_encode($result);
	}
	
	/* Fonction appelée dans le header. Elle permet d'adapter le onclick du bouton retour */
	function fct_majLinkBack($data) {
		If (isset($_SESSION['back']) && count($_SESSION['back'])>1 && substr(end($_SESSION['back']), 0, strlen($data['var_url'])) == $data['var_url']) {
			end($_SESSION['back']);
			$result['link_back'] = prev($_SESSION['back']);
		} Else If (isset($_SESSION['back']) && count($_SESSION['back'])>0 && substr(end($_SESSION['back']), 0, strlen($data['var_url'])) != $data['var_url']) {
			$result['link_back'] = end($_SESSION['back']);
		} Else {
			$result['link_back'] = 'Null';
		}
		
		If (isset($_SESSION['back']) && count($_SESSION['back'])>0 && substr(end($_SESSION['back']), 0, strlen($data['var_url'])) != $data['var_url'] && substr($data['var_url'], -10) != 'do=new_cnx') {
			$index = count($_SESSION['back']);
			$_SESSION['back'][$index] = $data['var_url'];
		} Else If (!isset($_SESSION['back']) || count($_SESSION['back']) == 0) {
			$_SESSION['back'][0] = $data['var_url'];
		}		
		$result['tbl_back'] = $_SESSION['back'];
		print json_encode($result);
	}
	
	/* Fonction appelée sur les pages composées d'onglets. Elle permet de mettre à jour le bouton retour sur l'onglet qui était actif à la page précédente. */
	function fct_updateLinkBack($data) {
		If (!isset($_SESSION['mdp'])) {
			$result['sessmdp'] = 'invalide';
		} Else {
			$result['sessmdp'] = 'valide';
		}
		If (isset($_SESSION['back']) && count($_SESSION['back']) > 0) {
			If (count($_SESSION['back']) > 1) {
				$lastLink = array_pop($_SESSION['back']);
			} Else {
				$lastLink = $_SESSION['back'][0];
			}
			$new_link = substr($lastLink, 0, strpos($lastLink, '#')).'#'.$data['var_li'];
			If ($lastLink != $new_link && strpos($lastLink, '#') > 0) {
				$_SESSION['back'][] = $new_link;
				$result['back'] = $new_link;
			} Else {
				$_SESSION['back'][] = $lastLink;
				$result['back'] = $lastLink;
			}
			$result['tbl_back'] = $_SESSION['back'];
		} Else {
			$result['tbl_back'] = '';
		}
		print json_encode($result);		
	}
	
	function rqExistant($data) {
		include ('../includes/configuration.php');
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		include('cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		$res = $bdd->prepare($data['rq']);
		$res->execute();
		If ($res->rowCount() > 0) {
			$nbr = chif_rome($res->rowCount()+1);
			$result['div'] = 'explorateur';
			$result['code'] = "
			<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_fond'>
				<tr>
					<td>
						<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_contenu' align='center'>
							<tr>
								<td class='label' colspan='2'>
									".$data['msg'].".
								</td>
							</tr>
							<tr>
								<td class='label' colspan='2'>
									Souhaitez-vous cr&eacute;er un homonyme ?
								</td>
							</tr>
							<tr>
								<td style='text-align:right;'>
									<input type='button' class='valid' onclick=\"cacheMess('explorateur');cacheMess('prob_saisie');changeOpac(100, '".$data['check_form']."');ConfirmForm('".$data['check_form']."','".$data['elt_modif']."',' (".$nbr.")');\">
								</td>
								<td style=\"text-align:left;\">";
								If ($data['origine'] == 'mfu') {
									$result['code'] .= "<img width='20px' style='cursor:pointer' src='". ROOT_PATH . "images/supprimer.png' onclick='self.close();'>";
								} Else {
									$result['code'] .= "
									<a href='" . ROOT_PATH . "contacts/contacts_liste_all.php?filtre_nom=".$data['elt_value']."'>
										<img width='20px' style=\"cursor:pointer\" src=\"" . ROOT_PATH . "images/supprimer.png\">
									</a>";
								}
								$result['code'] .= "		
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			";
		} Else {
			$result['div'] = '';
			$result['code'] = '';
		}
		print json_encode($result);
	}
	
	function ctrl_saisie_simple_in_bd($data) {
		include ('../includes/configuration.php');
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		$check_origin = $data['check_origin'];
		$check_form = $data['params']['check_form'];
		$check_table = $data['params']['check_table'];
		$check_lst_name = $data['params']['check_lst_name'];
		$check_champ_id = $data['params']['check_champ_id'];
		$check_champ_lib = $data['params']['check_champ_lib'];
		$check_where = $data['params']['check_where'];
		$check_intitule = $data['params']['check_intitule'];
		$check_intitule_lower = strtolower($check_intitule);
		$check_label = $data['params']['check_label'];
		$value = $data['params']['check_value'];
		$bad_caract = array("-","–","’", "'");
		$good_caract = array("–","-","''", "''");
		$val = str_replace($bad_caract, $good_caract, $value);
		$check_tab = array();
		$j=0;
		
		If (substr($check_origin, 0, 7) == 'confirm') {			
			$do = substr($value, 0, strpos($value, ';'));
			$new_val = substr($value, strpos($value, ';')+1, strlen($value)-strpos($value, ';'));
			
			If ($do == 'maj') {
				$new_val_add_bd = str_replace($bad_caract, $good_caract, $new_val);
				$rq_add_value = "INSERT INTO $check_table ($check_champ_lib) VALUES ('$new_val_add_bd')";
				$res_add_value = $bdd->prepare($rq_add_value);
				$res_add_value->execute();
			}
					
			$result['div'][0] = $check_lst_name;
			$result['action'][0] = 'innerHTML';
			$result['code'][0] = 
				"<select name='$check_lst_name' class='combobox' id='$check_lst_name'>
					<option value='-1'>- - - $check_intitule - - -</option>";									
					$rq_lst = "SELECT DISTINCT $check_champ_id as id, $check_champ_lib as lib FROM $check_table $check_where ORDER BY $check_champ_lib";
					$res_lst = $bdd->prepare($rq_lst);
					$res_lst->execute();
					While ($donnees = $res_lst->fetch()) {
						$result['code'][0] .="<option "; If ($donnees['lib'] == $new_val) {$result['code'][0] .= "selected ";} $result['code'][0] .= "value='$donnees[id]'>$donnees[lib]</option>";
					}
					$res_lst->closeCursor();
				$result['code'][0] .="</select>";
				
			$result['div'][1] = 'explorateur';
			$result['action'][1] = 'style';
			$result['code'][1] = 'none';
		} ElseIf ($check_origin == 'init') {//si la fonction est appeler pour la premiere fois
				//si une nouvelle valeur est bien saisie, on controle l'existence d'une valeur simileire dans la base
				If ($value <> '' && isset($value) && $value <> 'NULL' && strlen(str_replace(' ', '', $value)) > 0) {
					$rq_compare = "SELECT $check_champ_id AS ID, $check_champ_lib AS lib FROM $check_table";
					$res_compare = $bdd->prepare($rq_compare);
					$res_compare->execute();
					While ($donnees = $res_compare->fetch()) {
						$lib_compare = utf8_decode(str_replace("'", "", $donnees['lib']));
						$value_compare = utf8_decode(str_replace("'", "", $value));
						$test1 = levenshtein(strtolower($value_compare), strtolower($lib_compare));
						$test2 = levenshtein($value_compare, $lib_compare);
						$test3 = strlen($lib_compare) - strlen(str_replace(strtolower($value_compare) , '', strtolower($lib_compare)));
						$test4 = strlen($value_compare) - strlen(str_replace(strtolower($lib_compare) , '', strtolower($value_compare)));
						
						If ($value == $donnees['lib']) {
							$check_tab[0] = '$$**IDEM**$$';
							break;
						}
						If ($test2 == 0) {
							$check_tab = array();
							break;
						} ElseIf (($test1 <= round(strlen($value_compare)*0.2,0) OR ($test3 > 0 && strlen($lib_compare) > $test3) OR ($test4 > 0 && strlen($value_compare) > $test4)) && strlen($lib_compare) > 1) {
							$check_tab[$j] = $donnees['lib'];
							$j = $j + 1;
						}
					}
					$res_compare->closeCursor();
					
					If (count($check_tab) > 0 && $check_tab[0] <> '$$**IDEM**$$') {//Si une valeur similaire mais pas identique est trouvée on demande confirmation
						$result['div'][0] = 'explorateur_new';
						$result['action'][0] = 'innerHTML';
						$result['code'][0] = 
							"<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_contenu' align='center'>
								<tr>
									<td colspan='2' style='text-align:right;'>
										<img width='15px' style='cursor:pointer;' src=\"" . ROOT_PATH . "images/quitter.png\" onclick=\"cacheMess('explorateur');cacheMess('explorateur_alerte');changeOpac(100, '".$data['params']['check_form']."');\">
									</td>
								</tr>
								<tr>
									<td class='label'>
										Vous avez saisie <b>$value</b>. Veuillez confirmer votre choix.<br><br>
										<input type='radio' name='rad_new_elt' id='new_elt0' value=\"maj;$value\"><label for='new_elt0'>Conserver la saisie : <b>$value</b></label><br><br>";
										
										For ($j=1; $j<=count($check_tab); $j++) {
											{
												$lib_comp = $check_tab[$j-1];
												$result[0]['code'] .= "
												<input class='editbox' type='radio' name='rad_new_elt' "; If ($j==1) {$result[0]['code'] .= "checked='checked' ";} $result[0]['code'] .= "id='new_elt$j' value=\"keep;$lib_comp\"><label for='new_elt$j'>Utiliser : <b>$lib_comp</b></label><br><br>";
											}
										
										$result['code'][0] .= "
										<center><input type='button' class='valid' onclick=\"ajaxCtrlSaisie('confirm$j', '$check_form', '$check_lst_name', '$check_table', '$check_champ_id', '$check_champ_lib', '$check_intitule', '$check_label', '$check_where');changeOpac(100, '$check_form');\"></center>
									</td>
								</tr>
							</table>";
							
						$result['div'][1] = 'explorateur_alerte';
						$result['action'][1] = 'style';
						$result['code'][1] = 'none';
					} Else {
						If (count($check_tab) == 0 OR count($check_tab) == 1) {
							If (count($check_tab) == 0 OR (count($check_tab) == 1 && $check_tab[0] <> '$$**IDEM**$$')) {
								$rq_add_value = "INSERT INTO $check_table ($check_champ_lib) VALUES ('$val')";
								$res_add_value = $bdd->prepare($rq_add_value);
								$res_add_value->execute();
							}
																		
							$result['div'][0] = $check_lst_name;
							$result['action'][0] = 'innerHTML';
							$result['code'][0] = 
								"<select name='$check_lst_name' class='combobox' id='$check_lst_name'>
									<option value='-1'>- - - $check_intitule - - -</option>";									
									$rq_lst = "SELECT DISTINCT $check_champ_id as id_bd, $check_champ_lib as lib_bd FROM $check_table $check_where ORDER BY $check_champ_lib";
									$res_lst = $bdd->prepare($rq_lst);
									$res_lst->execute();
									While ($donnees = $res_lst->fetch()) {
										$result['code'][0] .="<option "; If ($donnees['lib_bd'] == $value) {$result['code'][0] .= "selected ";} $result['code'][0] .= "value='$donnees[id_bd]'>$donnees[lib_bd]</option>";
									}
							$result['code'][0] .="</select>";
						}
						
						$result['div'][1] = 'explorateur';
						$result['action'][1] = 'style';
						$result['code'][1] = 'none';
						
						$result['div'][2] = '';
						$result['action'][2] = 'changeOpac';
						$result['code'][2] = '';						
					}
				} Else {//si aucune valeur est saisie, on ajoute simplement un message d'alerte
					$result['div'][0] = 'explorateur_alerte';
					$result['action'][0] = 'style';
					$result['code'][0] = '';							
				}
			}
		print json_encode($result);
	}
	
	function reload_elt() {
		include ('../includes/configuration.php');
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		If (isset($_SESSION['NEW_NOTAIRE_ID'])) {
			$result['lst_notaire'] = "<option value='-1'>Notaire</option>";
			$rq_lst_notaire = "
			SELECT 
				personne_id as notaire_id, 
				nom  || ' ' || prenom || COALESCE(CASE WHEN t_nb_pers.nb_pers > 1 THEN ' (' || adresse3 || ')' END, '') as notaire_lib,
				row_number() OVER (ORDER BY personne_id) AS ordre
			FROM personnes.personne
				JOIN personnes.d_particules USING (particules_id)
				JOIN personnes.d_individu USING (individu_id)
				JOIN (SELECT individu_id, count(*) AS nb_pers FROM personnes.d_individu JOIN personnes.personne USING (individu_id) GROUP BY individu_id) t_nb_pers USING (individu_id)
			WHERE personne.fonctpers_id = ".FONCTNOT_ID."
			ORDER BY nom";
			$res_lst_notaire = $bdd->prepare($rq_lst_notaire);
			$res_lst_notaire->execute();
			While ($donnees = $res_lst_notaire->fetch()) {
				$result['lst_notaire'] .= "<option value='".$donnees['notaire_id']."'>".$donnees['notaire_lib']."</option>";
			}
			If (isset($_SESSION['NEW_NOTAIRE_ID'])) {
				$result['new_notaire'] = $_SESSION['NEW_NOTAIRE_ID'];
				unset($_SESSION['NEW_NOTAIRE_ID']);
			}		
			$res_lst_notaire->closeCursor();
		} Else {
			$result['lst_notaire'] ='';
			$result['new_notaire'] ='';
		}
		$bdd = null;
		print json_encode($result);
	}
?>