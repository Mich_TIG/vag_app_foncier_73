<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	/* Fonction permettant, à partir d'un tableau multidimensionnel, de créer toutes les combinaisons possible en rassemblant les différentes valeurs.
	Sources : https://gist.github.com/fabiocicerchia/4556892 */
	function combos($data, $all=array(), $group=array(), $val=null, $i=0) {
		if (isset($val)){
			array_push($group, $val);
		}
		if ($i >= count($data)){
			array_push($all, $group);
		} else {
			foreach ($data[$i] as $v){
				$all = combos($data, $all, $group, $v, $i + 1);
			}
		}
		return $all;
	}
	
	function chif_rome($num) {
		//I V X  L  C   D   M
		//1 5 10 50 100 500 1k
		$rome =array("","I","II","III","IV","V","VI","VII","VIII","IX");
		$rome2=array("","X","XX","XXX","XL","L","LX","LXX","LXXX","XC");
		$rome3=array("","C","CC","CCC","CD","D","DC","DCC","DCCC","CM");
		$rome4=array("","M","MM","MMM","IVM","VM","VIM","VIIM","VIIIM","IXM");
		$str=$rome[$num%10];
		$num-=($num%10);
		$num/=10;
		$str=$rome2[$num%10].$str;
		$num-=($num%10);
		$num/=10;
		$str=$rome3[$num%10].$str;
		$num-=($num%10);
		$num/=10;
		$str=$rome4[$num%10].$str;
		return $str;
	}	
	
	// Petite fonction toute simple pour permettre d'écrire un texte sur plusieurs lignes. Le nombre de caractères par lignes est passé en variable.
	// Le principe est d'insérer une balise <br> pour obliger un retour ligne en cours de chaîne.
	function retour_ligne($txt, $max_car) {
		$txt_return = wordwrap($txt, $max_car, " <br/>\n");
		return $txt_return;
	}
	
	//Fonction pour transformer une date au format jj/mm/aaaa
	function date_transform($date) {
		If (strlen($date) == 10) {
			$new_date = substr($date, 8, 2) . '/' . substr($date, 5, 2) . '/' . substr($date, 0, 4);
		} Else If (strlen($date) == 19) {
			$new_date = substr($date, 8, 2) . '/' . substr($date, 5, 2) . '/' . substr($date, 0, 4) . substr($date, 10, 9);
		} Else If (strlen($date) == 8) {
			$new_date = substr($date, 6, 2) . '/' . substr($date, 4, 2) . '/' . substr($date, 0, 4);
		} Else If (strlen($date) == 7) {
			$new_date = substr($date, 5, 2) . '/' . substr($date, 0, 4);
		} Else If (strlen($date) == 4) {
			$new_date = substr($date, 0, 4);
		} Else {
			$new_date = 'N.D.';
		}
		return $new_date;
	}
	
	//Fonction permettant de transformer une date au format aaaa-mm-jj
	function date_transform_inverse($date) {
		If ((strlen($date) == 3 OR strlen($date) == 4) && substr_count($date, '/') == 1) {
			$new_date = '-' . substr($date, 0, 2);
		} Else If (strlen($date) == 5 && substr_count($date, '/') == 1) {
			$new_date = substr($date, 3, 2) . '-' . substr($date, 0, 2);
		} Else If (strlen($date) > 5 && strlen($date) < 10 && substr_count($date, '/') == 2) {
			$new_date = '-' . substr($date, 3, 2) . '-' . substr($date, 0, 2);
		} Else If (strlen($date) == 7 && substr_count($date, '/') == 1) {
			$new_date = substr($date, 3, 7) . '-' . substr($date, 0, 2);
		} Else If (strlen($date) == 10 && substr_count($date, '/') == 2) {
			$new_date = substr($date, 6, 4) . '-' . substr($date, 3, 2) . '-' . substr($date, 0, 2);
		} Else If (strlen($date) == 19 && substr_count($date, '/') == 2 && substr_count($date, ':') == 2) {
			$new_date = substr($date, 6, 4) . '-' . substr($date, 3, 2) . '-' . substr($date, 0, 2) . substr($date, 10, 9);
		} Else {
			$new_date = $date;
		}
		return $new_date;
	}
	
	//Fonction pour transformer un numéro de téléphone "fr" 00 00 00 00 00
	function numtel_transform($numtel) {
		If ((strlen($numtel) == 10) && (substr($numtel,0,1) == '0')) {
			$new_numtel = substr($numtel, 0, 2) . ' ' . substr($numtel, 2, 2) . ' ' . substr($numtel, 4, 2) . ' ' . substr($numtel, 6, 2) . ' ' . substr($numtel, 8, 2);
		} Else {
			$new_numtel = $numtel;
		}
		return $new_numtel;
	}
?>