<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	include_once("header.php");
?>
<script type="text/javascript">	
	$(document).ready( function () {			
		$('.breadcrumb:gt(0)').each(function(){
			$(this).css('display', 'none');
		});			
	} ) ;
</script>
<nav>
	<table width='100%' class='breadcrumb'>
		<tr>
			<td style='width:30px;height:31px;'>
				<img id='img_accueil' src='<?php echo ROOT_PATH; ?>images/accueil.png' onclick="" width='25px'>
			</td>
			<td>
				<span>Accueil</span>
			</td>
		</tr>
	</table>
</nav>
<section id='main'>
	<h3>Un problème technique est survenu</h3>
	<center>
		<img src='<?php echo ROOT_PATH; ?>images/prob_tech.png'>
		<br>
		<label class='label'>
			La page ne peut pas être affichée correctement.
		</label>
		<br>
		<label class='label'>
			Si le problème persiste contactez votre service informatique.
		</label>
		<br>
		<br>
		<label class='label'>
			Pour revenir à une page opérationnelle cliquez <a href='<?php If (isset($page_operationnelle)) {echo $page_operationnelle;} Else {echo ROOT_PATH.'/index.php#general'; }?>'>ici</a>
		</label>
	</center>
	</div>
</section>
<?php 
	include_once("footer.php"); 
	die();
?>
		