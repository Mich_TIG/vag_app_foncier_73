<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	//Paramétrage de connexion à la base de données
	try {
		$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, 'first_cnx', MDP_FIRST_CNX);
	}
	catch (Exception $e) {
		die(include('prob_tech.php'));
	}
			
	$rq_bloqued_ip = "
	SELECT id
	FROM admin_sig.blocage_ip
	WHERE ip = :ip";
	$res_bloqued_ip = $bdd->prepare($rq_bloqued_ip);
	$res_bloqued_ip->execute(array('ip'=>$_SERVER['REMOTE_ADDR']));
	
	If ($res_bloqued_ip->rowCount() > 2) {
		$_SESSION['bloqued_ip'] = true;
	} Else {
		$_SESSION['bloqued_ip'] = false;
	}
	
	If (AUTO_CONN == 'oui') {
		$IPs_CEN = explode(';', IPs_CEN);
		If ((substr($_SERVER["REMOTE_ADDR"], 0, strlen(IPs_LOC)) != IPs_LOC) && !in_array($_SERVER["REMOTE_ADDR"], $IPs_CEN)){
			exit();
		}
	}

	If (!$_SESSION['bloqued_ip'] && AUTO_CONN == 'oui' && !isset($_SESSION['login']) && (!isset($_GET['do']) Or $_GET['do'] <> 'new_cnx')) {		
		$headers = apache_request_headers(); 	// Récupération de l'entête client
		If($headers['Authorization'] == NULL) {				//si l'entete autorisation est inexistante
			header( "HTTP/1.0 401 Unauthorized" );			//envoi au client le mode d'identification
			header( "WWW-Authenticate: NTLM" );			//dans notre cas le NTLM
			exit;							//on quitte
		}
		
		If(isset($headers['Authorization'])) {				//dans le cas d'une authorisation (identification)
			If(substr($headers['Authorization'],0,5) == 'NTLM ') { 	// on vérifie que le client soit en NTLM
				$chaine=$headers['Authorization']; 
				$chaine=substr($chaine, 5); 			// recuperation du base64-encoded type1 message
				$chained64=base64_decode($chaine);		// decodage base64 dans $chained64
		 
				If(ord($chained64{8}) == 1) {					
						$retAuth = "NTLMSSP".chr(000).chr(002).chr(000).chr(000).chr(000).chr(000).chr(000).chr(000);
						$retAuth .= chr(000).chr(040).chr(000).chr(000).chr(000).chr(001).chr(130).chr(000).chr(000);
						$retAuth .= chr(000).chr(002).chr(002).chr(002).chr(000).chr(000).chr(000).chr(000).chr(000);
						$retAuth .= chr(000).chr(000).chr(000).chr(000).chr(000).chr(000).chr(000);
			 
						$retAuth64 =base64_encode($retAuth);		// encode en base64
						$retAuth64 = trim($retAuth64); 			// enleve les espaces de debut et de fin
						header( "HTTP/1.0 401 Unauthorized" ); 		// envoi le nouveau header
						header( "WWW-Authenticate: NTLM $retAuth64" );	// avec l'identification supplémentaire
						exit;
				} Else If(ord($chained64{8}) == 3) {
					$lenght_login = (ord($chained64[39])*256 + ord($chained64[38])); // longueur du login.
					$offset_login = (ord($chained64[41])*256 + ord($chained64[40])); // position du login.
					$loginindex = str_replace("\0","",substr($chained64, $offset_login, $lenght_login)); // decoupage du login
					
					If ( $loginindex != NULL) {
						$_SESSION['login']=$loginindex;
					}
				}
			}
		}
	}

	If ($browser <> "" && isset($browser) && isset($_GET['do']) && $_GET['do'] == 'new_cnx') {
		unset($_SESSION['login']);
		unset($_SESSION['mdp']);
		unset($_SESSION['mdp2']);
		define('USER', '');
		define('PWD', '');
		define('PWD2', '');
	}
	
	If ($browser <> "" && isset($browser) && isset($_GET['disconnect']) && $_GET['disconnect'] == 'true') {
		unset($_SESSION['back']);
	}
		
	If (!$_SESSION['bloqued_ip'] && isset($browser) && $browser <> "" && !isset($_SESSION['mdp'])) {
		// Si on a dépassé le temps de blocage
		If(isset($_SESSION['nombre']) && $_SESSION['timestamp_limite'] < time()) {
			// Destruction des variables de session
			unset($_SESSION['nombre']);
			unset($_SESSION['timestamp_limite']);
		}

		// Si le formulaire est correctement rempli 
		
			// Si la variable de session qui compte le nombre de soumissions n'existe pas
			If(!isset($_SESSION['nombre'])) {
				// Initialisation de la variable 
				$_SESSION['nombre'] = 0;
				// Blocage pendant 10 min
				$_SESSION['timestamp_limite'] = time() + 60*10;
			}
			// Si on n'essaye pas de nous attaquer par force brute 
			If ($_SESSION['nombre'] <= 5) {				
				If ((isset($_GET['do']) && $_GET['do'] == 'new_cnx') or isset($_POST['pwd'])) {
					unset($_SESSION['login']);
					If (isset($_POST['pwd'])) {
						$test_pwd = strip_tags(md5($_POST['pwd']));
						$test_login = strip_tags($_POST['login']);
						
						$rq_pwd = "
						SELECT 
							mdp_cen, 
							mdp_ovh, 
							count(mdp_cen) as count
						FROM 
							admin_sig.utilisateurs 
						WHERE utilisateur_id = :utilisateur_id 
						GROUP BY mdp_cen, mdp_ovh";
						$res_pwd = $bdd->prepare($rq_pwd);
						$res_pwd->execute(array('utilisateur_id'=>$test_login));
						$pwd = $res_pwd->fetch();
						
						If ($res_pwd->rowCount() == 1 && (md5(base64_decode($pwd['mdp_cen'])) == $test_pwd || md5(base64_decode($pwd['mdp_ovh'])) == $test_pwd)) {
							$_SESSION['login'] = strip_tags($_POST['login']);
						} Else {
							unset($_SESSION['login']);
						}
					}
				}
					
				If (isset($_SESSION['login'])) {
					$rq_ident = "
					SELECT 
						mdp_cen,
						mdp_ovh,
						usename, 
						individu_id 
					FROM 
						pg_catalog.pg_user t1, 
						admin_sig.utilisateurs t2 
					WHERE 
						t2.oid = t1.usesysid 
						AND utilisateur_id = :utilisateur_id";
					$res_ident = $bdd->prepare($rq_ident);
					$res_ident->execute(array('utilisateur_id'=>$_SESSION['login']));
					If ($res_ident->rowCount() == 1) {
						$ident = $res_ident->fetch();
						$_SESSION['user'] = $ident['usename'];
						If (substr($_SERVER["REMOTE_ADDR"], 0, strlen(IPs_LOC)) == IPs_LOC) {
							$_SESSION['mdp'] = base64_decode($ident['mdp_cen']);
						} Else {
							$_SESSION['mdp'] = base64_decode($ident['mdp_ovh']);
						}
						$_SESSION['mdp2'] = base64_decode($ident['mdp_ovh']);
						defined('USER') or define('USER', $_SESSION['user']);
						defined('PWD') or define('PWD', $_SESSION['mdp']);
						defined('PWD2') or define('PWD2', $_SESSION['mdp2']);
						$individu_id = $ident['individu_id'];
						$rq_group = "WITH RECURSIVE t(oid) AS (
						VALUES ((SELECT oid FROM admin_sig.utilisateurs WHERE utilisateur_id = :utilisateur_id))
						UNION ALL
						SELECT grosysid
						FROM pg_catalog.pg_group JOIN t ON (t.oid = ANY (grolist))
						)
						SELECT array_to_string(array_agg(DISTINCT groname), ',') as groname
						FROM pg_catalog.pg_group JOIN t ON (t.oid = pg_group.grosysid)";
						$res_group = $bdd->prepare($rq_group);
						$res_group->execute(array('utilisateur_id'=>$_SESSION['login']));
						$group = $res_group->fetch();						
						$_SESSION['grp'] = explode(',', $group['groname']);
						
						$rq_prenom = "SELECT prenom FROM personnes.d_individu WHERE individu_id = :individu_id";
						$res_prenom = $bdd->prepare($rq_prenom);
						$res_prenom->execute(array('individu_id'=>$individu_id));
						$prenom = $res_prenom->fetch();
						$_SESSION['prenom'] = $prenom['prenom'];
												
						$rq_mail = "SELECT mail, conn_date FROM admin_sig.utilisateurs WHERE individu_id = :individu_id";
						$res_mail = $bdd->prepare($rq_mail);
						$res_mail->execute(array('individu_id'=>$individu_id));
						$mail = $res_mail->fetch();
						$_SESSION['email'] = $mail['mail'];
						
						$conn_date = $mail['conn_date'];
						$conn_date = str_replace(array('{','}'), array('',''),  $conn_date);
						$conn_date = preg_split("/,/", $conn_date);
						$aujourdhui = date('Y-m-d');
						
						// Enregistrement des 10 dernières dates de connexions dans le champ conn_date de la table admin_sig.utilisateurs (Attention, les droits UPDATE ont été donnés au rôle first_cnx)
						If ($conn_date[count($conn_date)-1] != $aujourdhui) {
							$new_conn_date = "{";
							If (count($conn_date) == 10) {
								$p = 1;
							} Else {
								$p = 0;
							}
							For ($i=$p; $i<count($conn_date); $i++) {
								$new_conn_date .= '"' . str_replace('"', '', $conn_date[$i]) . '",'; 
							}
							
							$new_conn_date .= '"'.$aujourdhui.'"}';
							
							$rq_update_conn_date = "UPDATE admin_sig.utilisateurs SET conn_date = :conn_date WHERE utilisateur_id = :utilisateur_id";
							$res_update_conn_date = $bdd->prepare($rq_update_conn_date);
							$res_update_conn_date->execute(array('conn_date'=>$new_conn_date, 'utilisateur_id'=>$_SESSION['login']));
						}
						
						If (isset($_SESSION['back']) && count($_SESSION['back']) > 0) {							
							$lien = end($_SESSION['back']);
							If (!strrpos($lien, $_SERVER['PHP_SELF'])) {
							?>
								<script>
									document.location='<?php echo end($_SESSION['back']);?>'
								</script>
							<?php
							}
						}			
					}
				}
			} Else {
				$rq_insert_bloqued_ip = "
				INSERT INTO admin_sig.blocage_ip (ip) VALUES (:ip)";
				$res_insert_bloqued_ip = $bdd->prepare($rq_insert_bloqued_ip);
				$res_insert_bloqued_ip->execute(array('ip'=>$_SERVER['REMOTE_ADDR']));
			}
		If (!isset($_SESSION['login']) && isset($_POST['pwd']) && isset($_POST['login'])) {
			$_SESSION['nombre']++;
		}
	}
	$bdd = null;
?>
