/**************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
Copyright Conservatoire d’espaces naturels de Savoie, 2018
-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
-	Adresse électronique : sig at cen-savoie.org

Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
-	de gérer et suivre ces actes (convention et acquisition) ;
-	et d’en établir des bilans.
-	Un module permet de réaliser des sessions d’animation foncière.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
***************************************************************************************************************************
***************************************************************************************************************************
**************************************************************************************************************************/

$(function(){
	// Les procédures suivantes se lancent dès que l'ascenseur verticale est utilisé (y compris la molette de la sousris)
	$(window).scroll(function(){	
		if($(window).scrollTop()<350) { // Si l'ascenseur masque moins de 350 pixels alors ...
			if ($('.tbl_entete_fixed').length) { // Si une entête fixe existe alors ...
				$('.tbl_entete_fixed').fadeOut(50); // On cahce cette entête fixe
			}
		} else { // Si l'ascenseur masque plus de 350 pixels alors ...
			if ($('.tbl_entete_fixed:visible').length == 0) { // Si l'entête fixe n'est pas encore visible
				// L'objectif de cette partie est d'adapter la largeur de chaque cellule de l'entête en fonction de la largeur des cellules du tableau correspondantes.
				tbl_width = []; // On initialise une variable tableau pour stocker les largeurs de chaque cellule 
				var tbl_entete; // Variable pour identifier l'entête fixe
				$('thead').each( function(){ // Pour chaque tbody de la page
					if ($(this).attr("class") == 'tbl_entete_fixed' && $('#'+ $(this).next().attr("id")).is(":visible")) { // Si le tbody correspond à l'entête fixe et que le prochain tbody est visible alors...
						tbl_entete = this.id; // On identifie l'entête fixe
						$('#'+ $(this).next().attr("id") + ' > tr:visible:eq(3) > td').each(function(){ // Pour chaque cellule de la troisième ligne du tableau du tbody après celui de l'entête fixe
							tbl_width.push($(this).width()); // On stock la largeur de chaque cellule
						});
					}			
				});
				var j = 0;
				$('#' + tbl_entete + ' > tr:first-child > th').each(function(){ // Pour chaque cellule de l'entête fixe
					$(this).width(tbl_width[j]); // on adapte la largeur identifiée précédement
					j++;
				});				
			}
			
			if($('#'+tbl_entete).length) { // Si il y a une entête fixe alors ...
				$('#'+tbl_entete).fadeIn(800); // on l'affiche
				$('#'+tbl_entete).width($('#main').width()-50)
			}
		}
	});
});

// Les procédure suivantes sont actives sur toutes les pages où la page foncier.js est appelée.

function autoChecked(elt1, elt2) {
	setTimeout(function(){
		if($('#'+elt1).val() != 'N.D.' &&  $('#'+elt1).val() != ''){
			$('#'+elt2).prop('checked', 'checked')
		}
	}, 500);
	
}

 function hoverOnRowspan(type, hoverclass) {
	$('.' + hoverclass + ' > td').each(function(){
		$(this).css('font-weight', 'bold');
		$(this).find('a').each(function(){
			$(this).css('font-size', '8pt');
		});
		if ($(this).attr('class').split('first').length > 1 || $(this).attr('rowspan') > 1) {
			$(this).css('border-top', 'solid 2px #f98d00');
		}
		if ($(this).attr('class').split('last').length > 1 || $(this).attr('rowspan') > 1) {
			$(this).css('border-bottom', 'solid 2px #f98d00');
		}
	});
 }
 
  function hoverOutRowspan(type, hoverclass) {
	$('.' + hoverclass + ' > td').each(function(){
		$(this).css('font-weight', 'normal');
		$(this).find('a').each(function(){
			$(this).css('font-size', '10pt');
		});
		if ($(this).attr('class').split('first').length > 1 || $(this).attr('rowspan') > 1) {
			$(this).css('border-top', 'solid 2px ' + $(this).css('backgroundColor'));
		}
		if ($(this).attr('class').split('last').length > 1 || $(this).attr('rowspan') > 1) {
			$(this).css('border-bottom', 'solid 2px ' + $(this).css('backgroundColor'));
		}
	});
 }

function slideBilanCommDetail(onglet, id, nbr_site) {
	var my_tr_com  = 'tr_'+onglet+'_'+id;
	var my_tr_site_first = 'tr_site_'+id+'_0';
	
	if (document.getElementById(my_tr_site_first).style.display == 'none') { // si les tr site sont cachés on les affiche
		if ($('#avertissement_'+onglet).css('display') == 'none') {
			$('#avertissement_'+onglet).show(400).delay(15000).fadeOut(800);
		}
		$('#'+my_tr_com+' td:first').css({
			'border-bottom-left-radius': 0
		});
		$('#'+my_tr_com+' td:last').css({
			'border-bottom-right-radius': 0
		});
		
		for (var i=0; i < nbr_site; i++) { //pour chaque site
			var my_tr_site  = 'tr_site_'+id+'_'+i;
			$('#'+my_tr_site).css('display', ''); //methode slide d'apparation
		}
	} else {
		$('#'+my_tr_com+' td:first').css({
			'border-bottom-left-radius': 20
		});
		$('#'+my_tr_com+' td:last').css({
			'border-bottom-right-radius': 10
		});
		for (var i=0; i < nbr_site; i++) { //pour chaque site
			var my_tr_site  = 'tr_site_'+id+'_'+i;
			$('#'+my_tr_site).css('display', 'none'); //methode slide d'apparation
		}
	}
}

function chgReconduction(nb_max) {
	if ($('#hid_tbl_grant_elt_form_id').length) {
		var tbl_grant_elt_form_id = $('#hid_tbl_grant_elt_form_id').val().split(',');
		
		if ($('#acte_chk_reconduction').is(':checked')) {		
			if ($.inArray('acte_pas_tps', tbl_grant_elt_form_id) > -1) {
				$("#div_acte_pas_tps").removeClass('readonly');
				$("#acte_pas_tps").removeClass('readonly');
				$('#acte_pas_tps').prop('disabled', false );
				$('#acte_pas_tps').prop('readonly', false );
				if ($("#acte_pas_tps").val() == '' || $("#acte_pas_tps").val() == ('N.D.')) {		
					$("#acte_pas_tps").val('132');
				}
			}
			if ($.inArray('acte_date_rappel', tbl_grant_elt_form_id) > -1) {
				$("#div_acte_date_rappel").removeClass('readonly');
				$("#acte_date_rappel").removeClass('readonly');
				$('#acte_date_rappel').prop('disabled', false );
				$('#acte_date_rappel').prop('readonly', false );
			}
			if ($.inArray('acte_recond_nbmax', tbl_grant_elt_form_id) > -1) {
				$("#div_acte_recond_nbmax").removeClass('readonly');
				$("#acte_recond_nbmax").removeClass('readonly');
				$('#acte_recond_nbmax').prop('disabled', false );
				$('#acte_recond_nbmax').prop('readonly', false );
				$('#acte_lbl_infini').css('display', '' );
				$('#acte_lbl_ou').css('display', '' );		
				$('#acte_chk_infini').css('display', '' );
				$('#acte_chk_infini').prop('disabled', false );
				$('#acte_chk_infini').prop('readonly', false );
				if (nb_max == '-1') {
					$('#acte_chk_infini').prop('checked', true );
					$("#acte_recond_nbmax").val('\u221e');
				}
				if ($('#acte_chk_infini').is(':checked')) {
					$('#acte_recond_nbmax').prop('disabled', true );
				} else {
					$('#acte_recond_nbmax').prop('disabled', false );
				}
				
				if ($("#acte_recond_nbmax").val() == '' || $("#acte_recond_nbmax").val() == ('N.D.')) {		
					$("#acte_recond_nbmax").val('');
				}
			}		
		} else {
			$('#acte_lbl_infini').css('display', 'none' );
			$('#acte_lbl_ou').css('display', 'none' );
			$('#acte_pas_tps').prop('disabled', true );
			$('#acte_pas_tps').prop('readonly', true );
			$("#acte_pas_tps").val('N.D.');
			$("#div_acte_pas_tps").addClass('readonly');
			$("#acte_pas_tps").addClass('readonly');
			$('#acte_date_rappel').prop('disabled', true );
			$('#acte_date_rappel').prop('readonly', true );
			$("#acte_date_rappel").val('N.D.');
			$("#div_acte_date_rappel").addClass('readonly');
			$("#acte_date_rappel").addClass('readonly');
			$('#acte_recond_nbmax').prop('disabled', true );
			$('#acte_recond_nbmax').prop('readonly', true );
			$("#acte_recond_nbmax").val('N.D.');
			$("#div_acte_recond_nbmax").addClass('readonly');
			$("#acte_recond_nbmax").addClass('readonly');
			$('#acte_chk_infini').css('display', 'none' );
			$('#acte_chk_infini').prop('checked', false );
		}
	}
}

/* Cette fonction permet d'adapter la saisie du formulaire lorsque le statut de l'acte change. */
function chgStatut(old_value=null) {	
	if ($('#hid_tbl_grant_elt_form_id').length) {
		var tbl_grant_elt_form_id = $('#hid_tbl_grant_elt_form_id').val().split(',');
		var elt_class;
		if ($('#need_acte_statut').val() == 0) {
			if ($.inArray('acte_date_signature', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_signature').prop('disabled', true );
				if (($('#acte_date_signature').val() == 'N.P.') || ($('#acte_date_signature').val() == 'N.D.')) {
					$('#acte_date_signature').val('N.P.')
				}
				$('#acte_date_signature').prop('readonly', true );
			}
			
			$('#tr_date_signature').hide();
			if($('#acte_date_debut').length && $.inArray('acte_date_debut', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_debut').prop('disabled', false );
				$('#acte_date_debut').attr('class', $('#acte_date_debut').attr('class').replace('readonly', ''));
			}
			if($('#tr_date_debut').length) {
				$('#tr_date_debut').show();			
			}
			if($('#acte_date_fin').length && $.inArray('acte_date_fin', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_fin').prop('disabled', true );
				$('#acte_date_fin').val('N.P.');
				$('#acte_date_fin').attr('class', $('#acte_date_fin').attr('class') + ' readonly');
			}
			if($('#tr_date_fin').length) {
				$('#tr_date_fin').hide();
			}
		} else if ($('#need_acte_statut').val() == 1 || $('#need_acte_statut').val() == 2) {
			if (old_value == 0 && $.inArray('acte_date_signature', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_signature').val('');
				$('#img_acte_date_signature').css('display', 'none');
				$('#div_acte_date_signature').removeClass('readonly')
				$('#acte_date_signature').prop('readonly', false );
				$('#acte_date_signature').prop('disabled', false );			
			}
			if ($.inArray('acte_date_signature', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_signature').val($('#acte_date_signature').val().replace('N.P.', 'N.D.'))
				if ($('#acte_date_signature').val() == 'N.D.') {$('#acte_date_signature').prop('disabled', false );}
				$('#acte_date_signature').prop('readonly', false );
				$('#acte_date_signature').prop('disabled', false );
			}
			$('#tr_date_signature').show();
			if($('#acte_date_debut').length && $.inArray('acte_date_debut', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_debut').val($('#acte_date_debut').val().replace('N.P.', 'N.D.'))
				if ($('#acte_date_debut').val() == 'N.D.') {$('#acte_date_debut').prop('disabled', false );}
				$('#acte_date_debut').removeClass('readonly');
			}
			if($('#tr_date_debut').length) {
				$('#tr_date_debut').show();
			}
			if($('#acte_date_fin').length && $.inArray('acte_date_fin', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_fin').prop('disabled', true );
				$('#acte_date_fin').val('N.P.');
				$('#acte_date_fin').addClass('readonly');
			}
			if($('#tr_date_fin').length) {
				$('#tr_date_fin').hide();
			}
		} else if ($('#need_acte_statut').val() == 3) {	
			$('#acte_date_signature').prop('disabled', true );
			$('#acte_date_signature').val('N.P.')
			$('#acte_date_signature').addClass('readonly');
			$('#tr_date_signature').hide();
			if($('#acte_date_debut').length) {
				$('#acte_date_debut').prop('disabled', true );
				$('#acte_date_debut').val('N.P.')
				$('#acte_date_debut').addClass('readonly');
			}
			if($('#tr_date_debut').length) {
				$('#tr_date_debut').hide();
			}		
			if($('#acte_date_fin').length) {
				$('#acte_date_fin').prop('disabled', true );
				$('#acte_date_fin').val('N.P.')
				$('#acte_date_fin').addClass('readonly');
			}
			if($('#tr_date_fin').length) {
				$('#tr_date_fin').hide();
			}
		} else if ($('#need_acte_statut').val() == 4) {	
			$('#acte_date_signature').val($('#acte_date_signature').val().replace('N.P.', 'N.D.'))
			if ($('#acte_date_signature').val() == 'N.D.') {$('#acte_date_signature').prop('disabled', false );}
			if ($.inArray('acte_date_signature', tbl_grant_elt_form_id) > -1) {$('#acte_date_signature').removeClass('readonly');}
			$('#tr_date_signature').show();
			if($('#acte_date_debut').length && $.inArray('acte_date_debut', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_debut').val($('#acte_date_debut').val().replace('N.P.', 'N.D.'))
				if ($('#acte_date_debut').val() == 'N.D.') {$('#acte_date_debut').prop('disabled', false );}
				$('#acte_date_debut').removeClass('readonly');
			}
			if($('#tr_date_debut').length) {
				$('#tr_date_debut').show();
			}
			if($('#acte_date_fin').length && $.inArray('acte_date_fin', tbl_grant_elt_form_id) > -1) {
				$('#acte_date_fin').val($('#acte_date_fin').val().replace('N.P.', 'N.D.'))
				if ($('#acte_date_fin').val() == 'N.D.') {$('#acte_date_fin').prop('disabled', false );}
				$('#acte_date_fin').removeClass('readonly');
			}
			if($('#tr_date_fin').length) {
				$('#tr_date_fin').show();
			}
		}
		resize_fieldset();
	}
}

/* Cette fonction permet d'associer ou de dissocier des parcelles à un acte. Elle se lance lors de l'édition de la liste des parcelles d'un acte au clic sur les bouton "<<<" ou ">>>".
En plus de passer une ou plusieurs parcelles d'une liste à l'autre, elle met également à jour des intput hidden utilisés dans les fonctions lancées par la validiation de la mise à jour de la liste des parcelles associées à l'acte. */
function parcAjoutSuppr(frm, mode) {	
	var parc_dispo_selected = $('#parc_dispo option:selected');
	var parc_assoc_acte_selected = $('#parc_assoc_acte option:selected');
	var parc_assoc_acte_not_selected = $('#parc_assoc_acte option:not(:selected)');
	var parc_ajout = document.getElementById('parc_ajout').value;
	var parc_suppr = document.getElementById('parc_suppr').value;
	var new_class
	$('#current_parc_conv').val('NON')
	if (mode == 'ajout') {
		
		for (i = 0; i < parc_dispo_selected.length; i++) {
			new_style = "style='color:#004494;'";
			if (parc_dispo_selected[i].text.substring((parc_dispo_selected[i].text.length - 10)) == 'Convention') {
				$('#current_parc_conv').val('OUI')
				new_style = "style='color:#d77a0f;'";
			}
			
			if (parc_suppr.lastIndexOf(parc_dispo_selected[i].value+';') == -1) {
				parc_ajout = parc_ajout + parc_dispo_selected[i].value+';';
				new_class = 'class="parc_evidence"';
			} else {
				$("#parc_suppr").val(parc_suppr.replace(parc_dispo_selected[i].value+';', ''));
				new_class = '';
			}
			$('#parc_assoc_acte').prepend($('<option ' + new_style + ' ' + new_class + '  value="' + parc_dispo_selected[i].value + '">' + parc_dispo_selected[i].text + '</option>'));
		}	
		$("#parc_dispo option:selected").remove();
		$("#parc_ajout").val(parc_ajout);
		if ($('#parc_assoc_acte option').size() == 0) {
			$('#btn_parcModify').css('display', 'none');
			$('#lbl_parcModify').css('display', '');
		} else {
			$('#btn_parcModify').css('display', '');
			$('#lbl_parcModify').css('display', 'none');
		}
	} else if (mode == 'suppr') {
		
		for (i = 0; i < parc_assoc_acte_selected.length; i++) {
			new_style = "style='color:#004494;'";
			if (parc_assoc_acte_selected[i].text.substring((parc_assoc_acte_selected[i].text.length - 10)) == 'Convention') {
				new_style = "style='color:#d77a0f;'";
			}			
			if (parc_ajout.lastIndexOf(parc_assoc_acte_selected[i].value+';') == -1) {
				parc_suppr = parc_suppr + parc_assoc_acte_selected[i].value+';';
				new_class = 'class="parc_evidence"';
			} else {
				$("#parc_ajout").val(parc_ajout.replace(parc_assoc_acte_selected[i].value+';', ''));
				new_class = '';
			}
			$('#parc_dispo').prepend($('<option ' + new_style + ' ' + new_class + '  value="' + parc_assoc_acte_selected[i].value + '">' + parc_assoc_acte_selected[i].text + '</option>')); 
		}	
		for (i = 0; i < parc_assoc_acte_not_selected.length; i++) {
			if (parc_assoc_acte_not_selected[i].text.substring((parc_assoc_acte_not_selected[i].text.length - 10)) == 'Convention') {
				$('#current_parc_conv').val('OUI')
			}
		}	
		
		$("#parc_assoc_acte option:selected").remove();
		$("#parc_suppr").val(parc_suppr);
		if ($('#parc_assoc_acte option').size() == 0) {
			$('#btn_parcModify').css('display', 'none');
			$('#lbl_parcModify').css('display', '');
		} else {
			$('#btn_parcModify').css('display', '');
			$('#lbl_parcModify').css('display', 'none');
		}
	}
}

/* Fonction utilisée principalement dans les pages 'fiche_parcelle.php' et 'fiche_proprio.php'
Elle permet de dérouler on d'enrouler le contenu d'une div 
mais aussi lors de multiple éléments scrollable de tout afficher/masquer en un clic 
------------Les variables
------------div -> Permet d'identifier la div sur laquelle s'applique la fonction mais aussi de savoir si on déroule ou on enroule tout*/
function slideIn(div) {
	if (div.substring(0,4) == 'ALL_') { //on verifie que le paramètre passé pour savoir s'il faut tout enrouler/dérouler
		var TabDivs=new Array() //variable tableau stockant les objets div
		TabDivs=document.getElementsByTagName('DIV') //remplissage de la variable tableau
		for(i=0;i<TabDivs.length;i++){ //pour chaque balise div identifiée
			if (TabDivs[i].id.substring(0,4) == 'sld_') { //on verifie que l'id commence par 'sld_' ce qui permet de savoir qu'elle doit être enroulée/déroulée
				if (div == 'ALL_IN') { //on regarde s'il faut dérouler
					$('#'+TabDivs[i].id).slideDown(800);
				} else if (div == 'ALL_OUT') { //ou au contraire enrouler
					$('#'+TabDivs[i].id).slideUp(800);
				}
			}
		}
		/* Ici on change les paramètres de la fonction appelée pour faire l'inverse au prochain appel */
		if (div == 'ALL_IN') {
			document.getElementById('tbl_collapse').innerHTML = "<tr><td width='100%'><label class='label' onclick=\"slideIn('ALL_OUT');\">Tout replier</label></td></tr>"
		} else if (div == 'ALL_OUT') {
			document.getElementById('tbl_collapse').innerHTML = "<tr><td width='100%'><label class='label' onclick=\"slideIn('ALL_IN');\">Tout d&eacute;plier</label></td></tr>"
		}
	} else if (div.substring(0,14) == 'ancien_proprio') {
		if (document.getElementById(div).style.display == 'none') {
			$('#'+div).show();
			document.getElementById(div+'_slideIn').innerHTML = "<label style='text-align:right;cursor:pointer;text-shadow:1px 1px 2px #ebebeb;color:#cf740d;'  class='label' onclick=\"slideIn('"+div+"');\">Masquer les anciens propri&eacute;taires</label>"
		} else {
			$('#'+div).hide();
			document.getElementById(div+'_slideIn').innerHTML = "<label style='text-align:right;cursor:pointer;text-shadow:1px 1px 2px #ebebeb;color:#cf740d;'  class='label' onclick=\"slideIn('"+div+"');\">Afficher les anciens propri&eacute;taires</label>"
		}	
	} else { //si le paramètre ne commence pas par 'ALL_' alors il ne faut enrouler/dérouler q'une seule div
		if (document.getElementById(div).style.display == 'none') {
			$('#'+div).slideDown(800);
		} else {
			$('#'+div).slideUp(800);
		}
	}
}

function change_style_lettre(id) {
	span = document.getElementById('lettre_all');
	if (span != null) {
		span.style.fontSize='10pt';
		span.style.fontWeight='normal';
	}
	var alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
	var length = alphabet.length;
	for (i = 0; i < length; i++) {
		span = document.getElementById('lettre_'+alphabet[i]);
		span.style.fontSize='10pt';
		span.style.fontWeight='normal';
	}
	current_span = document.getElementById('lettre_'+id);
	current_span.style.fontSize='11pt';
	current_span.style.fontWeight='bold';
}

function filter_cmb_commune(cmb_name, flt) {
	var firstCom = 0;
	var i = 0;
	flt_dept = $('#lst_departements').val();
	var flt_lettre;
	if (flt == 'DEPT') {
		$('#alphabet').find('span').each(function(){
			if ($(this).attr('style') && $(this).attr('style').split('bold').length > 1) {
				flt_lettre = $(this).attr('id').replace('lettre_', '').toUpperCase();
			} else {
				flt_lettre = 'ALL'
			}
		});
	} else {
		flt_lettre = flt.toUpperCase();
	}
	
	$('#'+cmb_name+" option").each( function(){
		i++;
		
		if(flt_lettre && (($(this).text().substring(0, flt_lettre.length).toUpperCase() != flt_lettre.toUpperCase() && flt_lettre != 'ALL' && flt_lettre != 'DEPT') || ($(this).val().substring(0, flt_dept.length).toUpperCase() != flt_dept.toUpperCase() && flt_dept != '-1')) && $(this).val() != '-1') {
			$(this).css('display', 'none');
		} else {
			$(this).css('display', '');
			if (firstCom == 0 && flt_lettre != 'ALL') {
				firstCom = i;
				firstCom = 0;
			}
		}
	});
	
	$('#'+cmb_name+' option:eq('+firstCom+')').prop('selected', true);
	if ($('#'+cmb_name+' option:selected').val() != '-1') {
		create_search_parc(0);
	} else {
		document.getElementById('tbl_parc_corps').innerHTML = "";
	}
}

function chk_infini() {
	if ($('#acte_chk_infini').is(':checked')) {
		$('#acte_recond_nbmax').prop('disabled', true );
		$('#acte_recond_nbmax').prop('readonly', true );
		$('#acte_recond_nbmax').attr('class', $('#acte_recond_nbmax').attr('class') + ' readonly');
		$('#acte_recond_nbmax').val('\u221e');
	} else {
		$('#acte_recond_nbmax').prop('disabled', false );
		$('#acte_recond_nbmax').prop('readonly', false );
		$('#acte_recond_nbmax').attr('class', $('#acte_recond_nbmax').attr('class').replace('readonly', ''));
		$('#acte_recond_nbmax').val('');
		$('#acte_recond_nbmax').focus();
	}
}

function updateSurfmu(contenance, cad_site_id, mu_id) {
	$('#img_'+cad_site_id).prop('src', $('#img_'+cad_site_id).prop('src').replace('edit.png', 'enregistrer.png'));
	$('#img_'+cad_site_id).attr('onclick', "updateSurfmuConfirm('"+contenance+"', '"+cad_site_id+"', '"+mu_id+"');");
	$('#surfmu_'+cad_site_id).css('background', '#ffffff');	
	$('#surfmu_'+cad_site_id).prop('readonly', false );
	$('#surfmu_'+cad_site_id).prop('disabled', false );
}

/*
  *********************************************
  *********************************************
  ****    IIIIII IIIIII IIIIII  I    I     ****
  ****    I    I    I   I    I   I  I      ****
  ****    IIIIII    I   IIIIII    II       ****
  ****    I    I  I I   I    I   I  I      ****
  ****    I    I  III   I    I  I    I     ****
  *********************************************
  ********************************************* 

  LES FONCTIONS SUIVANTES SONT BASEES SUR AJAX 
  ELLES PERMETTENT D'ADAPTER LE CONTENU DES PAGES
DYNAMIQUEMENT EN FONCTION DES CHOIX DE L'UTILISATEUR
            ET SANS RECHARGER LA PAGE
			
Elles fonctionnent toujours sur le même principe et sur le triptique PHP/JavaScipt/Ajax
A partir d'informations issues de la page elle-même et eventuellement d'autres informtions
filtrantes issues d'un formulaire HTML on adapte le contenu des pages
L'avantage principal est de pouvoir lancer ou relancer une requête SQL en fonction des informations choisies par l'utilisateur et de les afficher SANS RECHARGER LA PAGE
1ère partie on récupère toutes les informations nécessaires sur la page initiale
2ème partie on lance via Ajax une fonction php qui retourne du code (les fonctions PHP appelées dans cette partie sont toutes situées dans la page 'foncier_fct_ajax.php'
3ème partie ce code est utilisé pour adapter le contenu de la page initiale
 */
 
/* Cette fonction est utilisée dans la page foncier_liste_parcelle.php uniquement
Elle permet d'afficher une liste de parcelle en fonction des choix de l'utilisateur 
------------Les variables
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats*/
function create_search_parc(cur_offset){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var departement_sel //variable stockant le département séléctionnée dans la liste déroulante
	var commune_sel //variable stockant la commune séléctionnée dans la liste déroulante
	var flt_site_id //variable stockant l'id du site
	var flt_section //variable stockant la section
	var flt_par_num //variable stockant un numéro de parcelle
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if (document.getElementById('lst_departements') != null) {
		departement_sel = document.getElementById('lst_departements').value;
	} else {
		departement_sel = -1;
	}
	if (document.getElementById('lst_communes') != null) {
		commune_sel = document.getElementById('lst_communes').value;
	} else {
		commune_sel = -1;
	}
	if (document.getElementById('filtre_site_id') != null) {
		flt_site_id = document.getElementById('filtre_site_id').value;
	} else {
		flt_site_id = "";
	}
	if (document.getElementById('filtre_section') != null) {
		flt_section = document.getElementById('filtre_section').value;
	} else {
		flt_section = "";
	}
	if (document.getElementById('filtre_par_num') != null) {
		flt_par_num = document.getElementById('filtre_par_num').value;
	} else {
		flt_par_num = "";
	}
	
	//2ème partie basée sur Ajax
	$.ajax({ 
		url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		data      :{ 
			fonction:'fct_search_parc', params: {departement: departement_sel, commune: commune_sel, site_id: flt_site_id, section: flt_section, par_num: flt_par_num, offset: cur_offset} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
		},
		success: function(data) { //On récupère du code dans la variable data
			document.getElementById('tbl_parc_corps').innerHTML = data; //la page est adaptée à partir de la variable 'data'
		}
	});
}

/* Cette fonction est utilisée dans les pages fiche_proprio.php fiche_site.php fiche_mfu.php
Elle permet d'afficher une liste de parcelle concernées par la pages en cours
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------passage->paramètre permettant de savoir si la page vient d'être chargée ou si elle vient d'être filtrée
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------flt_champ_id->id de l'élément référant de la page (dnuper pour fiche_proprio.php site_id pour fiche_site.php acte_id pour fiche_mfu.php)*/
function create_lst_parc(passage, cur_offset, flt_champ_id){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_site_id //variable stockant l'id du site (utile uniquement dans fiche_proprio.php)
	var flt_section //variable stockant la section
	var flt_par_num //variable stockant un numéro de parcelle
	var flt_commune //variable stockant la commune séléctionnée dans la liste déroulante correspondante
	var flt_typ_prop //variable stockant le type de propriétaire dans la liste déroulante correspondante
	var flt_mfu //variable stockant la mfu en cours sur la parcelle
	var flt_type_acte //variable stockant le type d'acte entre acquisition et convention (utile uniquement dans fiche_mfu.php)
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP ppar Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if (document.getElementById('filtre_site_id') != null && document.getElementById('filtre_site_id').type == 'select-one') {
		flt_site_id = document.getElementById('filtre_site_id').options[document.getElementById('filtre_site_id').selectedIndex].text;
	} else if (document.getElementById('filtre_site_id') != null && document.getElementById('filtre_site_id').type == 'text') {
		flt_site_id = document.getElementById('filtre_site_id').value;
	} else {
		flt_site_id = "";
	}
	if (document.getElementById('filtre_commune') != null) {
		flt_commune = document.getElementById('filtre_commune').options[document.getElementById('filtre_commune').selectedIndex].text;
	} else {
		flt_commune = "";
	}
	if (document.getElementById('filtre_section') != null) {
		flt_section = document.getElementById('filtre_section').value;
	} else {
		flt_section = "";
	}
	if (document.getElementById('filtre_typ_prop') != null) {
		flt_typ_prop = document.getElementById('filtre_typ_prop').options[document.getElementById('filtre_typ_prop').selectedIndex].text;
	} else {
		flt_typ_prop = "";
	}
	if (document.getElementById('filtre_par_num') != null) {
		flt_par_num = document.getElementById('filtre_par_num').value;
	} else {
		flt_par_num = "";
	}
	if (document.getElementById('filtre_mfu') != null) {
		flt_mfu = document.getElementById('filtre_mfu').options[document.getElementById('filtre_mfu').selectedIndex].text;
	} else {
		flt_mfu = "";
	}
	if (document.getElementById('filtre_priorite') != null) {
		flt_priorite = document.getElementById('filtre_priorite').options[document.getElementById('filtre_priorite').selectedIndex].text;
	} else {
		flt_priorite = "";
	}
	if (document.getElementById('filtre_type_acte') != null) {
		flt_type_acte = document.getElementById('filtre_type_acte').value;
	} else {
		flt_type_acte = "";
	}
	if (document.getElementById('mode') != null) {
		mode = document.getElementById('mode').value;
	} else {
		mode = "";
	}
	
	//2ème partie basée sur Ajax
	$.ajax({
		url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_lst_parc', params: {page: flt_page, champ_id: flt_champ_id, site_id: flt_site_id, commune: flt_commune, section: flt_section, par_num: flt_par_num, typ_prop: flt_typ_prop, mfu: flt_mfu, priorite: flt_priorite, type_acte: flt_type_acte, var_mode: mode, offset: cur_offset} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#tbl_parc_corps').html(data['liste']); //la page est adaptée à partir de la variable 'data'
			document.getElementById('tbody_histo').innerHTML = data['histo']; //la page est adaptée à partir de la variable 'data'
			
			/* La procédure qui suit permet de remplir automatiquement les options des filtres en listes déroulantes
			Après verification qu'aucun filtre n'est renseigné et que la page vient d'être chargée on lance la fonction GetComboBoxFilter */
			var flt_null = 'OUI'; //variable permettant de savoir aucun filtre n'est renseigné
			var elements = document.forms['frm_parc_entete'].elements; //variable tableau stockant les éléments 
			for (k=0; k<elements.length; k++){
				if (elements[k].id.substring(0,6) == 'filtre' && ((elements[k].type == 'select-one' && document.getElementById(elements[k].id).value != '-1') || (elements[k].type == 'text' && document.getElementById(elements[k].id).value != ''))) {
					flt_null = 'NON';
				}
			}			
			if (passage == 'charge' && flt_null == 'OUI') {
				GetComboBoxFilter('tbl_parc_corps', 'frm_parc_entete', 'a');
			}
		}
	});
}

/* Cette fonction est utilisée dans les pages foncier_liste_proprio.php fiche_site.php fiche_mfu.php
Elle permet d'afficher une liste de propriétaires concernés par la pages en cours
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------flt_champ_id->id de l'élément référant de la page (NULL pour foncier_liste_proprio.php site_id pour fiche_site.php acte_id pour fiche_mfu.php)*/
function create_lst_proprio(cur_offset, flt_champ_id){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_nom //variable stockant le nom du propriétaire
	var flt_type_acte //variable stockant le type d'acte entre acquisition et convention (utile uniquement dans fiche_mfu.php)
	var flt_site_id //variable stockant l'id du site (utile uniquement dans foncier_liste_proprio.php)
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP ppar Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if (document.getElementById('filtre_nom') != null) {
		flt_nom = document.getElementById('filtre_nom').value;
	} else {
		flt_nom = "";
	}
	if (document.getElementById('filtre_type_acte') != null) {
		flt_type_acte = document.getElementById('filtre_type_acte').value;
	} else {
		flt_type_acte = "";
	}
	if (document.getElementById('filtre_site') != null) {
		flt_site_id = document.getElementById('filtre_site').value;
	} else {
		flt_site_id = "";
	}
	
	//2ème partie basée sur Ajax
	$.ajax({
		url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_lst_proprio', params: {page: flt_page, champ_id: flt_champ_id, nom: flt_nom, site_id: flt_site_id, type_acte: flt_type_acte, offset: cur_offset} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			document.getElementById('tbl_proprio_corps').innerHTML = data['liste']; //la page est adaptée à partir de la variable 'data'
			if (flt_page.lastIndexOf('/foncier_liste_proprio.php') == -1) {
				document.getElementById('tbody_histo').innerHTML = data['histo']; //la page est adaptée à partir de la variable 'data'
			}
		}
	});
}

/* Cette fonction est utilisée dans les pages fiche_parcelle.php fiche_proprio.php fiche_site.php
Elle permet d'afficher une liste de parcelle concernées par la pages en cours
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------passage->paramètre permettant de savoir si la page vient d'être chargée ou si elle vient d'être filtrée
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------flt_champ_id->id de l'élément référant de la page (par_id pour fiche_parcelle.php dnuper pour fiche_proprio.php site_id pour fiche_site.php)*/
function create_lst_mf(passage, cur_offset, flt_champ_id){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_acte //variable stockant le type d'acte entre acquisition et convention
	var flt_site_id_mfu //variable stockant l'id du site (utile uniquement dans fiche_proprio.php)
	var flt_libelle //variable stockant le libellé de l'acte
	var flt_type_mf //variable stockant le type de maitrise
	var flt_date //variable stockant la date de signature de l'acte
	var flt_statut //variable stockant le statut
	var flt_doc_pdf //variable stockant le statut
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP ppar Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	if (document.getElementById('filtre_site_id_mfu') != null && document.getElementById('filtre_site_id_mfu').type == 'select-one') {
		flt_site_id_mfu = document.getElementById('filtre_site_id_mfu').options[document.getElementById('filtre_site_id_mfu').selectedIndex].text;
	} else if (document.getElementById('filtre_site_id_mfu') != null && document.getElementById('filtre_site_id_mfu').type == 'text') {
		flt_site_id_mfu = document.getElementById('filtre_site_id_mfu').value;
	} else {
		flt_site_id_mfu = "";
	}
	if (document.getElementById('filtre_acte') != null) {
		flt_acte = document.getElementById('filtre_acte').options[document.getElementById('filtre_acte').selectedIndex].text;
	} else {
		flt_acte = "";
	}
	if (document.getElementById('filtre_acte_id') != null) {
		flt_acte_id = document.getElementById('filtre_acte_id').value;
	} else {
		flt_acte_id = "";
	}
	if (document.getElementById('filtre_libelle') != null) {
		flt_libelle = document.getElementById('filtre_libelle').value;
	} else {
		flt_libelle = "";
	}
	if (document.getElementById('filtre_type_mf') != null) {
		flt_type_mf = document.getElementById('filtre_type_mf').options[document.getElementById('filtre_type_mf').selectedIndex].text;
	} else {
		flt_type_mf = "";
	}
	if (document.getElementById('filtre_date') != null) {
		flt_date = document.getElementById('filtre_date').value;
	} else {
		flt_date = "";
	}
	if (document.getElementById('filtre_statut') != null) {
		flt_statut = document.getElementById('filtre_statut').options[document.getElementById('filtre_statut').selectedIndex].text;
	} else {
		flt_statut = "";
	}
	if (document.getElementById('filtre_doc_pdf') != null) {
		flt_doc_pdf = document.getElementById('filtre_doc_pdf').options[document.getElementById('filtre_doc_pdf').selectedIndex].text;
	} else {
		flt_doc_pdf = "";
	}
	
	//2ème partie basée sur Ajax
	$.ajax({ 
		url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_lst_mf', params: {page: flt_page, champ_id: flt_champ_id, acte_id: flt_acte_id, acte: flt_acte, site_id_mfu: flt_site_id_mfu, libelle: flt_libelle, type_mf: flt_type_mf, date: flt_date, statut: flt_statut, doc_pdf: flt_doc_pdf, offset: cur_offset} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			document.getElementById('tbl_mf_corps').innerHTML = data['code']; //la page est adaptée à partir de la variable 'data'
			
			/* La procédure qui suit permet de remplir automatiquement les options des filtres en listes déroulantes
			Après verification qu'aucun filtre n'est renseigné et que la page vient d'être chargée on lance la fonction GetComboBoxFilter */
			var flt_null = 'OUI'; //variable permettant de savoir aucun filtre n'est renseigné
			var elements = document.forms['frm_mf_entete'].elements; //variable tableau stockant les éléments 
			for (k=0; k<elements.length; k++){
				if (elements[k].id.substring(0,6) == 'filtre' && ((elements[k].type == 'select-one' && document.getElementById(elements[k].id).value != '-1') || (elements[k].type == 'text' && document.getElementById(elements[k].id).value != ''))) {
					flt_null = 'NON';
				}
			}
			if (passage == 'charge' && flt_null == 'OUI') {
				GetComboBoxFilter('tbl_mf_corps', 'frm_mf_entete', 'a');
			}
			if (data['nbr_result'] < 15) {
				$('#tbl_mf_new_bottom').css('display', 'none');
			} else {
				$('#tbl_mf_new_bottom').css('display', '');
			}
		}
	});
}


/* Cette fonction est utilisée dans les pages fiche_parcelle.php fiche_proprio.php fiche_site.php
Elle permet d'afficher une liste des sessions d'animation concernées par la pages en cours
Une même fonction peut être utilisée dans différentes pages si les processus s'adaptent correctement en fonction des cas
------------Les variables
------------passage->paramètre permettant de savoir si la page vient d'être chargée ou si elle vient d'être filtrée
------------cur_offset->paramètre utile côté fonction PHP pour déterminer à partir de quelle ligne la requête renvoie les résultats
------------flt_champ_id->id de l'élément référant de la page (par_id pour fiche_parcelle.php dnuper pour fiche_proprio.php site_id pour fiche_site.php)*/
function create_lst_saf(passage, cur_offset, flt_champ_id){
	//1ère partie de récupération des informations nécessaires sur la page initiale
	var flt_libelle //variable stockant le type d'acte entre acquisition et convention
	var flt_date //variable stockant l'id du site (utile uniquement dans fiche_proprio.php)
	var flt_origine //variable stockant le libellé de l'acte
	var flt_statut //variable stockant le statut
	var flt_page = window.location.pathname; //variable stockant la page PHP appelant la fonction, elle permettra d'adapter la requete SQL lancée côté PHP par Ajax
	
	/* On controle si une valeur est indiquée pour chaque filtre et on renseigne la variable correspondante en fonction */
	
	if (document.getElementById('filtre_saf_lib') != null) {
		flt_libelle = document.getElementById('filtre_saf_lib').value;
	} else {
		flt_libelle = "";
	}
	if (document.getElementById('filtre_saf_date') != null) {
		flt_date = document.getElementById('filtre_saf_date').value;
	} else {
		flt_date = "";
	}
	if (document.getElementById('filtre_saf_origin') != null) {
		flt_origine = document.getElementById('filtre_saf_origin').options[document.getElementById('filtre_saf_origin').selectedIndex].text;
	} else {
		flt_origine = "";
	}
	if (document.getElementById('filtre_saf_statut') != null) {
		flt_statut = document.getElementById('filtre_saf_statut').options[document.getElementById('filtre_saf_statut').selectedIndex].text;
	} else {
		flt_statut = "";
	}
	
	
	//2ème partie basée sur Ajax
	$.ajax({ 
		url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json",
		data      :{ 
			fonction:'fct_lst_saf', params: {page: flt_page, champ_id: flt_champ_id, libelle: flt_libelle, date: flt_date, origine: flt_origine, statut: flt_statut, offset: cur_offset} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
		},
		beforeSend: function(){
			$("#loading").show();
		},
		complete: function(){
			$("#loading").hide();
		},
		success: function(data) { //On récupère du code dans la variable data
			$('#tbl_saf_corps').html(data['code']); //la page est adaptée à partir de la variable 'data'
			
			/* La procédure qui suit permet de remplir automatiquement les options des filtres en listes déroulantes
			Après verification qu'aucun filtre n'est renseigné et que la page vient d'être chargée on lance la fonction GetComboBoxFilter */
			var flt_null = 'OUI'; //variable permettant de savoir aucun filtre n'est renseigné
			var elements = document.forms['frm_saf_entete'].elements; //variable tableau stockant les éléments 
			for (k=0; k<elements.length; k++){
				if (elements[k].id.substring(0,6) == 'filtre' && ((elements[k].type == 'select-one' && document.getElementById(elements[k].id).value != '-1') || (elements[k].type == 'text' && document.getElementById(elements[k].id).value != ''))) {
					flt_null = 'NON';
				}
			}
			if (passage == 'charge' && flt_null == 'OUI') {
				GetComboBoxFilter('tbl_saf_corps', 'frm_saf_entete', 'a');
			}
		}
	});
}

/* Cette fonction permet de mettre à jour la liste des parcelles associées à un acte.
On récupère les parcelles ajoutées et les parcelles supprimées pour ajouter ou archiver.   */
function parcModify(frm, id_acte, type_acte) {	
	var parcAjout = document.getElementById('parc_ajout').value;
	var parcSuppr = document.getElementById('parc_suppr').value;
	var dateMajParc = $('#acte_date_maj_parc').val();
	var tbl_grant_elt_form_id = $('#hid_tbl_grant_elt_form_id').val().split(',');
	if (dateMajParc == '' || dateMajParc == 'N.D.' || dateMajParc.split('/').length != 3 || dateMajParc.length != 10) {
		$('#lbl_parcModify').html('<b>Vous devez saisir une date correcte</b>')
		$('#lbl_parcModify').css('display', '');
	} else {
		$('#lbl_parcModify').css('display', 'none');
		$('#lbl_parcModify').html('<b>Vous devez associer au moins une parcelle</b>')
		if ($.inArray('acte_parc', tbl_grant_elt_form_id) > -1) {
			$.ajax({ 
				url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
				type      : "post", //type de communication entre les différentes pages/fonctions/langages
				data      :{ 
					fonction:'fct_parcModify', params: {parc_ajout: parcAjout, date_maj_parc: dateMajParc, parc_suppr: parcSuppr, acte_id: id_acte, acte_type: type_acte} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
				},
				success: function() { //On récupère du code dans la variable data
					window.location.reload()

				}
			});	
		}
	}
}

/* Cette fonction permet de renseigner la surface maitrisée d'une parcelle conventionnée pour partie
Attention elle réinitialise la geom qu'il faudra recréer sur QGis */
function updateSurfmuConfirm(contenance, cad_site_id, mu_id) {
	if ($('#surfmu_'+cad_site_id).val() > contenance || $('#surfmu_'+cad_site_id).val().substr(($('#surfmu_'+cad_site_id).val().length-6),1) != '.' || $('#surfmu_'+cad_site_id).val().substr((($('#surfmu_'+cad_site_id).val().length)-3),1) != '.') {
		verif = 'NON';
		$('#surfmu_'+cad_site_id).css('background', '#fe5656');
	} else {
		//2ème partie basée sur Ajax
		$.ajax({ 
			url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json",
			data      :{ 
				fonction:'fct_updateSurfmuConfirm', params: {var_contenance: contenance, var_new_surfmu: $('#surfmu_'+cad_site_id).val(), var_cad_site_id: cad_site_id, var_mu_id: mu_id} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
			},
			beforeSend: function(){
				$("#loading").show();
			},
			complete: function(){
				$("#loading").hide();
			},
			success: function(data) { //On récupère du code dans la variable data
				$('#img_'+cad_site_id).prop('src', $('#img_'+cad_site_id).prop('src').replace('enregistrer.png', 'edit.png'));
				$('#img_'+cad_site_id).attr('onclick', "updateSurfmu('"+contenance+"','"+cad_site_id+"','"+mu_id+"');");
				$('#surfmu_'+cad_site_id).css('background', 'none');
				$('#surfmu_'+cad_site_id).css('border', 0);
				$('#surfmu_'+cad_site_id).prop('readonly', true );
				$('#surfmu_'+cad_site_id).prop('disabled', true );
				if ($('#surfmu_'+cad_site_id).val() < contenance) {
					$('#a_type_mu_'+cad_site_id).html('Convention pour partie');
				} else if ($('#surfmu_'+cad_site_id).val() == contenance){
					$('#a_type_mu_'+cad_site_id).html('Convention');
				}
				GetComboBoxFilter('tbl_parc_corps', 'frm_parc_entete', 'a');
			}
		});
	}
}

/* Cette fonction permet de mettre à jour les droits par groupes d'utilisateur postgres.   */
function updateGrantGroup(elt, groname, group_elt_id) {	
	var grant;
	if ($('#'+elt.id).is(':checked')) {
		grant = 'true'
	} else {
		grant = 'false'
	}
	
	$.ajax({ 
		url       : "foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		data      :{ 
			fonction:'fct_updateGrantGroup', params: {var_groname: groname, var_group_elt_id: group_elt_id, var_grant: grant} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
		}
	});	
}