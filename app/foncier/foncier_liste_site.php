<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Foncier';
			$h3 ='Recherche par site';
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'));
			include("../includes/header.php");
			include('../includes/breadcrumb.php');			
			//include('../includes/construction.php');
			
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}		
								
			$rq_lst_site = "
			SELECT
				tlist.site_id, tlist.site_nom, tlist.typsite_lib as type_site, 
				CASE 
					WHEN count(DISTINCT tlist.nom) > 1 THEN array_to_string(array_agg(tlist.nom ORDER BY tlist.nom), '<br />') 
					ELSE Min(tlist.nom) 
				END as nom, 
				CASE 
					WHEN count(DISTINCT tlist.territoire_lib) > 1 THEN array_to_string(array_agg(tlist.territoire_lib ORDER BY tlist.nom), '<br />') 
					ELSE COALESCE(Min(tlist.territoire_lib), 'N.D.') 
				END as territoire_lib, 
				CASE 
					WHEN count(DISTINCT tlist.depart) > 1 THEN array_to_string(array_agg(tlist.depart ORDER BY tlist.nom), '<br />') 
					ELSE Min(tlist.depart) 
				END as departement
			FROM (
				SELECT DISTINCT
					t1.site_id, t1.site_nom, t2.typsite_lib, tcommunes.nom, replace(replace(replace(t_terr.territoire_lib, 'Communauté de communes', 'CC'),  'Communauté d''agglomération', 'CA'), 'Intercommunalité', 'Interco.') as territoire_lib, tcommunes.depart
				FROM
					sites.sites t1 
					JOIN sites.d_typsite t2 USING (typsite_id)
					JOIN foncier.cadastre_site USING (site_id)
					JOIN cadastre.cadastre_cen USING (cad_cen_id)
					JOIN cadastre.lots_cen USING (lot_id)
					JOIN cadastre.parcelles_cen USING (par_id)
					JOIN (
						SELECT t1.code_insee AS code_insee_cad, COALESCE(t2.code_insee, t1.code_insee) AS code_insee, COALESCE(t2.nom, t1.nom) AS nom, COALESCE(t2.actif, t1.actif) AS actif, COALESCE(t2.depart, t1.depart) AS depart
						FROM administratif.communes t1
							LEFT JOIN administratif.r_histo_com ON (r_histo_com.code_insee_old = t1.code_insee)
							LEFT JOIN administratif.communes t2 ON (t2.code_insee = r_histo_com.code_insee_new)
					) tcommunes ON (parcelles_cen.codcom = tcommunes.code_insee_cad)
					LEFT JOIN (
						SELECT DISTINCT
							ttrue.code_insee,
							t4.territoire_lib,
							t5.collectivites
						FROM
							(
								SELECT r_ter_com.territoire_id, r_ter_com.date_maj, r_ter_com.code_insee
								FROM territoires.r_ter_com
								WHERE r_ter_com.actif = TRUE
								GROUP BY r_ter_com.territoire_id, r_ter_com.date_maj, r_ter_com.code_insee
							) ttrue	
							JOIN (
								SELECT r_ter_com.territoire_id, max(r_ter_com.date_maj::text) AS max_date_maj, r_ter_com.code_insee
								FROM territoires.r_ter_com
								GROUP BY r_ter_com.territoire_id, r_ter_com.code_insee
								) tmax ON (tmax.max_date_maj = ttrue.date_maj AND tmax.territoire_id = ttrue.territoire_id AND tmax.code_insee = ttrue.code_insee)
							LEFT JOIN territoires.territoires t4 ON (t4.territoire_id = tmax.territoire_id) 
							LEFT JOIN territoires.d_typterritoire t5 USING (typterritoire_id)
					) t_terr USING (code_insee)
					 
				WHERE
					t1.typsite_id NOT In ('3', '4')
					AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.'
					AND (t_terr.collectivites = true OR t_terr.collectivites is null)
					AND tcommunes.actif = TRUE
				GROUP BY t1.site_id, t1.site_nom, t2.typsite_lib, tcommunes.nom, t_terr.territoire_lib, tcommunes.depart
			) as tlist
			GROUP BY tlist.site_id, tlist.site_nom, tlist.typsite_lib
			ORDER BY tlist.site_id;
			";
			
			$res_lst_site = $bdd->prepare($rq_lst_site);
			$res_lst_site->execute();
			$lst_site = $res_lst_site->fetchAll();
		?>
		<section id='main'>
			<h3>Recherche par site</h3>
			<table id='tbl_site' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
				<tbody id='tbl_site_entete'>
					<form id="frm_site_entete" onsubmit="return false">
						<tr>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 0, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 0, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='75px'>
								<input style='text-align:center' class='editbox' oninput="maj_table('frm_site_entete', 'tbl_site_corps', 0);" type='text' id='filtre_site_id' size='6' placeholder='SITE ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SITE ID';"> 
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer; top:1px" onclick="sortTable('tbl_site', 'tbl_site_corps', 1, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 1, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left'>
								<input style='text-align:center' class='editbox editbox_larger' oninput="maj_table('frm_site_entete', 'tbl_site_corps', 0);" type='text' id='filtre_nom' placeholder='Nom du site' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du site';"> 
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 2, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 2, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<label class="btn_combobox">
									<select onchange="maj_table('frm_site_entete', 'tbl_site_corps', 0);" id="filtre_type" class="combobox" style='text-align:center;max-width:300px'>
										<option value='-1'>Type de site</option>
										<?php
										$tab_site_type_unique = array_values(array_unique(array_column($lst_site, 'type_site')));
										sort($tab_site_type_unique);
										For ($j=0; $j<count($tab_site_type_unique); $j++)
											{echo "<option value='" . $j . "'>" . $tab_site_type_unique[$j] . "</option>";}
										?>				
									</select>
								</label>
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 3, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 3, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<input style='text-align:center' class='editbox' oninput="maj_table('frm_site_entete', 'tbl_site_corps', 0);" type='text' id='filtre_commune' placeholder='Commune' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Commune';"> 
							</td>
							<?php If (count(array_unique(array_column($lst_site, 'departement'))) > 1) { ?>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 4, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 4, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<input style='text-align:center' class='editbox' oninput="maj_table('frm_site_entete', 'tbl_site_corps', 0);" type='text' id='filtre_terr' placeholder='Département' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Département';"> 
							</td>
							
							<?php } Else If (count(array_unique(array_column($lst_site, 'departement'))) == 1) {?>
							
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 4, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_site', 'tbl_site_corps', 4, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<input style='text-align:center' class='editbox' oninput="maj_table('frm_site_entete', 'tbl_site_corps', 0);" type='text' id='filtre_terr' size='23' placeholder='Collectivité' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Collectivité';"> 
							</td>
							<?php } ?>
							<td width='5'>
								<img style='cursor:pointer;' onclick="
								document.getElementById('filtre_site_id').value='';
								document.getElementById('filtre_nom').value='';
								document.getElementById('filtre_type').value='-1';
								document.getElementById('filtre_commune').value='';
								document.getElementById('filtre_terr').value='';
								maj_table('frm_site_entete', 'tbl_site_corps', 0);
								" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
							</td>
						</tr>
					</form>
				</tbody>
				<tbody id='tbl_site_corps'>
				<?php
					For ($i=0; $i<$res_lst_site->rowCount(); $i++) {
						If($i%2)
							{$tr=1;}
						Else
							{$tr=0;}
						$vget = "site_id:" . $lst_site[$i]['site_id'] . ";site_nom:" . $lst_site[$i]['site_nom'] . ";";
						$vget_crypt = base64_encode($vget);
						$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";
						echo "<tr class='toggle_tr l$tr' onclick=\"document.location='$lien'\">";
						For($j=0; $j<$res_lst_site->columnCount(); $j++) {
							$meta = $res_lst_site->getColumnMeta($j);
							If ((count(array_unique(array_column($lst_site, 'departement'))) > 1 and $meta['name'] == 'departement') OR (count(array_unique(array_column($lst_site, 'departement'))) == 1 and $meta['name'] == 'territoire_lib') OR ($meta['name'] != 'territoire_lib' AND $meta['name'] != 'departement')) {
								echo "
								<td colspan='2'>
									<center><a href='$lien'><span style='text-align:right;'>" . $lst_site[$i][$j] . "</span></a></center>
								</td>";
							} 
						}
						echo "</tr>";
					}
				?>
				</tbody>
			</table>
		</section>
		<?php 
			$res_lst_site->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
