<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../../includes/configuration.php');
	try {
		$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
	}
	catch (Exception $e) {
		die(include('../../../includes/prob_tech.php'));
	}
			
	$site_id = $_GET['site_id'];

	$rq_prive = "
	SELECT 
		SUM(st_area(t1.geom))/10000 as surf
	FROM 
		cadastre.parcelles_cen t1
		LEFT JOIN cadastre.d_typprop USING (typprop_id)
		JOIN cadastre.lots_cen t2 USING (par_id)
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
	WHERE 
		t3.date_fin = 'N.D.' AND t4.date_fin = 'N.D.' AND site_id = :site_id AND typprop = 'Privé'";
	$res_prive = $bdd->prepare($rq_prive);
	$res_prive->execute(array('site_id'=>$site_id));
	$prive = $res_prive->fetch();
	$pie[0] = round($prive['surf'], 1);
	$pie_lgd[0] = 'Privé';

	$rq_public = "
	SELECT 
		SUM(st_area(t1.geom))/10000 as surf
	FROM 
		cadastre.parcelles_cen t1
		LEFT JOIN cadastre.d_typprop USING (typprop_id)
		JOIN cadastre.lots_cen t2 USING (par_id)
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
	WHERE 
		t3.date_fin = 'N.D.' AND t4.date_fin = 'N.D.' AND site_id = :site_id AND typprop = 'Public'";
	$res_public = $bdd->prepare($rq_public);
	$res_public->execute(array('site_id'=>$site_id));
	$public = $res_public->fetch();
	$pie[1] = round($public['surf'], 1);
	$pie_lgd[1] = 'Public';

	$rq_communal = "
	SELECT 
		SUM(st_area(t1.geom))/10000 as surf
	FROM 
		cadastre.parcelles_cen t1
		LEFT JOIN cadastre.d_typprop USING (typprop_id)
		JOIN cadastre.lots_cen t2 USING (par_id)
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
	WHERE 
		t3.date_fin = 'N.D.' AND t4.date_fin = 'N.D.' AND site_id = :site_id AND typprop = 'Communal'";
	$res_communal = $bdd->prepare($rq_communal);
	$res_communal->execute(array('site_id'=>$site_id));
	$communal = $res_communal->fetch();
	$pie[2] = round($communal['surf'], 1);
	$pie_lgd[2] = 'Communal';

	$rq_nd = "
	SELECT 
		SUM(st_area(t1.geom))/10000 as surf
	FROM 
		cadastre.parcelles_cen t1
		LEFT JOIN cadastre.d_typprop USING (typprop_id)
		JOIN cadastre.lots_cen t2 USING (par_id)
		JOIN cadastre.cadastre_cen t3 USING (lot_id)
		JOIN foncier.cadastre_site t4 USING (cad_cen_id)
	WHERE 
		t3.date_fin = 'N.D.' AND t4.date_fin = 'N.D.' AND site_id = :site_id AND (typprop IS NULL OR typprop = 'N.P.')";
	$res_nd = $bdd->prepare($rq_nd);
	$res_nd->execute(array('site_id'=>$site_id));
	$nd = $res_nd->fetch();
	$pie[3] = round($nd['surf'], 1);
	$pie_lgd[3] = 'N.D. - N.P.';

	For($i=0; $i<count($pie); $i++)
		{
			If((($pie[$i]*100)/array_sum($pie)) < 0.09)
				{$explode[$i] = 20000;}
			ElseIf ($i < count($pie)-1 && (($pie[$i]*100)/array_sum($pie)) < 2 && (($pie[$i]*100)/array_sum($pie)) > 0 && (($pie[$i+1]*100)/array_sum($pie)) < 1 && (($pie[$i+1]*100)/array_sum($pie))) 
				{$explode[$i] = 50;}
			ElseIf ((($pie[$i]*100)/array_sum($pie)) < 2 && (($pie[$i]*100)/array_sum($pie)) > 0) 
				{$explode[$i] = 25;}
			Else
				{$explode[$i] = 5;}
		}

	include ("../jpgraph/jpgraph.php");
	include ("../jpgraph/jpgraph_pie.php");
	include ("../jpgraph/jpgraph_pie3d.php");

	$graph = new PieGraph(360,220); 
	$graph->graph_theme = null;
	$title = "Type de propriété\n " . $_GET['site_nom'];
	$graph->title->Set($title);
	$graph->title->SetFont(FF_VERDANA,FS_BOLD, 9);
	$graph->SetColor('#ffffff');
	$graph->SetAntiAliasing(); 
	$graph->SetFrame(False);

	$bplot1 = new piePlot3d($pie);
	$bplot1->SetCenter(0.35, 0.53);
	$bplot1->SetSize(75);
	$bplot1->SetHeight(6);
	$bplot1->SetAngle(60);
	$bplot1->explode($explode);
	$bplot1->SetLegends($pie_lgd);
	$bplot1->SetSliceColors(array('#d77b0e','#7ab51d','#014495','#e0e0e0'));
	$bplot1->value->SetFont(FF_ARIAL,FS_NORMAL,8);
	$bplot1->value->SetColor('darkgray@0.1');
	$bplot1->SetLabelPos(0.94);
	$bplot1->value->SetFormat("%0.1f%%");
	$bplot1->SetStartAngle(90);

	$graph->legend->Pos(0.04,0.55);
	$graph->legend->SetMarkAbsSize(10);
	$graph->legend->SetFont(FF_VERDANA,FS_NORMAL,10);
	$graph->legend->SetFillColor('#ffffff');
	$graph->legend->SetFrameWeight(0);
	$graph->legend->SetColumns(1);

	$graph->Add($bplot1);
	$graph->Stroke();
?>
