<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	//TOUTES LES FONCTIONS DE CETTE PAGE SONT APPELLEES PAR LA METHODE AJAX DEPUIS LA PAGE foncier.js ET RENVOIENT DU CODE PHP DYNAMIQUEMENT INTERPRETE DANS LA NAVIGATION
	
	session_start();
	$fonction = $_POST['fonction'];
	unset($_POST['fonction']);
	$fonction($_POST);
	
	//Cette fonction permet d'associer des parcelles à des actes
	function fct_parcModify($data) {
		include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure	
		include ('../includes/configuration.php');
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		$rq_check_grant = "SELECT * FROM foncier.grant_group WHERE grant_elt_id = 3 AND grant_group_id IN ('".implode("','", $_SESSION['grp'])."') AND grant_group_droit = 't'";
		$res_check_grant = $bdd->prepare($rq_check_grant);
		$res_check_grant->execute();
		If ($res_check_grant->rowCount() > 0) {		
			$aujourdhui = date('Y-m-d H:i:s');
			$date_maj_parc = date_transform_inverse($data['params']['date_maj_parc']).' 00:00:00';
			$acte_id = $data['params']['acte_id'];
			$type_acte = $data['params']['acte_type'];
			$tbl_parc_ajout = $data['params']['parc_ajout'];
			If (substr($tbl_parc_ajout, -1, 1) == ';'){
				$tbl_parc_ajout = substr($tbl_parc_ajout, 0, strlen($tbl_parc_ajout)-1); //suppression de la dernière virgule	
			}
			$tbl_parc_suppr = $data['params']['parc_suppr'];
			If (substr($tbl_parc_suppr, -1, 1) == ';'){
				$tbl_parc_suppr = substr($tbl_parc_suppr, 0, strlen($tbl_parc_suppr)-1); //suppression de la dernière virgule	
			}
			$lst_parc_ajout = preg_split("/;/", $tbl_parc_ajout);
			$lst_parc_suppr = preg_split("/;/", $tbl_parc_suppr);
			
			If ($type_acte == 'Acquisition') {
				$suffixe = 'mf';
			} Else If ($type_acte == 'Convention') {
				$suffixe = 'mu';
			}
			
			$rq_ajout = "INSERT INTO foncier.r_cad_site_$suffixe (cad_site_id, $suffixe"."_id, date_entree, date_sortie) VALUES ";
			For ($i = 0; $i<count($lst_parc_ajout); $i++) {
				$rq_ajout .= "($lst_parc_ajout[$i], '$acte_id', '$date_maj_parc', 'N.D.'), ";
			}
			For ($i = 0; $i<count($lst_parc_suppr); $i++) {
				$rq_suppr = "UPDATE foncier.r_cad_site_$suffixe SET date_sortie = '$date_maj_parc' WHERE cad_site_id = $lst_parc_suppr[$i] AND $suffixe"."_id = '$acte_id'";
				$res_suppr = $bdd->prepare($rq_suppr);
				$res_suppr->execute();
				$res_suppr->closeCursor();
			}
			
			If (substr($rq_ajout, -2, 1) == ','){
				$rq_ajout = substr($rq_ajout, 0, strlen($rq_ajout)-2); //suppression de la dernière virgule	
			}
			If (count($lst_parc_ajout) > 0) {
				$res_ajout = $bdd->prepare($rq_ajout);
				$res_ajout->execute();
				$res_ajout->closeCursor();
			}
		}
		$res_check_grant->closeCursor();
		$bdd = null;
	}
	
	// Cette fonction est utilisée pour adapter la liste des parcelles dans la page foncier_liste_parcelle.php en fonction des filtres
	// Elle renvoie directement un nouveau tableau html qui remplacera le précédent
	function fct_search_parc($data) {
		include("foncier_fct.php");	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		//Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		//Ces variables correspondent aux filtres présents dans la page foncier_liste_parcelle.php
		$commune_insee = $data['params']['commune'];
		$departement = $data['params']['departement'];
		$site_id = $data['params']['site_id'];
		$section = $data['params']['section'];
		$par_num = $data['params']['par_num'];
		$offset = $data['params']['offset'];
		$nbr_result = 60; //variable correspondant au nombre de résultats renvoyés par la requête
		
		//initialisation d'une variable correpondant à la clause where de la requete de sélection des parcelles
		$where = ' WHERE t1.codcom = communes.code_insee'; 
		
		// Si le filtre des communes est rempli on ajoute un élément dans la clause where
		If ($commune_insee <> -1) { 
			$where .= " AND codcom = '$commune_insee'";
		}	
		If ($departement <> -1) { 
			$where .= " AND codcom LIKE '".$departement."%'";
		}	
		// Si le filtre des sites est rempli on ajoute un élément dans la clause where
		If ($site_id <> 'SITE ID' AND $site_id <> '' AND $where <> '') { 
			$where .= " AND site_id ILIKE '" . $site_id . "%'";
		}	
		// Si le filtre des sections est rempli on ajoute un élément dans la clause where
		If ($section <> 'SECTION' AND $section <> '' AND $where <> '') { 
			$where .= " AND ccosec ILIKE '%" . $section . "%'";
		}
		// Si le filtre des parcelles est rempli on ajoute un élément dans la clause where
		If ($par_num <> 'PAR NUM' AND $par_num <> '' AND $where <> '') { 
			$where .= " AND dnupla::text ILIKE '" . $par_num . "%'";
		}
		
		// On finalise la clause where
		$where .= " AND t1.par_id = t2.par_id AND t2.lot_id = t3.lot_id AND t3.cad_cen_id = t4.cad_cen_id 
		GROUP BY t1.par_id, t1.codcom, t1.ccosec, t1.dnupla, t4.site_id) tt1";
		// Construction de la requête 
		$rq_lst_parcelles = 
		"SELECT 
			tt1.par_id, 
			tt1.codcom, 
			tt1.ccosec, 
			tt1.dnupla, 
			tt1.site_id, 
			tt1.nbr_lots - (tt1.nbr_lot_suppr + tt1.nbr_lot_hors_site) as nbr_lots,
			CASE WHEN tt1.nbr_lot_suppr < tt1.nbr_lots THEN
				CASE WHEN tt1.nbr_lot_hors_site < tt1.nbr_lots THEN
					'N.D.'
				ELSE
					'Parcelle dissociée du site' 
				END
			ELSE
				CASE WHEN tt1.nbr_lot_suppr = tt1.nbr_lots THEN 
					'Parcelle supprimée'
				ELSE
					CASE WHEN tt1.nbr_lots = (tt1.nbr_lot_suppr + tt1.nbr_lot_hors_site) THEN 
						'Parcelle dissociée du site' 
					ELSE 
						'N.D.' 
					END 
				END
			END as motif_de_fin 
		FROM 
			(
			SELECT 
				t1.par_id, 
				t1.codcom, 
				t1.ccosec, 
				t1.dnupla, 
				t4.site_id, 
				count(DISTINCT t2.lot_id) as nbr_lots,

				sum(CASE WHEN t3.date_fin != 'N.D.' AND t3.motif_fin_id = 1 THEN 
					1
					ELSE 
					0
					END) as nbr_lot_suppr,

				sum(CASE WHEN t3.date_fin = 'N.D.' AND t4.date_fin != 'N.D.'  THEN 
					1
					ELSE 
					0
					END) as nbr_lot_hors_site
			FROM 
				administratif.communes, cadastre.parcelles_cen t1, cadastre.lots_cen t2, cadastre.cadastre_cen t3, foncier.cadastre_site t4" . $where . " ORDER BY site_id, par_id OFFSET $offset LIMIT $nbr_result";
		// echo $rq_lst_parcelles;
		// Lancement et execution de la requête
		$res_lst_parcelles = $bdd->prepare($rq_lst_parcelles);
		$res_lst_parcelles->execute();
		
		// Initialisation de la variable $result qui enverra le resultat final au navigateur
		// La fonction fct_suiv_prev est décrite dans la page foncier_fct.php
		$result = fct_suiv_prec($offset, $nbr_result, $res_lst_parcelles->rowCount(), 4, 'create_search_parc(offset)');
		
		$j=0; // Variable petite bidouille pour alterner la couleur des lignes
		// Création du tableau ligne par ligne à partir du résultat de la requête
		While ($donnees = $res_lst_parcelles->fetch()) {
			 //Cette variable directement issue de la requête permet de savoir si la parcelle existe
					
			If ($donnees['motif_de_fin'] != 'N.D.') { // Si la parcelle n'existe pas un lien existe mais la ligne est grise
				//Création du lien vers la fiche correspondante
				$vget = "par_id:$donnees[par_id];lot_id:$donnees[par_id];mode:consult;";
				If  ($donnees['motif_de_fin'] == 'Parcelle supprimée') { 
					$vget .= "parc_suppr:oui;";
				}
				$vget_crypt = base64_encode($vget);
				$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_parcelle.php?d=$vget_crypt#general";
				// Implémentation de la variable $result 
				$result .= "<tr class='toggle_tr l$j' onclick=\"document.location='$lien'\">
				<td style='background:#cccccc;border-color:#cccccc;'>
					<center><a href='$lien'>$donnees[site_id]</a></center>
				</td>
				<td style='background:#cccccc;border-color:#cccccc;'>
					<center><a href='$lien'>$donnees[ccosec]</a></center>
				</td>
				<td style='background:#cccccc;border-color:#cccccc;text-align:left;' colspan='2'>
					<a href='$lien'>$donnees[dnupla] ($donnees[motif_de_fin])</a></td>"; 			
			} Else { // Si la parcelle existe toujours on l'affiche avec l'alternance de couleur habituelle
				//Création du lien vers la fiche correspondante
				$vget = "par_id:$donnees[par_id];lot_id:$donnees[par_id];mode:consult;";
				$vget_crypt = base64_encode($vget);
				$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_parcelle.php?d=$vget_crypt#general";
				$result .= "<tr class='toggle_tr l$j' onclick=\"document.location='$lien'\">";
				$result .= "
				<td>
					<center><a href='$lien'>$donnees[site_id]</a></center>
				</td>
				<td>
					<center><a href='$lien'>$donnees[ccosec]</a></center>
				</td>
				<td colspan='2'>
					<a href='$lien'>$donnees[dnupla]       (";
				
				// Implémentation de la variable $result avec le nombre de lots entre parenthèse
				$result .= $donnees['nbr_lots'] . " lot"; If ($donnees['nbr_lots'] > 1) {$result .= "s";}
				$result .=  ")</a></td>"; 
				
				// Petite bidouille pour alterner la couleur des lignes
				If($j == 0) {
					$j=1;
				} Else {
					$j=0;
				}
			}
		}
		
		// Implémentation de la variable $result avec les liens suivant précedent en bas de page
		$result .= fct_suiv_prec($offset, $nbr_result, $res_lst_parcelles->rowCount(), 4, 'create_search_parc(offset)');
		
		// Ferme le curseur et désactive la connection à la base
		$res_lst_parcelles->closeCursor();
		$bdd = null;
		
		echo $result;
	}
	
	// Cette fonction permet de récupérer la liste des parcelles que l'on retrouve dans les différentes fiches (site, mfu, proprio)
	// Elle permet également d'adapter la liste des parcelle en fonction des filtres
	function fct_lst_parc($data) {
		include('foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure	
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$page = $data['params']['page']; // Cette variable récupère le nom de la page fiche d'origine
		$champ_id = $data['params']['champ_id']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
		$site_id = $data['params']['site_id'];
		$commune = $data['params']['commune'];
		$section = $data['params']['section'];
		$par_num = $data['params']['par_num'];
		$typ_prop = $data['params']['typ_prop'];
		$mfu = $data['params']['mfu'];
		$priorite = $data['params']['priorite'];
		$type_acte = $data['params']['type_acte'];
		$mode = $data['params']['var_mode'];
		$offset = $data['params']['offset'];
		$nbr_result = 60000000;
		
		// Pour simplifier la procédure on détermine le prefixe utilisé pour l'appelle de certaines tables et certains champs dans la requête
		// Cette particularité est utilisée uniquement pour la page fiche_mfu.php
		If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php' && $type_acte == 'Acquisition') {
			$suffixe = 'mf';
		} Else If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php' && $type_acte == 'Convention') {
			$suffixe = 'mu';
		}
		
		// SECTION RECHERCHE DE L'HISTORIQUE DES PARCELLES
		// initialisation d'une variable correpondant à la clause where de la requete de sélection de l'historique des parcelles
		$where_histo = "WHERE sites.site_id = t4.site_id AND t4.cad_cen_id = t3.cad_cen_id AND t3.lot_id = t2.lot_id and t2.par_id = t1.par_id AND t1.codcom = communes.code_insee AND (t4.date_fin != 'N.D.' OR t3.date_fin != 'N.D.'";
		
		If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$where_histo .= " OR r_cad_site_$suffixe.date_sortie != 'N.D.'";
		}
		
		$where_histo .=")";
		
		//En fonction de la page fiche d'origine on implémente la clause where avec les élémens nécessaires à la requête
		If (substr($page, strrpos($page, '/')) == '/fiche_site.php') {
			$where_histo .= " AND t4.site_id = '$champ_id'";
		} ElseIf (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
			$where_histo .= " AND t3.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t6.dnuper = '$champ_id'";
		} ElseIf (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$where_histo .= " AND r_cad_site_$suffixe.cad_site_id = t4.cad_site_id AND r_cad_site_$suffixe.$suffixe"."_id = '$champ_id'";
		}
		
		// Implémentation de la variable $where_histo avec la fin de la requête pour les clause GROUP BY et ORDER BY
		$where_histo .= " GROUP BY t4.site_id, t1.par_id, communes.nom, t1.ccosec, t1.dnupla, t1.dcntpa, d_typprop.typprop
						  ORDER BY t4.site_id, t1.par_id, tbl_dnulot, tbl_date_fin_cad, tbl_date_fin_site";	
		
		// Construction de la requête de sélection de l'historique des parcelles
		$rq_lst_histo_parcelles = "
		SELECT 
			t4.site_id, t1.par_id, communes.nom AS com_nom, t1.ccosec, t1.dnupla, t1.dcntpa,
			array_to_string(array_agg(t3.date_fin ORDER BY t2.dnulot), ',') AS tbl_date_fin_cad, 
			array_to_string(array_agg(CASE WHEN t3.date_fin != 'N.D.' THEN
				CASE WHEN t3.motif_fin_id IS NOT NULL THEN 
					d_motif_fin.motif_fin_lib
				ELSE 
					'N.D.' 
				END
			ELSE 
				CASE WHEN t4.date_fin != 'N.D.' THEN 
					'Parcelle dissociée du site' ";

		If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {			
			$rq_lst_histo_parcelles .= "		
				ELSE 
					CASE WHEN r_cad_site_$suffixe.date_sortie != 'N.D.' THEN 
						'Parcelle dissociée de l''acte le ' || date_part('day', r_cad_site_$suffixe.date_sortie::date) || '/' || date_part('month', r_cad_site_$suffixe.date_sortie::date) || '/' || date_part('year', r_cad_site_$suffixe.date_sortie::date)
					END";
		} 
			
		$rq_lst_histo_parcelles .= "	
				END
			END ORDER BY t2.dnulot), ',') AS tbl_motif_fin_cad, 
			array_to_string(array_agg(t4.date_fin ORDER BY t2.dnulot), ',') AS tbl_date_fin_site,
			CASE WHEN d_typprop.typprop != '' THEN d_typprop.typprop ELSE 'N.D.' END as typprop, 
			array_to_string(array_agg(t2.dnulot ORDER BY t2.dnulot), ',') AS tbl_dnulot, 
			CASE 
				WHEN array_to_string(array_agg(t2.dnulot ORDER BY t2.dnulot), ',') != '' THEN 
					array_to_string(array_agg(t2.dcntlo ORDER BY t2.dnulot), ',') 
				ELSE NULL 
			END AS tbl_dcntlo
		FROM 
			sites.sites, foncier.cadastre_site t4 , cadastre.cadastre_cen t3 LEFT JOIN cadastre.d_motif_fin USING (motif_fin_id), cadastre.lots_cen t2, cadastre.parcelles_cen t1 LEFT JOIN cadastre.d_typprop ON t1.typprop_id = d_typprop.typprop_id, administratif.communes
		";		
		
		//En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires
		If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
			$rq_lst_histo_parcelles .= ", cadastre.r_prop_cptprop_cen t6, cadastre.proprios_cen t7 ";
		} ElseIf (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$rq_lst_histo_parcelles .= ", foncier.r_cad_site_$suffixe ";
		}
		
		// Ajout de la varibale $where_histo contenant les clause WHERE GOUPE BY et ORDER BY à la fin de la requête
		$rq_lst_histo_parcelles .=  $where_histo;
		// echo $rq_lst_histo_parcelles;
		// Lancement et execution de la requête
		$res_lst_histo_parcelles = $bdd->prepare($rq_lst_histo_parcelles);
		$res_lst_histo_parcelles->execute();		
		
		// initialisation d'une variable correpondant à la clause where de la requete de sélection des parcelles
		If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$rq_statut_mfu = 
			"SELECT 
				r_$suffixe"."statut.statut"."$suffixe"."_id AS statut_id
			FROM
				foncier.r_"."$suffixe"."statut 
				JOIN (SELECT r_"."$suffixe"."statut."."$suffixe"."_id, Max(r_"."$suffixe"."statut.date) AS statut_date FROM foncier.r_"."$suffixe"."statut GROUP BY r_"."$suffixe"."statut."."$suffixe"."_id) AS tmax ON (r_"."$suffixe"."statut.date = tmax.statut_date)
			WHERE
				r_"."$suffixe"."statut."."$suffixe"."_id = :acte_id
			";
			$res_statut_mfu = $bdd->prepare($rq_statut_mfu);
			$res_statut_mfu->execute(array('acte_id'=>$champ_id));
			$statut_mfu = $res_statut_mfu->fetch();
			$res_statut_mfu->closeCursor();
			
			If ($type_acte == 'Convention' && ($statut_mfu['statut_id'] == 2 || $statut_mfu['statut_id'] == 4)) {
				$rq_dates_mu = 
				"SELECT date_effet_conv, date_fin_conv
				FROM foncier.mu_conventions
				WHERE mu_id = :acte_id";
				$res_dates_mu = $bdd->prepare($rq_dates_mu);
				$res_dates_mu->execute(array('acte_id'=>$champ_id));
				$dates_mu = $res_dates_mu->fetch();
				$res_dates_mu->closeCursor();
				
				$where = "
				WHERE 
					t3.date_fin >= '".$dates_mu['date_fin_conv']."' AND 
					t4.date_fin >= '".$dates_mu['date_fin_conv']."' 
					";
			} Else {
				$where = "
				WHERE 
					t3.date_fin = 'N.D.' AND 
					t4.date_fin = 'N.D.' 
					AND r_cad_site_"."$suffixe".".date_sortie = 'N.D.'
					";
			}
		} Else {
			$where = "
			WHERE 
				t3.date_fin = 'N.D.' AND 
				t4.date_fin = 'N.D.' 
				";
		}

		//En fonction de la page fiche d'origine on implémente la clause where avec les élémens nécessaires à la requête
		If (substr($page, strrpos($page, '/')) == '/fiche_site.php') {
			$where .= " AND t4.site_id = '$champ_id'";
		} ElseIf (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
			$where .= " AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t6.dnuper = '$champ_id'";
		} ElseIf (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$where .= " AND r_cad_site_$suffixe.cad_site_id = t4.cad_site_id AND r_cad_site_$suffixe.$suffixe"."_id = '$champ_id'";
		}
		// Si le filtre des sites est rempli on ajoute un élément dans la clause where
		If ($site_id <> 'SITE ID' AND $site_id <> '' AND $where <> '') {
			$where .= " AND t4.site_id ILIKE '%" . $site_id . "%'";
		}
		// Si le filtre des communes est rempli on ajoute un élément dans la clause where
		If ($commune <> 'Commune' AND $commune <> '' AND $where <> '') {
			$where .= " AND communes.nom ILIKE '%" . str_replace("'", "''", $commune) . "%'";
		}
		// Si le filtre des sections est rempli on ajoute un élément dans la clause where
		If ($section <> 'SECTION' AND $section <> '' AND $where <> '') {
			$where .= " AND ccosec ILIKE '%" . $section . "%'";
		}
		// Si le filtre des parcelles est rempli on ajoute un élément dans la clause where
		If ($par_num <> 'PAR NUM' AND $par_num <> '' AND $where <> '') {
			$where .= " AND to_char(dnupla, '9999') ILIKE '%" . $par_num . "%'";
		}
		
		// Implémentation de la variable $where avec la fin de la requête pour les clause GROUP BY et ORDER BY
		$where .= " GROUP BY t4.site_id, t1.par_id, t1.codcom, communes.nom, vl_cen.libelle, t1.ccosec, t1.dnupla, t1.dcntpa, d_typprop.typprop";
		
		// Construction de la requête de sélection des parcelles (celle la elle en impose non?)
		$rq_lst_parcelles = 
		"SELECT *
		FROM
		(SELECT t4.site_id, array_to_string(array_agg(t4.cad_site_id ORDER BY t2.dnulot), ',') AS tbl_cad_site_id, t1.par_id, t1.codcom AS code_insee, communes.nom AS com_nom, vl_cen.libelle as lieu_dit, t1.ccosec, t1.dnupla, t1.dcntpa, CASE WHEN d_typprop.typprop != '' THEN d_typprop.typprop ELSE 'N.D.' END as typprop, 
		array_to_string(array_agg(
			CASE 
				WHEN tmfu.mfu IS NULL THEN 0
				ELSE
					CASE 
						WHEN tmfu.surf_mfu is null THEN t2.dcntlo  
						ELSE tmfu.surf_mfu
					END
				END
		ORDER BY t2.dnulot), ',') AS tbl_surf_mu,		
		array_to_string(array_agg(t2.dnulot ORDER BY t2.dnulot), ',') AS tbl_dnulot, CASE WHEN array_to_string(array_agg(t2.dnulot ORDER BY t2.dnulot), ',') != '' THEN array_to_string(array_agg(t2.dcntlo ORDER BY t2.dnulot), ',') ELSE NULL END AS tbl_dcntlo, array_to_string(array_agg(CASE WHEN tmfu.mfu IS NULL THEN 'Pas de maîtrise' ELSE tmfu.mfu END ORDER BY t2.dnulot), ',') AS tbl_mfu_lib, array_to_string(array_agg(t5.dnupro ORDER BY t2.dnulot), ',') AS tbl_dnupro, array_to_string(array_agg(t3.motif_fin_id ORDER BY t2.dnulot), ',') AS tbl_motif_fin
		FROM 
			sites.sites 
			JOIN foncier.cadastre_site t4 USING (site_id)
			LEFT JOIN 
				(
					SELECT 
						cadastre_site.cad_site_id, 'Acquisition'::character varying(25) as mfu, Null AS surf_mfu
					FROM 
						foncier.cadastre_site, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf, (
						SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
						FROM foncier.r_mfstatut
						GROUP BY r_mfstatut.mf_id
						) as tmax
					WHERE 
						cadastre_site.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.typmf_id = d_typmf.typmf_id 
						AND mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id 
						AND (r_mfstatut.date = tmax.statut_date AND r_mfstatut.mf_id = tmax.mf_id) AND d_typmf.typmf_lib != 'Acquisition par un tiers' AND d_statutmf.statutmf_id = 1
						--AND r_cad_site_mf.date_sortie = 'N.D.'

				UNION

				SELECT 
					cadastre_site.cad_site_id, 
					CASE WHEN 
					 sum(
						CASE WHEN 
						(
							CASE WHEN recond_tacite = 't' AND date_effet_conv != 'N.D.' AND recond_duree > 0 THEN
								CASE WHEN EXTRACT(DAY FROM age(date(date_effet_conv) + interval '1 month' * mu_duree)) > 0 THEN
									admin_sig.f_special_round(
									(((EXTRACT(YEAR FROM age(date(date_effet_conv) + interval '1 month' * mu_duree)) * 12) + 
									EXTRACT(MONTH FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree))))+1) / recond_duree)::numeric, 0)::integer
								ELSE
									((((EXTRACT(YEAR FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree)))) * 12) + 
									EXTRACT(MONTH FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree))))+0) / recond_duree)-0)::integer
								END
							ELSE
								0
							END > mu_conventions.recond_nbmax AND mu_conventions.recond_nbmax != -1
						) OR 
						(mu_conventions.recond_tacite != 't' AND 
							CASE WHEN date_effet_conv IS NOT NULL AND date_effet_conv != 'N.D.' AND date_effet_conv != '' THEN 
								(date(date_effet_conv) + interval '1 month' * mu_duree)::character varying(10) 
							ELSE 
								'N.P.' 
							END < current_date::text) THEN 1 
						ELSE 
							0 
						END
					) > 0 THEN 
						'Indéterminé'::character varying(25) 
					ELSE 
						CASE WHEN d_typmu.mfu = true THEN 
							CASE WHEN r_cad_site_mu.surf_mu is null THEN 
								'Convention'::character varying(25) 
							ELSE 'Convention pour partie'::character varying(25) 
							END
						ELSE 'Autre'::character varying(25) --ATTENTION NO-MFU
						END
					END as mfu,
					r_cad_site_mu.surf_mu AS surf_mfu
					
				FROM 
					foncier.cadastre_site, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (
					SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
					FROM foncier.r_mustatut
					GROUP BY r_mustatut.mu_id
					) as tmax
				WHERE 
					cadastre_site.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.typmu_id = d_typmu.typmu_id 
					AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id 
					AND (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id) 
					AND r_cad_site_mu.date_sortie = 'N.D.' 
					AND d_statutmu.statutmu_id = 1 
				GROUP BY 
					cadastre_site.cad_site_id, r_cad_site_mu.surf_mu, d_typmu.mfu, mu_conventions.date_effet_conv, mu_conventions.date_fin_conv
				) AS tmfu ON t4.cad_site_id = tmfu.cad_site_id
			JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
			JOIN cadastre.cptprop_cen t5 USING (dnupro)
			JOIN cadastre.lots_cen t2 USING (lot_id)
			JOIN cadastre.parcelles_cen t1 USING (par_id)
			LEFT JOIN cadastre.d_typprop USING (typprop_id)
			LEFT JOIN cadastre.vl_cen USING (vl_id)
			JOIN administratif.communes ON t1.codcom = communes.code_insee";
		
		//En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires
		If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
			$rq_lst_parcelles .= ", cadastre.r_prop_cptprop_cen t6, cadastre.proprios_cen t7 ";
		} ElseIf (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$rq_lst_parcelles .= ", foncier.r_cad_site_$suffixe ";
		}
		
		//Implémentation de la requête avec des éléments intermédiaires nécessaires
		$rq_lst_parcelles .=  $where . " ORDER BY t4.site_id, t1.par_id, tbl_dnulot) as tt1";
		
		//Implémentation d'une clause where absolmument inutile mais bien pratique pour la suite.
		$rq_lst_parcelles .= " WHERE site_id IS NOT NULL";
		// Si le filtre des types de propriété est rempli on ajoute un élément dans la clause where
		If ($typ_prop <> 'TYP_PROP' AND $typ_prop <> '' AND $where <> '') {
			$rq_lst_parcelles .= " AND typprop ILIKE '%" . $typ_prop . "%'";
		}
		// Si le filtre de la MFU est rempli on ajoute un élément dans la clause where
		If ($mfu <> 'MFU' AND $mfu <> '') {
			$rq_lst_parcelles .= " AND tbl_mfu_lib ILIKE '%" . $mfu . "%'";
		}
				
		// Finalisation de la requête avec des éléments pour limiter le nombre de résultats en sortie
		// $rq_lst_parcelles .= " OFFSET $offset LIMIT $nbr_result;";
		// echo $rq_lst_parcelles;
		// Lancement et execution de la requête
		$res_lst_parcelles = $bdd->prepare($rq_lst_parcelles);
		$res_lst_parcelles->execute();
		
		// Variable tableau contenant le resultat à envoyé à AJAX [liste] pour la liste des parcelles en cours et [histo] pour la partie historique des parcelles
		$result['liste'] = "
				<tr class='no-export'>
					<td colspan='99' style='text-align:center'>
						<table class='no-border' border='0' width='100%'>
							<tr>
								<td width='100%'>
									<label class='label'"; If ($res_lst_histo_parcelles->rowCount() > 1) {$result['liste'] .= " style='margin-left:131px;'";} $result['liste'] .= " ><i>" . $res_lst_parcelles->rowCount() . " parcelle"; If ($res_lst_parcelles->rowCount() > 1) {$result['liste'] .= "s";} $result['liste'] .= "</i></label>
								</td>";
							// Si des parcelles sont présentes dans l'historique on affiche un bouton pour les consulter. Les lignes précédentes servent à centrer correctement le nombre de parcelles si le bouton historique est présent ou pas
							If ($res_lst_histo_parcelles->rowCount() > 0) {
								$result['liste'] .= "	
								<td style='text-align:right'>
									<label class='label' style='text-align:right;cursor:pointer;text-shadow:1px 1px 2px #ebebeb;color:#cf740d;' onclick=\"addParc_js('explorateur_histo', 'onglet');\">Consulter l'historique</label>
								</td>";
							}
		// Implémentation de la variable tableau permettant d'afficher la liste des parcelles
		$result['liste'] .= "						
							</tr>
						</table>
					</td>
				</tr>";
				
		// Cette ligne est commentée car on affiche l'intégralité des parcelles en une fois. Donc pas besoin de lien vers des pages suivantes ou précédentes
		// $result['liste'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_parcelles->rowCount(), 9, "create_lst_parc('modif', offset, '$champ_id')");
		
		$nbr_real_tr = 0; // Varible permettant de compter le vrai nombre de ligne du tableau sans prendre en compte les rowspans
		$j=0; // Variable petite bidouille pour alterner la couleur des lignes
		
		// Création du tableau ligne par ligne à partir du résultat de la requête
		While ($donnees = $res_lst_parcelles->fetch()) {
			// Variable correspondant à la valeur du rowspan à appliqué sur la ligne pour améliorer l'affichage en cas de multiples lots
			$rowspan = substr_count($donnees['tbl_dnulot'], ",")+1;			
			// Création du lien vers la fiche correspondante
			$vget = "par_id:$donnees[par_id];lot_id:$donnees[par_id];mode:consult;";
			If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){ $vget .= "parc_suppr:oui;"; }
			$vget_crypt = base64_encode($vget);
			$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_parcelle.php?d=$vget_crypt#general";
			
			// En cas de multiples lots, la structure du tableau est un peu compliquée et pour afficher des styles cohérents au survole de la souris on doit passer par des fonction JQuery décritent dans la page foncier.js
			If (substr_count($donnees['tbl_dnulot'], ",") == 0) { // Si il n'y ap as de lot rien de particulier
				$result['liste'] .= "
				<tr class='toggle_tr l$j hover$nbr_real_tr' rowspan='$rowspan' "; If($mode=='edit' && substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){$result['liste'] .= "style='cursor:default'";} $result['liste'] .= ">";
			} Else { // Si il y a des lots, on ajoute des fonction Javascript au survole de la souris
				$result['liste'] .= "
				<tr class='toggle_tr l$j hover$nbr_real_tr' rowspan='$rowspan' "; If($mode=='edit' && substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){$result['liste'] .= "style='cursor:default'";} $result['liste'] .= " onmouseover=\"hoverOnRowspan('master', 'hover$nbr_real_tr')\" onmouseout=\"hoverOutRowspan('master', 'hover$nbr_real_tr')\">";
			}
			
			If(substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){
				$result['liste'] .= "
				<td class='filtre_site_id' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\";";} $result['liste'] .= ">
					<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= ">$donnees[site_id]</a></center>
				</td>";
			} Else If(substr($page, strrpos($page, '/')) == '/fiche_proprio.php'){
				$result['liste'] .= "
				<td class='filtre_site_id' rowspan='$rowspan' onclick=\"document.location='$lien'\";>
					<center><a style='margin-left:0' href='$lien'>$donnees[site_id]</a></center>
				</td>";
			}
			
			If(substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){
				$result['liste'] .= "
				<td class='filtre_commune' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\"";} $result['liste'] .= ">
					<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= ">$donnees[com_nom]</a></center>
				</td>
				<td class='' rowspan='$rowspan' style='display:none;'>
					<center>$donnees[lieu_dit]</center>
				</td>
				<td class='' rowspan='$rowspan' style='display:none;'>
					<center>'$donnees[par_id]</center>
				</td>
				<td class='' rowspan='$rowspan' style='display:none;'>
					<center>$donnees[code_insee]</center>
				</td>
				<td class='' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\"";} $result['liste'] .= ">
					<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= ">$donnees[ccosec]</a></center>
				</td>
				<td class='' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\"";} $result['liste'] .= ">
					<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= ">$donnees[dnupla]</a></center>
				</td>
				<td class='' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\"";} $result['liste'] .= ">
					<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= ">" . conv_are($donnees['dcntpa']) . "</a></center>
				</td>";
			} Else {
				$result['liste'] .= "
				<td class='filtre_commune' rowspan='$rowspan' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[com_nom]</a></center>
				</td>
				<td class='' rowspan='$rowspan' style='display:none;'>
					<center>$donnees[lieu_dit]</center>
				</td>
				<td class='' rowspan='$rowspan' style='display:none;'>
					<center>'$donnees[par_id]</center>
				</td>
				<td class='' rowspan='$rowspan' style='display:none;'>
					<center>$donnees[code_insee]</center>
				</td>
				<td class='' rowspan='$rowspan' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[ccosec]</a></center>
				</td>
				<td class='' rowspan='$rowspan' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[dnupla]</a></center>
				</td>
				<td class='' rowspan='$rowspan' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>" . conv_are($donnees['dcntpa']) . "</a></center>
				</td>";
			}			
			If(substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){
				$result['liste'] .= "
				<td class='filtre_typ_prop' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\"";} $result['liste'] .= ">
					<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= ">$donnees[typprop]</a></center>
				</td>";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_site.php') {
				$result['liste'] .= "
				<td class='filtre_typ_prop' rowspan='$rowspan' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[typprop]</a></center>
				</td>";
			}
						
			// On arrive aux cellules prévues pour les lots (identifiant et contenance)
			If (count(preg_split("/,/", $donnees['tbl_dnulot'])) == 1 AND $donnees['tbl_dnulot'] == '') { // Si il n'y a pas de lot on affiche rien dans les deux prochaines cellules et on poursuit sans rowspan
				If(substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){
					$result['liste'] .= "
					<td rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\";";} $result['liste'] .= ">
						<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= "> </a></center>
					</td>
					<td rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\";";} $result['liste'] .= ">
						<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= "> </a></center>
					</td>
					<td class='filtre_mfu' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\";";} $result['liste'] .= ">
						<center><a id='a_type_mu_".$donnees['tbl_cad_site_id']."' style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien'";} $result['liste'] .= ">".$donnees['tbl_mfu_lib']."</a></center>
					</td>
					<td class='filtre_priorite' rowspan='$rowspan' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien'\";";} $result['liste'] .= ">";
					If ($mode=='edit' && $type_acte == 'Convention') {
						$result['liste'] .= 
						"<center><input style='text-align:center;border:0;background:none;' readonly disabled class='editbox' type='text' id='surfmu_".$donnees['tbl_cad_site_id']."' size='8' value='" . conv_are($donnees['tbl_surf_mu']) . "'></center>";
					} Else {
						$result['liste'] .= 
						"<center><a style='margin-left:0' href='$lien'>" . conv_are($donnees['tbl_surf_mu']) . "</a></center>";						
					}
					$result['liste'] .= 				
					"</td>
					<td rowspan='$rowspan' width='20px' "; If(!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false){$result['liste'] .= "style='background:#cccccc;border-color:#cccccc;' ";} $result['liste'] .=">";
					If ($mode=='edit' && $type_acte == 'Convention') {
						$result['liste'] .= 
						"<img id='img_".$donnees['tbl_cad_site_id']."' style='cursor:pointer;' src='".ROOT_PATH."images/edit.png' width='15px' onclick=\"updateSurfmu('" . conv_are($donnees['dcntpa']) . "', '".$donnees['tbl_cad_site_id']."', '".$champ_id."');\">";
					} Else {
						$result['liste'] .= 
						"&nbsp;";
					}
					$result['liste'] .= 		
					"</td>
				</tr>";
				} Else {
					$result['liste'] .= "
					<td rowspan='$rowspan' onclick=\"document.location='$lien'\">
						<center><a style='margin-left:0' href='$lien'> </a></center>
					</td>
					<td rowspan='$rowspan' onclick=\"document.location='$lien'\">
						<center><a style='margin-left:0' href='$lien'> </a></center>
					</td>
					<td class='filtre_mfu' rowspan='$rowspan' onclick=\"document.location='$lien'\">
						<center><a style='margin-left:0' href='$lien'>".$donnees['tbl_mfu_lib']."</a></center>
					</td>
					<td class='filtre_priorite' rowspan='$rowspan' onclick=\"document.location='$lien'\">
						<center><a style='margin-left:0' href='$lien'>" . conv_are($donnees['tbl_surf_mu']) . "</a></center>
					</td>
					<td rowspan='$rowspan' width='20px'>
						&nbsp;
					</td>
				</tr>";
				}
				
			} Else { // Si il y a des lots, il faut récupérer les informations particulières à chacun et affiche le tout avec des rowspans
				$dnulot = preg_split("/,/", $donnees['tbl_dnulot']); // Transformation en variable tableau des dnulot
				$dcntlo = preg_split("/,/", $donnees['tbl_dcntlo']); // Transformation en variable tableau des dcntlo
				$mfu_lib = preg_split("/,/", $donnees['tbl_mfu_lib']); // Transformation en variable tableau des mfu de chaque lot
				$surf_mu = preg_split("/,/", $donnees['tbl_surf_mu']); // Transformation en variable tableau du paf en simplifiant le libellé
				$cad_site_id = preg_split("/,/", $donnees['tbl_cad_site_id']); // Transformation en variable tableau du paf en simplifiant le libellé
				
				// Pour chaque lot on affiche une ligne en faisant attention aux rowspans
				For ($i=0; $i<count($dnulot); $i++) {
					// Création du lien vers la fiche correspondante
					$vget = "par_id:$donnees[par_id];lot_id:$dnulot[$i];lot_id_sel:$dnulot[$i];mode:consult;";
					$vget_crypt = base64_encode($vget);
					$lien_lot[$i] = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_parcelle.php?d=$vget_crypt#lot";
					
					// Il ne faut pas ajouter de nouveau tr pour le premier lot, par contre il le faut pour tous les autres.
					If ($i>0) {
						If(substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){
							$result['liste'] .= "<tr class='rowspan toggle_tr l$j hover$nbr_real_tr' rowspan='$i' "; If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien_lot[$i]'\";";} $result['liste'] .= " onmouseover=\"hoverOnRowspan('master', 'hover$nbr_real_tr')\" onmouseout=\"hoverOutRowspan('master', 'hover$nbr_real_tr')\">";
						} Else {
							$result['liste'] .= "<tr class='rowspan toggle_tr l$j hover$nbr_real_tr' rowspan='$i' onclick=\"document.location='$lien_lot[$i]'\" onmouseover=\"hoverOnRowspan('master', 'hover$nbr_real_tr')\" onmouseout=\"hoverOutRowspan('master', 'hover$nbr_real_tr')\">";
						}
					}
					
					// Les styles des lignes sont particuliers avec des arrondis aux extrémités et une bordure au survol de la souris
					// A cause des tr et rowspan il faut adapter les styles, en particulier pour le premier et le dernier lot
					If (count($dnulot) > 1) {
						If ($i == 0) { // Cas pour le premier lot
							$class = 'first';
							$style_td_first = 'border-bottom:0;border-radius:0;';
							$style_td = 'border-bottom:0;border-radius:0;';
							$style_td_last = 'border-bottom:0;border-bottom-right-radius:0px;border-top-right-radius:20px;';
						} Else If ($i > 0 && $i < (count($dnulot) -1)) { // Cas pour tous les lots du 'milieu'
							$class = 'middle';
							$style_td_first = 'border:0;border-radius:0;';
							$style_td = 'border:0;border-radius:0;';
							$style_td_last ='border:0;border-radius:0;';
						} Else If ($i == (count($dnulot)-1)) { // Cas pour le dernier lot
							$class = 'last';
							$style_td_first = 'border-top:0;border-radius:0;';
							$style_td = 'border-top:0;border-radius:0;';
							$style_td_last = 'border-top:0;border-bottom-right-radius:10px;border-top-right-radius:0px;';
						}
					} Else {
						$class = '';
						$style_td_first = '';
						$style_td = '';
						$style_td_last = '';
					}
					If (!empty($donnees['tbl_motif_fin']) && strpos($donnees['tbl_motif_fin'], '1')!==false) {
						$class .= '';
						$style_td_first .= ' background:#cccccc;border-color:#cccccc;';
						$style_td .= ' background:#cccccc;border-color:#cccccc;';
						$style_td_last .= ' background:#cccccc;border-color:#cccccc;';
					}
					
					If(substr($page, strrpos($page, '/')) == '/fiche_mfu.php'){
						// On peut maintenant intégrer les cellules avec tous les parmaètres définis précédemment
						$result['liste'] .= "
						<td style='$style_td_first' class='rowspan $class' rowspan='1'  "; If($mode=='consult'){$result['liste'] .= "onclick=\"document.location='$lien_lot[$i]'\";";} $result['liste'] .= " style='width:50%;'>
							<center><a style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien_lot[$i]'";} $result['liste'] .= ">$dnulot[$i]</a></center>
						</td>
						<td style='$style_td' class='rowspan $class' rowspan='1'>
							<center><a style='margin-left:0'"; If($mode=='consult'){$result['liste'] .= "href='$lien_lot[$i]'";} $result['liste'] .= ">" . conv_are($dcntlo[$i]) . "</a></center>
						</td>
						<td class='filtre_mfu rowspan $class' style='$style_td' rowspan='1'>
							<center><a id='a_type_mu_".$cad_site_id[$i]."' style='margin-left:0' "; If($mode=='consult'){$result['liste'] .= "href='$lien_lot[$i]'";} $result['liste'] .= ">$mfu_lib[$i]</a></center>
						</td>
						<td class='filtre_priorite rowspan $class' style='$style_td' rowspan='1'>";
						If ($mode=='edit' && $type_acte == 'Convention') {
							$result['liste'] .= 
							"<center><input style='text-align:center;border:0;background:none;' class='editbox' type='text' id='surfmu_".$cad_site_id[$i]."' size='8' value='" . conv_are($surf_mu[$i]) . "'></center>";
						} Else {
							$result['liste'] .= 
							"<center><a style='margin-left:0' href='$lien_lot[$i]'>" . conv_are($surf_mu[$i]) . "</a></center>";
						}
						$result['liste'] .= 
						"</td>
						<td width='20px' class='rowspan $class' style='$style_td_last' rowspan='1'>";
						If ($mode=='edit' && $type_acte == 'Convention') {
							$result['liste'] .= 
							"<img id='img_".$cad_site_id[$i]."' style='cursor:pointer;' src='".ROOT_PATH."images/edit.png' width='15px' onclick=\"updateSurfmu('" . conv_are($dcntlo[$i]) . "', '".$cad_site_id[$i]."', '".$champ_id."');\">";
						} Else {
							$result['liste'] .= 
							"&nbsp;";
						}
						$result['liste'] .= 		
						"</td>
					</tr>";
					} Else {
						// On peut maintenant intégrer les cellules avec tous les parmaètres définis précédemment
						$result['liste'] .= "
						<td style='$style_td_first' class='rowspan $class' rowspan='1' onclick=\"document.location='$lien_lot[$i]'\" style='width:50%;'>
							<center><a style='margin-left:0' href='$lien_lot[$i]'>$dnulot[$i]</a></center>
						</td>
						<td style='$style_td' class='rowspan $class' rowspan='1'>
							<center><a style='margin-left:0' href='$lien_lot[$i]'>" . conv_are($dcntlo[$i]) . "</a></center>
						</td>
						<td class='filtre_mfu rowspan $class' style='$style_td' rowspan='1'>
							<center><a style='margin-left:0' href='$lien_lot[$i]'>$mfu_lib[$i]</a></center>
						</td>
						<td class='filtre_priorite rowspan $class' style='$style_td' rowspan='1'>
							<center><a style='margin-left:0' href='$lien_lot[$i]'>" . conv_are($surf_mu[$i]) . "</a></center>
						</td>
						<td width='20px' class='rowspan $class' style='$style_td_last' rowspan='1'>
							&nbsp;
						</td>
					</tr>";
					}
				}
			}
			// Petite bidouille pour alterner la couleur des lignes
			If($j == 0) {
				$j=1;
			} Else {
				$j=0;
			}
			$nbr_real_tr++; // On implémente de 1 la variable permettant d'identifier le numéro de la ligne du tableau
		}
		
		// Cette ligne est commentée car on affiche l'intégralité des parcelles en une fois. Donc pas besoin de lien vers des pages suivantes ou précédentes
		// $result['liste'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_parcelles->rowCount(), 9, "create_lst_parc('modif', offset, '$champ_id')");
		// Comme la ligne préceédente n'est pas active, ajout d'une ligne pour coller aux tableaux limités
		$result['liste'] .= "
			<tr class='no-export'>
			</tr>";
		
		// On ferme le curseur de lecteur de la requête
		$res_lst_parcelles->closeCursor();
		
		// Variable permettant de créer le tableau de l'historique des parcelles 
		$result['histo'] = "
			<tr>
				<td style='text-align:center;' colspan='8'>
					<label class='label' style='font-size:11pt'>Historique des parcelles/lots</label>				
					<img width='15px' onclick=\"cacheMess('explorateur_histo');changeOpac(100, 'onglet');\" src='".ROOT_PATH."images/quitter.png' style='cursor:pointer;float:right;'>
				</td>									
			</tr>
			<tr>
				<td class='filtre' align='center'>
					<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='30' value='Commune' DISABLED> 
				</td>
				<td class='filtre' align='center'>
					<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='6' value='Section' DISABLED> 
				</td>
				<td class='filtre' align='center' width='90px'>
					<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='8' value='Num' DISABLED> 
				</td>
				<td align='center' width='120px'>
					<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='12' value='Contenance' DISABLED> 
				</td>
				<td class='filtre' align='center' width='90px'>
					<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='10' value='TYPE_PROP' DISABLED> 
				</td>
				<td align='center' width='180px'>
					<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_lots' value='Lots' DISABLED> 
				</td>
				<td align='center'>
					<input style='text-align:center;background-color:#accf8a;width:95px;' class='editbox' type='text' id='label_lots' value='Historisée le' DISABLED> 
				</td>
				<td align='center' style='width:165px;'>
					<input style='text-align:center; background-color:#accf8a;width:105px;' class='editbox' type='text' id='label_lots' value='Motif' DISABLED> 
				</td>
			</tr>
		";
		
		// Cette ligne est commentée car on affiche l'intégralité des parcelles en une fois. Donc pas besoin de lien vers des pages suivantes ou précédentes
		// $result['histo'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_histo_parcelles->rowCount(), 9, "create_lst_parc('modif', offset, '$champ_id')");		
		
		$j=0; // Variable petite bidouille pour alterner la couleur des lignes
		// Création du tableau ligne par ligne à partir du résultat de la requête
		While ($donnees = $res_lst_histo_parcelles->fetch()) {
			$rq_nbr_lot_assoc = "SELECT count(lot_id) FROM cadastre.lots_cen WHERE par_id = ?";
			$res_nbr_lot_assoc = $bdd->prepare($rq_nbr_lot_assoc);
			$res_nbr_lot_assoc->execute(array($donnees['par_id']));					
			$nbr_lot_assoc_temp = $res_nbr_lot_assoc->fetch();
			$nbr_lot_assoc = $nbr_lot_assoc_temp[0];
			$res_nbr_lot_assoc->closeCursor();
			
			// Création du lien vers la fiche correspondante
			$vget = "par_id:$donnees[par_id];lot_id:$donnees[par_id];mode:consult;";
			If (substr_count($donnees['tbl_motif_fin_cad'], "suppr") == $nbr_lot_assoc && count(preg_split("/,/", $donnees['tbl_dnulot'])) == $nbr_lot_assoc) {
				$vget .= 'parc_suppr:oui;';
			}
			$vget_crypt = base64_encode($vget);
			$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_parcelle.php?d=$vget_crypt#general";
			
			// Implémentation de la construction du tableau
			$result['histo'] .= "
			<tr class='toggle_tr l$j'>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[com_nom]</a></center>
				</td>	
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[ccosec]</a></center>
				</td>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[dnupla]</a></center>
				</td>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>" . conv_are($donnees['dcntpa']) . "</a></center>
				</td>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[typprop]</a></center>
				</td>";				
				
			// On arrive aux cellules prévues pour les lots (identifiant et contenance)
			If (count(preg_split("/,/", $donnees['tbl_dnulot'])) == 1 AND $donnees['tbl_dnulot'] == '' AND count(preg_split("/,/", $donnees['tbl_motif_fin_cad'])) == 1) { // Si il n'y a pas de lot on ne remplit rien dans les trois prochaines cellules
				// Implémentation de la construction du tableau
				$result['histo'] .= "
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>&nbsp;</a></center>
				</td>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>";
				If ($donnees['tbl_date_fin_cad'] <> 'N.D.') {
					$result['histo'] .= date_transform($donnees['tbl_date_fin_cad']);
				} Else {
					$result['histo'] .= date_transform($donnees['tbl_date_fin_site']);
				}
				$result['histo'] .= "</a></center>
				</td>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[tbl_motif_fin_cad]</a></center>
				</td>";
			} Else { // Si il y a des lots on commence par récupérer les info pour chacun
				$dnulot = preg_split("/,/", $donnees['tbl_dnulot']); // Transformation en variable tableau des dnulot
				$dcntlo = preg_split("/,/", $donnees['tbl_dcntlo']); // Transformation en variable tableau des dcntlo
				$date_fin_cad = preg_split("/,/", $donnees['tbl_date_fin_cad']); // Transformation en variable tableau des dates de fin de lot
				$motif_fin_cad = preg_split("/,/", $donnees['tbl_motif_fin_cad']); // Transformation en variable tableau des motifs de fin
				$date_fin_site = preg_split("/,/", $donnees['tbl_date_fin_site']); // Transformation en variable tableau des dates de fin au niveau du site
				
				// Implémentation de la construction du tableau
				$result['histo'] .= "
				<td style='text-align:center'>
					<center>
						<table class='no-border' border='1' width='100%' style='text-align:center;float:none;'>
							<tbody class='no-border' style='width:100%'>";
							// Pour chaque lot on affiche les infos nécessaires
							For ($i=0; $i<count($motif_fin_cad); $i++) {
								// Création du lien vers la fiche correspondante
								If (count(preg_split("/,/", $donnees['tbl_dnulot'])) > 0 AND $donnees['tbl_dnulot'] != '') {
									$vget = "par_id:$donnees[par_id];lot_id:$dnulot[$i];lot_id_sel:$dnulot[$i];mode:consult;";
									$vget_crypt = base64_encode($vget);
									$lien_lot[$i] = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_parcelle.php?d=$vget_crypt#lot";
								} Else {
									$vget = "par_id:$donnees[par_id];lot_id:$donnees[par_id];mode:consult;";
									$vget_crypt = base64_encode($vget);
									$lien_lot[$i] = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_parcelle.php?d=$vget_crypt#general";
								}
								
								If (substr_count($motif_fin_cad[$i], "suppr") > 0 && count($dnulot) == $nbr_lot_assoc) {
									$vget .= 'parc_suppr:oui;';
								}
								
								If (count(preg_split("/,/", $donnees['tbl_dnulot'])) > 0 AND $donnees['tbl_dnulot'] != '') {
									// Implémentation de la construction du tableau
									$result['histo'] .= "
									<tr onclick=\"document.location='$lien_lot[$i]'\" style='width:100%'>
										<td class='no-border' onclick=\"document.location='$lien_lot[$i]'\" style='width:50%'>
											<center><a style='margin-left:0' href='$lien_lot[$i]'>$dnulot[$i]</a></center>
										</td>
										<td class='no-border'>
											<center><a style='margin-left:0' href='$lien_lot[$i]'>" . conv_are($dcntlo[$i]) . "</a></center>
										</td>
									</tr>";
								} Else {
									$result['histo'] .= "
									<tr onclick=\"document.location='$lien_lot[$i]'\" style='width:100%'>
										<td class='no-border' onclick=\"document.location='$lien_lot[$i]'\" style='width:50%'>
											<center><a style='margin-left:0' href='$lien_lot[$i]'>&nbsp;</a></center>
										</td>
										<td class='no-border'>
											<center><a style='margin-left:0' href='$lien_lot[$i]'>&nbsp;</a></center>
										</td>
									</tr>";
								}
							}
							$result['histo'] .= "
							</tbody>
						</table>
					</center>
				</td>";
				// Implémentation de la construction du tableau
				$result['histo'] .= "
				<td style='text-align:center'>
					<center>
						<table class='no-border' border='1' width='100%' style='text-align:center;float:none;'>
							<tbody class='no-border' style='width:100%'>";
							// Pour chaque lot on affiche les infos nécessaires
							For ($i=0; $i<count($motif_fin_cad); $i++) {
								$result['histo'] .= "
								<tr onclick=\"document.location='$lien_lot[$i]'\" style='width:100%;'>
									<td class='no-border' style='width:100%'>
										<center><a style='margin-left:0' href='$lien_lot[$i]'>";
										If ($date_fin_cad[$i] <> 'N.D.') {
											$result['histo'] .= date_transform($date_fin_cad[$i]);
										} Else {
											$result['histo'] .= date_transform($date_fin_site[$i]);
										}
										$result['histo'] .= "</a></center>
									</td>
								</tr>";
							}
							$result['histo'] .= "
							</tbody>
						</table>
					</center>
				</td>";
				// Implémentation de la construction du tableau
				$result['histo'] .= "
				<td style='text-align:center'>
					<center>
						<table class='no-border' border='1' width='100%' style='text-align:center'>
							<tbody class='no-border' style='width:100%;'>";
							// Pour chaque lot on affiche les infos nécessaires
							For ($i=0; $i<count($motif_fin_cad); $i++) {
								$result['histo'] .= "
								<tr onclick=\"document.location='$lien_lot[$i]'\" style='width:100%;'>
									<td class='no-border' style='width:100%;'>
										<center><a style='margin-left:0' href='$lien_lot[$i]'>";
										If ($date_fin_cad[$i] <> 'N.D.') {
											$result['histo'] .= str_replace('Objet supprimé', 'Lot supprimé', $motif_fin_cad[$i]);
										} Else {
											$result['histo'] .= 'Dissocié du site';
										}
										$result['histo'] .= "</a></center>
									</td>
								</tr>";
							}
							$result['histo'] .= "
							</tbody>
						</table>
					</center>
				</td>";
			}
			
			// Petite bidouille pour alterner la couleur des lignes
			If($j == 0) {
				$j=1;
			} Else {
				$j=0;
			}
		}
		
		If ($res_lst_parcelles->rowCount() > 0)
			{$result['histo'] .= "</tr>";}
		
		// Cette ligne est commentée car on affiche l'intégralité des parcelles en une fois. Donc pas besoin de lien vers des pages suivantes ou précédentes
		// $result['histo'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_parcelles->rowCount(), 9, "create_lst_parc('modif', offset, '$champ_id')");
		
		$res_lst_parcelles->closeCursor();
		$res_lst_histo_parcelles->closeCursor();
		$bdd = null;
		print json_encode($result);
	}
	
	// Cette fonction permet de récupérer la liste des propriétaires que l'on retrouve dans les différentes fiches (site, mfu, parcelle) mais aussi dans la recherche par propriétaire.
	// Elle permet également d'adapter la liste des propriétaires en fonction des filtres
	function fct_lst_proprio($data) {
		include('foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}	
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$page = $data['params']['page']; // Cette variable récupère le nom de la page fiche d'origine
		$champ_id = $data['params']['champ_id']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
		$nom = $data['params']['nom'];
		$site_id = $data['params']['site_id'];
		$type_acte = $data['params']['type_acte'];
		$offset = $data['params']['offset'];
		$nbr_result = 100;
		
		// Pour simplifier la procédure on détermine le prefixe utilisé pour l'appelle de certaines tables et certains champs dans la requête
		// Cette particularité est utilisée uniquement pour la page fiche_mfu.php
		If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php' && $type_acte == 'Acquisition') {
			$suffixe = 'mf';
		} Else If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php' && $type_acte == 'Convention') {
			$suffixe = 'mu';
		}
				
		// initialisation d'une variable correpondant à la clause where de la requete de sélection des proprios
		$where = "WHERE t1.par_id = t2.par_id AND t2.lot_id = t3.lot_id AND t3.cad_cen_id = t4.cad_cen_id AND t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper";
		
		// En fonction de la page fiche d'origine on implémente la clause WHERE avec les élémens nécessaires
		If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$rq_statut_mfu = 
			"SELECT 
				r_$suffixe"."statut.statut"."$suffixe"."_id AS statut_id
			FROM
				foncier.r_"."$suffixe"."statut 
				JOIN (SELECT r_"."$suffixe"."statut."."$suffixe"."_id, Max(r_"."$suffixe"."statut.date) AS statut_date FROM foncier.r_"."$suffixe"."statut GROUP BY r_"."$suffixe"."statut."."$suffixe"."_id) AS tmax ON (r_"."$suffixe"."statut.date = tmax.statut_date)
			WHERE
				r_"."$suffixe"."statut."."$suffixe"."_id = :acte_id
			";
			$res_statut_mfu = $bdd->prepare($rq_statut_mfu);
			$res_statut_mfu->execute(array('acte_id'=>$champ_id));
			$statut_mfu = $res_statut_mfu->fetch();
			$res_statut_mfu->closeCursor();
			
			If ($type_acte == 'Convention' && ($statut_mfu['statut_id'] == 2 || $statut_mfu['statut_id'] == 4)) {
				$rq_dates_mu = 
				"SELECT date_effet_conv, date_fin_conv
				FROM foncier.mu_conventions
				WHERE mu_id = :acte_id";
				$res_dates_mu = $bdd->prepare($rq_dates_mu);
				$res_dates_mu->execute(array('acte_id'=>$champ_id));
				$dates_mu = $res_dates_mu->fetch();
				$res_dates_mu->closeCursor();
				
				$where .= "
					AND t3.date_fin >= '".$dates_mu['date_fin_conv']."' AND 
					t4.date_fin >= '".$dates_mu['date_fin_conv']."' 
					";	
			} Else {
				$where .= "
					AND t3.date_fin = 'N.D.' AND 
					t4.date_fin = 'N.D.' 
					";
			}
		} Else If (substr($page, strrpos($page, '/')) != '/foncier_liste_proprio.php') {
			$where .= "
				AND t3.date_fin = 'N.D.' AND 
				t4.date_fin = 'N.D.' 
				";
		}
		
		If (substr($page, strrpos($page, '/')) == '/fiche_site.php') {
			$where .= " AND t4.site_id = '$champ_id'";
		} Else If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$where .= " AND r_cad_site_$suffixe.cad_site_id = t4.cad_site_id AND r_cad_site_$suffixe.$suffixe"."_id = '$champ_id'";
		}
		
		// Si le filtre du nom est rempli on ajoute un élément dans la clause where
		If ($nom <> 'NOM' AND $nom <> '' AND $where <> '') {
			$where .= " AND (ddenom ILIKE '" . str_replace(' ', '/', $nom) . "%' OR t7.nom_usage ILIKE '" . $nom . "%')";
		}
		// Si le filtre du site est rempli on ajoute un élément dans la clause where
		If ($site_id <> 'SITE ID' AND $site_id <> '' AND $where <> '') {
			$where .= " AND t4.site_id ILIKE '%" . $site_id . "%'";
		}
		
		// Implémentation de la variable $where avec la clause GROUP BY
		$where .= " GROUP BY t7.dnuper, d_ccoqua.particule_lib, t7.ddenom, t7.nom_usage, t7.nom_jeunefille, t7.prenom_usage, t7.nom_conjoint, t7.prenom_conjoint";
		
		// Construction de la requête de sélection des proprios
		$rq_lst_proprios = 
		"SELECT DISTINCT t7.dnuper, (CASE WHEN d_ccoqua.particule_lib != '' THEN d_ccoqua.particule_lib ELSE 'N.P.' END) AS particule_lib, t7.ddenom, t7.nom_usage, t7.nom_jeunefille, t7.prenom_usage, t7.nom_conjoint, t7.prenom_conjoint, sum(CASE WHEN t3.date_fin = 'N.D.' AND t4.date_fin = 'N.D.' THEN 1 ELSE 0 END) as nbr_parcelle, sum(t1.dcntpa) as surf_total
		FROM cadastre.parcelles_cen t1, cadastre.lots_cen t2, cadastre.cadastre_cen t3, foncier.cadastre_site t4, cadastre.cptprop_cen t5, cadastre.r_prop_cptprop_cen t6, cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua) ";
		
		// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires
		If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
			$rq_lst_proprios .= ", foncier.r_cad_site_$suffixe ";
		}
		
		// Implémentation de la requête avec la clause ORDER BY et des éléments pour limiter le nombre de résultats en sortie
		$rq_lst_proprios .= $where . " ORDER BY nom_usage OFFSET $offset LIMIT $nbr_result";
		// echo $rq_lst_proprios;
		// Lancement et execution de la requête
		$res_lst_proprios = $bdd->prepare($rq_lst_proprios);
		$res_lst_proprios->execute();
		
		// Déclaration de la variable tableau de renvoie vers AJAX
		$result['liste'] = "";
				
		// Implémentation de la variable $result avec les liens suivant précedent en haut de page
		$result['liste'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_proprios->rowCount(), 3, "create_lst_proprio(offset, '$champ_id')");
		
		$j=0; // Variable petite bidouille pour alterner la couleur des lignes
		// Création du tableau ligne par ligne à partir du résultat de la requête
		While ($donnees = $res_lst_proprios->fetch()) {
			//Variable permettant d'avoir un libellé de proprio simple à utiliser
			$proprio_lib = proprio_lib($donnees['particule_lib'], $donnees['nom_usage'], $donnees['prenom_usage'], $donnees['ddenom']);
			
			//Création du lien vers la fiche correspondante
			$vget = "dnuper:".$donnees['dnuper'].";mode:consult;proprio_lib:$proprio_lib;";
			$vget_crypt = base64_encode($vget);
			$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt#general";
			
			// Implémentation de la variable retournant le tableau html avec des contrôles sur le contenu
			$result['liste'] .= "<tr class='toggle_tr l$j' onclick=\"document.location='$lien'\">";
			$result['liste'] .= "<td style='text-align:right;"; 
			If ($donnees['nbr_parcelle'] == 0) {
				$result['liste'] .= 'background:#cccccc;border-color:#cccccc;';
			}
			$result['liste'] .= "'><a href='$lien'>$proprio_lib";
			If (isset($donnees['nom_jeunefille']) AND $donnees['nom_jeunefille'] != 'N.P.') {
				$result['liste'] .= " (née $donnees[nom_jeunefille])";
			}
			$result['liste'] .= "</a></td>
				<td style='"; 
			If ($donnees['nbr_parcelle'] == 0) {
				$result['liste'] .= 'background:#cccccc;border-color:#cccccc;';
			}
			
			$result['liste'] .= "'><a href='$lien'><i>propriétaire de $donnees[nbr_parcelle] parcelle"; 
			If ($donnees['nbr_parcelle'] > 1) {
				$result['liste'] .= "s";
			}
			If (substr($page, strrpos($page, '/')) == '/foncier_liste_proprio.php') {
				$result['liste'] .= " sur nos sites";
				If ($donnees['nbr_parcelle'] != 0) {
					$result['liste'] .= " pour une surface de " . conv_are($donnees['surf_total']) . " ha.a.ca</i></a></td>";
				}
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_site.php') {
				$result['liste'] .= " sur ce site pour une surface de " . conv_are($donnees['surf_total']) . " ha.a.ca</i></a></td>";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_mfu.php') {
				$result['liste'] .= " sur cet acte ";
				If ($statut_mfu['statut_id'] <= 1) {
					$result['liste'] .= "pour une surface de " . conv_are($donnees['surf_total']) . " ha.a.ca</i></a></td>";
				}
			} 
			
			// Petite bidouille pour alterner la couleur des lignes
			If($j == 0 && $donnees['nbr_parcelle'] > 0) {
				$j=1;
			} Else {
				$j=0;
			}
		}
		
		// Implémentation de la variable $result avec les liens suivant précedent en bas de page
		$result['liste'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_proprios->rowCount(), 3, "create_lst_proprio(offset, '$champ_id')");
	
		// Ferme le curseur et désactive la connection à la base
		$res_lst_proprios->closeCursor();
		$bdd = null;
			
		print json_encode($result);
	}	
		
	// Cette fonction permet de récupérer la liste des actes que l'on retrouve dans les différentes fiches (site, proprio, parcelle)
	// Elle permet également d'adapter la liste des actes en fonction des filtres	
	function fct_lst_mf($data) {
		include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$page = $data['params']['page']; // Cette variable récupère le nom de la page fiche d'origine
		$champ_id = $data['params']['champ_id']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
		$acte = $data['params']['acte'];
		$acte_id = $data['params']['acte_id'];
		$site_id_mfu = $data['params']['site_id_mfu'];
		$libelle = $data['params']['libelle'];
		$type_mf = $data['params']['type_mf'];
		$date = $data['params']['date'];
		$statut = $data['params']['statut'];
		$doc_pdf = $data['params']['doc_pdf'];
		$offset = $data['params']['offset'];
		$nbr_result = 600000;
		
		// Création de la requête 
		$rq_lst_mfu = 
		"SELECT 
			type_acte,
			site_id,";
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_mfu .= 
				"dnuper, ";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_mfu .= 
				"par_id, ";
			}	
			$rq_lst_mfu .= 
			"acte_id,
			acte_lib,
			acte_typ,
			acte_date,
			acte_statut,
			acte_doc_pdf,
			CASE WHEN acte_id NOT IN (SELECT DISTINCT v_prob_cadastre_cen.mfu_id FROM alertes.v_prob_cadastre_cen UNION SELECT DISTINCT v_prob_mfu_baddata.mfu_id FROM alertes.v_prob_mfu_baddata) THEN 
				COALESCE(sum(surf_maitrise), 0)::text
			ELSE 
				'Données pas à jour'
			END as surf_maitrise	
		FROM
			(SELECT DISTINCT 
				'Acquisition' as type_acte, 
				t4.site_id, ";
			// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires	
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_mfu .= 
				"t7.dnuper, ";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_mfu .= 
				"t1.par_id, ";
			}	
				// Implémentation de la requête, partie commune pour toutes les fiches
				$rq_lst_mfu .= "	
				mf_acquisitions.mf_id as acte_id, 
				mf_acquisitions.mf_lib as acte_lib, 
				d_typmf.typmf_lib as acte_typ, 
				mf_acquisitions.date_sign_acte as acte_date, 
				d_statutmf.statutmf as acte_statut, 
				(CASE WHEN (substr(mf_acquisitions.mf_pdf,  length(mf_acquisitions.mf_pdf)-3, 4)) = '.pdf' THEN 'OUI' ELSE 'NON' END) as acte_doc_pdf,
				COALESCE(sum(lots_cen.dcntlo), 0) as surf_maitrise
			FROM 
				cadastre.lots_cen 
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN foncier.r_cad_site_mf USING (cad_site_id)
				JOIN foncier.mf_acquisitions USING (mf_id)
				JOIN foncier.d_typmf USING (typmf_id)
				JOIN foncier.r_mfstatut USING (mf_id)";
			// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_mfu .= 
				" JOIN cadastre.cptprop_cen t5 USING (dnupro) 
				JOIN cadastre.r_prop_cptprop_cen t6 USING (dnupro)
				JOIN cadastre.proprios_cen t7 USING (dnuper)	 
				";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_mfu .= 
				" JOIN cadastre.parcelles_cen t1 USING (par_id)";
			}		
			// Implémentation de la requête, partie commune pour toutes les fiches
			$rq_lst_mfu .= "	
				JOIN foncier.d_statutmf USING (statutmf_id), 
				(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
			WHERE 
				--t3.date_fin = 'N.D.' AND 
				t4.date_fin = 'N.D.' AND 
				r_cad_site_mf.date_sortie = 'N.D.' AND
				r_mfstatut.date = tt1.statut_date AND 
				r_mfstatut.mf_id = tt1.mf_id
			GROUP BY t4.site_id, ";
			// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires	
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_mfu .= 
				"t7.dnuper, ";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_mfu .= 
				"t1.par_id, ";
			}	
				// Implémentation de la requête, partie commune pour toutes les fiches
				$rq_lst_mfu .= "acte_id, acte_typ, acte_statut
			UNION
			SELECT DISTINCT
				'Convention' as type_acte, 
				t4.site_id, ";
			// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires	
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_mfu .= 
				"t7.dnuper, ";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_mfu .= 
				"t1.par_id, ";
			}			
				// Implémentation de la requête, partie commune pour toutes les fiches
				$rq_lst_mfu .= "
				mu_conventions.mu_id as acte_id, 
				mu_conventions.mu_lib as acte_lib, 
				d_typmu.typmu_lib as acte_typ, 
				mu_conventions.date_effet_conv as acte_date, 
				d_statutmu.statutmu as acte_statut, 
				(CASE WHEN (substr(mu_conventions.mu_pdf,  length(mu_conventions.mu_pdf)-3, 4)) = '.pdf' THEN 'OUI' ELSE 'NON' END) as acte_doc_pdf,
				COALESCE(sum(CASE WHEN r_cad_site_mu.surf_mu is not Null THEN r_cad_site_mu.surf_mu ELSE lots_cen.dcntlo END), 0) as surf_maitrise
			FROM 
				cadastre.lots_cen 
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
				JOIN foncier.r_cad_site_mu USING (cad_site_id)
				JOIN foncier.mu_conventions USING (mu_id)
				JOIN foncier.d_typmu USING (typmu_id)
				JOIN foncier.r_mustatut USING (mu_id)";
			// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_mfu .= 
				" JOIN cadastre.cptprop_cen t5 USING (dnupro) 
				JOIN cadastre.r_prop_cptprop_cen t6 USING (dnupro)
				JOIN cadastre.proprios_cen t7 USING (dnuper)	 
				";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_mfu .= 
				" JOIN cadastre.parcelles_cen t1 USING (par_id)";
			}		
			// Implémentation de la requête, partie commune pour toutes les fiches
			$rq_lst_mfu .= "	
				JOIN foncier.d_statutmu USING (statutmu_id), 
				(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt1
			WHERE 
				--t3.date_fin = 'N.D.' AND 
				t4.date_fin = 'N.D.' AND 
				r_cad_site_mu.date_sortie = 'N.D.' AND
				r_mustatut.date = tt1.statut_date AND 
				r_mustatut.mu_id = tt1.mu_id
			GROUP BY t4.site_id, ";
			// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires	
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_mfu .= 
				"t7.dnuper, ";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_mfu .= 
				"t1.par_id, ";
			}	
				// Implémentation de la requête, partie commune pour toutes les fiches
				$rq_lst_mfu .= "acte_id, acte_typ, acte_statut
			ORDER BY type_acte , acte_date) tt1
		WHERE ";
		
		// En fonction de la page fiche d'origine on implémente la requête avec les élémens nécessaires
		If (substr($page, strrpos($page, '/')) == '/fiche_site.php') {
			$where_lst_mfu = "site_id = '$champ_id'";
		} ElseIf (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
			$where_lst_mfu = "dnuper = '$champ_id'";
		} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
			$where_lst_mfu = "par_id = '$champ_id'";
		}		
		
		// Si le filtre du type d'acte est rempli on ajoute un élément dans la clause where
		If ($acte <> 'Acte' AND $acte <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND type_acte ILIKE '%" . $acte . "%'";
		}
		// Si le filtre du site est rempli on ajoute un élément dans la clause where
		If ($site_id_mfu <> 'SITE ID' AND $site_id_mfu <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND site_id ILIKE '%" . $site_id_mfu . "%'";
		}
		// Si le filtre de l'acte_id est rempli on ajoute un élément dans la clause where
		If ($acte_id <> 'ID' AND $acte_id <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND acte_id ILIKE '%" . str_replace("'", "''", $acte_id) . "%'";
		}
		// Si le filtre du libellé de l'acte est rempli on ajoute un élément dans la clause where
		If ($libelle <> 'Libellé' AND $libelle <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND acte_lib ILIKE '%" . str_replace("'", "''", $libelle) . "%'";
		}
		// Si le filtre du type est rempli on ajoute un élément dans la clause where
		If ($type_mf <> 'Type d\'acte' AND $type_mf <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND acte_typ ILIKE '%" . str_replace("'", "''", $type_mf) . "%'";
		}
		// Si le filtre de la date est rempli on ajoute un élément dans la clause where
		If ($date <> 'Date' AND $date <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND acte_date ILIKE '%" . date_transform_inverse($date) . "%'";
		}
		// Si le filtre du statut est rempli on ajoute un élément dans la clause where
		If ($statut <> 'Statut' AND $statut <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND acte_statut ILIKE '%" . $statut . "%'";
		}
		// Si le filtre du PDF est rempli on ajoute un élément dans la clause where
		If ($doc_pdf <> 'PDF' AND $doc_pdf <> '' AND $where_lst_mfu <> '') {
			$where_lst_mfu .= " AND acte_doc_pdf ILIKE '%" . $doc_pdf . "%'";
		}
		
		// Finalisation de la construction de la requête
		$rq_lst_mfu .= $where_lst_mfu;
		$rq_lst_mfu .= " GROUP BY type_acte, site_id, ";
		If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
			$rq_lst_mfu .= 
			"dnuper, ";
		} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
			$rq_lst_mfu .= 
			"par_id, ";
		}	
		$rq_lst_mfu .= "acte_id, acte_lib, acte_typ, acte_date, acte_statut, acte_doc_pdf";
		// Lancement et execution de la requête
		// echo $rq_lst_mfu;
		$res_lst_mfu = $bdd->prepare($rq_lst_mfu);
		$res_lst_mfu->execute();
		
		// Déclaration de la variable tableau de renvoie vers AJAX
		$result['code'] = "
			<tr class='no-export'>
				<td colspan='9' style='text-align:center'>
					<label class='label'><i>" . $res_lst_mfu->rowCount() . " acte"; If ($res_lst_mfu->rowCount() > 1) {$result['code'] .= "s";} $result['code'] .= "</i></label>
				</td>
			</tr>";
			
		// Cette ligne est commentée car on affiche l'intégralité des actes en une fois. Donc pas besoin de lien vers des pages suivantes ou précédentes
		// $result['code'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_mfu->rowCount(), 7, "create_lst_mf('modif', offset, '$champ_id')");		
						
		$j=0; // Variable petite bidouille pour alterner la couleur des lignes
		// Création du tableau ligne par ligne à partir du résultat de la requête
		While ($donnees = $res_lst_mfu->fetch()) {
			// Contrôle de la date et transformation pour l'affichage
			If (strlen($donnees['acte_date']) == 10) {
				$acte_date = date_transform($donnees['acte_date']); // La fonction date_transform est décrite dans la page foncier_fct.php
			} Else {
				$acte_date = 'N.P.';
			}
			
			//Création du lien vers la fiche correspondante
			$vget = "type_acte:$donnees[type_acte];mode:consult;acte_id:$donnees[acte_id];";
			$vget_crypt = base64_encode($vget);
			$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_mfu.php?d=$vget_crypt#general";
			
			// Création de la variable tableau retournée côté AJAX pour afficher un tableau html
			If ($donnees['surf_maitrise'] == 'Données pas à jour') { 
				$result['code'] .= 
				"<tr class='prob_acte toggle_tr l$j' onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"Cet acte n'est pas à jour\">"; 
			} else {
				$result['code'] .= 
				"<tr class='toggle_tr l$j'>";
			};
			$result['code'] .= "
				<td class='filtre_acte' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[type_acte]</a></center>
				</td>";
				
			// En fonction de la page fiche d'origine on implémente la variable avec les élémens nécessaires	
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$result['code'] .= "
				<td class='filtre_site_id_mfu' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[site_id]</a></center>
				</td>";
			}
			
			// Implémentation de la variable, partie commune pour toutes les fiches
			$result['code'] .= "
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[acte_id]</a></center>
				</td>
				<td max-width='270px' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0; white-space:normal;' href='$lien'>" . retour_ligne($donnees['acte_lib'], 30) . "</a></center>
				</td>	
				<td class='filtre_type_mf' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0; white-space:normal;' href='$lien'>" . preg_replace('#\((.+)\)#i', '', $donnees['acte_typ']) . "</a></center>
				</td>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$acte_date</a></center>
				</td>
				<td class='filtre_statut' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[acte_statut]</a></center>
				</td>
				<td class='filtre_doc_pdf' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[acte_doc_pdf]</a></center>
				</td>
				<td style='display:none'>
					<center>$donnees[surf_maitrise]</center>
				</td>
				<td width='20px'>
					&nbsp;
				</td>";
			
			// Petite bidouille pour alterner la couleur des lignes
			If($j == 0) {
				$j=1;
			} Else {
				$j=0;
			}
		}
		
		If ($res_lst_mfu->rowCount() > 0) {
			$result['code'] .= "</tr>";
		}
		
		// Cette ligne est commentée car on affiche l'intégralité des parcelles en une fois. Donc pas besoin de lien vers des pages suivantes ou précédentes
		// $result['code'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_mfu->rowCount(), 7, "create_lst_mf('modif', offset, '$champ_id')");
		// Comme la ligne préceédente n'est pas active, ajout d'une ligne pour coller aux tableaux limités
		$result['code'] .= "
			<tr class='no-export'>
			</tr>";
		$result['nbr_result'] = $res_lst_mfu->rowCount();
		
		// Ferme le curseur et désactive la connection à la base
		$res_lst_mfu->closeCursor();
		$bdd = null;
		
		print json_encode($result);
	}
	
	// Cette fonction permet de récupérer la liste des sessions d'animation que l'on retrouve dans les différentes fiches (site, proprio, parcelle)
	// Elle permet également d'adapter la liste des sessions d'animation en fonction des filtres	
	function fct_lst_saf($data) {
		include('foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
		include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$page = $data['params']['page']; // Cette variable récupère le nom de la page fiche d'origine
		$champ_id = $data['params']['champ_id']; // Cette variable récupère l'identifiant adapté à la page fiche d'origine
		$libelle = $data['params']['libelle'];
		$date = $data['params']['date'];
		$origine = $data['params']['origine'];
		$statut = $data['params']['statut'];
		$offset = $data['params']['offset'];
		$nbr_result = 600000;
		
		// Création de la requête 
		$rq_lst_saf = 
		"
		SELECT *
		FROM
			(SELECT DISTINCT
				session_af.session_af_id,
				session_af.session_af_lib,
				session_af.date_creation || '_00' as date,
				saf_origine_lib,
				CASE WHEN t_statut.nbr_cad_site_id_en_cours > 0 THEN 'En cours' ELSE 'Terminée' END AS statut
			FROM
				animation_fonciere.session_af 
				JOIN animation_fonciere.rel_saf_foncier USING (session_af_id) 
				JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
				JOIN foncier.cadastre_site USING (cad_site_id)
				JOIN 
					(
						SELECT 
							session_af.session_af_id, 
							saf_origine_lib,
							count(rel_saf_foncier.rel_saf_foncier_id) as nbr_cad_site_id_en_cours
						FROM
							animation_fonciere.session_af
							JOIN animation_fonciere.rel_saf_foncier USING (session_af_id)
							JOIN animation_fonciere.d_saf_origines ON (origine_id = saf_origine_id)
						GROUP BY 
							session_af.session_af_id,
							saf_origine_lib
					) t_statut USING (session_af_id)";
			If (substr($page, strrpos($page, '/')) == '/fiche_proprio.php') {
				$rq_lst_saf .= "
				JOIN cadastre.cadastre_cen USING (cad_cen_id)
				JOIN cadastre.cptprop_cen USING (dnupro)
				JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
			WHERE 
				r_prop_cptprop_cen.dnuper = '$champ_id'";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_parcelle.php') {
				$rq_lst_saf .= "
				JOIN cadastre.cadastre_cen USING (cad_cen_id)
				JOIN cadastre.lots_cen USING(lot_id)
			WHERE 
				lots_cen.par_id = '$champ_id'";
			} Else If (substr($page, strrpos($page, '/')) == '/fiche_site.php') {
				$rq_lst_saf .= "
			WHERE 
				cadastre_site.site_id = '$champ_id'";
			}					
		$rq_lst_saf .= ") t1
		WHERE session_af_id > 0 ";	
		// Si le filtre du libellé est rempli on ajoute un élément dans la clause where
		If ($libelle <> 'Libellé' AND $libelle <> '') {
			$rq_lst_saf .= " AND session_af_lib ILIKE '%" . str_replace("'", "''", $libelle) . "%'";
		}
		// Si le filtre de la date est rempli on ajoute un élément dans la clause where
		If ($date <> 'Date' AND $date <> '') {
			$rq_lst_saf .= " AND date ILIKE '%" . date_transform_inverse($date) . "%'";
		}
		// Si le filtre du origine est rempli on ajoute un élément dans la clause where
		If ($origine <> 'Origine' AND $origine <> '') {
			$rq_lst_saf .= " AND origine_id = '%" . $origine . "%'";
		}
		// Si le filtre du statut est rempli on ajoute un élément dans la clause where
		If ($statut <> 'Statut' AND $statut <> '') {
			$rq_lst_saf .= " AND statut ILIKE '%" . $statut . "%'";
		}
		
		// Lancement et execution de la requête
		// echo $rq_lst_saf;
		$res_lst_saf = $bdd->prepare($rq_lst_saf);
		$res_lst_saf->execute();
		
		// Déclaration de la variable tableau de renvoie vers AJAX
		$result['code'] = "
			<tr class='no-export'>
				<td colspan='9' style='text-align:center'>
					<label class='label'><i>" . $res_lst_saf->rowCount() . " session"; If ($res_lst_saf->rowCount() > 1) {$result['code'] .= "s";} $result['code'] .= " d'animation</i></label>
				</td>
			</tr>";
		// $result['code'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_saf->rowCount(), 7, "create_lst_mf('modif', offset, '$champ_id')");		
						
		$j=0; // Variable petite bidouille pour alterner la couleur des lignes
		// Création du tableau ligne par ligne à partir du résultat de la requête
		While ($donnees = $res_lst_saf->fetch()) {
			// Contrôle de la date et transformation pour l'affichage
			If (strlen($donnees['date']) == 10) {
				$date_saf = substr(date_transform($donnees['date']), 3, 7); // La fonction date_transform est décrite dans la page foncier_fct.php
			} Else {
				$date_saf = 'N.P.';
			}
			
			//Création du lien vers la fiche correspondante
			$vget = "mode:consult;session_af_id:$donnees[session_af_id];session_af_lib:$donnees[session_af_lib];";
			$vget_crypt = base64_encode($vget);
			$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/module_af/fiche_saf.php?d=$vget_crypt#general";
			
			// Création de la variable tableau retournée côté AJAX pour afficher un tableau html
			$result['code'] .= 
			"<tr class='toggle_tr l$j'>
				<td class='filtre_acte' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[session_af_lib]</a></center>
				</td>
				<td onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$date_saf</a></center>
				</td>
				<td class='filtre_saf_origine' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[saf_origine_lib]</a></center>
				</td>
				<td class='filtre_saf_statut' onclick=\"document.location='$lien'\">
					<center><a style='margin-left:0' href='$lien'>$donnees[statut]</a></center>
				</td>
				<td width='20px'>
					&nbsp;
				</td>";
			
			// Petite bidouille pour alterner la couleur des lignes
			If($j == 0) {
				$j=1;
			} Else {
				$j=0;
			}
		}
		
		If ($res_lst_saf->rowCount() > 0) {
			$result['code'] .= "</tr>";
		}
		
		// Cette ligne est commentée car on affiche l'intégralité des parcelles en une fois. Donc pas besoin de lien vers des pages suivantes ou précédentes
		// $result['code'] .= fct_suiv_prec($offset, $nbr_result, $res_lst_saf->rowCount(), 7, "create_lst_mf('modif', offset, '$champ_id')");	
		$result['nbr_result'] = $res_lst_saf->rowCount();
		
		// Ferme le curseur et désactive la connection à la base
		$res_lst_saf->closeCursor();
		$bdd = null;
		
		print json_encode($result);
	}
	
	// Cette fonction permet de mettre à jour les droits par groupes d'utilisateur postgres.
	function fct_updateGrantGroup($data) {
		include('foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		If (in_array('grp_sig', $_SESSION['grp'])) {		
			//Paramétrage de connexion à la base de données
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}
			
			// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
			// Ces variables correspondent aux filtres présents dans les différentes pages fiches
			// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
			$rq_update = "UPDATE foncier.grant_group SET grant_group_droit = '".$data['params']['var_grant']."' WHERE grant_group_id = '".$data['params']['var_groname']."' AND grant_elt_id = ".$data['params']['var_group_elt_id'];
			// echo $rq_update;
			$res_update = $bdd->prepare($rq_update);
			$res_update->execute();
			$res_update->closeCursor();
			
			If ($res_update->rowCount() == 0) {
				$rq_insert = "INSERT INTO foncier.grant_group (grant_group_id, grant_elt_id, grant_group_droit) VALUES  ('".$data['params']['var_groname']."', ".$data['params']['var_group_elt_id'].", '".$data['params']['var_grant']."')";
				// echo $rq_insert;
				$res_insert = $bdd->prepare($rq_insert);
				$res_insert->execute();
				$res_insert->closeCursor();
			}
			$res_update->closeCursor();
			$bdd = null;
		} 
	}
	
	// Cette fonction permet de renseigner la surface maitrisée d'une parcelle conventionnée pour partie
	// Attention elle réinitialise la geom qu'il faudra recréer sur QGis
	function fct_updateSurfmuConfirm($data) {
		include('foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$contenance = $data['params']['var_contenance'];
		$new_surfmu = $data['params']['var_new_surfmu']; // Cette variable récupère le nom de la page fiche d'origine
		$cad_site_id = $data['params']['var_cad_site_id'];
		$mu_id = $data['params']['var_mu_id'];
		
		// Création de la requête 
		If ($contenance == $new_surfmu) {
			$rq_update = 
				"UPDATE foncier.r_cad_site_mu 
				SET surf_mu = NULL, geom = NULL
				WHERE cad_site_id = $cad_site_id AND mu_id = '$mu_id'";
		} Else {
			$rq_update = 
				"UPDATE foncier.r_cad_site_mu 
				SET surf_mu = " . conv_are_inverse($new_surfmu) . ", geom = NULL
				WHERE cad_site_id = $cad_site_id AND mu_id = '$mu_id'";
		}
		
		$res_update = $bdd->prepare($rq_update);
		$res_update->execute();
		$res_update->closeCursor();
		$bdd = null;
	}
	
	function fct_update_commentaire_acte($data) {
		include('foncier_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		include ('../includes/configuration.php'); // Intégration du fichier de configuration notamment pour les accès à la base de données
		
		//Paramétrage de connexion à la base de données
		try {
			$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
		}
		catch (Exception $e) {
			die(include('../includes/prob_tech.php'));
		}
		
		// Récupération des différentes variables passées par AJAX et contenues dans une variable tableau $data
		// Ces variables correspondent aux filtres présents dans les différentes pages fiches
		// En fonction des fiches on ne retrouvera pas forcement les mêmes filtres
		$bad_caract = array("-","–","’", "'", "", "\t", "	");
		$good_caract = array("–","-","''", "''", "", "", "");
		$acte_id = $data['params']['var_acte_id'];
		$type_acte = $data['params']['var_type_acte'];
		$lib_acte = str_replace($bad_caract, $good_caract, $data['params']['var_acte_lib']);
		$commentaire = str_replace($bad_caract, $good_caract, $data['params']['var_commentaire']);
		
		// Création de la requête 
		If ($type_acte == 'Acquisition') {
			$rq_update = 
				"UPDATE foncier.mf_acquisitions 
				SET mf_lib = '$lib_acte', mf_commentaires = '$commentaire'
				WHERE mf_id = '$acte_id'";
		} Else If ($type_acte == 'Convention') {
			$rq_update = 
				"UPDATE foncier.mu_conventions
				SET mu_lib = '$lib_acte', mu_commentaires = '$commentaire'
				WHERE mu_id = '$acte_id'";
		}	
		// echo $rq_update;
		$res_update = $bdd->prepare($rq_update);
		$res_update->execute();
		$res_update->closeCursor();
		$bdd = null;
	}
?>