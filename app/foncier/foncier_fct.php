<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/
	
	// Cette fonction permet de transformer une surface de m² en are
	function conv_are($surf) {
		If (is_numeric($surf)) {// On contrôle qu'il s'agit bien d'une valeur numérique
			$are = str_pad($surf, 5, "0", STR_PAD_LEFT); //On complete la surface jusqu'à 5 chiffres minimum pour avoir un format homogène en sortie
			$are = strrev($are); //Inversion de la chaine pour facilité le système de conversion
			$are_fin = substr($are, 4, strlen($are)); //On récupère les hectares
			$are = $are{0}.$are{1}.'.'. $are{2}.$are{3}.'.' . $are_fin; //On écrit la surface en commencant par les ares, puis les centiares, puis on ajoute les hectares récupérés précédement
			$are = strrev($are); // On inverse de nouveau pour obtenir un résultat dans le bon ordre!
			return $are;
		} Else {
			return $surf;
		}
	}	
	
	// Cette fonction permet de transformer une surface de are foncier en hectare
	function conv_are_inverse($are) {
		If (substr_count($are, '.') == 2) {// On contrôle qu'il s'agit bien d'une valeur inscrite en are foncier (ha.ca.aa)
			$surf = str_replace('.', '', $are); // On transforme en hectares
			return $surf;
		} Else {
			return $are;
		}
	}	
		
	//Fonction pour obtenir simplement le libellé d'un propriétaire en fonction des infos présentes dans la base
	function proprio_lib($particule_lib, $nom_usage, $prn_usage, $ddenom) {
		$proprio_lib = '';
		If ($particule_lib == 'N.P.' && $nom_usage == 'N.P.' && $prn_usage == 'N.P.') {
			$proprio_lib = $ddenom;
		} Else {
			If ($particule_lib <> 'N.P.') {
				$proprio_lib = $particule_lib ;
			}
			If ($prn_usage == 'N.P.' && $nom_usage == 'N.P.') {
				$proprio_lib = '   ' . $ddenom;
			} Else {
				If ($nom_usage <> 'N.P.' ) {
					$proprio_lib .= '   ' . $nom_usage;
				}	
				If ($prn_usage <> 'N.P.' ) {
					$prenom_usage = '   ' . ucwords(strtolower($prn_usage));
				} Else If (substr_count($ddenom, '/') > 0) {
					$prenom_usage = '   ' . ucwords(strtolower(substr($ddenom, strpos($ddenom, '/')+1 , strlen($ddenom))));
				}
				Foreach (array('-', '\'') as $delimiter) {
					If (strpos($prenom_usage, $delimiter)!==false) {
						$prenom_usage = implode($delimiter, array_map('ucfirst', explode($delimiter, $prenom_usage)));
					}
				}
				$proprio_lib .= '   ' . $prenom_usage;
			}
		}
		return $proprio_lib;	
	}
	
	//Fonction permettant d'ajouter des liens suivant/ précédent pour alléger certaines liste comme la recherche par parcelles par exemple.
	function fct_suiv_prec($offset, $nbr_result, $compare, $colspan, $fct) {
		$offset_moins = $offset - $nbr_result;
		$offset_plus = $offset + $nbr_result;
		$colspan2 = $colspan-1;
		$fct_moins = str_replace('offset', $offset_moins, $fct);
		$fct_plus = str_replace('offset', $offset_plus, $fct);
		If ($offset == 0 && $compare == $nbr_result) {
			$suiv_prec = "
			<tr class='no-export'>
				<td style='text-align:right' colspan='$colspan'>
					<label class='prec_suiv' onclick=\"$fct_plus; $('html,body').animate({scrollTop: 0}, 'slow');\">
						>>
					</label>
				</td>
			</tr>";
		} ElseIf ($offset > 0 && $compare == $nbr_result) {
			$suiv_prec = "
			<tr class='no-export'>
				<td colspan='$colspan2' style='text-align:left'>
					<label class='prec_suiv' onclick=\"$fct_moins; $('html,body').animate({scrollTop: 0}, 'slow');\">
						<<
					</label>
				</td>
				<td style='text-align:right'>
					<label class='prec_suiv' onclick=\"$fct_plus; $('html,body').animate({scrollTop: 0}, 'slow');\">
						>>
					</label>
				</td>
			</tr>";
		} ElseIf ($offset == 0 && $compare < $nbr_result) {
			$suiv_prec = "
			<tr class='no-export'>
				<td colspan='$colspan'>
					&nbsp;
				</td>
			</tr>";
		} ElseIf ($offset > 0 && $compare < $nbr_result) {
			$suiv_prec = "
			<tr class='no-export'>
				<td colspan='$colspan' style='text-align:left'>
					<label class='prec_suiv' onclick=\"$fct_moins; $('html,body').animate({scrollTop: 0}, 'slow');\">
						<<
					</label>
				</td>
			</tr>";
		}
		return $suiv_prec;
	}
?>