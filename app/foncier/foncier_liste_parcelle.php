<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Foncier';
			$h3 ='Recherche par parcelle';
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'));
			include("../includes/header.php");
			include('../includes/breadcrumb.php');
			//include('../includes/construction.php');
			
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}		
		
			$rq_lst_communes = "
				SELECT nom, code_insee, depart as departement, LEFT(code_insee, 2) as code_dpt
				FROM administratif.communes
				ORDER BY nom";
			$res_lst_communes = $bdd->prepare($rq_lst_communes);
			$res_lst_communes->execute();
			$lst_communes = $res_lst_communes->fetchAll();	
			
			$alphabet = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
		?>
		<section id='main'>
			<h3>Recherche par parcelle</h3>
			<div class='alphabet' id="alphabet">		
				<div><span id='lettre_all' onclick="filter_cmb_commune('lst_communes', 'ALL'); change_style_lettre('all');">ALL</span></div>
				<?php
					For ($i=0; $i<count($alphabet); $i++) {
						echo "<div><span id='lettre_$alphabet[$i]' onclick=\"filter_cmb_commune('lst_communes', '$alphabet[$i]'); change_style_lettre('$alphabet[$i]')\">$alphabet[$i]</span></div>";
					}
				?>
			</div>
			<center>
			<?php If (count(array_unique(array_column($lst_communes, 'departement'))) > 0) { ?>
			<label class="btn_combobox">
				<select name="lst_departements" onchange="filter_cmb_commune('lst_communes', 'DEPT');" id="lst_departements" class="combobox">
					<option value='-1'>S&eacute;lectionnez un département</option>
					<?php
					$tab_departement_unique = array_values(array_unique(array_column($lst_communes, 'departement')));
					$tab_code_dpt_unique = array_values(array_unique(array_column($lst_communes, 'code_dpt')));
					For ($j=0; $j<count($tab_departement_unique); $j++) {
						echo "<option value='" . $tab_code_dpt_unique[$j] . "'>" . str_replace('$* ', '-', ucwords(str_replace('-', '$* ',strtolower($tab_departement_unique[$j])))) . "</option>";
					}
					?>				
				</select>
			</label>
			<?php } ?>
			<label class="btn_combobox">
				<select name="lst_communes" onchange="create_search_parc(0);" id="lst_communes" class="combobox">
					<option value='-1'>S&eacute;lectionnez une commune</option>
					<?php
					For ($i=0; $i<$res_lst_communes->rowCount(); $i++) {
						echo "<option value='" . $lst_communes[$i]['code_insee'] . "'>" . $lst_communes[$i]['nom'] . " (" . $lst_communes[$i]['code_insee'] . ")</option>";
					}
					?>
				</select>
			</label>
			</center>
			<table id='tbl_parc' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0' style='width: auto;'>
				<tbody id='tbl_parc_entete'>
					<form id="frm_parc_entete" onsubmit="return false">
						<tr>
							<td class='filtre' align='right' width='1px'>
								<input class='editbox' oninput="create_search_parc(0);" type='text' id='filtre_site_id' size='10' placeholder='SITE ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SITE ID';"> 
							</td>
							<td class='filtre' align='right' width='1px'>
								<input class='editbox' oninput="create_search_parc(0);" type='text' id='filtre_section' size='10' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';"> 
							</td>
							<td class='filtre' align='right' width='1px'>
								<input class='editbox' oninput="create_search_parc(0);" type='text' id='filtre_par_num' size='10' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';"> 
							</td>
							<td>
								<img style='cursor:pointer;margin:0 15px;' onclick="
								document.getElementById('filtre_site_id').value='';
								document.getElementById('filtre_section').value='';
								document.getElementById('filtre_par_num').value='';
								document.getElementById('lst_communes').value='-1';
								document.getElementById('lst_departements').value='-1';
								create_search_parc(0);
								" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
							</td>
						</tr>
					</form>
				</tbody>
				<tbody id='tbl_parc_corps'>
				</tbody>
			</table>
		</section>
		<?php 
			$res_lst_communes->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
