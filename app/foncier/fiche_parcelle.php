<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">		
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/explorateur.css">		
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script>
			$(document).ready( function () {
				if (document.getElementById('frm_modif_info_parc') != null) {
					var myDiv  = document.getElementById('frm_modif_info_parc');
					var inputArr = myDiv.getElementsByTagName( "textarea" );
					for (var i = 0; i < inputArr.length; i++) { //pour chaque élément de type input remplacer le suffixe temp par need pour le rendre obligatoire
						resizeTextarea(inputArr[i].id);
					}
				}
			} ) ;			
		</script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'));
			$h2 ='Foncier';
			//include('../includes/construction.php');
			include('foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
			include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}	
			
			$bad_caract = array("-","–","’", "'", "", "\t", "	");
			$good_caract = array("–","-","''", "''", "", "", "");
					
			If (isset($_GET['d']) && substr_count(base64_decode($_GET['d']), ':') > 0 && substr_count(base64_decode($_GET['d']), ';') > 0) {
				$d= base64_decode($_GET['d']);
				$min_crit = 0;
				$max_crit = strpos($d, ':');
				$min_val = strpos($d, ':') + 1;
				$max_val = strpos($d, ';') - $min_val;
				
				For ($p=0; $p<substr_count($d, ':'); $p++) {
					$crit = substr($d, $min_crit, $max_crit);
					$value = substr($d, $min_val, $max_val);
					$$crit = $value;
					$min_crit = strpos($d, ';', $min_crit) + 1;
					$max_crit= strpos($d, ':', $min_crit) - $min_crit;
					$min_val = $min_crit + $max_crit + 1;
					$max_val = strpos($d, ';', $min_val) - $min_val;
				}
				If (!isset($par_id) OR !isset($lot_id)) {
					$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
					include('../includes/prob_tech.php');
				}
			} Else {
				$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
				include('../includes/prob_tech.php');
			}
			include("../includes/header.php");
			include('../includes/breadcrumb.php');
			
			$rq_info_parc = 
			"SELECT 
				array_to_string(array_agg(t4.cad_site_id ORDER BY t2.dnulot), ',') AS tbl_cad_site_id, 
				array_to_string(array_agg(CASE WHEN t4.date_fin = 'N.D.' THEN 'actuel' ELSE 'fini' END ORDER BY t2.dnulot), ',') AS tbl_etat_site,
				array_to_string(array_agg(t6.site_id ORDER BY t2.dnulot), ',') AS tbl_site_id,
				array_to_string(array_agg(t6.site_nom ORDER BY t2.dnulot), ',') AS tbl_site_nom,
				array_to_string(array_agg(t6.site_nom || ' (' || t6.site_id || ')' ORDER BY t2.dnulot), ',') AS tbl_site_lib,	
				communes.nom as com_nom, communes.code_insee, vl_cen.libelle as lieu_dit, t1.ccosec as section, t1.dnupla as par_num, t1.dparpi, t1.dcntpa as dcntpa, 
				CASE WHEN d_typprop.typprop != '' THEN d_typprop.typprop ELSE 'N.D.' END as typprop, 				
				array_to_string(array_agg( CASE WHEN t4.date_fin = 'N.D.' THEN t2.lot_id ELSE Null END
					ORDER BY t2.dnulot), ',') AS tbl_lot_id, 
				array_to_string(array_agg( CASE WHEN t4.date_fin = 'N.D.' THEN t2.dnulot ELSE Null END
					ORDER BY t2.dnulot), ',') AS tbl_dnulot, 
				array_to_string(array_agg( CASE WHEN t4.date_fin = 'N.D.' THEN t2.dcntlo ELSE Null END
					ORDER BY t2.dnulot), ',') AS tbl_dcntlo, 
				array_to_string(array_agg( CASE WHEN t4.date_fin = 'N.D.' THEN CASE WHEN tmfu.mfu IS NULL THEN 'Pas de maîtrise' ELSE tmfu.mfu END ELSE Null END
					ORDER BY t2.dnulot), ',') AS tbl_mfu_lib, 
				array_to_string(array_agg( CASE WHEN t4.date_fin = 'N.D.' THEN t5.dnupro ELSE Null END
					ORDER BY t2.dnulot), ',') AS tbl_dnupro, 
				array_to_string(array_agg( CASE WHEN t4.date_fin = 'N.D.' THEN tmfu.surf_conv_calc ELSE Null END
					ORDER BY t2.dnulot), ',') AS tbl_surf_conv_calc,
				array_to_string(array_agg( CASE WHEN t4.date_fin = 'N.D.' THEN t3.date_fin ELSE Null END
					ORDER BY t2.dnulot), ',') AS tbl_date_fin,				
				CASE WHEN t1.annee_pci IS NULL THEN 'N.D.' ELSE t1.annee_pci END AS date_matrice
			FROM foncier.cadastre_site t4 LEFT JOIN sites.sites t6 USING (site_id) LEFT JOIN (
				SELECT cadastre_site.cad_site_id, 'Acquisition'::character varying(25) as mfu, 0 as surf_conv_calc
				FROM foncier.cadastre_site, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf, (
					SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
					FROM foncier.r_mfstatut
					GROUP BY r_mfstatut.mf_id
					) as tmax
				WHERE cadastre_site.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id
				AND (r_mfstatut.date = tmax.statut_date AND r_mfstatut.mf_id = tmax.mf_id)
				AND r_cad_site_mf.date_sortie = 'N.D.' AND d_statutmf.statutmf_id = 1
				UNION
				SELECT cadastre_site.cad_site_id, CASE WHEN d_typmu.mfu = true THEN CASE WHEN r_cad_site_mu.surf_mu is Null THEN 'Convention'::character varying(25) ELSE 'Convention pour partie'::character varying(25) END ELSE 'Autre'::character varying(25) END as mfu, CASE WHEN r_cad_site_mu.surf_mu is Null THEN 0 ELSE r_cad_site_mu.surf_mu END as surf_conv_calc
				FROM foncier.cadastre_site, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (
					SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
					FROM foncier.r_mustatut
					GROUP BY r_mustatut.mu_id
					) as tmax
				WHERE cadastre_site.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id
				AND (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id)
				AND r_cad_site_mu.date_sortie = 'N.D.' AND d_statutmu.statutmu_id = 1 AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text 
				) AS tmfu ON t4.cad_site_id = tmfu.cad_site_id, cadastre.cptprop_cen t5, cadastre.cadastre_cen t3, cadastre.lots_cen t2, cadastre.parcelles_cen t1 LEFT JOIN cadastre.d_typprop ON t1.typprop_id = d_typprop.typprop_id JOIN cadastre.vl_cen ON t1.vl_id = vl_cen.vl_id, administratif.communes
			WHERE t4.cad_cen_id = t3.cad_cen_id AND t3.dnupro = t5.dnupro AND t3.lot_id = t2.lot_id and t2.par_id = t1.par_id AND t1.codcom = communes.code_insee";
			If (!isset($parc_suppr) OR (isset($parc_suppr) && $parc_suppr != 'oui')) {
				$rq_info_parc .= " AND t3.date_fin = 'N.D.'";
			} Else {
				$rq_info_parc .= " AND t3.motif_fin_id = 1";
			}
			$rq_info_parc .= " AND t1.par_id = ?
			GROUP BY t1.par_id, communes.nom, communes.code_insee, vl_cen.libelle, t1.ccosec, t1.dnupla, t1.dcntpa, d_typprop.typprop
			ORDER BY tbl_lot_id, tbl_dnulot;";
			// echo $rq_info_parc;
			If (isset($rq_info_parc)) {
				$res_info_parc = $bdd->prepare($rq_info_parc);
				$res_info_parc->execute(array($par_id));					
				$info_parc = $res_info_parc->fetch();
			} Else {
				include('../includes/prob_tech.php');
			}
			
			If ($res_info_parc->rowCount() == 0){
				include('../includes/prob_tech.php');
			} Else {
				$tbl_etat_site = preg_split("/,/", $info_parc['tbl_etat_site']);
				$tbl_site_id = preg_split("/,/", $info_parc['tbl_site_id']);
				$tbl_site_nom = preg_split("/,/", $info_parc['tbl_site_nom']);
				$tbl_site_lib = preg_split("/,/", $info_parc['tbl_site_lib']);
				$my_value = 'actuel';
				$index_site_actuel = array_keys(array_filter($tbl_etat_site, function ($element) use ($my_value) {return ($element == $my_value);})); 
				If (count($index_site_actuel) > 0) {
					$cur_site_name = $tbl_site_nom[$index_site_actuel[0]];
					$cur_site_id = $tbl_site_id[$index_site_actuel[0]];
				}
				$tbl_cad_site_id = preg_split("/,/", $info_parc['tbl_cad_site_id']);
				$tbl_dnulot = preg_split("/,/", $info_parc['tbl_dnulot']);
				$tbl_lot_id = preg_split("/,/", $info_parc['tbl_lot_id']);
				$tbl_dcntlo = preg_split("/,/", $info_parc['tbl_dcntlo']);
				$tbl_mfu_lib = preg_split("/,/", $info_parc['tbl_mfu_lib']);
				$tbl_dnupro = preg_split("/,/", $info_parc['tbl_dnupro']);
				$tbl_surf_conv_calc  = preg_split("/,/", $info_parc['tbl_surf_conv_calc']);
				$tbl_date_fin  = preg_split("/,/", $info_parc['tbl_date_fin']);
			}
						
			$rq_last_maj = "
			(SELECT 'maitrise' as tbl, 'la ma&icirc;trise fonci&egrave;re et d''usage' as lib, t_mfu.maj_user, max(t_mfu.maj_date) as maj_date
			FROM
				(
					(SELECT 'les acquisitions' as table, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mf_acquisitions.maj_user END as maj_user, max(mf_acquisitions.maj_date) as maj_date
					FROM foncier.mf_acquisitions LEFT JOIN admin_sig.utilisateurs ON (mf_acquisitions.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), foncier.r_cad_site_mf, foncier.cadastre_site, cadastre.cadastre_cen, cadastre.lots_cen
					WHERE mf_acquisitions.mf_id = r_cad_site_mf.mf_id AND r_cad_site_mf.cad_site_id = cadastre_site.cad_site_id AND cadastre_site.cad_cen_id = cadastre_cen.cad_cen_id AND cadastre_cen.lot_id = lots_cen.lot_id AND lots_cen.par_id = :par_id
					GROUP BY d_individu.individu_id, mf_acquisitions.maj_user
					ORDER BY maj_date DESC LIMIT 1)
					UNION
					(SELECT 'les conventions' as table, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mu_conventions.maj_user END as maj_user, max(mu_conventions.maj_date) as maj_date
					FROM foncier.mu_conventions LEFT JOIN admin_sig.utilisateurs ON (mu_conventions.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), foncier.r_cad_site_mu, foncier.cadastre_site, cadastre.cadastre_cen, cadastre.lots_cen
					WHERE mu_conventions.mu_id = r_cad_site_mu.mu_id AND r_cad_site_mu.cad_site_id = cadastre_site.cad_site_id AND cadastre_site.cad_cen_id = cadastre_cen.cad_cen_id AND cadastre_cen.lot_id = lots_cen.lot_id AND lots_cen.par_id = :par_id
					GROUP BY d_individu.individu_id, mu_conventions.maj_user
					ORDER BY maj_date DESC LIMIT 1)
				) as t_mfu
			GROUP BY t_mfu.maj_user
			ORDER BY maj_date DESC LIMIT 1)
			UNION
			(SELECT 'cadastre' as tbl, 'le cadastre' as lib, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE cadastre_site.maj_user END as maj_user, max(cadastre_site.maj_date) as maj_date
			FROM foncier.cadastre_site LEFT JOIN admin_sig.utilisateurs ON (cadastre_site.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), cadastre.cadastre_cen, cadastre.lots_cen
			WHERE cadastre_site.cad_cen_id = cadastre_cen.cad_cen_id AND cadastre_cen.lot_id = lots_cen.lot_id AND lots_cen.par_id = :par_id
			GROUP BY d_individu.individu_id, cadastre_site.maj_user
			ORDER BY maj_date DESC LIMIT 1)
			UNION
			(SELECT 'proprios' as tbl, 'les donn&eacute;e du propri&eacute;taires' as tbl, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE t7.maj_user END as maj_user, max(t7.maj_date) as maj_date
			FROM cadastre.lots_cen t2, cadastre.cadastre_cen t3, foncier.cadastre_site t4, cadastre.cptprop_cen t5, cadastre.r_prop_cptprop_cen t6, cadastre.proprios_cen t7 LEFT JOIN admin_sig.utilisateurs ON (t7.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id)
			WHERE t3.cad_cen_id = t4.cad_cen_id AND t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t3.lot_id = t2.lot_id AND t2.par_id = :par_id
			GROUP BY d_individu.individu_id, t7.maj_user
			ORDER BY maj_date DESC LIMIT 1);";
			$res_last_maj = $bdd->prepare($rq_last_maj);
			$res_last_maj->execute(array('par_id'=>$par_id));
			$last_maj = $res_last_maj->fetchAll();
			$last_maj_tbl = array_column($last_maj, 'tbl');
			$last_maj_date = array_column($last_maj, 'maj_date');
			
			$rq_lots_histo = "
			SELECT 
				t4.cad_site_id AS cad_site_id,
				t2.lot_id AS lot_id, 
				t2.dnulot AS dnulot, 
				t2.dcntlo AS dcntlo, 
				--t4.utilissol as utilissol,
				t5.dnupro AS dnupro,
				t3.date_fin  AS date_fin 
			FROM 
				foncier.cadastre_site t4, cadastre.cptprop_cen t5, cadastre.cadastre_cen t3, cadastre.lots_cen t2
			WHERE 
				t4.cad_cen_id = t3.cad_cen_id AND t3.dnupro = t5.dnupro AND t3.lot_id = t2.lot_id 
				AND t3.date_fin != 'N.D.' AND t3.motif_fin_id = 1 AND t2.par_id = ? 
			ORDER BY dnulot;";
			$res_lots_histo = $bdd->prepare($rq_lots_histo);
			$res_lots_histo->execute(array($par_id));
		?>	
		
		<section id='main'>
			<h3>Fiche parcelle : <?php echo $par_id; ?><img style='position:absolute; right:50px; top:150px ; opacity:0.8' align='right' src="images/vign_bdfoncier.png" height='100px'></h3>			
			<img width="30px" id="btn_top" class="backtotop" alt="Retour haut de page" onclick="return false;" src="<?php echo ROOT_PATH; ?>images/back_top.png" style="display: none;">
			<div id='onglet' class='list'>
				<ul>
					<li>
						<a id='li_general' href="#general"><span>Gén&eacute;ral</span></a>
					</li>
					<li>
						<a id='li_lot' href="#lot"><span>Lots/ Propriétaires</span></a>
					</li>
					<li>
						<a id='li_maitrise' href="#maitrise" onclick="create_lst_mf('charge', 0, '<?php echo $par_id; ?>');">
							<span>Maîtrise foncière et d'usage</span>
						</a>
					</li>
				</ul>
				
				<div id='general'>
					<table width='100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<table width = '100%' CELLSPACING = '0' CELLPADDING ='10' style='border:0px solid black'>
									<tr>
										<td>
											<label class="label">
												<u>Site :</u> 
											<?php
												
												If (count($index_site_actuel) > 0) {
													echo $tbl_site_lib[$index_site_actuel[0]];
													$vget = "site_nom:".$cur_site_name.";site_id:".$cur_site_id.";";
													$vget_crypt = base64_encode($vget);
													$lien_site = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";
											?>
												<a href='<?php echo $lien_site; ?>'>
													<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche site">
												</a>
											<?php
												} Else {
													echo 'N.D.';
												}
											?>
											</label>
										</td>
									</tr>
								<?php	
									$my_value = 'fini';
									$index_ancien_site = array_keys(array_filter(array_unique($tbl_etat_site), function ($element) use ($my_value) {return ($element == $my_value);})); 
									If (count($index_ancien_site) > 0) {
								?>
									<tr>
										<td>
											<label class="label" style='color:#747474'>
												<u>Ancien<?php If (count($index_ancien_site) > 1) {echo 's'; } ?> site<?php If (count($index_ancien_site) > 1) {echo 's'; } ?> :</u> <?php echo $tbl_site_lib[$index_ancien_site[0]]; ?>
												<?php
													$vget = "site_nom:".$tbl_site_nom[$index_ancien_site[0]].";site_id:".$tbl_site_id[$index_ancien_site[0]].";";
													$vget_crypt = base64_encode($vget);
													$lien_site = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";
												?>
												<a href='<?php echo $lien_site; ?>'>
													<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche site">
												</a>
											<?php 
												For ($i=1; $i<count($index_ancien_site); $i++) {
													$vget = "site_nom:".$tbl_site_nom[$index_ancien_site[$i]].";site_id:".$tbl_site_id[$index_ancien_site[$i]].";";
													$vget_crypt = base64_encode($vget);
													$lien_site = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";
											?>
												<span style='display:block;margin-left:102px;'>
													<?php echo $tbl_site_lib[$index_ancien_site[$i]]; ?>
													<a href='<?php echo $lien_site; ?>'>
														<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche site">
													</a>
												</span>
											<?php
												}
											?>
											</label>
								<?php } ?>
									<tr>
										<td>
											<label class="label"><u>Commune :</u> <?php echo $info_parc['com_nom']?> (<?php echo $info_parc['code_insee']?>)</label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="label"><u>Lieu-dit :</u> <?php echo ucwords(strtolower($info_parc['lieu_dit']))?></label>
										</td>
									</tr>
									<tr>
										<td>
											<?php
												$rq_zh_parc = "
												SELECT ".ID_TABLE_ZH."
												FROM ".
													NAME_SCHEMA_ZH.".".NAME_TABLE_ZH." t1
													JOIN cadastre.parcelles_cen t2 ON (st_intersects(t1.geom, t2.geom))
												WHERE
													t2.par_id = ? AND
													t1.remblais = 'f'";
												// echo $rq_zh_parc;
												If (isset($rq_zh_parc)) {
													$res_zh_parc = $bdd->prepare($rq_zh_parc);
													$res_zh_parc->execute(array($par_id));					
													$zh_parc = $res_zh_parc->fetchall();
													$res_zh_parc->closeCursor();
												} Else {
													include('../includes/prob_tech.php');
												}
											?>
											<table style='border:0px solid black'>
												<tr>
													<td style="vertical-align:top">
														<label class="label"><u>Zones humides : </u> 
													</td>
													<td>
														<?php 
														If (count($zh_parc) == 0) {
															echo '<label class="label">Aucune</label>';
														} Else {
															For ($i=0; $i<count($zh_parc); $i++) { ?>
														<li style='list-style-type:none;'>
															<label class="label"><?php echo $zh_parc[$i][0]; ?></label>
														</li>
														<?php }
														} ?>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
							<td>
								<table width = '100%' CELLSPACING = '0' CELLPADDING ='10' style='border:0px solid black'>
									<tr>
										<td>
											<label class="label"><u>Section :</u> <?php echo $info_parc['section']?></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="label"><u>N° parcelle :</u> <?php echo $info_parc['par_num']?></label>
										</td>
									</tr>
									<tr>
										<td>
											<label class="label"><u>Contenance :</u> <?php echo conv_are($info_parc['dcntpa'])?> ha.a.ca</label>
										</td>
									</tr>
									<tr>
										<td>
											<?php
												$rq_nat_cult = 
												"SELECT t1.par_id, d_dsgrpf.natcult_ssgrp, d_dsgrpf.cgrnum, sum(lots_natcult_cen.dcntsf) as surf_nat_cult, CASE WHEN d_cnatsp.natcult_natspe IS NOT NULL THEN d_cnatsp.natcult_natspe ELSE 'N.D.' END as natcult_natspe, d_dsgrpf.dsgrpf
												FROM cadastre.parcelles_cen t1, cadastre.lots_cen as t2, cadastre.lots_natcult_cen LEFT JOIN cadastre.d_cnatsp ON (lots_natcult_cen.cnatsp = d_cnatsp.cnatsp),  cadastre.d_dsgrpf 
												WHERE t2.lot_id = lots_natcult_cen.lot_id AND lots_natcult_cen.dsgrpf = d_dsgrpf.dsgrpf AND t1.par_id = t2.par_id AND t1.par_id = ?
												GROUP BY t1.par_id, d_dsgrpf.natcult_ssgrp, d_dsgrpf.cgrnum, d_cnatsp.natcult_natspe, d_dsgrpf.dsgrpf
												ORDER BY surf_nat_cult DESC";
												If (isset($rq_nat_cult)) {
													$res_nat_cult = $bdd->prepare($rq_nat_cult);
													$res_nat_cult->execute(array($par_id));					
													$nat_cult = $res_nat_cult->fetchall();
													$res_nat_cult->closeCursor();
												} Else {
													include('../includes/prob_tech.php');
												}
											?>
											<table style='border:0px solid black'>
												<tr>
													<td style="vertical-align:top">
														<label class="label"><u>Natures de culture : </u> 
													</td>
													<td>
														<?php 
														If (count($nat_cult) == 0) {
															echo '<label class="label">N.D.</label>';
														} Else {
															For ($i=0; $i<count($nat_cult); $i++) { ?>
														<li style='list-style-type:none;'>
															<label class="label"><?php echo $nat_cult[$i]['natcult_ssgrp'] . ' (' . $nat_cult[$i]['natcult_natspe'] . ') : ' . conv_are($nat_cult[$i]['surf_nat_cult']) . ' ha.a.ca (' . round(($nat_cult[$i]['surf_nat_cult']*100)/$info_parc['dcntpa'], 0) . '%)'; ?></label>
														</li>
														<?php }
														} ?>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style='text-align:left'>
								<label class="last_maj">
									Ann&eacute;e matrice : <?php echo $info_parc['date_matrice']; ?>
								</label>
							</td>
							<td style='text-align:right'>
								<label class="last_maj">
									Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_user'];?> concernant <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['lib'];?>
								</label>
							</td>
						</tr>
					</table>
				</div>
				
				<div id='lot'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black; padding-top:5px;'>						
						<tbody>
							<?php If (count($tbl_lot_id) > 1) { ?>
							<tr>
								<td id='tbl_collapse' <?php If ($res_lots_histo->rowCount() == 0) { ?> colspan='2' <?php } ?>>
									<label id='lbl_slideIn' class='label' onclick="slideIn('ALL_OUT');">Tout replier</label>
								</td>
								<?php If ($res_lots_histo->rowCount() > 0 && (!isset($parc_suppr) OR (isset($parc_suppr) && $parc_suppr != 'oui'))) { ?>
								<td style='text-align:right'>
									<label id='lbl_slideIn_histo_lot' class='label' style='text-align:right;cursor:pointer;text-shadow:1px 1px 2px #ebebeb;color:#cf740d;' onclick="addParc_js('explorateur_histo', 'onglet');">Afficher l'historique des lots</label>
								</td>
								<?php } ?>
							</tr>
							<?php } Else { ?>
							<tr>
								<?php If ($res_lots_histo->rowCount() > 0 && (!isset($parc_suppr) OR (isset($parc_suppr) && $parc_suppr != 'oui'))) { ?>
								<td style='text-align:right' colspan='2' width='100%'>
									<label id='lbl_slideIn_histo_lot' class='label' style='text-align:right;cursor:pointer;text-shadow:1px 1px 2px #ebebeb;color:#cf740d;' onclick="addParc_js('explorateur_histo', 'onglet');">Afficher l'historique des lots</label>
								</td>
								<?php } ?>
							</tr>
							<?php } ?>
						</tbody>
						<tr>
							<td colspan='2' width='100%'>
								<?php
								If (substr_count($info_parc['tbl_dnulot'], ",") == 0) {
									echo "<center><label class='label'>Cette parcelle ne contient qu'un seul lot</label></center>";
								} Else {
									echo "<center><label class='label'>Cette parcelle contient " . count($tbl_dnulot) . " lots</label></center>";
								}
								?>
							</td>
						</tr><?php For ($i=0; $i<count($tbl_dnulot); $i++) {?>
						<tr>
							<td><?php If ($tbl_dnulot[$i] <> '') {?>
								<label class='label' onclick="slideIn('sld_<?php echo $tbl_dnulot[$i]; ?>');"><?php echo $i+1 . '. ' . $tbl_dnulot[$i]; ?></label> <?php } ?>
							</td>
							<td width='100%'>
								<div id=<?php If ($tbl_dnulot[$i] =='') {echo "'sld_".$tbl_lot_id[$i]."'";} Else {echo "'sld_".$tbl_dnulot[$i]; If(isset($tbl_lot_id_sel) AND ($tbl_dnulot[$i] == $tbl_lot_id_sel)) {echo "' style='display:block'";} Else {echo "' style='display:block'";}} ?>>
									<table style='border:0px solid black'>
										<tr>
											<td>
												<table CELLSPACING = '5' CELLPADDING ='5' style='border:0px solid black'>
													<tr>
														<td class='no-border'>
															<label class='label'><u>Lot ID :</u> <?php If (isset($tbl_lot_id[$i]) AND $tbl_lot_id[$i] != '') {echo $tbl_lot_id[$i];} Else {echo "N.D.";} ?> </label>
														</td>
													</tr>
													<tr>
														<td class='no-border'>
															<label class='label'><u>Contenance :</u> <?php If (isset($tbl_dcntlo[$i]) AND $tbl_dcntlo[$i] != '') {echo conv_are($tbl_dcntlo[$i]) . ' ha.a.ca';} Else {echo "N.D.";} ?> </label>
														</td>
													</tr>
													<tr>
														<td class='no-border'>
															<?php
																$rq_nat_cult = 
																"SELECT d_dsgrpf.natcult_ssgrp, d_dsgrpf.cgrnum, sum(lots_natcult_cen.dcntsf) as surf_nat_cult, CASE WHEN d_cnatsp.natcult_natspe IS NOT NULL THEN d_cnatsp.natcult_natspe ELSE 'N.D.' END as natcult_natspe, d_dsgrpf.dsgrpf
																FROM cadastre.lots_cen as t2, cadastre.lots_natcult_cen LEFT JOIN cadastre.d_cnatsp ON (lots_natcult_cen.cnatsp = d_cnatsp.cnatsp),  cadastre.d_dsgrpf 
																WHERE t2.lot_id = lots_natcult_cen.lot_id AND lots_natcult_cen.dsgrpf = d_dsgrpf.dsgrpf AND  t2.lot_id = ?
																GROUP BY d_dsgrpf.natcult_ssgrp, d_dsgrpf.cgrnum, d_cnatsp.natcult_natspe, d_dsgrpf.dsgrpf
																ORDER BY surf_nat_cult DESC";
																If (isset($rq_nat_cult)) {
																	$res_nat_cult = $bdd->prepare($rq_nat_cult);
																	$res_nat_cult->execute(array($tbl_lot_id[$i]));					
																	$nat_cult = $res_nat_cult->fetchall();
																	$res_nat_cult->closeCursor();
																} Else {
																	include('../includes/prob_tech.php');
																}
															?>
															<table style='border:0 solid black'>
																<tr>
																	<td style="vertical-align:top">
																		<label class="label" style='margin-left:-5px'><u>Natures de culture : </u> 
																	</td>
																</tr>
																<tr>
																	<td>
																		<table style='border:0 solid black;margin-left:20px;'>
																			
																		<?php 
																		If (count($nat_cult) == 0) {
																			echo '<td><label class="label">N.D.</label></td>';
																		} Else {
																			For ($j=0; $j<count($nat_cult); $j++) { ?>
																			<tr>
																				<td style='text-align:left'>
																					<label class="label">
																						<?php echo $nat_cult[$j]['natcult_ssgrp'] . ' : ' . conv_are($nat_cult[$j]['surf_nat_cult']) . ' ha.a.ca (' . round(($nat_cult[$j]['surf_nat_cult']*100)/$tbl_dcntlo[$i], 0) . '%)'; ?>
																					</label>
																				</td>
																			</tr>
																				<?php If ($nat_cult[$j]['natcult_natspe'] != 'N.D.') { ?>
																			<tr>
																				<td style='text-align:center'>
																					<label class="label" style='font-size:9pt;font-weight:normal;'>
																						<?php echo '(' . $nat_cult[$j]['natcult_natspe'] . ')'; ?>
																					</label>
																				</td>
																			</tr>
																				<?php } ?>
																			<?php }
																		} ?>
																			
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												<?php
													If (!isset($parc_suppr) OR (isset($parc_suppr) && $parc_suppr != 'oui')) {
												?>
													<tr>
														<td class='no-border'>
															<label class='label'><u>Maîtrise foncière :</u>
															<?php 
																If (isset($tbl_mfu_lib[$i]) AND $tbl_mfu_lib[$i] != '') {
																	echo $tbl_mfu_lib[$i];
																} Else {
																	echo "N.D."; 
																} 
																If (isset($tbl_surf_conv_calc[$i]) AND $tbl_surf_conv_calc[$i] > 0) {
																	echo " sur " . conv_are($tbl_surf_conv_calc[$i]) . " ha.a.ca soit " . round(($tbl_surf_conv_calc[$i]*100)/$tbl_dcntlo[$i]) . "%";
																}
															?> </label>
														</td>
													</tr>
												<?php
													} Else {
												?>	
													<tr>
														<td>
															<label class='label' style='color:#cf740d;'>
															<u>Historisé le :</u>
															<?php echo date_transform($tbl_date_fin[$i]); ?>
														</label>
														</td>
													</tr>
												<?php
													}
												?>	
												</table>
											</td>
											<td style='border-left:solid #004494 1px; width:100%'>
												<table style='border:0px solid black; width:100%'>
													<tr>
														<td>
															<table CELLSPACING = '5' CELLPADDING ='5' style='border:0px solid black'>
														<?php
															$rq_proprio_lot = 
															"SELECT DISTINCT 
															t6.dnupro, 
															t7.dnuper, 
															(CASE WHEN d_ccoqua.particule_lib != '' THEN d_ccoqua.particule_lib ELSE 'N.P.' END) AS particule_lib, 
															t7.ddenom, 
															t7.nom_usage, 
															CASE WHEN t7.prenom_usage IS NOT NULL AND t7.prenom_usage != 'N.P.' THEN 
																CASE WHEN strpos(t7.prenom_usage, ' ') > 0 THEN
																	substring(t7.prenom_usage from 0 for  strpos(t7.prenom_usage, ' ')) 
																ELSE 
																	t7.prenom_usage
																END
															ELSE	
																'N.P.' 
															END as prenom_usage,
															t7.nom_jeunefille,
															d_ccodro.droit_lib,
															d_ccodem.dqualp
															FROM 
															cadastre.cadastre_cen t3, 
															cadastre.cptprop_cen t5, 
															cadastre.r_prop_cptprop_cen t6 LEFT JOIN cadastre.d_ccodro ON (d_ccodro.ccodro = t6.ccodro) LEFT JOIN cadastre.d_ccodem ON (d_ccodem.ccodem = t6.ccodem), 
															cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua)
															WHERE t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t3.lot_id = :lot_id";
															If (!isset($parc_suppr) OR (isset($parc_suppr) && $parc_suppr != 'oui')) {
																$rq_proprio_lot .= " AND t3.date_fin = 'N.D.'";
															}
															$rq_proprio_lot .= " ORDER BY t6.dnupro, t7.ddenom";
															$res_proprio_lot = $bdd->prepare($rq_proprio_lot);
															$res_proprio_lot->execute(array('lot_id'=>$tbl_lot_id[$i]));
														?>
																<tr>
																	<td colspan=3>
																		<label class='label'><u>Compte de propriété :</u>
																		<?php 
																			$dnupro = $res_proprio_lot->fetchColumn();
																			echo $dnupro;
																		?>
																	</td>
																</tr>
																<tr>
																	<td>
																		&nbsp;
																	</td>
																	<td style='text-align:center;'>
																		<label class='label' style='font-weight:normal;'>
																			<i>Droit réel ou<br>particulier</i>
																		</label>
																	</td>
																	<td style='text-align:center;'>
																		<label class='label' style='font-weight:normal;'>
																			<i>Démembrement<br>Indivision</i>
																		</label>
																	</td>
																</tr>
														<?php
															$res_proprio_lot = $bdd->prepare($rq_proprio_lot);
															$res_proprio_lot->execute(array('lot_id'=>$tbl_lot_id[$i]));
															While ($donnees = $res_proprio_lot->fetch()) {
																$proprio_lib = proprio_lib($donnees['particule_lib'], $donnees['nom_usage'], $donnees['prenom_usage'], $donnees['ddenom']);
																
																$vget = "dnuper:".$donnees['dnuper'].";mode:consult;proprio_lib:$proprio_lib;";
																$vget_crypt = base64_encode($vget);
																$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt#general"; ?>
																<tr>
																	<td>
																		<label class='label' style='white-space:normal;'><?php echo $proprio_lib; If (isset($donnees['nom_jeunefille']) AND $donnees['nom_jeunefille'] != '' AND $donnees['nom_jeunefille'] != 'N.P.') { echo " (née $donnees[nom_jeunefille])"; }?></label>
																	</td>
																	<td>
																		<label class='label'>
																		<?php
																			If (isset($donnees['droit_lib']) && $donnees['droit_lib'] != '') {
																				echo retour_ligne(preg_replace('#\((.+)\)#i', '', $donnees['droit_lib']), 20);
																			} Else {
																				echo "N.D.";
																			}
																		?>
																		</label>
																	</td>
																	<td>
																		<label class='label'>
																		<?php
																			If (isset($donnees['dqualp']) && $donnees['dqualp'] != '')
																				{echo $donnees['dqualp'];}
																			Else
																				{echo "N.D.";}
																		?>
																		</label>
																	</td>
																	<td>
																		<a href='<?php echo $lien; ?>'>
																			<img src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche propri&eacute;taire">
																		</a>
																	</td>
																</tr> <?php 
															} ?>
															</table>
														</td>
													</tr>
												<?php
													If (!isset($parc_suppr) OR (isset($parc_suppr) && $parc_suppr != 'oui')) {
												?>
													<tr>
														<td style='text-align:center;'>
															<table style='border:0px solid black; width:100%;'>
														<?php			
															$rq_proprio_lot_histo = 
															"SELECT DISTINCT 
															t6.dnupro, 
															t7.dnuper, 
															(CASE WHEN d_ccoqua.particule_lib != '' THEN d_ccoqua.particule_lib ELSE 'N.P.' END) AS particule_lib, 
															t7.ddenom, 
															t7.nom_usage, 
															t7.prenom_usage,
															t7.nom_jeunefille,
															t3.date_fin
															FROM 
															cadastre.cadastre_cen t3, 
															cadastre.cptprop_cen t5, 
															cadastre.r_prop_cptprop_cen t6, 
															cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua)
															WHERE t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t3.lot_id = ?
															AND t3.date_fin != 'N.D.'
															ORDER BY t6.dnupro, t7.ddenom";
															$res_proprio_lot_histo = $bdd->prepare($rq_proprio_lot_histo);
															$res_proprio_lot_histo->execute(array($tbl_lot_id[$i]));
															
															If ($res_proprio_lot_histo->rowCount() > 0) { ?>
																<tr>
																	<td id='ancien_proprio<?php echo $i; ?>_slideIn' colspan='4' style='text-align:center'>
																		<label style='text-align:right;cursor:pointer;text-shadow:1px 1px 2px #ebebeb;color:#cf740d;'  class='label' onclick="slideIn('ancien_proprio<?php echo $i; ?>');">Afficher les anciens propriétaires</label>
																	</td>
																</tr> 
																<tbody id='ancien_proprio<?php echo $i; ?>' style='display:none;'><?php
															}
															
																While ($donnees = $res_proprio_lot_histo->fetch()) {
																	$proprio_lib = proprio_lib($donnees['particule_lib'], $donnees['nom_usage'], $donnees['prenom_usage'], $donnees['ddenom']);
																	
																	$vget = "dnuper:".$donnees['dnuper'].";mode:consult;proprio_lib:$proprio_lib;";
																	$vget_crypt = base64_encode($vget);
																	$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt#general"; ?>
																	<tr>
																		<td>
																			<label class='label' style='color:#949494'>
																				<?php echo $proprio_lib; If (isset($donnees['nom_jeunefille']) AND $donnees['nom_jeunefille'] != '' AND $donnees['nom_jeunefille'] != 'N.P.') { echo " (née $donnees[nom_jeunefille])"; }?> &nbsp; Historisé le <?php echo date_transform($donnees['date_fin']); ?> &nbsp;<a href='<?php echo $lien; ?>'><img style='opacity:0.7;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche propri&eacute;taire"></a>
																			</label>
																		</td>
																	</tr> <?php
																}
														?>		</tbody>
															</table>
														</td>
													</tr>
												<?php
													}
												?>
												</table>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td style="border-bottom: solid #004494 1px" colspan="2"></td>
						</tr>
					<?php } ?>					
					</table>
				</div id='lot'>
				
				<div id='maitrise'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								
								<table width = '100%' class='no-border' id='tbl_mf' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<?php 
									$lot_dispo = 'NON';
									For ($h=0; $h<count($tbl_mfu_lib); $h++) {
										If ($tbl_mfu_lib[$h] == 'Pas de maîtrise') {
											$lot_dispo = 'OUI';
											break;
										}
									}
									If ($verif_role_acces == 'OUI' && $lot_dispo == 'OUI') { 
										$vget = "site_id:".$cur_site_id.";";
										$vget .= "site_nom:".$cur_site_name.";";
										$vget .= "par_id:".$par_id.";";
										$vget_acq = "type_acte:Acquisition;".$vget;										
										$vget_conv = "type_acte:Convention;".$vget;	
										$vget_crypt_acq = base64_encode($vget_acq);
										$vget_crypt_conv = base64_encode($vget_conv);
										$lien_acq = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_new_mfu.php?d=$vget_crypt_acq";
										$lien_conv = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_new_mfu.php?d=$vget_crypt_conv";
									?>
									
									<tbody>
										<form id="frm_export_mfu" name="frm_export_mfu" onsubmit="return false" action='foncier_export.php?filename=<?php echo $par_id; ?>_MFU.csv' method='POST'>
											<label class="label">
												Exporter :
													<img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px' style="cursor:pointer;" onclick="exportTable('frm_export_mfu', 'tbl_mf_entete_fixed', 'tbl_mf_corps');">
													<input type="hidden" id="tbl_post" name="tbl_post" value=""/>
												<span style='padding-left:75px;'></span>
											</label>
											<label class="label">
												Ajouter :
													<input type='button' class='btn_frm' value='Une acquisition' onclick="document.location.href='<?php echo $lien_acq; ?>'" />
													<span style='padding-left:75px;'></span>
											</label>
											<label class="label">
												Ajouter :
													<input type='button' class='btn_frm' value='Une convention' onclick="document.location.href='<?php echo $lien_conv; ?>'" />
											</label>
										</form>
									</tbody>
									
									<?php } ?>
									<tbody id='tbl_mf_entete'>
										<form id="frm_mf_entete" onsubmit="return false">
											<tr>
												<td class='filtre' align='center' width='95px'>
													<label class="btn_combobox">
														<select name="filtre_acte" onchange="create_lst_mf('modif', 0, '<?php echo $par_id; ?>');" id="filtre_acte" class="combobox" style='text-align:center'>
															<option value='-1'>Acte</option>
														</select>
													</label>
												</td>												
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $par_id; ?>');" type='text' id='filtre_acte_id' size='9' placeholder='ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'ID';">   
												</td>		
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $par_id; ?>');" type='text' id='filtre_libelle' size='36' placeholder='Libell&eacute;' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Libell&eacute;';"> 
												</td>
												<td class='filtre' align='center' width='270px' >
													<label class="btn_combobox">
														<select name="filtre_type_mf" onchange="create_lst_mf('modif', 0, '<?php echo $par_id; ?>');" id="filtre_type_mf" class="combobox" style='text-align:center;max-width:270px'>
															<option value='-1'>Type d'acte</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='95px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $par_id; ?>');" type='text' id='filtre_date' size='10' placeholder='Date' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Date';"> 
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_statut" onchange="create_lst_mf('modif', 0, '<?php echo $par_id; ?>');" id="filtre_statut" class="combobox" style='text-align:center'>
															<option value='-1'>Statut</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='80px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_doc_pdf" onchange="create_lst_mf('modif', 0, '<?php echo $par_id; ?>');" id="filtre_doc_pdf" class="combobox" style='text-align:center'>
															<option value='-1'>PDF</option>
														</select>
													</label>
												</td>
												<td width='25px'>	
													<img onclick="
													document.getElementById('filtre_acte').value='-1';
													document.getElementById('filtre_acte_id').value='';
													document.getElementById('filtre_libelle').value='';
													document.getElementById('filtre_type_mf').value='';
													document.getElementById('filtre_date').value='';
													document.getElementById('filtre_statut').value='-1';
													document.getElementById('filtre_doc_pdf').value='-1';
													create_lst_mf('charge', 0, '<?php echo $par_id; ?>');
													" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
											<input type='hidden' id='filtre_par_id' value='<?php echo $par_id ?>';> 
										</form>
									</tbody>
									<thead id="tbl_mf_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
										<tr>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>Acte</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>ID</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Libell&eacute;</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Type de l'acte</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Date</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Statut</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>PDF</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>&nbsp;</label>
											</th>
										</tr>
									</thead>
									<tbody id='tbl_mf_corps'>
										
									</tbody>
									<?php If (in_array('maitrise', $last_maj_tbl)) { ?>
									<tbody id='tbl_mfu_maj'>
										<tr>
											<td style='text-align:right' colspan='7'>
												<label class="last_maj">
													Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search('maitrise', $last_maj_tbl)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search('maitrise', $last_maj_tbl)]['maj_user']; ?>
												</label>
											</td>
										</tr>
									</tbody>
									<?php } ?>
								</table>								
							</td>
						</tr>
					</table>
				</div id='maitrise'>
			</div>
			<?php If (isset($parc_suppr) && $parc_suppr == 'oui') { ?>
			<div id="avertissement_comm" class="popup_avertissement">
				<label style='font-size:15pt;padding-top: 10px'>
					<img style='top:3px;left:60px' src='<?php echo ROOT_PATH; ?>images/exclamation.png' width='40px'>
					Attention cette parcelle n'existe plus
					<img style='top:3px;right:60px' src='<?php echo ROOT_PATH; ?>images/exclamation.png' width='40px'>
				</label>
			</div>
			<?php } ?>
			<div id="explorateur_histo" class="explorateur_span" style="display: none;">
				<div id="explorateur_new">
					<table class="explorateur_contenu" cellspacing="0" cellpadding="4" border="0" align="center" frame="box" rules="NONE">
						<tbody id='tbody_histo'>
							<tr>
								<td style='text-align:center;' colspan='2'>
									<label class='label' style='font-size:11pt'>Historique des lots</label>
									<img width='15px' onclick="cacheMess('explorateur_histo');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style='cursor:pointer; float:right;'>
								</td>									
							</tr>
							<?php While ($donnees = $res_lots_histo->fetch()) {?>
							<tr>
								<td>
									<label class='label'><?php echo $i+1 . '. ' . $donnees['dnulot']; ?></label>
								</td>
								<td width='100%'>
									<table style='border:0px solid black'>
										<tr>
											<td>
												<table CELLSPACING = '1' CELLPADDING ='1' style='border:0px solid black'>
													<tr>
														<td class='no-border'>
															<label class='label'>
																<u>Lot ID :</u> <?php echo $donnees['lot_id'] ?>
															</label>
														</td>
													</tr>
													<tr>
														<td class='no-border'>
															<label class='label'>
																<u>Contenance :</u> <?php echo conv_are($donnees['dcntlo']); ?>
															</label>
														</td>
													</tr>
													<tr>
														<td class='no-border'>
															<?php
																$rq_nat_cult_lots_histo = 
																"SELECT d_dsgrpf.natcult_ssgrp, d_dsgrpf.cgrnum, sum(lots_natcult_cen.dcntsf) as surf_nat_cult, CASE WHEN d_cnatsp.natcult_natspe IS NOT NULL THEN d_cnatsp.natcult_natspe ELSE 'N.D.' END as natcult_natspe, d_dsgrpf.dsgrpf
																FROM cadastre.lots_cen as t2, cadastre.lots_natcult_cen LEFT JOIN cadastre.d_cnatsp ON (lots_natcult_cen.cnatsp = d_cnatsp.cnatsp),  cadastre.d_dsgrpf 
																WHERE t2.lot_id = lots_natcult_cen.lot_id AND lots_natcult_cen.dsgrpf = d_dsgrpf.dsgrpf AND  t2.lot_id = ?
																GROUP BY d_dsgrpf.natcult_ssgrp, d_dsgrpf.cgrnum, d_cnatsp.natcult_natspe, d_dsgrpf.dsgrpf
																ORDER BY surf_nat_cult DESC";
																If (isset($rq_nat_cult_lots_histo)) {
																	$res_nat_cult_lots_histo = $bdd->prepare($rq_nat_cult_lots_histo);
																	$res_nat_cult_lots_histo->execute(array($donnees['lot_id']));					
																	$nat_cult_lots_histo = $res_nat_cult_lots_histo->fetchall();
																	$res_proprio_lot_histo->closeCursor();
																} Else {
																	include('../includes/prob_tech.php');
																}
															?>
															<table style='border:0px solid black'>
																<tr>
																	<td style="vertical-align:top">
																		<label class="label"><u>Natures de culture : </u> 
																	</td>
																	<td>
																		<?php 
																		If (count($nat_cult_lots_histo) == 0) {
																			echo '<label class="label">N.D.</label>';
																		} Else {
																			For ($j=0; $j<count($nat_cult_lots_histo); $j++) { ?>
																		<li style='list-style-type:none;'>
																			<label class="label"><?php echo $nat_cult_lots_histo[$j]['natcult_ssgrp'] . ' (' . $nat_cult_lots_histo[$j]['natcult_natspe'] . ') : ' . conv_are($nat_cult_lots_histo[$j]['surf_nat_cult']) . ' ha.a.ca (' . round(($nat_cult_lots_histo[$j]['surf_nat_cult']*100)/$donnees['dcntlo'], 0) . '%)'; ?></label>
																		</li>
																		<?php }
																		} ?>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class='no-border'>
															<label class='label' style='color:#cf740d;'>
																<u>Historisé le :</u>
																<?php echo date_transform($donnees['date_fin']); ?>
															</label>
														</td>
													</tr>
												</table>
											</td>
											<td style='border-left: solid #004494 1px'>
												<table CELLSPACING = '5' CELLPADDING ='5' style='border:0px solid black'>
													<tr>
														<td>
															&nbsp;
														</td>
														<td style='text-align:center;'>
															<label class='label' style='font-weight:normal;'>
																<i>Droit réel ou<br>particulier</i>
															</label>
														</td>
														<td style='text-align:center;'>
															<label class='label' style='font-weight:normal;'>
																<i>Démembrement<br>Indivision</i>
															</label>
														</td>
														<td>
															&nbsp;
														</td>
													</tr>
												<?php
													$rq_proprio_lot_lots_histo = 
													"SELECT DISTINCT 
													t6.dnupro, 
													t7.dnuper, 
													(CASE WHEN d_ccoqua.particule_lib != '' THEN d_ccoqua.particule_lib ELSE 'N.P.' END) AS particule_lib, 
													t7.ddenom, 
													t7.nom_usage, 
													t7.prenom_usage,
													d_ccodro.droit_lib,
													d_ccodem.dqualp
													FROM 
													cadastre.cadastre_cen t3, 
													cadastre.cptprop_cen t5, 
													cadastre.r_prop_cptprop_cen t6 LEFT JOIN cadastre.d_ccodro ON (d_ccodro.ccodro = t6.ccodro) LEFT JOIN cadastre.d_ccodem ON (d_ccodem.ccodem = t6.ccodem), 
													cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua)
													WHERE t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t3.lot_id = :lot_id
													AND t3.date_fin != 'N.D.' AND t3.motif_fin_id = 1
													ORDER BY t6.dnupro, t7.ddenom";
													$res_proprio_lot_lots_histo = $bdd->prepare($rq_proprio_lot_lots_histo);
													$res_proprio_lot_lots_histo->execute(array('lot_id'=>$donnees['lot_id']));
													
													While ($donnee2 = $res_proprio_lot_lots_histo->fetch()) {
														$proprio_lib = proprio_lib($donnee2['particule_lib'], $donnee2['nom_usage'], $donnee2['prenom_usage'], $donnee2['ddenom']);
														
														$vget = "dnuper:".$donnee2['dnuper'].";mode:consult;proprio_lib:$proprio_lib;";
														$vget_crypt = base64_encode($vget);
														$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt#general"; ?>
													
														<tr>
															<td>
																<label class='label'>
																	<?php echo $proprio_lib; ?>
																</label>
															</td>
															<td>
																<label class='label'>
																<?php
																	If (isset($donnee2['droit_lib']) && $donnee2['droit_lib'] != '') {
																		echo retour_ligne(preg_replace('#\((.+)\)#i', '', $donnee2['droit_lib']), 20);
																	} Else {
																		echo "N.D.";
																	}
																?>																
																</label>
															</td>
															<td>
																<label class='label'>
																<?php
																	If (isset($donnee2['dqualp']) && $donnee2['dqualp'] != '')
																		{echo $donnee2['dqualp'];}
																	Else
																		{echo "N.D.";}
																?>
																</label>
															</td>
															<td>
																<a href='<?php echo $lien; ?>'>
																	<img src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche propri&eacute;taire">
																</a>
															</td>
														</tr> 	
												<?php 
													}
													$res_proprio_lot_lots_histo->closeCursor();
												?>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												&nbsp;
											</td>
											<td style='text-align:center'>
												<table CELLSPACING = '5' CELLPADDING ='5' style='border:0px solid black' width='100%'>
												<?php
													$rq_proprio_lot_histo_lots_histo = 
													"SELECT DISTINCT 
													t6.dnupro, 
													t7.dnuper, 
													(CASE WHEN d_ccoqua.particule_lib != '' THEN d_ccoqua.particule_lib ELSE 'N.P.' END) AS particule_lib, 
													t7.ddenom, 
													t7.nom_usage, 
													t7.prenom_usage,
													t3.date_fin
													FROM 
													cadastre.cadastre_cen t3, 
													cadastre.cptprop_cen t5, 
													cadastre.r_prop_cptprop_cen t6, 
													cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua)
													WHERE t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t3.lot_id = ?
													AND t3.date_fin != 'N.D.' AND t3.motif_fin_id = 2
													ORDER BY t6.dnupro, t7.ddenom";
													$res_proprio_lot_histo_lots_histo = $bdd->prepare($rq_proprio_lot_histo_lots_histo);
													$res_proprio_lot_histo_lots_histo->execute(array($donnees['lot_id']));
													
													If ($res_proprio_lot_histo_lots_histo->rowCount() > 0) {
														echo "
															<tr>
																<td id='ancien_proprio_slideIn' colspan='4' style='text-align:center'>
																	<label style='text-align:right;cursor:pointer;text-shadow:1px 1px 2px #ebebeb;color:#cf740d;'  class='label' onclick=\"slideIn('ancien_proprio');\">Afficher les anciens propriétaires</label>
																</td>
															</tr>";
													}
													
													While ($donnee3 = $res_proprio_lot_histo_lots_histo->fetch()) {
														$proprio_lib = proprio_lib($donnee3['particule_lib'], $donnee3['nom_usage'], $donnee3['prenom_usage'], $donnee3['ddenom']);
														
														$vget = "dnuper:".$donnee3['dnuper'].";mode:consult;proprio_lib:$proprio_lib;";
														$vget_crypt = base64_encode($vget);
														$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt#general";	
														echo "
														<tr id='ancien_proprio' style='display:none;'>
															<td>
																<label class='label' style='color:#949494'>
																	$proprio_lib
																</label>
															</td>
															<td colspan='2'>
																<label class='label' style='color:#949494'>
																	Historisé le ". date_transform($donnee3['date_fin']) . "
																</label>
															</td>
															<td>
																<a href='$lien'>
																	<img src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"Fiche propri&eacute;taire\">
																</a>
															</td>
														</tr>";	
													}
													$res_proprio_lot_histo_lots_histo->closeCursor();
												?>	
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style="border-bottom: solid #004494 1px" colspan="2"></td>
							</tr>
							<?php } ?>	
						</tbody>
					</table>
				</div>
			</div>
			<div id="loading" class="loading" style="display: none;">
				<img src="<?php echo ROOT_PATH; ?>images/ajax-loader.gif">
			</div>
		</section>			
		<?php
			$res_info_parc->closeCursor();
			$res_last_maj->closeCursor();
			$res_lots_histo->closeCursor();
			$res_proprio_lot->closeCursor();
			$res_proprio_lot_histo->closeCursor();			
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
