<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>foncier/foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Foncier';
			$h3 ='Administration de la BD Foncier';
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'), 'blocage');
			include("../includes/header.php");
			include('../includes/breadcrumb.php');			
			
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}
			
			include('foncier_fct.php');
			
			$rq_info_droit =
			"SELECT
				grant_elt_id,
				grant_elt_lib,
				grant_elt_form_id,
				grant_elt_type_acte,
				grant_group_id,
				grant_group_droit
			FROM
				foncier.d_grant_elt
				LEFT JOIN foncier.grant_group USING (grant_elt_id)
				JOIN (SELECT groname::character varying FROM pg_catalog.pg_group WHERE groname = 'grp_sig' OR groname ILIKE '%grp_foncier%') t_groupe_foncier ON (grant_group_id = groname)
			";
			$res_info_droit = $bdd->prepare($rq_info_droit);
			$res_info_droit->execute();
			$info_droit = $res_info_droit->fetchAll();
			$res_info_droit->closeCursor();
		?>
		<section id='main'>
			<h3>Administration de la BD Foncier</h3>
			
			<div style="padding: 0 5px; width: calc(100% - 10px);">
				<table CELLSPACING = '0' CELLPADDING ='0' border='1px solid #feooo' align='center'>
					<tr>
						<td>&nbsp;</td>
					<?php
						$rq_groups = "SELECT groname FROM pg_catalog.pg_group WHERE groname = 'grp_sig' OR groname ILIKE '%sgrp_foncier%' ORDER BY groname";
						$res_groups = $bdd->prepare($rq_groups);
						$res_groups->execute();
						$groups = $res_groups->fetchAll();
						For ($i=0; $i<$res_groups->rowCount(); $i++) {
					?>
						<td style='text-align:center;'>
							<label class='label'>
								<?php echo $groups[$i]['groname']; ?>
							</label>
						</td>
					<?php
						}
					?>					
					</tr>	
			<?php
				$rq_groups_droits = "
				WITH 
					t_group_foncier AS (
						SELECT grant_group_id, grant_elt_id
						FROM foncier.grant_group
						WHERE grant_group_droit = 't'
					),
					t_groname AS (
						SELECT groname, grant_elt_id
						FROM pg_catalog.pg_group LEFT JOIN t_group_foncier ON (grant_group_id::text = groname::text)
					)

				SELECT
					d_grant_elt.grant_elt_id,
					grant_elt_lib,
					grant_elt_form_id,
					grant_elt_type_acte,
					array_to_string(array_agg(DISTINCT COALESCE(groname, 'nobody')), ',') as tbl_group
				FROM
					foncier.d_grant_elt
					LEFT JOIN t_groname ON (t_groname.grant_elt_id = d_grant_elt.grant_elt_id) 
				GROUP BY d_grant_elt.grant_elt_id
				ORDER BY grant_elt_type_acte, grant_elt_id";
				$res_groups_droits = $bdd->prepare($rq_groups_droits);
				$res_groups_droits->execute();
				$groups_droits = $res_groups_droits->fetchAll();
				For ($d=0; $d<$res_groups_droits->rowCount(); $d++) {
					switch ($groups_droits[$d]['grant_elt_type_acte']) {
						case 'acte':
							$bg_color = 'rgb(208, 173, 71, 0.4)';
							break;
						case 'mf':
							$bg_color = 'rgb(68, 114, 196, 0.4)';
							break;
						case 'mu':
							$bg_color = 'rgb(112, 173, 71, 0.4)';
							break;
						case 'proprio':
							$bg_color = 'rgb(12, 150, 200, 0.4)';
							break;
					}
			?>					
					<tr style='background:<?php echo $bg_color; ?>;'>	
						<td style='text-align:right;'>
							<label class='label'>
								<?php echo $groups_droits[$d]['grant_elt_lib']; ?>
							</label>	
						</td>
				<?php
						For ($i=0; $i<$res_groups->rowCount(); $i++) {
							If (in_array($groups[$i]['groname'], explode(',', $groups_droits[$d]['tbl_group']))) {
								$checked = 'checked';
							} Else {
								$checked = '';
							}
				?>
						<td style='text-align:center;'>
							<input type='checkbox' width='15px' class='editbox' style='-moz-transform: scale(1.2);' id='<?php echo $groups_droits[$d]['grant_elt_id']; ?>-<?php echo $groups[$i]['groname']; ?>' <?php echo $checked; ?> onclick="updateGrantGroup(this, '<?php echo $groups[$i]['groname']; ?>', <?php echo $groups_droits[$d]['grant_elt_id']; ?>);">
						</td>					
				<?php
					}
				?>
					</tr>
			<?php
				}
			?>
				</table>
			</div>
		</section>
		<?php 
			$res_groups->closeCursor();
			$res_groups_droits->closeCursor();
			$bdd = null;
			include("../includes/footer.php"); 
		?>
	</body>
</html>
