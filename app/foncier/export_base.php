<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	If (isset($_GET['filename'])) {
		$filename = $_GET['filename'];
	} Else {
		$filename = 'BD_FONCIER_export_'.date("Y-m-d").'.csv';
	}
	header("Content-type: text/csv charset=utf-8");
	header("Content-disposition: attachment; filename=$filename");
	
	try {
		$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
	}
	catch (Exception $e) {
		die(include('../includes/prob_tech.php'));
	}
	
	$rq_export = "
	SELECT 
		COALESCE(t_territoires.lst_territoires, 'N.P.') as lst_territoires,
		t_communes.lst_communes,
		sites.site_id,
		sites.site_nom,
		d_typsite.typsite_lib,
		CASE WHEN sites.site_id = 'PCEN' THEN 'N.P.' ELSE d_milieux.milieu_lib END AS milieu_lib,
		t_mfu.acte_id as acte_id,
		t_mfu.lib as libelle,
		t_mfu.acte_type,
		t_mfu.acte_statut,
		sum(CASE WHEN t_actes.surf_mu > 0 THEN t_actes.surf_mu ELSE dcntlo END) as surf_maitrise,
		t_mfu.prix_acq,
		t_mfu.prix_acq_cen,
		round((t_mfu.prix_acq / (sum(CASE WHEN t_actes.surf_mu > 0 THEN t_actes.surf_mu ELSE dcntlo END))), 2) as prix_m2,
		t_mfu.frais_acte,
		t_mfu.date_effective,
		CASE WHEN t_mfu.date_compromis_vente = t_mfu.date_promesse_achat OR t_mfu.date_promesse_achat = 'N.D.' THEN t_mfu.date_compromis_vente ELSE t_mfu.date_promesse_achat END as date_cvpa,
		t_mfu.date_signature,
		t_mfu.date_effet,
		CASE WHEN t_mfu.clauses_part = true THEN 'Oui' ELSE 'Non' END as clauses_part,
		t_mfu.maj_date,

		array_to_string(array_agg(DISTINCT t_zh.id_bdd), '/') as lst_id_bdd,
		array_to_string(array_agg(DISTINCT t_zh.site_name), '/') as lst_zh_nom,

		CASE WHEN t_prob.mfu_id IS NOT NULL THEN 'ERREUR' ELSE NULL END as prob
	FROM
		sites.sites
		LEFT JOIN (
			SELECT site_id, array_to_string(array_agg(DISTINCT v_terrcg.territoire_lib), ' / ') as lst_territoires
			FROM sites.sites 
			JOIN territoires.v_terrcg ON (st_intersects(sites.geom_ecolo, v_terrcg.geom))
			GROUP BY site_id
		) t_territoires USING (site_id)
		JOIN sites.d_typsite USING (typsite_id)
		JOIN sites.d_milieux USING (milieu_id)
		--JOIN inventaires.suivi_zh ON (st_intersects(suivi_zh.geom, sites.geom_ecolo))
		JOIN foncier.cadastre_site t4 USING (site_id)
		JOIN (
			SELECT DISTINCT
				r_cad_site_mf.cad_site_id as cad_site_id,
				r_cad_site_mf.mf_id as acte_id,
				-1 as surf_mu,
				parcelles_cen.geom
			FROM
				foncier.r_cad_site_mf 
				JOIN foncier.cadastre_site USING (cad_site_id) 
				JOIN cadastre.cadastre_cen USING (cad_cen_id)
				JOIN cadastre.lots_cen USING (lot_id)
				JOIN cadastre.parcelles_cen USING (par_id)
			WHERE
				date_sortie = 'N.D.'
			UNION
			SELECT DISTINCT
				r_cad_site_mu.cad_site_id as cad_site_id,
				r_cad_site_mu.mu_id as acte_id,
				r_cad_site_mu.surf_mu as surf_mu,
				parcelles_cen.geom
			FROM
				foncier.r_cad_site_mu
				JOIN foncier.cadastre_site USING (cad_site_id) 
				JOIN cadastre.cadastre_cen USING (cad_cen_id)
				JOIN cadastre.lots_cen USING (lot_id)
				JOIN cadastre.parcelles_cen USING (par_id)
			WHERE
				date_sortie = 'N.D.'
			ORDER BY acte_id) t_actes ON (t4.cad_site_id = t_actes.cad_site_id)	
		JOIN (
			SELECT 
				mf_acquisitions.mf_id as acte_id,
				mf_acquisitions.mf_lib as lib,
				mf_acquisitions.date_sign_acte as date_effective,
				mf_acquisitions.date_compromis_pv_proprio as date_compromis_vente,
				mf_acquisitions.date_compromis_pa_president as date_promesse_achat,
				mf_acquisitions.date_sign_acte as date_signature,
				'N.P.' as date_effet,
				d_typmf.typmf_lib as acte_type,
				statutmf as acte_statut,
				t_prix.mffrais as prix_acq, 
				t_prix_cen.mffrais as prix_acq_cen,
				t_frais.mffrais as frais_acte,
				mf_clauses_part as clauses_part,
				mf_acquisitions.maj_date
			FROM 
				foncier.mf_acquisitions
				JOIN foncier.d_typmf USING (typmf_id)
				LEFT JOIN (
					SELECT r_frais_mf.mf_id, sum(r_frais_mf.mffrais) as mffrais
					FROM foncier.r_frais_mf 
					WHERE typfraismf_id IN (1, 5)
					GROUP BY r_frais_mf.mf_id
				) as t_prix USING (mf_id)
				LEFT JOIN (
					SELECT r_frais_mf.mf_id, r_frais_mf.mffrais as mffrais
					FROM foncier.r_frais_mf 
					WHERE typfraismf_id = 1
				) as t_prix_cen USING (mf_id)
				LEFT JOIN (
					SELECT r_frais_mf.mf_id, r_frais_mf.mffrais
					FROM foncier.r_frais_mf 
					WHERE typfraismf_id = 2
				) as t_frais USING (mf_id)
				JOIN foncier.r_mfstatut USING (mf_id)
				JOIN (
					SELECT 
						r_mfstatut.mf_id, 
						Max(r_mfstatut.date) as statut_date 
					FROM 
						foncier.r_mfstatut 
					GROUP BY r_mfstatut.mf_id) as tt1 ON (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id)
				JOIN foncier.d_statutmf USING (statutmf_id)
			--WHERE
				--r_mfstatut.statutmf_id = 1
			GROUP BY 
				acte_id,
				acte_type,
				acte_statut,
				t_prix.mffrais,
				t_prix_cen.mffrais,
				t_frais.mffrais
			UNION
			SELECT 
				mu_conventions.mu_id as acte_id,
				mu_conventions.mu_lib as lib,
				mu_conventions.date_effet_conv as date_effective,
				'N.P.' as date_compromis_vente,
				'N.P.' as date_promesse_achat,
				mu_conventions.date_sign_conv as date_signature,
				mu_conventions.date_effet_conv as date_effet,
				d_typmu.typmu_lib as acte_type,
				statutmu as acte_statut,
				0::numeric(10,2) as prix_acq,
				0::numeric(10,2) as prix_acq_cen,
				sum(t_frais.mufrais) as frais_acte,
				mu_clauses_part as clauses_part,
				mu_conventions.maj_date
			FROM 
				
				foncier.mu_conventions
				JOIN foncier.d_typmu USING (typmu_id)
				LEFT JOIN (
					SELECT r_frais_mu.mu_id, r_frais_mu.mufrais
					FROM foncier.r_frais_mu 
					WHERE typfraismu_id = 1
				) as t_frais USING (mu_id)
				JOIN foncier.r_mustatut USING (mu_id)
				JOIN (
					SELECT 
						r_mustatut.mu_id, 
						Max(r_mustatut.date) as statut_date 
					FROM 
						foncier.r_mustatut 
					GROUP BY r_mustatut.mu_id) as tt1 ON (r_mustatut.date = tt1.statut_date AND r_mustatut.mu_id = tt1.mu_id)
				JOIN foncier.d_statutmu USING (statutmu_id)
			GROUP BY 
				acte_id,
				acte_type,
				acte_statut
			ORDER BY acte_id) t_mfu ON (t_actes.acte_id = t_mfu.acte_id)

		LEFT JOIN inventaires.v2_zh t_zh ON (st_intersects(t_actes.geom, t_zh.geom))
			
		JOIN cadastre.cadastre_cen t3 ON (t4.cad_cen_id = t3.cad_cen_id)
		JOIN cadastre.lots_cen t2 USING (lot_id)
		JOIN cadastre.parcelles_cen t1 USING (par_id)
		
		JOIN (
			SELECT site_id, array_to_string(array_agg(DISTINCT v_communes.nom), ' / ') as lst_communes
			FROM administratif.v_communes
				JOIN cadastre.parcelles_cen t1 ON (t1.codcom = v_communes.code_insee)
				JOIN cadastre.lots_cen t2 USING (par_id)
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			GROUP BY site_id
		) t_communes USING (site_id)
		LEFT JOIN (
			SELECT DISTINCT v_prob_cadastre_cen.mfu_id FROM alertes.v_prob_cadastre_cen 
			UNION 
			SELECT DISTINCT v_prob_mfu_baddata.mfu_id FROM alertes.v_prob_mfu_baddata 
			UNION 
			SELECT DISTINCT v_prob_mfu_sansparc.mfu_id FROM alertes.v_prob_mfu_sansparc
		) t_prob ON (t_mfu.acte_id = t_prob.mfu_id)
	WHERE
		--st_npoints(sites.geom_ecolo) > 20 AND
		t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'
	GROUP BY
		t_territoires.lst_territoires,
		t_communes.lst_communes,
		sites.site_id,
		sites.site_nom,
		d_typsite.typsite_lib,
		d_milieux.milieu_lib,
		t_mfu.acte_type,
		t_mfu.acte_statut,
		t_mfu.prix_acq,
		t_mfu.prix_acq_cen,
		t_mfu.frais_acte,
		t_mfu.acte_id,
		t_mfu.lib,
		t_mfu.date_effective,
		t_mfu.date_compromis_vente,
		t_mfu.date_promesse_achat,
		t_mfu.date_signature,
		t_mfu.date_effet,
		t_mfu.clauses_part,
		t_mfu.maj_date,
		t_prob.mfu_id
	ORDER BY acte_id";
	$res_export = $bdd->prepare($rq_export);
	$res_export->execute();
	$export = $res_export->fetchAll();
	$entete = array('Territoire(s)', 'Commune(s)', 'Site ID', 'Nom du site', 'Type de site', 'Milieu', 'Acte ID', utf8_decode('Libellé'), 'Type acte', 'Statut acte', 'Surf. acte', 'Prix acq.', 'Prix acq. CEN', utf8_decode('Prix m²'), 'Frais acte', 'Date effective', 'Date CV/PA', 'Date sign. acte', 'Date effet conv.', 'Clauses part.', utf8_decode('Date màj.'), 'ID(s) ZH', 'Nom(s) ZH', utf8_decode('Problème BDD'));
	For ($i=0; $i<count($entete); $i++) {
		echo $entete[$i] . ';';
	}
	echo "\r\n";
	For ($row=0; $row<$res_export->rowCount(); $row++) {
		For ($col=0; $col<$res_export->columnCount(); $col++) {
			If (($col == 11) || ($col == 12) || ($col == 13) || ($col == 14)) {
				echo str_replace('.', ',', $export[$row][$col]) . ';';
			} Else {
				echo utf8_decode($export[$row][$col]) . ';';
			}
		}
		echo "\r\n";
	}
	$res_export->closeCursor();
	$bdd = null;	
?>