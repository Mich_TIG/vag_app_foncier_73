<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');		
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>	
	<body>
		<?php 
			$h2 ='Foncier';
			$h3 ='Recherche par propriétaire';
			include("../includes/header.php");
			include('../includes/breadcrumb.php');
		
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'));
			include("foncier_fct.php");
			//include('../includes/construction.php');	
			
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}		
		?>
		<section id='main'>
			<h3>Recherche par propriétaire</h3>
			<table id='tbl_proprio' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
				<tbody id='tbl_proprio_entete'>
					<form id="frm_proprio_entete" onsubmit="return false">
						<tr>
							<td class='filtre' align='right' style="padding:6px 1px;" width='47.5%'>
								<input class='editbox' oninput="create_lst_proprio(0, 'NULL');" type='text' id='filtre_site' size='6' placeholder='SITE ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SITE ID';">
								<input class='editbox' oninput="create_lst_proprio(0, 'NULL');" type='text' id='filtre_nom' size='35' placeholder='Propriétaire' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Propriétaire';">
							</td>
							<td class='filtre' align='left' width='auto' style="padding-left:18px;">
								<img style='cursor:pointer;' onclick="
								document.getElementById('filtre_nom').value='';
								document.getElementById('filtre_site').value='';
								create_lst_proprio(0, 'NULL');
								" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
							</td>
						</tr>
					</form>
				</tbody>
				<tbody id='tbl_proprio_corps'>				
				<?php
					$rq_lst_proprios = 
					"SELECT DISTINCT 
						t7.dnuper, 
						(CASE WHEN d_ccoqua.particule_lib != '' THEN d_ccoqua.particule_lib ELSE 'N.P.' END) AS particule_lib, 
						t7.ddenom, 
						t7.nom_usage,
						t7.prenom_usage,
						t7.nom_jeunefille,
						count(t1.dnupla) as nbr_parcelle, sum(t1.dcntpa) as surf_total
					FROM 
						cadastre.parcelles_cen t1
						JOIN cadastre.lots_cen t2 USING (par_id)
						JOIN cadastre.cadastre_cen t3 USING (lot_id)
						JOIN foncier.cadastre_site t4 USING (cad_cen_id)
						JOIN cadastre.cptprop_cen t5 USING (dnupro)
						JOIN cadastre.r_prop_cptprop_cen t6 USING (dnupro)
						JOIN cadastre.proprios_cen t7 USING (dnuper)
						LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua) 
					WHERE 
						t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'
					GROUP BY t7.dnuper, d_ccoqua.particule_lib, t7.ddenom, t7.nom_usage, t7.prenom_usage, t7.nom_jeunefille
					ORDER BY nom_usage
					OFFSET 0 LIMIT 100";
					$res_lst_proprios = $bdd->prepare($rq_lst_proprios);
					$res_lst_proprios->execute();
					
					$offset = 0;
					$nbr_result = 100;
					$suiv_prev = fct_suiv_prec($offset, $nbr_result, $res_lst_proprios->rowCount(), 2, 'create_lst_proprio(offset, null)');
					
					echo $suiv_prev;						
					$j=0;
					While ($donnees = $res_lst_proprios->fetch()) {
						$proprio_lib = proprio_lib($donnees['particule_lib'], $donnees['nom_usage'], $donnees['prenom_usage'], $donnees['ddenom']);
						$vget = "dnuper:".$donnees['dnuper'].";mode:consult;proprio_lib:$proprio_lib;";
						$vget_crypt = base64_encode($vget);
						$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt#general"; ?>
						<tr class='toggle_tr l<?php echo $j; ?>' onclick="document.location='<?php echo $lien; ?>';  return false;">	
							<td style='text-align:right'>
								<a href='<?php echo $lien; ?>'><?php echo $proprio_lib; If (isset($donnees['nom_jeunefille']) AND $donnees['nom_jeunefille'] != '' AND $donnees['nom_jeunefille'] != 'N.P.') 
								{echo " (née $donnees[nom_jeunefille])";}?></a>
							</td>
							<td>
								<a href='<?php echo $lien; ?>'><i>propriétaire de <?php echo $donnees['nbr_parcelle']; ?> parcelle<?php If ($donnees['nbr_parcelle'] > 1) {echo "s";} ?> sur nos sites pour une surface de <?php echo conv_are($donnees['surf_total']); ?> ha.a.ca</i></a>
							</td> 
						</tr>	<?php
						If($j == 0)
							{$j=1;}
						Else
							{$j=0;}
					}					
					echo $suiv_prev;
				?>
				</tbody>
				</tr>
			</table>
			</form>
		</section>
		<?php 
			$res_lst_proprios->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
