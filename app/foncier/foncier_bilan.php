<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/specific_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">	
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>		
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Foncier';
			$h3 ='Bilan complet';
			include("../includes/header.php");
			include('../includes/breadcrumb.php');		
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'));
			include('foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
			include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
			//include('../includes/construction.php');
			
			//Timer
			$timedeb = time();
			
			$lst_mois = array ('Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre');
			$lst_mois_num = array ('01','02','03','04','05','06','07','08','09','10','11','12');
			
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}
			
			$debut = '';
			If (isset($_POST['flt_mois_debut']) && isset($_POST['need_flt_annee_debut']) && $_POST['need_flt_annee_debut'] != '' && $_POST['need_flt_annee_debut'] != '-1') {
				$debut = $_POST['need_flt_annee_debut'];
				If ($_POST['flt_mois_debut'] != '' && $_POST['flt_mois_debut'] != '-1') {
					$debut .= '-' . $_POST['flt_mois_debut']. '-00';
				} Else {
					$debut .= '-01-00';
				}
			}
			
			$fin = '';
			If (isset($_POST['flt_mois_fin']) && isset($_POST['need_flt_annee_fin']) && $_POST['need_flt_annee_fin'] != '' && $_POST['need_flt_annee_fin'] != '-1') {
				$fin = $_POST['need_flt_annee_fin'];
				If ($_POST['flt_mois_fin'] != '' && $_POST['flt_mois_fin'] != '-1' ) {
					$fin .= '-' . $_POST['flt_mois_fin']. '-32';
				} Else {
					$fin .= '-12-32';
				}
			}
			
			$rq_lst_site = "
			SELECT DISTINCT
				sites.site_id,
				sites.site_nom, 
				d_typsite.typsite_lib AS type_site, 
				COALESCE(sum(tmfu.surf_acq), 0) AS surf_acq, 
				COALESCE(sum(tmfu.surf_conv), 0) AS surf_conv, 
				COALESCE(sum(tmfu.surf_acq + tmfu.surf_conv), 0) AS surf_mfu,
				CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan 
			FROM 
				sites.d_typsite 
				JOIN sites.sites USING (typsite_id)
				JOIN (
					SELECT DISTINCT sites.site_id, 'Acquisition'::character varying(25) as mfu, lots_cen.lot_id as lot_id, lots_cen.dcntlo as surf_acq, 0::bigint as surf_conv
					FROM cadastre.lots_cen 
						JOIN cadastre.cadastre_cen USING (lot_id)
						JOIN foncier.cadastre_site USING (cad_cen_id)
						JOIN sites.sites USING (site_id)
						JOIN foncier.r_cad_site_mf USING (cad_site_id)
						JOIN foncier.mf_acquisitions USING (mf_id)
						JOIN foncier.d_typmf USING (typmf_id)
						JOIN foncier.r_mfstatut USING (mf_id)
						JOIN foncier.d_statutmf USING (statutmf_id)
						JOIN (
							SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
							FROM foncier.r_mfstatut";
							If ($debut != '') {
								$rq_lst_site .= " WHERE r_mfstatut.date > '$debut'";
							}
							If ($fin != '' && $debut == '') {
								$rq_lst_site .= " WHERE r_mfstatut.date < '$fin'";
							} Else If ($fin != '' && $debut != '') {
								$rq_lst_site .= " AND r_mfstatut.date < '$fin'";
							}
							$rq_lst_site .= "
							GROUP BY r_mfstatut.mf_id
						) tmax ON (r_mfstatut.mf_id = tmax.mf_id AND r_mfstatut.date = tmax.statut_date)
					WHERE d_statutmf.statutmf_id = 1 ";
					If ($debut != '') {
						$rq_lst_site .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND r_cad_site_mf.date_sortie > '$debut'";
					} Else {
						$rq_lst_site .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mf.date_sortie = 'N.D.'";
					}
					If ($fin != '') {
						$rq_lst_site .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mf.date_entree < '$fin'";
					} 
					$rq_lst_site .= " 
					UNION
					SELECT DISTINCT sites.site_id, 'Convention'::character varying(25) as mfu, lots_cen.lot_id as lot_id, 0::bigint as surf_acq, CASE WHEN r_cad_site_mu.surf_mu is not Null THEN r_cad_site_mu.surf_mu ELSE lots_cen.dcntlo END as surf_conv
					FROM cadastre.lots_cen
						JOIN cadastre.cadastre_cen USING (lot_id)
						JOIN foncier.cadastre_site USING (cad_cen_id)
						JOIN sites.sites USING (site_id)
						JOIN foncier.r_cad_site_mu USING (cad_site_id)
						JOIN foncier.mu_conventions USING (mu_id)
						JOIN foncier.d_typmu USING (typmu_id)
						JOIN foncier.r_mustatut USING (mu_id)
						JOIN foncier.d_statutmu USING (statutmu_id)
						JOIN (
							SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
							FROM foncier.r_mustatut";
							If ($debut != '') {
								$rq_lst_site .= " WHERE r_mustatut.date > '$debut'";
							}
							If ($fin != '' && $debut == '') {
								$rq_lst_site .= " WHERE r_mustatut.date < '$fin'";
							} Else If ($fin != '' && $debut != '') {
								$rq_lst_site .= " AND r_mustatut.date < '$fin'";
							}
							$rq_lst_site .= "
							GROUP BY r_mustatut.mu_id
						) tmax ON (r_mustatut.mu_id = tmax.mu_id AND r_mustatut.date = tmax.statut_date)
					WHERE d_statutmu.statutmu_id = 1 ";
					If ($debut != '') {
						$rq_lst_site .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND r_cad_site_mu.date_sortie > '$debut' AND mu_conventions.date_effet_conv > '$debut'";
					} Else {
						$rq_lst_site .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mu.date_sortie = 'N.D.'";
					}
					If ($fin != '') {
						$rq_lst_site .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mu.date_entree < '$fin' AND  mu_conventions.date_effet_conv < '$fin'";
					} Else {
						$rq_lst_site .= " AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text";
					}
					$rq_lst_site .= " 
					AND d_typmu.mfu = true
				) tmfu USING (site_id)
				LEFT JOIN (
					SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata 
					UNION 
					SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen WHERE statutmfu_id = 1
					UNION 
					SELECT DISTINCT site_id FROM alertes.v_alertes_mu WHERE alerte LIKE 'termin%'
					UNION 
					SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc
				) tprob USING (site_id)
			GROUP BY sites.site_id, 
				sites.site_nom, 
				d_typsite.typsite_lib,
				tprob.site_id
			ORDER BY sites.site_id;";
			$res_lst_site = $bdd->prepare($rq_lst_site);
			$res_lst_site->execute();
			$lst_site = $res_lst_site->fetchAll();
			// echo $rq_lst_site;
			
			$rq_lst_metasite = "
			SELECT DISTINCT
				metasites.metasite_id,
				metasites.metasite_nom, 
				d_typmetasite.typmetasite_lib AS type_metasite, 
				COALESCE(sum(tmfu.surf_acq), 0) AS surf_acq, 
				COALESCE(sum(tmfu.surf_conv), 0) AS surf_conv, 
				COALESCE(sum(tmfu.surf_acq + tmfu.surf_conv), 0) AS surf_mfu,
				sum(tmfu.etat_bilan) as etat_bilan
			FROM 
				sites.d_typmetasite 
				JOIN sites.metasites USING (typmetasite_id)
				JOIN (
					SELECT DISTINCT 
						metasites.metasite_id, 
						'Acquisition'::character varying(25) as mfu, 
						lots_cen.lot_id as lot_id,
						lots_cen.dcntlo as surf_acq, 
						0::bigint as surf_conv, 
						CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan
					FROM cadastre.lots_cen 
						JOIN cadastre.cadastre_cen USING (lot_id)
						JOIN foncier.cadastre_site USING (cad_cen_id)
						JOIN sites.sites USING (site_id)
						JOIN sites.r_sites_metasites USING (site_id)
						JOIN sites.metasites USING (metasite_id)
						JOIN foncier.r_cad_site_mf USING (cad_site_id)
						JOIN foncier.mf_acquisitions USING (mf_id)
						JOIN foncier.d_typmf USING (typmf_id)
						JOIN foncier.r_mfstatut USING (mf_id)
						JOIN foncier.d_statutmf USING (statutmf_id)
						JOIN (
							SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
							FROM foncier.r_mfstatut";
							If ($debut != '') {
								$rq_lst_metasite .= " WHERE r_mfstatut.date > '$debut'";
							}
							If ($fin != '' && $debut == '') {
								$rq_lst_metasite .= " WHERE r_mfstatut.date < '$fin'";
							} Else If ($fin != '' && $debut != '') {
								$rq_lst_metasite .= "AND r_mfstatut.date < '$fin'";
							}
							$rq_lst_metasite .= "
							GROUP BY r_mfstatut.mf_id
						) tmax ON (r_mfstatut.mf_id = tmax.mf_id AND r_mfstatut.date = tmax.statut_date)
						LEFT JOIN (
							SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata UNION SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen UNION SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc
						) tprob USING (site_id)
					WHERE d_statutmf.statutmf_id = 1 ";
					If ($debut != '') {
						$rq_lst_metasite .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND (r_cad_site_mf.date_sortie > '$debut' OR r_cad_site_mf.date_sortie = 'N.D.') ";
					} Else {
						$rq_lst_metasite .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mf.date_sortie = 'N.D.'";
					}
					If ($fin != '') {
						$rq_lst_metasite .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mf.date_entree < '$fin'";
					} 
					$rq_lst_metasite .= "
					UNION
					SELECT 
						metasites.metasite_id, 
						'Convention'::character varying(25) as mfu, 
						lots_cen.lot_id as lot_id,
						0::bigint as surf_acq, 
						CASE WHEN r_cad_site_mu.surf_mu is not Null THEN r_cad_site_mu.surf_mu ELSE lots_cen.dcntlo END as surf_conv, 
						CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan
					FROM cadastre.lots_cen
						JOIN cadastre.cadastre_cen USING (lot_id)
						JOIN foncier.cadastre_site USING (cad_cen_id)
						JOIN sites.sites USING (site_id)
						JOIN sites.r_sites_metasites USING (site_id)
						JOIN sites.metasites USING (metasite_id)
						JOIN foncier.r_cad_site_mu USING (cad_site_id)
						JOIN foncier.mu_conventions USING (mu_id)
						JOIN foncier.d_typmu USING (typmu_id)
						JOIN foncier.r_mustatut USING (mu_id)
						JOIN foncier.d_statutmu USING (statutmu_id)
						JOIN (
							SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
							FROM foncier.r_mustatut";
							If ($debut != '') {
								$rq_lst_metasite .= " WHERE r_mustatut.date > '$debut'";
							}
							If ($fin != '' && $debut == '') {
								$rq_lst_metasite .= " WHERE r_mustatut.date < '$fin'";
							} Else If ($fin != '' && $debut != '') {
								$rq_lst_metasite .= "AND r_mustatut.date < '$fin'";
							}
							$rq_lst_metasite .= "
							GROUP BY r_mustatut.mu_id
						) as tmax ON (r_mustatut.mu_id = tmax.mu_id AND r_mustatut.date = tmax.statut_date)
						LEFT JOIN (
							SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata UNION SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen UNION SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc
						) tprob USING (site_id)
					WHERE d_statutmu.statutmu_id = 1 ";
					If ($debut != '') {
						$rq_lst_metasite .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND r_cad_site_mu.date_sortie > '$debut' AND mu_conventions.date_effet_conv > '$debut'";
					} Else {
						$rq_lst_metasite .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mu.date_sortie = 'N.D.'";
					}
					If ($fin != '') {
						$rq_lst_metasite .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mu.date_entree < '$fin' AND  mu_conventions.date_effet_conv < '$fin'";
					} Else {
						$rq_lst_metasite .= " AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text";
					}
					$rq_lst_metasite .= "
					AND d_typmu.mfu = true
				) tmfu USING (metasite_id)		
			GROUP BY metasites.metasite_id, metasites.metasite_nom, d_typmetasite.typmetasite_lib
			ORDER BY metasites.metasite_id;";
			// echo $rq_lst_metasite;
			$res_lst_metasite = $bdd->prepare($rq_lst_metasite);
			$res_lst_metasite->execute();
			$lst_metasite = $res_lst_metasite->fetchAll();
			
			$rq_lst_communes = "
				SELECT nom, code_insee
				FROM administratif.communes
				ORDER BY nom";
			$res_lst_communes = $bdd->prepare($rq_lst_communes);
			$res_lst_communes->execute();
			$lst_communes = $res_lst_communes->fetchAll();	
			
			$alphabet = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
			
			$rq_bilan_commune = "
				SELECT 
					communes.nom, 
					communes.code_insee, 
					COALESCE(sum(tcomsitag.surf_acq), 0) AS surf_acq, 
					COALESCE(sum(tcomsitag.surf_conv), 0) AS surf_conv, 
					COALESCE(sum(tcomsitag.surf_acq + tcomsitag.surf_conv), 0) AS surf_mfu, 
					array_to_string(array_agg(DISTINCT tcomsitag.site_id ORDER BY tcomsitag.site_id), ',') as tbl_site_id, 
					array_to_string(array_agg(tcomsitag.surf_acq ORDER BY tcomsitag.site_id), ',') as tbl_surf_acq, 
					array_to_string(array_agg(tcomsitag.surf_conv ORDER BY tcomsitag.site_id), ',') as tbl_surf_conv, 
					array_to_string(array_agg(tcomsitag.surf_acq + tcomsitag.surf_conv ORDER BY tcomsitag.site_id), ',') as tbl_surf_mfu,
					sum(etat_bilan) as etat_bilan
				FROM 
					administratif.communes 
					LEFT JOIN (
						SELECT 
							tcomsit.code_insee, 
							tcomsit.site_id,
							sum(tcomsit.surf_acq) AS surf_acq, 
							sum(tcomsit.surf_conv) AS surf_conv, 
							sum(tcomsit.surf_acq + tcomsit.surf_conv) AS surf_mfu, 
							sum(etat_foncier) as etat_bilan
						FROM (
							SELECT  
								communes.code_insee, sites.site_id, 
								COALESCE(tmfu.surf_acq, 0) AS surf_acq, 
								COALESCE(tmfu.surf_conv, 0) AS surf_conv, 
								COALESCE(tmfu.surf_acq, 0) + COALESCE(tmfu.surf_conv, 0) AS surf_mfu, 
								etat_bilan as etat_foncier
							FROM 
								sites.sites 
								LEFT JOIN (
									WITH tprob AS (
										SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata UNION SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen UNION SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc)
									SELECT 
										DISTINCT parcelles_cen.codcom, sites.site_id, 
										'Acquisition'::character varying(25) as mfu,
										parcelles_cen.par_id as par_id, lots_cen.lot_id as lot_id,
										lots_cen.dcntlo as surf_acq, 
										0::bigint as surf_conv, 
										CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan  
									FROM cadastre.parcelles_cen
										JOIN cadastre.lots_cen USING (par_id)
										JOIN cadastre.cadastre_cen USING (lot_id)
										JOIN foncier.cadastre_site USING (cad_cen_id)
										JOIN sites.sites USING (site_id)
										JOIN foncier.r_cad_site_mf USING (cad_site_id)
										JOIN foncier.mf_acquisitions USING (mf_id)
										JOIN foncier.d_typmf USING (typmf_id)
										JOIN foncier.r_mfstatut USING (mf_id)
										JOIN foncier.d_statutmf USING (statutmf_id)
										JOIN (
											SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
											FROM foncier.r_mfstatut";
											If ($debut != '') {
												$rq_bilan_commune .= " WHERE r_mfstatut.date > '$debut' ";
											}
											If ($fin != '' && $debut == '') {
												$rq_bilan_commune .= " WHERE r_mfstatut.date < '$fin' ";
											} Else If ($fin != '' && $debut != '') {
												$rq_bilan_commune .= "AND r_mfstatut.date < '$fin' ";
											}
											$rq_bilan_commune .= "
											GROUP BY r_mfstatut.mf_id
										) tmax ON (r_mfstatut.mf_id = tmax.mf_id AND r_mfstatut.date = tmax.statut_date)
										LEFT JOIN tprob USING (site_id)
									WHERE d_statutmf.statutmf_id = 1 ";
									If ($debut != '') {
										$rq_bilan_commune .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND (r_cad_site_mf.date_sortie > '$debut' OR r_cad_site_mf.date_sortie = 'N.D.') ";
									} Else {
										$rq_bilan_commune .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mf.date_sortie = 'N.D.' ";
									}
									If ($fin != '') {
										$rq_bilan_commune .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mf.date_entree < '$fin' ";
									} 
									$rq_bilan_commune .= "
									UNION
									SELECT 
										DISTINCT tparc.codcom, sites.site_id, 
										'Convention'::character varying(25) as mfu, 
										tparc.par_id as par_id, lots_cen.lot_id as lot_id,
										0::bigint as surf_acq, 
										CASE WHEN r_cad_site_mu.surf_mu is not Null THEN r_cad_site_mu.surf_mu ELSE lots_cen.dcntlo END as surf_conv, 
										CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan  
									FROM 
										(SELECT DISTINCT par_id, codcom FROM cadastre.parcelles_cen) tparc
										JOIN cadastre.lots_cen USING (par_id)
										JOIN cadastre.cadastre_cen USING (lot_id)
										JOIN foncier.cadastre_site USING (cad_cen_id)
										JOIN sites.sites USING (site_id)
										JOIN foncier.r_cad_site_mu USING (cad_site_id)
										JOIN foncier.mu_conventions USING (mu_id)
										JOIN foncier.d_typmu USING (typmu_id)
										JOIN foncier.r_mustatut USING (mu_id)
										JOIN foncier.d_statutmu USING (statutmu_id)
										JOIN (
											SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
											FROM foncier.r_mustatut";
											If ($debut != '') {
												$rq_bilan_commune .= " WHERE r_mustatut.date > '$debut'";
											}
											If ($fin != '' && $debut == '') {
												$rq_bilan_commune .= " WHERE r_mustatut.date < '$fin'";
											} Else If ($fin != '' && $debut != '') {
												$rq_bilan_commune .= "AND r_mustatut.date < '$fin'";
											}
											$rq_bilan_commune .= "
											GROUP BY r_mustatut.mu_id
										) tmax ON (r_mustatut.mu_id = tmax.mu_id AND r_mustatut.date = tmax.statut_date)
										LEFT JOIN tprob USING (site_id)
									WHERE d_statutmu.statutmu_id = 1 ";
									If ($debut != '') {
										$rq_bilan_commune .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND (r_cad_site_mu.date_sortie > '$debut' AND r_cad_site_mu.date_sortie = 'N.D.') AND mu_conventions.date_effet_conv > '$debut'";
									} Else {
										$rq_bilan_commune .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mu.date_sortie = 'N.D.'";
									}
									If ($fin != '') {
										$rq_bilan_commune .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mu.date_entree < '$fin' AND  mu_conventions.date_effet_conv < '$fin'";
									} Else {
										$rq_bilan_commune .= " AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text";
									}
									$rq_bilan_commune .= "
									AND d_typmu.mfu = true
								) tmfu USING (site_id) 
								JOIN cadastre.parcelles_cen t1 USING (par_id) 
								JOIN administratif.communes ON (t1.codcom = communes.code_insee AND communes.code_insee = tmfu.codcom) 
							) tcomsit
						GROUP BY tcomsit.code_insee, tcomsit.site_id
					) tcomsitag ON (tcomsitag.code_insee = communes.code_insee)
				WHERE surf_mfu > 0
				GROUP BY communes.nom, communes.code_insee
				ORDER BY communes.code_insee;";
				// echo $rq_bilan_commune;
				$res_bilan_commune = $bdd->prepare($rq_bilan_commune);
				
				$rq_bilan_collectivites = "
				SELECT 
					territoires.territoire_lib, 
					territoires.territoire_id, 
					territoires.typterritoire_id,
					COALESCE(sum(tcomsitag.surf_acq), 0) AS surf_acq, 
					COALESCE(sum(tcomsitag.surf_conv), 0) AS surf_conv, 
					COALESCE(sum(tcomsitag.surf_acq + tcomsitag.surf_conv), 0) AS surf_mfu, 
					array_to_string(array_agg(DISTINCT tcomsitag.site_id ORDER BY tcomsitag.site_id), ',') as tbl_site_id, 
					array_to_string(array_agg(tcomsitag.surf_acq ORDER BY tcomsitag.site_id), ',') as tbl_surf_acq, 
					array_to_string(array_agg(tcomsitag.surf_conv ORDER BY tcomsitag.site_id), ',') as tbl_surf_conv, 
					array_to_string(array_agg(tcomsitag.surf_acq + tcomsitag.surf_conv ORDER BY tcomsitag.site_id), ',') as tbl_surf_mfu,
					sum(etat_bilan) as etat_bilan
				FROM 
					territoires.territoires 
					JOIN (
						SELECT 
							tcomsit.territoire_id, 
							tcomsit.site_id, 
							sum(tcomsit.surf_acq) AS surf_acq, 
							sum(tcomsit.surf_conv) AS surf_conv, 
							sum(tcomsit.surf_acq + tcomsit.surf_conv) AS surf_mfu, 
							sum(etat_foncier) as etat_bilan 
						FROM (
							SELECT  
								territoires.territoire_id, 
								sites.site_id, 
								COALESCE(tmfu.surf_acq, 0) AS surf_acq, 
								COALESCE(tmfu.surf_conv, 0) AS surf_conv, 
								COALESCE(tmfu.surf_acq, 0) + COALESCE(tmfu.surf_conv, 0) AS surf_mfu, 
								etat_bilan as etat_foncier
							FROM 
								sites.sites 
								LEFT JOIN (
									WITH tprob AS (
										SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata UNION SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen UNION SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc
									)
									SELECT DISTINCT 
										parcelles_cen.codcom, sites.site_id, 
										'Acquisition'::character varying(25) as mfu,
										parcelles_cen.par_id as par_id, lots_cen.lot_id as lot_id,
										lots_cen.dcntlo as surf_acq, 
										0::bigint as surf_conv, 
										CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan  
									FROM cadastre.parcelles_cen
										JOIN cadastre.lots_cen USING (par_id)
										JOIN cadastre.cadastre_cen USING (lot_id)
										JOIN foncier.cadastre_site USING (cad_cen_id)
										JOIN sites.sites USING (site_id)
										JOIN foncier.r_cad_site_mf USING (cad_site_id)
										JOIN foncier.mf_acquisitions USING (mf_id)
										JOIN foncier.d_typmf USING (typmf_id)
										JOIN foncier.r_mfstatut USING (mf_id)
										JOIN foncier.d_statutmf USING (statutmf_id)
										JOIN (
											SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
											FROM foncier.r_mfstatut";
											If ($debut != '') {
												$rq_bilan_collectivites .= " WHERE r_mfstatut.date > '$debut'";
											}
											If ($fin != '' && $debut == '') {
												$rq_bilan_collectivites .= " WHERE r_mfstatut.date < '$fin'";
											} Else If ($fin != '' && $debut != '') {
												$rq_bilan_collectivites .= "AND r_mfstatut.date < '$fin'";
											}
											$rq_bilan_collectivites .= "
											GROUP BY r_mfstatut.mf_id
										) tmax ON (r_mfstatut.mf_id = tmax.mf_id AND r_mfstatut.date = tmax.statut_date)
										LEFT JOIN tprob USING (site_id)
									WHERE d_statutmf.statutmf_id = 1 ";
									If ($debut != '') {
										$rq_bilan_collectivites .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND (r_cad_site_mf.date_sortie > '$debut' OR r_cad_site_mf.date_sortie = 'N.D.') ";
									} Else {
										$rq_bilan_collectivites .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mf.date_sortie = 'N.D.'";
									}
									If ($fin != '') {
										$rq_bilan_collectivites .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mf.date_entree < '$fin' ";
									} 
									$rq_bilan_collectivites .= "
									UNION
									SELECT 
										DISTINCT tparc.codcom, sites.site_id, 
										'Convention'::character varying(25) as mfu, 
										tparc.par_id as par_id, lots_cen.lot_id as lot_id,
										0::bigint as surf_acq, 
										CASE WHEN r_cad_site_mu.surf_mu is not Null THEN r_cad_site_mu.surf_mu ELSE lots_cen.dcntlo END as surf_conv, 
										CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan  
									FROM 
										(SELECT DISTINCT par_id, codcom FROM cadastre.parcelles_cen) tparc
										JOIN cadastre.lots_cen USING (par_id)
										JOIN cadastre.cadastre_cen USING (lot_id)
										JOIN foncier.cadastre_site USING (cad_cen_id)
										JOIN sites.sites USING (site_id)
										JOIN foncier.r_cad_site_mu USING (cad_site_id)
										JOIN foncier.mu_conventions USING (mu_id)
										JOIN foncier.d_typmu USING (typmu_id)
										JOIN foncier.r_mustatut USING (mu_id)
										JOIN foncier.d_statutmu USING (statutmu_id)
										JOIN (
											SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
											FROM foncier.r_mustatut";
											If ($debut != '') {
												$rq_bilan_collectivites .= " WHERE r_mustatut.date > '$debut'";
											}
											If ($fin != '' && $debut == '') {
												$rq_bilan_collectivites .= " WHERE r_mustatut.date < '$fin'";
											} Else If ($fin != '' && $debut != '') {
												$rq_bilan_collectivites .= "AND r_mustatut.date < '$fin'";
											}
											$rq_bilan_collectivites .= "
											GROUP BY r_mustatut.mu_id
										) tmax ON (r_mustatut.mu_id = tmax.mu_id AND r_mustatut.date = tmax.statut_date)
										LEFT JOIN tprob USING (site_id)
									WHERE d_statutmu.statutmu_id = 1 ";
									If ($debut != '') {
										$rq_bilan_collectivites .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND (r_cad_site_mu.date_sortie > '$debut' OR r_cad_site_mu.date_sortie = 'N.D.') AND mu_conventions.date_effet_conv > '$debut'";
									} Else {
										$rq_bilan_collectivites .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mu.date_sortie = 'N.D.'";
									}
									If ($fin != '') {
										$rq_bilan_collectivites .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mu.date_entree < '$fin' AND  mu_conventions.date_effet_conv < '$fin'";
									} Else {
										$rq_bilan_collectivites .= " AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text";
									}
									$rq_bilan_collectivites .= " 
									AND d_typmu.mfu = true
								) tmfu USING (site_id)
								JOIN cadastre.parcelles_cen t1 USING (par_id) 
								JOIN administratif.communes ON (t1.codcom = communes.code_insee AND communes.code_insee = tmfu.codcom) 
								JOIN territoires.r_ter_com USING (code_insee)
								JOIN (
									SELECT r_ter_com.territoire_id, max(r_ter_com.date_maj::text) AS max_date_maj, r_ter_com.code_insee
									FROM territoires.r_ter_com
									GROUP BY r_ter_com.territoire_id, r_ter_com.code_insee
									) tmax ON (r_ter_com.date_maj::text = tmax.max_date_maj AND r_ter_com.territoire_id = tmax.territoire_id AND r_ter_com.code_insee::text = tmax.code_insee::text)
								JOIN territoires.territoires ON (r_ter_com.territoire_id = territoires.territoire_id) 
								JOIN territoires.d_typterritoire USING (typterritoire_id)
							WHERE d_typterritoire.collectivites = true AND r_ter_com.actif = true
						) tcomsit
					GROUP BY tcomsit.territoire_id, tcomsit.site_id
					) tcomsitag USING (territoire_id) 
				WHERE tcomsitag.surf_mfu > 0
				GROUP BY territoires.territoire_lib, territoires.territoire_id, territoires.typterritoire_id
				ORDER BY typterritoire_id DESC, territoire_lib;";
				// echo $rq_bilan_collectivites.'<br />';
				$res_bilan_collectivites = $bdd->prepare($rq_bilan_collectivites);
				
				$rq_bilan_territoires = "
				SELECT 
					territoires.territoire_lib, 
					territoires.territoire_id, 
					territoires.typterritoire_id,
					COALESCE(sum(tcomsitag.surf_acq), 0) AS surf_acq, 
					COALESCE(sum(tcomsitag.surf_conv), 0) AS surf_conv, 
					COALESCE(sum(tcomsitag.surf_acq + tcomsitag.surf_conv), 0) AS surf_mfu, 
					array_to_string(array_agg(DISTINCT tcomsitag.site_id ORDER BY tcomsitag.site_id), ',') as tbl_site_id, 
					array_to_string(array_agg(tcomsitag.surf_acq ORDER BY tcomsitag.site_id), ',') as tbl_surf_acq, 
					array_to_string(array_agg(tcomsitag.surf_conv ORDER BY tcomsitag.site_id), ',') as tbl_surf_conv, 
					array_to_string(array_agg(tcomsitag.surf_acq + tcomsitag.surf_conv ORDER BY tcomsitag.site_id), ',') as tbl_surf_mfu,
					sum(etat_bilan) as etat_bilan
				FROM 
					territoires.territoires 
					JOIN (
						SELECT 
							tcomsit.territoire_id, 
							tcomsit.site_id, 
							sum(tcomsit.surf_acq) AS surf_acq, 
							sum(tcomsit.surf_conv) AS surf_conv, 
							sum(tcomsit.surf_acq + tcomsit.surf_conv) AS surf_mfu, 
							sum(etat_foncier) as etat_bilan 
						FROM (
							SELECT  
								territoires.territoire_id, 
								sites.site_id, 
								COALESCE(tmfu.surf_acq, 0) AS surf_acq, 
								COALESCE(tmfu.surf_conv, 0) AS surf_conv, 
								COALESCE(tmfu.surf_acq, 0) + COALESCE(tmfu.surf_conv, 0) AS surf_mfu, 
								etat_bilan as etat_foncier
							FROM 
								sites.sites 
								LEFT JOIN (
									WITH tprob AS (
										SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata UNION SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen UNION SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc
									)
									SELECT DISTINCT 
										parcelles_cen.codcom, sites.site_id, 
										'Acquisition'::character varying(25) as mfu,
										parcelles_cen.par_id as par_id, lots_cen.lot_id as lot_id,
										lots_cen.dcntlo as surf_acq, 
										0::bigint as surf_conv, 
										CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan  
									FROM cadastre.parcelles_cen
										JOIN cadastre.lots_cen USING (par_id)
										JOIN cadastre.cadastre_cen USING (lot_id)
										JOIN foncier.cadastre_site USING (cad_cen_id)
										JOIN sites.sites USING (site_id)
										JOIN foncier.r_cad_site_mf USING (cad_site_id)
										JOIN foncier.mf_acquisitions USING (mf_id)
										JOIN foncier.d_typmf USING (typmf_id)
										JOIN foncier.r_mfstatut USING (mf_id)
										JOIN foncier.d_statutmf USING (statutmf_id)
										JOIN (
											SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
											FROM foncier.r_mfstatut";
											If ($debut != '') {
												$rq_bilan_territoires .= " WHERE r_mfstatut.date > '$debut'";
											}
											If ($fin != '' && $debut == '') {
												$rq_bilan_territoires .= " WHERE r_mfstatut.date < '$fin'";
											} Else If ($fin != '' && $debut != '') {
												$rq_bilan_territoires .= "AND r_mfstatut.date < '$fin'";
											}
											$rq_bilan_territoires .= "
											GROUP BY r_mfstatut.mf_id
										) tmax ON (r_mfstatut.mf_id = tmax.mf_id AND r_mfstatut.date = tmax.statut_date)
										LEFT JOIN tprob USING (site_id)
									WHERE d_statutmf.statutmf_id = 1 ";
									If ($debut != '') {
										$rq_bilan_territoires .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND r_cad_site_mf.date_sortie > '$debut' ";
									} Else {
										$rq_bilan_territoires .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mf.date_sortie = 'N.D.'";
									}
									If ($fin != '') {
										$rq_bilan_territoires .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mf.date_entree < '$fin' ";
									} 
									$rq_bilan_territoires .= "
									UNION
									SELECT 
										DISTINCT tparc.codcom, sites.site_id, 
										'Convention'::character varying(25) as mfu, 
										tparc.par_id as par_id, lots_cen.lot_id as lot_id,
										0::bigint as surf_acq, 
										CASE WHEN r_cad_site_mu.surf_mu is not Null THEN r_cad_site_mu.surf_mu ELSE lots_cen.dcntlo END as surf_conv, 
										CASE WHEN tprob.site_id IS NOT NULL THEN 1 ELSE 0 END as etat_bilan  
									FROM 
										(SELECT DISTINCT par_id, codcom FROM cadastre.parcelles_cen) tparc
										JOIN cadastre.lots_cen USING (par_id)
										JOIN cadastre.cadastre_cen USING (lot_id)
										JOIN foncier.cadastre_site USING (cad_cen_id)
										JOIN sites.sites USING (site_id)
										JOIN foncier.r_cad_site_mu USING (cad_site_id)
										JOIN foncier.mu_conventions USING (mu_id)
										JOIN foncier.d_typmu USING (typmu_id)
										JOIN foncier.r_mustatut USING (mu_id)
										JOIN foncier.d_statutmu USING (statutmu_id)
										JOIN (
											SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
											FROM foncier.r_mustatut";
											If ($debut != '') {
												$rq_bilan_territoires .= " WHERE r_mustatut.date > '$debut'";
											}
											If ($fin != '' && $debut == '') {
												$rq_bilan_territoires .= " WHERE r_mustatut.date < '$fin'";
											} Else If ($fin != '' && $debut != '') {
												$rq_bilan_territoires .= "AND r_mustatut.date < '$fin'";
											}
											$rq_bilan_territoires .= "
											GROUP BY r_mustatut.mu_id
										) tmax ON (r_mustatut.mu_id = tmax.mu_id AND r_mustatut.date = tmax.statut_date)
										LEFT JOIN tprob USING (site_id)
									WHERE d_statutmu.statutmu_id = 1 ";
									If ($debut != '') {
										$rq_bilan_territoires .= " AND (cadastre_cen.date_fin > '$debut' OR cadastre_cen.date_fin = 'N.D.') AND (cadastre_site.date_fin > '$debut' OR cadastre_site.date_fin = 'N.D.') AND r_cad_site_mu.date_sortie > '$debut' AND mu_conventions.date_effet_conv > '$debut'";
									} Else {
										$rq_bilan_territoires .= " AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.' AND r_cad_site_mu.date_sortie = 'N.D.'";
									}
									If ($fin != '') {
										$rq_bilan_territoires .= " AND cadastre_cen.date_deb < '$fin' AND cadastre_site.date_deb < '$fin' AND r_cad_site_mu.date_entree < '$fin' AND  mu_conventions.date_effet_conv < '$fin'";
									} Else {
										$rq_bilan_territoires .= " AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text";
									}
									$rq_bilan_territoires .= " 
									AND d_typmu.mfu = true
								) tmfu USING (site_id)
								JOIN cadastre.parcelles_cen t1 USING (par_id) 
								JOIN administratif.communes ON (t1.codcom = communes.code_insee AND communes.code_insee = tmfu.codcom) 
								JOIN territoires.r_ter_com USING (code_insee) 
								JOIN territoires.territoires USING (territoire_id) 
								JOIN territoires.d_typterritoire USING (typterritoire_id)
							WHERE d_typterritoire.collectivites = false AND r_ter_com.actif = true
						) tcomsit
					GROUP BY tcomsit.territoire_id, tcomsit.site_id
					) tcomsitag USING (territoire_id) 
				WHERE tcomsitag.surf_mfu > 0
				GROUP BY territoires.territoire_lib, territoires.territoire_id, territoires.typterritoire_id
				ORDER BY typterritoire_id DESC, territoire_lib;";
				// echo $rq_bilan_territoires;
				$res_bilan_territoires = $bdd->prepare($rq_bilan_territoires);
				
			//Timer
			$timefin = time();
				
		?>
		<section id='main'>
			<h3>Bilan complet</h3>
			<div class='lib_alerte' id='prob_saisie' style='position:relative;top:83px;margin-top:25px;text-align:center;width:100%;display:none'>Veuillez compl&eacuteter votre saisie</div>
			<div id='div_flt_date' style='position:relative;top:85px;width:100%;text-align:center;'>
				<form onsubmit="return valide_form('frm_flt_date')" id='frm_flt_date' name='frm_flt_date' class='form' action='<?php If (isset($_POST['hid_frm_url'])) {echo $_POST['hid_frm_url']; } Else {echo $_SERVER['PHP_SELF'].'#site';} ?>' method='POST'>
					<input type='hidden' id='hid_frm_url' name='hid_frm_url' value='<?php If (isset($_POST['hid_frm_url'])) {echo $_POST['hid_frm_url']; } Else {echo $_SERVER['PHP_SELF'].'#site';} ?>'>
					
						<label class='label'>
							<u>Filtrer par date</u> : 
						</label>
						<label class='label' style='margin-left:30px;'>
							Début
						</label>
						<label class="btn_combobox">				
							<select id="flt_mois_debut" name="flt_mois_debut" class="combobox" style='text-align:center;<?php If(isset($_POST['need_flt_annee_debut']) && $_POST['need_flt_annee_debut'] != '-1') {echo 'border-color:#ff8901;';}?>'>
								<option value='-1'>Mois</option>
								<?php For ($i=0; $i<count($lst_mois); $i++) {
									echo "<option ";
									If ((isset($_POST['flt_mois_debut']) && $_POST['flt_mois_debut'] == $lst_mois_num[$i]) || (isset($_POST['need_flt_annee_debut']) && $_POST['flt_mois_debut'] == '-1' && $_POST['need_flt_annee_debut'] != '-1' && $i == 0)) {
										echo 'selected ';
									} 
									echo "value='$lst_mois_num[$i]'>$lst_mois[$i]</option>";
								}
								?>						
							</select>
						</label>
						<label class="btn_combobox">			
							<select id="need_flt_annee_debut" name="need_flt_annee_debut" class="combobox" style='text-align:center;<?php If(isset($_POST['need_flt_annee_debut']) && $_POST['need_flt_annee_debut'] != '-1') {echo 'border-color:#ff8901;';}?>'>
								<option value='-1'>Année</option>
								<?php For ($i=date('Y'); $i>=1991; $i--) {
									echo "<option ";
									If (isset($_POST['need_flt_annee_debut']) && $_POST['need_flt_annee_debut'] == $i) {
										echo 'selected ';
									} 
									echo "value='$i'>$i</option>";
								}
								?>	
							</select>
						</label>
						<label class='label' style='margin-left:30px;'>
							Fin
						</label>
						<label class="btn_combobox">				
							<select id="flt_mois_fin" name="flt_mois_fin" class="combobox" style='text-align:center;<?php If(isset($_POST['need_flt_annee_fin']) && $_POST['need_flt_annee_fin'] != '-1') {echo 'border-color:#ff8901;';}?>'>
								<option value='-1'>Mois</option>
								<?php For ($i=0; $i<count($lst_mois); $i++) {
									echo "<option ";
									If ((isset($_POST['flt_mois_fin']) && $_POST['flt_mois_fin'] == $lst_mois_num[$i]) || (isset($_POST['need_flt_annee_fin']) && $_POST['flt_mois_fin'] == '-1' && $_POST['need_flt_annee_fin'] != '-1' && $i == count($lst_mois)-1)) {
										echo 'selected ';
									} 
									echo "value='$lst_mois_num[$i]'>$lst_mois[$i]</option>";
								}
								?>						
							</select>
						</label>
						<label class="btn_combobox">				
							<select id="need_flt_annee_fin" name="need_flt_annee_fin" class="combobox" style='text-align:center;<?php If(isset($_POST['need_flt_annee_fin']) && $_POST['need_flt_annee_fin'] != '-1') {echo 'border-color:#ff8901;';}?>'>
								<option value='-1'>Année</option>
								<?php For ($i=date('Y'); $i>=1991; $i--) {
									echo "<option ";
									If (isset($_POST['need_flt_annee_fin']) && $_POST['need_flt_annee_fin'] == $i) {
										echo 'selected ';
									} 
									echo "value='$i'>$i</option>";
								}
								?>	
							</select>
						</label>
						<input type='submit' value='' style='border:0;width:25px;height:25px;position:relative;cursor:pointer;margin:0 10px;background:url(<?php echo ROOT_PATH; ?>images/filtre.png) no-repeat'>
						<img onclick="
						document.getElementById('flt_mois_debut').value='-1';
						document.getElementById('need_flt_annee_debut').value='-1';
						document.getElementById('flt_mois_fin').value='-1';
						document.getElementById('need_flt_annee_fin').value='-1';
						document.forms['frm_flt_date'].submit();;
						" src='<?php echo ROOT_PATH; ?>images/supprimer.png' style='position:relative;top:5px;cursor:pointer;'>
						<input type='hidden' name='flt_date_export_filename' id='flt_date_export_filename' value='<?php if(isset($_POST['flt_date_export_filename'])) {echo $_POST['flt_date_export_filename'];} Else { echo 'foncier_export.php?filename=bilan_site.csv';} ?>'>
				</form>
					<script type="text/javascript">
						function adapteExportFilename() {
							var new_action = 'foncier_export.php?filename=bilan_';
							var cur_onglet = 'site';
							$('.tabs-container:not(".tabs-hide")').each(function(){
								new_action += $(this).attr("id");
								cur_onglet = $(this).attr("id");
							});
							
							if ($('#need_flt_annee_debut').val() != '-1' && $('#need_flt_annee_fin').val() != '-1') {
								if ($('#flt_mois_debut').val() == '-1') {
									var mois_debut = '01';
								} else {
									mois_debut = $('#flt_mois_debut').val()
								}
								if ($('#flt_mois_fin').val() == '-1') {
									var mois_fin = '12';
								} else {
									mois_fin = $('#flt_mois_fin').val()
								}
								if ((($('#flt_mois_debut').val() == '-1' && $('#flt_mois_fin').val() == '-1') || ($('#flt_mois_debut').val() == '01' && $('#flt_mois_fin').val() == '12')) && $('#need_flt_annee_debut').val() == $('#need_flt_annee_fin').val()) {
									new_action += '_' + $('#need_flt_annee_debut').val()
								} else if ($('#flt_mois_debut').val() == $('#flt_mois_fin').val() && $('#need_flt_annee_debut').val() == $('#need_flt_annee_fin').val()) {
									new_action += '_' + mois_debut + '-' + $('#need_flt_annee_debut').val();
								} else {
									new_action += '_' + mois_debut + '-' + $('#need_flt_annee_debut').val() + '-' + mois_fin + '-' + $('#need_flt_annee_fin').val();
								}
							}
							new_action += '.csv';
							$('#frm_export_bilan').attr('action', new_action)
							
							exportTable('frm_export_bilan', 'tbl_'+cur_onglet+'_entete_fixed', 'tbl_'+cur_onglet+'_corps');
						}
					</script>
				<form id="frm_export_bilan" name="frm_export_bilan" onsubmit="return false" method='POST'>
					<label class="label" style="margin-bottom:15px;font-size:8pt">Export :
						<img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px' style="cursor:pointer;" onclick="adapteExportFilename();">
						<input type="hidden" id="tbl_post" name="tbl_post" value="">
					</label>
				</form>
			</div>	
			
			<div class='list'>
				<ul>
					<li>
						<a href="#site" onclick="$('#frm_flt_date').attr('action', '<?php echo $_SERVER['PHP_SELF']; ?>#site');"><span>Par sites</span></a>
					</li>
					<li>
						<a href="#metasite" onclick="$('#frm_flt_date').attr('action', '<?php echo $_SERVER['PHP_SELF']; ?>#metasite');"><span>Par métasites</span></a>
					</li>
					<li>
						<a href="#comm" onclick="$('#frm_flt_date').attr('action', '<?php echo $_SERVER['PHP_SELF']; ?>#comm');"><span>Par communes</span></a>
					</li>
					<li>
						<a href="#coll" onclick="$('#frm_flt_date').attr('action', '<?php echo $_SERVER['PHP_SELF']; ?>#coll');"><span>Par collectivités</span></a>
					</li>
					<li>
						<a href="#terr" onclick="$('#frm_flt_date').attr('action', '<?php echo $_SERVER['PHP_SELF']; ?>#terr');"><span>Par territoires</span></a>
					</li>
				</ul>
				
				<div id='site'>
					<table id='tbl_bilan_site' width="100%" cellspacing="10" cellpadding="10" style="border:0px solid black;padding-top:45px;">
						<tbody>
							<tr>
								<td>
									<center>
									<table id='tbl_site' align='CENTER' class='no-border' width='100%' CELLSPACING='0' CELLPADDING='4' border='0'>
										<tbody id='tbl_site_entete'>
											<form id="frm_site_entete" onsubmit="return false">
												<tr>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 0, 'txt', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 0, 'txt', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<input style='text-align: center;' class='editbox' oninput="maj_table('frm_site_entete', 'tbl_site_corps', 0);" type='text' id='filtre_site_id' placeholder='SITE ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SITE ID';" size='6'> 
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 1, 'txt', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 1, 'txt', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;'>
														<input style='text-align: center; width:95%' class='editbox' oninput="maj_table('frm_site_entete', 'tbl_site_corps', 0);" type='text' id='filtre_nom' placeholder='Nom du site' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du site';"> 
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 2, 'txt', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 2, 'txt', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<label class="btn_combobox">			
															<select onchange="maj_table('frm_site_entete', 'tbl_site_corps', 0);" id="filtre_type" class="combobox" style='text-align:center'>
																<option value='-1'>Type de site</option>
																<?php
																$tab_site_type_unique = array_values(array_unique(array_column($lst_site, 'type_site')));
																sort($tab_site_type_unique);
																For ($j=0; $j<count($tab_site_type_unique); $j++)
																	{echo "<option value='" . $j . "'>" . $tab_site_type_unique[$j] . "</option>";}
																?>				
															</select>
														</label>
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 3, 'num', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 3, 'num', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 5, 'num', 'desc');">
																	<input style='margin-left:5px;background-color:transparent;border:0px;padding-bottom:0' class='editbox' type='text' id='surf_acq' value='Surf. acquise' size='12' DISABLED> 
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 5, 'num', 'asc');">
																	<input style='margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align: left;font-size: 8pt;' class='editbox' type='text' id='surf_total' value="(ha.a.ca)" size='12' DISABLED> 
																</td>
															</tr>
														</table>
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 4, 'num', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 4, 'num', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 5, 'num', 'desc');">
																	<input style='margin-left:2px;background-color:transparent; border:0px;padding-bottom:0' class='editbox' type='text' id='surf_conv' value='Surf. conv.' size='12' DISABLED> 
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 5, 'num', 'asc');">
																	<input style='margin-left:10px;background-color:transparent; border:0px;padding-top:0;text-align: left;font-size: 8pt;' class='editbox' type='text' id='surf_total' value="(ha.a.ca)" size='12' DISABLED> 
																</td>
															</tr>
														</table>
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 5, 'num', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_site', 'tbl_site_corps', 5, 'num', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant'>
																	<input style='margin-left:2px;background-color:transparent; border:0px;padding-bottom:0;text-align: left;' class='editbox' type='text' id='surf_total' value="Surf. MFU" size='12' DISABLED> 
																</td>
															</tr>
															<tr>
																<td class='tri_croissant'>
																	<input style='margin-left:10px;background-color:transparent; border:0px;padding-top:0;text-align: left;font-size: 8pt;' class='editbox' type='text' id='surf_total' value="(ha.a.ca)" size='12' DISABLED> 
																</td>
															</tr>
														</table>
													</td>
													<td>
														<img onclick="
														document.getElementById('filtre_site_id').value='';
														document.getElementById('filtre_nom').value='';
														document.getElementById('filtre_type').value='-1';
														maj_table('frm_site_entete', 'tbl_site_corps', 3);
														" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
													</td>
												</tr>
											</form>
										</tbody>
										<thead id="tbl_site_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
											<tr>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='text-align:center;margin-left:0px;'>SITE ID</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='text-align:center;margin-left:0px;'>Nom du site</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Type de site</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Surf. acquise</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Surf. conv.</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Surf. MFU</label>
												</th>												
											</tr>
										</thead>
										<tbody id='tbl_site_corps'>
										<?php
											For ($i=0; $i<$res_lst_site->rowCount(); $i++) {
												If($i%2)
													{$tr=1;}
												Else
													{$tr=0;}
										?>
											<tr class='bilan_site_tr l<?php echo $tr; ?>' style='cursor:default'>
												<td colspan='2'>
													<center><a><?php echo $lst_site[$i]['site_id']; ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo $lst_site[$i]['site_nom']; ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo $lst_site[$i]['type_site']; ?></a></center>
												</td>
											<?php 
												If ($lst_site[$i]['etat_bilan'] > 0) {
											?>
												<td colspan='6'>
													<center><a style='color:#fe0000;font-weight:bold;'>Ces données ne sont pas à jour</a></center>
												</td>	
											<?php
												} Else {
											?>
												<td colspan='2'>
													<center><a><?php echo conv_are($lst_site[$i]['surf_acq']); ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo conv_are($lst_site[$i]['surf_conv']); ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo conv_are($lst_site[$i]['surf_mfu']); ?></a></center>
												</td>
										<?php
												}
											}
										?>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					</center>
				</div>
				<div id='metasite'>
					<table id='tbl_bilan_metasite' width="100%" cellspacing="10" cellpadding="10" style="border:0px solid black;padding-top:45px;">
						<tbody>
							<tr>
								<td>
									<center>
									<table id='tbl_metasite' align='CENTER' class='no-border' width='100%' CELLSPACING='0' CELLPADDING='4' border='0'>
										<tbody id='tbl_metasite_entete'>
											<form id="frm_metasite_entete" onsubmit="return false">
												<tr>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 0, 'txt', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 0, 'txt', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<input style='text-align: center;' class='editbox' oninput="maj_table('frm_metasite_entete', 'tbl_metasite_corps', 0);" type='text' id='filtre_metasite_id' placeholder='META' onfocus="this.placeholder = '';" onblur="this.placeholder = 'META';" size='6'> 
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 1, 'txt', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 1, 'txt', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;'>
														<input style='text-align: center; width:95%' class='editbox' oninput="maj_table('frm_metasite_entete', 'tbl_metasite_corps', 0);" type='text' id='filtre_nom' placeholder='Nom du métasite' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du métasite';"> 
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 2, 'txt', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 2, 'txt', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<label class="btn_combobox">			
															<select onchange="maj_table('frm_metasite_entete', 'tbl_metasite_corps', 0);" id="filtre_type" class="combobox" style='text-align:center'>
																<option value='-1'>Type de métasite</option>
																<?php
																$tab_metasite_type_unique = array_values(array_unique(array_column($lst_metasite, 'type_metasite')));
																sort($tab_metasite_type_unique);
																For ($j=0; $j<count($tab_metasite_type_unique); $j++)
																	{echo "<option value='" . $j . "'>" . $tab_metasite_type_unique[$j] . "</option>";}
																?>				
															</select>
														</label>
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 3, 'num', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 3, 'num', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 5, 'num', 'desc');">
																	<input style='margin-left:5px;background-color:transparent;border:0px;padding-bottom:0' class='editbox' type='text' id='surf_acq' value='Surf. acquise' size='12' DISABLED> 
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 5, 'num', 'asc');">
																	<input style='margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align: left;font-size: 8pt;' class='editbox' type='text' id='surf_total' value="(ha.a.ca)" size='12' DISABLED> 
																</td>
															</tr>
														</table>
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 4, 'num', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 4, 'num', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 5, 'num', 'desc');">
																	<input style='margin-left:2px;background-color:transparent; border:0px;padding-bottom:0' class='editbox' type='text' id='surf_conv' value='Surf. conv.' size='12' DISABLED> 
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 5, 'num', 'asc');">
																	<input style='margin-left:10px;background-color:transparent; border:0px;padding-top:0;text-align: left;font-size: 8pt;' class='editbox' type='text' id='surf_total' value="(ha.a.ca)" size='12' DISABLED> 
																</td>
															</tr>
														</table>
													</td>
													<td class='ordre' align='right' width='15px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 5, 'num', 'desc');">
																	<span><</span>
																</td>
															</tr>
															<tr>
																<td class='tri_croissant' onclick="sortTable('tbl_metasite', 'tbl_metasite_corps', 5, 'num', 'asc');">
																	<span>></span>
																</td>
															</tr>
														</table>
													</td>
													<td class='filtre' style='text-align: left;' width='1px'>
														<table CELLSPACING='0' CELLPADDING='0' border='0' class='no-border'>
															<tr>
																<td class='tri_croissant'>
																	<input style='margin-left:2px;background-color:transparent; border:0px;padding-bottom:0;text-align: left;' class='editbox' type='text' id='surf_total' value="Surf. MFU" size='12' DISABLED> 
																</td>
															</tr>
															<tr>
																<td class='tri_croissant'>
																	<input style='margin-left:10px;background-color:transparent; border:0px;padding-top:0;text-align: left;font-size: 8pt;' class='editbox' type='text' id='surf_total' value="(ha.a.ca)" size='12' DISABLED> 
																</td>
															</tr>
														</table>
													</td>
													<td>
														<img onclick="
														document.getElementById('filtre_metasite_id').value='';
														document.getElementById('filtre_nom').value='';
														document.getElementById('filtre_type').value='-1';
														maj_table('frm_metasite_entete', 'tbl_metasite_corps', 3);
														" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
													</td>
												</tr>
											</form>
										</tbody>
										<thead id="tbl_metasite_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
											<tr>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='text-align:center;margin-left:0px;'>META</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='text-align:center;margin-left:0px;'>Nom du métasite</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Type de métasite</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Surf. acquise</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Surf. conv.</label>
												</th>
												<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:center;margin-left:0px;'>Surf. MFU</label>
												</th>												
											</tr>
										</thead>
										<tbody id='tbl_metasite_corps'>
										<?php
											For ($i=0; $i<$res_lst_metasite->rowCount(); $i++) {
												If($i%2)
													{$tr=1;}
												Else
													{$tr=0;}
										?>
											<tr class='bilan_site_tr l<?php echo $tr; ?>' style='cursor:default'>
												<td colspan='2'>
													<center><a><?php echo $lst_metasite[$i]['metasite_id']; ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo $lst_metasite[$i]['metasite_nom']; ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo $lst_metasite[$i]['type_metasite']; ?></a></center>
												</td>
											<?php 
												If ($lst_metasite[$i]['etat_bilan'] > 0) {
											?>
												<td colspan='6'>
													<center><a style='color:#fe0000;font-weight:bold;'>Ces données ne sont pas à jour</a></center>
												</td>	
											<?php
												} Else {
											?>
												<td colspan='2'>
													<center><a><?php echo conv_are($lst_metasite[$i]['surf_acq']); ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo conv_are($lst_metasite[$i]['surf_conv']); ?></a></center>
												</td>
												<td colspan='2'>
													<center><a><?php echo conv_are($lst_metasite[$i]['surf_mfu']); ?></a></center>
												</td>
										<?php
												}
											}
										?>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					</center>
				</div>
				<div id='comm'>
					<table width="100%" cellspacing="10" cellpadding="10" style="border:0px solid black;padding-top:45px;">
						<tbody>
							<tr>
								<td align='center'>
									<table width="100%" id='tbl_comm' align='CENTER' class='no-border' CELLSPACING='0' CELLPADDING='4' border='0'>
										<tbody id='tbl_comm_entete'>
											<form id="frm_comm_entete" onsubmit="return false">
												<tr>
													<td colspan='6'>
														<div class='alphabet' id="alphabet">		
															<?php For ($i=0; $i<count($alphabet); $i++) { ?>
															<div>
																<span id='lettre_<?php echo $alphabet[$i]; ?>' onclick="scrollToLetter('tbl_comm_corps', 1, 'tr_comm_', 0, '<?php echo $alphabet[$i]; ?>'); change_style_lettre('<?php echo $alphabet[$i]; ?>')"><?php echo $alphabet[$i]; ?></span>
															</div>
															<?php } ?>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														&nbsp;
													</td>
													<td class='filtre' style='text-align: right;'>
														<input style='text-align:right; background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='comm' value='Commune' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='surf_acq' value='Surface acquise' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='surf_conv' value='Surface conventionnée' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;margin-left:5px;background-color:transparent; padding-bottom:0;border:0px;margin-right:20px;font-weight:bold;' class='editbox' type='text' id='surf_total' value='Surface totale (MFU)' DISABLED> 
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														&nbsp;
													</td>
													<td>
														&nbsp;
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											</form>
										</tbody>
										<thead id="tbl_comm_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
											<tr>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='text-align:right;margin-right: 20px;'>&nbsp;</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='text-align:right;margin-right: 20px;'>Commune</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:right;margin-right: 20px;'>Surface acquise</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:right;margin-right: 20px;'>Surface conventionnée</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:right;margin-right: 20px;'>Surface totale (MFU)</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='text-align:right;margin-right: 20px;'>&nbsp;</label>
												</th>
											</tr>
										</thead>
										<tbody id='tbl_comm_corps'>
									<?php
										$res_bilan_commune->execute();
										$bilan_commune = $res_bilan_commune->fetchAll();
										For ($i=0; $i<$res_bilan_commune->rowCount(); $i++) {
											If($i%2)
												{$tr=1;}
											Else
												{$tr=0;}
											$tbl_site_id = preg_split("/,/", $bilan_commune[$i]['tbl_site_id']);
											$tbl_surf_acq = preg_split("/,/", $bilan_commune[$i]['tbl_surf_acq']);
											$tbl_surf_conv = preg_split("/,/", $bilan_commune[$i]['tbl_surf_conv']);
											$tbl_surf_mfu = preg_split("/,/", $bilan_commune[$i]['tbl_surf_mfu']);
									?>			
											<tr id='tr_comm_<?php echo $bilan_commune[$i]['code_insee']; ?>' class='<?php If ($bilan_commune[$i]['etat_bilan'] == 0) { ?>toggle_tr <?php } Else { ?>bilan_site_tr <?php } ?> l<?php echo $tr; ?>' <?php If ($bilan_commune[$i]['etat_bilan'] == 0) { ?>onclick="slideBilanCommDetail('comm', '<?php echo $bilan_commune[$i]['code_insee']; ?>', <?php echo count($tbl_site_id); ?> );"<?php } Else { ?> style='cursor:default' <?php } ?>>
												<td>
													&nbsp;
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo $bilan_commune[$i]['nom']; ?></a>
												</td>
									<?php
											If ($bilan_commune[$i]['etat_bilan'] == 0) {	
									?>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_commune[$i]['surf_acq']); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_commune[$i]['surf_conv']); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_commune[$i]['surf_mfu']); ?></a>
												</td>
									<?php 
											} Else {
									?>
												<td style='text-align:right;width:100px;'>
													&nbsp;
												</td>
												<td style='text-align:right;width:100px;'>
													<center><a style='color:#fe0000;font-weight:bold;'>Ces données ne sont pas à jour</a></center>
												</td>
												<td style='text-align:right;width:100px;'>
													&nbsp;
												</td>
									<?php 
											}
									?>
												<td>
													&nbsp;
												</td>
											</tr>
									<?php													
											If (count($tbl_site_id) > 0 && $tbl_site_id[0] != '' && $bilan_commune[$i]['etat_bilan'] == 0) {
												For ($j=0; $j<count($tbl_site_id); $j++) {
													If ($j<(count($tbl_site_id)-1))	{
														$border_radius_left = 'border-radius:0;';
														$border_radius_right = 'border-radius:0;';
													} Else If ($j==(count($tbl_site_id)-1))	{
														$border_radius_left = 'border-bottom-left-radius:20px;';
														$border_radius_right = 'border-bottom-right-radius:10px;';
													}
									?>
														
											<tr id='tr_site_<?php echo $bilan_commune[$i]['code_insee']. "_" . $j; ?>' class='tr_site_detail l<?php echo $tr; ?> no-export' style='cursor:default;display:none;'>
												<td style='<?php echo $border_radius_left; ?>'>
													&nbsp;
												</td>
												<td style='text-align:right;'>
													<a style='margin-right:0px;'><?php echo $tbl_site_id[$j]; ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_acq[$j]); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_conv[$j]); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_mfu[$j]); ?></a>
												</td>
												<td style='<?php echo $border_radius_right; ?>'>
													&nbsp;
												</td>
											</tr>
									<?php	
												}
											} Else {
									?>		
												<tr id='tr_site_<?php echo $bilan_commune[$i]['code_insee'] . "_0"; ?>' class='tr_site_detail l<?php echo $tr; ?>' style='cursor:default;display:none;'>
													<td colspan='6' style='text-align:center;'>
														<a style='margin-right:0px;'>Aucun détail disponible</a>
													</td>
												</tr>
									<?php
											}	
										}
									?>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<div id="avertissement_comm" class="popup_avertissement" style="display: none;">
						<label>Les surfaces maitrisées détaillées par site sont relatives à la commune concernée<br>Il ne s'agit donc pas obligatoirement des surfaces totales maitrisées sur le site</label>
					</div>
				</div>
				<div id='coll'>
					<table width="100%" cellspacing="10" cellpadding="10" style="border:0px solid black;padding-top:45px;">
						<tbody>
							<tr>
								<td align='center'>
									<table width="100%" id='tbl_coll' align='CENTER' class='no-border' CELLSPACING='0' CELLPADDING='4' border='0'>
										<tbody id='tbl_coll_entete'>
											<form id="frm_coll_entete" onsubmit="return false">
												<tr>
													<td colspan='6'>
														<div class='alphabet' id="alphabet">		
															<?php For ($i=0; $i<count($alphabet); $i++) { ?>
															<div>
																<span id='lettre_<?php echo $alphabet[$i]; ?>' onclick="scrollToLetter('tbl_coll_corps', 1, 'tr_coll_', 0, '<?php echo $alphabet[$i]; ?>'); change_style_lettre('<?php echo $alphabet[$i]; ?>')"><?php echo $alphabet[$i]; ?></span>
															</div>
															<?php } ?>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														&nbsp;
													</td>
													<td class='filtre' style='text-align: right;'>
														<input style='text-align:right; background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='comm' value='Collectivité' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='surf_acq' value='Surface acquise' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='surf_conv' value='Surface conventionnée' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;margin-left:5px;background-color:transparent; padding-bottom:0;border:0px;margin-right:20px;font-weight:bold;' class='editbox' type='text' id='surf_total' value='Surface totale (MFU)' DISABLED> 
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														&nbsp;
													</td>
													<td>
														&nbsp;
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											</form>
										</tbody>
										<thead id="tbl_coll_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
											<tr>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='margin-right: 20px;'>&nbsp;</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='margin-right: 20px;'>Collectivité</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>Surface acquise</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>Surface conventionnée</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>Surface totale (MFU)</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>&nbsp;</label>
												</th>
											</tr>
										</thead>	
										<tbody id='tbl_coll_corps'>
									<?php
										$res_bilan_collectivites->execute();
										$bilan_collectivites = $res_bilan_collectivites->fetchAll();
										For ($i=0; $i<$res_bilan_collectivites->rowCount(); $i++) {
											If($i%2)
												{$tr=1;}
											Else
												{$tr=0;}
											$tbl_site_id = preg_split("/,/", $bilan_collectivites[$i]['tbl_site_id']);
											$tbl_surf_acq = preg_split("/,/", $bilan_collectivites[$i]['tbl_surf_acq']);
											$tbl_surf_conv = preg_split("/,/", $bilan_collectivites[$i]['tbl_surf_conv']);
											$tbl_surf_mfu = preg_split("/,/", $bilan_collectivites[$i]['tbl_surf_mfu']);
									?>			
											<tr id='tr_coll_<?php echo $bilan_collectivites[$i]['territoire_id']; ?>' class='<?php If ($bilan_collectivites[$i]['etat_bilan'] == 0) { ?>toggle_tr <?php } Else { ?>bilan_site_tr <?php } ?> l<?php echo $tr; ?>' <?php If ($bilan_collectivites[$i]['etat_bilan'] == 0) { ?>onclick="slideBilanCommDetail('coll', '<?php echo $bilan_collectivites[$i]['territoire_id']; ?>', <?php echo count($tbl_site_id); ?> );"<?php } Else { ?> style='cursor:default' <?php } ?>>
												<td>
													&nbsp;
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo str_replace(array("Communauté de communes", "Communauté d'Agglomération", "Communauté Urbaine"), array("Com Com", "Com d'Agglo", "Com Urb"), $bilan_collectivites[$i]['territoire_lib']); ?></a>
												</td>
									<?php
											If ($bilan_collectivites[$i]['etat_bilan'] == 0) {	
									?>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_collectivites[$i]['surf_acq']); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_collectivites[$i]['surf_conv']); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_collectivites[$i]['surf_mfu']); ?></a>
												</td>
									<?php 
											} Else {
									?>
												<td style='text-align:right;width:100px;'>
													&nbsp;
												</td>
												<td style='text-align:right;width:100px;'>
													<center><a style='color:#fe0000;font-weight:bold;'>Ces données ne sont pas à jour</a></center>
												</td>
												<td style='text-align:right;width:100px;'>
													&nbsp;
												</td>
									<?php 
											}
									?>
												<td>
													&nbsp;
												</td>
											</tr>
									<?php													
											If (count($tbl_site_id) > 0 && $tbl_site_id[0] != '' && $bilan_collectivites[$i]['etat_bilan'] == 0) {
												For ($j=0; $j<count($tbl_site_id); $j++) {
													If ($j<(count($tbl_site_id)-1))	{
														$border_radius_left = 'border-radius:0;';
														$border_radius_right = 'border-radius:0;';
													} Else If ($j==(count($tbl_site_id)-1))	{
														$border_radius_left = 'border-bottom-left-radius:20px;';
														$border_radius_right = 'border-bottom-right-radius:10px;';
													}
									?>
														
											<tr id='tr_site_<?php echo $bilan_collectivites[$i]['territoire_id']. "_" . $j; ?>' class='tr_site_detail l<?php echo $tr; ?> no-export' style='cursor:default;display:none;'>
												<td style='<?php echo $border_radius_left; ?>'>
													&nbsp;
												</td>
												<td style='text-align:right;'>
													<a style='margin-right:0px;'><?php echo $tbl_site_id[$j]; ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_acq[$j]); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_conv[$j]); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_mfu[$j]); ?></a>
												</td>
												<td style='<?php echo $border_radius_right; ?>'>
													&nbsp;
												</td>
											</tr>
									<?php	
												}
											} Else {
									?>		
												<tr id='tr_site_<?php echo $bilan_collectivites[$i]['territoire_id'] . "_0"; ?>' class='tr_site_detail l<?php echo $tr; ?>' style='cursor:default;display:none;'>
													<td colspan='6' style='text-align:center;'>
														<a style='margin-right:0px;'>Aucun détail disponible</a>
													</td>
												</tr>
									<?php
											}	
										}
									?>
										</tbody>
										
										
										
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<div id="avertissement_coll" class="popup_avertissement" style="display: none;">
						<label>Les surfaces maitrisées détaillées par site sont relatives à la collectivité concernée<br>Il ne s'agit donc pas obligatoirement des surfaces totales maitrisées sur le site</label>
					</div>
				</div>
				<div id='terr'>
					<table width="100%" cellspacing="10" cellpadding="10" style="border:0px solid black;padding-top:45px;">
						<tbody>
							<tr>
								<td align='center'>
									<table width="100%" id='tbl_terr' align='CENTER' class='no-border' CELLSPACING='0' CELLPADDING='4' border='0'>
										<tbody id='tbl_terr_entete'>
											<form id="frm_terr_entete" onsubmit="return false">
												<tr>
													<td colspan='6'>
														<div class='alphabet' id="alphabet">		
															<?php For ($i=0; $i<count($alphabet); $i++) { ?>
															<div>
																<span id='lettre_<?php echo $alphabet[$i]; ?>' onclick="scrollToLetter('tbl_terr_corps', 1, 'tr_terr_', 0, '<?php echo $alphabet[$i]; ?>'); change_style_lettre('<?php echo $alphabet[$i]; ?>')"><?php echo $alphabet[$i]; ?></span>
															</div>
															<?php } ?>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														&nbsp;
													</td>
													<td class='filtre' style='text-align: right;'>
														<input style='text-align:right; background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='terr' value='Territoire' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='surf_acq' value='Surface acquise' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;background-color:transparent; border:0px;margin-right:20px;font-weight:bold;padding-bottom:0;' class='editbox' type='text' id='surf_conv' value='Surface conventionnée' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style='text-align:right;margin-left:5px;background-color:transparent; padding-bottom:0;border:0px;margin-right:20px;font-weight:bold;' class='editbox' type='text' id='surf_total' value='Surface totale (MFU)' DISABLED> 
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
												<tr>
													<td>
														&nbsp;
													</td>
													<td>
														&nbsp;
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td class='filtre' style='text-align: right;' width='1px'>
														<input style="margin-left:5px;background-color:transparent; border:0px;padding-top:0;text-align:right;font-size: 8pt;margin-right:20px;" class='editbox' type='text' id='surf_total' value='(ha.a.ca)' DISABLED> 
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											</form>
										</tbody>
										<thead id="tbl_terr_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
											<tr>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='margin-right: 20px;'>&nbsp;</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>
													<label class='label' style='margin-right: 20px;'>Territoire</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>Surface acquise</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>Surface conventionnée</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>Surface totale (MFU)</label>
												</th>
												<th style='text-align:right;border:0px solid #accf8a;padding:6px 5px;'>	
													<label class='label' style='margin-right: 20px;'>&nbsp;</label>
												</th>
											</tr>
										</thead>
										<tbody id='tbl_terr_corps'>
									<?php
										$res_bilan_territoires->execute();
										$bilan_territoires = $res_bilan_territoires->fetchAll();
										For ($i=0; $i<$res_bilan_territoires->rowCount(); $i++) {
											If($i%2)
												{$tr=1;}
											Else
												{$tr=0;}
											$tbl_site_id = preg_split("/,/", $bilan_territoires[$i]['tbl_site_id']);
											$tbl_surf_acq = preg_split("/,/", $bilan_territoires[$i]['tbl_surf_acq']);
											$tbl_surf_conv = preg_split("/,/", $bilan_territoires[$i]['tbl_surf_conv']);
											$tbl_surf_mfu = preg_split("/,/", $bilan_territoires[$i]['tbl_surf_mfu']);
									?>			
											<tr id='tr_terr_<?php echo $bilan_territoires[$i]['territoire_id']; ?>' class='<?php If ($bilan_territoires[$i]['etat_bilan'] == 0) { ?>toggle_tr <?php } Else { ?>bilan_site_tr <?php } ?> l<?php echo $tr; ?>' <?php If ($bilan_territoires[$i]['etat_bilan'] == 0) { ?>onclick="slideBilanCommDetail('terr', '<?php echo $bilan_territoires[$i]['territoire_id']; ?>', <?php echo count($tbl_site_id); ?> );"<?php } Else { ?> style='cursor:default' <?php } ?>>
												<td>
													&nbsp;
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo str_replace(array("Communauté de communes", "Communauté d'Agglomération", "Communauté Urbaine"), array("Com Com", "Com d'Agglo", "Com Urb"), $bilan_territoires[$i]['territoire_lib']); ?></a>
												</td>
									<?php
											If ($bilan_territoires[$i]['etat_bilan'] == 0) {	
									?>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_territoires[$i]['surf_acq']); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_territoires[$i]['surf_conv']); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($bilan_territoires[$i]['surf_mfu']); ?></a>
												</td>
									<?php 
											} Else {
									?>
												<td style='text-align:right;width:100px;'>
													&nbsp;
												</td>
												<td style='text-align:right;width:100px;'>
													<center><a style='color:#fe0000;font-weight:bold;'>Ces données ne sont pas à jour</a></center>
												</td>
												<td style='text-align:right;width:100px;'>
													&nbsp;
												</td>
									<?php 
											}
									?>
												<td>
													&nbsp;
												</td>
											</tr>
									<?php													
											If (count($tbl_site_id) > 0 && $tbl_site_id[0] != '' && $bilan_territoires[$i]['etat_bilan'] == 0) {
												For ($j=0; $j<count($tbl_site_id); $j++) {
													If ($j<(count($tbl_site_id)-1))	{
														$border_radius_left = 'border-radius:0;';
														$border_radius_right = 'border-radius:0;';
													} Else If ($j==(count($tbl_site_id)-1))	{
														$border_radius_left = 'border-bottom-left-radius:20px;';
														$border_radius_right = 'border-bottom-right-radius:10px;';
													}
									?>
														
											<tr id='tr_site_<?php echo $bilan_territoires[$i]['territoire_id']. "_" . $j; ?>' class='tr_site_detail l<?php echo $tr; ?> no-export' style='cursor:default;display:none;'>
												<td style='<?php echo $border_radius_left; ?>'>
													&nbsp;
												</td>
												<td style='text-align:right;'>
													<a style='margin-right:0px;'><?php echo $tbl_site_id[$j]; ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_acq[$j]); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_conv[$j]); ?></a>
												</td>
												<td style='text-align:right;width:100px;'>
													<a style='margin-right:20px;'><?php echo conv_are($tbl_surf_mfu[$j]); ?></a>
												</td>
												<td style='<?php echo $border_radius_right; ?>'>
													&nbsp;
												</td>
											</tr>
									<?php	
												}
											} Else {
									?>		
												<tr id='tr_site_<?php echo $bilan_territoires[$i]['territoire_id'] . "_0"; ?>' class='tr_site_detail l<?php echo $tr; ?>' style='cursor:default;display:none;'>
													<td colspan='6' style='text-align:center;'>
														<a style='margin-right:0px;'>Aucun détail disponible</a>
													</td>
												</tr>
									<?php
											}	
										}
									?>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<div id="avertissement_terr" class="popup_avertissement" style="display: none;">
						<label>Les surfaces maitrisées détaillées par site sont relatives au territoire concerné<br>Il ne s'agit donc pas obligatoirement des surfaces totales maitrisées sur le site</label>
					</div>
				</div>
			</div>
		</section>
		<?php 
			$res_lst_site->closeCursor();
			$res_lst_metasite->closeCursor();
			$res_lst_communes->closeCursor();
			$res_bilan_commune->closeCursor();
			$res_bilan_collectivites->closeCursor();
			$res_bilan_territoires->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
