<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>foncier/foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<script>
			$(document).ready( function () {
				$('.right > a').each(function () {
					left = '-'+$(this).css('width');
					$(this).css('left', left);
					$(this).animate({left:'0'},600);
				});
				
				$('.left > a').each(function () {
					left = '+'+$(this).css('width');
					$(this).css('left', left);
					$(this).animate({left:'0'},600);
				});
			});
			
		</script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Foncier';
			$h3 ='Accueil BD Foncier';
			include("../includes/header.php");
			include('../includes/breadcrumb.php');
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig'));
			
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}
			
			include('foncier_fct.php');
			
			$rq_info_maitrise =
			"SELECT COALESCE(sum(tmfutot.nbr_parcelle)) AS nbr_parcelle, COALESCE(sum(tmfutot.nbr_lot)) AS nbr_lot, COALESCE(sum(tmfutot.surf_total)) AS surf_total, tmfutot.bilan_mfu
			FROM (
				SELECT COALESCE(count(DISTINCT t1.par_id),0) AS nbr_parcelle, COALESCE(count(DISTINCT t2.lot_id),0) AS nbr_lot, COALESCE(sum(CASE WHEN tmfu.surf_mfu is Null THEN t2.dcntlo ELSE tmfu.surf_mfu END),0) as surf_total, CASE WHEN tmfu.mfu IS NULL THEN 'Pas de ma&icirc;trise' ELSE tmfu.mfu END AS bilan_mfu
				FROM sites.sites, foncier.cadastre_site t4 LEFT JOIN (
					SELECT cadastre_site.cad_site_id, 'Acquisition'::character varying(25) as mfu, Null as surf_mfu
					FROM foncier.cadastre_site, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf, (
						SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
						FROM foncier.r_mfstatut
						GROUP BY r_mfstatut.mf_id
						) as tmax
					WHERE cadastre_site.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id
					AND (r_mfstatut.date = tmax.statut_date AND r_mfstatut.mf_id = tmax.mf_id)
					AND d_typmf.typmf_lib != 'Acquisition par un tiers' AND r_cad_site_mf.date_sortie = 'N.D.' AND d_statutmf.statutmf_id = 1
					UNION
					SELECT cadastre_site.cad_site_id, 'Convention'::character varying(25) as mfu, r_cad_site_mu.surf_mu AS surf_mfu
					FROM foncier.cadastre_site, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (
						SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
						FROM foncier.r_mustatut
						GROUP BY r_mustatut.mu_id
						) as tmax
					WHERE cadastre_site.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id
					AND d_typmu.mfu = true --ATTENTION NO-MFU
					AND (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id)
					AND r_cad_site_mu.date_sortie = 'N.D.' AND d_statutmu.statutmu_id = 1 AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text 
					) AS tmfu ON t4.cad_site_id = tmfu.cad_site_id, cadastre.cadastre_cen t3, cadastre.lots_cen t2, cadastre.parcelles_cen t1
				WHERE sites.site_id = t4.site_id AND t4.cad_cen_id = t3.cad_cen_id AND t3.lot_id = t2.lot_id and t2.par_id = t1.par_id
				AND t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'
				GROUP BY tmfu.mfu
				UNION
				SELECT '0', '0', '0', 'Acquisition' UNION SELECT '0', '0', '0', 'Convention' UNION SELECT '0', '0', '0', 'Pas de ma&icirc;trise'
				) AS tmfutot
			GROUP BY tmfutot.bilan_mfu
			ORDER BY tmfutot.bilan_mfu";
			$res_info_maitrise = $bdd->prepare($rq_info_maitrise);
			$res_info_maitrise->execute();
			$info_maitrise = $res_info_maitrise->fetchAll();
			
			$rq_nbr_actes = "
			SELECT tt1.type_acte, count(tt1.acte_id) as nbr_acte
			FROM
				(
					SELECT DISTINCT 'Acquisition' as type_acte, mf_acquisitions.mf_id as acte_id
					FROM foncier.cadastre_site t4, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.r_mfstatut, 
					(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
					WHERE t4.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.mf_id = r_mfstatut.mf_id  AND (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id) AND r_mfstatut.statutmf_id = 1 AND date_sortie = 'N.D.'
					UNION
					SELECT DISTINCT 'Convention' as type_acte, mu_conventions.mu_id as acte_id
					FROM foncier.cadastre_site t4, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.r_mustatut, 
					(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt1, foncier.d_typmu
					WHERE t4.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.mu_id = r_mustatut.mu_id AND d_typmu.typmu_id = mu_conventions.typmu_id 
					AND d_typmu.mfu = true --ATTENTION NO-MFU
					AND (r_mustatut.date = tt1.statut_date AND r_mustatut.mu_id = tt1.mu_id) 
					AND r_mustatut.statutmu_id = 1 AND date_sortie = 'N.D.'
					ORDER BY type_acte
				) tt1
			GROUP BY tt1.type_acte";
			$res_nbr_actes = $bdd->prepare($rq_nbr_actes);
			$res_nbr_actes->execute();
			$nbr_actes = $res_nbr_actes->fetchAll();
			
			$rq_etat_foncier = "
			WITH tglobal AS (SELECT count(tnb.site_id) as nb FROM (
				SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata 
				UNION 
				SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen WHERE statutmfu_id = 1
				UNION 
				SELECT DISTINCT site_id FROM alertes.v_alertes_mu WHERE alerte LIKE 'termin%'
				UNION
				SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc
			) AS tnb) SELECT CASE WHEN nb > 0 THEN 1 ELSE 0 END as etat_bilan FROM tglobal";
			$res_etat_foncier = $bdd->prepare($rq_etat_foncier);
			$res_etat_foncier->execute();
			$etat_foncier_tmp = $res_etat_foncier->fetch();
			$etat_foncier = $etat_foncier_tmp['etat_bilan'];
	
		?>
		<section id='main'>
			<h3>Accueil BD Foncier</h3>
			
			<div style="padding: 0 5px; width: calc(100% - 10px);">
				<table width = '100%' height='100%' CELLSPACING = '0' CELLPADDING ='0' style='border:none;'>
					<tr>
						<td style="width: 20%;">
							<fieldset>
								<table CELLSPACING = '5' CELLPADDING ='5' class='bilan-foncier'>
									<?php If ($etat_foncier > 0) { ?>
									<div class='prob_info'>
										<label>											
											Ces donn&eacute;es ne sont pas &agrave; jour
										</label>
									</div>
									<?php } ?>
									<tr>
										<td class='bilan-foncier' style='vertical-align:top;text-align:left'>
											<label class="label"><u>Bilan de la ma&icirc;trise :</u></label>
										</td>
									</tr>
									<tr>
										<td style='text-align:left'>
											<table CELLSPACING = '5' CELLPADDING ='5' class='bilan-foncier'>
												<?php For ($i=0; $i<$res_info_maitrise->rowCount(); $i++) { ?>
												<tr <?php If ($etat_foncier > 0) { ?>style='opacity:0.7;'<?php } ?>>
													<td class='bilan-foncier' style='vertical-align:top; text-align:right'>
														<li class='label'style="text-align:right"><?php echo $info_maitrise[$i]['bilan_mfu']; ?> : </li>
													</td>
													<td class='bilan-foncier' style='padding-bottom:10px'>
														<li style='margin:0' class='label'>
															<?php echo $info_maitrise[$i]['nbr_lot']; ?> lot<?php If ($info_maitrise[$i]['nbr_lot']>1) {echo "s";} ?>
														</li>
														<li style='margin:0' class='label'>
															<?php echo conv_are($info_maitrise[$i]['surf_total']); ?> ha.a.ca
														</li>
														<li style='margin:0' class='label'>
															<?php
															For ($j=0; $j<$res_nbr_actes->rowCount(); $j++) 
															{ 
																If ($info_maitrise[$i]['bilan_mfu'] == $nbr_actes[$j]['type_acte']) { 
																	echo $nbr_actes[$j]['nbr_acte'] . ' acte';
																	If ($nbr_actes[$j]['nbr_acte'] > 1) {echo 's';}
																}
															} 
															?>
														</li>
													</td>
												</tr>
												<?php } ?>
											</table>
										</td>
									</tr>													
								</table>
							</fieldset>
						</td>
						<td style="vertical-align: top;">
							<div class='tbl_btn'>
								<div id='btn_1' class='btn_accueil left btn_1'>
									<a href='foncier_liste_site.php'>Recherche par site</a>
								</div>
								<div id='btn_2' class='btn_accueil right btn_2'>
									<a href='foncier_liste_parcelle.php'>Recherche par parcelle</a>
								</div>
								<div id='btn_3' class='btn_accueil left btn_3'>
									<a href='foncier_liste_proprio.php'>Recherche par propriétaire</a>
								</div>
								<div id='btn_4' class='btn_accueil right btn_4'>
									<a href='foncier_liste_acte.php'>Recherche par acte</a>
								</div>
								<div id='btn_5' class='btn_accueil left btn_5'>
									<a href='foncier_bilan.php#site'>Bilan complet</a>
								</div>
								<div id='btn_6' class='btn_accueil right btn_6'>
									<a href='../alertes/accueil_alertes.php#foncier'>Alertes</a>
								</div>
						<?php
							If (role_access(array('grp_sig')) == 'OUI') {	
						?>
								<div id='btn_7' class='btn_accueil left btn_7'>
									<a href='foncier_admin.php'>Administration</a>
								</div>
						<?php
							}
						?>
							</div>
						</td>
						<td style="width: 20%;">							
						</td>
					</tr>					
				</table>
			</div>
		</section>
		<?php
			$res_info_maitrise->closeCursor();
			$res_nbr_actes->closeCursor();
			$res_etat_foncier->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
