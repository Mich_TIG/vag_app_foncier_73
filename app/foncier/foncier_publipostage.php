<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	$d= base64_decode($_GET['d']);
	$min_crit = 0;
	$max_crit = strpos($d, ':');
	$min_val = strpos($d, ':') + 1;
	$max_val = strpos($d, ';') - $min_val;
	
	For ($p=0; $p<substr_count($d, ':'); $p++) {
		$crit = substr($d, $min_crit, $max_crit);
		$value = substr($d, $min_val, $max_val);
		$$crit = $value;
		$min_crit = strpos($d, ';', $min_crit) + 1;
		$max_crit= strpos($d, ':', $min_crit) - $min_crit;
		$min_val = $min_crit + $max_crit + 1;
		$max_val = strpos($d, ';', $min_val) - $min_val;
	}
	
	header("Content-type: text/csv charset=utf-8");
	header("Content-disposition: attachment; filename=$champ_id-publipostage.csv");

	try //tentative de création d'un objet PDO permettant une connexion à la base foncière
	{
		$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']); //variable de connection à la base foncière
	}
	catch (Exception $e) //récupération des erreurs en cas d'echec de création du PDO
	{
		die("Impossible de créer le fichier XLS"); //en cas d'échec intégration de la page affichant un problème technique
	}	
						
	If ($page == 'fiche_mfu.php' && isset($type_acte) && $type_acte == 'Acquisition')
		{$suffixe = 'mf';}
	Else If ($page == 'fiche_mfu.php' && isset($type_acte)  && $type_acte == 'Convention')
		{$suffixe = 'mu';}
	
	$where = "WHERE t3.date_fin = 'N.D.' AND t4.date_fin = 'N.D.' AND t3.cad_cen_id = t4.cad_cen_id AND t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper";
	
	If ($page == 'fiche_site.php')
		{$where .= " AND t4.site_id = '$champ_id'";}
	ElseIf ($page == 'fiche_mfu.php')
		{$where .= " AND r_cad_site_$suffixe.cad_site_id = t4.cad_site_id AND r_cad_site_$suffixe.$suffixe"."_id = '$champ_id'";}
		
	$rq_lst_proprios = 
	"SELECT DISTINCT
	d_ccoqua.particule_lib,	t7.ddenom,	t7.nom_usage, t7.prenom_usage, t7.nom_jeunefille, dlign3, dlign4, dlign5, dlign6
	FROM cadastre.cadastre_cen t3, foncier.cadastre_site t4, cadastre.cptprop_cen t5, cadastre.r_prop_cptprop_cen t6, cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua) ";
	
	If ($page == 'fiche_mfu.php')
		{$rq_lst_proprios .= ", foncier.r_cad_site_$suffixe ";}
	
	$rq_lst_proprios .= $where . " ORDER BY nom_usage, ddenom";
	$res_lst_proprios = $bdd->prepare($rq_lst_proprios);
	$res_lst_proprios->execute();
	// echo $rq_lst_proprios;
	
	// Création de la ligne d'en-tête
	$entete = array("Particule", "Nom", "Nom de jeune fille", "Prenom", "Adresse1", "Adresse2", "Adresse3", "Adresse4");
	$separateur = ";";	
	// Affichage de la ligne de titre, terminée par un retour chariot
	echo implode($separateur, $entete)."\r\n";
	
	While ($donnees = $res_lst_proprios->fetch()) {
		If ($donnees['nom_usage'] != 'N.P.' ) {
			echo $donnees['particule_lib'] . ';';
			echo $donnees['nom_usage'] . ';'; 
			If ($donnees['nom_jeunefille'] != 'N.P.' AND $donnees['nom_jeunefille'] != $donnees['nom_usage']) {
				echo $donnees['nom_jeunefille'] . ';'; 
			} Else {
				echo ';'; 
			}
			If ($donnees['prenom_usage'] != 'N.P.' ) {
				If (substr_count($donnees['prenom_usage'], ' ') > 0) {
						$prenom = substr($donnees['prenom_usage'], 0, strpos($donnees['prenom_usage'], ' '));
					} Else {
						$prenom = $donnees['prenom_usage'];
					}
			} Else {
				If (substr_count($donnees['ddenom'], '/') > 0) {
					If (substr_count($donnees['ddenom'], ' ') > 0) {
						$prenom = substr($donnees['ddenom'], strpos($donnees['ddenom'], '/')+1, (strpos($donnees['ddenom'], ' ') - strpos($donnees['ddenom'], '/')));
					} Else {
						$prenom = substr($donnees['ddenom'], strpos($donnees['ddenom'], '/')+1, (strlen($donnees['ddenom']) - strpos($donnees['ddenom'], '/')));
					}
				} Else {
					$prenom = '';
				}
			}
			echo $prenom . ';';
			echo $donnees['dlign3'] . ';';
			echo $donnees['dlign4'] . ';';
			echo $donnees['dlign5'] . ';';
			echo $donnees['dlign6'] . ';';
			echo "\r\n";
			
		} Else If ($donnees['ddenom'] != '') {
			If (substr_count($donnees['ddenom'], '/') > 0) {
				$nom = substr($donnees['ddenom'], 0, strpos($donnees['ddenom'], '/'));
				If (substr_count($donnees['ddenom'], ' ') > 0) {
					$prenom = ucwords(strtolower(substr($donnees['ddenom'], strpos($donnees['ddenom'], '/')+1, (strpos($donnees['ddenom'], ' ') - strpos($donnees['ddenom'], '/')))));
				} Else {
					$prenom = ucwords(strtolower(substr($donnees['ddenom'], strpos($donnees['ddenom'], '/')+1, (strlen($donnees['ddenom']) - strpos($donnees['ddenom'], '/')))));
				}
			} Else {
				$nom = $donnees['ddenom'];
				$prenom = '';
			}
			echo $donnees['particule_lib'] . ';';
			echo $nom . ';';
			echo $prenom . ';';
			echo $donnees['dlign3'] . ';';
			echo $donnees['dlign4'] . ';';
			echo $donnees['dlign5'] . ';';
			echo $donnees['dlign6'] . ';';
			echo "\r\n";
		}
	}	
	$res_lst_proprios->closeCursor();
	$bdd = null;
?>