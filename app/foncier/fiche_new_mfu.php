<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">		
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/explorateur.css">		
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/jquery-ui.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script>
			$(document).ready( function () {
				chgStatut();
				chgReconduction('');
				resizeTextarea('acte_txt_com');
				resize_fieldset();
				
				$(window).focus(function(){
				  ajaxReloadElt('acte_notaire')
				});
			} ) ;

			jQuery(function($){
				$.datepicker.setDefaults($.datepicker.regional['fr']);
				$('.datepicker').datepicker({
					dateFormat : 'dd/mm/yy',
					changeMonth: true,
					changeYear: true,
					onSelect: function(date){
						if(this.id == 'acte_date_fin') {
							$('#acte_date_debut').datepicker('option', 'maxDate', date);
						} else if(this.id == 'acte_date_debut'){
							$('#acte_date_fin').datepicker('option', 'minDate', date);
						}
						if(this.id == 'acte_date_signature') {
							$('#acte_date_delib_ca').datepicker('option', 'maxDate', date);
							$('#acte_date_promesse').datepicker('option', 'maxDate', date);
							$('#acte_date_compromis').datepicker('option', 'maxDate', date);
						} else if(this.id == 'acte_date_delib_ca'){
							$('#acte_date_signature').datepicker('option', 'minDate', date);
						} else if(this.id == 'acte_date_promesse'){
							$('#acte_date_signature').datepicker('option', 'minDate', date);
						} else if(this.id == 'acte_date_compromis'){
							$('#acte_date_signature').datepicker('option', 'minDate', date);
						}
					}
				});
				if ($("#acte_date_rappel").length) {
					if ($('#acte_date_rappel').val() == 'N.D.') {
						$('#acte_date_rappel').datepicker('option', 'minDate', '0');
						$('#acte_date_rappel').val('N.D.');
					} else if ($('#acte_date_signature').val() == 'N.D.') {
						$('#acte_date_signature').datepicker('option', 'maxDate', '0');
						$('#acte_date_signature').val('N.D.');
					}
				} else {
					
					if ($('#acte_date_signature').datepicker("getDate") && $('#acte_date_signature').val() != 'N.D.'){
						$('#acte_date_delib_ca').datepicker('option', 'maxDate', $('#acte_date_signature').datepicker("getDate"));
						$('#acte_date_promesse').datepicker('option', 'maxDate', $('#acte_date_signature').datepicker("getDate"));
						$('#acte_date_compromis').datepicker('option', 'maxDate', $('#acte_date_signature').datepicker("getDate"));
					} else if ($('#acte_date_signature').val() == 'N.D.') {
						$('#acte_date_signature').datepicker('option', 'maxDate', '0');
						$('#acte_date_signature').val('N.D.');
						$('#acte_date_delib_ca').datepicker('option', 'maxDate', '0');
						$('#acte_date_delib_ca').val('N.D.');
						$('#acte_date_promesse').datepicker('option', 'maxDate', '0');
						$('#acte_date_promesse').val('N.D.');
						$('#acte_date_compromis').datepicker('option', 'maxDate', '0');
						$('#acte_date_compromis').val('N.D.');
					}
					if ($('#acte_date_delib_ca').datepicker("getDate") && $('#acte_date_delib_ca').val() != 'N.D.'){
						$('#acte_date_signature').datepicker('option', 'minDate', $('#acte_date_delib_ca').datepicker("getDate"));
					}

				}
			});
		</script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php
			include ('../includes/valide_acces.php');
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}					
			$bad_caract = array("-","–","’", "'", "", "\t", "	");
			$good_caract = array("–","-","''", "''", "", "", "");
			
			If (isset($_GET['d']) && substr_count(base64_decode($_GET['d']), ':') > 0 && substr_count(base64_decode($_GET['d']), ';') > 0) {
				$d= base64_decode($_GET['d']);
				$min_crit = 0;
				$max_crit = strpos($d, ':');
				$min_val = strpos($d, ':') + 1;
				$max_val = strpos($d, ';') - $min_val;
				
				For ($p=0; $p<substr_count($d, ':'); $p++) {
					$crit = substr($d, $min_crit, $max_crit);
					$value = substr($d, $min_val, $max_val);
					$$crit = $value;
					$min_crit = strpos($d, ';', $min_crit) + 1;
					$max_crit= strpos($d, ':', $min_crit) - $min_crit;
					$min_val = $min_crit + $max_crit + 1;
					$max_val = strpos($d, ';', $min_val) - $min_val;
				}
				If (!isset($type_acte)) {
					$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
					include('../includes/prob_tech.php');
				}
			} Else {
				$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
				include('../includes/prob_tech.php');
			}
			
			$verif_role_acces = role_access(array('grp_sig', 'grp_foncier')); //variable permettant de définir quels utilisateurs peuvent passer en mode avancé(édition)
			
			$h2 ='Foncier'; //varible stockant le titre h2 de la page et utiliser dans la page header intégreée juste après
			include('../includes/header.php'); //intégration de la page affichant l'entête nécessaire dans toutes les pages du site
			include('../includes/breadcrumb.php');
			//include('../includes/construction.php'); //(A COMMENTER POUR RENDRE LA PAGE OPERATIONNELLE) intégration de la page permettant d'afficher une page en construction pour tous ceux qui ne sont pas administrateurs 
			include('foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
			include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
						
			If ($type_acte == 'Acquisition') {
				$suffixe = 'mf';
				$lst_date['lib'] = array("Compromis de vente", "Promesse d'achat", "Délibération en CA", "Signature");
				$lst_date['champ'] = array('date_compromis', 'date_promesse', 'date_delib_ca', 'date_signature');
			} Else If ($type_acte == 'Convention') {
				$suffixe = 'mu';
				$lst_date['lib'] = array("Signature", "Début", "Fin");
				$lst_date['champ'] = array('date_signature', 'date_debut', 'date_fin');
			}
				
			$rq_check_grant = "
			SELECT 
				array_to_string(array_agg(grant_elt_id), ',') as lst_elt_id,
				array_to_string(array_agg(grant_elt_form_id), ',') as lst_elt_form_id
			FROM 
				foncier.grant_group 
				JOIN foncier.d_grant_elt USING (grant_elt_id)
			WHERE 
				grant_group_id IN ('".implode("','", $_SESSION['grp'])."') AND 
				grant_group_droit = 't' AND
				grant_elt_type_acte IN ('acte', '".$suffixe."')";			
			$res_check_grant = $bdd->prepare($rq_check_grant);
			$res_check_grant->execute();
			$lst_grant_elt_temp = $res_check_grant->fetch();
			$tbl_grant_elt_form_id = explode (',', $lst_grant_elt_temp[1]);
			$tbl_grant_elt_id = explode (',', $lst_grant_elt_temp[0]);
			
			$rq_parc_dispo =
			"SELECT 
				tt1.cad_site_id,
				tt1.codcom,
				tt1.ccosec,
				tt1.dnupla,
				tt1.dnulot,
				tt1.par_id,
				CASE WHEN mfu = 'R.A.S.' THEN tt1.cad_site_val ELSE tt1.cad_site_val || ' ' || mfu 
				END 
				|| CASE WHEN tt1.parm_id IS NOT NULL THEN ' Parcelle mère : ' || tt1.parm_id ELSE ''
				END	as cad_site_val
			FROM
				(SELECT 
					t4.cad_site_id, t1.codcom, t1.ccosec, t1.dnupla, t2.dnulot, t1.par_id,
					replace(t1.codcom || ' ' || t1.ccosec || lpad(t1.dnupla::text, 4, '0') || ' ' || COALESCE(t2.dnulot,'') || ' (' || t2.dcntlo || ' m²)', '  ', ' ') as cad_site_val, 
					COALESCE(tmfu.mfu, 'R.A.S.') as mfu, tparm.parm_id
				FROM 
					foncier.cadastre_site t4
					JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
					JOIN cadastre.lots_cen t2 USING (lot_id)
					JOIN cadastre.parcelles_cen t1 USING (par_id)
					JOIN sites.sites USING (site_id)
					LEFT JOIN
						(SELECT 
							cadastre_site.cad_site_id, 'Acquisition'::character varying(25) as mfu, Null AS surf_mfu
						FROM 
							foncier.cadastre_site, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf, (
							SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
							FROM foncier.r_mfstatut
							GROUP BY r_mfstatut.mf_id
							) as tmax
						WHERE 
							cadastre_site.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id 
							AND mf_acquisitions.typmf_id = d_typmf.typmf_id 
							AND mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id 
							AND (r_mfstatut.date = tmax.statut_date AND r_mfstatut.mf_id = tmax.mf_id) AND d_typmf.typmf_lib != 'Acquisition par un tiers' 
							AND d_statutmf.statutmf_id = 1 
							AND r_cad_site_mf.date_sortie = 'N.D.' 
							
						UNION

						SELECT 
							cadastre_site.cad_site_id, 
							CASE WHEN r_cad_site_mu.surf_mu is null THEN 
								'Convention'::character varying(25) 
							ELSE 'Convention pour partie'::character varying(25) 
							END as mfu,
							r_cad_site_mu.surf_mu AS surf_mfu
							
						FROM 
							foncier.cadastre_site, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (
							SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
							FROM foncier.r_mustatut
							GROUP BY r_mustatut.mu_id
							) as tmax
						WHERE 
							cadastre_site.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id 
							AND mu_conventions.typmu_id = d_typmu.typmu_id 
							AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id 
							AND (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id)
							AND d_statutmu.statutmu_id = 1 
							AND r_cad_site_mu.date_sortie = 'N.D.'
						GROUP BY 
							cadastre_site.cad_site_id, r_cad_site_mu.surf_mu) tmfu ON t4.cad_site_id = tmfu.cad_site_id
					LEFT JOIN
						(SELECT cadastre_site.cad_site_id, lpad(ccosecm,2,'0') || dnuplam as parm_id
						FROM cadastre.parcelles_cen
						JOIN cadastre.lots_cen USING (par_id)
						JOIN cadastre.cadastre_cen USING (lot_id)
						JOIN foncier.cadastre_site USING (cad_cen_id)
						WHERE ccocomm IS NOT NULL OR ccoprem IS NOT NULL OR ccosecm IS NOT NULL OR dnuplam IS NOT NULL) tparm ON t4.cad_site_id = tparm.cad_site_id 
				WHERE 
					t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'
					AND t4.site_id IN (?)
				) tt1
			WHERE mfu != 'Acquisition'
			ORDER BY cad_site_val";

			$res_parc_dispo = $bdd->prepare($rq_parc_dispo);
			$res_parc_dispo->execute(array($site_id));
			$tbl_parc_already_assoc = array();
			$tbl_parc_dispo = array();
			$p=0;
			$q=0;
			While ($donnees = $res_parc_dispo->fetch()) {
				If (isset($par_id) && $donnees['par_id'] != $par_id || !isset($par_id)) {
					$tbl_parc_dispo['cad_site_id'][$q] = $donnees['cad_site_id'];
					$tbl_parc_dispo['cad_site_val'][$q] = $donnees['cad_site_val'];
					$q++;
				} Else {
					$tbl_parc_already_assoc['cad_site_id'][$p] = $donnees['cad_site_id'];
					$tbl_parc_already_assoc['cad_site_val'][$p] = $donnees['cad_site_val'];
					$p++;
				}
			}
		?>
		<section id='main'>
			<h3>Nouvelle <?php echo $type_acte; ?></h3>
			<?php
			$cur_info = -1;
			$vget_maj = "type_acte:" . $type_acte . ";";
			$vget_maj .= "acte_id:new;";
			$vget_maj .= "mode:consult;";
			$vget_maj .= "do:maj;";
			$vget_maj .= "type_maj:add;";
			$vget_crypt_maj = base64_encode($vget_maj);
			$lien_maj= substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_mfu.php?d=$vget_crypt_maj#general";?>
			<form class='form' onsubmit="return valide_form('frm_new_mfu')" name='frm_new_mfu' action='<?php echo $lien_maj; ?>' id='frm_new_mfu' method='POST'>
				<input type='hidden' id='hid_tbl_grant_elt_form_id' value='<?php echo $lst_grant_elt_temp[1]; ?>'>
				<div id='onglet' class='list'>
					<ul>
						<li>
							<a href="#general"><span>G&eacute;n&eacute;ral</span></a>
						</li>
					</ul>
					<div id='general'>
						<input type='hidden' id='filtre_type_acte' value='<?php echo $type_acte; ?>';>
						<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
							<tr>
								<td>									
									<center>
										<table CELLSPACING = '0' CELLPADDING ='5' class='no-border' width='100%'>
											<tr class='tr_fieldset'>
												<td>
													<fieldset id='fieldset_description' class='fieldset'>
														<legend>Description</legend>
														<table id='tbl_description' CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='min-width:400px'>
															<tr style='height:32px'>
																<td style='text-align:right'>
																	<label class="label"><u>Site :</u></label>
																</td>
																<td>
																	<label class='label'>
																		<?php echo $site_nom; ?> (<?php echo $site_id; ?>)
																		<?php
																			$vget = "site_nom:$site_nom;site_id:$site_id;";
																			$vget_crypt = base64_encode($vget);
																			$lien_site = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";
																		?>
																		<a href='<?php echo $lien_site; ?>'>
																			<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche site">
																		</a>
																	</label>
																</td>
															</tr>
															<?php 
															If (in_array('acte_id', $tbl_grant_elt_form_id)) {														
															?>
															<tr style='height:32px'>
																<td style='text-align:right'>
																	<label class="label"><u>ID :</u></label>
																</td>
																<td style='width:451px'>
																	<input type='text' class='editbox' size='20' id='acte_id' name='acte_id'>
																	<label class='last_maj'>Id automatique si vide</label>
																</td>
															</tr>
															<?php
															}
															?>
															<tr style='height:32px'>
																<td style='text-align:right'>
																	<label class="label"><u>Libellé :</u></label>
																</td>
																<td style='width:451px'>
																	<input type='text' class='editbox' style='border-color:#a2c282;' size='60' id='need_acte_lib' name='need_acte_lib' value='' onchange="$('#need_acte_lib').css({'border-color': '#a2c282'});">
																</td>
															</tr>
															<tr style='height:32px'>
																<td style='text-align:right'>
																	<label class="label"><u>Type :</u></label>
																</td>
																<td>
																	<label class="btn_combobox">			
																		<select id="need_acte_type" name="need_acte_type" class="combobox" style='text-align:left;border-color:#a2c282;'>
																			<option value='-1'></option>
																			<?php
																			$rq_lst_acte_type = "SELECT typ".$suffixe."_id as id, typ".$suffixe."_lib as lib FROM foncier.d_typ".$suffixe." WHERE typ".$suffixe."_lib NOT LIKE '%(MAJ en 1 ou 6)' ORDER BY lib";
																			$res_lst_acte_type = $bdd->prepare($rq_lst_acte_type);
																			$res_lst_acte_type->execute();																			
																			While ($donnees = $res_lst_acte_type->fetch()) {
																				echo "<option value='$donnees[id]'>$donnees[lib]</option>";
																			}
																			$res_lst_acte_type->closeCursor();
																			?>				
																		</select>
																	</label>
																</td>		
															</tr>
															<tr style='height:32px'>
																<td style='text-align:right'>
																	<label class="label"><u>Statut :</u></label>
																</td>
																<td>
																	<label class="btn_combobox">			
																		<select name="need_acte_statut" id="need_acte_statut" class="combobox" style='text-align:left;border-color:#a2c282;' onchange='chgStatut()'>
																			<option value='-1'></option>
																			<?php
																			
																			$rq_lst_type_statut = "SELECT statut".$suffixe."_id as id, statut".$suffixe." as lib FROM foncier.d_statut".$suffixe;
																			If (!in_array('need_acte_statut', $tbl_grant_elt_form_id)) {
																				$rq_lst_type_statut .= " WHERE statut".$suffixe."_id = 0";
																			}
																			$res_lst_type_statut = $bdd->prepare($rq_lst_type_statut);
																			$res_lst_type_statut->execute();																			
																			While ($donnees = $res_lst_type_statut->fetch()) {
																				echo "<option value='$donnees[id]'>$donnees[lib]</option>";
																			}
																			$res_lst_type_statut->closeCursor();
																			?>				
																		</select>
																	</label>
																</td>
															</tr>
														</table>
													</fieldset>
												</td>
												<td>
													<fieldset id='fieldset_date' class='fieldset'>
														<legend>Dates</legend>
														<table id='tbl_date' CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='width:100%;height:100%;'>
															<?php For($i=0; $i<count($lst_date['champ']); $i++) { ?>
															<tr id='tr_<?php echo $lst_date['champ'][$i]; ?>'>
																<td style='text-align:right'>
																	<label class='label'><u><?php echo $lst_date['lib'][$i]; ?> :</u></label>
																</td>
																<td>
															<?php
																If (in_array('acte_'.$lst_date['champ'][$i], $tbl_grant_elt_form_id)) {
															?>																
																	<div>
																		<input type='text' size='10' style='border-color:#a2c282;' class='datepicker' id='acte_<?php echo $lst_date['champ'][$i]; ?>' name='acte_<?php echo $lst_date['champ'][$i]; ?>'>
																	</div>
															<?php
																} Else {
															?>
																	<div class='readonly'>
																		<input type='text' size='10' style='border-color:#a2c282;' class='datepicker' id='acte_<?php echo $lst_date['champ'][$i]; ?>' name='acte_<?php echo $lst_date['champ'][$i]; ?>' READONLY DISABLED>
																	</div>
															<?php
																}
															?>
																</td>								
															</tr>
															<?php } ?>
															<?php If ($type_acte == 'Convention') { ?>
															<tr>
																<td style='text-align:right'>
																	<label class='label'><u>Durée initiale :</u></label>
																</td>
																<td>
															<?php
																If (in_array('acte_duree', $tbl_grant_elt_form_id)) {
															?>																
																	<div>
																		<input type='text' size='4' class='editbox' style='border-color:#a2c282;text-align:right;' id='acte_duree' name='acte_duree' value='132'>
																	<label class="label"> mois</label>
																	</div>
															<?php
																} Else {
															?>
																	<div class='readonly'>
																		<input type='text' size='4' class='editbox' style='border-color:#a2c282;text-align:right;' id='acte_duree' name='acte_duree' value='' READONLY DISABLED>
																		<label class="label"> mois</label>
																	</div>
															<?php
																}
															?>	
																</td>
															</tr>
															<tr>
																<td style='text-align:right'>
																	<label class='label'><u>Reconduction tacite :</u></label>
																</td>
																<td colspan='2'>
															<?php
																If (in_array('acte_chk_reconduction', $tbl_grant_elt_form_id)) {
															?>																
																	<div id='div_acte_chk_reconduction'>
																		<input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='acte_chk_reconduction' name='acte_chk_reconduction' onclick="chgReconduction('')">
																	</div>
															<?php
																} Else {
															?>
																	<div id='div_acte_chk_reconduction' class='readonly'>
																		<input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='acte_chk_reconduction' name='acte_chk_reconduction' onclick="chgReconduction('')" READONLY DISABLED>
																	</div>
															<?php
																}
															?>
																</td>
															</tr>
															<tr>
																<td style='text-align:right'>
																	<label class='label'><u>Nombre de reconduction maximum :</u></label>
																</td>
																<td colspan='2' style='min-width:152px'>
															<?php
																If (in_array(array('acte_chk_reconduction', 'acte_recond_nbmax'), $tbl_grant_elt_form_id)) {
															?>																
																	<div id='div_acte_recond_nbmax'>
																		<input type='text' size='4' class='editbox' style='border-color:#a2c282;' id='acte_recond_nbmax' name='acte_recond_nbmax'>
																		<label id='acte_lbl_ou' class='label'> ou</label>
																		<input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='acte_chk_infini' name='acte_chk_infini' onclick='chk_infini()'>
																		<label id='acte_lbl_infini' class='label'>infini</label>
																	</div>
															<?php
																} Else {
															?>
																	<div id='div_acte_recond_nbmax' class='readonly'>
																		<input type='text' size='4' class='editbox' style='border-color:#a2c282;' id='acte_recond_nbmax' name='acte_recond_nbmax' READONLY DISABLED>
																		<label id='acte_lbl_ou' class='label'> ou</label>
																		<input type='checkbox' width='15px' class='editbox' style='border-color:#a2c282;-moz-transform: scale(1.2);' id='acte_chk_infini' name='acte_chk_infini' onclick='chk_infini()' READONLY DISABLED>
																		<label id='acte_lbl_infini' class='label'>infini</label>
																	</div>
															<?php
																}
															?>
																</td>
															</tr>
															<tr>
																<td style='text-align:right'>
																	<label class='label'><u>Pas de temps de la reconduction :</u></label>
																</td>
																<td colspan='2'>
															<?php
																If (in_array(array('acte_chk_reconduction', 'acte_recond_nbmax'), $tbl_grant_elt_form_id)) {
															?>																
																	<div id='div_acte_pas_tps'>
																		<input type='text' size='4' class='editbox' style='border-color:#a2c282;' id='acte_pas_tps' name='acte_pas_tps'><label class="label"> mois</label>
																	</div>
															<?php
																} Else {
															?>
																	<div id='div_acte_pas_tps' class='readonly'>
																		<input type='text' size='4' class='editbox' style='border-color:#a2c282;' id='acte_pas_tps' name='acte_pas_tps' READONLY DISABLED><label class="label"> mois</label>
																	</div>
															<?php
																}
															?>		
																	
																</td>
															</tr>
															<tr>
																<td style='text-align:right'>
																	<label class='label'><u>Date de rappel avant reconduction :</u></label>
																</td>
																<td colspan='2'>
															<?php
																If (in_array(array('acte_chk_reconduction', 'acte_recond_nbmax'), $tbl_grant_elt_form_id)) {
															?>																
																	<div id='div_acte_date_rappel'>
																		<input type='text' size='10' class='datepicker' style='border-color:#a2c282;' id='acte_date_rappel' name='acte_date_rappel'>
																	</div>
															<?php
																} Else {
															?>
																	<div id='div_acte_date_rappel' class='readonly'>
																		<input type='text' size='10' class='datepicker' style='border-color:#a2c282;' id='acte_date_rappel' name='acte_date_rappel' READONLY DISABLED>
																	</div>
															<?php
																}
															?>
																	
																</td>
															</tr>
															<?php } ?>
														</table>
													</fieldset>
												</td>
											</tr>
											<tr class='tr_fieldset'>
												<td>
													<fieldset id='fieldset_parcelle' class='fieldset'>
														<legend>Parcelles/ Surface</legend>
														<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='width:100%; height:100%'>
															<tr>
																<td style='text-align:center; vertical-align:bottom'>
																	<input type="button" value="Associer des parcelles à l'acte" id='btn_add_parc' name='btn_add_parc' class='btn_frm' onclick="addParc_js('explorateur', 'onglet');"/>
																</td>
															</tr>
															<tr>
																<td style='text-align:center;padding-top:15px; vertical-align:top'>
																</td>
															</tr>
														</table>	
													</fieldset>
												</td>
												<td>
													<fieldset id='fieldset_frais' class='fieldset'>
														<legend>Montants</legend>
														<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='width:100%;height:100%;'>
															<?php 
															$rq_lst_type_frais = 
															"SELECT d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_id as id, d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_lib as lib FROM foncier.d_typfrais" . $suffixe . " ORDER BY lib";
															$res_lst_type_frais = $bdd->prepare($rq_lst_type_frais);
															$res_lst_type_frais->execute();
															While ($donnees = $res_lst_type_frais->fetch()) { ?>
															<tr>
																<td style='text-align:right'>
																	<label class='label'><u><?php echo $donnees['lib']; ?> :</u></label>
																</td>
																<td>
															<?php
																If (in_array('acte_frais_', $tbl_grant_elt_form_id)) {
															?>																
																	<div>
																		<input type='text' size='10' class='editbox' style='border-color:#a2c282;' id='acte_frais_<?php echo $donnees['id']; ?>' name='acte_frais_<?php echo $donnees['id']; ?>' value=''><label class="label"> €</label>
																	</div>
															<?php
																} Else {
															?>
																	<div class='readonly'>
																		<input type='text' size='10' class='editbox' style='border-color:#a2c282;' id='acte_frais_<?php echo $donnees['id']; ?>' name='acte_frais_<?php echo $donnees['id']; ?>' value='' READONLY DISABLED><label class="label"> €</label>
																	</div>
															<?php
																}
															?>
																</td>
															</tr>
															<?php 
															}
															$res_lst_type_frais->closeCursor();
															?>
														</table>
													</fieldset>
												</td>
											</tr>
											<?php  If ($type_acte == 'Acquisition') { ?> 
											<tr>
												<td colspan='2'>
													<fieldset class='fieldset'>
														<legend>Notaire</legend>
														<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='width:100%;height:100%;'>
															<tr>
																<td style='text-align:left;width:50%;'>
																	<div id='div_acte_notaire' <?php If (!in_array('acte_notaire', $tbl_grant_elt_form_id)) {echo "class='readonly'";} ?>>
																		<label class="btn_combobox">			
																			<select name="acte_notaire" id="acte_notaire" class="combobox" style='text-align:center;border-color:#a2c282;' <?php If (!in_array('acte_notaire', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED";} ?>>
																				<option value='-1'>Notaire</option>
																				<?php
																				If (in_array('acte_notaire', $tbl_grant_elt_form_id)) {
																					$rq_lst_notaire =
																					"SELECT personne_id as notaire_id, nom  || ' ' || prenom || COALESCE(CASE WHEN t_nb_pers.nb_pers > 1 THEN ' (' || personne.adresse3 || ')' END, '') as notaire_lib
																					FROM personnes.personne 
																						JOIN personnes.d_particules USING (particules_id)
																						JOIN personnes.d_individu USING (individu_id)
																						JOIN (SELECT individu_id, count(*) AS nb_pers FROM personnes.d_individu JOIN personnes.personne USING (individu_id) GROUP BY individu_id) t_nb_pers USING (individu_id)
																					WHERE personne.fonctpers_id = ".FONCTNOT_ID."
																					ORDER BY nom";
																				} Else {
																					$rq_lst_notaire = "SELECT notaire_id, notaire_lib WHERE 1 = 2";
																				}
																				$res_lst_notaire = $bdd->prepare($rq_lst_notaire);
																				$res_lst_notaire->execute();
																				While ($donnees = $res_lst_notaire->fetch()) {
																					echo "<option value='$donnees[notaire_id]'>$donnees[notaire_lib]</option>";
																				}
																				$res_lst_notaire->closeCursor();
																				?>
																			</select>
																		</label>
																	<?php 
																		If (in_array('acte_notaire', $tbl_grant_elt_form_id)) {
																			$vget = "type_contact:notaire;origine:mfu;";
																			$vget_crypt = base64_encode($vget);
																			$lien = ROOT_PATH . "contacts/new_contact.php?d=$vget_crypt";
																	?>
																		<a target="_blank" href="<?php echo $lien; ?>">
																			<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour gérer la liste des notaires">
																		</a>
																	<?php
																		}
																	?>
																	</div>																	
																</td>
																<td style='text-align:left;'>
																	<label class='label'>Contact : </label>
																<?php 
																	If (in_array('acte_notaire', $tbl_grant_elt_form_id)) {
																?>	
																	<input type='text' size='70' class='editbox' style='border-color:#a2c282;' id='acte_notaire_contact' name='acte_notaire_contact' value=''>
																<?php 
																	} Else {
																?>
																	<input type='text' size='70' class='editbox readonly' style='border-color:#a2c282;' id='acte_notaire_contact' name='acte_notaire_contact' value='' READONLY DISABLED>
																<?php
																	}
																?>																	
																</td>
															</tr>
														</table>
													</fieldset>
												</td>
											</tr>
											<tr>
												<td colspan='2'>
													<fieldset class='fieldset'>
														<legend>Suivi administratif</legend>
														<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='height:100%;'>
															<tr>
																<td style='text-align:left;'>
																	<label class='label'><u>Acte re&ccedil;u :</u></label>
																	<input type='checkbox' class='editbox' style='-moz-transform: scale(1.2);' id='acte_chk_recep_courrier' name='acte_chk_recep_courrier' <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";} ?>>
																</td>
																<td style='text-align:left;'>
																	<label class='label'>le : </label>
																	<input type='text' size='10' class='datepicker <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " readonly";} ?>' style='border-color:#a2c282;' id='acte_date_recep_courrier' name='acte_date_recep_courrier' onblur="autoChecked('acte_date_recep_courrier', 'acte_chk_recep_courrier');" <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";} ?>>
																</td>
															</tr>
														</table>
													</fieldset>
												</td>
											</tr>
											<?php } Else If ($type_acte == 'Convention') { ?> 
											<tr>
												<td colspan='2'>
													<fieldset class='fieldset'>
														<legend>Suivi administratif</legend>
														<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='height:100%;'>
															<tr>
																<td style='text-align:left;'>
																	<label class='label'><u>Convention sign&eacute;e et retourn&eacute;e aux propri&eacute;taires :</u></label>
																	<input type='checkbox' class='editbox' style='-moz-transform: scale(1.2);' id='acte_chk_envoi_courrier' name='acte_chk_envoi_courrier' <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";} ?>>
																</td>
																<td style='text-align:left;'>
																	<label class='label'>le : </label>
																	<input type='text' size='10' class='datepicker <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " readonly";} ?>' style='border-color:#a2c282;' id='acte_date_envoi_courrier' name='acte_date_envoi_courrier' onblur="autoChecked('acte_date_envoi_courrier', 'acte_chk_envoi_courrier');" <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";} ?>>
																</td>
															</tr>
														</table>
													</fieldset>
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td colspan='2'>
													<fieldset class='fieldset'>
														<legend>Commentaires</legend>
														<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px'>
															<tr>
																<td>
																	<label class='label'><u>Clauses particulières :</u></label>
																	<input type='checkbox' class='editbox' style='-moz-transform: scale(1.2);' id='acte_chk_clauses' name='acte_chk_clauses' <?php If (!in_array('acte_chk_clauses', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED";} ?>>
																</td>
															</tr>
															<tr>
																<td>																
															<?php
																If (in_array('acte_txt_com', $tbl_grant_elt_form_id)) {
															?>																
																	<div>
																		<textarea onkeyup="resizeTextarea('acte_txt_com');" onchange="resizeTextarea('acte_txt_com');" id='acte_txt_com' name='acte_txt_com' class="label textarea_com_edit" rows='3' cols='1000'></textarea>
																	</div>
															<?php
																} Else {
															?>
																	<div class='readonly'>
																		<textarea onkeyup="resizeTextarea('acte_txt_com');" onchange="resizeTextarea('acte_txt_com');" id='acte_txt_com' name='acte_txt_com' class="label textarea_com_edit" rows='3' cols='1000' READONLY DISABLED></textarea>
																	</div>
															<?php
																}
															?>
																</td>
															</tr>
														</table>
													</fieldset>
												</td>
											</tr>
										</table>
									</center>
								</td>
							</tr>
							<tr>
								<td colspan='3' class='save'>
									<div class='lib_alerte' id='prob_saisie' style='display:none;'>Veuillez compl&eacuteter votre saisie</div>
									<input type='hidden' name='site_id' value='<?php echo $site_id; ?>'>
									<input type='hidden' name='parc_ajout' id='parc_ajout' value='<?php If (isset($tbl_parc_already_assoc['cad_site_id']) && count($tbl_parc_already_assoc['cad_site_id']) > 0) {For ($p=0; $p<count($tbl_parc_already_assoc['cad_site_val']); $p++) {echo str_replace($bad_caract, $good_caract, $tbl_parc_already_assoc['cad_site_id'][$p]).';';}}?>'>
									<input type='hidden' name='parc_suppr' id='parc_suppr'>
									<input type='submit' name='valid_form' id='valid_form' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
								</td>
							</tr>
						</table>
					</div id='general'>
				</div id='onglet'>	
				<?php If (isset($par_id) && $par_id != '') {?>
				<script>
					jQuery(function($){
						addParc_js('explorateur', 'onglet');
					});
				</script>
				<?php } ?>
				<div id="explorateur" class="explorateur_span" style="display: none;">
					<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
						<tbody>
							<tr>
								<td>
									<div id="explorateur_new" style="top: 106px;">
										<table class="explorateur_contenu" cellspacing="5" cellpadding="5" border="0" align="center" frame="box" rules="NONE" width='100%'>
											<tbody>
												<tr>
													<td style="text-align:right;">
														<img width="15px" onclick="cacheMess('explorateur');cacheMess('explorateur_alerte');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
													</td>
												</tr>
												<tr>
													<td class="label">
														<form id="frm_parc_add" onsubmit="return false">
															<table CELLSPACING = '0' CELLPADDING ='3' border='0' ALIGN='CENTER' width='100%'>
																<tr>
																	<td style='text-align:center'>Parcelles disponibles</td>
																	<td>&nbsp;</td>
																	<td style='text-align:center'>Parcelles associées à l'acte</td>
																</tr>
																<tr>
																	<td width='50%'>
																		<select style='width:100%;height:380px' class='info_contenu' multiple='multiple' id='parc_dispo' name='parc_dispo[]'>";
																			<?php 
																			For ($p=0; $p<count($tbl_parc_dispo['cad_site_id']); $p++) {
																				If (strpos($tbl_parc_dispo['cad_site_val'][$p], 'Convention') > 0) {
																					echo "<option style='color:#d77a0f;' value='".$tbl_parc_dispo['cad_site_id'][$p]."'>".$tbl_parc_dispo['cad_site_val'][$p]."</option>";

																				} Else {
																					echo "<option style='color:#004494;' value='".$tbl_parc_dispo['cad_site_id'][$p]."'>".$tbl_parc_dispo['cad_site_val'][$p]."</option>";
																				}
																			}
																			?>
																		</select>
																	</td>
																	<td width='50px' align='center'>
																		<label class='prec_suiv' onclick="parcAjoutSuppr('frm_parc_add', 'ajout');">
																			>>>
																		</label>
																		<br>
																		<br>
																		<br>
																		<label class='prec_suiv' onclick="parcAjoutSuppr('frm_parc_add', 'suppr');">
																			<<<
																		</label>																	
																	</td>
																	<td width='50%'>
																		<select style='width:100%;height:380px' multiple='multiple' class='info_contenu' id='parc_assoc_acte' name='parc_assoc_acte[]'>";
																		<?php 
																			If (isset($tbl_parc_already_assoc['cad_site_id'])) {
																				For ($p=0; $p<count($tbl_parc_already_assoc['cad_site_id']); $p++) {
																					echo "<option class='parc_evidence' value='".$tbl_parc_already_assoc['cad_site_id'][$p]."'>".$tbl_parc_already_assoc['cad_site_val'][$p]."
																					</option>";
																				}
																			}
																		?>
																		</select>
																	</td>
																</tr>
															</table>
														</form>
													</td>
												</tr>
												<tr>
													<td class='save'>
														<input id='btn_parcModify' name='btn_parcModify' style='background:url(../images/valider.png) no-repeat scroll 5px 5px;box-shadow: 10px 10px 20px 0 #6c6c6b;box-shadow: 0 0 15px 3px #6c6c6b;border-radius:20px;' type="button" onclick="cacheMess('explorateur');cacheMess('explorateur_alerte');changeOpac(100, 'onglet');">
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div id="explorateur_alerte" class="lib_alerte" style="display: none; top: 271px;">
										<b>Veuillez saisir une donnée</b>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</form>
		</section>
		<?php 
			include("../includes/footer.php");
		?>
		<script>
			jQuery(function($){
				$.datepicker.setDefaults($.datepicker.regional['fr']);
				$('.datepicker').datepicker({
					dateFormat : 'dd/mm/yy',
					changeMonth: true,
					changeYear: true,
					onSelect: function(date){
						if(this.id == 'acte_date_fin') {
							$('#acte_date_debut').datepicker('option', 'maxDate', date);
						} else if(this.id == 'acte_date_debut'){
							$('#acte_date_fin').datepicker('option', 'minDate', date);
						}
						if(this.id == 'acte_date_signature') {
							$('#acte_date_delib_ca').datepicker('option', 'maxDate', date);
						} else if(this.id == 'acte_date_delib_ca'){
							$('#acte_date_signature').datepicker('option', 'minDate', date);
						}
					}
				});
			});
		</script>
		<?php
			$res_parc_dispo->closeCursor();
			$res_check_grant->closeCursor();
			$bdd = null;
		?>
	</body>
</html>
