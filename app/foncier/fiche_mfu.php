<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">	
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/toggle_tr.css">	
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/explorateur.css">	
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/jquery-ui.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script>
			$(document).ready( function () {
				chgStatut();
				chgReconduction('');
				$('#main').find('textarea').each(function(){
					resizeTextarea($(this).attr("id"));
				});
				resize_fieldset('general');
				$(window).focus(function(){
					if ($('#acte_notaire')) {
						ajaxReloadElt('acte_notaire')
					}
				});
			} ) ;

			jQuery(function($){
				$.datepicker.setDefaults($.datepicker.regional['fr']);
				$('.datepicker').datepicker({
					dateFormat : 'dd/mm/yy',
					changeMonth: true,
					changeYear: true,
					onSelect: function(date){
						if(this.id == 'acte_date_fin') {
							$('#acte_date_debut').datepicker('option', 'maxDate', date);
						} else if(this.id == 'acte_date_debut'){
							$('#acte_date_fin').datepicker('option', 'minDate', date);
						}
						if(this.id == 'acte_date_signature') {
							$('#acte_date_delib_ca').datepicker('option', 'maxDate', date);
							$('#acte_date_promesse').datepicker('option', 'maxDate', date);
							$('#acte_date_compromis').datepicker('option', 'maxDate', date);
						} else if(this.id == 'acte_date_delib_ca'){
							$('#acte_date_signature').datepicker('option', 'minDate', date);
						} else if(this.id == 'acte_date_promesse'){
							$('#acte_date_signature').datepicker('option', 'minDate', date);
						} else if(this.id == 'acte_date_compromis'){
							$('#acte_date_signature').datepicker('option', 'minDate', date);
						}
					}
				});
				if ($("#acte_date_rappel").length) {
					if ($('#acte_date_rappel').val() == 'N.D.') {
						$('#acte_date_rappel').datepicker('option', 'minDate', '0');
						$('#acte_date_rappel').val('N.D.');
					} else if ($('#acte_date_signature').val() == 'N.D.') {
						$('#acte_date_signature').datepicker('option', 'maxDate', '0');
						$('#acte_date_signature').val('N.D.');
					}
				} else {
					if ($('#acte_date_signature').datepicker("getDate") && $('#acte_date_signature').val() != 'N.D.'){
						$('#acte_date_signature').datepicker('option', 'maxDate', '0'); 
						$('#acte_date_delib_ca').datepicker('option', 'maxDate', $('#acte_date_signature').datepicker("getDate"));
						$('#acte_date_promesse').datepicker('option', 'maxDate', $('#acte_date_signature').datepicker("getDate"));
						$('#acte_date_compromis').datepicker('option', 'maxDate', $('#acte_date_signature').datepicker("getDate"));
					} else if ($('#acte_date_signature').val() == 'N.D.') {
						$('#acte_date_signature').datepicker('option', 'maxDate', '0');
						$('#acte_date_signature').val('N.D.');
						if ($('#acte_date_delib_ca').val() == 'N.D.') { 
							$('#acte_date_delib_ca').datepicker('option', 'maxDate', '0');
							$('#acte_date_delib_ca').val('N.D.');
						} else { 
							$('#acte_date_delib_ca').datepicker('option', 'maxDate', '0');
						}
						if ($('#acte_date_promesse').val() == 'N.D.') { 
							$('#acte_date_promesse').datepicker('option', 'maxDate', '0'); 
							$('#acte_date_promesse').val('N.D.'); 
						} else { 
							$('#acte_date_promesse').datepicker('option', 'maxDate', '0'); 
						} 
						if ($('#acte_date_compromis').val() == 'N.D.') { 
							$('#acte_date_compromis').datepicker('option', 'maxDate', '0'); 
							$('#acte_date_compromis').val('N.D.'); 
						} else { 
							$('#acte_date_compromis').datepicker('option', 'maxDate', '0');
						} 
					}
					if ($('#acte_date_delib_ca').datepicker("getDate") && $('#acte_date_delib_ca').val() != 'N.D.'){
						if ($('#acte_date_signature').val() == 'N.D.') { 
							$('#acte_date_signature').datepicker('option', 'minDate', $('#acte_date_delib_ca').datepicker("getDate")); 
							$('#acte_date_signature').val('N.D.'); 
						} else { 
							$('#acte_date_signature').datepicker('option', 'minDate', $('#acte_date_delib_ca').datepicker("getDate"));
						}
					}					
				}
			});
		</script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php
			include('../includes/valide_acces.php'); //intégration da la page contenant toutes les fonctions permettant la validation de l'accès
			$verif_role_acces = role_access(array('grp_sig', 'grp_foncier')); //variable permettant de définir quels utilisateurs peuvent passer en mode avancé(édition)
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}					
			$bad_caract = array("-","–","’", "'", "", "\t", "	");
			$good_caract = array("–","-","''", "''", "", "", "");
			
			If (isset($_GET['d']) && substr_count(base64_decode($_GET['d']), ':') > 0 && substr_count(base64_decode($_GET['d']), ';') > 0) {
				$d= base64_decode($_GET['d']);
				$min_crit = 0;
				$max_crit = strpos($d, ':');
				$min_val = strpos($d, ':') + 1;
				$max_val = strpos($d, ';') - $min_val;
				
				For ($p=0; $p<substr_count($d, ':'); $p++) {
					$crit = substr($d, $min_crit, $max_crit);
					$value = substr($d, $min_val, $max_val);
					$$crit = $value;
					$min_crit = strpos($d, ';', $min_crit) + 1;
					$max_crit= strpos($d, ':', $min_crit) - $min_crit;
					$min_val = $min_crit + $max_crit + 1;
					$max_val = strpos($d, ';', $min_val) - $min_val;
				}
				If (!isset($type_acte) OR !isset($mode) OR !isset($acte_id)) {
					$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
					include('../includes/prob_tech.php');
				}
				/* Création du lien utilisé dans la page includes/header.php permettant de passer du mode consultation au mode édition et inversement
				Pour cela il faut récréer la variable GET 'd' en modifiant le mode */
				If ($mode=='consult') //Si le mode en cours est consultation 
					{$vget_cnx = "mode:edit;";} //alors on propose de passer en édition
				Else //Sinon le mode en cours est édition
					{$vget_cnx = "mode:consult;";} //on propose donc de passer en consultation
				$vget_cnx .= "type_acte:" . $type_acte . ";"; //on ajouteà la variable GET 'd' les autres informations nécessaires
			} Else {
				$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
				include('../includes/prob_tech.php');
			}
						
			$h2 ='Foncier'; //varible stockant le titre h2 de la page et utiliser dans la page header intégreée juste après
			
			include('foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
			include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
			
			If ($type_acte == 'Acquisition') {
				$suffixe = 'mf';				
			} Else If ($type_acte == 'Convention') {
				$suffixe = 'mu';
			}
			
			$rq_check_grant = "
			SELECT 
				array_to_string(array_agg(grant_elt_id), ',') as lst_elt_id,
				array_to_string(array_agg(grant_elt_form_id), ',') as lst_elt_form_id
			FROM 
				foncier.grant_group 
				JOIN foncier.d_grant_elt USING (grant_elt_id)
			WHERE 
				grant_group_id IN ('".implode("','", $_SESSION['grp'])."') AND 
				grant_group_droit = 't' AND
				grant_elt_type_acte IN ('acte', '".$suffixe."')";			
			$res_check_grant = $bdd->prepare($rq_check_grant);
			$res_check_grant->execute();
			$lst_grant_elt_temp = $res_check_grant->fetch();
			$tbl_grant_elt_form_id = explode (',', $lst_grant_elt_temp[1]);
			$tbl_grant_elt_id = explode (',', $lst_grant_elt_temp[0]);
		
			If (isset($do) && $do == 'maj' && $verif_role_acces == 'OUI') {//La variable 'do' récupérée en décryptant la variable GET 'd' doit être 'maj' et l'utilisateur doit avoir les droits nécéssaires pour traiter le code ci-après
				If (isset($type_maj) && $type_maj == 'update') {
					$aujourdhui = date('Y-m-d H:i:s');				
					$form_modif = 'NON';
					If (isset($_POST['cur_info_mfu_elt_name']) && isset($_POST['cur_info_mfu_elt_val'])) {
						$cur_info_mfu_elt_name = explode("*;*", $_POST['cur_info_mfu_elt_name']);
						$cur_info_mfu_elt_val = explode("*;*", $_POST['cur_info_mfu_elt_val']);
						
						For ($i=0; $i<count($cur_info_mfu_elt_name); $i++) {
							$elt_name = $cur_info_mfu_elt_name[$i];
							$elt_val = $cur_info_mfu_elt_val[$i];
							If (isset($_POST[$elt_name]) && substr($elt_name, 0, 9) != 'acte_chk_' && substr($elt_name, 0, 10) != 'acte_date_'){
								$elt_new_val = $_POST[$elt_name];
								If (utf8_encode($elt_val) != utf8_encode($elt_new_val)) {
									$form_modif = 'OUI';
								}
							} Else If (substr($elt_name, 0, 9) == 'acte_chk_') {
								If (isset($_POST[$elt_name])) {
									$elt_new_val = 't';
								} Else {
									$elt_new_val = 'f';
								}
								If (utf8_encode($elt_val) != utf8_encode($elt_new_val)) {
									$form_modif = 'OUI';
								}
							} Else If (isset($_POST[$elt_name]) && substr($elt_name, 0, 10) == 'acte_date_') {
								$elt_new_val =($_POST[$elt_name]);
								If (utf8_encode($elt_val) != utf8_encode($elt_new_val)) {
									$form_modif = 'OUI';
								}
							}
						}
					}
					If ($form_modif == 'OUI') {				
						If (isset($_POST['need_acte_lib'])) {$need_acte_lib = str_replace($bad_caract, $good_caract, $_POST['need_acte_lib']);}
						If (isset($_POST['need_acte_type'])) {$need_acte_type = str_replace($bad_caract, $good_caract, $_POST['need_acte_type']);}
						If (isset($_POST['need_acte_statut'])) {$need_acte_statut = str_replace($bad_caract, $good_caract, $_POST['need_acte_statut']);}
						If (isset($_POST['acte_date_signature'])) {$acte_date_signature = str_replace('/', '-', date_transform_inverse($_POST['acte_date_signature']));}
						If (isset($_POST['acte_chk_clauses'])) {$acte_clauses = 'TRUE';} Else {$acte_clauses = 'FALSE';}
						If (isset($_POST['acte_txt_com'])) {$acte_txt_com = str_replace($bad_caract, $good_caract, $_POST['acte_txt_com']);}
						
						If ($type_acte == 'Acquisition') {							
							If (isset($_POST['acte_date_compromis'])) {$acte_date_compromis = str_replace('/', '-', date_transform_inverse($_POST['acte_date_compromis']));}
							If (isset($_POST['acte_date_promesse'])) {$acte_date_promesse = str_replace('/', '-', date_transform_inverse($_POST['acte_date_promesse']));}
							If (isset($_POST['acte_date_delib_ca'])) {$acte_date_delib_ca = str_replace('/', '-', date_transform_inverse($_POST['acte_date_delib_ca']));}
							If (isset($_POST['acte_notaire'])) {$acte_notaire = str_replace($bad_caract, $good_caract, $_POST['acte_notaire']);}
							If (isset($_POST['acte_notaire_contact'])) {$acte_notaire_contact = str_replace($bad_caract, $good_caract, $_POST['acte_notaire_contact']);}
							If (isset($_POST['acte_chk_recep_courrier'])) {$acte_recep_courrier = 'TRUE';} Else {$acte_recep_courrier = 'FALSE';}
							If (isset($_POST['acte_date_recep_courrier'])) {$acte_date_recep_courrier = str_replace('/', '-', date_transform_inverse($_POST['acte_date_recep_courrier']));}
							
							$rq_update_acte = "UPDATE foncier.mf_acquisitions SET ";
							$rq_update_statut = "INSERT INTO foncier.r_mfstatut (mf_id, statutmf_id, date) VALUES ";
							If (isset($acte_date_compromis)) {$rq_update_acte .= "date_compromis_pv_proprio = COALESCE(NULLIF('$acte_date_compromis', ''), 'N.D.'), ";}
							If (isset($acte_date_promesse)) {$rq_update_acte .= "date_compromis_pa_president = COALESCE(NULLIF('$acte_date_promesse', ''), 'N.D.'), ";}
							If (isset($acte_date_delib_ca)) {$rq_update_acte .= "date_delib_ca = COALESCE(NULLIF('$acte_date_delib_ca', ''), 'N.D.'), ";}
							If (isset($acte_date_signature)) {$rq_update_acte .= "date_sign_acte = COALESCE(NULLIF('$acte_date_signature', ''), 'N.D.'), ";}
							If (isset($acte_notaire) && $acte_notaire != '-1') {$rq_update_acte .= "notaire_id = $acte_notaire, ";}
							If (isset($acte_notaire_contact)) {$rq_update_acte .= "notaire_contact = COALESCE(NULLIF('$acte_notaire_contact', ''), 'N.D.'), ";}
							If (isset($acte_recep_courrier)) {$rq_update_acte .= "mf_courier_recep_notaire = '$acte_recep_courrier', ";}
							If (isset($acte_date_recep_courrier)) {$rq_update_acte .= "mf_courier_recep_notaire_date = COALESCE(NULLIF('$acte_date_recep_courrier', ''), 'N.D.'), ";}
						} Else If ($type_acte == 'Convention') {							
							If (isset($_POST['acte_date_debut'])) {$acte_date_debut = str_replace('/', '-', date_transform_inverse($_POST['acte_date_debut']));}
							If (isset($_POST['acte_date_fin'])) {$acte_date_fin = str_replace('/', '-', date_transform_inverse($_POST['acte_date_fin']));}
							If (isset($_POST['acte_duree'])) {$acte_duree = $_POST['acte_duree'];}
							If (isset($_POST['acte_chk_reconduction'])) {$acte_reconduction = 'TRUE';} Else {$acte_reconduction = 'FALSE';}

							If ($acte_reconduction == 'TRUE') {
								If (isset($_POST['acte_chk_infini'])) {
									$acte_recond_nbmax = -1;
								} Else {
									$acte_recond_nbmax = $_POST['acte_recond_nbmax'];
								}
								If (isset($_POST['acte_pas_tps'])) {$acte_pas_tps = $_POST['acte_pas_tps'];}
								If (isset($_POST['acte_date_rappel'])) {$acte_date_rappel = str_replace('/', '-', date_transform_inverse($_POST['acte_date_rappel']));}
							} Else {
								$acte_recond_nbmax = 0;
								$acte_pas_tps = 0;
								$acte_date_rappel = 'N.D.';
							}
							
							If (isset($_POST['acte_chk_envoi_courrier'])) {$acte_envoi_courrier = 'TRUE';} Else {$acte_envoi_courrier = 'FALSE';}
							If (isset($_POST['acte_date_envoi_courrier'])) {$acte_date_envoi_courrier = str_replace('/', '-', date_transform_inverse($_POST['acte_date_envoi_courrier']));}
							
							$rq_update_acte = "UPDATE foncier.mu_conventions SET ";
							$rq_update_statut = "INSERT INTO foncier.r_mustatut (mu_id, statutmu_id, date) VALUES ";							
							If (isset($acte_date_debut) && in_array('acte_date_debut', $tbl_grant_elt_form_id)) {$rq_update_acte .= "date_effet_conv = COALESCE(NULLIF('$acte_date_debut', ''), 'N.D.'), ";}
							If (isset($acte_date_fin) && in_array('acte_date_fin', $tbl_grant_elt_form_id)) {$rq_update_acte .= "date_fin_conv = COALESCE(NULLIF('$acte_date_fin', ''), 'N.D.'), ";}
							If (isset($acte_date_signature) && in_array('acte_date_signature', $tbl_grant_elt_form_id)) {$rq_update_acte .= "date_sign_conv = COALESCE(NULLIF('$acte_date_signature', ''), 'N.D.'), ";}
							If (isset($acte_duree) && in_array('acte_duree', $tbl_grant_elt_form_id)) {$rq_update_acte .= "mu_duree = $acte_duree, ";}
							If (isset($acte_reconduction) && in_array('acte_chk_reconduction', $tbl_grant_elt_form_id)) {$rq_update_acte .= "recond_tacite = '$acte_reconduction', ";}
							If (isset($acte_recond_nbmax) && in_array('acte_recond_nbmax', $tbl_grant_elt_form_id)) {$rq_update_acte .= "recond_nbmax = $acte_recond_nbmax, ";}
							If (isset($acte_pas_tps) && in_array('acte_pas_tps', $tbl_grant_elt_form_id)) {$rq_update_acte .= "recond_duree = $acte_pas_tps, ";}
							If (isset($acte_date_rappel) && in_array('acte_date_rappel', $tbl_grant_elt_form_id)) {$rq_update_acte .= "recond_daterappel = COALESCE(NULLIF('$acte_date_rappel', ''), 'N.D.'), ";}
							If (isset($acte_envoi_courrier) && in_array(10, $tbl_grant_elt_id)) {$rq_update_acte .= "mu_courier_envoi_proprio = '$acte_envoi_courrier', ";}
							If (isset($acte_date_envoi_courrier) && in_array(10, $tbl_grant_elt_id)) {$rq_update_acte .= "mu_courier_envoi_proprio_date = COALESCE(NULLIF('$acte_date_envoi_courrier', ''), 'N.D.'), ";}
						}
						
						/* Création de la requête SQL de mise à jour des données relatives aux actes
						L'existance d'une valeur pour les élements du formulaire est testée avant d'intégrer la mise à jour du champ */
						If (isset($need_acte_lib) && in_array('need_acte_lib', $tbl_grant_elt_form_id)) {$rq_update_acte .= $suffixe."_lib = '$need_acte_lib', ";}
						If (isset($need_acte_type) && in_array('need_acte_type', $tbl_grant_elt_form_id)) {$rq_update_acte .= "typ".$suffixe."_id = '$need_acte_type', ";}
						If (isset($acte_clauses) && in_array('acte_chk_clauses', $tbl_grant_elt_form_id)) {$rq_update_acte .= $suffixe."_clauses_part = '$acte_clauses', ";}
						If (isset($acte_txt_com) && in_array('acte_txt_com', $tbl_grant_elt_form_id)) {$rq_update_acte .= $suffixe."_commentaires = '$acte_txt_com', ";}
												
						$rq_update_acte .= " maj_date = '$aujourdhui', maj_user = '$_SESSION[login]'  WHERE ".$suffixe."_id = '$acte_id'"; //intégration de la clause WHERE à la requête SQL
						$res_update_acte = $bdd->prepare($rq_update_acte); //prépare la reqète à l'execution
						$res_update_acte->execute(); //execution de la requete
						$res_update_acte->closeCursor(); //fermeture du curseur
						
						If (isset($_POST['need_acte_statut']) && in_array('need_acte_statut', $tbl_grant_elt_form_id)) {
							$rq_update_statut .= " ('$acte_id', $need_acte_statut, '$aujourdhui')"; //intégration de la clause WHERE à la requête SQL
							$res_update_statut = $bdd->prepare($rq_update_statut); //prépare la reqète à l'execution
							$res_update_statut->execute(); //execution de la requete
							$res_update_statut->closeCursor(); //fermeture du curseur	
						}
						
						If (in_array('acte_frais_', $tbl_grant_elt_form_id)) {
							$rq_lst_type_frais = 
							"SELECT d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_id as id, d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_lib as lib FROM foncier.d_typfrais" . $suffixe . " ORDER BY id";
							$res_lst_type_frais = $bdd->prepare($rq_lst_type_frais);
							$res_lst_type_frais->execute();
							While ($donnees = $res_lst_type_frais->fetch()) { 
								$f = $donnees['id'];
								If (isset($_POST['acte_frais_'.$f])) {
									$acte_frais[$f] = str_replace(',', '.', $_POST['acte_frais_'.$f]);
								}
								If (isset($acte_frais[$f])) {
									$rq_verif_frais = "SELECT " . $suffixe . "frais FROM foncier.r_frais_" . $suffixe . " WHERE " . $suffixe . "_id = '$acte_id' AND typfrais" . $suffixe . "_id = $f";
									$res_verif_frais = $bdd->prepare($rq_verif_frais);
									$res_verif_frais->execute();
									If ($res_verif_frais->rowCount() == 0 && $acte_frais[$f] > 0){
										$rq_insert_frais = "INSERT INTO foncier.r_frais_" . $suffixe . " (" . $suffixe . "_id, typfrais" . $suffixe . "_id, " . $suffixe . "frais) VALUES ('$acte_id', $f, $acte_frais[$f])";
										$res_insert_frais = $bdd->prepare($rq_insert_frais);
										$res_insert_frais->execute();
										$res_insert_frais->closeCursor();
									} Else If ($res_verif_frais->rowCount() == 1 && $acte_frais[$f] == 0){
										$rq_delete_frais = "DELETE FROM foncier.r_frais_" . $suffixe . " WHERE " . $suffixe . "_id = '$acte_id' AND typfrais" . $suffixe . "_id = $f";
										$res_delete_frais = $bdd->prepare($rq_delete_frais);
										$res_delete_frais->execute();
										$res_delete_frais->closeCursor();
									} Else If ($res_verif_frais->rowCount() == 1 && $acte_frais[$f] > 0){
										$rq_update_frais = "UPDATE foncier.r_frais_" . $suffixe . " SET " . $suffixe . "frais = $acte_frais[$f] WHERE " . $suffixe . "_id = '$acte_id' AND typfrais" . $suffixe . "_id = $f";
										$res_update_frais = $bdd->prepare($rq_update_frais);
										$res_update_frais->execute();
										$res_update_frais->closeCursor();
									}
									$res_verif_frais->closeCursor();
								}
							}
							$res_lst_type_frais->closeCursor();
						}
					}
					
					If (in_array('_acte_pdf', $tbl_grant_elt_form_id)) {
						If (isset($_POST['remove_acte_pdf']) && $_POST['remove_acte_pdf'] != 'removed') {
							$actes_extensions_valides = array('pdf');
							$acte_filename = $acte_id . '.pdf';
							If (GEST_ACT == 'web') {
								$actes_path = PATH_ACT;
								If ($_FILES['file_acte_pdf']['error'] == 0 && in_array(strtolower(substr(strrchr($_FILES['file_acte_pdf']['name'], '.'),1)), $actes_extensions_valides) && $_FILES['file_acte_pdf']['size'] < 2000000) {
									$transfert_pdf = move_uploaded_file($_FILES['file_acte_pdf']['tmp_name'], $actes_path . $acte_filename);
								}
							} Else If (GEST_ACT == 'file') {
								If (isset($_POST['acte_chk_pdf'])) {
									$transfert_pdf = True;
								} Else {
									$transfert_pdf = False;
								}
							}
							
							If (isset($transfert_pdf) && $transfert_pdf) {
								If ($type_acte == 'Acquisition') {
									$rq_update_acte_pdf = "UPDATE foncier.mf_acquisitions SET mf_pdf = '$acte_filename' WHERE mf_id = '$acte_id'";
									$rq_update_acte_last_maj = "UPDATE foncier.mf_acquisitions SET maj_date = '$aujourdhui', maj_user = '$_SESSION[login]'  WHERE mf_id = '$acte_id'";
								} Else If ($type_acte == 'Convention') {
									$rq_update_acte_pdf = "UPDATE foncier.mu_conventions SET mu_pdf = '$acte_filename' WHERE mu_id = '$acte_id'";
									$rq_update_acte_last_maj = "UPDATE foncier.mu_conventions SET maj_date = '$aujourdhui', maj_user = '$_SESSION[login]'  WHERE mu_id = '$acte_id'";
								}
								
								$res_update_acte_pdf = $bdd->prepare($rq_update_acte_pdf); //prépare la requête à l'execution
								$res_update_acte_pdf->execute(); //execution de la requete
								$res_update_acte_pdf->closeCursor(); //fermeture du curseur
								
								$res_update_acte_last_maj = $bdd->prepare($rq_update_acte_last_maj); //prépare la requête à l'execution
								$res_update_acte_last_maj->execute(); //execution de la requete
								$res_update_acte_last_maj->closeCursor(); //fermeture du curseur
							} Else If (isset($transfert_pdf) && !$transfert_pdf && GEST_ACT == 'file') {
								If ($type_acte == 'Acquisition') {
									$rq_update_acte_pdf = "UPDATE foncier.mf_acquisitions SET mf_pdf = NULL WHERE mf_id = '$acte_id'";
								} Else If ($type_acte == 'Convention') {
									$rq_update_acte_pdf = "UPDATE foncier.mu_conventions SET mu_pdf = NULL WHERE mu_id = '$acte_id'";
								}
								$res_update_acte_pdf = $bdd->prepare($rq_update_acte_pdf); //prépare la requête à l'execution
								$res_update_acte_pdf->execute(); //execution de la requete
								$res_update_acte_pdf->closeCursor(); //fermeture du curseur
							}
						} Else If (isset($_POST['remove_acte_pdf']) && $_POST['remove_acte_pdf'] == 'removed') {
							If (GEST_ACT == 'web') {
								unlink(PATH_ACT .$acte_id . '.pdf');
								If ($type_acte == 'Acquisition') {
									$rq_update_acte_pdf = "UPDATE foncier.mf_acquisitions SET mf_pdf = NULL WHERE mf_id = '$acte_id'";
								} Else If ($type_acte == 'Convention') {
									$rq_update_acte_pdf = "UPDATE foncier.mu_conventions SET mu_pdf = NULL WHERE mu_id = '$acte_id'";
								}
								$res_update_acte_pdf = $bdd->prepare($rq_update_acte_pdf); //prépare la requête à l'execution
								$res_update_acte_pdf->execute(); //execution de la requete
								$res_update_acte_pdf->closeCursor(); //fermeture du curseur
							}
						}
					}
				} Else If (isset($type_maj) && $type_maj == 'add') {
					$aujourdhui = date('Y-m-d H:i:s');	
					
					If (isset($_POST['need_acte_lib'])) {$need_acte_lib = str_replace($bad_caract, $good_caract, $_POST['need_acte_lib']);}
					If (isset($_POST['need_acte_type'])) {$need_acte_type = str_replace($bad_caract, $good_caract, $_POST['need_acte_type']);}
					If (isset($_POST['need_acte_statut']) && in_array('need_acte_statut', $tbl_grant_elt_form_id)) {$need_acte_statut = str_replace($bad_caract, $good_caract, $_POST['need_acte_statut']);} Else {$need_acte_statut = 0;}
					If (isset($_POST['acte_date_signature']) && $_POST['acte_date_signature'] != '' && in_array('acte_date_signature', $tbl_grant_elt_form_id)) {
						$acte_date_signature = str_replace('/', '-', date_transform_inverse($_POST['acte_date_signature']));
					} Else {
						$acte_date_signature = 'N.D.';
					}
					If (isset($_POST['acte_chk_clauses']) && in_array('acte_chk_clauses', $tbl_grant_elt_form_id)) {$acte_clauses = 'TRUE';} Else {$acte_clauses = 'FALSE';}
					If (isset($_POST['acte_txt_com']) && in_array('acte_chk_clauses', $tbl_grant_elt_form_id)) {$acte_txt_com = str_replace($bad_caract, $good_caract, $_POST['acte_txt_com']);} Else {$acte_txt_com = '';}
					
					$len_site_id = strlen($_POST['site_id']);
					If ($type_acte == 'Acquisition' && in_array(1, $tbl_grant_elt_id)) {
						$suffixe = 'mf';
						If (isset($_POST['acte_id']) && $_POST['acte_id'] != '') {
							$new_acte_id = $acte_id = $_POST['acte_id'];
						} Else {
							//définition du nouvel identifiant
							$rq_new_acte_id = "SELECT COALESCE(max(substring(mf_id from ($len_site_id+4) for (length(mf_id)-($len_site_id+3)))::numeric),0)+1 as suffixe_next_id FROM foncier.mf_acquisitions WHERE mf_id LIKE '%" . $_POST['site_id'] . "%'";
							$res_new_acte_id = $bdd->prepare($rq_new_acte_id);
							$res_new_acte_id->execute();
							$new_acte_id = $res_new_acte_id->fetch();
							$res_new_acte_id->closeCursor();
							$new_acte_id = 'MF_' . $_POST['site_id'] . $new_acte_id['suffixe_next_id'];							
						}
						$acte_id = $new_acte_id;
						
						If (isset($_POST['acte_date_compromis']) && $_POST['acte_date_compromis'] != '' && in_array('acte_date_compromis', $tbl_grant_elt_form_id)) {
							$acte_date_compromis = str_replace('/', '-', date_transform_inverse($_POST['acte_date_compromis']));
						} Else {
							$acte_date_compromis = 'N.D.';
						}
						If (isset($_POST['acte_date_promesse']) && $_POST['acte_date_promesse'] != '' && in_array('acte_date_promesse', $tbl_grant_elt_form_id)) {
							$acte_date_promesse = str_replace('/', '-', date_transform_inverse($_POST['acte_date_promesse']));
						} Else {
							$acte_date_promesse = 'N.D.';
						}
						If (isset($_POST['acte_date_delib_ca']) && $_POST['acte_date_delib_ca'] != '' && in_array('acte_date_delib_ca', $tbl_grant_elt_form_id)) {
							$acte_date_delib_ca = str_replace('/', '-', date_transform_inverse($_POST['acte_date_delib_ca']));
						} Else {
							$acte_date_delib_ca = 'N.D.';
						}
						If (isset($_POST['acte_notaire']) && $_POST['acte_notaire'] != '-1' && in_array('acte_notaire', $tbl_grant_elt_form_id)) {
							$acte_notaire = str_replace($bad_caract, $good_caract, $_POST['acte_notaire']);
						} Else {
							$acte_notaire = 'NULL';
						}
						If (isset($_POST['acte_notaire_contact']) && in_array('acte_notaire', $tbl_grant_elt_form_id)) {$acte_notaire_contact = str_replace($bad_caract, $good_caract, $_POST['acte_notaire_contact']);} Else {$acte_notaire_contact = '';}
						If (isset($_POST['acte_chk_recep_courrier']) && in_array(10, $tbl_grant_elt_id)) {$acte_recep_courrier = 'TRUE';} Else {$acte_recep_courrier = 'FALSE';}
						If (isset($_POST['acte_date_recep_courrier']) && $_POST['acte_date_recep_courrier'] != '' && in_array(10, $tbl_grant_elt_id)) {
							$acte_date_recep_courrier = str_replace('/', '-', date_transform_inverse($_POST['acte_date_recep_courrier']));
						} Else {
							$acte_date_recep_courrier = 'N.D.';
						}
						
						$acte_filename = '';

						$rq_insert_acte = "INSERT INTO foncier.mf_acquisitions (mf_id, mf_lib, typmf_id, date_compromis_pv_proprio, date_compromis_pa_president, date_delib_ca, date_sign_acte, notaire_id, notaire_contact, mf_courier_recep_notaire, mf_courier_recep_notaire_date, mf_clauses_part, mf_commentaires, mf_pdf, maj_user, maj_date) VALUES ('$new_acte_id', '$need_acte_lib', $need_acte_type, '$acte_date_compromis', '$acte_date_promesse', '$acte_date_delib_ca', '$acte_date_signature', $acte_notaire, '$acte_notaire_contact', '$acte_recep_courrier', '$acte_date_recep_courrier', '$acte_clauses', '$acte_txt_com', '$acte_filename', '$_SESSION[login]', '$aujourdhui')";
						$res_insert_acte = $bdd->prepare($rq_insert_acte); //prépare la reqète à l'execution
						$res_insert_acte->execute(); //execution de la requete
						$res_insert_acte->closeCursor(); //fermeture du curseur
						$rq_insert_statut = "INSERT INTO foncier.r_mfstatut (mf_id, statutmf_id, date) VALUES ('$new_acte_id', $need_acte_statut, '$aujourdhui')";
						$res_insert_statut = $bdd->prepare($rq_insert_statut); //prépare la reqète à l'execution
						$res_insert_statut->execute(); //execution de la requete
						$res_insert_statut->closeCursor(); //fermeture du curseur
												
						$tbl_parc_ajout = $_POST['parc_ajout'];
						If (substr($tbl_parc_ajout, -1, 1) == ';'){
							$tbl_parc_ajout = substr($tbl_parc_ajout, 0, strlen($tbl_parc_ajout)-1); //suppression de la dernière virgule	
						}
						$lst_parc_ajout = preg_split("/;/", $tbl_parc_ajout);
						$rq_ajout_parc = "INSERT INTO foncier.r_cad_site_mf (cad_site_id, mf_id, date_entree, date_sortie) VALUES ";
							For ($i = 0; $i<count($lst_parc_ajout); $i++) {
								$rq_ajout_parc .= "($lst_parc_ajout[$i], '$new_acte_id', '$aujourdhui', 'N.D.'), ";
							}
						If (substr($rq_ajout_parc, -2, 1) == ','){
							$rq_ajout_parc = substr($rq_ajout_parc, 0, strlen($rq_ajout_parc)-2); //suppression de la dernière virgule	
						}
						$res_ajout_parc = $bdd->prepare($rq_ajout_parc);
						$res_ajout_parc->execute();
						$res_ajout_parc->closeCursor();
					} Else If ($type_acte == 'Convention' && in_array(1, $tbl_grant_elt_id)) {
						$suffixe = 'mu';
						If (isset($_POST['acte_id']) && $_POST['acte_id'] != '') {
							$new_acte_id = $_POST['acte_id'];
						} Else {
							//définition du nouvel identifiant
							$rq_new_acte_id = "SELECT COALESCE(max(substring(mu_id from ($len_site_id+4) for (length(mu_id)-($len_site_id+3)))::numeric),0)+1 as suffixe_next_id FROM foncier.mu_conventions WHERE mu_id LIKE '%" . $_POST['site_id'] . "%'";
							$res_new_acte_id = $bdd->prepare($rq_new_acte_id);
							$res_new_acte_id->execute();
							$new_acte_id = $res_new_acte_id->fetch();
							$res_new_acte_id->closeCursor();
							$new_acte_id = 'MU_' . $_POST['site_id'] . $new_acte_id['suffixe_next_id'];							
						}
						$acte_id = $new_acte_id;
						
						If (isset($_POST['acte_date_debut']) && in_array('acte_date_debut', $tbl_grant_elt_form_id)) {
							$acte_date_debut = str_replace('/', '-', date_transform_inverse($_POST['acte_date_debut']));
						} Else {
							$acte_date_debut = 'N.D.';
						}
						If (isset($_POST['acte_date_fin']) && in_array('acte_date_fin', $tbl_grant_elt_form_id)) {
							$acte_date_fin = str_replace('/', '-', date_transform_inverse($_POST['acte_date_fin']));
						} Else {
							$acte_date_fin = 'N.D.';
						}
						If (isset($_POST['acte_duree']) && $_POST['acte_duree'] != '' && $_POST['acte_duree'] != 'N.D.' && in_array('acte_duree', $tbl_grant_elt_form_id)) {
							$acte_duree = $_POST['acte_duree'];
						} Else {
							$acte_duree = 0;
						}							
						If (isset($_POST['acte_chk_reconduction']) && in_array('acte_chk_reconduction', $tbl_grant_elt_form_id)) {$acte_reconduction = 'TRUE';} Else {$acte_reconduction = 'FALSE';}
						If ($acte_reconduction == 'TRUE') {
							If (isset($_POST['acte_chk_infini'])) {
								$acte_recond_nbmax = -1;
							} Else {
								If (isset($_POST['acte_recond_nbmax'])) {$acte_recond_nbmax = $_POST['acte_recond_nbmax'];}
							}
							If (isset($_POST['acte_pas_tps'])) {$acte_pas_tps = $_POST['acte_pas_tps'];}
						} Else {
							$acte_pas_tps = 0;
							$acte_recond_nbmax = 0;
							$date_rappel = 'N.D.';
						}
						If (isset($_POST['acte_date_rappel']) && in_array(array('acte_chk_reconduction', 'acte_date_rappel'), $tbl_grant_elt_form_id)) {
							$acte_date_rappel = str_replace('/', '-', date_transform_inverse($_POST['acte_date_rappel']));
						} Else {
							$acte_date_rappel = 'N.D.';
						}
						If (isset($_POST['acte_chk_envoi_courrier']) && in_array(10, $tbl_grant_elt_id)) {$acte_envoi_courrier = 'TRUE';} Else {$acte_envoi_courrier = 'FALSE';}
						If (isset($_POST['acte_date_envoi_courrier']) && $_POST['acte_date_envoi_courrier'] != '' && in_array(10, $tbl_grant_elt_id)) {
							$acte_date_envoi_courrier = str_replace('/', '-', date_transform_inverse($_POST['acte_date_envoi_courrier']));
						} Else {
							$acte_date_envoi_courrier = 'N.D.';
						}
						
						$acte_filename = '';
						
						$rq_insert_acte = "INSERT INTO foncier.mu_conventions (mu_id, mu_lib, typmu_id, date_sign_conv, date_effet_conv, date_fin_conv, mu_duree, recond_tacite, recond_nbmax, recond_duree, recond_daterappel, mu_courier_envoi_proprio, mu_courier_envoi_proprio_date, mu_clauses_part, mu_commentaires, mu_pdf, maj_user, maj_date) VALUES ('$new_acte_id', '$need_acte_lib', $need_acte_type, '$acte_date_signature', '$acte_date_debut', '$acte_date_fin', $acte_duree, '$acte_reconduction', $acte_recond_nbmax, $acte_pas_tps, '$acte_date_rappel', '$acte_envoi_courrier', '$acte_date_envoi_courrier', '$acte_clauses', '$acte_txt_com', '$acte_filename', '$_SESSION[login]', '$aujourdhui')";
						$res_update_acte = $bdd->prepare($rq_insert_acte); //prépare la reqète à l'execution
						$res_update_acte->execute(); //execution de la requete
						$res_update_acte->closeCursor(); //fermeture du curseur
						
						$rq_insert_statut = "INSERT INTO foncier.r_mustatut (mu_id, statutmu_id, date) VALUES ('$new_acte_id', $need_acte_statut, '$aujourdhui')";
						$res_insert_statut = $bdd->prepare($rq_insert_statut); //prépare la reqète à l'execution
						$res_insert_statut->execute(); //execution de la requete
						$res_insert_statut->closeCursor(); //fermeture du curseur
						
						$tbl_parc_ajout = $_POST['parc_ajout'];
						If (substr($tbl_parc_ajout, -1, 1) == ';'){
							$tbl_parc_ajout = substr($tbl_parc_ajout, 0, strlen($tbl_parc_ajout)-1); //suppression de la dernière virgule	
						}
						$lst_parc_ajout = preg_split("/;/", $tbl_parc_ajout);
						$rq_ajout_parc = "INSERT INTO foncier.r_cad_site_mu (cad_site_id, mu_id, date_entree, date_sortie) VALUES ";
							For ($i = 0; $i<count($lst_parc_ajout); $i++) {
								$rq_ajout_parc .= "($lst_parc_ajout[$i], '$new_acte_id', '$aujourdhui', 'N.D.'), ";
							}				
						If (substr($rq_ajout_parc, -2, 1) == ','){
							$rq_ajout_parc = substr($rq_ajout_parc, 0, strlen($rq_ajout_parc)-2); //suppression de la dernière virgule	
						}
						$res_ajout_parc = $bdd->prepare($rq_ajout_parc);
						$res_ajout_parc->execute();
						$res_ajout_parc->closeCursor();
					}
					$rq_lst_site_id = "
					SELECT 
						array_to_string(array_agg(DISTINCT site_id), ',') as tbl_site_id
					FROM
						foncier.cadastre_site
					WHERE cad_site_id IN (".implode(",", $lst_parc_ajout).")";
					// echo $rq_lst_site_id;
					$res_lst_site_id = $bdd->prepare($rq_lst_site_id);
					$res_lst_site_id->execute();
					$lst_site_id_temp = $res_lst_site_id->fetch();
					$res_lst_site_id->closeCursor();
					$lst_site_id = $lst_site_id_temp['tbl_site_id'];
					
					If (in_array('acte_frais_', $tbl_grant_elt_form_id)) {
						$rq_nbr_frais = "SELECT DISTINCT typfrais".$suffixe."_id FROM foncier.d_typfrais".$suffixe."";
						$res_nbr_frais = $bdd->prepare($rq_nbr_frais);
						$res_nbr_frais->execute();
						$res_nbr_frais->closeCursor();
						For ($f=1; $f<=$res_nbr_frais->rowCount(); $f++) {
							$g = $f-1;
							If (isset($_POST['acte_frais_'.$f])) {$acte_frais[$f] = str_replace(',', '.', $_POST['acte_frais_'.$f]);}
							If (isset($acte_frais[$f])) {
								If ($acte_frais[$f] > 0){
									$rq_insert_frais = "INSERT INTO foncier.r_frais_".$suffixe." (".$suffixe."_id, typfrais".$suffixe."_id, ".$suffixe."frais) VALUES ('$new_acte_id', $f, $acte_frais[$f])";
									$res_insert_frais = $bdd->prepare($rq_insert_frais);
									$res_insert_frais->execute();
									$res_insert_frais->closeCursor();
								}							
							}
						}
					}					
				}
				//Section destinée au rafraichissement aux vues matérialisées
				try {
					$refresh_foncier = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, 'foncier', MDP_FONCIER);
				}
				catch (Exception $e) {
					die(include('../includes/prob_tech.php'));
				}
				$rq_refresh_materialized_view = "REFRESH MATERIALIZED VIEW alertes.v_tdb_mfu";
				$res_refresh_materialized_view = $refresh_foncier->prepare($rq_refresh_materialized_view);
				$res_refresh_materialized_view->execute();
				$res_refresh_materialized_view->closeCursor();
				$rq_refresh_materialized_view = "REFRESH MATERIALIZED VIEW sites.v_sites_parcelles";
				$res_refresh_materialized_view = $refresh_foncier->prepare($rq_refresh_materialized_view);
				$res_refresh_materialized_view->execute();
				$res_refresh_materialized_view->closeCursor();
				$rq_update_geom_mfu = "
				WITH t1 AS (
					SELECT site_id, st_multi(st_union(t1.geom))::geometry(MultiPolygon,2154) AS geom
					FROM sites.v_sites_parcelles t1
					WHERE t1.site_id IN ('".str_replace(",", "','", $lst_site_id)."') AND t1.mfu <> ALL (ARRAY['Autre'::text, 'Pas de maîtrise'::text, 'Projet en cours'::text, 'Ces données ne sont pas à jour'::text])
					GROUP BY site_id
					)
				UPDATE sites.sites SET geom_mfu = t1.geom FROM t1 WHERE sites.site_id = t1.site_id";
				// echo $rq_update_geom_mfu;
				$res_update_geom_mfu = $refresh_foncier->prepare($rq_update_geom_mfu); //prépare la requête à l'execution
				$res_update_geom_mfu->execute(); //execution de la requete
				$res_update_geom_mfu->closeCursor(); //fermeture du curseur
				
				//redirection
				If ($do == 'maj' && $type_maj == 'add') {
					$vget_cnx = "mode:consult;";
					$vget_cnx .= "type_acte:" . $type_acte . ";";
					$vget_cnx .= "acte_id:" . $acte_id . ";";
					$vget_crypt_cnx = base64_encode($vget_cnx);
					$lien_cnx = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_mfu.php?d=$vget_crypt_cnx#general";
				?>
					<script type='text/javascript'>window.location.replace('<?php echo $lien_cnx; ?>');</script>
				<?php
				}
			}
			
			$vget_cnx .= "acte_id:" . $acte_id . ";"; //on ajouteà la variable GET 'd' les autres informations nécessaires
			$vget_crypt_cnx = base64_encode($vget_cnx); //on code la variable GET 'd' en base64
			$lien_cnx = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_mfu.php?d=$vget_crypt_cnx#general"; //création du lien utile dans le header			
			include('../includes/header.php'); //intégration de la page affichant l'entête nécessaire dans toutes les pages du site
			include('../includes/breadcrumb.php');
			//include('../includes/construction.php');
			
			If ($type_acte == 'Acquisition') { $suffixe = 'mf'; } Else If ($type_acte == 'Convention') { $suffixe = 'mu'; }
			$rq_statut_mfu = 
			"SELECT 
				r_$suffixe"."statut.statut"."$suffixe"."_id AS statut_id
			FROM
				foncier.r_"."$suffixe"."statut 
				JOIN (SELECT r_"."$suffixe"."statut."."$suffixe"."_id, Max(r_"."$suffixe"."statut.date) AS statut_date FROM foncier.r_"."$suffixe"."statut GROUP BY r_"."$suffixe"."statut."."$suffixe"."_id) AS tmax ON (r_"."$suffixe"."statut.date = tmax.statut_date)
			WHERE
				r_"."$suffixe"."statut."."$suffixe"."_id = :acte_id
			";
			$res_statut_mfu = $bdd->prepare($rq_statut_mfu);
			$res_statut_mfu->execute(array('acte_id'=>$acte_id));
			$statut_mfu = $res_statut_mfu->fetch();
			$res_statut_mfu->closeCursor();
			
			If ($type_acte == 'Convention' && ($statut_mfu['statut_id'] == 2 || $statut_mfu['statut_id'] == 4)) {
				$rq_dates_mu = 
				"SELECT date_effet_conv, date_fin_conv
				FROM foncier.mu_conventions
				WHERE mu_id = :acte_id";
				$res_dates_mu = $bdd->prepare($rq_dates_mu);
				$res_dates_mu->execute(array('acte_id'=>$acte_id));
				$dates_mu = $res_dates_mu->fetch();
				$res_dates_mu->closeCursor();
			}
			
			If ($type_acte == 'Acquisition') {
				$suffixe = 'mf';
				$rq_info_mfu = 
				"SELECT 
					array_to_string(array_agg(CASE WHEN tsyn2.site_id IS NOT NULL THEN tsyn2.site_id ELSE 'N.D.' END), ',') as tbl_site_id, 
					array_to_string(array_agg(CASE WHEN tsyn2.site_nom IS NOT NULL THEN tsyn2.site_nom ELSE 'N.D.' END), ',') as tbl_site_nom, 
					'Acquisition'::character varying(15) as acte, 
					mf_acquisitions.mf_id, mf_acquisitions.mf_lib as mfu_lib, mf_acquisitions.typmf_id as typmfu_id, d_typmf.typmf_lib as typmfu_lib, 
					d_statutmf.statutmf_id as statutmfu_id, d_statutmf.statutmf as statutmfu_lib, mf_acquisitions.date_compromis_pv_proprio,
					mf_acquisitions.date_compromis_pa_president, mf_acquisitions.date_delib_ca, mf_acquisitions.date_sign_acte, 
					sum(CASE WHEN tsyn.nbr_parcelle IS NOT NULL THEN tsyn.nbr_parcelle ELSE 0 END) as nbr_parcelle, 
					sum(CASE WHEN tsyn.surf_pa_total IS NOT NULL THEN tsyn.surf_pa_total ELSE 0 END) as surf_pa_total, 
					sum(CASE WHEN tsyn.nbr_lot IS NOT NULL THEN tsyn.nbr_lot ELSE 0 END) as nbr_lot, 
					sum(CASE WHEN tsyn.surf_lo_total IS NOT NULL THEN tsyn.surf_lo_total ELSE 0 END) as surf_lo_total, 
					sum(CASE WHEN tsyn.nbr_cad_cen IS NOT NULL THEN tsyn.nbr_cad_cen ELSE 0 END) as nbr_cad_cen, 
					sum(CASE WHEN tsyn.nbr_cad_site IS NOT NULL THEN tsyn.nbr_cad_site ELSE 0 END) as nbr_cad_site, 
					sum(CASE WHEN tsyn.surf_lo_reel IS NOT NULL THEN tsyn.surf_lo_reel ELSE 0 END) as surf_lo_reel,
					tfrais.frais_id, tfrais.frais_typ, tfrais.frais, mf_acquisitions.mf_clauses_part as mfu_clauses_part, 
					mf_acquisitions.mf_courier_recep_notaire, mf_acquisitions.mf_courier_recep_notaire_date, mf_acquisitions.mf_commentaires as mfu_commentaires, mf_acquisitions.mf_pdf as mfu_pdf, 
					CASE WHEN mf_acquisitions.notaire_id is not null THEN notaire_id ELSE 0 END as notaire_id, 
					CASE WHEN mf_acquisitions.notaire_contact is not null THEN mf_acquisitions.notaire_contact ELSE 'N.D.' END as notaire_contact, 
					mf_acquisitions.maj_date, 
					CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mf_acquisitions.maj_user END as maj_user
				FROM 
					foncier.mf_acquisitions 
					LEFT JOIN
						(SELECT 
							r_frais_mf.mf_id, 
							array_to_string(array_agg(d_typfraismf.typfraismf_id ORDER BY d_typfraismf.typfraismf_lib), ',') AS frais_id, 
							array_to_string(array_agg(d_typfraismf.typfraismf_lib ORDER BY d_typfraismf.typfraismf_lib), ',') AS frais_typ, 
							array_to_string(array_agg(r_frais_mf.mffrais ORDER BY d_typfraismf.typfraismf_lib), ',') AS frais 
						FROM 
							foncier.r_frais_mf, foncier.d_typfraismf 
						WHERE r_frais_mf.typfraismf_id = d_typfraismf.typfraismf_id 
						GROUP BY r_frais_mf.mf_id) AS tfrais 
					ON (mf_acquisitions.mf_id = tfrais.mf_id)
					LEFT JOIN admin_sig.utilisateurs ON (mf_acquisitions.maj_user = utilisateurs.utilisateur_id) 
					LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), 
					foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf, 
					(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) AS statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id ORDER BY r_mfstatut.mf_id) AS tmax 
					LEFT JOIN
						(SELECT 
							sites.site_id, sites.site_nom, r_cad_site_mf.mf_id, 
							count(DISTINCT t1.par_id) AS nbr_parcelle, 
							sum(t1.dcntpa) AS surf_pa_total, 
							count(DISTINCT t2.lot_id) AS nbr_lot, 
							sum(t2.dcntlo) AS surf_lo_total, 
							sum(t2.dcntlo) AS surf_lo_reel, 
							count(DISTINCT t3.cad_cen_id) AS nbr_cad_cen, 
							count(DISTINCT t4.cad_site_id) AS nbr_cad_site 
						FROM 
							foncier.r_cad_site_mf 
							LEFT JOIN foncier.cadastre_site t4 USING (cad_site_id) 
							LEFT JOIN cadastre.cadastre_cen t3 USING (cad_cen_id),
							cadastre.parcelles_cen t1 , 
							cadastre.lots_cen t2, 
							sites.sites
						WHERE 
							t1.par_id = t2.par_id AND 
							t2.lot_id = t3.lot_id AND 
							t4.cad_site_id = r_cad_site_mf.cad_site_id AND 
							t4.site_id = sites.site_id AND
							t3.date_fin = 'N.D.' AND
							t4.date_fin = 'N.D.' AND
							r_cad_site_mf.date_sortie = 'N.D.' AND
							r_cad_site_mf.mf_id = :acte_id
						GROUP BY sites.site_id, sites.site_nom, r_cad_site_mf.mf_id 
						ORDER BY r_cad_site_mf.mf_id) AS tsyn USING (mf_id) 
					LEFT JOIN 
						(SELECT 
							sites.site_id, 
							sites.site_nom, 
							r_cad_site_mf.mf_id 
						FROM 
							foncier.r_cad_site_mf 
							LEFT JOIN foncier.cadastre_site t4 USING (cad_site_id) 
							JOIN sites.sites USING (site_id)
						WHERE 
							t4.date_fin = 'N.D.' AND 
							r_cad_site_mf.date_sortie = 'N.D.' AND 
							r_cad_site_mf.mf_id = :acte_id 
						GROUP BY 
							sites.site_id, 
							sites.site_nom, 
							r_cad_site_mf.mf_id 
						ORDER BY r_cad_site_mf.mf_id) AS tsyn2 ON (tsyn2.mf_id = tsyn.mf_id AND tsyn2.site_id = tsyn.site_id)
					
				WHERE 
					mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id
					AND (r_mfstatut.date = tmax.statut_date AND mf_acquisitions.mf_id = tmax.mf_id) AND mf_acquisitions.mf_id = :acte_id
				GROUP BY mf_acquisitions.mf_id, d_typmf.typmf_lib, d_statutmf.statutmf_id, tfrais.frais_id, tfrais.frais_typ, tfrais.frais, d_individu.individu_id
				ORDER BY mf_acquisitions.mf_id";
				
				$rq_parc_assoc_acte = 
				"SELECT DISTINCT
					t4.cad_site_id, 
					t2.lot_id,
					t1.ccosec,
					t1.dnupla, 
					t2.dnulot, 
					CASE WHEN tmfu.mfu IS NOT NULL THEN t1.codcom || '   ' || t1.ccosec || lpad(t1.dnupla::text, 4, '0')  || ' ' ||  COALESCE(t2.dnulot,'') || '   (' || t2.dcntlo || ' m²)'  || ' ' ||  tmfu.mfu ELSE t1.codcom || '   ' || t1.ccosec || lpad(t1.dnupla::text, 4, '0')  || ' ' ||  COALESCE(t2.dnulot,'') || '   (' || t2.dcntlo || ' m²)' END  as cad_site_val, 
					CASE WHEN t3.date_fin != 'N.D.' OR t4.date_fin != 'N.D.' THEN 'prob' ELSE 'ok' END as etat_foncier 
				FROM 
					foncier.cadastre_site t4
					JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
					JOIN cadastre.lots_cen t2 USING (lot_id)
					JOIN cadastre.parcelles_cen t1 USING (par_id)
					JOIN foncier.r_cad_site_mf USING (cad_site_id)
					LEFT JOIN
						(SELECT 
							cadastre_site.cad_site_id, 
							CASE WHEN r_cad_site_mu.surf_mu is null THEN 
								'Convention'::character varying(25) 
							ELSE 'Convention pour partie'::character varying(25) 
							END as mfu,
							r_cad_site_mu.surf_mu AS surf_mfu
							
						FROM 
							foncier.cadastre_site, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (
							SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
							FROM foncier.r_mustatut
							GROUP BY r_mustatut.mu_id
							) as tmax
						WHERE 
							cadastre_site.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id 
							AND mu_conventions.typmu_id = d_typmu.typmu_id 
							AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id 
							AND (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id) 
							AND r_cad_site_mu.date_sortie = 'N.D.' 
							AND d_statutmu.statutmu_id = 1 
							AND r_cad_site_mu.mu_id != :acte_id
						GROUP BY 
							cadastre_site.cad_site_id, r_cad_site_mu.surf_mu) tmfu ON t4.cad_site_id = tmfu.cad_site_id
				WHERE r_cad_site_mf.mf_id = :acte_id AND r_cad_site_mf.date_sortie = 'N.D.'
				ORDER BY t2.lot_id";	
				
			} Else If ($type_acte == 'Convention') {
				$suffixe = 'mu';
				$rq_info_mfu = 
				"SELECT 
					array_to_string(array_agg(CASE WHEN tsyn2.site_id IS NOT NULL THEN tsyn2.site_id ELSE 'N.D.' END), ',') as tbl_site_id, 
					array_to_string(array_agg(CASE WHEN tsyn2.site_nom IS NOT NULL THEN tsyn2.site_nom ELSE 'N.D.' END), ',') as tbl_site_nom, 
					'Convention'::character varying(15) as acte, 
					mu_conventions.mu_id, mu_conventions.mu_lib as mfu_lib, mu_conventions.typmu_id as typmfu_id, d_typmu.typmu_lib as typmfu_lib, 
					CASE WHEN mu_conventions.mu_duree > 0 THEN mu_conventions.mu_duree ELSE 0 END as mu_duree, 
					d_statutmu.statutmu_id as statutmfu_id, d_statutmu.statutmu as statutmfu_lib, mu_conventions.date_sign_conv, mu_conventions.date_effet_conv, 
					mu_conventions.date_fin_conv, mu_conventions.recond_tacite, mu_conventions.recond_nbmax, mu_conventions.recond_duree, mu_conventions.recond_daterappel, current_date as current_date, 
					sum(CASE WHEN tsyn.nbr_parcelle IS NOT NULL THEN tsyn.nbr_parcelle ELSE 0 END) as nbr_parcelle, 
					sum(CASE WHEN tsyn.surf_pa_total IS NOT NULL THEN tsyn.surf_pa_total ELSE 0 END) as surf_pa_total, 
					sum(CASE WHEN tsyn.nbr_lot IS NOT NULL THEN tsyn.nbr_lot ELSE 0 END) as nbr_lot, 
					sum(CASE WHEN tsyn.surf_lo_total IS NOT NULL THEN tsyn.surf_lo_total ELSE 0 END) as surf_lo_total, 
					sum(CASE WHEN tsyn.nbr_cad_cen IS NOT NULL THEN tsyn.nbr_cad_cen ELSE 0 END) as nbr_cad_cen, 
					sum(CASE WHEN tsyn.nbr_cad_site IS NOT NULL THEN tsyn.nbr_cad_site ELSE 0 END) as nbr_cad_site,
					sum(CASE WHEN tsyn.surf_lo_reel IS NOT NULL THEN tsyn.surf_lo_reel ELSE 0 END) as surf_lo_reel, 
					tfrais.frais_id, tfrais.frais_typ, tfrais.frais, mu_conventions.mu_clauses_part as mfu_clauses_part, 
					mu_conventions.mu_courier_envoi_proprio, mu_conventions.mu_courier_envoi_proprio_date, mu_conventions.mu_commentaires as mfu_commentaires, mu_conventions.mu_pdf as mfu_pdf, 
					mu_conventions.maj_date, 
					CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mu_conventions.maj_user END as maj_user,
					CASE WHEN date_effet_conv IS NOT NULL AND date_effet_conv != 'N.D.' AND date_effet_conv != '' AND mu_duree > 0 THEN 
						(date(date_effet_conv) + interval '1 month' * mu_duree)::character varying(10) ELSE 'N.P.' 
					END as first_end, --Prochaine date fin avant reconduction
					CASE WHEN recond_tacite = 't' AND mu_duree > 0 AND date_effet_conv != 'N.D.' THEN
						CASE WHEN EXTRACT(DAY FROM age(date(date_effet_conv) + interval '1 month' * mu_duree)) > 0 THEN
							admin_sig.f_special_round((((EXTRACT(YEAR FROM age(date(date_effet_conv) + interval '1 month' * mu_duree)) * 12) + EXTRACT(MONTH FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree))))+1) / recond_duree)::numeric, 0)::integer
						ELSE
							((((EXTRACT(YEAR FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree)))) * 12) + EXTRACT(MONTH FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree))))+0) / recond_duree)-0)::integer
						END
					ELSE
						0
					END as nbr_reconduction,
					CASE WHEN recond_tacite = 't' AND mu_duree > 0 AND date_effet_conv != 'N.D.' THEN
						CASE WHEN EXTRACT(DAY FROM age(date(date_effet_conv) + interval '1 month' * mu_duree)) > 0 THEN
							(date((date(date_effet_conv) + interval '1 month' * mu_duree)::date) + interval '1 month' * (recond_duree*admin_sig.f_special_round((((EXTRACT(YEAR FROM age(date(date_effet_conv) + interval '1 month' * mu_duree)) * 12) + EXTRACT(MONTH FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree))))+1) / recond_duree)::numeric, 0)))::character varying(10)
						ELSE
							(date((date(date_effet_conv) + interval '1 month' * mu_duree)::date) + interval '1 month' * (recond_duree*((((EXTRACT(YEAR FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree)))) * 12) + EXTRACT(MONTH FROM age(((date(date_effet_conv) + interval '1 month' * mu_duree))))+0) / recond_duree)-0)))::character varying(10)
						END
					ELSE
						'N.P.'::character varying(10)
					END as next_end
				FROM 
					foncier.mu_conventions
					LEFT JOIN
						(SELECT 
							r_frais_mu.mu_id, 
							array_to_string(array_agg(d_typfraismu.typfraismu_id ORDER BY d_typfraismu.typfraismu_id), ',') AS frais_id, 
							array_to_string(array_agg(d_typfraismu.typfraismu_lib ORDER BY d_typfraismu.typfraismu_id), ',') AS frais_typ, 
							array_to_string(array_agg(r_frais_mu.mufrais ORDER BY d_typfraismu.typfraismu_id), ',') AS frais 
						FROM 
							foncier.r_frais_mu, foncier.d_typfraismu 
						WHERE r_frais_mu.typfraismu_id = d_typfraismu.typfraismu_id 
						GROUP BY r_frais_mu.mu_id) AS tfrais 
					ON (mu_conventions.mu_id = tfrais.mu_id)
					LEFT JOIN admin_sig.utilisateurs ON (mu_conventions.maj_user = utilisateurs.utilisateur_id) 
					LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), 
					foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, 
					(SELECT r_mustatut.mu_id, Max(r_mustatut.date) AS statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id ORDER BY r_mustatut.mu_id) AS tmax 
					LEFT JOIN
						(SELECT 
							sites.site_id, sites.site_nom, r_cad_site_mu.mu_id, 
							count(DISTINCT t1.par_id) AS nbr_parcelle, 
							sum(t1.dcntpa) AS surf_pa_total, 
							count(DISTINCT t2.lot_id) AS nbr_lot, 
							sum(t2.dcntlo) AS surf_lo_total, 
							sum(CASE 
								WHEN r_cad_site_mu.surf_mu is null THEN t2.dcntlo
								ELSE r_cad_site_mu.surf_mu 
							END) AS surf_lo_reel,
							count(DISTINCT t3.cad_cen_id) AS nbr_cad_cen, 
							count(DISTINCT t4.cad_site_id) AS nbr_cad_site 
						FROM 
							foncier.r_cad_site_mu 
							LEFT JOIN foncier.cadastre_site t4 USING (cad_site_id) 
							LEFT JOIN cadastre.cadastre_cen t3 USING (cad_cen_id),
							cadastre.parcelles_cen t1 , 
							cadastre.lots_cen t2, 
							sites.sites
						WHERE 
							t1.par_id = t2.par_id AND 
							t2.lot_id = t3.lot_id AND 
							t4.cad_site_id = r_cad_site_mu.cad_site_id AND 
							t4.site_id = sites.site_id AND ";
				If ($statut_mfu['statut_id'] == 2 || $statut_mfu['statut_id'] == 4) {
						$rq_info_mfu .= 
							"t3.date_fin >= '".$dates_mu['date_fin_conv']."' AND 
							t4.date_fin >= '".$dates_mu['date_fin_conv']."' AND ";			
				} Else {
					$rq_info_mfu .= 
							"t3.date_fin = 'N.D.' AND
							t4.date_fin = 'N.D.' AND
							r_cad_site_mu.date_sortie = 'N.D.' AND ";
				}
				$rq_info_mfu .= 
						"r_cad_site_mu.mu_id = :acte_id
						GROUP BY sites.site_id, sites.site_nom, r_cad_site_mu.mu_id
						ORDER BY r_cad_site_mu.mu_id) AS tsyn USING (mu_id) 
					LEFT JOIN 
						(SELECT 
							sites.site_id, 
							sites.site_nom, 
							r_cad_site_mu.mu_id 
						FROM 
							foncier.r_cad_site_mu 
							LEFT JOIN foncier.cadastre_site t4 USING (cad_site_id) 
							JOIN sites.sites USING (site_id)
						WHERE 
							t4.date_fin = 'N.D.' AND 
							r_cad_site_mu.date_sortie = 'N.D.' AND 
							r_cad_site_mu.mu_id = :acte_id 
						GROUP BY 
							sites.site_id, 
							sites.site_nom, 
							r_cad_site_mu.mu_id 
						ORDER BY r_cad_site_mu.mu_id) AS tsyn2 ON (tsyn2.mu_id = tsyn.mu_id AND tsyn2.site_id = tsyn.site_id) 						
				WHERE 
					mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id
					AND (r_mustatut.date = tmax.statut_date AND mu_conventions.mu_id = tmax.mu_id) AND mu_conventions.mu_id = :acte_id
				GROUP BY mu_conventions.mu_id, d_typmu.typmu_lib, d_statutmu.statutmu_id, tfrais.frais_id, tfrais.frais_typ, tfrais.frais, d_individu.individu_id
				ORDER BY mu_conventions.mu_id;";
				
				$rq_parc_assoc_acte = 
				"SELECT DISTINCT 
					t4.cad_site_id, 
					t2.lot_id, 
					t1.ccosec, 
					t1.dnupla, 
					t2.dnulot, 
					CASE WHEN tmfu.mfu IS NOT NULL THEN t1.codcom || '   ' || t1.ccosec || lpad(t1.dnupla::text, 4, '0')  || ' ' ||  COALESCE(t2.dnulot,'') || '   (' || t2.dcntlo || ' m²)'  || ' ' ||  tmfu.mfu ELSE t1.codcom || '   ' || t1.ccosec || lpad(t1.dnupla::text, 4, '0')  || ' ' ||  COALESCE(t2.dnulot,'') || '   (' || t2.dcntlo || ' m²)' END  as cad_site_val, 
					CASE WHEN t3.date_fin != 'N.D.' OR t4.date_fin != 'N.D.' THEN 'prob' ELSE 'ok' END as etat_foncier 
				FROM 
					foncier.cadastre_site t4
					JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
					JOIN cadastre.lots_cen t2 USING (lot_id)
					JOIN cadastre.parcelles_cen t1 USING (par_id)
					JOIN foncier.r_cad_site_mu USING (cad_site_id)
					LEFT JOIN
					(SELECT 
						cadastre_site.cad_site_id, 
						CASE WHEN r_cad_site_mu.surf_mu is null THEN 
							'Convention'::character varying(25) 
						ELSE 'Convention pour partie'::character varying(25) 
						END as mfu,
						r_cad_site_mu.surf_mu AS surf_mfu
						
					FROM 
						foncier.cadastre_site, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (
						SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
						FROM foncier.r_mustatut
						GROUP BY r_mustatut.mu_id
						) as tmax
					WHERE 
						cadastre_site.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id 
						AND mu_conventions.typmu_id = d_typmu.typmu_id 
						AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id 
						AND (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id) 
						AND r_cad_site_mu.date_sortie = 'N.D.' 
						AND d_statutmu.statutmu_id = 1 
						AND r_cad_site_mu.mu_id != :acte_id
					GROUP BY 
						cadastre_site.cad_site_id, r_cad_site_mu.surf_mu) tmfu ON t4.cad_site_id = tmfu.cad_site_id
				WHERE r_cad_site_mu.mu_id = :acte_id AND r_cad_site_mu.date_sortie = 'N.D.'
				ORDER BY t2.lot_id";
			}
			$res_parc_assoc_acte = $bdd->prepare($rq_parc_assoc_acte);
			$res_parc_assoc_acte->execute(array('acte_id'=>$acte_id));	
				
			If (isset($rq_info_mfu)) {
				// echo $rq_info_mfu;
				$res_info_mfu = $bdd->prepare($rq_info_mfu);
				$res_info_mfu->execute(array('acte_id'=>$acte_id));
				$info_mfu = $res_info_mfu->fetch();
			} Else {
				include('../includes/prob_tech.php');
			}
			If ($res_info_mfu->rowCount() == 0){
				include('../includes/prob_tech.php');
			}
			
			$lst_site['id'] = preg_split("/,/", $info_mfu['tbl_site_id']);
			$lst_site['nom'] = preg_split("/,/", $info_mfu['tbl_site_nom']);
			$lst_frais['id'] = preg_split("/,/", $info_mfu['frais_id']);
			$lst_frais['type'] = preg_split("/,/", $info_mfu['frais_typ']);
			$lst_frais['montant'] = preg_split("/,/", $info_mfu['frais']);
			
			$rq_parc_dispo =
			"SELECT 
				tt1.cad_site_id,
				tt1.codcom,
				tt1.ccosec,
				tt1.dnupla,
				tt1.dnulot,
				CASE WHEN mfu = 'R.A.S.' THEN tt1.cad_site_val ELSE tt1.cad_site_val || ' ' || mfu 
				END 
				|| CASE WHEN tt1.parm_id IS NOT NULL THEN ' Parcelle mère : ' || tt1.parm_id ELSE ''
				END	as cad_site_val
			FROM
				(SELECT 
					t4.cad_site_id, t1.codcom, t1.ccosec, t1.dnupla, t2.dnulot, 
					replace(t1.codcom || ' ' || t1.ccosec || lpad(t1.dnupla::text, 4, '0') || ' ' || COALESCE(t2.dnulot,'') || ' (' || t2.dcntlo || ' m²)', '  ', ' ') as cad_site_val, 
					COALESCE(tmfu.mfu, 'R.A.S.') as mfu, tparm.parm_id
				FROM 
					foncier.cadastre_site t4
					JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
					JOIN cadastre.lots_cen t2 USING (lot_id)
					JOIN cadastre.parcelles_cen t1 USING (par_id)
					JOIN sites.sites USING (site_id)
					LEFT JOIN
						(SELECT 
							cadastre_site.cad_site_id, 'Acquisition'::character varying(25) as mfu, Null AS surf_mfu
						FROM 
							foncier.cadastre_site 
							JOIN foncier.r_cad_site_mf USING (cad_site_id) 
							JOIN foncier.mf_acquisitions USING (mf_id)
							JOIN foncier.d_typmf USING (typmf_id)
							JOIN foncier.r_mfstatut USING (mf_id) 
							JOIN foncier.d_statutmf USING (statutmf_id)
							JOIN (SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id ) as tmax ON (r_mfstatut.date = tmax.statut_date AND r_mfstatut.mf_id = tmax.mf_id)
						WHERE 
							 d_typmf.typmf_lib != 'Acquisition par un tiers' AND d_statutmf.statutmf_id = 1 AND r_cad_site_mf.date_sortie = 'N.D.'
							
						UNION

						SELECT 
							cadastre_site.cad_site_id, 
							CASE WHEN r_cad_site_mu.surf_mu is null THEN 
								'Convention'::character varying(25) 
							ELSE 'Convention pour partie'::character varying(25) 
							END as mfu,
							r_cad_site_mu.surf_mu AS surf_mfu							
						FROM 
							foncier.cadastre_site 
							JOIN foncier.r_cad_site_mu USING (cad_site_id)
							JOIN foncier.mu_conventions USING (mu_id) 
							JOIN foncier.d_typmu USING (typmu_id)
							JOIN foncier.r_mustatut USING (mu_id)
							JOIN foncier.d_statutmu USING (statutmu_id) 
							JOIN (SELECT r_mustatut.mu_id, r_mustatut.statutmu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id, r_mustatut.statutmu_id ) as tmax ON (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id)
						WHERE 
							CASE WHEN tmax.statutmu_id = 1 THEN r_cad_site_mu.date_sortie = 'N.D.' END";							
							$rq_parc_dispo .=
							" AND d_statutmu.statutmu_id = 1 
						GROUP BY 
							cadastre_site.cad_site_id, r_cad_site_mu.surf_mu) tmfu ON t4.cad_site_id = tmfu.cad_site_id
					LEFT JOIN
						(SELECT cadastre_site.cad_site_id, lpad(ccosecm,2,'0') || dnuplam as parm_id
						FROM cadastre.parcelles_cen
						JOIN cadastre.lots_cen USING (par_id)
						JOIN cadastre.cadastre_cen USING (lot_id)
						JOIN foncier.cadastre_site USING (cad_cen_id)
						WHERE ccocomm IS NOT NULL OR ccoprem IS NOT NULL OR ccosecm IS NOT NULL OR dnuplam IS NOT NULL) tparm ON t4.cad_site_id = tparm.cad_site_id 
				WHERE 
					t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'
					AND t4.cad_site_id NOT IN (SELECT cad_site_id FROM foncier.r_cad_site_".$suffixe." WHERE ".$suffixe."_id = '$acte_id' AND date_sortie = 'N.D.')";
				If ($res_parc_assoc_acte->rowCount() > 0) {	
					$rq_parc_dispo .= " AND t4.site_id IN ('".implode("','",explode(',', $info_mfu['tbl_site_id']))."')";
				}
				$rq_parc_dispo .=
				") tt1
			WHERE mfu != 'Acquisition'";
			$rq_parc_dispo .= 
			" ORDER BY cad_site_val";
			// echo $rq_parc_dispo;
			If ($type_acte == 'Acquisition') {
				$date_compromis = date_transform($info_mfu['date_compromis_pv_proprio']);
				$date_promesse = date_transform($info_mfu['date_compromis_pa_president']);
				$date_delib_ca = date_transform($info_mfu['date_delib_ca']);
				$date_signature = date_transform($info_mfu['date_sign_acte']);
				
				$lst_date['lib'] = array("Compromis de vente", "Promesse d'achat", "Délibération en CA", "Signature");
				$lst_date['date'] = array($date_compromis, $date_promesse, $date_delib_ca, $date_signature);
				$lst_date['champ'] = array('date_compromis', 'date_promesse', 'date_delib_ca', 'date_signature');
			} Else If ($type_acte == 'Convention') {
				$date_signature = date_transform($info_mfu['date_sign_conv']);
				$date_debut = date_transform($info_mfu['date_effet_conv']);
				$date_fin = date_transform($info_mfu['date_fin_conv']);
				
				$lst_date['lib'] = array("Signature", "Début", "Fin");
				$lst_date['date'] = array($date_signature, $date_debut, $date_fin);
				$lst_date['champ'] = array('date_signature', 'date_debut', 'date_fin');
			}
		?>	
		
		<section id='main'>
			<h3><?php echo $type_acte . ' : ' . $acte_id; ?></h3>
			<div id='onglet' class='list'>
				<ul>
					<li>
						<a id='li_general' href="#general"><span>G&eacute;n&eacute;ral</span></a>
					</li>
					<li>
						<a id='li_parcelle' href="#parcelle" onclick="create_lst_parc('charge', 0, '<?php echo $acte_id; ?>');"><span>Parcelles/ Lots</span></a>
					</li>
					<li>
						<a id='li_proprio' href="#proprio" onclick="create_lst_proprio(0, '<?php echo $acte_id; ?>');"><span>Propriétaires</span></a>
					</li>
				</ul>
						
				<div id='general'>
					<input type='hidden' id='filtre_type_acte' value='<?php echo $type_acte; ?>';>
					<input type='hidden' id='acte_id' value='<?php echo $acte_id; ?>';>
					<input type='hidden' id='hid_tbl_grant_elt_form_id' value='<?php echo $lst_grant_elt_temp[1]; ?>'>
					<?php If ($mode == 'consult') { ?>
					<table id='frm_acte' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<span style="padding-left:calc(100% - 30px);"></span>
								<label class='label black'>
								<?php If ($info_mfu['mfu_pdf'] != '') { ?>
									<a target="_blank" href="<?php echo PATH_ACT.$info_mfu['mfu_pdf']; ?>"><img src='images/pdf.png' width='25px' style='position:relative; top:-13px;'></a>
								<?php } Else { ?>
									<img src='images/pdf-no.png' width='25px' style='position:relative; top:-13px;'>
								<?php } ?>
								</label>
								<center>
									<table CELLSPACING = '0' CELLPADDING ='5' class='no-border bilan-foncier' width='100%'>
										<tr class='tr_fieldset'>
											<td>
												<fieldset id='fieldset_description' class='fieldset'>
													<legend>Description</legend>
													<table id='tbl_description' CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px;'>
														<tr>
															<td style='text-align:right'>
																<label class="label"><u>Site :</u></label>
															</td>
															<td>
																<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px'>
																<?php 
																If ($lst_site['id'][0]) {
																	For ($i=0; $i<count($lst_site['id']); $i++) { ?>
																	<tr>
																		<td style='text-align:left'>
																			<label class='label'>
																				<?php echo $lst_site['id'][$i]?> (<?php echo $lst_site['nom'][$i]?>)
																				<?php
																					$vget = "site_nom:".$lst_site['nom'][$i].";site_id:".$lst_site['id'][$i].";";
																					$vget_crypt = base64_encode($vget);
																					$lien_site = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";
																				?>
																				<a href='<?php echo $lien_site; ?>'>
																					<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche site">
																				</a>
																			</label>
																		</td>
																	</tr>
																<?php 
																	}
																} Else {
																?>
																	<tr>
																		<td style='text-align:left'>
																			<label class='label'>
																				N.D.
																			</label>
																		</td>
																	</tr>
																<?php
																}
																?>
																</table>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><u>Libellé :</u></label>
															</td>
															<td style='width:451px'>
																<label class="label"><?php echo retour_ligne($info_mfu['mfu_lib'], 50);?></label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><u>Type :</u></label>
															</td>
															<td>
																<label class="label"><?php echo $info_mfu['typmfu_lib'];?></label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><u>Statut :</u></label>
															</td>
															<td>
																<label class="label"><?php echo $info_mfu['statutmfu_lib'];?></label>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
											<td>
												<fieldset id='fieldset_date' class='fieldset'>
													<legend>Dates</legend>
													<table id='tbl_date' CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px;'>
														<?php 
														For($i=0; $i<count($lst_date['date']); $i++) { 
															If ($lst_date['date'][$i] != 'N.D.') {?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u><?php echo $lst_date['lib'][$i]; ?> :</u></label>
															</td>
															<td>
																<label class="label"><?php echo $lst_date['date'][$i]; ?></label>
															</td>
														</tr>
														<?php } 
														}
														?>
													<?php If ($type_acte == 'Convention' && $info_mfu['statutmfu_id'] != 2 && $info_mfu['statutmfu_id'] != 4) { ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Durée initiale :</u></label>
															</td>
															<td>
																<label class="label"><?php echo $info_mfu['mu_duree']; ?> mois</label>
																<label class="label_simple"><em>(<?php If (is_int($info_mfu['mu_duree']/12)) { echo $info_mfu['mu_duree']/12; } Else If ($info_mfu['mu_duree'] < 0) { echo abs(round(($info_mfu['mu_duree']/12),0)); } Else { echo round(($info_mfu['mu_duree']/12),1); } ?> an<?php If ($info_mfu['mu_duree']/12 > 1) { echo 's'; } ?>)</em></label>
																<label class="label">(échéance : <span style='text-decoration:<?php If ($info_mfu['first_end'] < $info_mfu['current_date']) { echo 'line-through';} Else { echo 'none'; } ?>'><?php echo date_transform($info_mfu['first_end']); ?></span>)</label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Reconduction tacite :</u></label>
															</td>
															<td>
																<label class="label"><?php If ($info_mfu['recond_tacite'] == 't') {echo 'Oui';} Else {echo 'Non';}?></label>
															</td>
														</tr>
														<?php If ($info_mfu['recond_tacite'] == 't') { ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Durée de reconduction :</u></label>
															</td>
															<td>
																<label class="label"><?php echo $info_mfu['recond_duree']; ?> mois</label>
																<label class="label_simple"><em>(<?php If (is_int($info_mfu['recond_duree']/12)) { echo $info_mfu['recond_duree']/12; } Else If ($info_mfu['recond_duree'] < 0) { echo abs(round(($info_mfu['recond_duree']/12),0)); } Else { echo round(($info_mfu['recond_duree']/12),1); } ?> an<?php If ($info_mfu['recond_duree']/12 > 1) { echo 's'; } ?>)</em></label>
																<?php If($info_mfu['nbr_reconduction'] > 0) { ?><label class="label">(échéance : <span style='text-decoration:<?php If ($info_mfu['next_end'] < $info_mfu['current_date']) {echo 'line-through';} Else {echo 'none';} ?>'><?php echo date_transform($info_mfu['next_end']); ?></span>)</label><?php } ?>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Nombre de reconduction maximal :</u></label>
															</td>
															<td>
																<label class="label"><?php If ($info_mfu['recond_nbmax'] == -1) { ?> &infin; <?php } Else { echo $info_mfu['recond_nbmax'];} ?></label>
															</td>
														</tr>
														<?php If ($info_mfu['nbr_reconduction'] > 0) { ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Convention reconduite :</u></label>
															</td>
															<td>
																<label class="label"><?php echo $info_mfu['nbr_reconduction']; ?> fois</label>
															</td>
														</tr>
														<?php } ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Date de rappel avant reconduction :</u></label>
															</td>
															<td>
																<label class="label"><?php echo date_transform($info_mfu['recond_daterappel']); ?></label>
															</td>
														</tr>
														<?php } ?>
													<?php } ?>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr class='tr_fieldset'>
											<td>
												<fieldset id='fieldset_parcelle' class='fieldset'>
													<legend>Parcelles/ Surface</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px;'>
														<tr>
															<td style='text-align:right'>
																<label class="label"><?php echo $info_mfu['nbr_parcelle'];?></label>
															</td>
															<td>
																<label class="label">parcelle<?php If ($info_mfu['nbr_parcelle'] > 1) {echo 's';} ?></label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><?php echo $info_mfu['nbr_lot'];?></label>
															</td>
															<td>
																<label class="label">lot<?php If ($info_mfu['nbr_lot'] > 1) {echo 's';} ?></label>
																<label class="label" style='font-weight:normal;'>soit <?php echo conv_are($info_mfu['surf_lo_total']); ?> ha.a.ca</label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><u>Surface <?php If ($type_acte == 'Acquisition') {echo 'acquise';} Else If ($type_acte == 'Convention') {echo 'conventionnée';} ?> :</u></label>
															</td>
															<td>
																<label class="label">
																<?php 
																	If ($info_mfu['nbr_parcelle'] > 0) {
																		echo conv_are($info_mfu['surf_lo_reel']);
																	} Else {
																		echo conv_are(0);
																	}
																?> 
																	ha.a.ca
																</label>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
											<td>
												<fieldset id='fieldset_frais' class='fieldset'>
													<legend>Montants</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='width:90%;'>
														<?php If (array_sum($lst_frais['montant']) > 0) { 
														$lst_frais_total = 0;
														?>
														<?php For($i=0; $i<count($lst_frais['type']); $i++) { ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u><?php echo $lst_frais['type'][$i]; ?> :</u></label>
															</td>
															<td style="text-align:right" width='1%'>
																<label class="label"><?php echo  number_format($lst_frais['montant'][$i],2, ',', ' '); ?> €</label>
															</td>
														<?php If ($lst_frais['id'][$i] == 1 || $lst_frais['id'][$i] == 5) {
															$lst_frais_total = $lst_frais_total + $lst_frais['montant'][$i];
														} ?>
														</tr>
														<?php } ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>TOTAL PRIX D'ACQUISITION :</u></label>
															</td>
															<td style="text-align:right" width='1%'>
																<label class="label"><?php echo number_format($lst_frais_total,2, ',', ' '); ?> €</label>
															</td>
														</tr>
														<?php } Else { ?>
														<tr>
															<td style='text-align:center;'>
																<label class='label' <?php If ($type_acte=='Acquisition') { ?>style='color:red;' <?php } ?> <?php If ($type_acte=='Convention') { ?>style='font-weight:normal;' <?php } ?>>Aucun montant n'a été saisi !</label>
															</td>
														</tr>
														<?php } ?>
													</table>
												</fieldset>
											</td>
										</tr>
										<?php  If ($type_acte == 'Acquisition') { ?> 
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Notaire</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='width:100%;height:100%;'>
														<tr>
															<?php If ($info_mfu['notaire_id'] != 0) {
																$rq_info_notaire = "
																SELECT particules_ini, nom, prenom, personne.adresse3, personne.individu_id, personne.structure_id
																FROM personnes.personne, personnes.d_particules, personnes.d_individu
																WHERE personne.individu_id = d_individu.individu_id AND personne.particules_id = d_particules.particules_id AND personne.personne_id = " . $info_mfu['notaire_id'];
																$res_info_notaire = $bdd->prepare($rq_info_notaire);
																$res_info_notaire->execute();
																$info_notaire = $res_info_notaire->fetch();
																$res_info_notaire->closeCursor();
															?>
															<td style='text-align:left;width:50%;'>
																<label class='label'><?php echo $info_notaire['particules_ini'] . '. ' . strtoupper($info_notaire['nom']); If ($info_notaire['prenom'] != 'N.D.') {echo ' ' . ucwords($info_notaire['prenom']);} ?></label>
																<label class='label'><i>Etude à <?php echo $info_notaire['adresse3']; ?></i></label>
																<?php
																	$vget = "mode:consult;";
																	$vget .= "individu_id:" . $info_notaire['individu_id'] . ";";
																	$vget .= "structure_id:" . $info_notaire['structure_id'] . ";";
																	$vget_crypt = base64_encode($vget);
																	$lien_notaire = ROOT_PATH . "contacts/contact_select.php?d=$vget_crypt#$info_notaire[structure_id]";
																?>
																<a href='<?php echo $lien_notaire; ?>'>
																	<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche notaire">
																</a>
															</td>
															<?php } Else { ?>
															<td style='text-align:left;width:50%;'>
																<label class='label'>N.D.</label>
															</td>
															<?php } ?>
															<td style='text-align:left;'>
																<label class='label'>Contact : <?php echo $info_mfu['notaire_contact']; ?></label>
															</td>
														</tr>
													</table>	
												</fieldset>
											</td>	
										</tr>
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Suivi administratif</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='height:100%;'>
														<tr>
															<td style='text-align:left;'>
																<label class='label'><u>Acte re&ccedil;u :</u><?php If ($info_mfu['mf_courier_recep_notaire'] == 't') {echo ' Oui';} Else {echo ' Non';} ?></label>
															</td>
															<td style='text-align:left;'>
																<label class='label'>le : <?php echo date_transform($info_mfu['mf_courier_recep_notaire_date']); ?></label>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>
										<?php } Else If ($type_acte == 'Convention') { ?> 
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Suivi administratif</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='height:100%;'>
														<tr>
															<td style='text-align:left;'>
																<label class='label'><u>Convention sign&eacute;e et retourn&eacute;e aux propri&eacute;taires :</u><?php If ($info_mfu['mu_courier_envoi_proprio'] == 't') {echo ' Oui';} Else {echo ' Non';} ?></label>
															</td>
															<td style='text-align:left;'>
																<label class='label'>le : <?php echo date_transform($info_mfu['mu_courier_envoi_proprio_date']); ?></label>
															</td>
														</tr>
													</table>
													
												</fieldset>
											</td>
										</tr>
										<?php } ?>
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Commentaires</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px;height:100%;'>
														<tr>
															<td>
																<label class='label'><u>Clauses particulières :</u><?php If ($info_mfu['mfu_clauses_part'] == 't') {echo ' Oui';} Else {echo ' Non';} ?></label>
															</td>
														</tr>
														<tr>
															<td>
																<textarea id='txt_com' class="label textarea_com" readonly rows='3' cols='1000'><?php echo $info_mfu['mfu_commentaires'];?></textarea>
															</td>
														</tr>
													</table>
													
												</fieldset>
											</td>
										</tr>
									</table>
								</center>
							</td>
						</tr>
						<tr>
							<td style='text-align:right' colspan='3'>
								<label class="last_maj">
									Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($info_mfu['maj_date'], 0, 10)); ?> par <?php echo $info_mfu['maj_user'];?>
								</label>
							</td>
						</tr>
					</table>
					<?php } Else If ($mode == 'edit' && $verif_role_acces == 'OUI') { 
					$cur_info = -1;
					$vget_maj = "type_acte:" . $type_acte . ";";
					$vget_maj .= "acte_id:" . $acte_id . ";";
					$vget_maj .= "mode:consult;";
					$vget_maj .= "do:maj;";
					$vget_maj .= "type_maj:update;";
					$vget_maj .= "lst_site_id:" . $info_mfu['tbl_site_id'] . ";";
					$vget_crypt_maj = base64_encode($vget_maj);
					$lien_maj= substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_mfu.php?d=$vget_crypt_maj#general";?>
					<form class='form' onsubmit="return valide_form('frm_modif_mfu')" name='frm_modif_mfu' action='<?php echo $lien_maj; ?>' id='frm_modif_mfu' method='POST' enctype="multipart/form-data">					
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<?php
									If (GEST_ACT == 'web') {
								?>
										<input type="file" id='file_acte_pdf' name='file_acte_pdf' style="display:none" onchange="getfile('etape2', 'file_acte_pdf', 'text_acte_pdf')"/>
										<label  id="text_acte_pdf" name='text_acte_pdf' class="label">
											<?php If ($info_mfu['mfu_pdf'] != '') {echo $info_mfu['mfu_pdf'];} Else {echo 'Pas de document pdf associé';} ?>
										</label>
								<?php
									If (in_array('_acte_pdf', $tbl_grant_elt_form_id)) {
								?>
										<input type="button" value="<?php If ($info_mfu['mfu_pdf'] != '') {echo 'Modifier le pdf ...';} Else {echo 'Parcourir ...';} ?>" class='btn_frm' onclick="getfile('etape1', 'file_acte_pdf', 'text_acte_pdf');" style="padding: 4px;"/>
								<?php
										If ($info_mfu['mfu_pdf'] != '') {
								?>
											<input type="button" value='Supprimer le pdf' class='btn_frm' onclick="$('#text_acte_pdf').css('text-decoration', 'line-through');$('#remove_acte_pdf').val('removed');" style="padding: 4px;"/>
								<?php
										}
									}
										$cur_info++;
										$cur_info_mfu[$cur_info]['elt_name'] = 'file_acte_pdf';
										$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['mfu_pdf'];
									} Else If (GEST_ACT == 'file') {
								?>
										<label  id="text_acte_pdf" name='text_acte_pdf' class="label">
											<?php If ($info_mfu['mfu_pdf'] != '') {echo $info_mfu['mfu_pdf'];} Else {echo 'Pas de document pdf associé';} ?>
										</label>
										<input id='acte_chk_pdf' name='acte_chk_pdf' type="checkbox" class='editbox' style="-moz-transform: scale(1.2); margin: 1px 3px 8px;" width="15px" <?php If ($info_mfu['mfu_pdf'] != '') { echo "checked value='t'"; } Else { echo "value='f'"; } If (!in_array('_acte_pdf', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED"; }?>/>
								<?php
										If ($info_mfu['mfu_pdf'] == '') {											
											$cur_info++;
											$cur_info_mfu[$cur_info]['elt_name'] = 'acte_chk_pdf';
											$cur_info_mfu[$cur_info]['elt_val'] = 'f';
										}
									}
								?>
								<input type='hidden' id='remove_acte_pdf' name='remove_acte_pdf' value=''>
								<center>
									<table id='frm_acte' CELLSPACING = '0' CELLPADDING ='5' class='no-border' width='100%'>
										<tr class='tr_fieldset'>
											<td>
												<fieldset id='fieldset_description' class='fieldset'>
													<legend>Description</legend>
													<table id='tbl_description' CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='min-width:400px;'>
														<tr style='height:32px'>
															<td style='text-align:right'>
																<label class="label"><u>Site :</u></label>
															</td>
															<td>
																<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px'>
																	<?php For ($i=0; $i<count($lst_site['id']); $i++) { ?>
																	<tr>
																		<td style='text-align:left'>
																			<label class='label'>
																				<?php echo $lst_site['id'][$i]?> (<?php echo $lst_site['nom'][$i]?>)
																				<?php
																					$vget = "site_nom:".$lst_site['nom'][$i].";site_id:".$lst_site['id'][$i].";";
																					$vget_crypt = base64_encode($vget);
																					$lien_site = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";
																				?>
																				<a href='<?php echo $lien_site; ?>'>
																					<img style='margin:-5px 0;' src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche site">
																				</a>
																			</label>
																		</td>
																	</tr>
																	<?php } ?>
																</table>
															</td>
														</tr>														
														<tr style='height:32px'>
															<td style='text-align:right'>
																<label class="label"><u>Libellé :</u></label>
															</td>
															<td style='width:451px'>
																<input type='text' class='editbox <?php If (!in_array('need_acte_lib', $tbl_grant_elt_form_id)) {echo ' readonly';} ?>' style='' size='60' id='need_acte_lib' name='need_acte_lib' value="<?php echo $info_mfu['mfu_lib'];?>" <?php If (!in_array('need_acte_lib', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';} ?>>
																<?php
																	$cur_info++; 
																	$cur_info_mfu[$cur_info]['elt_name'] = 'need_acte_lib';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['mfu_lib'];
																?>
															</td>
														</tr>
														<tr style='height:32px'>
															<td style='text-align:right'>
																<label class="label"><u>Type :</u></label>
															</td>
															<td>
																<div id='div_need_acte_type' <?php If (!in_array('need_acte_type', $tbl_grant_elt_form_id)) {echo "class='readonly'";} ?>>
																	<label class="btn_combobox">			
																		<select id="need_acte_type" name="need_acte_type" class="combobox" style='text-align:left' <?php If (!in_array('need_acte_type', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED";} ?>>
																			<?php
																			$rq_lst_acte_type = "SELECT typ".$suffixe."_id as id, typ".$suffixe."_lib as lib FROM foncier.d_typ".$suffixe . " WHERE typ".$suffixe."_lib NOT LIKE '%(MAJ en 1 ou 6)' ORDER BY lib";
																			$res_lst_acte_type = $bdd->prepare($rq_lst_acte_type);
																			$res_lst_acte_type->execute();																			
																			while ($donnees = $res_lst_acte_type->fetch()) {
																					echo "<option ";
																					If ($donnees['lib'] == $info_mfu['typmfu_lib']) {
																						echo 'selected ';
																					}
																					echo "value='$donnees[id]'>$donnees[lib]</option>";
																				}
																			$res_lst_acte_type->closeCursor();
																			?>				
																		</select>
																		<?php
																			$cur_info++;
																			$cur_info_mfu[$cur_info]['elt_name'] = 'need_acte_type';
																			$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['typmfu_id'];
																		?>
																	</label>
																</div>
															</td>		
														</tr>
														<tr style='height:32px'>
															<td style='text-align:right'>
																<label class="label"><u>Statut :</u></label>
															</td>
															<td>
																<div id="div_need_acte_statut" class="<?php If ($info_mfu['statutmfu_id'] != 0 || !in_array('need_acte_statut', $tbl_grant_elt_form_id)) {echo "readonly";} ?>">			
																	<label class="btn_combobox">			
																		<select name="need_acte_statut" id="need_acte_statut" class="combobox" style='text-align:left' <?php If ($info_mfu['statutmfu_id'] != 0 || !in_array('need_acte_statut', $tbl_grant_elt_form_id)) { echo " READONLY DISABLED ";} ?> onchange="chgStatut('<?php echo $info_mfu['statutmfu_id'] ?>');">
																		<?php
																			$rq_lst_type_statut = "SELECT statut".$suffixe."_id as id, statut".$suffixe." as lib FROM foncier.d_statut".$suffixe;
																			$res_lst_type_statut = $bdd->prepare($rq_lst_type_statut);
																			$res_lst_type_statut->execute();																			
																			While ($donnees = $res_lst_type_statut->fetch()) {
																				echo "<option ";
																				If ($donnees['id'] == $info_mfu['statutmfu_id']) {
																					echo 'selected ';
																				}
																				echo "value='$donnees[id]'>$donnees[lib]</option>";
																			}
																			$res_lst_type_statut->closeCursor();
																		?>				
																		</select>
																		<?php
																			$cur_info++;
																			$cur_info_mfu[$cur_info]['elt_name'] = 'need_acte_statut';
																			$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['statutmfu_id'];
																		?>
																	</label>
																	<?php If ($info_mfu['statutmfu_id'] != 0 && in_array('need_acte_statut', $tbl_grant_elt_form_id)) { ?>
																	<img id='img_need_acte_statut' style='margin:-5px 0;cursor:pointer;' src='<?php echo ROOT_PATH; ?>/images/lock.png' width='25px' onclick="unlockField('img_need_acte_statut', 'div_need_acte_statut', 'need_acte_statut', 'popup_avertissement', '<?php echo $info_mfu['statutmfu_id']; ?>'); chgStatut();">
																	<?php } ?>
																</div>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
											<td>
												<fieldset id='fieldset_date' class='fieldset'>
													<legend>Dates</legend>
													<table id='tbl_date' CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='height:100%;width:100%;'>
														<?php For($i=0; $i<count($lst_date['date']); $i++) { ?>
														<tr id='tr_<?php echo $lst_date['champ'][$i]; ?>'>
															<td style='text-align:right'>
																<label class='label'><u><?php echo $lst_date['lib'][$i]; ?> :</u></label>
															</td>
															<td style='text-align:left;width:100px;'>
																<div id="div_acte_<?php echo $lst_date['champ'][$i]; ?>" class="<?php If($lst_date['date'][$i] != 'N.D.'  || !in_array('acte_'.$lst_date['champ'][$i], $tbl_grant_elt_form_id)) {echo 'readonly';}?>">
																<input type='text' size='10' class='datepicker' id='acte_<?php echo $lst_date['champ'][$i]; ?>' name='acte_<?php echo $lst_date['champ'][$i]; ?>' value='<?php echo $lst_date['date'][$i]; ?>' <?php If( $lst_date['date'][$i] != 'N.D.' || !in_array('acte_'.$lst_date['champ'][$i], $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_'.$lst_date['champ'][$i];
																	$cur_info_mfu[$cur_info]['elt_val'] = $lst_date['date'][$i];
																?>
															</td>
															<td style='width:30%;'>
																<?php If($lst_date['date'][$i] != 'N.D.' && in_array('acte_'.$lst_date['champ'][$i], $tbl_grant_elt_form_id)) { ?>
																<img id='img_acte_<?php echo $lst_date['champ'][$i]; ?>' style='margin:-5px 0;cursor:pointer;' src='<?php echo ROOT_PATH; ?>/images/lock.png' width='25px' onclick="unlockField('img_acte_<?php echo $lst_date['champ'][$i]; ?>', 'div_acte_<?php echo $lst_date['champ'][$i]; ?>', 'acte_<?php echo $lst_date['champ'][$i]; ?>', 'popup_avertissement', '<?php echo $lst_date['date'][$i]; ?>')">
																<?php } Else { ?>
																&nbsp; 
																<?php } ?>
																</div>
																
																<?php 
																	// Ajout bouton pour corriger les doubles saisies PA PV
																	If ($type_acte == 'Acquisition') {
																		If ($date_compromis == $date_promesse && ($lst_date['champ'][$i] == 'date_compromis' || $lst_date['champ'][$i] == 'date_promesse') && $date_compromis != 'N.D.' && in_array('acte_'.$lst_date['champ'][$i], $tbl_grant_elt_form_id)) { 
																?>
																			<img id='img_acte_<?php echo $lst_date['champ'][$i]; ?>_suppr' style='cursor:pointer;' src='<?php echo ROOT_PATH; ?>/images/supprimer.png' width='15px' onclick="deleteField('img_acte_<?php echo $lst_date['champ'][$i]; ?>', 'img_acte_<?php echo $lst_date['champ'][$i]; ?>_suppr', 'div_acte_<?php echo $lst_date['champ'][$i]; ?>', 'acte_<?php echo $lst_date['champ'][$i]; ?>', '<?php echo $lst_date['date'][$i]; ?>')">
																<?php
																		}
																	}
																?>
																
															</td>
														</tr>
														<?php } ?>
														<?php If ($type_acte == 'Convention') { ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Durée initiale :</u></label>
															</td>
															<td>
																<div id="div_need_acte_duree" class="<?php If($info_mfu['statutmfu_id'] == 1 || $info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3 || !in_array('acte_duree', $tbl_grant_elt_form_id)) {echo 'readonly';}?>">
																<input type='text' size='4' class='editbox' style='text-align:right;' id='acte_duree' name='acte_duree' value='<?php echo $info_mfu['mu_duree']; ?>' <?php If($info_mfu['statutmfu_id'] == 1 || $info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3 || !in_array('acte_duree', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?>>
																<label class="label"> mois</label>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_duree';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['mu_duree'];
																?>
															</td>
															<td>
																<?php If(($info_mfu['statutmfu_id'] == 1 || $info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3) && $info_mfu['mu_duree'] >= 0 && in_array('acte_duree', $tbl_grant_elt_form_id)) { ?>
																<img id='img_need_acte_duree' style='margin:-5px 0;cursor:pointer;' src='<?php echo ROOT_PATH; ?>/images/lock.png' width='25px' onclick="unlockField('img_need_acte_duree', 'div_need_acte_duree', 'acte_duree', 'popup_avertissement', '<?php echo $info_mfu['mu_duree']; ?>')">
																<?php } Else { ?>
																&nbsp; 
																<?php } ?>
																</div>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Reconduction tacite :</u></label>
															</td>
															<td colspan='2'>
																<input type='checkbox' width='15px' class='editbox' style='-moz-transform: scale(1.2);' id='acte_chk_reconduction' name='acte_chk_reconduction' <?php If ($info_mfu['recond_tacite'] == 't') {echo "checked value='t'";} Else {echo "value='f'";} If($info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3 || !in_array('acte_chk_reconduction', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?> onclick="chgReconduction('<?php echo $info_mfu['recond_nbmax']; ?>')">
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_chk_reconduction';
																	If ($info_mfu['recond_tacite'] == 't') {
																		$cur_info_mfu[$cur_info]['elt_val'] = 't';
																	} Else {
																		$cur_info_mfu[$cur_info]['elt_val'] = 'f';
																	}
																?>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Nombre de reconduction maximum :</u></label>
															</td>
															<td colspan='2' style='min-width:152px'>
																<input type='text' size='4' class='editbox<?php If($info_mfu['recond_nbmax'] == -1 || !in_array('acte_recond_nbmax', $tbl_grant_elt_form_id)) {echo ' readonly';}?>' style='text-align:right;' id='acte_recond_nbmax' name='acte_recond_nbmax' value="<?php If ($info_mfu['recond_nbmax'] == -1) {echo "&infin;";} Else {echo $info_mfu['recond_nbmax'];} ?>" <?php If($info_mfu['recond_nbmax'] == -1 || !in_array('acte_recond_nbmax', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_recond_nbmax';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['recond_nbmax'];
																?>
																<label id='acte_lbl_ou' class='label'> ou</label>
																<input type='checkbox' width='15px' class='editbox' style='-moz-transform: scale(1.2);' id='acte_chk_infini' name='acte_chk_infini' <?php If ($info_mfu['recond_nbmax'] == -1) {echo "checked value='t'";} Else {echo "value='f'";} If($info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3 || !in_array('acte_recond_nbmax', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?> onclick='chk_infini()'>
																<label id='acte_lbl_infini' class='label'>infini</label>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_chk_infini';
																	If ($info_mfu['recond_tacite'] == 't') {
																		$cur_info_mfu[$cur_info]['elt_val'] = 't';
																	} Else {
																		$cur_info_mfu[$cur_info]['elt_val'] = 'f';
																	}
																?>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Pas de temps de la reconduction :</u></label>
															</td>
															<td colspan='2'>
																<input type='text' size='4' class='editbox <?php If(!in_array('acte_pas_tps', $tbl_grant_elt_form_id)) {echo ' readonly';}?>' style='text-align:right;' id='acte_pas_tps' name='acte_pas_tps' value='<?php echo $info_mfu['recond_duree']; ?>' <?php If($info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3 || !in_array('acte_pas_tps', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?>><label class="label"> mois</label>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_pas_tps';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['recond_duree'];
																?>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u>Date de rappel avant reconduction :</u></label>
															</td>
															<td colspan='2'>
																<input type='text' size='10' class='datepicker <?php If(!in_array('acte_date_rappel', $tbl_grant_elt_form_id)) {echo ' readonly';}?>' style='text-align:left;' id='acte_date_rappel' name='acte_date_rappel' value='<?php echo date_transform($info_mfu['recond_daterappel']); ?>' <?php If(!in_array('acte_date_rappel', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_date_rappel';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['recond_daterappel'];
																?>
															</td>
														</tr>
														<?php } ?>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr class='tr_fieldset'>
											<td>
												<fieldset id='fieldset_parcelle' class='fieldset'>
													<input type='hidden' id='nbr_parc_assoc_acte' value='<?php echo $info_mfu['nbr_parcelle'];?>'>
													<legend>Parcelles/ Surface</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='width:480px;height:100%;'>
														<tr>
															<td style='text-align:center;margin-bottom:20px;' colspan='2'>
																<label class="label" style='text-align:center;margin-bottom:20px;'><i>Pour modifier les parcelles associées à cet acte</br>rendez-vous sur l'onglet Parcelles/Lots</i></label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><?php echo $info_mfu['nbr_parcelle'];?></label>
															</td>
															<td>
																<label class="label">parcelle<?php If ($info_mfu['nbr_parcelle'] > 1) {echo 's';} ?></label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><?php echo $info_mfu['nbr_lot'];?></label>
															</td>
															<td>
																<label class="label">lot<?php If ($info_mfu['nbr_lot'] > 1) {echo 's';} ?></label>
																<label class="label" style='font-weight:normal;'>soit <?php echo conv_are($info_mfu['surf_lo_total']); ?> ha.a.ca</label>
															</td>
														</tr>
														<tr>
															<td style='text-align:right'>
																<label class="label"><u>Surface <?php If ($type_acte == 'Acquisition') {echo 'acquise';} Else If ($type_acte == 'Convention') {echo 'conventionnée';} ?> :</u></label>
															</td>
															<td>
																<label class="label">
																<?php 
																	If ($info_mfu['nbr_parcelle'] > 0) {
																		echo conv_are($info_mfu['surf_lo_reel']).' ha.a.ca';
																	} Else {
																		echo conv_are(0);
																	}
																?>
																</label>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
											<td>
												<fieldset id='fieldset_frais' class='fieldset'>
													<legend>Montants</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='width:100%;'>
														<?php 
														$rq_lst_type_frais = 
														"SELECT t1.id, t1.lib, COALESCE(t2." . $suffixe . "frais, 0.00) as montant
														FROM
															(SELECT d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_id as id, d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_lib as lib FROM foncier.d_typfrais" . $suffixe . " ORDER BY lib) t1
														LEFT JOIN
															(SELECT d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_id as id, d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_lib, r_frais_" . $suffixe . "." . $suffixe . "frais FROM foncier.r_frais_" . $suffixe . ", foncier.d_typfrais" . $suffixe . " WHERE r_frais_" . $suffixe . ".typfrais" . $suffixe . "_id = d_typfrais" . $suffixe . ".typfrais" . $suffixe . "_id AND r_frais_" . $suffixe . "." . $suffixe . "_id = '$acte_id') t2
														ON t1.id = t2.id
														ORDER BY lib";
														$res_lst_type_frais = $bdd->prepare($rq_lst_type_frais);
														$res_lst_type_frais->execute();
														While ($donnees = $res_lst_type_frais->fetch()) { ?>
														<tr>
															<td style='text-align:right'>
																<label class='label'><u><?php echo $donnees['lib']; ?> :</u></label>
															</td>
															<td>
																<input type='text' size='10' class='editbox <?php If(!in_array('acte_frais_', $tbl_grant_elt_form_id)) {echo ' readonly';}?>' style='text-align:right;' id='acte_frais_<?php echo $donnees['id']; ?>' name='acte_frais_<?php echo $donnees['id']; ?>' value='<?php echo $donnees['montant']; ?>' <?php If(!in_array('acte_frais_', $tbl_grant_elt_form_id)) {echo ' READONLY DISABLED';}?>><label class="label"> €</label>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_frais_'.$donnees['id'];
																	$cur_info_mfu[$cur_info]['elt_val'] = $donnees['montant'];
																 ?>
															</td>
														</tr>
													<?php
														} 
														$res_lst_type_frais->closeCursor();
													?>
													</table>
												</fieldset>
											</td>
										</tr>
										<?php  If ($type_acte == 'Acquisition') { ?> 
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Notaire</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='width:100%;height:100%;'>
														<tr>
															<td style='text-align:left;width:50%;'>
																<div id='div_acte_notaire' <?php If (!in_array('acte_notaire', $tbl_grant_elt_form_id)) {echo "class='readonly'";} ?>>
																	<label class="btn_combobox">			
																		<select name="acte_notaire" id="acte_notaire" class="combobox" style='<?php If (!in_array('acte_notaire', $tbl_grant_elt_form_id)) {echo " readonly";} ?>' <?php If (!in_array('acte_notaire', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED";} ?>>
																			<option value='NULL'></option>
																		<?php																	
																			
																			$rq_lst_notaire =
																			"SELECT personne_id as notaire_id, nom  || ' ' || prenom || COALESCE(CASE WHEN t_nb_pers.nb_pers > 1 THEN ' (' || personne.adresse3 || ')' END, '') as notaire_lib
																			FROM personnes.personne 
																				JOIN personnes.d_particules USING (particules_id)
																				JOIN personnes.d_individu USING (individu_id)
																				JOIN (SELECT individu_id, count(*) AS nb_pers FROM personnes.d_individu JOIN personnes.personne USING (individu_id) GROUP BY individu_id) t_nb_pers USING (individu_id)
																			WHERE personne.fonctpers_id = ".FONCTNOT_ID."
																			ORDER BY nom";
																			$res_lst_notaire = $bdd->prepare($rq_lst_notaire);
																			$res_lst_notaire->execute();
																			While ($donnees = $res_lst_notaire->fetch()) {
																				echo "<option ";
																				If ($donnees['notaire_id'] == $info_mfu['notaire_id']) {
																					echo 'selected ';
																				}
																				echo "value='$donnees[notaire_id]'>$donnees[notaire_lib]</option>";
																			}
																			$res_lst_notaire->closeCursor();
																		?>				
																		</select>
																		<?php
																			$cur_info++;
																			$cur_info_mfu[$cur_info]['elt_name'] = 'acte_notaire';
																			$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['notaire_id'];
																		?>
																	</label>																
																<?php 
																	If (in_array('acte_notaire', $tbl_grant_elt_form_id)) {
																		$vget = "type_contact:notaire;origine:mfu;";
																		$vget_crypt = base64_encode($vget);
																		$lien = ROOT_PATH . "contacts/new_contact.php?d=$vget_crypt";
																?>
																	<a target="_blank" href="<?php echo $lien; ?>">
																		<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour gérer la liste des notaires">
																	</a>
																<?php
																	}
																?>
																</div>
															</td>
															<td style='text-align:left;'>
																<label class='label'>Contact : </label><input type='text' size='70' class='editbox <?php If (!in_array('acte_notaire', $tbl_grant_elt_form_id)) {echo " readonly";} ?>' style='' id='acte_notaire_contact' name='acte_notaire_contact' value='<?php echo $info_mfu['notaire_contact'];?>'<?php If (!in_array('acte_notaire', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED";} ?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_notaire_contact';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['notaire_contact'];
																?>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Suivi administratif</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='height:100%;'>
														<tr>
															<td style='text-align:left;'>
																<label class='label'><u>Acte re&ccedil;u :</u></label>
																<input type='checkbox' class='editbox'  style='-moz-transform: scale(1.2);' id='acte_chk_recep_courrier' name='acte_chk_recep_courrier' <?php If ($info_mfu['mf_courier_recep_notaire'] == 't') {echo "checked value='t'";} Else {echo "value='f'";} If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";}?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_chk_recep_courrier';
																	If ($info_mfu['mf_courier_recep_notaire'] == 't') {
																		$cur_info_mfu[$cur_info]['elt_val'] = 't';
																	} Else {
																		$cur_info_mfu[$cur_info]['elt_val'] = 'f';
																	}
																?>
															</td>
															<td style='text-align:left;'>
																<label class='label'>le : </label>
																<input type='text' size='10' class='datepicker <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " readonly";} ?>' style='text-align:left;' id='acte_date_recep_courrier' name='acte_date_recep_courrier' value='<?php echo date_transform($info_mfu['mf_courier_recep_notaire_date']); ?>' onblur="autoChecked('acte_date_recep_courrier', 'acte_chk_recep_courrier');" <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";} ?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_date_recep_courrier';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['mf_courier_recep_notaire_date'];
																?>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>
										<?php } Else If ($type_acte == 'Convention') { ?> 
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Suivi administratif</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border' style='height:100%;'>
														<tr>
															<td style='text-align:left;'>
																<label class='label'><u>Convention sign&eacute;e et retourn&eacute;e aux propri&eacute;taires :</u></label>
																<input type='checkbox' class='editbox'  style='-moz-transform: scale(1.2);' id='acte_chk_envoi_courrier' name='acte_chk_envoi_courrier' <?php If ($info_mfu['mu_courier_envoi_proprio'] == 't') {echo "checked value='t'";} Else {echo "value='f'";} If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";}?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_chk_envoi_courrier';
																	If ($info_mfu['mu_courier_envoi_proprio'] == 't') {
																		$cur_info_mfu[$cur_info]['elt_val'] = 't';
																	} Else {
																		$cur_info_mfu[$cur_info]['elt_val'] = 'f';
																	}
																?>
															</td>
															<td style='text-align:left;'>
																<label class='label'>le : </label>
																<input type='text' size='10' class='datepicker <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " readonly";} ?>' style='text-align:left;' id='acte_date_envoi_courrier' name='acte_date_envoi_courrier' value='<?php echo date_transform($info_mfu['mu_courier_envoi_proprio_date']); ?>' onblur="autoChecked('acte_date_envoi_courrier', 'acte_chk_envoi_courrier');" <?php If (!in_array(10, $tbl_grant_elt_id)) {echo " READONLY DISABLED";} ?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_date_envoi_courrier';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['mu_courier_envoi_proprio_date'];
																?>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>
										<?php } ?>
										<tr>
											<td colspan='2'>
												<fieldset class='fieldset'>
													<legend>Commentaires</legend>
													<table CELLSPACING = '1' CELLPADDING ='1' class='no-border bilan-foncier' style='min-width:400px;height:100%;'>
														<tr>
															<td>
																<label class='label'><u>Clauses particulières :</u></label>
																<input type='checkbox' class='editbox'  style='-moz-transform: scale(1.2);' id='acte_chk_clauses' name='acte_chk_clauses' <?php If ($info_mfu['mfu_clauses_part'] == 't') {echo "checked value='t'";} Else {echo "value='f'";} If (!in_array('acte_chk_clauses', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED";}?>>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_chk_clauses';
																	If ($info_mfu['mfu_clauses_part'] == 't') {
																		$cur_info_mfu[$cur_info]['elt_val'] = 't';
																	} Else {
																		$cur_info_mfu[$cur_info]['elt_val'] = 'f';
																	}
																?>
															</td>
														</tr>
														<tr>
															<td>
																<div <?php If (!in_array('acte_txt_com', $tbl_grant_elt_form_id)) {echo " class='readonly'";}?>>
																	<textarea onkeyup="resizeTextarea('acte_txt_com');" onchange="resizeTextarea('acte_txt_com');" id='acte_txt_com' name='acte_txt_com' class="label textarea_com_edit" rows='3' cols='1000' <?php If (!in_array('acte_txt_com', $tbl_grant_elt_form_id)) {echo " READONLY DISABLED";}?>><?php echo $info_mfu['mfu_commentaires'];?></textarea>
																</div>
																<?php
																	$cur_info++;
																	$cur_info_mfu[$cur_info]['elt_name'] = 'acte_txt_com';
																	$cur_info_mfu[$cur_info]['elt_val'] = $info_mfu['mfu_commentaires'];
																?>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>
									</table>
								</center>
							</td>
						</tr>
						<tr>
							<td colspan='3' class='save'>
								<div class='lib_alerte' id='prob_saisie' style='display:none;'>Veuillez compl&eacuteter votre saisie</div>
								<?php
									$cur_info_mfu_elt_name = implode("*;*", array_column($cur_info_mfu, 'elt_name'));
									$cur_info_mfu_elt_val = implode("*;*", array_column($cur_info_mfu, 'elt_val'));
								?>
								<input type='hidden' name='cur_info_mfu_elt_name' value="<?php echo $cur_info_mfu_elt_name; ?>">
								<input type='hidden' name='cur_info_mfu_elt_val' value="<?php echo $cur_info_mfu_elt_val; ?>">
								<input type='submit' name='valid_form' id='valid_form' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
							</td>
						</tr>
						<tr>
							<td style='text-align:right' colspan='3'>
								<label class="last_maj">
									Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($info_mfu['maj_date'], 0, 10)); ?> par <?php echo $info_mfu['maj_user'];?>
								</label>
							</td>
						</tr>
					</table>
					</form>
					<?php } ?>
				</div id='general'>
				
				<div id='parcelle'>
					<input type='hidden' id='mode' value='<?php echo $mode; ?>';>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<!--<center>-->
								<table width = '100%' class='no-border' id='tbl_parc' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody>
										<form id="frm_export_parc" name="frm_export_parc" onsubmit="return false" action='foncier_export.php?filename=<?php echo $acte_id; ?>_parcelles.csv' method='POST'>
											<label class="label black">
												Exporter :
													<img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px' style="cursor:pointer;" onclick="exportTable('frm_export_parc', 'tbl_parc_entete_fixed', 'tbl_parc_corps');">
													<input type="hidden" id="tbl_post" name="tbl_post" value="">
													<span style='padding-left:75px;'></span>
											</label>
											<?php If ($mode == 'edit' && $verif_role_acces == 'OUI' && ($info_mfu['statutmfu_id'] == 0 || $info_mfu['statutmfu_id'] == 1) && in_array(3, $tbl_grant_elt_id)) { ?>
												<span id="div_btn_modif_parc" class="<?php If($info_mfu['statutmfu_id'] == 1 || $info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3 || !in_array(3, $tbl_grant_elt_id)) {echo 'readonly';}?>">
													<label class="label black">
														Modifier :
															<input id='btn_modif_parc' type="button" value="La liste des parcelles" class='btn_frm' onclick="addParc_js('explorateur', 'onglet');" <?php If ($info_mfu['statutmfu_id'] == 1 || $info_mfu['statutmfu_id'] == 2 || $info_mfu['statutmfu_id'] == 3 || !in_array(3, $tbl_grant_elt_id)) { echo " READONLY DISABLED"; } ?>/>
													</label>
															<?php If($info_mfu['statutmfu_id'] == 1 && in_array('acte_parc', $tbl_grant_elt_form_id)) { ?>
															<label class="label black">
																<img id='img_btn_modif_parc' style='margin:-5px 0;cursor:pointer;' src='<?php echo ROOT_PATH; ?>/images/lock.png' width='25px' onclick="unlockField('img_btn_modif_parc', 'div_btn_modif_parc', 'btn_modif_parc', 'popup_avertissement', 'La liste des parcelles')">
															</label>
															<?php } ?>
													
												</span>
												 <?php } Else { ?>
												 &nbsp;
												 <?php } ?>
										</form>
									</tbody>									
									<tbody id='tbl_parc_entete'>
										<form id="frm_parc_entete" onsubmit="return false">
											<tr>
												<td class='filtre' align='center' width='90px'>
													<label class="btn_combobox">
														<select name="filtre_site_id" onchange="create_lst_parc('modif', 0, '<?php echo $acte_id; ?>');" id="filtre_site_id" class="combobox" style='text-align:center'>
															<option value='-1'>SITE ID</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center'>
													<label class="btn_combobox">
														<select name="filtre_commune" onchange="create_lst_parc('modif', 0, '<?php echo $acte_id; ?>');" id="filtre_commune" class="combobox" style='text-align:center'>
															<option value='-1'>Commune</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='90px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_parc('modif', 0, '<?php echo $acte_id; ?>');" type='text' id='filtre_section' size='6' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';"> 
												</td>
												<td class='filtre' align='center' width='90px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_parc('modif', 0, '<?php echo $acte_id; ?>');" type='text' id='filtre_par_num' size='6' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';"> 
												</td>
												<td align='center' width='120px'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='12' value='Contenance' DISABLED> 
												</td>
												<td class='filtre' align='center' width='90px'>
													<label class="btn_combobox">
														<select name="filtre_typ_prop" onchange="create_lst_parc('modif', 0, '<?php echo $acte_id; ?>');" id="filtre_typ_prop" class="combobox" style='text-align:center'>
															<option value='-1'>TYP_PROP</option>
														</select>
													</label>
												</td>
												<td align='center' width='180px' colspan='2'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_lots' size='3' value='Lots' DISABLED> 
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_mfu" onchange="create_lst_parc('modif', 0, '<?php echo $acte_id; ?>');" id="filtre_mfu" class="combobox" style='text-align:center'>
															<option value='-1'>MFU</option>
														</select>
													</label>
												</td>												
												<td align='center' width='120px'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_surf_maitrise' size='12' value='Surf. ma&icirc;tris&eacute;e' DISABLED> 
												</td>
												<td width='25px'>	
													<img onclick="
													document.getElementById('filtre_site_id').value='-1';
													document.getElementById('filtre_commune').value='-1';
													document.getElementById('filtre_section').value='';
													document.getElementById('filtre_par_num').value='';
													document.getElementById('filtre_typ_prop').value='-1';
													document.getElementById('filtre_mfu').value='-1';
													create_lst_parc('charge', 0, '<?php echo $acte_id; ?>');
													" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>											
										</form>
									</tbody>									
									<thead id="tbl_parc_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
										<tr>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>SITE ID</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>Commune</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>Lieu-dit</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>PAR ID</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>CODE INSEE</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>SECTION</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>PAR NUM</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Contenance</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>TYP_PROP</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Lots</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Conten.</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>MFU</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Surf. ma&icirc;tris&eacute;e</label>
											</th>
											<th style='text-align:center;border-top:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>&nbsp;</label>
											</th>
										</tr>
									</thead>
									<tbody id='tbl_parc_corps' class='tbody_contenu'>
									
									</tbody>
								</table>
								<!--</center>-->
							</td>
						</tr>
					</table>
				</div id='parcelle'>
				
				<div id='proprio'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<?php
									$vget = "page:fiche_mfu.php;type_acte:$type_acte;champ_id:$acte_id;";
									$vget_crypt = base64_encode($vget);
									$lien = "foncier_publipostage.php?d=$vget_crypt";
								?>
								<label class="label">Publipostage :
									<a href="<?php echo $lien;?>"><img src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px'></a>
								</label>
								<table width = '100%' class='no-border' id='tbl_prop' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody id='tbl_proprio_entete'>
										<form id="frm_proprio_entete" onsubmit="return false">
											<tr>
												<td class='filtre' align='right' width='50%'>
													<input class='editbox' style="text-align:center" oninput="create_lst_proprio(0, '<?php echo $acte_id; ?>');" type='text' id='filtre_nom' size='35' placeholder='Propri&eacute;taire' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Propri&eacute;taire';">
												</td>
												<td align='left' style="padding-left:18px;">
													<img onclick="document.getElementById('filtre_nom').value=''; create_lst_proprio(0, '<?php echo $acte_id; ?>');" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
										</form>
									</tbody>
									<tbody id='tbl_proprio_corps'>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div id='proprio'>
			</div>			
			
			<div id="explorateur" class="explorateur_span" style="display:none;">
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE" width='100%'>
					<tbody>
						<tr>
							<td>
								<div id="explorateur_new">
									<table class="explorateur_contenu" cellspacing="5" cellpadding="5" border="0" align="center" frame="box" rules="NONE" width='100%'>
										<tbody>
											<tr>
												<td style="text-align:center;">
													<label class='label'>Date de mise à jour des parcelles : </label>
													<input type='text' size='10' class='datepicker <?php If (!in_array(3, $tbl_grant_elt_id)) {echo " readonly";} ?>' style='text-align:left;' id='acte_date_maj_parc' name='acte_date_maj_parc' <?php If (!in_array(3, $tbl_grant_elt_id)) {echo " READONLY DISABLED";} ?>>
													<label class='last_maj'>(utilisée pour les bilans par période)</label>
												</td>
												<td style="text-align:right;width:25px;">
													<img width="15px" onclick="cacheMess('explorateur');cacheMess('explorateur_alerte');changeOpac(100, 'onglet');" src="<?php echo ROOT_PATH; ?>images/quitter.png" style="cursor:pointer">
												</td>
											</tr>
											<tr>
												<td class="label">
													<form id="frm_parc_modif" onsubmit="return false">
														<table CELLSPACING = '0' CELLPADDING ='3' border='0' ALIGN='CENTER' width='100%'>
															<tr>
																<td style='text-align:center'>Parcelles disponibles</td>
																<td>&nbsp;</td>
																<td style='text-align:center'>Parcelles associées à l'acte</td>
															</tr>
															<tr>
																<td width='50%'>
																	<select style='width:100%;height:380px' class='info_contenu' multiple='multiple' id='parc_dispo' name='parc_dispo[]'>";
																		<?php 
																			$res_parc_dispo = $bdd->prepare($rq_parc_dispo);
																			$res_parc_dispo->execute();
																			while ($donnees = $res_parc_dispo->fetch()) {
																				If (strpos($donnees['cad_site_val'], 'Convention') > 0) {
																					echo "<option style='color:#d77a0f;' value='$donnees[cad_site_id]'>$donnees[cad_site_val]</option>";
																				} Else {
																					echo "<option style='color:#004494;' value='$donnees[cad_site_id]'>$donnees[cad_site_val]</option>";
																				}
																			}
																		?>
																	</select>
																</td>
																<td width='50px' align='center'>
																	<label class='prec_suiv' onclick="parcAjoutSuppr('frm_parc_modif', 'ajout');">
																		>>>
																	</label>
																	<br>
																	<br>
																	<br>
																	<label class='prec_suiv' onclick="parcAjoutSuppr('frm_parc_modif', 'suppr');">
																		<<<
																	</label>																	
																</td>
																<td width='50%'>
																	<select style='width:100%;height:380px' multiple='multiple' class='info_contenu' id='parc_assoc_acte' name='parc_assoc_acte[]'>";
																		<?php 		
																			$current_parc_conv = 'NON';															
																			while ($donnees = $res_parc_assoc_acte->fetch()) {
																				If ($donnees['etat_foncier'] == 'prob') {
																					echo "<option style='text-decoration:line-through;' value='$donnees[cad_site_id]'>$donnees[cad_site_val]</option>";
																				} Else If (substr_count($donnees['cad_site_val'], 'Convention') == 1) {
																					echo "<option style='color:#d77a0f;' value='$donnees[cad_site_id]'>$donnees[cad_site_val]</option>";
																					$current_parc_conv = 'OUI';
																				} Else {
																					echo "<option style='color:#004494;' value='$donnees[cad_site_id]'>$donnees[cad_site_val]</option>";
																				}
																			}
																		?>
																	</select>
																</td>
															</tr>
															<tr>
																<td>
																	<input style='width:100%; display:none' type='text' id='parc_ajout'>
																</td>
																<td width='50px' align='center'>
																	
																</td>
																<td>
																	<input style='width:100%; display:none' type='text' id='parc_suppr'>
																</td>
															</tr>
														</table>
													</form>
												</td>
											</tr>
											<tr>
												<td class='save'>
													<label id="lbl_parcModify" class="lib_alerte" style="display: none;">
														<b>Vous devez associer au moins une parcelle</b>
													</label>
													<input id='btn_parcModify' name='btn_parcModify' type="button" onclick="parcModify('frm_parc_modif', '<?php echo $acte_id; ?>', '<?php echo $type_acte; ?>');"  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div id="explorateur_alerte" class="lib_alerte" style="display: none; top: 271px;">
									<b>Veuillez saisir une donnée</b>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<input type='hidden' id='current_parc_conv' value='<?php echo $current_parc_conv; ?>'>
			</div>
			<?php 
			$rq_etat_foncier = "
			SELECT CASE WHEN ? IN (
			SELECT DISTINCT mfu_id as acte_id FROM alertes.v_prob_mfu_baddata 
			UNION 
			SELECT DISTINCT mfu_id as acte_id FROM alertes.v_prob_cadastre_cen
			UNION 
			SELECT DISTINCT mfu_id as acte_id FROM alertes.v_prob_mfu_sansparc
			UNION 
			SELECT DISTINCT mu_id as acte_id FROM alertes.v_alertes_mu WHERE alerte = 'terminée'
			) THEN 1 ELSE 0  END as etat_bilan";
			If (isset($rq_etat_foncier)) {
				$res_etat_foncier = $bdd->prepare($rq_etat_foncier);
				$res_etat_foncier->execute(array($acte_id));
				$etat_foncier_tmp = $res_etat_foncier->fetch();
				$etat_foncier = $etat_foncier_tmp['etat_bilan'];
			}
			
			If ($mode == 'consult' && $etat_foncier > 0) {?>
				<div class="prob_conv">
					<table cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
						<tbody>
							<tr>
								<td>
									<label>
										<b>Cet acte doit être mis à jour par le service foncier</b>
									</label>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			<?php 
			}			
			?>
			<div id="explorateur_histo" class="explorateur_span" style="display: none;">
				<div id="explorateur_new">
					<table class="explorateur_contenu" cellspacing="0" cellpadding="4" border="0" align="center" frame="box" rules="NONE">
						<tbody id='tbody_histo'>
						</tbody>
					</table>
				</div>
			</div>
			<div id="popup_avertissement" class="popup_avertissement" style="display: none;">						
				<img width="15px" onclick="cacheMess('popup_avertissement');" src="<?php echo ROOT_PATH; ?>images/quitter.png">
				<label>Vous vous apprétez à modifier une donnée vachement importante<br>Faisez bien gaffe</label>									
			</div>
			<div id="loading" class="loading" style="display: none;">
				<img src="<?php echo ROOT_PATH; ?>images/ajax-loader.gif">
			</div>
		</section>		
		<?php 
			$res_info_mfu->closeCursor();
			$res_check_grant->closeCursor();
			$res_parc_assoc_acte->closeCursor();
			$res_etat_foncier->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
