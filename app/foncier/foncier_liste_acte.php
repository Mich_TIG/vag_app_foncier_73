<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip" style='background-color:#d66a6a;border-color:#ec1f00 #a01200 #a01200 #ec1f00;color:#cc0700; max-width:250px;font-size:10pt;font-weight:bold;'></div>
		<script type="text/javascript" src="foncier.js"></script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Foncier';
			$h3 ='Recherche par acte';
			include ('../includes/valide_acces.php');
			include("../includes/header.php");
			include('../includes/breadcrumb.php');
			include("foncier_fct.php");
			include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure	
			//include('../includes/construction.php');
			
			try
			{
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e)
			{
				die(include('../includes/prob_tech.php'));
			}		
			
			$rq_lst_acte = "
			SELECT tt1.*, CASE WHEN tt2.acte_id IS NOT NULL THEN 'prob' ELSE 'ok' END as etat_acte
			FROM 	
				(
					SELECT DISTINCT 
						t4.site_id, 'Acquisition' as type_acte, mf_acquisitions.mf_id as acte_id, COALESCE(mf_acquisitions.mf_lib, 'N.D.') as acte_lib, d_typmf.typmf_lib as acte_typ ,
						 mf_acquisitions.date_sign_acte as acte_date, CASE WHEN d_statutmf.statutmf_id = 1 THEN 'En cours' WHEN d_statutmf.statutmf_id = 2 THEN 'Terminée' ELSE d_statutmf.statutmf END as acte_statut, (CASE WHEN (substr(mf_acquisitions.mf_pdf,  length(mf_acquisitions.mf_pdf)-3, 4)) = '.pdf' THEN 'OUI' ELSE 'NON' END) as acte_doc_pdf
					FROM 
						foncier.cadastre_site t4, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf,
						(
							SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date 
							FROM foncier.r_mfstatut 
							GROUP BY r_mfstatut.mf_id
						) as tt1	 				
					WHERE 
						t4.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.typmf_id = d_typmf.typmf_id AND 
						mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id AND 
						(r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id)

					UNION 				

					SELECT DISTINCT 
						t4.site_id, 'Convention' as type_acte, mu_conventions.mu_id as acte_id, COALESCE(mu_conventions.mu_lib, 'N.D.') as acte_lib, d_typmu.typmu_lib as acte_typ, 
						mu_conventions.date_effet_conv as acte_date, CASE WHEN d_statutmu.statutmu_id = 1 THEN 'En cours' WHEN d_statutmu.statutmu_id IN (2, 4) THEN 'Terminée' ELSE d_statutmu.statutmu END as acte_statut, (CASE WHEN (substr(mu_conventions.mu_pdf,  length(mu_conventions.mu_pdf)-3, 4)) = '.pdf' THEN 'OUI' ELSE 'NON' END) as acte_doc_pdf
					FROM
						foncier.cadastre_site t4, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu,
						(
							SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date 
							FROM foncier.r_mustatut 
							GROUP BY r_mustatut.mu_id
						) as tt1	 				
					WHERE 
						t4.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.typmu_id = d_typmu.typmu_id AND 
						mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id AND 
						(r_mustatut.date = tt1.statut_date AND r_mustatut.mu_id = tt1.mu_id)
											
					ORDER BY type_acte , acte_date
				) tt1 
				LEFT JOIN
				(SELECT DISTINCT mfu_id as acte_id FROM alertes.v_prob_mfu_baddata 
				UNION 
				SELECT DISTINCT mfu_id as acte_id FROM alertes.v_prob_cadastre_cen
				UNION 
				SELECT DISTINCT mfu_id as acte_id FROM alertes.v_prob_mfu_sansparc
				UNION 
				SELECT DISTINCT mu_id as acte_id FROM alertes.v_alertes_mu WHERE alerte = 'terminée') tt2
				USING (acte_id)
			ORDER BY site_id, type_acte";
			$res_lst_acte = $bdd->prepare($rq_lst_acte);
			$res_lst_acte->execute();
			$lst_acte = $res_lst_acte->fetchAll();
		?>
		<section id='main'>
			<h3>Recherche par acte</h3>
			<table id='tbl_acte' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
				<tbody id='tbl_acte_entete'>
					<form id="frm_acte_entete" onsubmit="return false">
						<tr>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 0, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 0, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='75px'>
								<input style='text-align:center' class='editbox' oninput="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" type='text' id='filtre_site_id' size='6' placeholder='SITE ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SITE ID';"> 
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 1, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 1, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<label class="btn_combobox">			
									<select onchange="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" id="filtre_acte" class="combobox" style='text-align:center'>
										<option value='-1'>Acte</option>
										<?php
										$tab_acte_type_unique = array_values(array_unique(array_column($lst_acte, 'type_acte')));
										For ($j=0; $j<count($tab_acte_type_unique); $j++)
											{echo "<option value='" . $j . "'>" . $tab_acte_type_unique[$j] . "</option>";}
										?>				
									</select>
								</label>
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer; top:1px" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 2, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 2, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left'>
								<input style='text-align:center;min-width:80px;' class='editbox editbox_larger' oninput="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" type='text' id='filtre_acte_id' placeholder='ACTE ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'ACTE ID';"> 
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 3, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 3, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='100%'>
								<input style='text-align:center; min-width:90%' class='editbox editbox_larger' oninput="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" type='text' id='filtre_acte_lib' placeholder='Libellé' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Libellé';"> 
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 4, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 4, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<label class="btn_combobox">			
									<select onchange="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" id="filtre_type" class="combobox" style='text-align:center'>
										<option value='-1'>Type de l'acte</option>
										<?php
										$tab_acte_type_unique = array_values(array_unique(array_column($lst_acte, 'acte_typ')));
										sort($tab_acte_type_unique);
										For ($j=0; $j<count($tab_acte_type_unique); $j++) {
											echo "<option value='" . $j . "'>" .  preg_replace('#\((.+)\)#i', '', $tab_acte_type_unique[$j]) . "</option>";
										}
										?>
									</select>
								</label>
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 5, 'date', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 5, 'date', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<input style='text-align:center' class='editbox' size='10' oninput="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" type='text' id='filtre_date' placeholder='Date' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Date';"> 
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 6, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 6, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<label class="btn_combobox">			
									<select onchange="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" id="filtre_statut" class="combobox" style='text-align:center' >
										<option value='-1'>Maîtrise</option>
										<?php
										$tab_acte_statut_unique = array_values(array_unique(array_column($lst_acte, 'acte_statut')));
										For ($j=0; $j<count($tab_acte_statut_unique); $j++) {
											echo "<option value='" . $j . "'>" . $tab_acte_statut_unique[$j] . "</option>";
										}
										?>				
									</select>
								</label>
							</td>
							<td class='ordre' align='right' width='10px'>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td style='padding-bottom:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 7, 'txt', 'desc');" src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='10px'>
										</td>
									</tr>
									<tr>
										<td style='padding-top:4px;'>
											<img style="cursor:pointer" onclick="sortTable('tbl_acte', 'tbl_acte_corps', 7, 'txt', 'asc');" src="<?php echo ROOT_PATH; ?>images/croissant.png" width='10px'>
										</td>
									</tr>
								</table>
							</td>
							<td class='filtre' align='left' width='1px'>
								<label class="btn_combobox">			
									<select onchange="maj_table('frm_acte_entete', 'tbl_acte_corps', 0);" id="filtre_doc_pdf" class="combobox" style='text-align:center'>
										<option value='-1'>PDF</option>
										<?php
										$tab_acte_pdf_unique = array_values(array_unique(array_column($lst_acte, 'acte_doc_pdf')));
										For ($j=0; $j<count($tab_acte_pdf_unique); $j++) {
											echo "<option value='" . $j . "'>" . $tab_acte_pdf_unique[$j] . "</option>";
										}
										?>				
									</select>
								</label>
							</td>
							<td width='5'>
								<img style='cursor:pointer;'onclick="
								document.getElementById('filtre_site_id').value='';
								document.getElementById('filtre_acte').value='-1';
								document.getElementById('filtre_acte_id').value='';
								document.getElementById('filtre_acte_lib').value='';
								document.getElementById('filtre_type').value='-1';
								document.getElementById('filtre_date').value='';
								document.getElementById('filtre_statut').value='-1';
								document.getElementById('filtre_doc_pdf').value='-1';
								maj_table('frm_acte_entete', 'tbl_acte_corps', 0);
								" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
							</td>
						</tr>
					</form>
				</tbody>
				<tbody id='tbl_acte_corps'>
				<?php
					For ($i=0; $i<$res_lst_acte->rowCount(); $i++) {
						If($i%2)
							{$tr=1;}
						Else
							{$tr=0;}
						$vget = "type_acte:" . $lst_acte[$i]['type_acte'] . ";mode:consult;acte_id:" . $lst_acte[$i]['acte_id'] . ";";
						$vget_crypt = base64_encode($vget);
						$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_mfu.php?d=$vget_crypt#general";
						If ($lst_acte[$i]['etat_acte'] == 'prob') {
							echo "<tr class='prob_acte toggle_tr l$tr' onclick=\"document.location='$lien'\" onmouseover=\"tooltip.show(this)\" onmouseout=\"tooltip.hide(this)\" title=\"Cet acte n'est pas à jour\">";
						} Else {
							echo "<tr class='toggle_tr l$tr' onclick=\"document.location='$lien'\">";
						}
				?>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;'><?php echo $lst_acte[$i]['site_id']; ?></span></a></center>
						</td>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;'><?php echo $lst_acte[$i]['type_acte']; ?></span></a></center>
						</td>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;'><?php echo $lst_acte[$i]['acte_id']; ?></span></a></center>
						</td>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;'><?php If (strlen($lst_acte[$i]['acte_lib']) > 25) {echo utf8_encode(substr(utf8_decode($lst_acte[$i]['acte_lib']), 0, 25)) . '[...]';} Else {echo $lst_acte[$i]['acte_lib'];}?></span></a></center>
						</td>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;'><?php echo preg_replace('#\((.+)\)#i', '', $lst_acte[$i]['acte_typ']); ?></span></a></center>
						</td>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;'><?php echo date_transform($lst_acte[$i]['acte_date']); ?></span></a></center>
						</td>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;'><?php echo $lst_acte[$i]['acte_statut']; ?></span></a></center>
						</td>
						<td colspan='2'>
							<center><a href='<?php echo $lien; ?>'><span style='text-align:right;margin-right:5px;'><?php echo $lst_acte[$i]['acte_doc_pdf']; ?></span></a></center>
						</td>
					</tr>
				<?php
					}
				?>
				</tbody>
			</table>
		</section>
		<?php 
			$res_lst_acte->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
