<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include('../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">		
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/explorateur.css">		
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip" style='background-color:#d66a6a;border-color:#ec1f00 #a01200 #a01200 #ec1f00;color:#cc0700; max-width:250px;font-size:10pt;font-weight:bold;'></div>
		<script type="text/javascript" src="foncier.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig', 'grp_foncier'));
			$h2 ='Foncier';
			include('../includes/header.php');
			include('../includes/breadcrumb.php');
			//include('../includes/construction.php');
			include('foncier_fct.php');			
			include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}		
			
			If (isset($_GET['d']) && substr_count(base64_decode($_GET['d']), ':') > 0 && substr_count(base64_decode($_GET['d']), ';') > 0) {
				$d= base64_decode($_GET['d']);
				$min_crit = 0;
				$max_crit = strpos($d, ':');
				$min_val = strpos($d, ':') + 1;
				$max_val = strpos($d, ';') - $min_val;
				
				For ($p=0; $p<substr_count($d, ':'); $p++) {
					$crit = substr($d, $min_crit, $max_crit);
					$value = substr($d, $min_val, $max_val);
					$$crit = $value;
					$min_crit = strpos($d, ';', $min_crit) + 1;
					$max_crit= strpos($d, ':', $min_crit) - $min_crit;
					$min_val = $min_crit + $max_crit + 1;
					$max_val = strpos($d, ';', $min_val) - $min_val;
				}
				If (!isset($site_id) OR !isset($site_nom)) {
					$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
					include('../includes/prob_tech.php');
				}
			} Else {
				$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php';
				include('../includes/prob_tech.php');
			}
			
			$rq_info_parcelle =
			"SELECT coalesce(count(distinct t1.par_id),0) as nbr_parcelle, coalesce(count(*),0) as nbr_lot, coalesce(sum(dcntlo),0) as surf_total
			FROM cadastre.parcelles_cen t1, cadastre.lots_cen t2, cadastre.cadastre_cen t3, foncier.cadastre_site t4
			WHERE t1.par_id = t2.par_id AND t2.lot_id = t3.lot_id AND t3.cad_cen_id = t4.cad_cen_id AND t4.site_id = ? AND t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'";
			$res_info_parcelle = $bdd->prepare($rq_info_parcelle);
			$res_info_parcelle->execute(array($site_id));
			$info_parcelle = $res_info_parcelle->fetch();
			If ($res_info_parcelle->rowCount() == 0){
				include('../includes/prob_tech.php');
			}

			$rq_info_proprio =
			"SELECT count(distinct t6.dnupro) as nbr_compte, count(distinct t6.dnuper) as nbr_proprio
			FROM cadastre.cadastre_cen t3, foncier.cadastre_site t4, cadastre.cptprop_cen t5, cadastre.r_prop_cptprop_cen t6
			WHERE t3.cad_cen_id = t4.cad_cen_id AND t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t4.site_id = ? AND t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'";
			$res_info_proprio = $bdd->prepare($rq_info_proprio);
			$res_info_proprio->execute(array($site_id));
			$info_proprio = $res_info_proprio->fetch();
			If ($res_info_proprio->rowCount() == 0){
				include('../includes/prob_tech.php');
			}
			
			$rq_info_maitrise =
			"SELECT COALESCE(sum(tmfutot.nbr_parcelle)) AS nbr_parcelle, COALESCE(sum(tmfutot.nbr_lot)) AS nbr_lot, COALESCE(sum(tmfutot.surf_total)) AS surf_total, tmfutot.bilan_mfu
			FROM (
				SELECT COALESCE(count(DISTINCT t1.par_id),0) AS nbr_parcelle, COALESCE(count(DISTINCT t2.lot_id),0) AS nbr_lot, COALESCE(sum(CASE WHEN tmfu.surf_mfu is Null THEN t2.dcntlo ELSE tmfu.surf_mfu END),0) as surf_total, CASE WHEN tmfu.mfu IS NULL THEN 'Pas de ma&icirc;trise' ELSE tmfu.mfu END AS bilan_mfu
				FROM sites.sites, foncier.cadastre_site t4 LEFT JOIN (
					SELECT cadastre_site.cad_site_id, 'Acquisition'::character varying(25) as mfu, Null as surf_mfu
					FROM foncier.cadastre_site, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.d_typmf, foncier.r_mfstatut, foncier.d_statutmf, (
						SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date
						FROM foncier.r_mfstatut
						GROUP BY r_mfstatut.mf_id
						) as tmax
					WHERE cadastre_site.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id = r_mfstatut.mf_id AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id
					AND (r_mfstatut.date = tmax.statut_date AND r_mfstatut.mf_id = tmax.mf_id)
					AND d_typmf.typmf_lib != 'Acquisition par un tiers' AND r_cad_site_mf.date_sortie = 'N.D.' AND d_statutmf.statutmf_id = 1
					UNION
					SELECT cadastre_site.cad_site_id, 'Convention'::character varying(25) as mfu, r_cad_site_mu.surf_mu AS surf_mfu
					FROM foncier.cadastre_site, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.d_typmu, foncier.r_mustatut, foncier.d_statutmu, (
						SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date
						FROM foncier.r_mustatut
						GROUP BY r_mustatut.mu_id
						) as tmax
					WHERE cadastre_site.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id = r_mustatut.mu_id AND r_mustatut.statutmu_id = d_statutmu.statutmu_id
					AND d_typmu.mfu = true --ATTENTION NO-MFU
					AND (r_mustatut.date = tmax.statut_date AND r_mustatut.mu_id = tmax.mu_id)
					AND r_cad_site_mu.date_sortie = 'N.D.' AND d_statutmu.statutmu_id = 1 AND mu_conventions.date_effet_conv <= to_char(now(),'YYYY-MM-DD')::text 
					) AS tmfu ON t4.cad_site_id = tmfu.cad_site_id, cadastre.cadastre_cen t3, cadastre.lots_cen t2, cadastre.parcelles_cen t1
				WHERE sites.site_id = t4.site_id AND t4.cad_cen_id = t3.cad_cen_id AND t3.lot_id = t2.lot_id and t2.par_id = t1.par_id
				AND t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'
				AND t4.site_id = ?
				GROUP BY tmfu.mfu
				UNION
				SELECT '0', '0', '0', 'Acquisition' UNION SELECT '0', '0', '0', 'Convention' UNION SELECT '0', '0', '0', 'Pas de ma&icirc;trise'
				) AS tmfutot
			GROUP BY tmfutot.bilan_mfu
			ORDER BY tmfutot.bilan_mfu";
			$res_info_maitrise = $bdd->prepare($rq_info_maitrise);
			$res_info_maitrise->execute(array($site_id));
			$info_maitrise = $res_info_maitrise->fetchAll();

			$rq_etat_foncier = "
			SELECT CASE WHEN ? IN (
				SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata 
				UNION 
				SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen WHERE statutmfu_id = 1
				UNION 
				SELECT DISTINCT site_id FROM alertes.v_alertes_mu WHERE alerte LIKE 'termin%'
				UNION
				SELECT DISTINCT substring(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id FROM alertes.v_prob_mfu_sansparc
			) THEN 1 ELSE 0 END as etat_bilan";
			$res_etat_foncier = $bdd->prepare($rq_etat_foncier);
			$res_etat_foncier->execute(array($site_id));
			$etat_foncier_tmp = $res_etat_foncier->fetch();
			$etat_foncier = $etat_foncier_tmp['etat_bilan'];
			$res_etat_foncier->closeCursor();
			
			$rq_nbr_actes = "
			SELECT tt1.type_acte, count(tt1.acte_id) as nbr_acte
			FROM
				(
					SELECT DISTINCT 'Acquisition' as type_acte, mf_acquisitions.mf_id as acte_id
					FROM foncier.cadastre_site t4, foncier.r_cad_site_mf, foncier.mf_acquisitions, foncier.r_mfstatut, 
					(SELECT r_mfstatut.mf_id, Max(r_mfstatut.date) as statut_date FROM foncier.r_mfstatut GROUP BY r_mfstatut.mf_id) as tt1
					WHERE t4.site_id = :site_id AND t4.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND mf_acquisitions.mf_id = r_mfstatut.mf_id  AND (r_mfstatut.date = tt1.statut_date AND r_mfstatut.mf_id = tt1.mf_id) AND r_mfstatut.statutmf_id = 1 AND date_sortie = 'N.D.'
					UNION
					SELECT DISTINCT 'Convention' as type_acte, mu_conventions.mu_id as acte_id
					FROM foncier.cadastre_site t4, foncier.r_cad_site_mu, foncier.mu_conventions, foncier.r_mustatut, 
					(SELECT r_mustatut.mu_id, Max(r_mustatut.date) as statut_date FROM foncier.r_mustatut GROUP BY r_mustatut.mu_id) as tt1, foncier.d_typmu
					WHERE t4.site_id = :site_id AND t4.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND mu_conventions.mu_id = r_mustatut.mu_id AND d_typmu.typmu_id = mu_conventions.typmu_id AND (r_mustatut.date = tt1.statut_date AND r_mustatut.mu_id = tt1.mu_id) AND r_mustatut.statutmu_id = 1 AND date_sortie = 'N.D.'
					AND d_typmu.mfu = true --ATTENTION NO-MFU
					ORDER BY type_acte
				) tt1
			GROUP BY tt1.type_acte";
			$res_nbr_actes = $bdd->prepare($rq_nbr_actes);
			$res_nbr_actes->execute(array('site_id'=>$site_id));
			$nbr_actes = $res_nbr_actes->fetchAll();
			
			$rq_lst_communes =
			"SELECT 
				array_to_string(array_agg(DISTINCT t0.nom),',') as communes
			FROM (
				SELECT t1.code_insee AS code_insee_cad, COALESCE(t2.code_insee, t1.code_insee) AS code_insee, COALESCE(t2.nom, t1.nom) AS nom, COALESCE(t2.actif, t1.actif) AS actif, COALESCE(t2.depart, t1.depart) AS depart
				FROM administratif.communes t1
					LEFT JOIN administratif.r_histo_com ON (r_histo_com.code_insee_old = t1.code_insee)
					LEFT JOIN administratif.communes t2 ON (t2.code_insee = r_histo_com.code_insee_new)
				) t0 
				JOIN cadastre.parcelles_cen t1 ON (t0.code_insee_cad = t1.codcom)
				JOIN cadastre.lots_cen t2 USING (par_id)
				JOIN cadastre.cadastre_cen t3 USING (lot_id)
				JOIN foncier.cadastre_site t4 USING (cad_cen_id) 
			WHERE t4.site_id = ? AND t3.date_fin = 'N.D.' AND t4.date_fin = 'N.D.' AND t0.actif = TRUE;";
			$res_lst_communes = $bdd->prepare($rq_lst_communes);
			$res_lst_communes->execute(array($site_id));
			$lst_communes = $res_lst_communes->fetch();
			$lst_communes = preg_split("/,/", $lst_communes['communes']);
			
			$rq_ref_foncier =
			"SELECT d_individu.individu_id, prenom, nom
			FROM personnes.d_individu 
				JOIN personnes.personne USING (individu_id)
				JOIN sites.r_sites_referents USING (personne_id)
			WHERE r_sites_referents.site_id = ? AND r_sites_referents.typreferent_id = 3";
			$res_ref_foncier = $bdd->prepare($rq_ref_foncier);
			$res_ref_foncier->execute(array($site_id));
			$ref_foncier = $res_ref_foncier->fetch();
			$res_ref_foncier->closeCursor();
			
			$rq_last_maj = "
			(SELECT 'maitrise' as tbl, 'la ma&icirc;trise fonci&egrave;re et d''usage' as lib, t_mfu.maj_user, max(t_mfu.maj_date) as maj_date
			FROM
				(
					(SELECT 'les acquisitions' as table, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mf_acquisitions.maj_user END as maj_user, max(mf_acquisitions.maj_date) as maj_date
					FROM foncier.mf_acquisitions LEFT JOIN admin_sig.utilisateurs ON (mf_acquisitions.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), foncier.r_cad_site_mf, foncier.cadastre_site
					WHERE mf_acquisitions.mf_id = r_cad_site_mf.mf_id AND r_cad_site_mf.cad_site_id = cadastre_site.cad_site_id AND cadastre_site.site_id = :site_id
					GROUP BY d_individu.individu_id, mf_acquisitions.maj_user
					ORDER BY maj_date DESC LIMIT 1)
					UNION
					(SELECT 'les conventions' as table, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mu_conventions.maj_user END as maj_user, max(mu_conventions.maj_date) as maj_date
					FROM foncier.mu_conventions LEFT JOIN admin_sig.utilisateurs ON (mu_conventions.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), foncier.r_cad_site_mu, foncier.cadastre_site
					WHERE mu_conventions.mu_id = r_cad_site_mu.mu_id AND r_cad_site_mu.cad_site_id = cadastre_site.cad_site_id AND cadastre_site.site_id = :site_id
					GROUP BY d_individu.individu_id, mu_conventions.maj_user
					ORDER BY maj_date DESC LIMIT 1)
				) as t_mfu
			GROUP BY t_mfu.maj_user
			ORDER BY maj_date DESC LIMIT 1)
			UNION
			(SELECT 'cadastre' as tbl, 'le cadastre' as lib, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE cadastre_site.maj_user END as maj_user, max(cadastre_site.maj_date) as maj_date
			FROM foncier.cadastre_site LEFT JOIN admin_sig.utilisateurs ON (cadastre_site.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id)
			WHERE cadastre_site.site_id = :site_id
			GROUP BY d_individu.individu_id, cadastre_site.maj_user
			ORDER BY maj_date DESC LIMIT 1)
			UNION
			(SELECT 'proprios' as tbl, 'les propri&eacute;taires' as tbl, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE t7.maj_user END as maj_user, max(t7.maj_date) as maj_date
			FROM cadastre.cadastre_cen t3, foncier.cadastre_site t4, cadastre.cptprop_cen t5, cadastre.r_prop_cptprop_cen t6, cadastre.proprios_cen t7 LEFT JOIN admin_sig.utilisateurs ON (t7.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id)
			WHERE t3.cad_cen_id = t4.cad_cen_id AND t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = t7.dnuper AND t4.site_id = :site_id
			GROUP BY d_individu.individu_id, t7.maj_user
			ORDER BY maj_date DESC LIMIT 1)";
			$res_last_maj = $bdd->prepare($rq_last_maj);
			$res_last_maj->execute(array('site_id'=>$site_id));
			$last_maj = $res_last_maj->fetchAll();
			$last_maj_tbl = array_column($last_maj, 'tbl');
			$last_maj_date = array_column($last_maj, 'maj_date');
		?>	
		<section id='main'>
		
			<h3>Fiche site : <?php echo $site_id . ' - ' . $site_nom; ?><img style='position:absolute; right:50px; top:150px ; opacity:0.8' align='right' src="images/vign_bdfoncier.png" height='100px'></h3>
			<div id='onglet' class='list'>
				<ul>
					<li>
						<a href="#general"><span>Général</span></a>
					</li>
					<li>
						<a href="#parcelle" onclick="create_lst_parc('charge', 0, '<?php echo $site_id; ?>');"><span>Parcelles/ Lots</span></a>
					</li>
					<li>
						<a href="#proprio" onclick="create_lst_proprio(0, '<?php echo $site_id; ?>');"><span>Propriétaires</span></a>
					</li>
					<li>
						<a href="#maitrise" onclick="create_lst_mf('charge', 0, '<?php echo $site_id; ?>');"><span>Maîtrise foncière et d'usage</span></a>
					</li>
				<?php If ($verif_role_acces == 'OUI') { ?>
					<li style='background-color:#edab5f;'>
						<a href="#af" onclick="create_lst_saf('charge', 0, '<?php echo $site_id; ?>');"><span>Animation foncière</span></a>
					</li>
				<?php } ?>
				</ul>
				
				<div id='general'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td colspan='3' style="padding-top:14.5px;">
								<label class="label">
									R&eacute;f&eacute;rent foncier :
									<?php 
										If ($ref_foncier['individu_id'] > 0) {
											echo $ref_foncier['prenom'].' '.$ref_foncier['nom']; 
										} Else {
											echo 'N.D.';
										}
									?>
								</label>
							</td>
						</tr>
						<tr>
							<td style='vertical-align:top' width='28%'>
								<fieldset class='fieldset'>
									<table CELLSPACING = '5' CELLPADDING ='5' class='no-border'>
										<tr>
											<td class='bilan-foncier'>
												<label class="label">
													<u>Code :</u>
												</label>
											</td>
											<td class='bilan-foncier'>
												<li><label class="label">
													<?php echo $site_id?>
												</label></li>
											</td>
										</tr>
										<tr>
											<td class='bilan-foncier'>
												<label class="label">
													<u>Commune<?php If (count($lst_communes)>1) {echo "s";} ?> :</u>
												</label>
											</td>
											<td class='bilan-foncier'>
												<?php For ($i=0; $i<count($lst_communes); $i++) { ?>
												<li style='margin-bottom:5px;'>
													<label class='label' style='white-space:normal;'><?php echo $lst_communes[$i]; ?></label>
												</li>
												<?php } ?>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
							<td style='vertical-align:top' width='36%'>
								<fieldset class='fieldset'>
									<table CELLSPACING = '5' CELLPADDING ='5' class='bilan-foncier'>
										<tr>
											<td class='bilan-foncier'>
												<label class="label">
													<u>Superficie cadastrale :</u>
												</label>
											</td>
											<td class='bilan-foncier'>
												<li><label class="label">
													<?php echo conv_are($info_parcelle['surf_total']) ?> ha.a.ca
												</label></li>
											</td>
										</tr>
										<tr>
											<td class='bilan-foncier'>
												<label class="label">
													<u>Situation cadastrale :</u>
												</label>
											</td>
											<td class='bilan-foncier'>
												<li>
													<label class="label"><?php echo $info_parcelle['nbr_parcelle'];?> parcelle<?php If ($info_parcelle['nbr_parcelle']>1) {echo "s";} ?></label>
												</li>
												<li>
													<label class="label"><?php echo $info_parcelle['nbr_lot'];?> lot<?php If ($info_parcelle['nbr_lot']>1) {echo "s";} ?></label>
												</li>
												<li>
													<label class="label"><?php echo $info_proprio['nbr_compte'];?> compte<?php If ($info_proprio['nbr_compte']>1) {echo "s";} ?> de propri&eacute;t&eacute;</label>
												</li>
												<li>
													<label class="label"><?php echo $info_proprio['nbr_proprio'];?> propri&eacute;taire<?php If ($info_proprio['nbr_proprio']>1) {echo "s";} ?></label>
												</li>
											</td>
										</tr>	
									</table>
									<center>
										<table CELLSPACING = '5' CELLPADDING ='5' class='bilan-foncier' style='float: none;'>
											<tr width='100%'>
												<td colspan='2' class='bilan-foncier'>
													<img style='border-radius:15px;box-shadow:2px 2px 3px 0 #6a6a6a;' src="charts/site_pie_type_prop.php?site_id=<?php echo $site_id; ?>&site_nom=<?php echo $site_nom; ?>" alt='site_pie_type_prop'/>
												</td>
											</tr>
										</table>
									</center>
								</fieldset>
							</td>
							<td style='vertical-align:top' width='36%'>
							<fieldset class='fieldset'>
								<table CELLSPACING = '5' CELLPADDING ='5' class='bilan-foncier'>
									<?php If ($etat_foncier > 0) { ?>
									<div class='prob_info'>
										<label>											
											Ces donn&eacute;es ne sont pas &agrave; jour
										</label>
									</div>
									<?php } ?>
									<tr>
										<td class='bilan-foncier' style='vertical-align:top;text-align:left'>
											<label class="label"><u>Bilan de la ma&icirc;trise :</u></label>
										</td>
									</tr>
									<tr>
										<td style='text-align:left'>
											<table CELLSPACING = '5' CELLPADDING ='5' class='bilan-foncier'>
												<?php For ($i=0; $i<$res_info_maitrise->rowCount(); $i++) { ?>
												<tr <?php If ($etat_foncier > 0) { ?>style='opacity:0.7;'<?php } ?>>
													<td class='bilan-foncier' style='vertical-align:top; text-align:right'>
														<li class='label'style="text-align:right"><?php echo $info_maitrise[$i]['bilan_mfu']; ?> : </li>
													</td>
													<td class='bilan-foncier' style='padding-bottom:10px'>
														<li style='margin:0' class='label'>
															<?php echo $info_maitrise[$i]['nbr_lot']; ?> lot<?php If ($info_maitrise[$i]['nbr_lot']>1) {echo "s";} ?>
														</li>
														<li style='margin:0' class='label'>
															<?php echo conv_are($info_maitrise[$i]['surf_total']); ?> ha.a.ca <?php If ($info_parcelle['surf_total']>0) { echo "(".round((($info_maitrise[$i]['surf_total']/10000)*100)/($info_parcelle['surf_total']/10000), 0)."%)"; } ?>
														</li>
														<li style='margin:0' class='label'>
															<?php
															For ($j=0; $j<$res_nbr_actes->rowCount(); $j++) 
															{ 
																If ($info_maitrise[$i]['bilan_mfu'] == $nbr_actes[$j]['type_acte']) { 
																	echo $nbr_actes[$j]['nbr_acte'] . ' acte';
																	If ($nbr_actes[$j]['nbr_acte'] > 1) {echo 's';}
																}
															} 
															?>
														</li>
													</td>
												</tr>
												<?php } ?>
											</table>
										</td>
									</tr>													
								</table>
							</fieldset>
							</td>
						</tr>
						<tr>
							<td style='text-align:right' colspan='3'>
								<label class="last_maj">
									<?php If (count($last_maj) > 0) { ?>
									Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_user'];?> concernant <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['lib'];
									} ?>
								</label>
							</td>
						</tr>
					</table>
				</div id='general'>
				
				<div id='parcelle'>
					<input type='hidden' id='mode' value='consult';>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<table width = '100%' class='no-border' id='tbl_parc' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody>
										<form id="frm_export_parc" name="frm_export_parc" onsubmit="return false" action='foncier_export.php?filename=<?php echo $site_id; ?>_parcelles.csv' method='POST'>
											<label class="label">Exporter :
												<img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px' style="cursor:pointer;" onclick="exportTable('frm_export_parc', 'tbl_parc_entete_fixed', 'tbl_parc_corps');">
												<input type="hidden" id="tbl_post" name="tbl_post" value="">
											</label>
										</form>
									</tbody>
									<tbody id='tbl_parc_entete'>
										<form id="frm_parc_entete" onsubmit="return false">
											<tr>
												<td class='filtre' align='center'>
													<label class="btn_combobox">
														<select name="filtre_commune" onchange="create_lst_parc('modif', 0, '<?php echo $site_id; ?>');" id="filtre_commune" class="combobox" style='text-align:center'>
															<option value='-1'>Commune</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='90px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_parc('modif', 0, '<?php echo $site_id; ?>');" type='text' id='filtre_section' size='8' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';">
												</td>
												<td class='filtre' align='center' width='90px'>
													<input style='text-align:center' class='editbox' 
													oninput="create_lst_parc('modif', 0, '<?php echo $site_id; ?>');" 
													type='text' id='filtre_par_num' size='8' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';">
												</td>
												<td align='center' width='120px'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='12' value='Contenance' DISABLED> 
												</td>
												<td class='filtre' align='center' width='90px'>
													<label class="btn_combobox">
														<select name="filtre_typ_prop" onchange="create_lst_parc('modif', 0, '<?php echo $site_id; ?>');" id="filtre_typ_prop" class="combobox" style='text-align:center'>
															<option value='-1'>TYP_PROP</option>
														</select>
													</label>
												</td>
												<td align='center' width='180px' colspan='2'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_lots' size='3' value='Lots' DISABLED> 
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_mfu" onchange="create_lst_parc('modif', 0, '<?php echo $site_id; ?>');" id="filtre_mfu" class="combobox" style='text-align:center'>
															<option value='-1'>MFU</option>
														</select>
													</label>
												</td>
												<td align='center' width='120px'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_surf_maitrise' size='12' value='Surf. ma&icirc;tris&eacute;e' DISABLED> 
												</td>
												<td width='25px'>	
													<img onclick="
													document.getElementById('filtre_commune').value='-1';
													document.getElementById('filtre_section').value='';
													document.getElementById('filtre_par_num').value='';
													document.getElementById('filtre_typ_prop').value='-1';
													document.getElementById('filtre_mfu').value='-1';
													create_lst_parc('charge', 0, '<?php echo $site_id; ?>');
													" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
											<input type='hidden' id='filtre_site_id' value='<?php echo $site_id ?>';> 
										</form>
									</tbody>
									<thead id="tbl_parc_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
										<tr>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>Commune</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>Lieu-dit</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>PAR ID</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>CODE INSEE</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>SECTION</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>PAR NUM</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Contenance</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>TYP_PROP</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Lot</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Contenance</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>MFU</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Surf. ma&icirc;tris&eacute;e</label>
											</th>
											<th style='text-align:center;border-top:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>&nbsp;</label>
											</th>
										</tr>
									</thead>
									<tbody id='tbl_parc_corps'>
									</tbody>
									<?php If (in_array('cadastre', $last_maj_tbl)) { ?>
									<tbody id='tbl_parc_maj'>
										<tr>
											<td style='text-align:right' colspan='10'>
												<label class="last_maj">
													Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search('cadastre', $last_maj_tbl)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search('cadastre', $last_maj_tbl)]['maj_user']; ?>
												</label>
											</td>
										</tr>
									</tbody>
									<?php } ?>
								</table>
							</td>
						</tr>
					</table>
				</div id='parcelle'>
				
				<div id='proprio'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<?php
									$vget = "page:fiche_site.php;champ_id:$site_id;";
									$vget_crypt = base64_encode($vget);
									$lien = "foncier_publipostage.php?d=$vget_crypt";
								?>
								<label class="label">Publipostage :
									<a href="<?php echo $lien;?>"><img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px' style="cursor:pointer;"></a>
								</label>
								<table width = '100%' class='no-border' id='tbl_prop' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody id='tbl_proprio_entete'>
										<form id="frm_proprio_entete" onsubmit="return false">
											<input type='hidden' id='filtre_site' value='<?php echo $site_id ?>';> 
											<tr>
												<td class='filtre' align='right' width='50%'>
													<input class='editbox' style="text-align:center" oninput="create_lst_proprio(0, '<?php echo $site_id; ?>');" type='text' id='filtre_nom' size='45' placeholder='Propri&eacute;taire' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Propri&eacute;taire';">
												</td>
												<td class='filtre' align='left' style="padding-left:18px;">
													<img onclick="document.getElementById('filtre_nom').value=''; create_lst_proprio(0, '<?php echo $site_id; ?>');" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
										</form>
									</tbody>
									<tbody id='tbl_proprio_corps'>
									</tbody>
									<?php If (in_array('proprios', $last_maj_tbl)) { ?>
									<tbody id='tbl_proprio_maj'>
										<tr>
											<td style='text-align:right' colspan='3'>
												<label class="last_maj">
													Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search('proprios', $last_maj_tbl)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search('proprios', $last_maj_tbl)]['maj_user']; ?>
												</label>
											</td>
										</tr>
									</tbody>
									<?php } ?>
								</table>
							</td>
						</tr>
					</table>
				</div id='proprio'>
				
				<div id='maitrise'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<table width = '100%' class='no-border' id='tbl_mf' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody id='tbl_mf_entete'>
										<form id="frm_mf_entete" onsubmit="return false">
											<tr>
												<td class='filtre' align='center' width='95px'>
													<label class="btn_combobox">
														<select name="filtre_acte" onchange="create_lst_mf('modif', 0, '<?php echo $site_id; ?>');" id="filtre_acte" class="combobox" style='text-align:center'>
															<option value='-1'>Acte</option>
														</select>
													</label>
												</td>												
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $site_id; ?>');" type='text' id='filtre_acte_id' size='9' placeholder='ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'ID';">   
												</td>												
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $site_id; ?>');" type='text' id='filtre_libelle' size='36' placeholder='Libell&eacute;' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Libell&eacute;';">   
												</td>
												<td class='filtre' align='center' width='270px' >
													<label class="btn_combobox">
														<select name="filtre_type_mf" onchange="create_lst_mf('modif', 0, '<?php echo $site_id; ?>');" id="filtre_type_mf" class="combobox" style='text-align:center;max-width:270px'>
															<option value='-1'>Type d'acte</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='95px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $site_id; ?>');" type='text' id='filtre_date' size='10' placeholder='Date' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Date';"> 
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_statut" onchange="create_lst_mf('modif', 0, '<?php echo $site_id; ?>');" id="filtre_statut" class="combobox" style='text-align:center'>
															<option value='-1'>Statut</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='80px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_doc_pdf" onchange="create_lst_mf('modif', 0, '<?php echo $site_id; ?>');" id="filtre_doc_pdf" class="combobox" style='text-align:center'>
															<option value='-1'>PDF</option>
														</select>
													</label>
												</td>
												<td width='25px'>	
													<img onclick="
													document.getElementById('filtre_acte').value='-1';
													document.getElementById('filtre_acte_id').value='';
													document.getElementById('filtre_libelle').value='';
													document.getElementById('filtre_type_mf').value='';
													document.getElementById('filtre_date').value='';
													document.getElementById('filtre_statut').value='-1';
													document.getElementById('filtre_doc_pdf').value='-1';
													create_lst_mf('charge', 0, '<?php echo $site_id; ?>');
													" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
											<input type='hidden' id='filtre_site_id' value='<?php echo $site_id ?>';> 
										</form>
									</tbody>
									<?php If ($verif_role_acces == 'OUI') { 
										$vget = "site_id:".$site_id.";";
										$vget .= "site_nom:".$site_nom.";";
										$vget_acq = "type_acte:Acquisition;".$vget;										
										$vget_conv = "type_acte:Convention;".$vget;	
										$vget_crypt_acq = base64_encode($vget_acq);
										$vget_crypt_conv = base64_encode($vget_conv);
										$lien_acq = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_new_mfu.php?d=$vget_crypt_acq";
										$lien_conv = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_new_mfu.php?d=$vget_crypt_conv";
									?>
									<tbody>
										<form id="frm_export_mfu" name="frm_export_mfu" onsubmit="return false" action='foncier_export.php?filename=<?php echo $site_id; ?>_MFU.csv' method='POST'>
											<label class="label">
												Exporter :
													<img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px' style="cursor:pointer;" onclick="exportTable('frm_export_mfu', 'tbl_mf_entete_fixed', 'tbl_mf_corps');">
													<input type="hidden" id="tbl_post" name="tbl_post" value=""/>
												<span style='padding-left:75px;'></span>
											</label>
									<?php
									
										$rq_check_grant = "SELECT * FROM foncier.grant_group WHERE grant_elt_id = 1 AND grant_group_id IN ('".implode("','", $_SESSION['grp'])."') AND grant_group_droit = 't'";
										$res_check_grant = $bdd->prepare($rq_check_grant);
										$res_check_grant->execute();
										If ($res_check_grant->rowCount() > 0) {
									?>
											<label class="label">
												Ajouter :
													<input type='button' class='btn_frm' value='Une acquisition' onclick="document.location.href='<?php echo $lien_acq; ?>'" />
													<span style='padding-left:75px;'></span>
											</label>
											<label class="label">
												Ajouter :
													<input type='button' class='btn_frm' value='Une convention' onclick="document.location.href='<?php echo $lien_conv; ?>'" />
											</label>
									<?php
										}
									?>
										</form>
									</tbody>
									<?php } ?>
									<thead id="tbl_mf_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
										<tr>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>Acte</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>ID</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Libell&eacute;</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Type d'acte</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Date</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Statut</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>PDF</label>
											</th>
											<th style='display:none'>	
												<label>Surface maitris&eacute;e (m&sup2;)</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>&nbsp;</label>
											</th>
										</tr>
									</thead>
									<tbody id='tbl_mf_corps'>
									</tbody>									
									<?php If (in_array('maitrise', $last_maj_tbl)) { ?>
									<tbody id='tbl_mfu_maj'>
										<tr>
											<td style='text-align:right' colspan='7'>
												<label class="last_maj">
													Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search('maitrise', $last_maj_tbl)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search('maitrise', $last_maj_tbl)]['maj_user']; ?>
												</label>
											</td>
										</tr>
									</tbody>
									<?php } ?>
								</table>
							</td>
						</tr>
					</table>
				</div id='maitrise'>
				<div id='af'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black; background-color: #edab5f;'>
						<tr>
							<td>
								<center>
								<table width = '100%' class='no-border' id='tbl_mf' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody id='tbl_saf_entete'>
										<form id="frm_saf_entete" onsubmit="return false">
											<tr>								
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox editbox_af' oninput="create_lst_saf('modif', 0, '<?php echo $site_id; ?>');" type='text' id='filtre_saf_lib' size='36' placeholder='Libell&eacute;' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Libell&eacute;';">   
												</td>												
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox editbox_af' oninput="create_lst_saf('modif', 0, '<?php echo $site_id; ?>');" type='text' id='filtre_saf_date' size='12' placeholder='Date' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Date';">   
												</td>
												<td class='filtre' align='center' width='270px' >
													<label class="btn_combobox btn_combobox_af">
														<select name="filtre_saf_origin" onchange="create_lst_saf('modif', 0, '<?php echo $site_id; ?>');" id="filtre_saf_origin" class="combobox combobox_af" style='text-align:center;max-width:270px'>
															<option value='-1'>Origine</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox btn_combobox_af">
														<select name="filtre_saf_statut" onchange="create_lst_saf('modif', 0, '<?php echo $site_id; ?>');" id="filtre_saf_statut" class="combobox combobox_af" style='text-align:center'>
															<option value='-1'>Statut</option>
														</select>
													</label>
												</td>
												<td width='25px'>	
													<img onclick="
													document.getElementById('filtre_saf_lib').value='';
													document.getElementById('filtre_saf_date').value='';
													document.getElementById('filtre_saf_origin').value='-1';
													document.getElementById('filtre_saf_statut').value='-1';
													create_lst_saf('charge', 0, '<?php echo $site_id; ?>');
													" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
											<input type='hidden' id='filtre_site_id' value='<?php echo $site_id ?>';> 
										</form>
									</tbody>
									<tbody id="tbl_saf_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
										<tr>
											<td style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>Libell&eacute;</label>
											</td>
											<td style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>Date</label>
											</td>
											<td style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Origine</label>
											</td>
											<td style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Statut</label>
											</td>
										</tr>
									</tbody>
									<tbody id='tbl_saf_corps'>
										
									</tbody>									
									<?php If (in_array('maitrise', $last_maj_tbl)) { ?>
									<tbody id='tbl_af_maj'>
										<tr>
											<td style='text-align:right' colspan='7'>
												<label class="last_maj">
													Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search('maitrise', $last_maj_tbl)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search('maitrise', $last_maj_tbl)]['maj_user']; ?>
												</label>
											</td>
										</tr>
									</tbody>
									<?php } ?>
								</table>
								</center>
							</td>
						</tr>
					</table>
				</div id='af'>
			</div>
			
			<div id="explorateur_histo" class="explorateur_span" style="display: none;">
				<div id="explorateur_new">
					<table class="explorateur_contenu" cellspacing="0" cellpadding="4" border="0" align="center" frame="box" rules="NONE">
						<tbody id='tbody_histo'>
						</tbody>
					</table>
				</div>
			</div>
			
			<div id="loading" class="loading" style="display: none;">
				<img src="<?php echo ROOT_PATH; ?>images/ajax-loader.gif">
			</div>
		</section>
		<?php 
			$res_info_parcelle->closeCursor();
			$res_info_proprio->closeCursor();
			$res_info_maitrise->closeCursor();
			$res_nbr_actes->closeCursor();
			$res_lst_communes->closeCursor();
			$res_last_maj->closeCursor();
			$res_check_grant->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>