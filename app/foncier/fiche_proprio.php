<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start(); //ouverture d'une session PHP nécessaire à l'ensemble des pages
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	 //varibale permettant de vérifier l'utilisation de Firefox nécessaire dans fct\valide_acces.php
	include ('../includes/configuration.php'); //intégration du fichier de configuration personnalisé
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">		
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/explorateur.css">		
		<link rel="stylesheet" type="text/css" href= "foncier.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/jquery-ui.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="foncier.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script>
			$(document).ready( function () {
				$('#main').find('textarea').each(function(){
					resizeTextarea($(this).attr("id"));
				});
			});
			
			jQuery(function($){
				$.datepicker.setDefaults($.datepicker.regional['fr']);
				$('.datepicker').datepicker({
					dateFormat : 'dd/mm/yy',
					changeMonth: true,
					changeYear: true
				});				
			});
		</script>		
		<TITLE>Base Fonci&egrave;re - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php			
			try //tentative de création d'un objet PDO permettant une connexion à la base foncière
			{
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']); //variable de connection à la base foncière
			}
			catch (Exception $e) //récupération des erreurs en cas d'echec de création du PDO
			{
				die(include('../includes/prob_tech.php')); //en cas d'échec intégration de la page affichant un problème technique
			}	
			$bad_caract = array("-","–","’", "'"); //variable tableau utilisée pour l'adaptation de certains caractères mal interprétés par PHP ou PostgreSQL
			$good_caract = array("–","-","''", "''"); //variable tableau utilisée pour l'adaptation de certains caractères mal interprétés par PHP ou PostgreSQL
			
			//Requete pour identifier les droits de l'utilisateur afin de rendre accessible la page en édition. Doublon avec la fonction role_acces mais on est jamais trop prudent :)
			$rq_check_grant = "SELECT * FROM foncier.grant_group WHERE grant_elt_id = 24 AND grant_group_id IN ('".implode("','", $_SESSION['grp'])."') AND grant_group_droit = 't'";
			$res_check_grant = $bdd->prepare($rq_check_grant);
			$res_check_grant->execute();
			
			/* Procédure permettant de récuperer les différentes variables passées par la méthode GET
			Cette procédure est utilisée dans toutes les pages devant récupérer des variables par la méthode GET
			Elle permet de ne passer en réalité qu'une seule variable TOUJOURS nommée 'd' et contenant toutes les informations nécessaires codées en base64
			L'intérêt majeur et que les variables passées ne sont pas lisible par les utilisateurs
			Elle fonctionne en créant des variables PHP ($$crit sisi il faut 2$) et leur valeur correspondante ($value) localisées successivement par les symboles ':' et ';'
			En clair, il faut passer les informations de la façon suivante : variable1:valeur1;variable2:valeur2;variable3:valeur3;
			Attention, le dernier ';' est obligatoire */
			If (isset($_GET['d']) && substr_count(base64_decode($_GET['d']), ';') > 0 && substr_count(base64_decode($_GET['d']), ':') > 0) { //on commence par vérifier que la varible GET 'd' existe et qu'elle contient au moins un caractère ';' et au moins un caractère ':'
				$d= base64_decode($_GET['d']); //Décodage de la variable GET 'd' codée en base64
				$min_crit = 0; //déclaration de la variable $min_crit à 0 correspondant à la position du premier caractère de la première variable
				$max_crit = strpos($d, ':'); //déclaration de la variable $max_crit correspondant au nombre de caractère de la première variable en l'occurrence il s'agit de la position du premier ':'
				$min_val = strpos($d, ':') + 1; //déclaration de la variable $min_val correspondant à la position du premier caractère de la valeur de la première variable, en l'occurence il s'agit de la position du premier ':' + 1 (on aurait pu ecrire $min_val = $max_crit + 1
				$max_val = strpos($d, ';') - $min_val; //déclaration de la variable $max_val correspondant au nombre de caractère de la valeur de la première variable
				
				For ($p=0; $p<substr_count($d, ':'); $p++) { //on boucle sur la variable GET 'd' pour créer les différentes variables PHP
					$crit = substr($d, $min_crit, $max_crit); //déclaration du nom de la variable PHP
					$value = substr($d, $min_val, $max_val); //déclaration de la valeur de la variable PHP
					$$crit = $value; //création de la variable PHP
					$min_crit = strpos($d, ';', $min_crit) + 1; //recalcul de la variable $min_crit pour la prochaine variable
					$max_crit= strpos($d, ':', $min_crit) - $min_crit; //recalcul de la variable $max_crit pour la prochaine variable
					$min_val = $min_crit + $max_crit + 1; //recalcul de la variable $min_val pour la valeur de la prochaine variable
					$max_val = strpos($d, ';', $min_val) - $min_val; //recalcul de la variable $max_val pour la valeur de la prochaine variable
				}
				If (!isset($dnuper) OR !isset($mode) OR !isset($proprio_lib)) { //on verifie que les variables ont bien été créées dans le cas contraire on affiche un problème technique
					$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php'; //déclaration d'une variable utilisée dans la page includes/prob_tech.php intégrée juste après et permettant un retour vers une page fonctionnelle en cas de problème technique
					include('../includes/prob_tech.php'); //intégration de la page affichant un problème technique
				}
				If ($res_check_grant->rowCount() == 0) {
					$mode='consult';
				}
				/* Création du lien utilisé dans la page includes/header.php permettant de passer du mode consultation au mode édition et inversement
				Pour cela il faut récréer la variable GET 'd' en modifiant le mode */
				If ($mode=='consult') //Si le mode en cours est consultation 
					{$vget_cnx = "mode:edit;";} //alors on propose de passer en édition
				Else //Sinon le mode en cours est édition
					{$vget_cnx = "mode:consult;";} //on propose donc de passer en consultation
				$vget_cnx .= "dnuper:" . $dnuper . ";"; //on ajouteà la variable GET 'd' les autres informations nécessaires
				$vget_cnx .= "proprio_lib:" . $proprio_lib . ";"; //on ajouteà la variable GET 'd' les autres informations nécessaires
				$vget_crypt_cnx = base64_encode($vget_cnx); //on code la variable GET 'd' en base64
				$lien_cnx = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt_cnx#general"; //création du lien utile dans le header				
			} Else { //la vérification de la vriable GET 'd' a identifiée un problème
				$page_operationnelle = ROOT_PATH . 'foncier/accueil_foncier.php'; //déclaration d'une variable utilisée dans la page includes/prob_tech.php intégrée juste après et permettant un retour vers une page fonctionnelle en cas de problème technique
				include('../includes/prob_tech.php'); //intégration de la page affichant un problème technique
			}
			
			include ('../includes/valide_acces.php'); //intégration da la page contenant toutes les fonctions permettant la validation de l'accès
			$verif_role_acces = role_access(array('grp_sig', 'grp_foncier')); //variable permettant de définir quels utilisateurs peuvent passer en mode avancé(édition)
			
			$h2 ='Foncier'; //varible stockant le titre h2 de la page et utiliser dans la page header intégreée juste après
			If ($res_check_grant->rowCount() == 0) {
				unset($mode);
			}
			include('../includes/header.php'); //intégration de la page affichant l'entête nécessaire dans toutes les pages du site
			include('../includes/breadcrumb.php');
			If ($res_check_grant->rowCount() == 0) {
				$mode='consult';
			}
			
			//include('../includes/construction.php'); //(A COMMENTER POUR RENDRE LA PAGE OPERATIONNELLE) intégration de la page permettant d'afficher une page en construction pour tous ceux qui ne sont pas administrateurs 
			include('foncier_fct.php');	//intégration de toutes les fonctions PHP nécéssaires pour les traitements des pages sur la base foncière
			include('../includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
						
			/* Procédure de mise à jour des informations personnelles du propriétaires
			On passant en mode édition, on accède à un formulaire permettant la saisie ou la modification de toutes les données personnelles du propriétaire
			Ces données sont donc passées par la methode POST à la même page et sont traitées ci-après
			Une procédure Javascript permet de valider le formulaire et en particulier les éléments indispensables identifiés grâce au préfixe 'need_' de leur ID*/
			If (isset($do) && $do == 'maj' && $verif_role_acces == 'OUI' && $res_check_grant->rowCount() > 0) {//La variable 'do' récupérée en décryptant la variable GET 'd' doit être 'maj' et l'utilisateur doit avoir les droits nécéssaires pour traiter le code ci-après
				$aujourdhui = date('Y-m-d H:i:s');	
				$form_modif = 'NON';
				If (isset($_POST['cur_info_mfu_elt_name']) && isset($_POST['cur_info_mfu_elt_val'])) {
					$cur_info_mfu_elt_name = explode("*;*", $_POST['cur_info_mfu_elt_name']);
					$cur_info_mfu_elt_val = explode("*;*", $_POST['cur_info_mfu_elt_val']);
					
					For ($i=0; $i<count($cur_info_mfu_elt_name); $i++) {
						$elt_name = $cur_info_mfu_elt_name[$i];
						$elt_val = $cur_info_mfu_elt_val[$i];
						If (isset($_POST[$elt_name])){
							$elt_new_val = $_POST[$elt_name];
							// echo $elt_name . '->' . $elt_val . ' ' . $elt_new_val . '<br>';
							If (utf8_encode($elt_val) != utf8_encode($elt_new_val)) {
								$form_modif = 'OUI';
							}
						}
					}
				}								
				
				If ($form_modif == 'OUI') {	
					//Les variables suivantes fixent les nouvelles infos propriétaires
					If (isset($_POST['need_lst_particule'])) {$need_lst_particule = str_replace($bad_caract, $good_caract, $_POST['need_lst_particule']);}
					If (isset($_POST['need_new_ddenom'])) {$need_new_ddenom = strtoupper(str_replace($bad_caract, $good_caract, $_POST['need_new_ddenom']));}
					If (isset($_POST['need_new_nom_usage'])) {$need_new_nom_usage = strtoupper(str_replace($bad_caract, $good_caract, $_POST['need_new_nom_usage']));}
					If (isset($_POST['need_new_prenom_usage'])) {$need_new_prenom_usage = strtoupper(str_replace($bad_caract, $good_caract, $_POST['need_new_prenom_usage']));}
					$new_nom_jeunefille = 'N.P.';
					If (isset($_POST['new_nom_jeunefille'])) {$new_nom_jeunefille = strtoupper(str_replace($bad_caract, $good_caract, $_POST['new_nom_jeunefille']));}
					If ($new_nom_jeunefille == '') {$new_nom_jeunefille = 'N.P.';}
					$new_nom_conjoint = 'N.P.';
					If (isset($_POST['new_nom_conjoint'])) {$new_nom_conjoint = strtoupper(str_replace($bad_caract, $good_caract, $_POST['new_nom_conjoint']));}
					If ($new_nom_conjoint == '') {$new_nom_conjoint = 'N.P.';}
					$new_prenom_conjoint = 'N.P.';
					If (isset($_POST['new_prenom_conjoint'])) {$new_prenom_conjoint = strtoupper(str_replace($bad_caract, $good_caract, $_POST['new_prenom_conjoint']));}
					If ($new_prenom_conjoint == '') {$new_prenom_conjoint = 'N.P.';}
					If (isset($_POST['new_date_naissance'])) {$new_date_naissance = str_replace('/', '-', date_transform_inverse($_POST['new_date_naissance']));}
					If (isset($_POST['new_adresse1'])) {$new_adresse1 = str_replace($bad_caract, $good_caract, $_POST['new_adresse1']);}
					If (isset($_POST['new_adresse2'])) {$new_adresse2 = str_replace($bad_caract, $good_caract, $_POST['new_adresse2']);}
					If (isset($_POST['new_adresse3'])) {$new_adresse3 = str_replace($bad_caract, $good_caract, $_POST['new_adresse3']);}
					If (isset($_POST['need_new_adresse4'])) {$need_new_adresse4 = str_replace($bad_caract, $good_caract, $_POST['need_new_adresse4']);}
					If (isset($_POST['new_mail'])) {$new_mail = str_replace($bad_caract, $good_caract, $_POST['new_mail']);}
					If (isset($_POST['new_tel_fix1'])) {$new_tel_fix1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix1']);}
					If (isset($_POST['new_tel_fix2'])) {$new_tel_fix2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix2']);}
					If (isset($_POST['new_tel_portable1'])) {$new_tel_portable1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable1']);}
					If (isset($_POST['new_tel_portable2'])) {$new_tel_portable2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable2']);}
					If (isset($_POST['new_tel_fax'])) {$new_tel_fax = str_replace($bad_caract, $good_caract, $_POST['new_tel_fax']);}	
					If (isset($_POST['new_commentaires'])) {$new_commentaires = str_replace($bad_caract, $good_caract, $_POST['new_commentaires']);}						
					
					/* Création de la requête SQL de mise à jour des données relatives aux propriétaires
					L'existance d'une valeur pour les élements du formulaire est testée avant d'intégrer la mise à jour du champ
					La gestion des noms de naissance et des informations liées aux maris est prise en compte */
					$rq_update_proprio = "UPDATE cadastre.proprios_cen SET ";
					If (isset($need_lst_particule)) {$rq_update_proprio .= "ccoqua = $need_lst_particule, ";}
					If (isset($need_new_ddenom)) {$rq_update_proprio .= "ddenom = '$need_new_ddenom', ";}
					If (isset($need_new_nom_usage)) {$rq_update_proprio .= "nom_usage = '$need_new_nom_usage', ";}
					If (isset($need_new_prenom_usage)) {$rq_update_proprio .= "prenom_usage = '$need_new_prenom_usage', ";}
					If (isset($new_nom_jeunefille)) {$rq_update_proprio .= "nom_jeunefille = '$new_nom_jeunefille', ";}
					If (isset($new_nom_conjoint)) {$rq_update_proprio .= "nom_conjoint = '$new_nom_conjoint', ";}
					If (isset($new_prenom_conjoint)) {$rq_update_proprio .= "prenom_conjoint = '$new_prenom_conjoint', ";}
					If (isset($new_date_naissance)) {$rq_update_proprio .= "jdatnss = '$new_date_naissance', ";}
					If (isset($new_adresse1)) {$rq_update_proprio .= "dlign3 = '$new_adresse1', ";}
					If (isset($new_adresse2)) {$rq_update_proprio .= "dlign4 = '$new_adresse2', ";}
					If (isset($new_adresse3)) {$rq_update_proprio .= "dlign5 = '$new_adresse3', ";}
					If (isset($need_new_adresse4)) {$rq_update_proprio .= "dlign6= '$need_new_adresse4', ";}
					If (isset($new_mail)) {$rq_update_proprio .= "email = '$new_mail', ";}
					If (isset($new_tel_fix1)) {$rq_update_proprio .= "fixe_domicile = replace('$new_tel_fix1', ' ', ''), ";}
					If (isset($new_tel_fix2)) {$rq_update_proprio .= "fixe_travail = replace('$new_tel_fix2', ' ', ''), ";}
					If (isset($new_tel_portable1)) {$rq_update_proprio .= "portable_domicile = replace('$new_tel_portable1', ' ', ''), ";}
					If (isset($new_tel_portable2)) {$rq_update_proprio .= "portable_travail = replace('$new_tel_portable2', ' ', ''), ";}
					If (isset($new_tel_fax)) {$rq_update_proprio .= "fax = replace('$new_tel_fax', ' ', ''), ";}
					If (isset($new_commentaires)) {$rq_update_proprio .= "commentaires = '$new_commentaires', ";}
					$rq_update_proprio .= " maj_date = '$aujourdhui', maj_user = '$_SESSION[login]'"; 
					If (substr($rq_update_proprio, -2, 1) == ','){
						$rq_update_proprio = substr($rq_update_proprio, 0, strlen($rq_update_proprio)-2); //suppression de la dernière virgule	
					}
					$rq_update_proprio .= " WHERE dnuper = '$dnuper'";
					$res_update_proprio = $bdd->prepare($rq_update_proprio); //prépare la reqète à l'execution
					$res_update_proprio->execute();	 //execution de la requete
					$res_update_proprio->closeCursor(); //fermeture du curseur
				}
			}			
			
			//requete SQL permettant de récupérer l'ensemble des informations relatives au propriétaire concerné par la fiche en cours
			$rq_info_proprio = 
			"SELECT DISTINCT
			t7.ccoqua as particule_id,
			COALESCE(d_ccoqua.particule_lib, 'N.P.') as particule_lib,
			t7.ddenom,
			nom_usage,
			nom_jeunefille,
			prenom_usage,
			nom_conjoint,
			prenom_conjoint,
			dldnss as lieu_naissance,
			trim(jdatnss) as date_naissance,
			email,
			dlign3, dlign4, dlign5, dlign6,
			fixe_domicile, fixe_travail, portable_domicile, portable_travail, fax,
			t2.gtoper_lib, t3.ccogrm_lib, 
			COALESCE(t7.commentaires, 'R.A.S.') as commentaires,
			CASE WHEN t7.annee_matrice IS NULL THEN 'N.D.' ELSE t7.annee_matrice END AS date_matrice
			FROM cadastre.r_prop_cptprop_cen t6, cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON d_ccoqua.ccoqua = t7.ccoqua LEFT JOIN cadastre.d_gtoper t2 ON t7.gtoper = t2.gtoper LEFT JOIN cadastre.d_ccogrm t3 ON t7.ccogrm = t3.ccogrm
			WHERE t6.dnuper = t7.dnuper
			AND t7.dnuper = ?";
			$res_info_proprio = $bdd->prepare($rq_info_proprio); //prépare la reqète à l'execution
			$res_info_proprio->execute(array($dnuper)); //execution de la requete en remplacant les '?' par les variables passées dans la fonction
			$info_proprio = $res_info_proprio->fetch(); //récupération des informations
						
			//requete SQL permettant de récupérer la liste des sites sur lesquels le propriétaire possède des parcelles
			$rq_lst_site = "
			SELECT DISTINCT t4.site_id as site_id, sites.site_nom as site_nom
			FROM cadastre.cadastre_cen t3, foncier.cadastre_site t4, cadastre.cptprop_cen t5, cadastre.r_prop_cptprop_cen t6, sites.sites
			WHERE sites.site_id = t4.site_id AND t3.cad_cen_id = t4.cad_cen_id AND t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = ? AND t4.date_fin = 'N.D.' AND t3.date_fin = 'N.D.'
			ORDER BY site_id";
			$res_lst_site = $bdd->prepare($rq_lst_site); //prépare la reqète à l'execution
			$res_lst_site->execute(array($dnuper)); //execution de la requete en remplacant les '?' par les variables passées dans la fonction		
			$lst_site = $res_lst_site->fetchAll(); //retourne un tableau contenant toutes les lignes de la requête
			
			//requete SQL permettant de récupérer les dernières dates de mise à jour et les utilisateurs correspondants sur les table concernées par les propriétaires
			$rq_last_maj = "
			(SELECT 'maitrise' as tbl, 'la ma&icirc;trise fonci&egrave;re et d''usage' as lib, t_mfu.maj_user, max(t_mfu.maj_date) as maj_date
			FROM
				(
					(SELECT DISTINCT CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mf_acquisitions.maj_user END as maj_user, max(mf_acquisitions.maj_date) as maj_date
					FROM foncier.cadastre_site t4, foncier.r_cad_site_mf, foncier.mf_acquisitions LEFT JOIN admin_sig.utilisateurs ON (mf_acquisitions.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), cadastre.r_prop_cptprop_cen t6, cadastre.cptprop_cen t5, cadastre.cadastre_cen t3
					WHERE t4.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id = mf_acquisitions.mf_id AND t6.dnupro = t5.dnupro AND t5.dnupro = t3.dnupro AND t3.cad_cen_id = t4.cad_cen_id AND t6.dnuper = ''
					GROUP BY d_individu.individu_id, mf_acquisitions.maj_user
					ORDER BY maj_date DESC LIMIT 1)
					UNION
					(SELECT DISTINCT CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE mu_conventions.maj_user END as maj_user, max(mu_conventions.maj_date) as maj_date
					FROM foncier.cadastre_site t4, foncier.r_cad_site_mu, foncier.mu_conventions LEFT JOIN admin_sig.utilisateurs ON (mu_conventions.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), cadastre.r_prop_cptprop_cen t6, cadastre.cptprop_cen t5, cadastre.cadastre_cen t3
					WHERE t4.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id = mu_conventions.mu_id AND t6.dnupro = t5.dnupro AND t5.dnupro = t3.dnupro AND t3.cad_cen_id = t4.cad_cen_id AND t6.dnuper = :dnuper 
					GROUP BY d_individu.individu_id, mu_conventions.maj_user
					ORDER BY maj_date DESC LIMIT 1)
				) as t_mfu
			GROUP BY t_mfu.maj_user)
			UNION
			(SELECT 'cadastre' as tbl, 'le cadastre' as lib, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE t4.maj_user END as maj_user, max(t4.maj_date) as maj_date
			FROM foncier.cadastre_site t4 LEFT JOIN admin_sig.utilisateurs ON (t4.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id), cadastre.cadastre_cen t3, cadastre.cptprop_cen t5, cadastre.r_prop_cptprop_cen t6
			WHERE t4.cad_cen_id = t3.cad_cen_id AND t3.dnupro = t5.dnupro AND t5.dnupro = t6.dnupro AND t6.dnuper = :dnuper 
			GROUP BY d_individu.individu_id, t4.maj_user
			ORDER BY maj_date DESC LIMIT 1)
			UNION
			(SELECT 'proprios' as tbl, 'les donn&eacute;es du propri&eacute;taire' as tbl, CASE WHEN d_individu.individu_id IS NOT NULL THEN (d_individu.prenom || ' ' || d_individu.nom) ELSE t7.maj_user END as maj_user, max(t7.maj_date) as maj_date
			FROM cadastre.proprios_cen t7 LEFT JOIN admin_sig.utilisateurs ON (t7.maj_user = utilisateurs.utilisateur_id) LEFT JOIN personnes.d_individu ON (utilisateurs.individu_id = d_individu.individu_id)
			WHERE t7.dnuper = :dnuper 
			GROUP BY d_individu.individu_id, t7.maj_user
			ORDER BY maj_date DESC LIMIT 1)";
			/* $rq_last_maj .= ", admin_sig.utilisateur, personnes.d_individu
			WHERE utilisateurs.utilisateur_id = t1.maj_user AND utilisateurs.individu_id = d_individu.individu_id";  */
			$res_last_maj = $bdd->prepare($rq_last_maj); //prépare la reqète à l'execution
			$res_last_maj->execute(array('dnuper'=>$dnuper)); //execution de la requete en remplacant les variables identifiées par':' par les variables passées dans la fonction
			$last_maj = $res_last_maj->fetchAll(); //retourne un tableau contenant toutes les lignes de la requête
			$last_maj_tbl = array_column($last_maj, 'tbl'); //récupération dans une variable PHP des valeurs contenues dans une colonne du tableau retourné par la requète
			$last_maj_date = array_column($last_maj, 'maj_date'); //récupération dans une variable PHP des valeurs contenues dans une colonne du tableau retourné par la requète
			
			If ($res_info_proprio->rowCount() == 0){ //vérification que la requête renvoie au mois une ligne
				include('../includes/prob_tech.php'); //intégration d'une page affichant un problème technique en cas de retour vide
			}
		?>
		<section id='main'> <!-- Balise identifiant la section principale de la page-->
			<h3>Fiche propriétaire : <?php echo $proprio_lib . ' (' . $dnuper . ')'; ?><img style='position:absolute; right:50px; top:150px ; opacity:0.8' align='right' src="images/vign_bdfoncier.png" height='100px'></h3>
			<div class='list' id='onglet'>
				<!-- 
				La balise ul permet l'affichage de la page en onglet
				Chaque balise li à l'intérieur de cette balise ul doit avoir un lien a href (préfixé de #) commun avec un id d'une balise div située plus loin dans le code
				Lorsque le lien a href est précisé d'un évenement onclick, une procédure complexe composé du triptique Javascript AJAX et PHP est lancée pour adapter le contenu de la page
				-->
				<ul>
					<li><!-- Onglet 'Général' indispensable en cas de page avec onglet, en effet une page avec onglet doit systématique s'ouvrir sur l'onglet 'Général' grâce au suffixe #general du lien permettant d'y accéder-->
						<a id='li_general' href="#general"><span>G&eacute;n&eacute;ral</span></a>
					</li>
					<li>
						<a id='li_parcelle' href="#parcelle" onclick="create_lst_parc('charge', 0, '<?php echo $dnuper; ?>');"><span>Parcelles/ Lots</span></a>
					</li>
					<li>
						<a id='li_compte_proprio' href="#compte_proprio"><span>Comptes de propriété</span></a>
					</li>
					<li>
						<a id='li_maitrise' href="#maitrise" onclick="create_lst_mf('charge', 0, '<?php echo $dnuper; ?>');"><span>Maîtrise foncière et d'usage</span></a>
					</li>
				</ul>
				
				<div id='general'>
					<?php If ($mode=='consult' || $res_check_grant->rowCount() == 0) { ?>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black; padding-top: 5px;' align='CENTER'>
						<tr>
							<td>
								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px;width:100%">
									<tr>
										<td style='vertical-align:top'>
											<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px;">
												<?php If($res_lst_site->rowCount() > 0) {?>
												<tr>
													<td style='text-align:left; vertical-align:top'>
														<label class='label'>
															Propriétaire présent sur le<?php If($res_lst_site->rowCount() > 1) {echo 's';}?> site<?php If($res_lst_site->rowCount() > 1) {echo 's';}?> :
														</label>
													</td>
												
													<td style='text-align:left; vertical-align:top'>
														<ul class='bilan-foncier' style='-moz-columns:<?php echo round($res_lst_site->rowCount()/5, 0); ?>; margin:0; padding-left:5px'>
															<?php For ($j=0; $j<$res_lst_site->rowCount(); $j++) { 
															$vget = "site_id:" . $lst_site[$j]['site_id'] . ";site_nom:" . $lst_site[$j]['site_nom'] . ";";
															$vget_crypt = base64_encode($vget);
															$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";?>
															<li><a class='label' href='<?php echo $lien; ?>'><?php echo $lst_site[$j]['site_id']; ?></a></li>
															<?php } ?>
														</ul>
													</td>
												</tr>
												<?php } ?>
											</table>
										</td>
										<td style='vertical-align:top'>
											<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px;width:100%">
												<?php If($info_proprio['gtoper_lib'] != '' OR $info_proprio['particule_id'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<label class='label'>
															<?php If ($info_proprio['ccogrm_lib'] == '') {echo 'Personne physique';} Else {echo $info_proprio['ccogrm_lib'];}?>
														</label>
													</td>
												</tr><?php  }  If($info_proprio['date_naissance'] != '' or $info_proprio['lieu_naissance'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<label class='label'>
															Né<?php If ($info_proprio['particule_id'] <> '1') {echo 'e';}  If($info_proprio['date_naissance'] != '') { ?> le <?php echo date_transform($info_proprio['date_naissance']); }  If($info_proprio['lieu_naissance'] != '') { ?> à <?php echo $info_proprio['lieu_naissance']; } ?>
														</label>
													</td>
												</tr><?php } ?>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<?php 
						If ($info_proprio['gtoper_lib'] == 'physique' OR $info_proprio['particule_id'] != '') { ?>
						<tr>
							<td>
								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px;float:none;">
									<tr>
										<?php If ($info_proprio['gtoper_lib'] == 'physique' OR ($info_proprio['gtoper_lib'] <> 'morale' AND ($info_proprio['particule_lib'] <> '' OR $info_proprio['nom_usage'] <> 'N.P.' OR $info_proprio['prenom_usage'] <> 'N.P.'))) {?>
										<td style='text-align:center;white-space:nowrap;'>
											<label class='label'>
												<?php If ($info_proprio['particule_lib'] <> '') {echo $info_proprio['particule_lib'];}?>
											</label>
											<label class='label'>
												<?php If ($info_proprio['nom_usage'] <> 'N.P.') {echo $info_proprio['nom_usage'];} Else {echo substr($info_proprio['ddenom'], 0 , strpos($info_proprio['ddenom'], '/'));}?>
											</label>
											<label class='label'>
												<?php 
													If ($info_proprio['prenom_usage'] <> 'N.P.') {
														$prenom_usage = ucwords(strtolower($info_proprio['prenom_usage']));
													} Else {
														$prenom_usage =  ucwords(strtolower(substr($info_proprio['ddenom'], strpos($info_proprio['ddenom'], '/')+1 , strlen($info_proprio['ddenom']))));
													}
													Foreach (array('-', '\'') as $delimiter) {
														If (strpos($prenom_usage, $delimiter)!==false) {
															$prenom_usage = implode($delimiter, array_map('ucfirst', explode($delimiter, $prenom_usage)));
														}
													}
													echo $prenom_usage;
												?>
											</label>
										</td>	
										<?php } Else {?>
										<td style='text-align:right;'>
											<label class='label'><?php echo $info_proprio['ddenom']; ?></label>
										</td>	
										<?php } ?>
									</tr>
									<?php If ($info_proprio['prenom_conjoint'] <> 'N.P.' || $info_proprio['nom_conjoint'] <> 'N.P.') { ?>
									<tr>
										<td>
											<label class='label'>
												Epou<?php If ($info_proprio['particule_id'] == 1) {echo 'x';} ElseIf ($info_proprio['particule_id'] > 1) {echo 'se';} Else {echo 'x/Epouse';} ?> de 
												<?php 
													echo $info_proprio['nom_conjoint'] . ' ';
													If ($info_proprio['prenom_conjoint'] <> 'N.P.') {
														$prenom_conjoint = ucwords(strtolower($info_proprio['prenom_conjoint']));
														Foreach (array('-', '\'') as $delimiter) {
															If (strpos($prenom_conjoint, $delimiter)!==false) {
																$prenom_conjoint = implode($delimiter, array_map('ucfirst', explode($delimiter, $prenom_conjoint)));
															}
														}
													} Else {
														$prenom_conjoint = '';
													}
													echo $prenom_conjoint;
												?>
											</label>
										</td>
									</tr>
									<?php } If ($info_proprio['nom_jeunefille'] <> 'N.P.') { ?>
									<tr>
										<td>
											<label class='label'>
												Nom de jeune fille : <?php echo $info_proprio['nom_jeunefille']; ?>
											</label>
										</td>
									</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
						<?php  
							} Else { ?>
						<tr>
							<td style='text-align:center;white-space:nowrap;'>
								<label class='label'><?php echo $info_proprio['ddenom']; ?></label>
							</td>	
						</tr>
						<?php } ?>
						<tr>
							<td>
								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px" width='100%'>
									<tr>
										<?php If($info_proprio['dlign3'] != '' Or $info_proprio['dlign4'] != '' Or $info_proprio['dlign5'] != '' Or $info_proprio['dlign6'] != '') {?>
										<td>
											<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
												<tr>
													<td colspan='3'>
														<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
															<tr>
																<td rowspan='4' style="vertical-align:top;">
																	<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
																</td>
																<td style='text-align:right;'>
																	<?php If($info_proprio['dlign3'] != '') { ?>
																	<label class='label'><?php echo $info_proprio['dlign3']; ?></label>
																	<?php } ?>
																</td>
															</tr>
															<tr>
																<td style='text-align:right;'>
																	<?php If($info_proprio['dlign4'] != '') { ?>
																	<label class='label'><?php echo $info_proprio['dlign4']; ?></label>
																	<?php } ?>
																</td>	
															</tr>
															<tr>
																<td style='text-align:right;'>
																	<?php If($info_proprio['dlign5'] != '') { ?>
																	<label class='label'><?php echo $info_proprio['dlign5']; ?></label>
																	<?php } ?>
																</td>	
															</tr>
															<tr>
																<td style='text-align:right;'>
																	<?php If($info_proprio['dlign6'] != '') { ?>
																	<label class='label'><?php echo $info_proprio['dlign6']; ?></label>
																	<?php } ?>
																</td>	
															</tr>
														</table>
													</td>
												</tr>
												<?php } 
												If($info_proprio['email'] != '') {?>
												<tr>
													<td style='vertical-align:middle;padding-top:20px;'>
														<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
													</td>
													<td style='vertical-align:middle;padding-top:20px;'>
														<label class='label'><?php echo $info_proprio['email']; ?></label>
													</td>
													<td style='vertical-align:middle;padding-top:25px;'>
														<a href="https://mail.espaces-naturels.fr/zimbra/?view=compose&to=<?php echo $info_proprio['email']; ?>#1" class="label"  target='_blank'><img src='<?php echo ROOT_PATH; ?>images/send_mail.png' width='20px'></a>
													</td>
												</tr>
												<?php } ?>
											</table>
										</td>
										<?php 
										If($info_proprio['fixe_domicile'] != '' Or $info_proprio['fixe_travail'] != '' Or $info_proprio['portable_domicile'] != '' Or $info_proprio['portable_travail'] != '' Or $info_proprio['fax'] != '') {?>
										<td>
											<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
												<?php If($info_proprio['fixe_domicile'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
													</td>
													<td>
														<label class='label'><?php echo numtel_transform($info_proprio['fixe_domicile']); ?> (perso)</label>
													</td>	
												</tr>
												<?php } 
												If($info_proprio['fixe_travail'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
													</td>
													<td style='padding-bottom:8px;'>
														<label class='label'><?php echo numtel_transform($info_proprio['fixe_travail']); ?> (pro)</label>
													</td>	
												</tr>
												<?php } 
												If($info_proprio['portable_domicile'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
													</td>
													<td>
														<label class='label'><?php echo numtel_transform($info_proprio['portable_domicile']); ?> (perso)</label>
													</td>	
												</tr>
												<?php } 
												If($info_proprio['portable_travail'] != '') {?>
												<tr>
													<td style='text-align:right;padding-bottom:8px;'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
													</td>
													<td style='padding-bottom:8px;'>
														<label class='label'><?php echo numtel_transform($info_proprio['portable_travail']); ?> (pro)</label>
													</td>	
												</tr>
												<?php } 
												If($info_proprio['fax'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
													</td>
													<td>
														<label class='label'><?php echo numtel_transform($info_proprio['fax']); ?></label>
													</td>	
												</tr>
												<?php } ?>
											</table>
										</td>
										<?php } ?>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px" width='100%'>
									<tr>
										<td style='text-align:left; width:0;'>
											<label class="label">
												Commentaires :
											</label>
										</td>
										<td style='text-align:left'>
											<label class="label" style='font-weight:normal;text-align:left;white-space:pre-wrap;padding:0' ><?php echo $info_proprio['commentaires']; ?></label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px" width='100%'>
									<tr>
										<td style='text-align:left'>
											<label class="last_maj">
												Ann&eacute;e matrice : <?php echo $info_proprio['date_matrice']; ?>
											</label>
										</td>
										<td style='text-align:right'>
											<label class="last_maj">
												Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_user'];?> concernant <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['lib'];?>
											</label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<?php } Else If ($mode=='edit' && $res_check_grant->rowCount() > 0) {
					$cur_info = -1;	
					$vget_maj = "dnuper:" . $dnuper . ";";
					$vget_maj .= "proprio_lib:" . $proprio_lib . ";";
					$vget_maj .= "mode:consult;";
					$vget_maj .= "do:maj;";
					$vget_crypt_maj = base64_encode($vget_maj);
					$lien_maj= substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt_maj#general";?>
					<form class='form' onsubmit="return valide_form('frm_modif_proprio')" name='frm_modif_proprio' action='<?php echo $lien_maj; ?>' id='frm_modif_proprio' method='POST'>
						<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black; padding-top: 5px;' align='CENTER'>
							<tr>
								<td colspan='2'>
									<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px;width:100%">
										<tr>
											<td style='vertical-align:top'>
												<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px;">
													<?php If($res_lst_site->rowCount() > 0) {?>
													<tr>
														<td style='text-align:left; vertical-align:top'>
															<label class='label'>
																Propriétaire présent sur le<?php If($res_lst_site->rowCount() > 1) {echo 's';}?> site<?php If($res_lst_site->rowCount() > 1) {echo 's';}?> :
															</label>
														</td>
													
														<td style='text-align:left; vertical-align:top'>
															<ul class='bilan-foncier' style='-moz-columns:<?php echo round($res_lst_site->rowCount()/5, 0); ?>; margin:0; padding-left:5px'>
																<?php For ($j=0; $j<$res_lst_site->rowCount(); $j++) { 
																$vget = "site_id:" . $lst_site[$j]['site_id'] . ";site_nom:" . $lst_site[$j]['site_nom'] . ";";
																$vget_crypt = base64_encode($vget);
																$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_site.php?d=$vget_crypt#general";?>
																<li><a class='label' href='<?php echo $lien; ?>'><?php echo $lst_site[$j]['site_id']; ?></a></li>
																<?php } ?>
															</ul>
														</td>
													</tr>
													<?php } ?>
												</table>
											</td>
											<td style='vertical-align:top'>
												<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px;width:100%">
													<?php If($info_proprio['gtoper_lib'] != '' OR $info_proprio['particule_id'] != '') {?>
													<tr>
														<td style='text-align:right'>
															<label class='label'>
																<?php If ($info_proprio['ccogrm_lib'] == '') {echo 'Personne physique';} Else {echo $info_proprio['ccogrm_lib'];}?>
															</label>
														</td>
													</tr><?php } If($info_proprio['date_naissance'] != '') {?>
													<tr>
														<td style='text-align:right'>
															<label class='label'>
																Né<?php If ($info_proprio['particule_id'] <> '1') {echo 'e';} ?> le <input type='text' value="<?php If ($info_proprio['date_naissance'] <> 'N.P.') {echo date_transform($info_proprio['date_naissance']);}?>" size='10' placeholder='Date de naissance' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Date de naissance';" class='datepicker' name='new_date_naissance' id='new_date_naissance'>
															</label>
															<?php
																$cur_info++;
																$cur_info_mfu[$cur_info]['elt_name'] = 'new_date_naissance';
																$cur_info_mfu[$cur_info]['elt_val'] = date_transform($info_proprio['date_naissance']);
															?>
														</td>
													</tr><?php } ?>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php If ($info_proprio['gtoper_lib'] == 'physique' OR $info_proprio['particule_id'] != '') { ?>							
							<tr>
								<td colspan='2'>
									<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px;float:none;">
										<tr>
											<?php If ($info_proprio['particule_lib'] <> '' OR $info_proprio['nom_usage'] <> '' OR $info_proprio['prenom_usage'] <> '') {?>
											<td style='text-align:center;white-space:nowrap;'>
												<label class="btn_combobox">
													<select name="need_lst_particule" id="need_lst_particule" class="combobox">
														<option value='-1'>- - - Particule - - -</option>
														<?php
														$rq_lst_particules = "SELECT DISTINCT ccoqua, particule_lib FROM cadastre.d_ccoqua ORDER BY ccoqua";
														$res_lst_particules = $bdd->prepare($rq_lst_particules);
														$res_lst_particules->execute();
														While ($donnees = $res_lst_particules->fetch())  {
															echo "<option ";
															If ($donnees['ccoqua'] == $info_proprio['particule_id'])
																{echo " selected='selected' ";}
															echo "value='$donnees[ccoqua]'>$donnees[particule_lib]</option>";
														}
														$res_lst_particules->closeCursor();
														?>
													</select>
													<?php
														$cur_info++;
														$cur_info_mfu[$cur_info]['elt_name'] = 'need_lst_particule';
														$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['particule_id'];
													?>
												</label>
												<input type='text' value="<?php If ($info_proprio['nom_usage'] <> 'N.P.') {echo $info_proprio['nom_usage'];} Else {echo substr($info_proprio['ddenom'], 0 , strpos($info_proprio['ddenom'], '/'));}?>" size='40' class='editbox' name='need_new_nom_usage' id='need_new_nom_usage'>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'need_new_nom_usage';
													If ($info_proprio['nom_usage'] <> 'N.P.') {
														$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['nom_usage'];
													} Else {
														$cur_info_mfu[$cur_info]['elt_val'] = substr($info_proprio['ddenom'], 0 , strpos($info_proprio['ddenom'], '/'));
													}
												?>
												<input type='text' value="<?php If ($info_proprio['prenom_usage'] <> 'N.P.') {echo ucwords(strtolower($info_proprio['prenom_usage']));} Else {echo ucwords(strtolower(substr($info_proprio['ddenom'], strpos($info_proprio['ddenom'], '/')+1 , strlen($info_proprio['ddenom']))));}?>" size='40' class='editbox' name='need_new_prenom_usage' id='need_new_prenom_usage'>
											</td>	
											<?php } Else {?>
											<td style='text-align:right;'>
												<input type='text' value="<?php echo $info_proprio['ddenom'];?>" size='100' class='editbox' name='need_new_ddenom' id='need_new_ddenom'>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'need_new_ddenom';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['ddenom'];
												?>
											</td>	
											<?php } ?>
										</tr>
										<?php If ($info_proprio['gtoper_lib'] <> 'morale' OR $info_proprio['prenom_conjoint'] <> 'N.P.' OR ($info_proprio['particule_id'] == '' AND $info_proprio['gtoper_lib'] <> 'morale')) {?>
										<tr>
											<td>
												<label class='label'>
													Epou<?php If ($info_proprio['particule_id'] == 1) {echo 'x';} ElseIf ($info_proprio['particule_id'] > 1) {echo 'se';} Else {echo 'x/Epouse';} ?> de <input type='text' value="<?php If ($info_proprio['nom_conjoint'] <> 'N.P.') {echo $info_proprio['nom_conjoint'];}?>" size='40' placeholder='Nom du conjoint' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du conjoint';" class='editbox' name='new_nom_conjoint' id='new_nom_conjoint'>
													<input type='text' value="<?php If ($info_proprio['prenom_conjoint'] <> 'N.P.') {echo ucwords(strtolower($info_proprio['prenom_conjoint']));} ?>" size='40' placeholder='Prénom du conjoint' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Prénom du conjoint';" class='editbox' name='new_prenom_conjoint' id='new_prenom_conjoint'>
												</label>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_nom_conjoint';
													If ($info_proprio['nom_conjoint'] <> 'N.P.') {
														$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['nom_conjoint'];
													} Else {
														$cur_info_mfu[$cur_info]['elt_val'] = '';
													}
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_prenom_conjoint';
													If ($info_proprio['nom_conjoint'] <> 'N.P.') {
														$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['prenom_conjoint'];
													} Else {
														$cur_info_mfu[$cur_info]['elt_val'] = '';
													}													
												?>
											</td>
										</tr>
										<?php If ($info_proprio['particule_id'] != '1') { ?>
										<tr>
											<td>
												<label class='label'>
													Nom de jeune fille : <input type='text' value="<?php If ($info_proprio['nom_jeunefille'] <> 'N.P.') { echo $info_proprio['nom_jeunefille'];}?>" size='40' placeholder='Nom de jeune fille' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom de jeune fille';" class='editbox' name='new_nom_jeunefille' id='new_nom_jeunefille'>
												</label>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_nom_jeunefille';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['nom_jeunefille'];
												?>
											</td>
										</tr>		
										<?php } ?>										
										<?php } ?>
									</table>
								</td>
							</tr>
							<?php } Else {?>
							<tr>
								<td colspan='2' style='text-align:center;white-space:nowrap;'>
									<input type='text' value="<?php echo $info_proprio['ddenom'];?>" size='100' class='editbox' name='need_new_ddenom' id='need_new_ddenom'>
									<?php
										$cur_info++;
										$cur_info_mfu[$cur_info]['elt_name'] = 'need_new_ddenom';
										$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['ddenom'];
									?>
								</td>
							</tr>
							<?php } ?>
							<tr>
								<td>
									<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
										<tr>
											<td colspan='2'>
												<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
													<tr>
														<td rowspan='4' style="vertical-align:top;">
															<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
														</td>
														<td>
															<input type='text' value="<?php echo $info_proprio['dlign3'];?>" size='80' class='editbox' id='new_adresse1' name='new_adresse1' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / B&acirc;timent">
															<?php
																$cur_info++;
																$cur_info_mfu[$cur_info]['elt_name'] = 'new_adresse1';
																$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['dlign3'];
															?>
														</td>
													</tr>
													<tr>
														<td style='text-align:right;'>
															<input type='text' value="<?php echo $info_proprio['dlign4'];?>" size='80' class='editbox' id='new_adresse2' name='new_adresse2' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N&deg;, Rue, Boite postale">
															<?php
																$cur_info++;
																$cur_info_mfu[$cur_info]['elt_name'] = 'new_adresse2';
																$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['dlign4'];
															?>
														</td>	
													</tr>
													<?php If ($info_proprio['dlign5'] <> '') { ?>
													<tr>
														<td style='text-align:right;'>
															<input type='text' value="<?php echo $info_proprio['dlign5'];?>" size='80' class='editbox' id='new_adresse3' name='new_adresse3' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Complément">
															<?php
																$cur_info++;
																$cur_info_mfu[$cur_info]['elt_name'] = 'new_adresse3';
																$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['dlign5'];
															?>
														</td>	
													</tr>
													<?php } ?>
													<tr>
														<td style='text-align:right;'>
															<input type='text' value="<?php echo $info_proprio['dlign6'];?>" size='80' class='editbox' id='need_new_adresse4' name='need_new_adresse4' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal & Ville">
															<?php
																$cur_info++;
																$cur_info_mfu[$cur_info]['elt_name'] = 'need_new_adresse4';
																$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['dlign6'];
															?>
														</td>	
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style='vertical-align:bottom;'>
												<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
											</td>
											<td style='vertical-align:middle;padding-top:20px;'>
												<input type='text' value="<?php echo $info_proprio['email'];?>" size='80' class='editbox' name='new_mail' id='new_mail'>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_mail';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['email'];
												?>
											</td>
										</tr>
									</table>
								</td>
								<td>
									<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-right:50px">
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
											</td>
											<td>
												<input type='text' value="<?php echo $info_proprio['fixe_domicile'];?>" class='editbox' name='new_tel_fix1' id='new_tel_fix1'><label class='label'>(perso)</label>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_tel_fix1';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['fixe_domicile'];
												?>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
											</td>
											<td style='padding-bottom:8px;'>
												<input type='text' value="<?php echo $info_proprio['fixe_travail'];?>" class='editbox' name='new_tel_fix2' id='new_tel_fix2'><label class='label'>(pro)</label>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_tel_fix2';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['fixe_travail'];
												?>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
											</td>
											<td>
												<input type='text' value="<?php echo $info_proprio['portable_domicile'];?>" class='editbox' name='new_tel_portable1' id='new_tel_portable1'><label class='label'>(perso)</label>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_tel_portable1';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['portable_domicile'];
												?>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
											</td>
											<td style='padding-bottom:8px;'>
												<input type='text' value="<?php echo $info_proprio['portable_travail'];?>" class='editbox' name='new_tel_portable2' id='new_tel_portable2'><label class='label'>(pro)</label>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_tel_portable2';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['portable_travail'];
												?>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
											</td>
											<td>
												<input type='text' value="<?php echo $info_proprio['fax'];?>" class='editbox' name='new_tel_fax' id='new_tel_fax'>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_tel_fax';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['fax'];
												?>
											</td>	
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan='2'>
									<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px" width='100%'>
										<tr>
											<td style='text-align:left'>
												<label class="label">
													Commentaires : 
												</label>
											</td>
											<td style='text-align:left'>
												<textarea id='new_commentaires' name='new_commentaires' onkeyup="resizeTextarea('new_commentaires');" onchange="resizeTextarea('new_commentaires');" class="label textarea_com_edit" rows='3' cols='1000' style='font-weight:normal;'><?php echo $info_proprio['commentaires']; ?></textarea>
												<?php
													$cur_info++;
													$cur_info_mfu[$cur_info]['elt_name'] = 'new_commentaires';
													$cur_info_mfu[$cur_info]['elt_val'] = $info_proprio['commentaires'];
												?>
											</td>
										</tr>
										<tr>
											<td>
												&nbsp;
											</td>
											<td style='text-align:center;'>
												<label class='last_maj' style='color:#d77a0f;font-weight:bold;'>
													Attention les commentaires sont visibles par tous
												</label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan='2' class='save'>
									<?php
										$cur_info_mfu_elt_name = implode("*;*", array_column($cur_info_mfu, 'elt_name'));
										$cur_info_mfu_elt_val = implode("*;*", array_column($cur_info_mfu, 'elt_val'));
									?>
									<input type='hidden' name='cur_info_mfu_elt_name' value="<?php echo $cur_info_mfu_elt_name; ?>">
									<input type='hidden' name='cur_info_mfu_elt_val' value="<?php echo $cur_info_mfu_elt_val; ?>">
									<input type='submit' name='valid_form' id='valid_form' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
								</td>
							</tr>
							<tr>
								<td style='text-align:right' colspan='2'>
									<label class="last_maj">
										Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['maj_user'];?> concernant <?php echo $last_maj[array_search(max($last_maj_date), $last_maj_date)]['lib'];?>
									</label>
								</td>
							</tr>
						</table>
					</form>
					<?php } ?>					
				</div>
				<div id='parcelle'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<table width = '100%' class='no-border' id='tbl_parc' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0'>
									<tbody id='tbl_parc_entete'>
										<form id="frm_parc_entete" onsubmit="return false">
											<tr>
												<td class='filtre' align='center' width='90px'>
													<label class="btn_combobox">
														<select name="filtre_site_id" onchange="create_lst_parc('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_site_id" class="combobox" style='text-align:center'>
															<option value='-1'>SITE ID</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center'>
													<label class="btn_combobox">
														<select name="filtre_commune" onchange="create_lst_parc('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_commune" class="combobox" style='text-align:center'>
															<option value='-1'>Commune</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='90px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_parc('modif', 0, '<?php echo $dnuper; ?>');" type='text' id='filtre_section' size='8' placeholder='SECTION' onfocus="this.placeholder = '';" onblur="this.placeholder = 'SECTION';"> 
												</td>
												<td class='filtre' align='center' width='90px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_parc('modif', 0, '<?php echo $dnuper; ?>');" type='text' id='filtre_par_num' size='8' placeholder='PAR NUM' onfocus="this.placeholder = '';" onblur="this.placeholder = 'PAR NUM';"> 
												</td>
												<td align='center' width='120px'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_contenance' size='12' value='Contenance' DISABLED> 
												</td>
												<td align='center' width='180px' colspan='2'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_lots' size='3' value='Lots' DISABLED> 
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_mfu" onchange="create_lst_parc('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_mfu" class="combobox" style='text-align:center'>
															<option value='-1'>MFU</option>
														</select>
													</label>
												</td>
												<td align='center' width='120px'>
													<input style='text-align:center; background-color:#accf8a' class='editbox' type='text' id='label_surf_maitrise' size='12' value='Surf. ma&icirc;tris&eacute;e' DISABLED> 
												</td>
												<td width='25px'>
													<img onclick="
													document.getElementById('filtre_site_id').value='-1';
													document.getElementById('filtre_commune').value='-1';
													document.getElementById('filtre_section').value='';
													document.getElementById('filtre_par_num').value='';
													document.getElementById('filtre_mfu').value='-1';
													create_lst_parc('charge', 0, '<?php echo $dnuper; ?>');
													" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
										</form>
									</tbody>
									<tbody>
										<form id="frm_export_parc" name="frm_export_parc" onsubmit="return false" action='foncier_export.php?filename=<?php echo str_replace(array("M. ", "Mme. ", "Mlle. ", " "), array("","","", ""), $proprio_lib); ?>_parcelles.csv' method='POST'>
											<label class="label">Exporter :
												<img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" width='20px' style="cursor:pointer;" onclick="exportTable('frm_export_parc', 'tbl_parc_entete_fixed', 'tbl_parc_corps');">
												<input type="hidden" id="tbl_post" name="tbl_post" value="">
											</label>
										</form>
									</tbody>
									<thead id="tbl_parc_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
										<tr>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>SITE ID</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>Commune</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>Lieu-dit</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>PAR ID</label>
											</th>
											<th style='display:none;'>
												<label class='label' style='text-align:center;'>CODE INSEE</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>
												<label class='label' style='text-align:center;'>SECTION</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>PAR NUM</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Contenance</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Lots</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Contenance</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>MFU</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>Surf. ma&icirc;tris&eacute;e</label>
											</th>
											<th style='text-align:center;border-top:0px solid #accf8a;padding:6px 5px'>	
												<label class='label' style='text-align:center;'>&nbsp;</label>
											</th>
										</tr>
									</thead>
									<tbody id='tbl_parc_corps'>
										<!--Section mise à jour dynamique en Javascritp AJAX et PHP-->
									</tbody>
									<?php If (in_array('cadastre', $last_maj_tbl)) { ?>
									<tbody id='tbl_parc_maj'>
										<tr>
											<td style='text-align:right' colspan='10'>
												<label class="last_maj">
													Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search('cadastre', $last_maj_tbl)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search('cadastre', $last_maj_tbl)]['maj_user']; ?>
												</label>
											</td>
										</tr>
									</tbody>
									<?php } ?>
								</table>
							</td>
						</tr>
					</table>
				</div id='parcelle'>
				<div id='compte_proprio'>
					<?php
						$rq_cpt_proprio = "
						SELECT *
						FROM 
							(SELECT DISTINCT 
							t6.dnupro
							, array_to_string(array_agg(t7.dnuper ORDER BY t7.dnuper), ',') as tbl_dnuper
							, array_to_string(array_agg(CASE WHEN d_ccoqua.particule_lib != '' THEN d_ccoqua.particule_lib ELSE 'N.P.' END ORDER BY t7.dnuper), ',') as tbl_particule,
							array_to_string(array_agg(t7.ddenom ORDER BY t7.dnuper), ',') as tbl_ddenom
							, array_to_string(array_agg(t7.nom_usage ORDER BY t7.dnuper), ',') as tbl_nom_usage
							, array_to_string(array_agg(t7.prenom_usage ORDER BY t7.dnuper), ',') as tbl_prn_usage
							FROM 
							cadastre.cptprop_cen t5, 
							cadastre.r_prop_cptprop_cen t6, 
							cadastre.proprios_cen t7 LEFT JOIN cadastre.d_ccoqua ON (d_ccoqua.ccoqua = t7.ccoqua) 
							WHERE 
							t5.dnupro = t6.dnupro AND 
							t6.dnuper = t7.dnuper
							GROUP BY t6.dnupro
							ORDER BY t6.dnupro) tt1
						WHERE tbl_dnuper LIKE ?";
						$res_cpt_proprio = $bdd->prepare($rq_cpt_proprio);
						$res_cpt_proprio->execute(array('%'.$dnuper.'%'));				
						$cpt_proprio = $res_cpt_proprio->fetchAll();
					?>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' id="tbl_cptprop" style='border:0px solid black'>
						<?php If ($res_cpt_proprio->rowCount() > 1) { ?>
						<tbody id='tbl_collapse'>
							<tr>
								<td width='100%'>
									<label class='label' onclick="slideIn('ALL_IN');">Tout déplier</label>
								</td>
							</tr>
						</tbody>
						<?php } ?>
						<tbody id='tbl_compte_proprio_corps'>
							<tr>
								<td>
									<?php	
									For ($r=0; $r<$res_cpt_proprio->rowCount(); $r++) {
										$lst_dnuper = preg_split("/,/", $cpt_proprio[$r]['tbl_dnuper']);
										$lst_particule = preg_split("/,/", $cpt_proprio[$r]['tbl_particule']);
										$lst_ddenom = preg_split("/,/", $cpt_proprio[$r]['tbl_ddenom']);
										$lst_nom_usage = preg_split("/,/", $cpt_proprio[$r]['tbl_nom_usage']);
										$lst_prn_usage = preg_split("/,/", $cpt_proprio[$r]['tbl_prn_usage']);
									?>
										
									<table width = '100%' CELLSPACING = '5' CELLPADDING ='5' style='border:0px solid black'>
										<tr>
											<td colspan='2' width='100%'>
												<?php
												If (substr_count($cpt_proprio[$r]['tbl_dnuper'], ",") == 0)
													{
														echo "<center><label class='label'>Ce compte ne concerne qu'une seule personne</label></center>";
													}
												Else
													{
														echo "<center><label class='label'>Ce compte concerne " . count($lst_dnuper) . " personnes</label></center>";
													}
												?>
											</td>
										</tr>
										<tr>
											<td width='60px'>
												<label class='label' <?php If ($res_cpt_proprio->rowCount() > 1) { ?> onclick="slideIn('sld_<?php echo str_replace('+', '_', $cpt_proprio[$r]['dnupro']); ?>');"<?php } ?>><?php echo $r+1 . '. ' .$cpt_proprio[$r]['dnupro']; ?></label>
											</td>
											<td width='100%'>
												<div id='sld_<?php echo str_replace('+', '_', $cpt_proprio[$r]['dnupro']);?>' <?php If ($res_cpt_proprio->rowCount() > 1) { ?> style='display:none' <?php } ?>>
													<table style='border:0px solid black'><?php For ($i=0; $i<count($lst_dnuper); $i++) {?>
														<tr>
															<td>
																<table CELLSPACING = '5' CELLPADDING ='5' style='border:0px solid black'>
																	<?php
																		$proprio_lib = proprio_lib($lst_particule[$i], $lst_nom_usage[$i], $lst_prn_usage[$i], $lst_ddenom[$i]);
																		
																		$vget = "dnuper:$lst_dnuper[$i];proprio_lib:$proprio_lib;mode:consult;";
																		$vget_crypt = base64_encode($vget);
																		$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/fiche_proprio.php?d=$vget_crypt#general";	
																	?>
																	<tr>
																		<td class='no-border'>
																			<label class='label'>
																				<?php echo $proprio_lib; ?>
																			</label>
																		</td>
																		<td class='no-border'>
																			<label class='label'>
																				 (<?php echo $lst_dnuper[$i]; ?>)
																			</label>
																		</td>
																		<td>
																			<?php If ($lst_dnuper[$i] != $dnuper) { ?>
																			<a href='<?php echo $lien; ?>'><img src='<?php echo ROOT_PATH; ?>images/fiche_site.png' width='15px' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Fiche propri&eacute;taire"></a>
																			<?php } ?>
																		</td>	
																	</tr>
																</table>
															</td>
														</tr><?php } ?>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td style="border-bottom: solid #004494 1px" colspan="2"></td>
										</tr>
									</table>
										
									<?php } ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id='maitrise'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<table width = '100%' class='no-border' id='tbl_mf' align='CENTER' CELLSPACING='0' CELLPADDING='4' border='0' style="padding-top: 25px;">
									<tbody>
										<form id="frm_export_mfu" name="frm_export_mfu" onsubmit="return false" action='foncier_export.php?filename=<?php echo str_replace(array("M. ", "Mme. ", "Mlle. ", " "), array("","","", ""), $proprio_lib); ?>_MFU.csv' method='POST'>
											<label class="label">
												Exporter :
													<img id='export_btn' src="<?php echo ROOT_PATH; ?>images/xls-icon.png" style="cursor:pointer;" onclick="exportTable('frm_export_mfu', 'tbl_mf_entete_fixed', 'tbl_mf_corps');" width='20px' />
													<input type="hidden" id="tbl_post" name="tbl_post" value=""/>
												<span style='padding-left:75px;'></span>
											</label>
										</form>
									</tbody>
									<tbody id='tbl_mf_entete'>
										<form id="frm_mf_entete" onsubmit="return false">
											<tr>
												<td class='filtre' align='center' width='95px'>
													<label class="btn_combobox">
														<select name="filtre_acte" onchange="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_acte" class="combobox" style='text-align:center'>
															<option value='-1'>Acte</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='90px'>
													<label class="btn_combobox">
														<select name="filtre_site_id_mfu" onchange="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_site_id_mfu" class="combobox" style='text-align:center'>
															<option value='-1'>SITE ID</option>
														</select>
													</label>
												</td>												
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" type='text' id='filtre_acte_id' size='9' placeholder='ID' onfocus="this.placeholder = '';" onblur="this.placeholder = 'ID';">   
												</td>		
												<td class='filtre' align='center'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" type='text' id='filtre_libelle' placeholder='Libell&eacute;' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Libell&eacute;';"> 
												</td>
												<td class='filtre' align='center' width='270px' >
													<label class="btn_combobox">
														<select name="filtre_type_mf" onchange="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_type_mf" class="combobox" style='text-align:center'>
															<option value='-1'>Type d'acte</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='95px'>
													<input style='text-align:center' class='editbox' oninput="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" type='text' id='filtre_date' size='12' placeholder='Date' onfocus="this.placeholder = '';" onblur="this.placeholder = 'Date';"> 
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_statut" onchange="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_statut" class="combobox" style='text-align:center'>
															<option value='-1'>Statut</option>
														</select>
													</label>
												</td>
												<td class='filtre' align='center' width='130px' style='white-space:nowrap'>
													<label class="btn_combobox">
														<select name="filtre_doc_pdf" onchange="create_lst_mf('modif', 0, '<?php echo $dnuper; ?>');" id="filtre_doc_pdf" class="combobox" style='text-align:center'>
															<option value='-1'>PDF</option>
														</select>
													</label>
												</td>
												<td width='25px'>	
													<img onclick="
													document.getElementById('filtre_acte').value='-1';
													document.getElementById('filtre_acte_id').value='';
													document.getElementById('filtre_site_id_mfu').value='-1';
													document.getElementById('filtre_libelle').value='';
													document.getElementById('filtre_type_mf').value='';
													document.getElementById('filtre_date').value='';
													document.getElementById('filtre_statut').value='-1';
													document.getElementById('filtre_doc_pdf').value='-1';
													create_lst_mf('charge', 0, '<?php echo $dnuper; ?>');
													" src='<?php echo ROOT_PATH; ?>images/supprimer.png'>
												</td>
											</tr>
										</form>
									</tbody>
									<thead id="tbl_mf_entete_fixed" class="tbl_entete_fixed" style="position:fixed;top:0px;display:none; height:50px;background-color:#accf8a;padding-top:15px;">
										<tr>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>Acte</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>SITE ID</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>
												<label class='label' style='text-align:center;'>ID</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Libell&eacute;</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Type d'acte</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Date</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>Statut</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>PDF</label>
											</th>
											<th style='text-align:center;border:0px solid #accf8a;padding:6px 5px;'>	
												<label class='label' style='text-align:center;'>&nbsp;</label>
											</th>
										</tr>
									</thead>
									<tbody id='tbl_mf_corps'>
									</tbody>
									<?php If (in_array('maitrise', $last_maj_tbl)) { ?>
									<tbody id='tbl_mfu_maj'>
										<tr>
											<td style='text-align:right' colspan='8'>
												<label class="last_maj">
													Derni&egrave;re mise &agrave; jour le <?php echo date_transform(substr($last_maj[array_search('maitrise', $last_maj_tbl)]['maj_date'], 0, 10)); ?> par <?php echo $last_maj[array_search('maitrise', $last_maj_tbl)]['maj_user']; ?>
												</label>
											</td>
										</tr>
									</tbody>
									<?php } ?>
								</table>
							</td>
						</tr>
					</table>
				</div id='maitrise'>
			</div>
			<div class='lib_alerte' id='prob_saisie' style='display:none; margin-top:25px'>Veuillez compl&eacuteter votre saisie</div>
			<div id="explorateur_histo" class="explorateur_span" style="display: none;">
				<table class="explorateur_fond" cellspacing="5" cellpadding="5" border="0" frame="box" rules="NONE">
					<tbody>
						<tr>
							<td>
								<div id="explorateur_new">
									<table class="explorateur_contenu" cellspacing="0" cellpadding="4" border="0" align="center" frame="box" rules="NONE">
										<tbody id='tbody_histo'>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</tbody>		
				</table>
			</div>
			<div id="loading" class="loading" style="display: none;">
				<img src="<?php echo ROOT_PATH; ?>images/ajax-loader.gif">
			</div>
		</section>
		<?php
			$res_info_proprio->closeCursor();
			$res_lst_site->closeCursor();
			$res_cpt_proprio->closeCursor();
			$res_last_maj->closeCursor();
			$res_check_grant->closeCursor();
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
