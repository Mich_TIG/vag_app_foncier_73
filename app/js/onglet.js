/**************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
Copyright Conservatoire d’espaces naturels de Savoie, 2018
-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
-	Adresse électronique : sig at cen-savoie.org

Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
-	de gérer et suivre ces actes (convention et acquisition) ;
-	et d’en établir des bilans.
-	Un module permet de réaliser des sessions d’animation foncière.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
***************************************************************************************************************************
***************************************************************************************************************************
**************************************************************************************************************************/

$(function() {
	$('.list').tabs({fxShow: true, fxSpeed: 'fast' });
});

$(document).ready( function () {	
	// Permet d'identifier si une fonction est à déclencher au clic sur un onglet. Si c'est le cas, la fonction est lancée même si on accède a cet onglet directement depuis un lien (bouton retour par exemple)
	if ($('.tabs-selected').find('a').attr('onclick')) {
		jQuery(function($){
			$('.tabs-selected').find('a').click();
		});
	}
	
	$('#onglet ul li > a').click(function(){		
		var dieze = $(this).attr('href').substring(1,$(this).attr('href').length);
		setTimeout(function(){
			resize_fieldset(dieze)
		}, 200);
		$.ajax({ //On entre maintenant dans la partie ajax
			url       : JS_ROOT_PATH+"includes/cen_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
			type      : "post", //type de communication entre les différentes pages/fonctions/langages
			dataType : "json", //format de la réponse permettant notamment un retour en variable tableau
			data      :{ 
				fonction:'fct_updateLinkBack', var_li: $(this).attr('href').substring(1,$(this).attr('href').length)//variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
			}, 
			success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
				if (data['sessmdp'] == 'invalide') {
					window.location.reload()
				}
				$('#explorateur_debug').find('#page').each(function(){
					$(this).val(data['back']);
				});
				$('#deco').find('#chg_mode').each(function(){
					$(this).attr('onclick', $(this).attr('onclick').replace($(this).attr('onclick').substring($(this).attr('onclick').indexOf('#'), $(this).attr('onclick').length), data['back'].substring(data['back'].indexOf('#'), data['back'].length))+"'");
				});
			} 
		});
	});	
} ) ;