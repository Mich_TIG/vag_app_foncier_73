/**************************************************************************************************************************
***************************************************************************************************************************
***************************************************************************************************************************
Copyright Conservatoire d’espaces naturels de Savoie, 2018
-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
-	Adresse électronique : sig at cen-savoie.org

Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
-	de gérer et suivre ces actes (convention et acquisition) ;
-	et d’en établir des bilans.
-	Un module permet de réaliser des sessions d’animation foncière.

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
***************************************************************************************************************************
***************************************************************************************************************************
**************************************************************************************************************************/

function Init() {
	if ($('#compteur').length) {
		$('#compteur').html(waitTime);
		x = window.setInterval('Decompte()', 1000);
	}	
};

function Decompte() {
	((waitTime > 0)) ? (window.document.getElementById('compteur').innerHTML = --waitTime) : (window.clearInterval(x));
	if (waitTime == 0) {
		window.location = url;
	}
};

/* Fonction permettant d'harmoniser la hauteur de tous les fieldset d'un bloc.
La variable elt correspond au bloc en question.
Pour que cette procédure fonctionne, il faut préciser la classe tr_fieldset à la ligne surlaquelle se situe les fieldset à harmoniser. */
function resize_fieldset(elt='main') {
	$('#'+elt+' .tr_fieldset').each(function(){
		maxHeight = 0
		$(this).find('fieldset').each(function(){		
			if (parseInt($('#'+this.id+' table:first').height()) +16  > maxHeight) {
				maxHeight = parseInt($('#'+this.id+' table:first').height()) + 16;
			};
		});
		$(this).find('fieldset').each(function(){
			$(this).height(maxHeight)
		});		
	});
}

/*
 Fonction complexe permettant l'export XLS d'un tableau HTML
 */
 function exportTable(frm, entete, corps) {
	var tblXLS = [];
	var first_line = [];
	$('#'+ entete + ' > tr > th').each(function(){
		if (!$(this).attr('class') || ($(this).attr('class') && $(this).attr('class').split('no-export').length == 1)) {
			cell_contenu = $(this).html();
			if (cell_contenu.split('no-export').length > 1) {
				pos1 = cell_contenu.indexOf('>', cell_contenu.indexOf('no-export'));
				pos2 = cell_contenu.indexOf('</', cell_contenu.indexOf('no-export'));
				cell_contenu_temp1 = cell_contenu.substr(0, pos1)
				cell_contenu_temp2 = cell_contenu.substr(pos2)
				cell_contenu = cell_contenu_temp1+cell_contenu_temp2
			}
			cell_contenu = cell_contenu.replace(/<(?:.|\s)*?>/gm,"");
			cell_contenu = cell_contenu.replace(/\t/g, ''); //nettoyage de la variable pour éliminer les balises html
			cell_contenu = cell_contenu.replace(/\n/g, ''); //nettoyage de la variable pour éliminer les balises html
			if ((cell_contenu.toLowerCase().split('surf').length == 2 || cell_contenu.toLowerCase().split('contenance').length == 2 || cell_contenu.toLowerCase().split('conten;').length == 2) && cell_contenu.toLowerCase().split('(m').length != 2) { 
				cell_contenu = cell_contenu.concat(' (ha)');
			}
			if (cell_contenu.split('&nbsp;').length == 1) {
				first_line.push(cell_contenu);
			}
		}
	}); 
	first_line.push('*$');
	tblXLS.push(first_line);
	
	$('#'+ corps + ' > tr:visible').each(function(){ 
		var new_line = [];
		if ($(this).attr('class') && $(this).attr('class').split('no-export').length == 1) {
			if ($(this).attr('class').split('rowspan').length > 1) {
				$(this).prevAll('.toggle_tr:not(".rowspan")').first().find('td').each(function(){
					if (!$(this).attr('class') || ($(this).attr('class') && $(this).attr('class').split('no-export').length == 1)) {
						cell_contenu = $(this).html();
						cell_contenu = cell_contenu.replace(/<(?:.|\s)*?>/gm,"");
						cell_contenu = cell_contenu.replace(/\t/g, ''); //nettoyage de la variable pour éliminer les balises html
						cell_contenu = cell_contenu.replace(/\n/g, ''); //nettoyage de la variable pour éliminer les balises html
						tbl_cell_contenu = cell_contenu.split('.')						
						if (tbl_cell_contenu.length == 3) {
							is_contenance = 'good'
							for(var i=0; i<tbl_cell_contenu.length; i++) {		
								if (!$.isNumeric(tbl_cell_contenu[i]) || tbl_cell_contenu[i] == '') {
									is_contenance = 'bad'
								}
							}
							if (is_contenance == 'good') {
								cell_contenu = cell_contenu.replace('.', '%&#&#%').replace('.', '');
							}
						}
						if ($(this).attr('rowspan') > 1 && cell_contenu.split('&nbsp;').length == 1) {
							new_line.push(cell_contenu);
						}
					}
				});
			}
			$(this).find('td').each(function(){
				if (!$(this).attr('class') || ($(this).attr('class') && $(this).attr('class').split('no-export').length == 1)) {
					cell_contenu = $(this).html();				
					if (cell_contenu.split('no-export').length > 1) {
						pos1 = cell_contenu.indexOf('>', cell_contenu.indexOf('no-export'));
						pos2 = cell_contenu.indexOf('</', cell_contenu.indexOf('no-export'));
						cell_contenu_temp1 = cell_contenu.substr(0, pos1)
						cell_contenu_temp2 = cell_contenu.substr(pos2)
						cell_contenu = cell_contenu_temp1+cell_contenu_temp2
					}
					if (($(this).find('img').attr('alt')) != null) {
						cell_contenu = $(this).find('img').attr('alt');
					} //espèce
					cell_contenu = cell_contenu.replace(/,/g,'/');
					cell_contenu = cell_contenu.replace(/  /g,' ');
					
					cell_contenu = cell_contenu.replace(/<(?:.|\s)*?>/gm,"");
					cell_contenu = cell_contenu.replace(/\t/g, ''); //nettoyage de la variable pour éliminer les balises html
					cell_contenu = cell_contenu.replace(/\n/g, ''); //nettoyage de la variable pour éliminer les balises html
					tbl_cell_contenu = cell_contenu.split('.')						
					if (tbl_cell_contenu.length == 3) {
						is_contenance = 'good'
						for(var i=0; i<tbl_cell_contenu.length; i++) {		
							if (!$.isNumeric(tbl_cell_contenu[i]) || tbl_cell_contenu[i] == '') {
								is_contenance = 'bad'
							}
						}
						if (is_contenance == 'good') {
							cell_contenu = cell_contenu.replace('.', '%&#&#%').replace('.', '');
						}
					}
					if (cell_contenu.split('&nbsp;').length == 1) {
						new_line.push(cell_contenu);
					}
				}
			});
			new_line.push('*$');
			tblXLS.push(new_line);
		}
	}); 
	document.forms[frm].elements['tbl_post'].value = tblXLS;
	document.forms[frm].submit();
 }
 
 function displayPlusMoins(id, balise, id_suffixe, id_prefixe) {
	if ($('#'+balise+id_suffixe+id_prefixe).css('display') == 'none') {
		$('#'+balise+id_suffixe+id_prefixe).css('display', '')
		$(id).attr('src', $(id).attr('src').replace('plus', 'moins'))
	} else {
		$('#'+balise+id_suffixe+id_prefixe).css('display', 'none')
		$(id).attr('src', $(id).attr('src').replace('moins', 'plus'))
	}
}

function sortTable(table, tid, col, type, ord) {
	var mybody = $("#"+tid),
	lines = mybody.children('tr'),
	sorter = [],
	i = -1,
	j = 0;
	
	while(lines[++i]){
		cell = clear_accent(lines.eq(i).children('td').eq(col).text());
		if (type == 'date') {
			cell = inverseDate(cell.trim());
		}
		sorter.push([lines.eq(i), cell])
	}
	
	if(ord == 'asc'){
		if (type == 'num') {
			sorter.sort(compareNombres);
		} else {
			sorter.sort();
		}
	} else if (ord == 'desc') {
		if (type == 'num') {
			sorter.sort(compareNombres).reverse();
		} else {
			sorter.sort().reverse();
		}
	}
	
	while(sorter[++j]){
		mybody.append(sorter[j][0]);
	}
	
	var arrayLignes = document.getElementById(table).rows; //on récupère les lignes du tableau
	var longueur = arrayLignes.length;//on peut donc appliquer la propriété length
	var first = 0;
	
	for(var i=1; i<longueur; i++) {//on peut directement définir la variable i dans la boucle
		if (arrayLignes[i].style.display == '') {
			if (first == 0) {
				arrayLignes[i].className = arrayLignes[i].className.replace('toggle_tr l1', 'toggle_tr l0');
				first = 1;
			} else {			
				var k = i-1;
				while (arrayLignes[k].style.display != '') {
					k = k - 1;
				}
				if (arrayLignes[k].className.split('toggle_tr l0').length > 1) {
					arrayLignes[i].className = arrayLignes[i].className.replace('toggle_tr l0', 'toggle_tr l1');
				} else {
					arrayLignes[i].className = arrayLignes[i].className.replace('toggle_tr l1', 'toggle_tr l0');
				}
			}
		}
	}	
}

/* Fonction utilisée en particulier sur les pages foncier_liste_site.php et foncier_bilan.php
Elle permet d'adapter la liste présentée sur la page en fonction des filtres renseignés en début de liste
Le principe général est de contrôler pour chaque ligne la présence en tout ou partie du filtre dans la colonne correspondante
Si on ne trouve pas de correspondance, on change le style à 'display=none'
------------Les variables
------------frm -> id du formulaire contenant les filtres
------------table -> id du tbody contenant les lignes à filtrer
------------nbr_lettre -> nombre de lettres minimum saisies avant d'appliquer le filtre*/
function maj_table(frm, table, nbr_lettre) {
	var filtre = new Array(); //variable tableau stockant la valeur du filtre. L'indice correspondant au numéro de la colonne
	var elements = document.forms[frm].elements; //variable tableau tableau récupérant l'ensemble des éléments du formulaire 'frm'
	for (k=0; k<elements.length; k++){ //boucle pour chaque élément du formulaire 'frm'
		/* Pour chaque élément on controle son type son id et son contenu
		Chaque id doit commencer par 'filtre'
		Si le contenu est vide ou null on n'oublie pas d'insérer une valeur de filtre à null pour la colonne afin d'éviter tout décalage dans la comparaison filtre/colonne */
		if (elements[k].type == 'select-one' && elements[k].id.substring(0,6) == 'filtre' && document.getElementById(elements[k].id).value != '-1') {
			filtre.push(document.getElementById(elements[k].id).options[document.getElementById(elements[k].id).selectedIndex].text);
		} else if (elements[k].type == 'select-one' && elements[k].id.substring(0,6) == 'filtre' && document.getElementById(elements[k].id).value == '-1') {
			filtre.push('');
		} else if (elements[k].type == 'text' && elements[k].id.substring(0,6) == 'filtre' && elements[k].id.substring(7,elements[k].id.length).toUpperCase() != document.getElementById(elements[k].id).value.toUpperCase()) {
			filtre.push(document.getElementById(elements[k].id).value);
		} else if (elements[k].type == 'text' && elements[k].id.substring(0,6) == 'filtre' && elements[k].id.substring(7,elements[k].id.length).toUpperCase() == document.getElementById(elements[k].id).value.toUpperCase()) {
			filtre.push('');
		}
	}
	/* Une fois la variable tableau 'filtre' bien constituée on passe à la partie comparaison pour chaque ligne du tableau
	Pour cela il faut préparer quelques variables pour caractériser le tableau de données à filtrer */
	var arrayLignes = document.getElementById(table).rows; //variable tableau stockant les lignes du tableau 'table'
	var longueur = arrayLignes.length;//variable stockant le nombre de ligne du tableau 'table'
	var first = 0; //variable initilaisée à 0 et servant en fin de boucle pour adapter le changement de couleur une ligne sur deux
	for(var i=0; i<longueur; i++) {//boucle sur chaque ligne du tableau 'table'
		for(var j=0; j<filtre.length; j++) { //boucle sur chaque colonne du tableau en utilisant la variable tableau 'filtre'
			var cell_contenu = arrayLignes[i].cells[j].innerHTML; //variable stockant le contenu de la cellule en cours de bouclage
			cell_contenu = cell_contenu.replace('<center>', ''); //nettoyage de la variable pour éliminer les balises html
			cell_contenu = cell_contenu.replace('</center>', ''); //nettoyage de la variable pour éliminer les balises html
			cell_contenu = cell_contenu.replace('</a>', ''); //nettoyage de la variable pour éliminer les balises html
			cell_contenu = cell_contenu.replace('</span>', ''); //nettoyage de la variable pour éliminer les balises span
			cell_contenu = cell_contenu.replace(/\t/g, ''); //nettoyage de la variable pour éliminer les balises html
			pos_cell_contenu = cell_contenu.lastIndexOf('">'); //nettoyage de la variable pour éliminer les balises html
			cell_contenu = cell_contenu.substring(pos_cell_contenu+2, cell_contenu.length); //nettoyage de la variable pour éliminer les balises html
			/* On arrive à la comparaison entre le contenu de la cellule et le filtre
			On vérifie d'abord que le nombre de lettres nécessaires est respecté puis on regarde si on trouve le filtre dans la cellule en passant tout en majuscule pour ne pas s'inquiéter de la casse
			Si lastIndexOf renvoie -1 alors le filtre n'est pas trouvé dans la chaine et la ligne est rendue 'display = none' soit invisible
			Sinon le filtre est trouvé dans la chaine on affiche la ligne par display = '' */
			if (filtre[j].length > nbr_lettre && cell_contenu.replace("'", "").toUpperCase().lastIndexOf(filtre[j].replace("'", "").toUpperCase()) == -1) {
				arrayLignes[i].style.display = 'none';
				break;
			}
			else {
				arrayLignes[i].style.display = '';
			}		
		}
		/* Avant de passer à la ligne suivante il faut reprendre la classe css de la ligne pour alterner les couleurs une ligne sur deux */
		if (arrayLignes[i].style.display == '') { //on s'assure que la ligne est visible
			if (first == 0) { //on regarde si il s'agit de la première ligne visible
				arrayLignes[i].className  = arrayLignes[i].className.replace('toggle_tr l1', 'toggle_tr l0'); //on adapte la nouvelle classe css de la première ligne visible
				first = 1; //on change la variable first de 0 à n'importe quoi d'autre pour indiquer au prochain passage que ce n'est plus la première ligne visible
			} else { //pour les autres lignes après la première
				var k = i-1; //variable stockant l'indice de la ligne précédente
				while (arrayLignes[k].style.display != '') { //on boucle en remontant le tableau jusqu'a ce qu'une ligne soit visible
					k = k - 1;
				}
				/* on adapte la classe de la ligne visible en cours de bouclage en fonction de la classe de la ligne visible précédente */
				if (arrayLignes[k].className == 'toggle_tr l0') { 
					arrayLignes[i].className  = arrayLignes[i].className.replace('toggle_tr l0', 'toggle_tr l1');
				} else {
					arrayLignes[i].className  = arrayLignes[i].className.replace('toggle_tr l1', 'toggle_tr l0');
				}
			}
		}
	}
}


//Fonction permettant de redimensionner les textareas en fonction du texte affiché et modifié
function resizeTextarea(id) {
	if (document.getElementById(id) != null) {
		if($('#'+id).height()+20 < document.getElementById(id).scrollHeight) {
			$('#'+id).height(document.getElementById(id).scrollHeight-20)
		}
	}
}

//Fonction permettant de supprimer les accents d'une chaine
function clear_accent(str){
	var accent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
	];
	var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
	for(var i = 0; i < accent.length; i++){
		str = str.replace(accent[i], noaccent[i]);
	}
	return str;
}

//Fonction pour écrire une date au format aaaa-mm-jj. Le format en entrée DOIT ETRE jj/mm/aaaa
function inverseDate(date) {
	var inverse_date = date.substring(6,10).concat('-').concat(date.substring(3,5)).concat('-').concat(date.substring(0,2));
	return inverse_date;
}

/*
Fonction permettant d'aller automatiquement à la lettre choisie grace à la ligne alphabet
*/
function scrollToLetter(tbody, col, prefixe, first_line, letter) {
	var arrayLignes = document.getElementById(tbody).rows; //variable tableau stockant les lignes du tableau 'tbody'
	var longueur = arrayLignes.length;//variable stockant le nombre de ligne du tableau 'table'
	for(var i=0; i<longueur; i++) {//boucle sur chaque ligne du tableau 'table'
		if (arrayLignes[i].id.substring(0,prefixe.length) == prefixe) {
			var cell = arrayLignes[i].cells[col];
			var cell_contenu = arrayLignes[i].cells[col].innerHTML;
			var cell_contenu_clean = cell_contenu.replace(/<([^ >]+)[^>]*>/ig, "").replace(/\t/g, '').replace("Com Com ", "").replace("Com d'Agglo ", "").replace("de l'", "").replace("de ", "").replace("de la ", "").replace("des ", "").replace("du ", "");
			
			var cell_contenu_clean_first_letter = cell_contenu_clean.substring(1,2);
			if (cell_contenu_clean_first_letter == letter) {
				$('html,body').animate({
					scrollTop: $("#"+arrayLignes[i].id).offset().top - 30},
					'slow');
				break;
			}			
		}
	}
}

function unlockField(img_id, elt_id, field_id, avertissement_id, data_origin) {
	var img_src = $('#'+img_id).attr('src').replace('lock', 'lock_open');
	var img_onclick = $('#'+img_id).attr('onclick').replace('unlock', 'lock');
	var elt_class = $('#'+elt_id).attr('class').replace('readonly', '');
	$('#'+img_id).attr('src', img_src);
	$('#'+img_id).attr('onclick', img_onclick);
	$('#'+elt_id).attr('class', elt_class);
	$('#'+field_id).prop('disabled', false );
	if (field_id.substring(0,9) != 'acte_date') {
		$('#'+field_id).prop('readonly', false );
	}
	
	if ($('#'+avertissement_id).css('display') == 'none') {
		$('#'+avertissement_id).show(400);
	}
}

function lockField(img_id, elt_id, field_id, avertissement_id, data_origin) {
	var img_src = $('#'+img_id).attr('src').replace('lock_open', 'lock');
	var img_onclick = $('#'+img_id).attr('onclick').replace('lock', 'unlock');
	var elt_class = $('#'+elt_id).attr('class') + ' readonly';
	$('#'+img_id).attr('src', img_src);
	$('#'+img_id).attr('onclick', img_onclick);
	$('#'+elt_id).attr('class', elt_class);
	$('#'+field_id).prop('disabled', true );
	$('#'+field_id).val(data_origin);
	if ($('#'+avertissement_id).css('display') != 'none') {
		$('#'+avertissement_id).hide();
	}
	$('#'+avertissement_id).hide();
}

function deleteField(imglock_id, imgsuppr_id, elt_id, field_id, data_origin) {
	$('#'+imgsuppr_id).hide();
	var img_src = $('#'+imglock_id).attr('src').replace('lock', 'lock_open');
	$('#'+imglock_id).attr('src', img_src);
	var elt_class = $('#'+elt_id).attr('class').replace('readonly', '');
	$('#'+elt_id).attr('class', elt_class);
	$('#'+field_id).prop('disabled', false );
	$('#'+field_id).val('N.D.')
}

/*
Fonction permettant d'adapter et de personnaliser les éléments de formulaire de type input='file'
un input type='file caché est appelé lors d'un click sur un bouton classique
On récupère 
*/
function getfile(etape, id_file, id_text){
	if (etape == 'etape1') {
		 document.getElementById(id_file).click();
	} else if (etape == 'etape2') {
		document.getElementById(id_text).innerHTML = document.getElementById(id_file).value;
		$('#remove_acte_pdf').val('');
		$('#text_acte_pdf').css('text-decoration', '');
	}
}

function isDate(myDate) {
	if(myDate != '' && myDate != 'N.D.') {
		//Declare Regex  
		var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
		var dtArray = myDate.match(rxDatePattern); // is format OK?

		if (dtArray == null) {
			return false;
		}

		//Checks for mm/dd/yyyy format.
		dtDay = dtArray[1];
		dtMonth= dtArray[3];
		dtYear = dtArray[5];

		if (dtMonth < 1 || dtMonth > 12) {
			return false;
		} else if (dtDay < 1 || dtDay> 31) {
			return false;
		} else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay==31) {
			return false;
		} else if (dtMonth == 2) {
			var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
			if (dtDay> 29 || (dtDay==29 && !isleap)) {
				return false;
			}
		}
		return true;
	} else {
		return true;
	}
}


/* 
Procedure utilisée à chaque validation de formulaire
La première partie contrôle que tous les elements obligatoires du formulaire (cad avec le prefixe 'need_' en id et en name) sont bien renseignés
Après cette première verification on passe à un contrôle dédié à chaque formulaire
Les 10 dernières lignes permettent l'envoi ou l'echec de l'envoi du formulaire 
 */
function valide_form(form) {
	var verif = 'OUI';
	var elements = document.forms[form].elements;
	var msg = "Veuillez compl&eacuteter votre saisie";
	for (i=0; i<elements.length; i++){
		if (elements[i].type != 'submit' && elements[i].id.substring(0,5) == 'need_') {
			if (((elements[i].type == 'select-one') && (elements[i].value == '-1') && !$('#'+elements[i].id).is(":hidden")) || ((elements[i].type == 'text') && (elements[i].value.trim() == '') && !$('#'+elements[i].id).is(":hidden"))) {
				document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
				verif = 'NON';
			} else {
				document.getElementById(elements[i].id).style.border = "2px solid #accf8a";
			}
		}
	}
	//verification dédié du formulaire
	if (form.substring(0,20) == 'frm_modif_structure_') {
		$('#'+form).find('select').each(function(){
			if (this.id.substring(0,18) == 'need_lst_fonctpers' && $('#hid_notaire_id').length && $(this).val() == $('#hid_notaire_id').val() && $('#'+this.id.replace('need_lst_fonctpers', 'need_new_adresse3')).val() == '') {
				$('#'+this.id.replace('need_lst_fonctpers', 'new_adresse3')).css({'border-color':'#fe0000'});
				verif = 'NON';
			} else {
				$('#'+this.id.replace('need_lst_fonctpers', 'new_adresse3')).css({'border-color':'#accf8a'});
			}
		});		
	}
	if ((form == 'frm_ajout_contact') && (verif == 'OUI')) {	
		rq_verif_individu = "SELECT individu_id FROM personnes.d_individu WHERE CASE WHEN strpos(nom, ' (I') > 0 THEN substr(nom, 1, strpos(nom, ' (I')-1) = '"+document.forms[form].elements['need_new_nom'].value+"' ELSE nom = '"+document.forms[form].elements['need_new_nom'].value+"' END AND prenom = '"+document.forms[form].elements['need_new_prenom'].value+"'";
		verif = 'NON';
		
		$.ajax({
			url       : JS_ROOT_PATH+"includes/cen_fct_ajax.php",
			type      : "post",
			dataType : "json",
			data      :{ 
				fonction:'rqExistant',rq: rq_verif_individu, check_form: 'frm_ajout_contact', elt_modif: 'need_new_nom',msg: document.forms[form].elements['need_new_nom'].value+' '+document.forms[form].elements['need_new_prenom'].value+' existe d&eacute;j&agrave;', elt_value: document.forms[form].elements['need_new_nom'].value, origine: document.forms[form].elements['origine'].value
			},
			success: function(data) {
				div = data['div'];
				code = data['code'];
				if (div != '' && code != '') {
					document.getElementById(div).innerHTML = code;
					document.getElementById(div).style.display='';
					changeOpac(20, 'frm_ajout_contact');
				} else {
					document.forms[form].submit();
				}
			}
		});		
	} else if ((form == 'frm_modif_proprio' || form == 'frm_ajout_structure' || form.substring(0,19) == 'frm_modif_structure') && (verif == 'OUI')) {
		for (i=0; i<elements.length; i++){
			if (elements[i].id.substring(0,7) == 'new_tel' && elements[i].value != '') {
				if (elements[i].value.substring(0,1) != '0' || isNaN(elements[i].value.replace(/ /g,"").substring(1,10)) == true || (elements[i].value.length != 14 && elements[i].value.length != 10)) {
					document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
					verif = 'NON';
					msg = "Veuillez v&eacute;rifier votre saisie (error1)";
				} else {
					document.getElementById(elements[i].id).style.border = "2px solid #accf8a";
				}
			} else if (elements[i].id.substring(0,8) == 'new_mail' && elements[i].value != '') {
				var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
				if(reg.test(elements[i].value)) {
					document.getElementById(elements[i].id).style.border = "2px solid #accf8a";
				} else {
					document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
					verif = 'NON';
					msg = "Veuillez v&eacute;rifier votre saisie (error2)";
				}
			}
		}
	} else if ((form == 'frm_modif_mfu' || form == 'frm_new_mfu')){
		for (i=0; i<elements.length; i++){			
			if (elements[i].id == 'acte_id') {				
				if (elements[i].value == "''") {	
					document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
					verif = 'NON';
					msg = "Veuillez v&eacute;rifier votre saisie (errorID)";
				} else {
					document.getElementById(elements[i].id).style.border = "2px solid #a2c282 ";
				}
			}
			if (elements[i].id.substring(0,11) == 'acte_frais_') {
				if (elements[i].value != '' && $('#'+elements[i].id).prop('disabled') == false && ($.isNumeric(elements[i].value.replace(',', '.')) == false || parseFloat(elements[i].value.replace(',', '.')) < 0)) {	
					document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
					verif = 'NON';
					msg = "Veuillez v&eacute;rifier votre saisie (error3)";
				} else {
					document.getElementById(elements[i].id).style.border = "2px solid #a2c282 ";
				}
			}
			if (elements[i].id.substring(0,10) == 'acte_date_') {
				if(!isDate(elements[i].value) &&  $('#'+elements[i].id).prop('disabled') == false) {
					document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
					verif = 'NON';
					msg = "Veuillez v&eacute;rifier votre saisie (error4)";		
				} else {
					document.getElementById(elements[i].id).style.border = "2px solid #a2c282 ";
				}
			}
			if (elements[i].id.substring(0,10) == 'acte_date_' && elements[i].id != 'acte_date_compromis' && elements[i].id != 'acte_date_promesse' && elements[i].id != 'acte_date_rappel' && elements[i].id != 'acte_date_recep_courrier' && elements[i].id != 'acte_date_envoi_courrier' && isDate(elements[i].value)) {
				if ((elements[i].value == '' || elements[i].value == 'N.D.' || elements[i].value == 'N.P.') && (document.getElementById('need_acte_statut').value == '1' || document.getElementById('need_acte_statut').value == '2' || document.getElementById('need_acte_statut').value == '4') &&  $('#'+elements[i].id).prop('disabled') == false) {
					document.getElementById(elements[i].id).style.border = "2px solid #fe0000";
					verif = 'NON';
					msg = "Veuillez v&eacute;rifier votre saisie (error5)";					
				} else {
					document.getElementById(elements[i].id).style.border = "2px solid #a2c282 ";
				}
			}
			if (elements[i].id == 'file_acte_pdf') {			
				var file = document.getElementById('file_acte_pdf').files[0];
				if (file && file.size > 2000000) { // 10 MB (this size is in bytes)
					$('#text_acte_pdf').css('color', '#fe0000');
					verif = 'NON';
					msg = "Veuillez v&eacute;rifier votre saisie (PDF trop volumineux)";	    
				} else {
					$('#text_acte_pdf').css('color', '#004494');
				}
			}
		}
		
		if ($('#acte_date_compromis').length && $('#acte_date_signature').length && $('#acte_date_compromis').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_compromis').val()) > inverseDate($('#acte_date_signature').val()) && document.getElementById('need_acte_statut').value != '0' && document.getElementById('need_acte_statut').value != '3') {
			$('#acte_date_compromis').css({'border-color':'#fe0000'});
			verif = 'NON';
			msg += "<br>Veuillez contr&ocirc;ler la date de compromis de vente (error7)";
		} else if ($('#acte_date_compromis').length && $('#acte_date_signature').length && $('#acte_date_compromis').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_compromis').val()) < inverseDate($('#acte_date_signature').val())){
			$('#acte_date_compromis').css({'border-color':'#a2c282'});
		}
		
		if ($('#acte_date_promesse').length && $('#acte_date_signature').length && $('#acte_date_promesse').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_promesse').val()) > inverseDate($('#acte_date_signature').val()) && document.getElementById('need_acte_statut').value != '0' && document.getElementById('need_acte_statut').value != '3') {
			$('#acte_date_promesse').css({'border-color':'#fe0000'});
			verif = 'NON';
			msg += "<br>Veuillez contr&ocirc;ler la date de promesse d'achat (error9)";
		} else if ($('#acte_date_promesse').length && $('#acte_date_signature').length && $('#acte_date_promesse').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_promesse').val()) < inverseDate($('#acte_date_signature').val())) {
			$('#acte_date_promesse').css({'border-color':'#a2c282'});
		}
		if ($('#acte_date_delib_ca').length && $('#acte_date_signature').length && $('#acte_date_delib_ca').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_delib_ca').val()) > inverseDate($('#acte_date_signature').val()) && document.getElementById('need_acte_statut').value != '0' && document.getElementById('need_acte_statut').value != '3') {
			$('#acte_date_delib_ca').css({'border-color':'#fe0000'});
			verif = 'NON';
			msg += "<br>Veuillez contr&ocirc;ler la date de d&eacute;lib&eacute;ration en CA (error10)";
		} else if ($('#acte_date_delib_ca').length && $('#acte_date_signature').length && $('#acte_date_delib_ca').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_delib_ca').val()) < inverseDate($('#acte_date_signature').val())){
			$('#acte_date_delib_ca').css({'border-color':'#a2c282'});
		}
		if ($('#acte_date_fin').length && $('#acte_date_fin').is(":visible") && $('#acte_date_signature').length && $('#acte_date_fin').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_fin').val()) < inverseDate($('#acte_date_signature').val()) && document.getElementById('need_acte_statut').value != '0' && document.getElementById('need_acte_statut').value != '3') {
			$('#acte_date_fin').css({'border-color':'#fe0000'});
			verif = 'NON';
			msg = "Veuillez v&eacute;rifier votre saisie (error11)";
		} else if ($('#acte_date_fin').length && $('#acte_date_signature').length && $('#acte_date_fin').val() != '' && $('#acte_date_signature').val() != '' && inverseDate($('#acte_date_fin').val()) > inverseDate($('#acte_date_signature').val())){
			$('#acte_date_fin').css({'border-color':'#a2c282'});
		}
		if ($('#acte_date_debut').length && $('#acte_date_fin').length && $('#acte_date_fin').is(":visible")  && $('#acte_date_debut').val() != '' && $('#acte_date_fin').val() != '' && inverseDate($('#acte_date_debut').val()) > inverseDate($('#acte_date_fin').val())) {
			$('#acte_date_debut').css({'border-color':'#fe0000'});
			verif = 'NON';
			msg = "Veuillez v&eacute;rifier votre saisie (error12)";
		} else if ($('#acte_date_debut').length && $('#acte_date_fin').length && $('#acte_date_debut').val() != '' && $('#acte_date_fin').val() != '' && inverseDate($('#acte_date_debut').val()) < inverseDate($('#acte_date_fin').val())){
			$('#acte_date_debut').css({'border-color':'#a2c282'});
		}
		
		if ($('#acte_duree').length && document.getElementById('need_acte_statut').value != '0' ) {
			if ($('#acte_duree').prop('disabled') == false && (Math.floor($('#acte_duree').val()) != $('#acte_duree').val() || $.isNumeric($('#acte_duree').val()) == false || $('#acte_duree').val() <= 0)) {
				$('#acte_duree').css({'border-color':'#fe0000'});
				verif = 'NON';
				msg = "Veuillez v&eacute;rifier votre saisie (error13)";
			} else {
				$('#acte_duree').css({'border-color':'#a2c282'});
			}
		}
			
		if ($('#acte_recond_nbmax').length) {
			if ($('#acte_recond_nbmax').prop('disabled') == false && (Math.floor($('#acte_recond_nbmax').val()) != $('#acte_recond_nbmax').val() || $.isNumeric($('#acte_recond_nbmax').val()) == false || $('#acte_recond_nbmax').val() <= 0)) {
				$('#acte_recond_nbmax').css({'border-color':'#fe0000'});
				verif = 'NON';
				msg = "Veuillez v&eacute;rifier votre saisie (error14)";
			} else {
				$('#acte_recond_nbmax').css({'border-color':'#a2c282'});
			}
		}
		
		if ($('#acte_recond_nbmax').length && document.getElementById('need_acte_statut').value != '0') {
			if ($('#acte_pas_tps').prop('disabled') == false && (Math.floor($('#acte_pas_tps').val()) != $('#acte_pas_tps').val() || $.isNumeric($('#acte_pas_tps').val()) == false || $('#acte_pas_tps').val() <= 0)) {
				$('#acte_pas_tps').css({'border-color':'#fe0000'});
				verif = 'NON';
				msg = "Veuillez v&eacute;rifier votre saisie (error15)";
			} else {
				$('#acte_pas_tps').css({'border-color':'#a2c282'});
			}
		}
		
		if ($('#nbr_parc_assoc_acte').length) {
			if ($('#nbr_parc_assoc_acte').val() == 0 && $('#need_acte_statut').val() == '1') {
				$('#fieldset_parcelle').css({'border-color': '#fe0000'});
				$('#fieldset_parcelle > legend').css({'color': '#fe0000'});
				verif = 'NON';
				msg += "<br>Veuillez v&eacute;rifier votre saisie (error16)";
			} else {
				$('#fieldset_parcelle').css({'border-color': '#6a6a6a'});
				$('#fieldset_parcelle > legend').css({'color': '#6a6a6a'});
			}
		}
		
		if ($('#current_parc_conv').length) {
			if ($('#current_parc_conv').val() == 'OUI' && $('#need_acte_statut').val() == '1') {
				$('#fieldset_parcelle').css({'border-color': '#fe0000'});
				$('#fieldset_parcelle > legend').css({'color': '#fe0000'});
				verif = 'NON';
				msg += "<br>Des parcelles conventionn&eacute;es sont associ&eacute;es &agrave; cet acte (error17)";
			} else {
				$('#fieldset_parcelle').css({'border-color': '#6a6a6a'});
				$('#fieldset_parcelle > legend').css({'color': '#6a6a6a'});
			}
		}
		
		if ($('#parc_ajout').length && $('#btn_add_parc').length) {
			if ($('#parc_ajout').val() == '') {
				verif = 'NON';
				$('#btn_add_parc').css({'border-color': '#fe0000'});
				msg += "<br>Vous devez associer des parcelles (error18)";
			} else {
				$('#btn_add_parc').css({'border-top-color': '#cce2b7'});
				$('#btn_add_parc').css({'border-right-color': '#6c6c6b '});
				$('#btn_add_parc').css({'border-bottom-color': '#6c6c6b '});
				$('#btn_add_parc').css({'border-left-color': '#cce2b7'});
			}
		}
		
		if (form == 'frm_new_mfu') {
			msg += "<br><br><u>ATTENTION, vos donn&eacute;es ne sont pas enregistr&eacute;es !!!</u>";
		}
		
	} else if (form == 'frm_creat_user' && verif == 'OUI') {
		var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
		if(reg.test(document.getElementById('need_user_mail').value)) {
			document.getElementById('need_user_mail').style.border = "2px solid #accf8a";
		} else {
			document.getElementById('need_user_mail').style.border = "2px solid #fe0000";
			verif = 'NON';
			msg = "Veuillez v&eacute;rifier l'adresse mail";
		}
		if ($('#chk_bd_foncier').is(':checked') || $('#chk_sicen').is(':checked')) {
			if ($('#chk_sicen').is(':checked') && (!$('#lst_sicen option:selected').length)) {
				$('#lbl_sicen').css({'color':'#fe0000'});
				verif = 'NON';
				msg = "Veuillez s&eacute;lectionner un groupe SiCen";
			} else {
				$('#lbl_sicen').css({'color':'#004494'});
			}
			if ($('#chk_bd_foncier').is(':checked') && (!$('#lst_bd_foncier option:selected').length)) {
				$('#lbl_bd_foncier').css({'color':'#fe0000'});
				verif = 'NON';
				msg = "Veuillez s&eacute;lectionner un groupe PostgreSQL";
			} else {
				$('#lbl_bd_foncier').css({'color':'#004494'});
			}
		} else {
			$('#lbl_bd_foncier').css({'color':'#fe0000'});
			$('#lbl_sicen').css({'color':'#fe0000'});
			verif = 'NON';
			msg = "Veuillez s&eacute;lectionner une base de donn&eacute;es";
		}
	} 
	
	if (verif == 'NON') {
		document.getElementById('prob_saisie').innerHTML = msg;
		setTimeout(function() {document.getElementById('prob_saisie').style.display = '';}, 600);
		if (form == 'frm_modif_mfu') {
			$.ajax({
				url       : "../foncier/foncier_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
				type      : "post",
				data      :{ 
					fonction:'fct_update_commentaire_acte', params: {var_acte_id: $('#acte_id').val(), var_type_acte: $('#filtre_type_acte').val(), var_acte_lib: $('#need_acte_lib').val(), var_commentaire: $('#acte_txt_com').val()} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page foncier_fct_ajax.php
				},
				beforeSend: function(){
					$("#loading").show();
				},
				complete: function(){
					$("#loading").hide();
				},
				success: function(data) { //On récupère du code dans la variable data
					$('#prob_saisie').html($('#prob_saisie').html() + '<br> <span style="font-weight: normal;">(si vous avez modifi&eacute; le libell&eacute; et/ ou les commentaires, ils ont &eacute;t&eacute; enregistr&eacute;s)</span>')
				}
			});	
		}
		return false;
	} else {
		document.getElementById('prob_saisie').style.display = 'none';
		return true;
	}
}

function ConfirmForm(FormName, elt, nbr) {
	document.forms[FormName].elements[elt].value = document.forms[FormName].elements[elt].value+nbr;
	document.forms[FormName].submit();
}

function cacheMess(aff1) {
	document.getElementById(aff1).style.display = 'none';
	if ($('#onglet').height() < $(window).height() - 297) {
		$('#main').css('height', $(window).height() - 297);
	}
}

//change the opacity for different browsers
function changeOpac(opacity, id) {
	var object = document.getElementById(id).style;
	object.opacity = opacity / 100;
	object.MozOpacity = opacity / 100;
	object.KhtmlOpacity = opacity / 100;
}




/*
Procédure complexe permettant l'ajout dynamique d'options supplémentaires dans les comboboxes
1. Affichage d'une fênetre avec zone de saisie dans la page en cours et inaccessibilité au formulaire principal : function addSpanValue_js
2. Verification dans la base de l'existance d'une entrée similaire : function ajaxCtrlSaisie
3. Proposition de choix similaires par bouton radio et confirmation
4. Ajout dans la base si necessaire et séléction du choix de l'opérateur dans la combobox
Arguments : 
	form_name : nom et id du formulaire utilisé comme élément de référence pour modifier l'opacité
	lst_name : name et id du menu déroulant à l'origine de la fonction
	table, champ_id et champ_lib : variables utilisées lors de l'appel de la fonction php pour faire la requête SQL de comparaison d'entrées existantes dans la base
	intitule, label : variables utilisées pour personnaliser la nouvelle fenêtre au cas par cas
	where : permet de passer un argument WHERE à la requête SQL côté php, NE PAS OUBLIER ->  , '') à la fin de l'appel de la fonction si inutile
*/
function addSpanValue_js(form_name, lst_name, table, champ_id, champ_lib, intitule, label, where){
	var code = "\r\n\t\t\t<table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_fond'>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<div id='explorateur_new'>\r\n\t\t\t\t\t\t\t <table RULES='NONE' FRAME='box' CELLSPACING = '5' CELLPADDING ='5' border = '0' class='explorateur_contenu' align='center'>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td colspan='2' style=\"text-align:right;\">\r\n\t\t\t\t\t\t\t\t\t\t<img width='15px' style=\"cursor:pointer\" src=\""+JS_ROOT_PATH+"images\/quitter.png\" onclick=\"cacheMess('explorateur');cacheMess('explorateur_alerte');changeOpac(100, '"+form_name+"');\">\r\n\t\t\t\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td class='label'>"+label+" : <\/td>\r\n\t\t\t\t\t\t\t\t\t<td><input type='text' class='editbox' id='new_elt' size='60'><\/td>\r\n\t\t\t\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td colspan='2'>\r\n\t\t\t\t\t\t\t\t\t\t<center>\r\n\t\t\t\t\t\t\t\t\t\t<input type='button' class='valid' onclick=\"ajaxCtrlSaisie('init', '"+form_name+"', '"+lst_name+"', '"+table+"', '"+champ_id+"', '"+champ_lib+"', '"+intitule+"', '"+label+"', '"+where+"');\">\r\n\t\t\t\t\t\t\t\t\t\t<\/center>\r\n\t\t\t\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t\t\t<\/table>\r\n\t\t\t\t\t\t<\/div>\r\n\t\t\t\t\t\t<div class='lib_alerte' id ='explorateur_alerte'  style='display:none' ><b>Veuillez saisir une donn&eacute;e<\/b><\/div>\r\n\t\t\t\t\t<\/td>\r\n\t\t\t\t<\/tr>\r\n\t\t\t<\/table>\r\n\t\t\t"; //tableau affichant la zone de saisie
	
	document.getElementById('explorateur').innerHTML = code; //modification du contenu de la div 'explorateur'
	document.getElementById('explorateur').style.display=''; //affichage de la div 'explorateur'
	
	var xtop = $('#'+lst_name).offset().top; //récuperation de la position top de la combobx d'appel
	if (xtop > 0) { //si la fonction position est cohérante
		document.getElementById('explorateur_new').style.top = xtop-235+"px"; //on positionne au niveau de la combobox d'appel la nouvelle fenetre 
		document.getElementById('explorateur_alerte').style.top = xtop-70+"px"; //on positionne correctement un message d'alerte en cas de probleme de saisie
	}
	
	document.getElementById('new_elt').focus(); //dès l'ouverture de la nouvelle fenetre, on met le focus sur la zone de texte pour eviter un clic supplémentaire
	
	changeOpac(20, form_name); //Diminution de l'opacité de la fenetre principale
}

/*
Procédure complexe permettant l'ajout dynamique de parcelles à un site ou à un acte
Arguments : 
	form_name : nom et id du formulaire utilisé comme élément de référence pour modifier l'opacité
	lst_name : name et id du menu déroulant à l'origine de la fonction
*/
function addParc_js(div, form_name){
	document.getElementById(div).style.display=''; //affichage de la div 'explorateur'
	var xtop = $('#main').offset().top;
	var h = document.getElementById('main').offsetHeight;
	if (xtop > 0) { //si la position top est cohérante
		document.getElementById('explorateur_new').style.height = h-24+"px"; //on adapte la hauteur de l'élément pour bloquer l'accès au reste du formulaire en arrière plan
		$('#explorateur_new').css('top', '0px');
		$('#parc_dispo').height(h-180);
		$('#parc_assoc_acte').height(h-180);
		$('#'+div).height($('#'+div).height()+95);
		if(document.getElementById('explorateur_alerte')) {
			document.getElementById('explorateur_alerte').style.top = xtop-70+"px"; //on positionne correctement un message d'alerte en cas de probleme de saisie
		}
	}
	changeOpac(20, form_name); //Diminution de l'opacité de la fenetre principale
}
/* 
Fonction utiliser pour les retour page précédante un peu plus efficace que le bouton du navigateur.
Il prend notamment en compte le dernier onglet actif de la page précédente
 */
function backPage(tabLien, root_path) {
	$.ajax({ //On entre maintenant dans la partie ajax
		url       : JS_ROOT_PATH+"includes/cen_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json", //format de la réponse permettant notamment un retour en variable tableau
		data      :{ 
			fonction:'fct_backPage', var_url: tabLien//variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
		},
		success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
			document.location.href=tabLien;
		}
	});
}

/*
Fonction appelée lors de la validation de la fenetre issue de la fonction addSpanValue_js
Cette fonction a pour objectif de controler la saisie faite precedement. Ce controle compare la saisie aux entrées déjà existantes et propose des choix similaires
Utilisation du triptique php/javascript/ajax : la fonction javascript appelle, via ajax, une fonction php qui renvoie du code permettant d'adapter la page en cours
Selon les cas cette fonction peut être appelée plusieurs fois avant la validation
Arguments : 
	origin : indique comment est appelé la fonction. Deux possibilité->init si elle est appelée pour la première fois ou confirmx où x fait référence au choix sélectionné si des correspondances ont été trouvées dans la base
	form_name : nom et id du formulaire utilisé comme élément de référence pour modifier l'opacité
	lst_name : name et id du menu déroulant à l'origine de la fonction
	table, champ_id et champ_lib : variables utilisées lors de l'appel de la fonction php pour faire la requête SQL de comparaison d'entrées existantes dans la base
	intitule, label : variables utilisées pour personnaliser la nouvelle fenêtre au cas par cas
	where : permet de passer un argument WHERE à la requête SQL côté php, NE PAS OUBLIER ->  , '') à la fin de l'appel de la fonction si inutile
*/
function ajaxCtrlSaisie(origin, form_name, lst_name, table, champ_id, champ_lib, intitule, label, where){
	if (origin == 'init') { //si la fonction est appelée pour la première fois
		value = document.getElementById('new_elt').value; //on récupère la valeur saisie dans la zone de texte
	} else if (origin.substring(0,7) == 'confirm') { //Si la fonction est appelée depuis la fenetre listant les choix possible déjà existant dans la base
		for (i=0; i<parseInt(origin.substring(7,8)); i++) { //On récupère le prefixe de la variable origin pour boucler sur chaque élément radio
			if (document.getElementById('new_elt'+i).checked) { //On teste si le radio est coché
				value = document.getElementById('new_elt'+i).value; //Si il est coché on récupère sa valeur
				break; //Et on sort de la boucle
			}
		}
	}
	if (value == '') {value = 'NULL';} //Si par le plus grand des hasard on récupère une valeur vide a ce niveau de la procédure on lui indique NULL
	$.ajax({ //On entre maintenant dans la partie ajax
		url       : JS_ROOT_PATH+"includes/cen_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json", //format de la réponse permettant notamment un retour en variable tableau
		data      :{ 
			fonction:'ctrl_saisie_simple_in_bd', check_origin: origin, params: {check_form: form_name, check_lst_name: lst_name, check_table: table, check_champ_id: champ_id, check_champ_lib: champ_lib, check_value: value, check_intitule: intitule, check_label: label, check_where: where} //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
		},
		success: function(data) { //On récupère de multiples infos groupées dans le tableau data 
			for (i=0; i<data['action'].length; i++) { //pour chaque ligne du tableau en retour
				div = data['div'][i]; //on identifie la div concernée
				action = data['action'][i]; //on identifie une action à effectuer
				code = data['code'][i]; //on récupère du code html à implanter dans la div identifiée plus haut
				if (action=='style') { //si l'action concerne l'affichage
					document.getElementById(div).style.display=code; //on fait apparaitre ou disparaitre la div
				} else if (action=='innerHTML') { //si l'action concerne la modification du code HTML de la div
					document.getElementById(div).innerHTML = code; //on change le code
				} else if (action=='changeOpac') { //si l'action concerne l'opacité du formulaire principale
					changeOpac(100, form_name); //on change l'opacité
				}
			}
		}
	});
}

function ajaxReloadElt(elt_id) {
	$.ajax({ //On entre maintenant dans la partie ajax
		url       : JS_ROOT_PATH+"includes/cen_fct_ajax.php", //variable permettant de savoir où se trouve la fonction php appelé plus loin
		type      : "post", //type de communication entre les différentes pages/fonctions/langages
		dataType : "json", //format de la réponse permettant notamment un retour en variable tableau
		data      :{ 
			fonction:'reload_elt' //variable passées à la page de fonction php on y retrouve les variables de la fonction javascript groupées dans une variable tableau params ainsi que la fonction a appeler dans la page cen_fct_ajax.php
		},
		success: function(data) { //On récupère du code dans la variable data
			if ($('#acte_notaire').length && data['lst_notaire'].length && data['lst_notaire'] != '') { 
				$('#acte_notaire').html(data['lst_notaire']); //la page est adaptée à partir de la variable 'data'
				$('#acte_notaire option').each(function(){ 
					if ($(this).val() == data['new_notaire']) {
						$(this).prop('selected', true);
					}
				});				
			}
		}
	});	
}

/* Permet de faire apparaitre ou masquer une div par un slide
Permet également et si besoin de modifier le nom et l'id d'elements de formulaire pour les rendre obligatoires ou pas
Pour cela on utilise le prefixe 'temp_' devant les noms et ids des éléments que l'on souhaite rendre obligatoires
Exemple d'utilisation : <a href="javascript:slidediv('ma_div')">Masquer/Disparaitre une div</a> */
function slidediv(div) {
	var myDiv  = document.getElementById(div); //déclaration de la div à faire apparaite/disparaitre
	var inputArr = myDiv.getElementsByTagName( "input" ); //déclaration d'une varaible pour tout les elements input du formulaire 
	var selectArr = myDiv.getElementsByTagName( "select" ); //déclaration d'une varaible pour tout les elements select du formulaire
	if ($('#'+div).is(":hidden")) { // si la div était cachée on l'affiche
		$('#'+div).slideDown(800); //methode slide d'apparation
		for (var i = 0; i < inputArr.length; i++) { //pour chaque élément de type input remplacer le suffixe temp par need pour le rendre obligatoire
			inputArr[i].name = inputArr[i].name.replace('temp_', 'need_'); //pour le name
			inputArr[i].id = inputArr[i].id.replace('temp_', 'need_'); //pour l'id
		}
		for (var i = 0; i < selectArr.length; i++) { //pour chaque élément de type select remplacer le suffixe temp par need pour le rendre obligatoire
			selectArr[i].name = selectArr[i].name.replace('temp_', 'need_'); //pour le name
			selectArr[i].id = selectArr[i].id.replace('temp_', 'need_'); //pour l'id
		}
	} else { // si la div était déjà ouverte, on la cache
		$('#'+div).slideUp(800); //methode slide de disparition
		for (var i = 0; i < inputArr.length; i++) { //pour chaque élément de type input remplacer le suffixe need par temp pour le rendre facultatif
			inputArr[i].name = inputArr[i].name.replace('need_', 'temp_'); //pour le name
			inputArr[i].id = inputArr[i].id.replace('need_', 'temp_'); //pour l'id
		}
		for (var i = 0; i < selectArr.length; i++) { //pour chaque élément de type select remplacer le suffixe need par temp pour le rendre facultatif
			selectArr[i].name = selectArr[i].name.replace('need_', 'temp_'); //pour le name
			selectArr[i].id = selectArr[i].id.replace('need_', 'temp_'); //pour l'id
		}
	}
}	

//Fonction permettant de générer un mot de passe aléatoire
function generatePwd(id) {
	var chars=new Array( "abcdefghijklmnopqrstuvwxyz".split(''),"abcdefghijklmnopqrstuvwxyz".toUpperCase().split(''),"0123456789".split(''),"!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~".split(''));
	var level=3
	var long=10	 
	var rep=new Array();
	myrand=new Date().getMilliseconds();
	 
	while(level>1){ 
		templong=Math.ceil(Math.random(myrand)*(long-(--level))) ;
		rep.push(templong);
		long=long-templong;
	}
	rep.push(long)
	var password=new Array();
	var i=-1;
	while(rep[++i]){
		BaseL=chars[i].length
		j=-1;
		while((j++<rep[i]-1) && (password.push (chars[i][Math.floor(Math.random(Math.pow(new Date().getMilliseconds(),3))*BaseL)]))){}
	}
	document.getElementById(id).value=password.sort(sortRand).reverse().sort(sortRand).sort(sortRand).reverse().join('')
}
function sortRand(){return (Math.round(Math.random(myrand))-0.5); }

/* Cette fonction est appelée par les fonction ajax en fin de page
Elle permet de remplir automatiquement les listes déroulantes en fonction du contenu du tableau
Son principe est de récupérer toutes les valeurs par colonne dans une variable tableau puis de la simplifiée pour avoir des valeurs uniques
Puis chaque valeur unique est insérée dans la liste déroulante
------------Les variables
------------tbody -> id du tbody contenant le tableau 
------------frm -> id du formulaire contenant les éléments de filtrage */
function GetComboBoxFilter (tbody, frm, tag) {
	var elements = document.forms[frm].elements; //variable tableau stockant les éléménts du formulaire
	for (k=0; k<elements.length; k++){ //pour chaque élément du formulaire
		if (elements[k].type == 'select-one' && elements[k].id.substring(0,6) == 'filtre') { //Si il s'agit d'une liste déroulante dont l'id commence par 'filtre'
			var first_option = document.getElementById(elements[k].id).options[0].text;//.toUpperCase(); //variable stockant la première 
			cmb_statut = ColUniqueValue(tbody, elements[k].id, tag);
			cmb_option = "<option value='-1'>"+first_option+"</option>\n";
			for(var i=0; i<cmb_statut.length; i++) {
				cmb_option += "<option ";
				cmb_option += "value='"+cmb_statut[i]+"'>"+cmb_statut[i]+"</option>\n";
			}	
			document.getElementById(elements[k].id).innerHTML = cmb_option;
		} 
	}
}

function ColUniqueValue(tbody, cmb_id, tag) {
	var tab_col_content = [];
	$('#'+ tbody).find('.'+cmb_id).find(tag).each(function(){
		cell_contenu = $(this).text();
		if (tab_col_content.indexOf(cell_contenu) == -1 && cell_contenu) {
			tab_col_content.push(cell_contenu);
		}
	});
	tab_col_content.sort();
	return tab_col_content;	
}

