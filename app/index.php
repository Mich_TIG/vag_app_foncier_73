<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('includes/configuration.php');
	include ('includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/home.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>		
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<TITLE>Accueil - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Vos outils dédiés test';
			include('includes/header.php');
			include('includes/breadcrumb.php');
			include('includes/valide_acces.php');		
			include('includes/cen_fct.php');	// intégration d'une page contenant des fonctions utiles au bon déroulement de la procédure
		?>
		
		<section id='main'>
			<h3>Bienvenue</h3>
			<div id='onglet' class='list'>
				<ul>
					<li>
						<a href='#general'><span>Général</span></a>
					</li>
					<li>
						<a href='#foncier'><span><?php echo utf8_encode('Foncier'); ?></span></a>
					</li>
					<li>
						<a href='#sig'><span><?php echo utf8_encode('Sig'); ?></span></a>
					</li>
				</ul>
				
				<div id='general'>
					<table class='tbl_li' width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td style='width:100%;'>
								<center>
								<table CELLSPACING = '0' CELLPADDING ='10' style='border:0px solid black'>
									<tr>										
										<td>
											<a href="<?php echo ROOT_PATH; ?>contacts/contacts_liste_all.php"><IMG src="images/contact.png" border=0 width=161 onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Acc&egrave;s aux contacts"></a>
										</td>
									</tr>
								</center>
							</td>
									</tr>
								</table>
								</center>
							</td>
							<td style='vertical-align:top'>
								<fieldset class='fieldset_alert'>
									<legend>Vos Alertes</legend>
									<?php 
										try
										{
											$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
										}
										catch (Exception $e)
										{
											die(include('includes/prob_tech2.php'));
										}
									
										If (role_access(array('grp_foncier', 'grp_sig')) == 'OUI') {
									?>										
										<fieldset>
											<legend>Foncier</legend>
											<?php
												include ('alertes/alertes_grp_foncier.php');
											?>
										</fieldset>
									<?php
										}
										If (role_access(array('grp_af')) == 'OUI') {
									?>										
										<fieldset>
											<legend>Animation foncière</legend>
											<?php
												include ('alertes/alertes_grp_af.php');
											?>
										</fieldset>
									<?php
										}
									?>										
								</fieldset>
							</td>
						</tr>
					</table>
				</div id='general'>
				
				<div id='foncier'>
					<table class='tbl_li' width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td style='height:200px;'>
								<center>
								<table CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
									<tr>
										<td style='height:200px; vertical-align:top;'>
											<div id="crossfade" style='width:150px;'>
												<img width='150px' class="bottom" src="images/bdfoncier_sel.png"/>
												<a href="<?php echo ROOT_PATH; ?>foncier/accueil_foncier.php">
													<img width='150px' class="top" src="images/bdfoncier.png">
												</a>
											</div>
										</td>	
										<td style='height:200px; vertical-align:top;'>
											<div id="crossfade" style='width:100px;'>
												<img width='100px' class="bottom" src="foncier/images/export_base.png"/>
												<a href="<?php echo ROOT_PATH; ?>foncier/export_base.php">
													<img width='100px' class="top" src="foncier/images/export_base.png">
												</a>
											</div>
										</td>		
									</tr>
								</table>
								</center>
							</td>
						</tr>
					</table>
				</div id='foncier'>
				
				<div id='sig'>
					<table class='tbl_li' width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td>
								<center>
								<table CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
									<tr>
										<td>
											<div id="crossfade" style='width:140px;'>
												<img style='top:-70px;' width='140px' class="bottom" src="images/plugins_QGis_sel.png"/>
												<a href="<?php echo ROOT_PATH; ?>sig/guide/outils_qgis.php#plugins">
													<img style='top:-70px;' width='140px' class="top" src="images/plugins_QGis.png">
												</a>
											</div>
										</td>	
										<td>
											<a href="http://www.qgis.org/fr/docs/index.html#214" target="_blank" onMouseOver="document.img_doc_qgis.src='images/documentation_qgis_sel.png';" onMouseOut="document.img_doc_qgis.src='images/documentation_qgis.png';">
											<img name="img_doc_qgis" src="images/documentation_qgis.png" border=0 width=200 alt="Documentation officielle QGis"> </a>
										</td>							
										<?php If (role_access(array('grp_sig')) == 'OUI') { ?>										
										<td>
											<div style='width:100px;height:100px'>
												<a href="<?php echo ROOT_PATH; ?>sig/admin/creat_user.php"><img height='100' src="images/creat_user.png" href="" /></a>
											</div>
										</td>
										<?php } ?>
									</tr>
								</table>
								</center>
							</td>
						</tr>
					</table>
				</div id='sig'>
			</div>
		</section>
		<?php include("includes/footer.php"); ?>
	</body>
</html>
	
