<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<TITLE>Vos alertes - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Vos alertes';
			include('../includes/header.php');
			include('../includes/breadcrumb.php');
			include ('../includes/valide_acces.php');	
			try
			{
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e)
			{
				die(include('../includes/prob_tech.php'));
			}
			
		?>
		<section id='main'>
			<h3>Vos alertes en détail</h3>
			<div id='onglet' class='list'>
				<ul>
					<li>
						<a href='#general'><span>Général</span></a>
					</li>
					<?php
						If (role_access(array('grp_sig', 'grp_foncier')) == 'OUI') {
							echo "<li>";
						} Else {
							echo "<li style='pointer-events:none; opacity:0.6;'>";
						}
						echo "<a href='#foncier'><span>". utf8_encode('Foncier') ."</span></a></li>";
					?>
					<?php
						If (role_access(array('grp_animation_fonciere')) == 'OUI') {
							echo "<li>";
						} Else {
							echo "<li style='pointer-events:none; opacity:0.6;'>";
						}
						echo "<a href='#animation_fonciere'><span>". utf8_encode('Animation fonci&egrave;re') ."</span></a></li>";
					?>
				</ul>
				
				<div id='general'>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black'>
						<tr>
							<td style='vertical-align:top'>
								
								<?php
									$rq_nbr_site_sans_mfu = "SELECT count(distinct site_id) as nbr_total, count(distinct geom_mfu) as nbr_sans_mfu FROM sites.sites WHERE left(typsite_id,1) = '1'";
									$res_nbr_site_sans_mfu = $bdd->prepare($rq_nbr_site_sans_mfu);
									$res_nbr_site_sans_mfu->execute();
									$nbr_site_sans_mfu = $res_nbr_site_sans_mfu->fetch();
									
									$rq_nbr_site_prob_mfu = "SELECT count(distinct t1.site_id) as nbr_total FROM (SELECT DISTINCT site_id FROM alertes.v_prob_mfu_baddata UNION SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen) AS t1 WHERE t1.site_id IS NOT NULL AND t1.site_id != ''";
									$res_nbr_site_prob_mfu = $bdd->prepare($rq_nbr_site_prob_mfu);
									$res_nbr_site_prob_mfu->execute();
									$nbr_site_prob_mfu = $res_nbr_site_prob_mfu->fetch();
									
									$rq_nbr_site2_prob_mfu = "SELECT count (distinct v_sites_parcelles.site_id) as nb FROM sites.v_sites_parcelles JOIN sites.sites USING (site_id) WHERE (sites.typsite_id::text IN ('2-1'::character varying::text, '2-2'::character varying::text)) AND v_sites_parcelles.mfu NOT IN ('Autre'::text, 'Pas de maîtrise'::text, 'Projet en cours'::text, 'Ces données ne sont pas à jour'::text)";
									$res_nbr_site2_prob_mfu = $bdd->prepare($rq_nbr_site2_prob_mfu);
									$res_nbr_site2_prob_mfu->execute();
									$nbr_site2_prob_mfu = $res_nbr_site2_prob_mfu->fetch();
								?>
								<table CELLSPACING = '5' CELLPADDING ='5' style="background-color: transparent !important; border-radius: 0 !important; border-width: 0 !important; display: table !important; float: left; white-space:nowrap; padding:0 !important;">
									<tr>
										<td>
											<label class="label">
												<u>Foncier :</u>
											</label>
											<label class="label" style='font-weight:normal;'>
												<?php
													If (($nbr_site_sans_mfu[0] - $nbr_site_sans_mfu[1]) == 1) {
														echo "il y a <b>" . ($nbr_site_sans_mfu[0] - $nbr_site_sans_mfu[1]) . "</b> site de type 1 sans maîtrise foncière ou d'usage";
													} Else If (($nbr_site_sans_mfu[0] - $nbr_site_sans_mfu[1]) > 1) {
														echo "il y a <b>" . ($nbr_site_sans_mfu[0] - $nbr_site_sans_mfu[1]) . "</b> sites de type 1 sans maîtrise foncière ou d'usage";
													}
												?>
											</label>
											<br />
											<label class="label" style='font-weight:normal; position:relative; left:68px;'>
												<?php
													If ($nbr_site_prob_mfu[0] <= 1) {
														echo "il y a <b>" . $nbr_site_prob_mfu[0] . "</b> site de type 1 présentant un problème de saisie";
													} Else If ($nbr_site_prob_mfu[0] > 1) {
														echo "il y a <b>" . $nbr_site_prob_mfu[0] . "</b> sites de type 1 présentant un problème de saisie";
													}
												?> 
											</label>
											<br />
											<label class="label" style='font-weight:normal; position:relative; left:68px;'>
												<?php
													If ($nbr_site2_prob_mfu[0] <= 1) {
														echo "il y a <b>" . $nbr_site2_prob_mfu[0] . "</b> site de type 2 avec une maîtrise foncière ou d'usage";
													} Else If ($nbr_site_prob_mfu[0] > 1) {
														echo "il y a <b>" . $nbr_site2_prob_mfu[0] . "</b> sites de type 2 avec une maîtrise foncière ou d'usage";
													}
												?> 
											</label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				
				<div id='foncier'>
				<?php
					If (role_access(array('grp_sig', 'grp_foncier')) == 'OUI') {
				?>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black; border-radius: 5px 5px 0 0;'>
						<tr>
							<td>
								<center>
									<div id='layer_codex'>
										<label class='label'>
											Voici les alertes dédectées sur les données foncières
										</label>
									</div>
									<p>&nbsp;</p>
									<label class='label' style='font-weight:normal;display:flex;margin-left:25px;'>
										Les données peuvent être mises à jour <i>&nbsp;via&nbsp;</i> les liens cliquables ci-dessous ou <i>&nbsp;via&nbsp;</i> QGis, sur les tables PostgreSQL indiquées.
									</label>
								</center>
								
								<?php
									$rq_sites_prob_cadastre_cen = "SELECT DISTINCT site_id FROM alertes.v_prob_cadastre_cen ORDER BY site_id";
									$res_sites_prob_cadastre_cen = $bdd->prepare($rq_sites_prob_cadastre_cen);
									$res_sites_prob_cadastre_cen->execute();
									$sites_prob_cadastre_cen = $res_sites_prob_cadastre_cen->fetchall();								
									
									If ($res_sites_prob_cadastre_cen->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										<tr>
											<td>
												<label class='label' style='color:#fe0000;margin-left:50px;'>Actes dont le cadastre pose problème</label>
											</td>					
										</tr>
									<?php
										For ($s=0; $s<$res_sites_prob_cadastre_cen->rowCount(); $s++) { 
									?>
										<tr>
											<td>
												<label class='label' style='margin-left:50px;'><?php If ($sites_prob_cadastre_cen[$s]['site_id'] != '') {echo $sites_prob_cadastre_cen[$s]['site_id'];} Else {echo 'N.D.';} ?></label>
												<img id='btn_detail_prob_cadastre_<?php echo $sites_prob_cadastre_cen[$s]['site_id']; ?>' src='<?php echo ROOT_PATH; ?>images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick="displayPlusMoins(this, 'td_detail_','prob_cadastre_', '<?php echo $sites_prob_cadastre_cen[$s]['site_id']; ?>');">
											</td>
										</tr>
										<tr>
											<td id='td_detail_prob_cadastre_<?php echo $sites_prob_cadastre_cen[$s]['site_id']; ?>' style='display:none;'>
												<table style='border:0px solid black;'>														
											<?php
												$rq_prob_cadastre_cen = "SELECT distinct site_id, mfu_id, acte, par_id, lot_id, cadcen_motif_fin as motif FROM alertes.v_prob_cadastre_cen WHERE site_id = '".$sites_prob_cadastre_cen[$s]['site_id']."' ORDER BY site_id, mfu_id";
												$res_prob_cadastre_cen = $bdd->prepare($rq_prob_cadastre_cen);
												$res_prob_cadastre_cen->execute();
												$prob_cadastre_cen = $res_prob_cadastre_cen->fetchall();
												For ($i=0; $i<$res_prob_cadastre_cen->rowCount(); $i++) { 
													If (role_access(array('grp_foncier')) == 'OUI') {
														$vget = "type_acte:".$prob_cadastre_cen[$i]['acte'].";mode:edit;acte_id:".$prob_cadastre_cen[$i]['mfu_id'].";";
														$color = '#fe0000';
														$label = "Corriger...";
													} Else {	
														$vget = "type_acte:".$prob_cadastre_cen[$i]['acte'].";mode:consult;acte_id:".$prob_cadastre_cen[$i]['mfu_id'].";";
														$color = '#004494';
														$label = "Consulter la fiche...";
													}
													$vget_crypt = base64_encode($vget);
													$lien = ROOT_PATH . "foncier/fiche_mfu.php?d=$vget_crypt#general";
											?>	
													<tr>
														<td>
															<label class='label' style='font-weight:normal;margin-left:50px;' >
																L'acte <b><?php echo $prob_cadastre_cen[$i]['mfu_id']; ?></b> contient <?php If ($prob_cadastre_cen[$i]['par_id'] != $prob_cadastre_cen[$i]['lot_id']) {echo 'un lot ';} Else {echo 'une parcelle ';} If ($prob_cadastre_cen[$i]['par_id'] != $prob_cadastre_cen[$i]['lot_id'] && $prob_cadastre_cen[$i]['motif'] == 1) {echo 'supprimé';} Else If ($prob_cadastre_cen[$i]['par_id'] == $prob_cadastre_cen[$i]['lot_id'] && $prob_cadastre_cen[$i]['motif'] == 1) {echo 'supprimée';} Else If ($prob_cadastre_cen[$i]['motif'] == 2) {echo 'dont le propriétaire a changé';}?>
															</label>
															<a style='color:<?php echo $color;?>;cursor:pointer;font-size:10pt;font-weight:bold' href='<?php echo $lien; ?>'><?php echo $label; ?></a>
														</td>
													</tr>
											<?php 
												} 
											?>
												</table>
											</td>
										</tr>
									<?php  
										}
									?>
									</table>
								</center>
								<?php  
									}
								
									$rq_sites_prob_baddata = "SELECT DISTINCT RIGHT(LEFT(mfu_id, 7), 4) as site_id FROM alertes.v_prob_mfu_baddata ORDER BY site_id";
									$res_sites_prob_baddata = $bdd->prepare($rq_sites_prob_baddata);
									$res_sites_prob_baddata->execute();
									$sites_prob_baddata = $res_sites_prob_baddata->fetchall();								
									
									If ($res_sites_prob_baddata->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										<tr>
											<td>
												<label class='label' style='color:#fe0000;margin-left:50px;'>Actes dont la saisie pose problème</label>
											</td>					
										</tr>
									<?php
										For ($s=0; $s<$res_sites_prob_baddata->rowCount(); $s++) { 
									?>
										<tr>
											<td>
												<label class='label' style='margin-left:50px;'><?php If ($sites_prob_baddata[$s]['site_id'] != '') {echo $sites_prob_baddata[$s]['site_id'];} Else {echo 'N.D.';} ?></label>
												<img id='btn_detail_prob_saisie_<?php echo $sites_prob_baddata[$s]['site_id']; ?>' src='<?php echo ROOT_PATH; ?>images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick="displayPlusMoins(this, 'td_detail_','prob_saisie_', '<?php echo $sites_prob_baddata[$s]['site_id']; ?>');">
											</td>
										</tr>
										<tr>
											<td id='td_detail_prob_saisie_<?php echo $sites_prob_baddata[$s]['site_id']; ?>' style='display:none;'>
												<table style='border:0px solid black;'>														
											<?php
												$rq_prob_baddata = "SELECT distinct mfu_id, acte, chp_baddata FROM alertes.v_prob_mfu_baddata WHERE mfu_id ILIKE '%".$sites_prob_baddata[$s]['site_id']."%' ORDER BY mfu_id";
												$res_prob_baddata = $bdd->prepare($rq_prob_baddata);
												$res_prob_baddata->execute();
												$prob_baddata = $res_prob_baddata->fetchall();
												For ($i=0; $i<$res_prob_baddata->rowCount(); $i++) { 
													If (role_access(array('grp_foncier')) == 'OUI') {
														$vget = "type_acte:".$prob_baddata[$i]['acte'].";mode:edit;acte_id:".$prob_baddata[$i]['mfu_id'].";";
														$color = '#fe0000';
														$label = "Corriger...";
													} Else {	
														$vget = "type_acte:".$prob_baddata[$i]['acte'].";mode:consult;acte_id:".$prob_baddata[$i]['mfu_id'].";";
														$color = '#004494';
														$label = "Consulter la fiche...";
													}
													$vget_crypt = base64_encode($vget);
													$lien = ROOT_PATH . "foncier/fiche_mfu.php?d=$vget_crypt#general";
											?>	
													<tr>
														<td>
															<label class='label' style='font-weight:normal;margin-left:50px;'>
																<?php If ($prob_baddata[$i]['acte'] == 'Acquisition') {echo "L'acquisition ";} Else {echo 'La convention ';} ?> <b> <?php echo $prob_baddata[$i]['mfu_id']?></b> contient <?php If(substr_count($prob_baddata[$i]['chp_baddata'], ";") == 0) {echo 'une erreur de saisie';} Else {echo 'des erreurs de saisie';} ?>
															</label>
															<a style='color:<?php echo $color;?>;cursor:pointer;font-size:10pt;font-weight:bold' href='<?php echo $lien; ?>'><?php echo $label; ?></a>
														</td>
													</tr>
											<?php 
												} 
											?>
												</table>
											</td>
										</tr>
									<?php	
										}
									?>
									</table>
								</center>
								<?php 
									} 
								
									$rq_sites_prob_indiv = "SELECT DISTINCT RIGHT(LEFT(mfu_id, 7), 4) as site_id FROM alertes.v_prob_mfu_indiv ORDER BY site_id";
									$res_sites_prob_indiv = $bdd->prepare($rq_sites_prob_indiv);
									$res_sites_prob_indiv->execute();
									$sites_prob_indiv = $res_sites_prob_indiv->fetchall();								
									
									If ($res_sites_prob_indiv->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										<tr>
											<td>
												<label class='label' style='color:#d77b0e;margin-left:50px;'>Actes dont les montants posent problème</label>
											</td>					
										</tr>
									<?php
										For ($s=0; $s<$res_sites_prob_indiv->rowCount(); $s++) { 
									?>
										<tr>
											<td>
												<label class='label' style='margin-left:50px;'><?php If ($sites_prob_indiv[$s]['site_id'] != '') {echo $sites_prob_indiv[$s]['site_id'];} Else {echo 'N.D.';} ?></label>
												<img id='btn_detail_prob_indiv_<?php echo $sites_prob_indiv[$s]['site_id']; ?>' src='<?php echo ROOT_PATH; ?>images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick="displayPlusMoins(this, 'td_detail_','prob_indiv_', '<?php echo $sites_prob_indiv[$s]['site_id']; ?>');">
											</td>
										</tr>
										<tr>
											<td id='td_detail_prob_indiv_<?php echo $sites_prob_indiv[$s]['site_id']; ?>' style='display:none;'>
												<table style='border:0px solid black;'>														
											<?php
												$rq_prob_indiv = "SELECT distinct mfu_id, acte, statutmfu_lib FROM alertes.v_prob_mfu_indiv WHERE mfu_id ILIKE '%".$sites_prob_indiv[$s]['site_id']."%' ORDER BY mfu_id";
												$res_prob_indiv = $bdd->prepare($rq_prob_indiv);
												$res_prob_indiv->execute();
												$prob_indiv = $res_prob_indiv->fetchall();
												For ($i=0; $i<$res_prob_indiv->rowCount(); $i++) { 
													If (role_access(array('grp_foncier')) == 'OUI') {
														$vget = "type_acte:".$prob_indiv[$i]['acte'].";mode:edit;acte_id:".$prob_indiv[$i]['mfu_id'].";";
														$color = '#fe0000';
														$label = "Corriger...";
													} Else {	
														$vget = "type_acte:".$prob_indiv[$i]['acte'].";mode:consult;acte_id:".$prob_indiv[$i]['mfu_id'].";";
														$color = '#004494';
														$label = "Consulter la fiche...";
													}
													$vget_crypt = base64_encode($vget);
													$lien = ROOT_PATH . "foncier/fiche_mfu.php?d=$vget_crypt#general";
											?>	
													<tr>
														<td>
															<label class='label' style='font-weight:normal;margin-left:50px;'>
																L'acte <b> <?php echo $prob_indiv[$i]['mfu_id']?></b> (<i><?php echo $prob_indiv[$i]['statutmfu_lib']; ?></i>) contient une erreur
															</label>
															<a style='color:<?php echo $color;?>;cursor:pointer;font-size:10pt;font-weight:bold' href='<?php echo $lien; ?>'><?php echo $label; ?></a>
														</td>
													</tr>
											<?php 
												} 
											?>
												</table>
											</td>
										</tr>
									<?php	
										}
									?>
									</table>
								</center>
								<?php 
									} 
								
									$rq_sites_prob_sansparc = "SELECT DISTINCT translate(RIGHT(mfu_id, length(mfu_id)-3), '0123456789', '') as site_id FROM alertes.v_prob_mfu_sansparc ORDER BY site_id";
									$res_sites_prob_sansparc = $bdd->prepare($rq_sites_prob_sansparc);
									$res_sites_prob_sansparc->execute();
									$sites_prob_sansparc = $res_sites_prob_sansparc->fetchall();								
									
									If ($res_sites_prob_sansparc->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										<tr>
											<td>
												<label class='label' style='color:#fe0000;margin-left:50px;'>Actes sans parcelles associées</label>
											</td>					
										</tr>
									<?php
										For ($s=0; $s<$res_sites_prob_sansparc->rowCount(); $s++) {
									?>
										<tr>
											<td>
												<label class='label' style='margin-left:50px;'><?php If ($sites_prob_sansparc[$s]['site_id'] != '') {echo $sites_prob_sansparc[$s]['site_id'];} Else {echo 'N.D.';} ?></label>
												<img id='btn_detail_prob_sansparc_<?php echo $sites_prob_sansparc[$s]['site_id']; ?>' src='<?php echo ROOT_PATH; ?>images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick="displayPlusMoins(this, 'td_detail_','prob_sansparc_', '<?php echo $sites_prob_sansparc[$s]['site_id']; ?>');">
											</td>
										</tr>
										<tr>
											<td id='td_detail_prob_sansparc_<?php echo $sites_prob_sansparc[$s]['site_id']; ?>' style='display:none;'>
												<table style='border:0px solid black;'>
												<?php
													$rq_prob_sansparc = "SELECT DISTINCT mfu_id, acte FROM alertes.v_prob_mfu_sansparc WHERE mfu_id ILIKE '%".str_replace("\r\n", "%", chunk_split($sites_prob_sansparc[$s]['site_id'], 1))."%' ORDER BY mfu_id";
													$res_prob_sansparc = $bdd->prepare($rq_prob_sansparc);
													$res_prob_sansparc->execute();
													$prob_sansparc = $res_prob_sansparc->fetchall();
													For ($i=0; $i<$res_prob_sansparc->rowCount(); $i++) { 
														If (role_access(array('grp_foncier')) == 'OUI') {
															$vget = "type_acte:".$prob_sansparc[$i]['acte'].";mode:edit;acte_id:".$prob_sansparc[$i]['mfu_id'].";";
															$color = '#fe0000';
															$label = "Corriger...";
														} Else {
															$vget = "type_acte:".$prob_sansparc[$i]['acte'].";mode:consult;acte_id:".$prob_sansparc[$i]['mfu_id'].";";
															$color = '#004494';
															$label = "Consulter la fiche...";
														}
														$vget_crypt = base64_encode($vget);
														$lien = ROOT_PATH . "foncier/fiche_mfu.php?d=$vget_crypt#general";
												?>	
													<tr>
														<td>
															<label class='label' style='font-weight:normal;margin-left:50px;'>
																<?php If ($prob_sansparc[$i]['acte'] == 'Acquisition') {echo "L'acquisition ";} Else {echo 'La convention ';} ?> <b><?php echo $prob_sansparc[$i]['mfu_id']?></b> n'a pas de parcelle associée
															</label>
															<a style='color:<?php echo $color;?>;cursor:pointer;font-size:10pt;font-weight:bold' href='<?php echo $lien; ?>'><?php echo $label; ?></a>
														</td>
													</tr>
												<?php 
													}
												?>
												</table>
											</td>
										</tr>
										<?php
										}
										?>
									</table>
								</center>
								<?php 
									}
								?>
							</td>
						</tr>
					</table>			
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black; border-radius: 0 0 5px 5px;'>
						<tr>
							<td>
								<?php
									$rq_alerte_termine = "SELECT distinct mu_id, acte FROM alertes.v_alertes_mu WHERE alerte = 'terminée' ORDER BY mu_id";
									$res_alerte_termine = $bdd->prepare($rq_alerte_termine);
									$res_alerte_termine->execute();
									$alerte_termine = $res_alerte_termine->fetchall();
									If ($res_alerte_termine->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										<tr>
											<td>
												<label class='label' style='color:#d77b0e;margin-left:50px;'>Actes dont l'échéance est dépassée</label>
											</td>					
										</tr>
									<?php
										For ($i=0; $i<$res_alerte_termine->rowCount(); $i++) { 
											If (role_access(array('grp_foncier')) == 'OUI') {
												$vget = "type_acte:".$alerte_termine[$i]['acte'].";mode:edit;acte_id:".$alerte_termine[$i]['mu_id'].";";
												$color = '#d77b0e';
												$label = "Mettre à jour...";
											} Else {
												$vget = "type_acte:".$alerte_termine[$i]['acte'].";mode:consult;acte_id:".$alerte_termine[$i]['mu_id'].";";
												$color = '#004494';
												$label = "Consulter la fiche...";
											}
											$vget_crypt = base64_encode($vget);
											$lien = ROOT_PATH . "foncier/fiche_mfu.php?d=$vget_crypt#general";
									?>	
										<tr>
											<td>
												<label class='label' style='font-weight:normal;margin-left:50px;'>
													<?php If ($alerte_termine[$i]['acte'] == 'Acquisition') {echo "L'acquisition ";} Else {echo 'La convention ';} ?> <b><?php echo $alerte_termine[$i]['mu_id']?></b> est terminée
												</label>
												<a style='color:<?php echo $color;?>;cursor:pointer;font-size:10pt;font-weight:bold' href='<?php echo $lien; ?>'><?php echo $label; ?></a>
											</td>
										</tr>
									<?php 
										}
									?>
									</table>
								</center>
								<?php 
									}
								
									$rq_alerte_echeance = "SELECT distinct mu_id, acte, alerte FROM alertes.v_alertes_mu WHERE alerte != 'terminée' ORDER BY mu_id";
									$res_alerte_echeance = $bdd->prepare($rq_alerte_echeance);
									$res_alerte_echeance->execute();
									$alerte_echeance = $res_alerte_echeance->fetchall();
									If ($res_alerte_echeance->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										<tr>
											<td>
												<label class='label' style='color:#004494;margin-left:50px;'>Actes arrivant à échéance</label>
											</td>					
										</tr>
									<?php
											For ($i=0; $i<$res_alerte_echeance->rowCount(); $i++) { 
											$vget = "type_acte:".$alerte_echeance[$i]['acte'].";mode:consult;acte_id:".$alerte_echeance[$i]['mu_id'].";";
											$vget_crypt = base64_encode($vget);
											$lien = ROOT_PATH . "foncier/fiche_mfu.php?d=$vget_crypt#general";
									?>	
										<tr>
											<td>
												<label class='label' style='font-weight:normal;margin-left:50px;'>
													<?php If ($alerte_echeance[$i]['acte'] == 'Acquisition') {echo "L'acquisition ";} Else {echo 'La convention ';} ?> <b><?php echo $alerte_echeance[$i]['mu_id']?></b> verra sa <?php echo $alerte_echeance[$i]['alerte']; ?>
												</label>
												<a style='color:#004494;cursor:pointer;font-size:10pt;font-weight:bold' href='<?php echo $lien; ?>'>Consulter la fiche...</a>
											</td>
										</tr>
									<?php 
										}
									?>
									</table>
								</center>
								<?php 
									}
								
									$rq_alerte_decoup = "select distinct cad_site_id, mu_id FROM foncier.r_cad_site_mu WHERE surf_mu is not null AND geom is null";
									$res_alerte_decoup = $bdd->prepare($rq_alerte_decoup);
									$res_alerte_decoup->execute();
									$alerte_decoup = $res_alerte_decoup->fetchall();
									If ($res_alerte_decoup->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										<tr>
											<td>
												<label class='label' style='color:#d77b0e;margin-left:50px;'>Parcelles à découper</label>
											</td>					
										</tr>
										<tr>
											<td>
												<label class='label' style='font-weight:normal;margin-left:50px;'>
													Les parcelles doivent être découpées dans QGis, directement sur la table PostgreSQL nommée parcelles_conventions
												</label>
											</td>
										</tr>
									<?php 
										} 
									?>
									</table>
								</center>
							</td>
						</tr>
					</table>
				<?php
					} Else { echo '&nbsp;'; }
				?>
				</div>
				
				<div id='animation_fonciere'>
				<?php
					If (role_access(array('grp_af')) == 'OUI') {
				?>
					<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black; border-radius: 5px 5px 0 0;'>
						<tr>
							<td>
								<center>
									<div id='layer_codex'>
										<label class='label'>
											Voici les alertes détectées sur les animations en cours
										</label>
									</div>									
								</center>								
								<?php
									$rq_saf_prob_cadastre = "
									SELECT DISTINCT
										session_af_id,
										session_af_lib
									FROM			
										cadastre.cadastre_cen t3
										JOIN foncier.cadastre_site t4 USING (cad_cen_id)
										JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
										JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)	
										JOIN animation_fonciere.session_af USING (session_af_id)
									WHERE 
										(rel_saf_foncier.paf_id != 0 OR rel_saf_foncier.objectif_af_id != 3) AND  
										(t3.date_fin != 'N.D.' OR t4.date_fin != 'N.D.')";
									$res_saf_prob_cadastre= $bdd->prepare($rq_saf_prob_cadastre);
									$res_saf_prob_cadastre->execute();
									$saf_prob_cadastre = $res_saf_prob_cadastre->fetchall();								
									
									If ($res_saf_prob_cadastre->rowCount() > 0) {
								?>
								<center>
									<table CELLSPACING = '1' CELLPADDING ='3' style='border:0px solid black'>
										
									<?php
										For ($s=0; $s<$res_saf_prob_cadastre->rowCount(); $s++) { 
										
											$vget_af = "mode:consult;session_af_id:".$saf_prob_cadastre[$s]['session_af_id'].";session_af_lib:".$saf_prob_cadastre[$s]['session_af_lib'].";";
											$vget_crypt_af = base64_encode($vget_af);
											$lien_af = ROOT_PATH . "foncier/fiche_saf.php?d=$vget_crypt_af#general";
											
											$rq_nbr_parcelles_supprimees = "
											SELECT count(*) as nbr
											FROM (
												SELECT DISTINCT
													cad_site_id,
													session_af_id
												FROM			
													cadastre.cadastre_cen t3
													JOIN foncier.cadastre_site t4 USING (cad_cen_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
													JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)				
												WHERE 
													(rel_saf_foncier.paf_id != 0 OR rel_saf_foncier.objectif_af_id != 3) AND  
													t3.date_fin != 'N.D.' AND 
													t3.motif_fin_id = 1 AND
													t4.date_fin = 'N.D.' AND
													session_af_id = :session_af_id
												) as t1";
											$res_nbr_parcelles_supprimees = $bdd->prepare($rq_nbr_parcelles_supprimees);
											$res_nbr_parcelles_supprimees->execute(array('session_af_id'=>$saf_prob_cadastre[$s]['session_af_id']));
											$nbr_parcelles_supprimees = $res_nbr_parcelles_supprimees->fetch();											
											
											$rq_nbr_proprios_supprimes = "
											SELECT count(*) as nbr
											FROM (
												SELECT DISTINCT
													cad_site_id,
													session_af_id
												FROM			
													cadastre.cadastre_cen t3
													JOIN foncier.cadastre_site t4 USING (cad_cen_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
													JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)				
												WHERE 
													(rel_saf_foncier.paf_id != 0 OR rel_saf_foncier.objectif_af_id != 3) AND  
													t3.date_fin != 'N.D.' AND 
													t3.motif_fin_id = 2 AND
													t4.date_fin = 'N.D.' AND
													session_af_id = :session_af_id
												) as t1";
											$res_nbr_proprios_supprimes = $bdd->prepare($rq_nbr_proprios_supprimes);
											$res_nbr_proprios_supprimes->execute(array('session_af_id'=>$saf_prob_cadastre[$s]['session_af_id']));
											$nbr_proprios_supprimes = $res_nbr_proprios_supprimes->fetch();
											
											$rq_nbr_sites_dissocies = "
											SELECT count(*) as nbr
											FROM (
												SELECT DISTINCT
													cad_site_id,
													session_af_id
												FROM			
													cadastre.cadastre_cen t3
													JOIN foncier.cadastre_site t4 USING (cad_cen_id)
													JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
													JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)				
												WHERE 
													(rel_saf_foncier.paf_id != 0 OR rel_saf_foncier.objectif_af_id != 3) AND  
													t3.date_fin = 'N.D.' AND 
													t4.date_fin != 'N.D.' AND
													session_af_id = :session_af_id
												) as t1";
											$res_nbr_sites_dissocies = $bdd->prepare($rq_nbr_sites_dissocies);
											$res_nbr_sites_dissocies->execute(array('session_af_id'=>$saf_prob_cadastre[$s]['session_af_id']));
											$nbr_sites_dissocies = $res_nbr_sites_dissocies->fetch();
									?>
										<tr>
											<td>
												<label class='label' style='margin-left:50px;'><?php If ($saf_prob_cadastre[$s]['session_af_id'] != '') {echo $saf_prob_cadastre[$s]['session_af_lib'];} Else {echo 'N.D.';} ?></label>
												<img id='btn_detail_prob_cadastre_<?php echo $saf_prob_cadastre[$s]['session_af_id']; ?>' src='<?php echo ROOT_PATH; ?>images/plus.png' width='15px' style='cursor:pointer;padding-top:10px;' onclick="displayPlusMoins(this, 'td_detail_','prob_cadastre_', '<?php echo $saf_prob_cadastre[$s]['session_af_id']; ?>');">
											</td>
										</tr>
										<tr>
											<td id='td_detail_prob_cadastre_<?php echo $saf_prob_cadastre[$s]['session_af_id']; ?>' style='display:none;'>
												<table style='border:0px solid black;'>														
													<?php
														If ($nbr_parcelles_supprimees['nbr'] > 0){
															echo "<tr><td><label class='label_simple'> il y a ".$nbr_parcelles_supprimees['nbr']." parcelle(s) supprimée(s) sur cette SAF</label></td></tr>";
														}
														If ($nbr_proprios_supprimes['nbr'] > 0){
															echo "<tr><td><label class='label_simple'> il y a ".$nbr_proprios_supprimes['nbr']." changement(s) de propriétaire sur cette SAF</label></td></tr>";
														}
														If ($nbr_sites_dissocies['nbr'] > 0){
															echo "<tr><td><label class='label_simple'> il y a ".$nbr_sites_dissocies['nbr']." parcelle(s) hors site sur cette SAF</label></td></tr>";
														}
													?>
													<tr>
														<td>
															<a style='color:#004494;cursor:pointer;font-size:10pt;font-weight:bold' href='<?php echo $lien_af; ?>'>Consulter la fiche...</a>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									<?php  
										}
									?>
									</table>
								</center>
								<?php  
									}
								?>
							</td>
						</tr>
					</table>	
				<?php
					} Else { echo '&nbsp;'; }
				?>
				</div>
			</div>
		</section>
		<?php include('../includes/footer.php'); ?>
	</body>
</html>