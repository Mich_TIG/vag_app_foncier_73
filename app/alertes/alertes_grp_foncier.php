<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	$lien_foncier = False;

	$rq_nbr_prob_acte_parcelles = "SELECT count(*) as nbr FROM (SELECT DISTINCT mfu_id FROM alertes.v_prob_cadastre_cen) as t1";
	$res_nbr_prob_acte_parcelles = $bdd->prepare($rq_nbr_prob_acte_parcelles);
	$res_nbr_prob_acte_parcelles->execute();
	$nbr_prob_acte_parcelles = $res_nbr_prob_acte_parcelles->fetch();
	If ($nbr_prob_acte_parcelles['nbr'] > 0){
		echo "<label class='alerte'> il y a $nbr_prob_acte_parcelles[nbr] acte(s) dont le cadastre pose problème</label>";
		$lien_foncier = True;
	}
	
	$rq_nbr_prob_acte_saisie = "SELECT count(*) as nbr FROM (SELECT DISTINCT mfu_id FROM alertes.v_prob_mfu_baddata) as t1";
	$res_nbr_prob_acte_saisie = $bdd->prepare($rq_nbr_prob_acte_saisie);
	$res_nbr_prob_acte_saisie->execute();
	$nbr_prob_acte_saisie = $res_nbr_prob_acte_saisie->fetch();
	If ($nbr_prob_acte_saisie['nbr'] > 0){
		echo "<label class='alerte'> il y a $nbr_prob_acte_saisie[nbr] acte(s) comportant des erreurs de saisie</label>";
		$lien_foncier = True;
	}
	
	$rq_nbr_prob_acte_sansparc = "SELECT count(*) as nbr FROM (SELECT DISTINCT mfu_id FROM alertes.v_prob_mfu_sansparc) as t1";
	$res_nbr_prob_acte_sansparc = $bdd->prepare($rq_nbr_prob_acte_sansparc);
	$res_nbr_prob_acte_sansparc->execute();
	$nbr_prob_acte_sansparc = $res_nbr_prob_acte_sansparc->fetch();
	If ($nbr_prob_acte_sansparc['nbr'] > 0){
		echo "<label class='alerte'> il y a $nbr_prob_acte_sansparc[nbr] acte(s) sans parcelle associée</label>";
		$lien_foncier = True;
	}
	
	$rq_nbr_prob_acte_echeance = "SELECT count(*) as nbr, t1.alerte FROM (SELECT DISTINCT mu_id, alerte FROM alertes.v_alertes_mu) as t1 GROUP BY t1.alerte ORDER BY substring(t1.alerte, 3, 1) DESC";
	$res_nbr_prob_acte_echeance = $bdd->prepare($rq_nbr_prob_acte_echeance);
	$res_nbr_prob_acte_echeance->execute();
	$nbr_prob_acte_echeance = $res_nbr_prob_acte_echeance->fetchall();
	For ($i=0; $i<$res_nbr_prob_acte_echeance->rowCount(); $i++) { 
		If ($nbr_prob_acte_echeance[$i]['nbr'] > 0) {
			echo "<label class='alerte'> il y a ". $nbr_prob_acte_echeance[$i]['nbr'] ." acte(s)";
			If ($nbr_prob_acte_echeance[$i]['alerte'] == 'terminée') {
				echo " dont l'échéance est dépassée</label>"; }
			ElseIf ($nbr_prob_acte_echeance[$i]['alerte'] != 'terminée') {
				If (substr($nbr_prob_acte_echeance[$i]['alerte'], 0, 3) == 'fin') {
					echo " fini(s) "; }
				ElseIf (substr($nbr_prob_acte_echeance[$i]['alerte'], 0, 6) == 'recond') {
					echo " reconduit(s) "; }
				If (substr($nbr_prob_acte_echeance[$i]['alerte'], -3) == 'hui') {
					echo "aujourd'hui</label>"; }
				ElseIf (substr($nbr_prob_acte_echeance[$i]['alerte'], -7) == 'un mois') {
					echo "dans moins d'un mois</label>"; }
				ElseIf (substr($nbr_prob_acte_echeance[$i]['alerte'], -6) == '6 mois') {
					echo "dans moins de 6 mois</label>"; }
			}
			$lien_foncier = True;
		}
	}
	
	$rq_nbr_prob_parc_a_decouper = "SELECT count(distinct t1.cad_site_id) as nbr, count(distinct t1.mu_id) as nbr2 FROM (SELECT DISTINCT cad_site_id, mu_id FROM foncier.r_cad_site_mu WHERE surf_mu is not null AND geom is null) as t1";
	$res_nbr_prob_parc_a_decouper = $bdd->prepare($rq_nbr_prob_parc_a_decouper);
	$res_nbr_prob_parc_a_decouper->execute();
	$nbr_prob_parc_a_decouper = $res_nbr_prob_parc_a_decouper->fetch();
	If ($nbr_prob_parc_a_decouper['nbr'] > 0){
		echo "<label class='alerte'> il y a $nbr_prob_parc_a_decouper[nbr] parcelle(s) à découper sur $nbr_prob_parc_a_decouper[nbr2] convention(s)</label>";
		$lien_foncier = True;
	}
	
	If ($lien_foncier == True) {
		echo "<div style='text-align: right;'>
			<a href='". ROOT_PATH ."alertes/accueil_alertes.php#foncier'>Voir en détail...</a>
		</div>";
	} else {
		echo "<label class='alerte'> R.A.S.</label>";
	}
	
?>
	