<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	$lien_af = False;

	$rq_nbr_parcelles_supprimees = "
	SELECT count(*) as nbr
	FROM (
		SELECT DISTINCT
			cad_site_id,
			session_af_id
		FROM			
			cadastre.cadastre_cen t3
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)				
		WHERE 
			(rel_saf_foncier.paf_id != 0 OR rel_saf_foncier.objectif_af_id != 3) AND  
			t3.date_fin != 'N.D.' AND 
			t3.motif_fin_id = 1 AND
			t4.date_fin = 'N.D.'
		) as t1";
	$res_nbr_parcelles_supprimees = $bdd->prepare($rq_nbr_parcelles_supprimees);
	$res_nbr_parcelles_supprimees->execute();
	$nbr_parcelles_supprimees = $res_nbr_parcelles_supprimees->fetch();
	If ($nbr_parcelles_supprimees['nbr'] > 0){
		echo "<label class='alerte'> il y a ".$nbr_parcelles_supprimees['nbr']." parcelle(s) supprimée(s) en cours d'animation</label>";
		$lien_af = True;
	}
	
	$rq_nbr_proprios_supprimes = "
	SELECT count(*) as nbr
	FROM (
		SELECT DISTINCT
			cad_site_id,
			session_af_id
		FROM			
			cadastre.cadastre_cen t3
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)				
		WHERE 
			(rel_saf_foncier.paf_id != 0 OR rel_saf_foncier.objectif_af_id != 3) AND  
			t3.date_fin != 'N.D.' AND 
			t3.motif_fin_id = 2 AND
			t4.date_fin = 'N.D.'
		) as t1";
	$res_nbr_proprios_supprimes = $bdd->prepare($rq_nbr_proprios_supprimes);
	$res_nbr_proprios_supprimes->execute();
	$nbr_proprios_supprimes = $res_nbr_proprios_supprimes->fetch();
	If ($nbr_proprios_supprimes['nbr'] > 0){
		echo "<label class='alerte'> il y a ".$nbr_proprios_supprimes['nbr']." changement(s) de propriétaire sur des parcelles en cours d'animation</label>";
		$lien_af = True;
	}
	
	$rq_nbr_sites_dissocies = "
	SELECT count(*) as nbr
	FROM (
		SELECT DISTINCT
			cad_site_id,
			session_af_id
		FROM			
			cadastre.cadastre_cen t3
			JOIN foncier.cadastre_site t4 USING (cad_cen_id)
			JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)				
		WHERE 
			(rel_saf_foncier.paf_id != 0 OR rel_saf_foncier.objectif_af_id != 3) AND  
			t3.date_fin = 'N.D.' AND 
			t4.date_fin != 'N.D.'
		) as t1";
	$res_nbr_sites_dissocies = $bdd->prepare($rq_nbr_sites_dissocies);
	$res_nbr_sites_dissocies->execute();
	$nbr_sites_dissocies = $res_nbr_sites_dissocies->fetch();
	If ($nbr_sites_dissocies['nbr'] > 0){
		echo "<label class='alerte'> il y a ".$nbr_sites_dissocies['nbr']." parcelles hors site en cours d'animation</label>";
		$lien_af = True;
	}
		
	If ($lien_af == True) {
		echo "<div style='text-align: right;'>
			<a href='". ROOT_PATH ."alertes/accueil_alertes.php#animation_fonciere'>Voir en détail...</a>
		</div>";
	} else {
		echo "<label class='alerte'> R.A.S.</label>";
	}
	
?>
	