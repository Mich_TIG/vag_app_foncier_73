<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
	include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/toggle_tr.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">	
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<TITLE>Base Contacts - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 			
			$h2 ='Les contacts du Conservatoire';
			$h3 ='Liste des contacts';
			include("../includes/header.php");
			include('../includes/breadcrumb.php');
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig', 'grp_administration'));
			// include ('../includes/construction.php');
			
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}
			
			If (isset($_GET['filtre_fonction'])) {$flt_fonction_id = $_GET['filtre_fonction'];} Else {$flt_fonction_id = '';}
			If (isset($_GET['filtre_nom'])) {$flt_nom = $_GET['filtre_nom'];}
			$where_liste_contact = "";
			$where_liste_contact2 = "";

			If ($flt_fonction_id <> '' && isset($flt_fonction_id) && $flt_fonction_id <> -1)
				{$where_liste_contact .= " AND t3.fonctpers_id = '$flt_fonction_id'";}	
			
			If (isset($flt_nom))
				{$where_liste_contact2 .= " AND nom || prenom ILIKE '".$flt_nom."'";}	
			
			$rq_list_contact = "
			SELECT DISTINCT
				particules_ini, 
				particules_lib, 
				nom, 
				prenom, 
				fonctpers_lib, 
				structure_lib, 
				t4.structure_id, 
				t5.individu_id 
			FROM 
				personnes.d_particules as t1
				JOIN personnes.personne as t5 USING (particules_id)
				JOIN personnes.d_individu as t2 USING (individu_id)
				JOIN personnes.d_fonctpers as t3 USING (fonctpers_id)
				JOIN personnes.d_structures as t4 USING (structure_id)
			WHERE 1=1 " . $where_liste_contact . $where_liste_contact2 . "
			ORDER BY nom, prenom";
			$res_list_contact = $bdd->prepare($rq_list_contact);
			$res_list_contact->execute();
			
			$rq_filtre_structures = "SELECT DISTINCT t4.structure_id, t4.structure_lib FROM personnes.d_individu as t2, personnes.d_fonctpers as t3, personnes.d_structures as t4, personnes.personne as t5 WHERE t2.individu_id = t5.individu_id AND t3.fonctpers_id = t5.fonctpers_id AND t4.structure_id = t5.structure_id" . $where_liste_contact . " ORDER BY t4.structure_lib";
			$res_filtre_structures = $bdd->prepare($rq_filtre_structures);
			$res_filtre_structures->execute();

			$rq_filtre_fonction = "SELECT DISTINCT t3.fonctpers_id, t3.fonctpers_lib FROM personnes.d_individu as t2, personnes.d_fonctpers as t3, personnes.d_structures as t4, personnes.personne as t5 WHERE t2.individu_id = t5.individu_id AND t3.fonctpers_id = t5.fonctpers_id AND t4.structure_id = t5.structure_id" . $where_liste_contact . " ORDER BY t3.fonctpers_lib";
			$res_filtre_fonction = $bdd->prepare($rq_filtre_fonction);
			$res_filtre_fonction->execute();
		?>
		
		<section id='main'>
			<h3>Liste des contacts</h3>
			<form action='<?php echo $_SERVER['PHP_SELF']; ?>' method='GET' id='contacts_liste'>
				<input type='hidden' name='hide_order' id='hide_order' value='<?php echo $order_liste_contact; ?>'>
				<table id='tbl_contact' align='CENTER' width='95%' CELLSPACING='0' CELLPADDING='4' border='0'>
					<tbody>
						<tr>
							<td width='55px'><input type='text' id='filtre1' style='display:none;'> </td>
							<td>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td>
											<img  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Tri décroissant"src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='12px' onclick="sortTable('tbl_contact', 'tbody_contacts_corps', 1, 'txt', 'desc');">
										</td>
									</tr>
									<tr>
										<td>											
											<img  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Tri croissant"src="<?php echo ROOT_PATH; ?>images/croissant.png" width='12px' onclick="sortTable('tbl_contact', 'tbody_contacts_corps', 1, 'txt', 'asc');">
										</td>
									</tr>
								</table>
							</td>
							<td>
								<input class='editbox' oninput="maj_table('contacts_liste', 'tbody_contacts_corps', 0);"  type='text' name='filtre_nom' id='filtre_nom' size='35' placeholder="Nom du contact" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nom du contact';" value='<?php If (isset($flt_nom)) {echo "$flt_nom";} ?>'> 
							</td>
							<td>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td>
											<img  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Tri décroissant"src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='12px' onclick="sortTable('tbl_contact', 'tbody_contacts_corps', 2, 'txt', 'desc');">
										</td>
									</tr>
									<tr>
										<td>											
											<img  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Tri croissant"src="<?php echo ROOT_PATH; ?>images/croissant.png" width='12px' onclick="sortTable('tbl_contact', 'tbody_contacts_corps', 2, 'txt', 'asc');">
										</td>
									</tr>
								</table>
							</td>
							<td>
								<label class="btn_combobox">
									<select onchange="maj_table('contacts_liste', 'tbody_contacts_corps', 0);" name="filtre_fonction" id="filtre_fonction" class="combobox">
										<option value='-1'>Fonctions</option>
										<?php
										While ($donnees = $res_filtre_fonction->fetch()) {
											echo "<option ";
											If ($flt_fonction_id == $donnees['fonctpers_id'])
												{echo " selected='selected' ";}
											echo "value='$donnees[fonctpers_id]'>$donnees[fonctpers_lib]</option>";
										}
										$res_filtre_fonction->closeCursor();
										?>
									</select>
								</label>
							</td>
							<td>
								<table CELLSPACING='0' CELLPADDING='0' border='0'>
									<tr>
										<td>
											<img  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Tri décroissant"src="<?php echo ROOT_PATH; ?>images/decroissant.png" width='12px' onclick="sortTable('tbl_contact', 'tbody_contacts_corps', 3, 'txt', 'desc');">
										</td>
									</tr>
									<tr>
										<td>											
											<img  onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Tri croissant"src="<?php echo ROOT_PATH; ?>images/croissant.png" width='12px' onclick="sortTable('tbl_contact', 'tbody_contacts_corps', 3, 'txt', 'asc');">
										</td>
									</tr>
								</table>
							</td>
							<td>
								<label class="btn_combobox">
									<select onchange="maj_table('contacts_liste', 'tbody_contacts_corps', 0);" name="filtre_structure" id="filtre_structure" class="combobox">
										<option value='-1'>Structures</option>
										<?php
										While ($donnees = $res_filtre_structures->fetch()) {
											echo "<option value='$donnees[structure_id]'>$donnees[structure_lib]</option>";
										}
										$res_filtre_structures->closeCursor();
										?>
									</select>
								</label>
							</td>
							<td>
								<a href='<?php echo $_SERVER['PHP_SELF'];?>'><img src='<?php echo ROOT_PATH; ?>images/supprimer.png'></a>
							</td>
						</tr>
					</tbody>
					<?php
					If ($verif_role_acces == 'OUI' && !isset($_GET['filtre_fonction'])) {
						$i=0;
						echo "
						<tbody id='tbody_contacts_new'>
							<tr class='toggle_tr l$i' onclick=\"document.location='new_contact.php'\">
								<td colspan='8'>
									<center><b><a href='new_contact.php' style='font-size:9pt'>
										Ajouter un contact
									</a></b></center>
								</td>
							</tr>
						</tbody>";
					}
					$i=$i+1;
				?>
					<tbody id='tbody_contacts_corps'>
				<?php
					While ($donnees = $res_list_contact->fetch()) {
						$vget = "mode:consult;";
						$vget .= "individu_id:" . $donnees['individu_id'] . ";";
						$vget .= "structure_id:" . $donnees['structure_id'] . ";";
						$vget_crypt = base64_encode($vget);
						$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/contact_select.php?d=$vget_crypt#$donnees[structure_id]";
						
						echo "
						<tr class='toggle_tr l$i' onclick=\"document.location='$lien'\">
							<td width='55px'>
								<a href='$lien'>$donnees[particules_ini].</a>
							</td>
							<td colspan='2'>
								<a href='$lien'>$donnees[nom] $donnees[prenom]</a>
							</td>
							<td colspan='2'>
								<a href='$lien'>$donnees[fonctpers_lib]</a>
							</td>
							<td colspan='3'>
								<a href='$lien'>$donnees[structure_lib]</a>
							</td>
						</tr>
						";
						If ($i == 0){
							$i=1;
						} Else {
							$i=0;
						}
					}
					$res_list_contact->closeCursor();
					?>
					</tbody>
				</table>
			</form>
		</section>
		<?php 
			$bdd = null;
			include("../includes/footer.php"); 
		?>
	</body>
</html>
