<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');	
	// include ('../includes/identify.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/specific_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/explorateur.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<TITLE>Base Contacts - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Les Contacts du Conservatoire';
			$h3 ="Formulaire de saisie d'un nouveau contact";
			include('../includes/header.php');
			include('../includes/breadcrumb.php');
						
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}		
			
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig', 'grp_administration'));
			// include ('../includes/construction.php');
		?>
				
		<section id='main'>
			<?php
			$bad_caract = array("-","–","’", "'", "?", "\t", "	");
			$good_caract = array("–","-","''", "''", "", "", "");
			
			If (isset($_GET['d'])) {
				$d= base64_decode($_GET['d']);
				$min_crit = 0;
				$max_crit = strpos($d, ':');
				$min_val = strpos($d, ':') + 1;
				$max_val = strpos($d, ';') - $min_val;
				
				For ($p=0; $p<substr_count($d, ':'); $p++) {
					$crit = substr($d, $min_crit, $max_crit);
					$value = substr($d, $min_val, $max_val);
					$$crit = $value;
					$min_crit = strpos($d, ';', $min_crit) + 1;
					$max_crit= strpos($d, ':', $min_crit) - $min_crit;
					$min_val = $min_crit + $max_crit + 1;
					$max_val = strpos($d, ';', $min_val) - $min_val;
				}
			}
			//Recupération, verification et intégration des données puis redirection vers la fiche contact si tout est ok
			if (isset($_GET['do']) && $_GET['do'] == 'new_contact') {
				$particule_id = $_POST['need_lst_particules'];
				$nom = strip_tags(str_replace($bad_caract, $good_caract, $_POST['need_new_nom']));
				$prenom = strip_tags(str_replace($bad_caract, $good_caract, $_POST['need_new_prenom']));
				$structure_id = $_POST['need_lst_structure'];
				$fonction_id = $_POST['need_lst_fonctpers'];
				
				$rq_new_individu_id = "SELECT nextval('personnes.d_individu_gid_seq'::regclass)";
				$res_new_individu_id = $bdd->prepare($rq_new_individu_id);
				$res_new_individu_id->execute();
				$new_individu_id_temp = $res_new_individu_id->fetch();
				$new_individu_id = $new_individu_id_temp[0];
				
				$rq_insert_individu = "INSERT INTO personnes.d_individu (individu_id, nom, prenom) VALUES ($new_individu_id, UPPER('$nom'), '$prenom')";
				$res_insert_individu = $bdd->prepare($rq_insert_individu); //prépare la requete à l'execution
				$res_insert_individu->execute(); //execution de la requete
				
				$rq_new_personne_id = "SELECT nextval('personnes.personne_gid_seq'::regclass)";
				$res_new_personne_id = $bdd->prepare($rq_new_personne_id);
				$res_new_personne_id->execute();
				$new_personne_id_temp = $res_new_personne_id->fetch();
				$new_personne_id = $new_personne_id_temp[0];
								
				$rq_insert_personne = "INSERT INTO personnes.personne (personne_id, particules_id, fonctpers_id, individu_id, structure_id) VALUES ($new_personne_id, '$particule_id', '$fonction_id', $new_individu_id, '$structure_id')";
				$res_insert_personne = $bdd->prepare($rq_insert_personne); //prépare la requete à l'execution
				$res_insert_personne->execute(); //execution de la requete
				
				If(isset($_POST['origine'])) {
					$_SESSION['NEW_NOTAIRE_ID'] = $new_personne_id;
					$origine = $_POST['origine'];
				} Else {
					$origine = '';
				}
				
				$vget = "mode:edit;";
				$vget .= "individu_id:" . $new_individu_id . ";";
				$vget .= "personne_id:" . $new_personne_id . ";";
				$vget .= "structure_id:" . $structure_id . ";";
				$vget .= "origine:" . $origine . ";";
				$vget_crypt = base64_encode($vget);
				$lien = ROOT_PATH."contacts/contact_select.php?d=$vget_crypt#$structure_id";
				echo "<script type='text/javascript'>document.location.replace('$lien');</script>";
			}
				
			?>
			<form onsubmit="return valide_form('frm_ajout_contact')" name="frm_ajout_contact" action='new_contact.php?do=new_contact' id='frm_ajout_contact' method='POST' class='wrapper' style='display:block'>
				<input type='hidden' id='origine' name='origine' value='<?php If (isset($origine)) {echo $origine;} ?>'>
				<h3>Formulaire de saisie d'un nouveau contact</h3>
				<table align='CENTER' CELLSPACING='6' CELLPADDING='4' border='0' class='sous_table' style="padding-top:20px; padding-bottom:20px;">
					<tr>
						<td>
							<div <?php If (isset($origine) && $origine == 'mfu') {echo "class='readonly'";} ?>>
								<label class="btn_combobox">
									<select name="need_lst_particules" id="need_lst_particules" class="combobox" <?php If (isset($origine) && $origine == 'mfu') {echo 'READONLY';} ?>>
										<option value='-1'>Civilit&eacute;</option>
										<?php
											$rq_lst_particules = "SELECT DISTINCT particules_id, particules_lib FROM personnes.d_particules ORDER BY particules_id";
											$res_lst_particules = $bdd->prepare($rq_lst_particules);
											$res_lst_particules->execute();
											While ($donnees = $res_lst_particules->fetch()) {
												echo "<option ";
												If ($donnees['particules_id'] == PARTINOT_ID && isset($type_contact) && $type_contact == 'notaire') {
													echo " selected='selected' ";
												}
												echo "value='$donnees[particules_id]'>$donnees[particules_lib]</option>";
											}
											$res_lst_particules->closeCursor();
										?>
									</select>
								</label>
							</div>
						</td>
						<td>
							<label class='label'>Pr&eacute;nom :</label>
							<input type='text' class='editbox' name='need_new_prenom' id='need_new_prenom'>
						</td>
						<td>
							<label class='label'>Nom :</label>
							<input type='text' class='editbox' name='need_new_nom' id='need_new_nom' onkeyup='this.value=this.value.toUpperCase()'>
						</td>
					</tr>
				</table>
				
				<table align='CENTER' CELLSPACING='6' CELLPADDING='4' border='0' class='sous_table' style="padding-top:20px; padding-bottom:20px;">
					<tr>
						<td>
							<label class='label'>Structure :</label>
							<div <?php If (isset($origine) && $origine == 'mfu') {echo "class='readonly'";} ?>>
								<label class="btn_combobox">
									<select name="need_lst_structure" class="combobox " id="need_lst_structure" <?php If (isset($origine) && $origine == 'mfu') {echo 'READONLY';} ?>>
										<option value='-1'>Structure</option>
										<?php
										$rq_lst_structure = "SELECT DISTINCT structure_id, structure_lib FROM personnes.d_structures ORDER BY structure_lib";
										$res_lst_structure = $bdd->prepare($rq_lst_structure);
										$res_lst_structure->execute();
										While ($donnees = $res_lst_structure->fetch()) {
											echo "<option ";
											If ($donnees['structure_id'] == STRUCTNOT_ID && isset($type_contact) && $type_contact == 'notaire') {
												echo " selected='selected' ";
											}
											echo "value='$donnees[structure_id]'>$donnees[structure_lib]</option>";
										}
										$res_lst_structure->closeCursor();
										?>
									</select>
								</label>
							</div>
							<?php If ($verif_role_acces == 'OUI' && !isset($origine) || (isset($origine) && $origine !== 'mfu')) {?>
							<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour ajouter une structure &agrave; la liste" onclick="addSpanValue_js('frm_ajout_contact', 'need_lst_structure', 'personnes.d_structures', 'structure_id', 'structure_lib', 'Structure', 'Nom de la structure', '');">
							<?php } ?>
						</td>
						<td>
							<label class='label'>Fonction :</label>
							<div <?php If (isset($origine) && $origine == 'mfu') {echo "class='readonly'";} ?>>
								<label class="btn_combobox">
									<select name="need_lst_fonctpers" class="combobox" id="need_lst_fonctpers"  <?php If (isset($origine) && $origine == 'mfu') {echo 'READONLY';} ?>>
										<option value='-1'>Fonction</option>
										<?php
										$rq_lst_fonctpers = "SELECT DISTINCT fonctpers_id, fonctpers_lib FROM personnes.d_fonctpers ORDER BY fonctpers_lib";
										$res_lst_fonctpers = $bdd->prepare($rq_lst_fonctpers);
										$res_lst_fonctpers->execute();
										While ($donnees = $res_lst_fonctpers->fetch()) {
											echo "<option ";
											If ($donnees['fonctpers_id'] == FONCTNOT_ID && isset($type_contact) && $type_contact == 'notaire') {
												echo " selected='selected' ";
											}
											echo "value='$donnees[fonctpers_id]'>$donnees[fonctpers_lib]</option>";
										}
										$res_lst_fonctpers->closeCursor();
										?>
									</select>
								</label>
							</div>
							<?php If ($verif_role_acces == 'OUI' && !isset($origine) || (isset($origine) && $origine !== 'mfu')) {?>
							<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour ajouter une fonction &agrave; la liste" onclick="addSpanValue_js('frm_ajout_contact', 'need_lst_fonctpers', 'personnes.d_fonctpers', 'fonctpers_id', 'fonctpers_lib', 'Fonction', 'Nom de la fonction', '');">
							<?php } ?>
						</td>
					</tr>
				</table>
				<input type='submit' class='valid' name='valid_form' id='valid_form'>
			</form>
			<div class='lib_alerte' id='prob_saisie' style='display:none; margin-top:25px'><b>Veuillez compl&eacuteter votre saisie</b></div>
			<div class='explorateur_span' id='explorateur' style='display:none'></div>
		</section>
		<?php
			$bdd = null;
			include("../includes/footer.php");
		?>
	</body>
</html>
