<?php
	/**************************************************************************************************************************
	***************************************************************************************************************************
	***************************************************************************************************************************
	Copyright Conservatoire d’espaces naturels de Savoie, 2018
	-	Contributeur : Nicolas MIGNOT & Alexandre LESCONNEC
	-	Adresse électronique : sig at cen-savoie.org

	Ce *développement avec l’ensemble de ses composants constitue un système informatique servant à agréger et gérer les données cadastrales sur les sites Conservatoire. Le logiciel* permet notamment :
	-	d’associer à ces entités des actes apportant la maîtrise foncière et/ou d’usage au Conservatoire ; 
	-	de gérer et suivre ces actes (convention et acquisition) ;
	-	et d’en établir des bilans.
	-	Un module permet de réaliser des sessions d’animation foncière.

	Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".

	En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.

	À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
	***************************************************************************************************************************
	***************************************************************************************************************************
	**************************************************************************************************************************/

	session_start();
	$browser = strstr(@$_SERVER['HTTP_USER_AGENT'], 'Firefox');	
	include ('../includes/configuration.php');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/base_content.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/regular_content.css">
		<link rel="stylesheet" type="text/css" href= "<?php echo ROOT_PATH; ?>css/onglet.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>css/explorateur.css">
		<link type="image/x-icon" href="<?php echo ROOT_PATH; ?>images/favicon.ico" rel="shortcut icon">
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.tabs.pack.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/cen_scripts.js"></script>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/tooltip.js"></script><div id="tooltip"></div>
		<script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/onglet.js"></script>
		<TITLE>Base Contacts - <?php echo STRUCTURE; ?></TITLE>
	</head>
	<body>
		<?php 
			$h2 ='Les Contacts du Conservatoire';
			$h3 ="Formulaire de saisie d'un nouveau contact";
			include ('../includes/valide_acces.php');
			$verif_role_acces = role_access(array('grp_sig', 'grp_administration'));
			// include ('../includes/construction.php');
			/* Création du lien utilisé dans la page includes/header.php permettant de passer du mode consultation au mode édition et inversement
			Pour cela il faut récréer la variable GET 'd' en modifiant le mode */
			$bad_caract = array("-","–","’", "'");
			$good_caract = array("–","-","''", "''");
			$d= base64_decode($_GET['d']);
			$min_crit = 0;
			$max_crit = strpos($d, ':');
			$min_val = strpos($d, ':') + 1;
			$max_val = strpos($d, ';') - $min_val;
			
			For ($p=0; $p<substr_count($d, ':'); $p++) {
				$crit = substr($d, $min_crit, $max_crit);
				$value = substr($d, $min_val, $max_val);
				$$crit = $value;
				$min_crit = strpos($d, ';', $min_crit) + 1;
				$max_crit= strpos($d, ':', $min_crit) - $min_crit;
				$min_val = $min_crit + $max_crit + 1;
				$max_val = strpos($d, ';', $min_val) - $min_val;
			}
			
			If ($structure_id == STRUCTNOT_ID) {
				$verif_role_acces = role_access(array('grp_foncier'));
			}
			
			if ($mode=='consult') {
				$vget_cnx = "mode:edit;";
			} else {
				$vget_cnx = "mode:consult;";
			}
			$vget_cnx .= "individu_id:" . $individu_id . ";";
			$vget_cnx .= "structure_id:" . $structure_id . ";";
			$vget_crypt_cnx = base64_encode($vget_cnx);
			$lien_cnx = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/contact_select.php?d=$vget_crypt_cnx#$structure_id";
			include("../includes/header.php");
			include('../includes/breadcrumb.php');
			try {
				$bdd = new PDO("pgsql:host=".HOST.";port=".PORT.";dbname=".DBNAME, $_SESSION['user'], $_SESSION['mdp']);
			}
			catch (Exception $e) {
				die(include('../includes/prob_tech.php'));
			}		
			include('../includes/cen_fct.php');
		
			$vget = "mode:consult;";
			$vget .= "individu_id:" . $individu_id . ";";
			$vget .= "structure_id:" . $structure_id . ";";
			If (isset($origine)) {$vget .= "origine:" . $origine . ";";}
			$vget_crypt = base64_encode($vget);
			$lien = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/contact_select.php?d=$vget_crypt#$structure_id";
			
			If (isset($do) && $do == 'new_structure') {
				$individu_id = str_replace($bad_caract, $good_caract, $_POST['individu_id']);
				$structure_id = str_replace($bad_caract, $good_caract, $_POST['need_lst_structure']);
				$particule_id = str_replace($bad_caract, $good_caract, $_POST['need_lst_particules']);
				$fonction_id = str_replace($bad_caract, $good_caract, $_POST['need_lst_fonctpers']);
				$adresse1 = str_replace($bad_caract, $good_caract, $_POST['new_adresse1']);
				$adresse2 = str_replace($bad_caract, $good_caract, $_POST['new_adresse2']);
				$adresse3 = str_replace($bad_caract, $good_caract, $_POST['new_adresse3']);
				$new_mail = str_replace($bad_caract, $good_caract, $_POST['new_mail']);
				$new_tel_fix1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix1']);
				$new_tel_fix2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix2']);
				$new_tel_portable1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable1']);
				$new_tel_portable2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable2']);
				$new_tel_fax = str_replace($bad_caract, $good_caract, $_POST['new_tel_fax']);
				$new_comm = str_replace($bad_caract, $good_caract, $_POST['new_comm']);
				
				$rq_insert_personne = "INSERT INTO personnes.personne (particules_id, fonctpers_id, individu_id, structure_id, email, adresse1, adresse2, adresse3, fixe_domicile, fixe_travail, portable_domicile, portable_travail, fax, remarques) VALUES ('$particule_id', '$fonction_id', '$individu_id', '$structure_id', '$new_mail', '$adresse1', '$adresse2', '$adresse3', replace('$new_tel_fix1',' ',''), replace('$new_tel_fix2',' ',''), replace('$new_tel_portable1',' ',''), replace('$new_tel_portable2',' ',''), replace('$new_tel_fax',' ',''), '$new_comm')";
				$res_insert_personne = $bdd->prepare($rq_insert_personne); //prépare la requête à l'execution
				$res_insert_personne->execute(); //execution de la requete
			} ElseIf (isset($do) && $do == 'modif_structure') {
				If (!isset($_POST['individu_id']) || !isset($_POST['srtructure_id']) || !isset($_POST['nbr_fct'])) {
					$page_operationnelle = ROOT_PATH . 'contacts/contacts_liste_all.php';
					die(include('../includes/prob_tech.php'));
				}
				$individu_id = $_POST['individu_id'];
				$structure_id = $_POST['srtructure_id'];
				
				For ($i=0; $i<$_POST['nbr_fct']; $i++) {
					$personne_id = str_replace($bad_caract, $good_caract, $_POST['personne_id_'.$i]);
					$particule_id = str_replace($bad_caract, $good_caract, $_POST['need_lst_particules_'.$i]);
					$fonction_id = str_replace($bad_caract, $good_caract, $_POST['need_lst_fonctpers_'.$i]);
					$adresse1 = str_replace($bad_caract, $good_caract, $_POST['new_adresse1_'.$i]);
					$adresse2 = str_replace($bad_caract, $good_caract, $_POST['new_adresse2_'.$i]);
					$adresse3 = str_replace($bad_caract, $good_caract, $_POST['new_adresse3_'.$i]);
					$new_mail = str_replace($bad_caract, $good_caract, $_POST['new_mail_'.$i]);
					$new_tel_fix1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix1_'.$i]);
					$new_tel_fix2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix2_'.$i]);
					$new_tel_portable1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable1_'.$i]);
					$new_tel_portable2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable2_'.$i]);
					$new_tel_fax = str_replace($bad_caract, $good_caract, $_POST['new_tel_fax_'.$i]);
					$new_comm = str_replace($bad_caract, $good_caract, $_POST['new_comm_'.$i]);
					
					$rq_update_personne = "UPDATE personnes.personne SET particules_id = '$particule_id', fonctpers_id = '$fonction_id', individu_id = '$individu_id', structure_id = '$structure_id', email = '$new_mail', adresse1 = '$adresse1', adresse2 = '$adresse2', adresse3 = '$adresse3', fixe_domicile = replace('$new_tel_fix1',' ',''), fixe_travail = replace('$new_tel_fix2',' ',''), portable_domicile = replace('$new_tel_portable1',' ',''), portable_travail = replace('$new_tel_portable2',' ',''), fax = replace('$new_tel_fax',' ',''), remarques = '$new_comm' WHERE personne_id = '$personne_id'";
					$res_update_personne = $bdd->prepare($rq_update_personne); //prépare la requête à l'execution
					$res_update_personne->execute(); //execution de la requete
				}
				
				If (isset($_POST['need_lst_particules'])) {
					$particule_id = str_replace($bad_caract, $good_caract, $_POST['need_lst_particules']);
					$fonction_id = str_replace($bad_caract, $good_caract, $_POST['need_lst_fonctpers']);
					$adresse1 = str_replace($bad_caract, $good_caract, $_POST['new_adresse1']);
					$adresse2 = str_replace($bad_caract, $good_caract, $_POST['new_adresse2']);
					$adresse3 = str_replace($bad_caract, $good_caract, $_POST['new_adresse3']);
					$new_mail = str_replace($bad_caract, $good_caract, $_POST['new_mail']);
					$new_tel_fix1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix1']);
					$new_tel_fix2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_fix2']);
					$new_tel_portable1 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable1']);
					$new_tel_portable2 = str_replace($bad_caract, $good_caract, $_POST['new_tel_portable2']);
					$new_tel_fax = str_replace($bad_caract, $good_caract, $_POST['new_tel_fax']);
					$new_comm = str_replace($bad_caract, $good_caract, $_POST['new_comm']);
					
					$rq_insert_personne = "INSERT INTO personnes.personne (particules_id, fonctpers_id, individu_id, structure_id, email, adresse1, adresse2, adresse3, fixe_domicile, fixe_travail, portable_domicile, portable_travail, fax, remarques) VALUES ('$particule_id', '$fonction_id', '$individu_id', '$structure_id', '$new_mail', '$adresse1', '$adresse2', '$adresse3', replace('$new_tel_fix1',' ',''), replace('$new_tel_fix2',' ',''), replace('$new_tel_portable1',' ',''), replace('$new_tel_portable2',' ',''), replace('$new_tel_fax',' ',''), '$new_comm')";
					$res_insert_personne = $bdd->prepare($rq_insert_personne); //prépare la requête à l'execution
					$res_insert_personne->execute(); //execution de la requete
				}
			}
			
			$rq_select_nom_prenom = "SELECT DISTINCT nom, prenom FROM personnes.d_individu WHERE individu_id ='$individu_id'";
			$res_select_nom_prenom = $bdd->prepare($rq_select_nom_prenom);
			$res_select_nom_prenom->execute();
			$return_select_nom_prenom = $res_select_nom_prenom->fetch();
			$nom = $return_select_nom_prenom['nom'];
			$prenom = $return_select_nom_prenom['prenom'];
			
			$rq_lst_structures = "SELECT DISTINCT t1.structure_id as id, t1.structure_lib as lib FROM personnes.d_structures t1, personnes.personne t2 WHERE t1.structure_id = t2.structure_id AND t2.individu_id = '$individu_id'";
			$res_lst_strucutres = $bdd->prepare($rq_lst_structures);
			$res_lst_strucutres->execute();
			$structure = $res_lst_strucutres->fetchall();
		?>
		
		<section id='main'>
			<h3><?php echo $prenom . ' ' . $nom; ?></h3>
			<div class='list' id='onglet'>
				<ul>
					<?php
						For ($i=0; $i<$res_lst_strucutres->rowCount(); $i++)
							{
								echo "
								<li>
									<a href='#". $structure[$i]['id'] ."'><span>" . $structure[$i]['lib'] . "</span></a>
								</li>";
							}
						If ($mode=='edit') {	
					?>
					<li class="plus" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Ajouter une autre structure &agrave; ce contact">
						<a href='#new'><span><img width='14px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png"></span></a>
					</li>
					<?php }  ?>
				</ul>
				
				<?php 
					For ($i=0; $i<count($structure); $i++)
						{
							$rq_lst_info = "SELECT t1.fonctpers_lib, t2.particules_lib, t3.email, t3.adresse1, t3.adresse2, t3.adresse3, t3.fixe_domicile, t3.fixe_travail, t3.portable_domicile, t3.portable_travail, t3.fax, t3.remarques, t1.fonctpers_id, t2.particules_id, t3.personne_id FROM personnes.d_fonctpers t1, personnes.d_particules t2, personnes.personne t3 WHERE t1.fonctpers_id = t3.fonctpers_id AND t2.particules_id = t3.particules_id AND t3.structure_id = '". $structure[$i]['id'] ."' AND t3.individu_id = '$individu_id'";
							$res_lst_info = $bdd->prepare($rq_lst_info);
							$res_lst_info->execute();
							$return_lst_info = $res_lst_info->fetchall();
				?>
							<div id='<?php echo $structure[$i]['id']; ?>'>
							<?php 
								If ($mode=='consult') { 
								If (isset($origine) && $origine == 'mfu') {
									echo "<script type='text/javascript'>self.close();</script>";
								}
							?>
								<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black' align='CENTER'>
								<?php For ($j=0; $j<$res_lst_info->rowCount(); $j++) {?>										
									<tr>
										<td colspan='2'>
											<table width = '100%' align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px">
												<tr>
													<td style='padding-bottom:10px;text-align:center;white-space:nowrap;'>
														<label class="label"><?php echo $return_lst_info[$j]['particules_lib']; ?></label>					
														<label class='label'><?php echo $prenom; ?></label>
														<label class='label'><?php echo $nom; ?></label>
													</td>
												</tr>
												<tr>
													<td style='padding-top:0px;text-align:center'>
														<label class='label'>Fonction : <?php echo $return_lst_info[$j]['fonctpers_lib']; ?></label>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<?php If($return_lst_info[$j]['email'] != '' Or $return_lst_info[$j]['adresse1'] != '' Or $return_lst_info[$j]['adresse3']) {?>
										<td>
											<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
												<?php If($return_lst_info[$j]['adresse1'] != '' Or $return_lst_info[$j]['adresse2'] != '' Or $return_lst_info[$j]['adresse3'] != '') {?>
												<tr>
													<td colspan='3'>
														<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
															<tr>
																<td rowspan='3' style="vertical-align:top;">
																	<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
																</td>
																<?php If($return_lst_info[$j]['adresse1'] != '') { ?>
																<td style='text-align:right;'>
																	<label class='label'><?php echo $return_lst_info[$j]['adresse1']; ?></label>
																</td>
																<?php } ?>
															</tr>
															<?php If($return_lst_info[$j]['adresse2'] != '') { ?>
															<tr>
																<td style='text-align:right;'>
																	<label class='label'><?php echo $return_lst_info[$j]['adresse2']; ?></label>
																</td>	
															</tr>
															<?php } ?>
															<tr>
																<td style='text-align:right;'>
																	<label class='label'><?php echo $return_lst_info[$j]['adresse3']; ?></label>
																</td>	
															</tr>
														</table>
													</td>
												</tr>
												<?php } 
												If($return_lst_info[$j]['email'] != '') {?>
												<tr>
													<td style='vertical-align:middle;padding-top:20px;'>
														<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
													</td>
													<td style='vertical-align:middle;padding-top:20px;'>
														<label class='label'><?php echo $return_lst_info[$j]['email']; ?></label>
													</td>
													<td style='vertical-align:middle;padding-top:25px;'>
														<a href="https://mail.espaces-naturels.fr/zimbra/?view=compose&to=<?php echo $prenom; ?>%20<?php echo strtoupper($nom); ?>%20<?php echo $return_lst_info[$j]['email']; ?>#1" class="label"  target='_blank'><img src='<?php echo ROOT_PATH; ?>images/send_mail.png' width='20px'></a>
													</td>
												</tr>
												<?php } ?>
											</table>
										</td>
										<?php }
										If($return_lst_info[$j]['fixe_domicile'] != '' Or $return_lst_info[$j]['fixe_travail'] != '' Or $return_lst_info[$j]['portable_domicile'] != '' Or $return_lst_info[$j]['portable_travail'] != '' Or $return_lst_info[$j]['fax'] != '') { ?>
										<td>
											<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
												<?php If($return_lst_info[$j]['fixe_domicile'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
													</td>
													<td>
														<label class='label'><?php echo numtel_transform($return_lst_info[$j]['fixe_domicile']); ?> (perso)</label>
													</td>	
												</tr>
												<?php } 
												If($return_lst_info[$j]['fixe_travail'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
													</td>
													<td style='padding-bottom:8px;'>
														<label class='label'><?php echo numtel_transform($return_lst_info[$j]['fixe_travail']); ?> (pro)</label>
													</td>	
												</tr>
												<?php } 
												If($return_lst_info[$j]['portable_domicile'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
													</td>
													<td>
														<label class='label'><?php echo numtel_transform($return_lst_info[$j]['portable_domicile']); ?> (perso)</label>
													</td>	
												</tr>
												<?php } 
												If($return_lst_info[$j]['portable_travail'] != '') {?>
												<tr>
													<td style='text-align:right;padding-bottom:8px;'>
														<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
													</td>
													<td style='padding-bottom:8px;'>
														<label class='label'><?php echo numtel_transform($return_lst_info[$j]['portable_travail']); ?> (pro)</label>
													</td>	
												</tr>
												<?php } 
												If($return_lst_info[$j]['fax'] != '') {?>
												<tr>
													<td style='text-align:right'>
														<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
													</td>
													<td>
														<label class='label'><?php echo numtel_transform($return_lst_info[$j]['fax']); ?></label>
													</td>	
												</tr>
												<?php } ?>
											</table>
										</td>
										<?php } ?>
									</tr>
									<?php If($return_lst_info[$j]['remarques'] != '') { ?>
									<tr>
										<td style='text-align:center' colspan='2'>
											<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="width: 90%;padding-top:0px;border:0px;">
												<tr>
													<td style='text-align:right; vertical-align:top;padding:10px;'>
														<label class='label'>Remarques :</label>
													</td>
													<td>
														<textarea id='txt_com' class="label textarea_com" readonly rows="3" cols="50"><?php echo $return_lst_info[$j]['remarques']; ?></textarea>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<?php } ?>
									<tr>
										<td style='text-align:center' colspan='2'>												
											<label class='label'>___________________________________________</label>
										</td>
									</tr>
									<?php }?>
								</table>
								<?php } Else If ($mode=='edit') {
								$vget_modif_structure = $vget . "do:modif_structure;";
								$vget_crypt_modif_structure = base64_encode($vget_modif_structure);
								$lien_modif_structure = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/contact_select.php?d=$vget_crypt_modif_structure#" . $structure[$i]['id'];?>
								<form onsubmit="return valide_form('frm_modif_structure_<?php echo $structure[$i]['id']; ?>')" name="frm_modif_structure_<?php echo $structure[$i]['id']; ?>" action='<?php echo $lien_modif_structure; ?>' id='frm_modif_structure_<?php echo $structure[$i]['id']; ?>' method='POST'>
									<input type='hidden' name='individu_id' id='individu_id' value='<?php echo $individu_id; ?>'>
									<input type='hidden' name='nbr_fct' id='nbr_fct' value='<?php echo $res_lst_info->rowCount(); ?>'>
									<input type='hidden' name='srtructure_id' id='srtructure_id' value='<?php echo $structure[$i]['id']; ?>'>
									<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black' align='CENTER'>
										<?php For ($j=0; $j<$res_lst_info->rowCount(); $j++) {?>		
										<input type='hidden' name="personne_id_<?php echo $j; ?>" id="personne_id_<?php echo $j; ?>" value='<?php echo $return_lst_info[$j]['personne_id']; ?>'>
										<tr>
											<td colspan='2'>
												<table width = '100%' align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px">
													<tr>
														<td style='text-align:center;white-space:nowrap;'>
															<label class="btn_combobox">
																<select name="need_lst_particules_<?php echo $j; ?>" id="need_lst_particules_<?php echo $j; ?>" class="combobox">
																	<option value='-1'>Civilit&eacute;</option>
																	<?php
																	$rq_lst_particules = "SELECT DISTINCT particules_id, particules_lib FROM personnes.d_particules ORDER BY particules_id";
																	$res_lst_particules = $bdd->prepare($rq_lst_particules);
																	$res_lst_particules->execute();
																	While ($donnees = $res_lst_particules->fetch()) {
																		echo "<option ";
																		If ($donnees['particules_id'] == $return_lst_info[$j]['particules_id']) {
																			echo " selected='selected' ";
																		}
																		echo "value='$donnees[particules_id]'>$donnees[particules_lib]</option>";
																	}
																	$res_lst_particules->closeCursor();
																	?>
																</select>
															</label>											
															<label class='label'><?php echo $prenom; ?></label>
															<label class='label'><?php echo $nom; ?></label>	
														</td>
													</tr>
													<tr>
														<td style='padding-top:20px;text-align:center'>
															<label class='label'>Fonction :</label>
															<label class="btn_combobox">
																<select name="need_lst_fonctpers_<?php echo $j; ?>" class="combobox" id="need_lst_fonctpers_<?php echo $j; ?>">
																	<option value='-1'>Fonction</option>
																	<?php
																	$rq_lst_fonctpers = "SELECT DISTINCT fonctpers_id, fonctpers_lib FROM personnes.d_fonctpers ORDER BY fonctpers_lib";
																	$res_lst_fonctpers = $bdd->prepare($rq_lst_fonctpers);
																	$res_lst_fonctpers->execute();
																	While ($donnees = $res_lst_fonctpers->fetch()) {
																		echo "<option ";
																		If ($donnees['fonctpers_id'] == $return_lst_info[$j]['fonctpers_id'])
																			{echo " selected='selected' ";}
																		echo "value='$donnees[fonctpers_id]'>$donnees[fonctpers_lib]</option>";
																	}
																	$res_lst_fonctpers->closeCursor();
																	?>
																</select>
															</label>
															<?php If ($verif_role_acces == 'OUI') {?>
															<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour ajouter une fonction &agrave; la liste" onclick="addSpanValue_js('onglet', 'need_lst_fonctpers_<?php echo $j; ?>', 'personnes.d_fonctpers', 'fonctpers_id', 'fonctpers_lib', 'Fonction', 'Nom de la fonction', '');">
															<?php } ?>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
													<tr>
														<td colspan='2'>
															<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
																<tr>
																	<td rowspan='3' style="vertical-align:top;">
																		<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
																	</td>
																	<td>
																		<input type='text' value="<?php echo $return_lst_info[$j]['adresse1'];?>" size='80' class='editbox' name='new_adresse1_<?php echo $j; ?>' id='new_adresse1_<?php echo $j; ?>' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / B&acirc;timent">
																	</td>
																</tr>
																<tr>
																	<td style='text-align:right;'>
																		<input type='text' value="<?php echo $return_lst_info[$j]['adresse2'];?>" size='80' class='editbox' name='new_adresse2_<?php echo $j; ?>' id='new_adresse2_<?php echo $j; ?>' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N&deg;, Rue, Boite postale">
																	</td>	
																</tr>
																<tr>
																	<td style='text-align:right;'>
																		<input type='text' value="<?php echo $return_lst_info[$j]['adresse3'];?>" size='80' class='editbox' name='new_adresse3_<?php echo $j; ?>' id='new_adresse3_<?php echo $j; ?>' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal & Ville">
																	</td>	
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style='vertical-align:bottom;'>
															<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
														</td>
														<td style='vertical-align:middle;padding-top:20px;'>
															<input type='text' value="<?php echo$return_lst_info[$j]['email'];?>" size='80' class='editbox' name='new_mail_<?php echo $j; ?>' id='new_mail_<?php echo $j; ?>'>
														</td>
													</tr>
												</table>
											</td>
											<td>
												<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-right:50px">
													<tr>
														<td style='text-align:right'>
															<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
														</td>
														<td>
															<input type='text' value="<?php echo $return_lst_info[$j]['fixe_domicile'];?>" class='editbox' name='new_tel_fix1_<?php echo $j; ?>' id='new_tel_fix1_<?php echo $j; ?>'><label class='label'>(perso)</label>
														</td>	
													</tr>
													<tr>
														<td style='text-align:right'>
															<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
														</td>
														<td style='padding-bottom:8px;'>
															<input type='text' value="<?php echo $return_lst_info[$j]['fixe_travail'];?>" class='editbox' name='new_tel_fix2_<?php echo $j; ?>' id='new_tel_fix2_<?php echo $j; ?>'><label class='label'>(pro)</label>
														</td>	
													</tr>
													<tr>
														<td style='text-align:right'>
															<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
														</td>
														<td>
															<input type='text' value="<?php echo $return_lst_info[$j]['portable_domicile'];?>" class='editbox' name='new_tel_portable1_<?php echo $j; ?>' id='new_tel_portable1_<?php echo $j; ?>'><label class='label'>(perso)</label>
														</td>	
													</tr>
													<tr>
														<td style='text-align:right'>
															<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
														</td>
														<td style='padding-bottom:8px;'>
															<input type='text' value="<?php echo $return_lst_info[$j]['portable_travail'];?>" class='editbox' name='new_tel_portable2_<?php echo $j; ?>' id='new_tel_portable2_<?php echo $j; ?>'><label class='label'>(pro)</label>
														</td>	
													</tr>
													<tr>
														<td style='text-align:right'>
															<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
														</td>
														<td>
															<input type='text' value="<?php echo $return_lst_info[$j]['fax'];?>" class='editbox' name='new_tel_fax_<?php echo $j; ?>' id='new_tel_fax_<?php echo $j; ?>'>
														</td>	
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style='text-align:center' colspan='2'>
												<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="width: 90%;padding-top:0px;border:0px;">
													<tr>
														<td style='text-align: right; vertical-align: top;'>
															<label class='label'>Remarques :</label>
														</td>
														<td>
															<textarea style="" onkeyup="" onchange="" id='new_comm_<?php echo $j; ?>' name='new_comm_<?php echo $j; ?>' class="label textarea_com_edit" rows='3' cols='50'><?php echo $return_lst_info[$j]['remarques']; ?></textarea>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style='text-align:center' colspan='2'>												
												<label class='label'>___________________________________________</label>
											</td>
										</tr>
										<?php } ?>
										<tr>
											<td colspan='2' style='text-align:center'>
												<a class='label' href="javascript:slidediv('td_new_fonction_<?php echo $structure[$i]['id']; ?>')">Ajouter une fonction &agrave; <?php echo $prenom . ' ' . $nom; ?> pour la structure <?php echo $structure[$i]['lib']; ?></a>
											</td>
										</tr>
										<tr>
											<td colspan='2'>
												<div style='display:none' id='td_new_fonction_<?php echo $structure[$i]['id']; ?>'>
													<input type='hidden' name='individu_id' id='individu_id' value='<?php echo $individu_id; ?>'>
													<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black' align='CENTER'>
														<tr>
															<td colspan='2'>
																<table width = '100%' align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px">
																	<tr>
																		<td style='text-align:center;white-space:nowrap;'>
																			<label class="btn_combobox">
																				<select name="temp_lst_particules" id="temp_lst_particules" class="combobox">
																					<option value='-1'>Civilit&eacute;</option>
																					<?php
																					$rq_lst_particules = "SELECT DISTINCT particules_id, particules_lib FROM personnes.d_particules ORDER BY particules_id";
																					$res_lst_particules = $bdd->prepare($rq_lst_particules);
																					$res_lst_particules->execute();
																					While ($donnees = $res_lst_particules->fetch()) {
																						echo "<option value='$donnees[particules_id]'>$donnees[particules_lib]</option>";
																					}
																					$res_lst_particules->closeCursor();
																					?>
																				</select>
																			</label>												
																			<label class='label'><?php echo $prenom; ?></label>
																			<label class='label'><?php echo $nom; ?></label>
																		</td>
																	</tr>
																	<tr>
																		<td style='padding-top:20px;text-align:center'>
																			<label class='label'>Fonction :</label>
																			<label class="btn_combobox">
																				<select name="temp_lst_fonctpers" class="combobox" id="temp_lst_fonctpers">
																					<option value='-1'>Fonction</option>
																					<?php
																					$rq_lst_fonctpers = "SELECT DISTINCT fonctpers_id, fonctpers_lib FROM personnes.d_fonctpers ORDER BY fonctpers_lib";
																					$res_lst_fonctpers = $bdd->prepare($rq_lst_fonctpers);
																					$res_lst_fonctpers->execute();
																					While ($donnees = $res_lst_fonctpers->fetch()) {
																						echo "<option value='$donnees[fonctpers_id]'>$donnees[fonctpers_lib]</option>";
																					}
																					$res_lst_fonctpers->closeCursor();
																					?>
																				</select>
																			</label>
																			<?php If ($verif_role_acces == 'OUI') {?>
																			<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour ajouter une fonction &agrave; la liste" onclick="addSpanValue_js('onglet', 'need_lst_fonctpers', 'personnes.d_fonctpers', 'fonctpers_id', 'fonctpers_lib', 'Fonction', 'Nom de la fonction', '');">
																			<?php } ?>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td>
																<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
																	<tr>
																		<td colspan='2'>
																			<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
																				<tr>
																					<td rowspan='3' style="vertical-align:top;">
																						<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
																					</td>
																					<td>
																						<input type='text' size='80' class='editbox' name='new_adresse1' id='new_adresse1' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / B&acirc;timent">
																					</td>
																				</tr>
																				<tr>
																					<td style='text-align:right;'>
																						<input type='text' size='80' class='editbox' name='new_adresse2' id='new_adresse2' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N&deg;, Rue, Boite postale">
																					</td>	
																				</tr>
																				<tr>
																					<td style='text-align:right;'>
																						<input type='text' size='80' class='editbox' name='temp_new_adresse3' id='temp_new_adresse3' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal & Ville">
																					</td>	
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td style='vertical-align:bottom;'>
																			<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
																		</td>
																		<td style='vertical-align:middle;padding-top:20px;'>
																			<input type='text' size='80' class='editbox' name='new_mail' id='new_mail'>
																		</td>
																	</tr>
																</table>
															</td>
															<td>
																<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-right:50px">
																	<tr>
																		<td style='text-align:right'>
																			<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
																		</td>
																		<td>
																			<input type='text' class='editbox' name='new_tel_fix1' id='new_tel_fix1'><label class='label'>(perso)</label>
																		</td>	
																	</tr>
																	<tr>
																		<td style='text-align:right'>
																			<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
																		</td>
																		<td style='padding-bottom:8px;'>
																			<input type='text' class='editbox' name='new_tel_fix2' id='new_tel_fix2'><label class='label'>(pro)</label>
																		</td>	
																	</tr>
																	<tr>
																		<td style='text-align:right'>
																			<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
																		</td>
																		<td>
																			<input type='text' class='editbox' name='new_tel_portable1' id='new_tel_portable1'><label class='label'>(perso)</label>
																		</td>	
																	</tr>
																	<tr>
																		<td style='text-align:right'>
																			<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
																		</td>
																		<td style='padding-bottom:8px;'>
																			<input type='text' class='editbox' name='new_tel_portable2' id='new_tel_portable2'><label class='label'>(pro)</label>
																		</td>	
																	</tr>
																	<tr>
																		<td style='text-align:right'>
																			<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
																		</td>
																		<td>
																			<input type='text' class='editbox' name='new_tel_fax' id='new_tel_fax'>
																		</td>	
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td style='text-align:center' colspan='2'>		
																<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="width: 90%;padding-top:0px;border:0px;">
																	<tr>
																		<td style='text-align: right; vertical-align: top;'>
																			<label class='label'>Remarques :</label>
																		</td>
																		<td>
																			<textarea onkeyup="" onchange="" id='new_comm' name='new_comm' class="label textarea_com_edit" rows='3' cols='50' style=""></textarea>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td colspan='2' class="save">
												<input type='submit' name='valid_form' id='valid_form' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
											</td>
										</tr>
									</table>
								</form>
								<?php } ?>
							</div> <?php
						}
					If ($mode=='edit') {
						$vget_new_structure = $vget . "do:new_structure;";
						$vget_crypt_new_structure = base64_encode($vget_new_structure);
						$lien_new_structure = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], "/")) . "/contact_select.php?d=$vget_crypt#$structure_id";
				?>
				<div id='new'>
					<form onsubmit="return valide_form('frm_ajout_structure')" name="frm_ajout_structure" action='<?php echo $lien_new_structure; ?>' id='frm_ajout_structure' method='POST'>
						<input type='hidden' name='individu_id' id='individu_id' value='<?php echo $individu_id; ?>'>
						<table width = '100%' CELLSPACING = '10' CELLPADDING ='10' style='border:0px solid black' align='CENTER'>
							<tr>
								<td colspan='2'>
									<table width = '100%' align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:10px;border:0px">
										<tr>
											<td style='padding-bottom:30px;text-align:center'>
												<label class='label'>Structure :</label>
												<label class="btn_combobox">
													<select name="need_lst_structure" class="combobox" id="need_lst_structure">
														<option value='-1'>Structure</option>
														<?php
														$rq_lst_structure = "SELECT DISTINCT structure_id, structure_lib FROM personnes.d_structures WHERE structure_id NOT IN (SELECT DISTINCT structure_id FROM personnes.personne WHERE individu_id = '$individu_id') ORDER BY structure_lib";
														$res_lst_structure = $bdd->prepare($rq_lst_structure);
														$res_lst_structure->execute();
														While ($donnees = $res_lst_structure->fetch()) {
															echo "<option value='$donnees[structure_id]'>$donnees[structure_lib]</option>";
														}
														$res_lst_structure->closeCursor();
														?>
													</select>
												</label>
												<?php If ($verif_role_acces == 'OUI') {?>
												<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour ajouter une structure &agrave; la liste" onclick="addSpanValue_js('onglet', 'need_lst_structure', 'personnes.d_structures', 'structure_id', 'structure_lib', 'Structure', 'Nom de la structure', '<?php echo " WHERE structure_id NOT IN (SELECT DISTINCT structure_id FROM personnes.personne WHERE individu_id = $individu_id) "; ?>');">
												<?php } ?>
											</td>
										</tr>
										<tr>
											<td style='text-align:center;white-space:nowrap;'>
												<label class="btn_combobox">
													<select name="need_lst_particules" id="need_lst_particules" class="combobox">
														<option value='-1'>Civilit&eacute;</option>
														<?php
														$rq_lst_particules = "SELECT DISTINCT particules_id, particules_lib FROM personnes.d_particules ORDER BY particules_id";
														$res_lst_particules = $bdd->prepare($rq_lst_particules);
														$res_lst_particules->execute();
														While ($donnees = $res_lst_particules->fetch()) {
															echo "<option value='$donnees[particules_id]'>$donnees[particules_lib]</option>";
														}
														$res_lst_particules->closeCursor();
														?>
													</select>
												</label>									
												<label class='label'><?php echo $prenom; ?></label>
												<label class='label'><?php echo $nom; ?></label>		
											</td>
										</tr>
										<tr>
											<td style='padding-top:20px;text-align:center'>
												<label class='label'>Fonction :</label>
												<label class="btn_combobox">
													<select name="need_lst_fonctpers" class="combobox" id="need_lst_fonctpers">
														<option value='-1'>Fonction</option>
														<?php
														$rq_lst_fonctpers = "SELECT DISTINCT fonctpers_id, fonctpers_lib FROM personnes.d_fonctpers ORDER BY fonctpers_lib";
														$res_lst_fonctpers = $bdd->prepare($rq_lst_fonctpers);
														$res_lst_fonctpers->execute();
														While ($donnees = $res_lst_fonctpers->fetch()) {
															echo "<option value='$donnees[fonctpers_id]'>$donnees[fonctpers_lib]</option>";
														}
														$res_lst_fonctpers->closeCursor();
														?>
													</select>
												</label>
												<?php If ($verif_role_acces == 'OUI') {?>
												<img width='11px' style="cursor:pointer;" src="<?php echo ROOT_PATH; ?>images/ajouter.png" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Cliquez pour ajouter une fonction &agrave; la liste" onclick="addSpanValue_js('onglet', 'need_lst_fonctpers', 'personnes.d_fonctpers', 'fonctpers_id', 'fonctpers_lib', 'Fonction', 'Nom de la fonction', '');">
												<?php } ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-left:50px">
										<tr>
											<td colspan='2'>
												<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px">
													<tr>
														<td rowspan='3' style="vertical-align:top;">
															<img src='<?php echo ROOT_PATH; ?>images/adresse.gif' width='20px'>
														</td>
														<td>
															<input type='text' size='80' class='editbox' name='new_adresse1' id='new_adresse1' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Lieu-dit / B&acirc;timent">
														</td>
													</tr>
													<tr>
														<td style='text-align:right;'>
															<input type='text' size='80' class='editbox' name='new_adresse2' id='new_adresse2' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="N&deg;, Rue, Boite postale">
														</td>	
													</tr>
													<tr>
														<td style='text-align:right;'>
															<input type='text' size='80' class='editbox' name='new_adresse3' id='new_adresse3' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Code postal & Ville">
														</td>	
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td style='vertical-align:bottom;'>
												<img src='<?php echo ROOT_PATH; ?>images/mail.png' width='20px'>
											</td>
											<td style='vertical-align:middle;padding-top:20px;'>
												<input type='text' size='80' class='editbox' name='new_mail' id='new_mail'>
											</td>
										</tr>
									</table>
								</td>
								<td>
									<table align='CENTER' CELLSPACING='0' CELLPADDING='0' style="padding-top:0px;border:0px; padding-right:50px">
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
											</td>
											<td>
												<input type='text' class='editbox' name='new_tel_fix1' id='new_tel_fix1'><label class='label'>(perso)</label>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_fix.gif' width='20px'>
											</td>
											<td style='padding-bottom:8px;'>
												<input type='text' class='editbox' name='new_tel_fix2' id='new_tel_fix2'><label class='label'>(pro)</label>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
											</td>
											<td>
												<input type='text' class='editbox' name='new_tel_portable1' id='new_tel_portable1'><label class='label'>(perso)</label>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/tel_portable.png' width='12px'>
											</td>
											<td style='padding-bottom:8px;'>
												<input type='text' class='editbox' name='new_tel_portable2' id='new_tel_portable2'><label class='label'>(pro)</label>
											</td>	
										</tr>
										<tr>
											<td style='text-align:right'>
												<img src='<?php echo ROOT_PATH; ?>images/fax.png' width='20px'>
											</td>
											<td>
												<input type='text' class='editbox' name='new_tel_fax' id='new_tel_fax'>
											</td>	
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan='2' class="save">
									<input type='submit' name='valid_form' id='valid_form' onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="Enregistrer">
								</td>
							</tr>
						</table>
					</form>
				</div>
				<?php } ?>
			</div>
			<div class='lib_alerte' id='prob_saisie' style='display:none; margin-top:25px'>Veuillez compl&eacuteter votre saisie</div>
			<div class='explorateur_span' id='explorateur' style='display:none'></div>
		</section>
		<?php 
			$bdd = null;
			include("../includes/footer.php"); 
		?>
	</body>
</html>
