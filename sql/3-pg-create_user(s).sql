﻿
/*! Côté PostgreSQL - dans la base de données créée au préalable  */


/* Création d'un "super" utilisateur : */

CREATE ROLE remi_clement LOGIN
  ENCRYPTED PASSWORD 'adm1n*CEN'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT cen_user TO remi_clement;
GRANT grp_sig TO remi_clement;

WITH t1 AS (
	INSERT INTO personnes.d_individu (nom, prenom) VALUES (upper('Clément'), 'Rémi') RETURNING individu_id
)
INSERT INTO admin_sig.utilisateurs(utilisateur_id, individu_id, mdp_cen, mdp_ovh, mail)
	SELECT 'remi_clement', t1.individu_id, encode('adm1n*CEN', 'base64'), encode('adm1n*CEN', 'base64'), 'remi.clement@espaces-naturels.fr' FROM t1;

WITH t1 AS (
	SELECT oid, rolname FROM pg_roles
)
UPDATE admin_sig.utilisateurs SET oid = t1.oid FROM t1 WHERE utilisateur_id = t1.rolname; --> ne pas oublier de lancer cette requête après la création d'un utilisateur, elle peut être exécutée qu'une seule fois, à la fin de la création de l'ensemble des utilisateurs.


/* Création d'un utilisateur foncier en sql : */

CREATE ROLE nicolas_mignot LOGIN
  ENCRYPTED PASSWORD 'mdp'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT cen_user TO nicolas_mignot;
GRANT sgrp_foncier_all TO nicolas_mignot; --> seule ligne à modifier !

WITH t1 AS (
	INSERT INTO personnes.d_individu (nom, prenom) VALUES (upper('Mignot'), 'Nicolas') RETURNING individu_id
)
INSERT INTO admin_sig.utilisateurs(utilisateur_id, individu_id, mdp_cen, mdp_ovh, mail)
	SELECT 'nicolas_mignot', t1.individu_id, encode('mdp', 'base64'), encode('mdp', 'base64'), 'n.mignot@cen-savoie.org' FROM t1;

WITH t1 AS (
	SELECT oid, rolname FROM pg_roles
)
UPDATE admin_sig.utilisateurs SET oid = t1.oid FROM t1 WHERE utilisateur_id = t1.rolname; --> ne pas oublier de lancer cette requête après la création d'un utilisateur, elle peut être exécutée qu'une seule fois, à la fin de la création de l'ensemble des utilisateurs.


/* Création d'un utilisateur foncier via l'interface : */

--> vous pourrez configurer le fichier configuration.php pour activer cette fonctionnalité, un compte "super" utilisateur dans le grp_sig pourra alors créer des utilisateurs à l'aide d'une interface.
