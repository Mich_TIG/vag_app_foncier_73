﻿
/*! Côté PostgreSQL */


/* Création de la base de données : */

CREATE DATABASE bd_cen_savoie --> nom de la base de données à bien respecter par la suite (cf. 2-pg-install_foncier.sql + cf. configuration.php)
  WITH OWNER = postgres --> le propriétaire sera modifié juste après (cf. 2-pg-install_foncier.sql)
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'fr_FR.UTF-8' --> 'fr_FR.UTF-8' (Ubuntu) ou 'French_France.1252' (Windows)
       LC_CTYPE = 'fr_FR.UTF-8' --> 'fr_FR.UTF-8' (Ubuntu) ou 'French_France.1252' (Windows)
       CONNECTION LIMIT = -1;
