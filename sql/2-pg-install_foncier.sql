﻿
/*! Côté PostgreSQL - dans la base de données créée au préalable */


/* Création des rôles spécifiques de la base foncière : */

CREATE ROLE grp_sig
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
CREATE ROLE cen_user
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
CREATE ROLE grp_foncier
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT cen_user TO grp_foncier;
CREATE ROLE sgrp_foncier_all
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT grp_foncier TO sgrp_foncier_all;
CREATE ROLE sgrp_foncier_base
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT grp_foncier TO sgrp_foncier_base;
CREATE ROLE sgrp_foncier_suivi
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT grp_foncier TO sgrp_foncier_suivi;
CREATE ROLE grp_administratif
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT cen_user TO grp_administratif;

CREATE ROLE first_cnx LOGIN
  ENCRYPTED PASSWORD 'first_cnx.com' --> paramétrable
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
CREATE ROLE foncier LOGIN
  ENCRYPTED PASSWORD 'foncier.com' --> paramétrable
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT cen_user TO foncier;

ALTER DATABASE bd_cen_savoie OWNER TO grp_sig; --> attention, nom de la base de données créée au préalable (cf. 1-pg-create_database.sql)


/* Création des extensions : */

CREATE EXTENSION postgis; --> obligatoire pour le foncier


/* Schéma admin_sig : */

CREATE SCHEMA admin_sig
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA admin_sig TO first_cnx;
GRANT USAGE ON SCHEMA admin_sig TO cen_user;

-- Table: admin_sig.utilisateurs
DROP TABLE IF EXISTS admin_sig.utilisateurs;
CREATE TABLE IF NOT EXISTS admin_sig.utilisateurs
(
  utilisateur_id character varying(25) NOT NULL,
  individu_id integer,
  mdp_cen character varying(25),
  mdp_ovh character varying(25),
  mail character varying(50),
  oid oid,
  conn_date text[],
  CONSTRAINT utilisateurs_pkey PRIMARY KEY (utilisateur_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE admin_sig.utilisateurs
  OWNER TO grp_sig;
GRANT SELECT ON TABLE admin_sig.utilisateurs TO cen_user;
GRANT SELECT ON TABLE admin_sig.utilisateurs TO first_cnx;
GRANT UPDATE(conn_date) ON admin_sig.utilisateurs TO first_cnx;

-- Table: admin_sig.blocage_ip
DROP SEQUENCE IF EXISTS admin_sig.blocage_ip_id_seq;
CREATE SEQUENCE admin_sig.blocage_ip_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE admin_sig.blocage_ip_id_seq
  OWNER TO grp_sig;
SELECT setval('admin_sig.blocage_ip_id_seq', 0, true);
GRANT USAGE ON SEQUENCE admin_sig.blocage_ip_id_seq TO first_cnx;

DROP TABLE IF EXISTS admin_sig.blocage_ip;
CREATE TABLE IF NOT EXISTS admin_sig.blocage_ip
(
  id integer NOT NULL DEFAULT nextval('admin_sig.blocage_ip_id_seq'::regclass),
  ip character varying(15) NOT NULL,
  CONSTRAINT blocage_ip_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE admin_sig.blocage_ip
  OWNER TO grp_sig;
GRANT SELECT, INSERT ON TABLE admin_sig.blocage_ip TO first_cnx;

-- Function: admin_sig.f_special_round(numeric, integer)
DROP FUNCTION IF EXISTS admin_sig.f_special_round(numeric, integer);
CREATE OR REPLACE FUNCTION admin_sig.f_special_round(
    p_to_round numeric,
    p_nbre_decimal integer)
  RETURNS numeric AS
$BODY$
DECLARE
	p_return decimal;
BEGIN
	IF(round(p_to_round,p_nbre_decimal)<p_to_round) THEN
		p_to_round := p_to_round + 9 /10^(p_nbre_decimal+1);
	END IF;
	p_return := round(p_to_round,p_nbre_decimal);
	RETURN p_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admin_sig.f_special_round(numeric, integer)
  OWNER TO grp_sig;

-- Function: admin_sig.f_sites_communes(character varying)
DROP FUNCTION IF EXISTS admin_sig.f_sites_communes(character varying);
CREATE OR REPLACE FUNCTION admin_sig.f_sites_communes(var_site_id character varying)
  RETURNS text AS
$BODY$
DECLARE
    
BEGIN
RETURN COALESCE(array_to_string(array_agg(t1.nomcomm ORDER BY t1.nomcomm), ' / '::text), '') as site_nomcomm FROM (
	SELECT DISTINCT upper(nom) as nomcomm
	FROM administratif.commune JOIN cadastre.parcelles_cen ON (parcelles_cen.codcom = commune.code_insee) JOIN cadastre.lots_cen USING (par_id) JOIN cadastre.cadastre_cen USING (lot_id) JOIN foncier.cadastre_site USING (cad_cen_id)
	WHERE site_id = var_site_id AND cadastre_cen.date_fin = 'N.D.' AND cadastre_site.date_fin = 'N.D.'
	) t1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admin_sig.f_sites_communes(character varying)
  OWNER TO grp_sig;


/* Schéma administratif : */

CREATE SCHEMA administratif
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA administratif TO cen_user;

-- Table: administratif.communes
DROP TABLE IF EXISTS administratif.communes;
CREATE TABLE IF NOT EXISTS administratif.communes
(
  id character varying(24),
  prec_plani double precision,
  nom character varying(45),
  code_insee character varying(5) NOT NULL,
  statut character varying(20),
  canton character varying(45),
  arrondisst character varying(45),
  depart character varying(30),
  region character varying(30),
  popul integer,
  multican character varying(3),
  actif boolean,
  epfl character varying(10),
  geom geometry(MultiPolygon,2154),
  CONSTRAINT communes_pkey PRIMARY KEY (code_insee)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administratif.communes
  OWNER TO grp_sig;
GRANT SELECT ON TABLE administratif.communes TO cen_user;

CREATE INDEX communes_geom_gist
  ON administratif.communes
  USING gist
  (geom);

-- Table: administratif.r_histo_com
DROP TABLE IF EXISTS administratif.r_histo_com;
CREATE TABLE administratif.r_histo_com
(
  code_insee_new character varying(5) NOT NULL,
  code_insee_old character varying(5) NOT NULL,
  type_com character varying(10) NOT NULL,
  date_maj character varying(19) NOT NULL,
  nom_old character varying(45),
  geom_old geometry(MultiPolygon,2154),
  CONSTRAINT r_histo_com_pkey PRIMARY KEY (code_insee_new, code_insee_old, date_maj),
  CONSTRAINT r_histo_com_code_insee_new_fk FOREIGN KEY (code_insee_new)
      REFERENCES administratif.communes (code_insee) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_histo_com_code_insee_old_fk FOREIGN KEY (code_insee_old)
      REFERENCES administratif.communes (code_insee) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administratif.r_histo_com
  OWNER TO grp_sig;
GRANT SELECT ON TABLE administratif.r_histo_com TO cen_user;

CREATE INDEX r_histo_com_geom_gist
  ON administratif.r_histo_com
  USING gist
  (geom_old);
  
-- View: administratif.v_communes
DROP VIEW IF EXISTS administratif.v_communes;
CREATE OR REPLACE VIEW administratif.v_communes AS 
 SELECT communes.id,
    communes.prec_plani,
    communes.nom,
    communes.code_insee,
    communes.statut,
    communes.canton,
    communes.arrondisst,
    communes.depart,
    communes.region,
    communes.popul,
    communes.multican,
    communes.epfl,
    communes.geom
   FROM administratif.communes
  WHERE communes.actif = true;
ALTER TABLE administratif.v_communes
  OWNER TO grp_sig;
GRANT SELECT ON TABLE administratif.v_communes TO cen_user;

-- View: administratif.v_communes_deleguees
DROP VIEW IF EXISTS administratif.v_communes_deleguees;
CREATE OR REPLACE VIEW administratif.v_communes_deleguees AS 
 SELECT communes.id,
    r_histo_com.nom_old AS nom,
    r_histo_com.code_insee_old AS code_insee,
    r_histo_com.geom_old AS geom
   FROM administratif.r_histo_com
     JOIN administratif.communes ON communes.code_insee::text = r_histo_com.code_insee_old::text
  WHERE r_histo_com.type_com::text = 'déléguée'::text;

ALTER TABLE administratif.v_communes_deleguees
  OWNER TO grp_sig;
GRANT SELECT ON TABLE administratif.v_communes_deleguees TO cen_user;


/* Schéma territoires : */

CREATE SCHEMA territoires
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA territoires TO cen_user;

-- Table: territoires.d_typterritoire
DROP TABLE IF EXISTS territoires.d_typterritoire;
CREATE TABLE IF NOT EXISTS territoires.d_typterritoire
(
  typterritoire_id character varying(10) NOT NULL,
  typterritoire_lib character varying(150) NOT NULL,
  collectivites boolean NOT NULL,
  CONSTRAINT d_typterritoire_pk PRIMARY KEY (typterritoire_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE territoires.d_typterritoire
  OWNER TO grp_sig;
GRANT SELECT ON TABLE territoires.d_typterritoire TO cen_user;

INSERT INTO territoires.d_typterritoire VALUES ('sixte', 'Syndicat Mixte', false);
INSERT INTO territoires.d_typterritoire VALUES ('comagglo', 'Communauté d''Agglomération', true);
INSERT INTO territoires.d_typterritoire VALUES ('sivom', 'Syndicat Intercommunal à Vocation Multiple', false);
INSERT INTO territoires.d_typterritoire VALUES ('sivu', 'Syndicat Intercommunal à Vocation Unique', false);
INSERT INTO territoires.d_typterritoire VALUES ('terrcg', 'Territoire de Savoie', false);
INSERT INTO territoires.d_typterritoire VALUES ('metro', 'Métropole', true);
INSERT INTO territoires.d_typterritoire VALUES ('pnr', 'Parc Naturel Régional', false);
INSERT INTO territoires.d_typterritoire VALUES ('paysagri', 'Pays agricole', false);
INSERT INTO territoires.d_typterritoire VALUES ('bv', 'Bassin versant', false);
INSERT INTO territoires.d_typterritoire VALUES ('comcom', 'Communauté de Communes', true);
INSERT INTO territoires.d_typterritoire VALUES ('comurb', 'Communauté Urbaine', true);
INSERT INTO territoires.d_typterritoire VALUES ('terrpro', 'Territoire de projet', false);

-- Table: territoires.territoires
DROP SEQUENCE IF EXISTS territoires.territoires_territoire_id_seq;
CREATE SEQUENCE territoires.territoires_territoire_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE territoires.territoires_territoire_id_seq
  OWNER TO grp_sig;
SELECT setval('territoires.territoires_territoire_id_seq', 0, true);

DROP TABLE IF EXISTS territoires.territoires;
CREATE TABLE IF NOT EXISTS territoires.territoires
(
  territoire_id integer NOT NULL DEFAULT nextval('territoires.territoires_territoire_id_seq'::regclass),
  siren character varying(10) NOT NULL,
  territoire_lib character varying(150) NOT NULL,
  territoire_sigle character varying(50),
  typterritoire_id character varying(10),
  administratif boolean,
  prefixe character varying(50),
  geom geometry(MultiPolygon,2154),
  CONSTRAINT territoires_pk PRIMARY KEY (territoire_id),
  CONSTRAINT territoires_d_typterritoire_fk FOREIGN KEY (typterritoire_id)
      REFERENCES territoires.d_typterritoire (typterritoire_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE territoires.territoires
  OWNER TO grp_sig;
GRANT SELECT ON TABLE territoires.territoires TO cen_user;

CREATE INDEX fki_territoires_d_typterritoire_fk
  ON territoires.territoires
  USING btree
  (typterritoire_id COLLATE pg_catalog."default");
CREATE INDEX territoires_geom_gist
  ON territoires.territoires
  USING gist
  (geom);

-- Table: territoires.r_ter_com
DROP TABLE IF EXISTS territoires.r_ter_com;
CREATE TABLE IF NOT EXISTS territoires.r_ter_com
(
  code_insee character varying(5) NOT NULL,
  territoire_id bigint NOT NULL,
  actif boolean NOT NULL,
  utilisateur_id character varying(20),
  date_maj character varying(19) NOT NULL,
  CONSTRAINT r_ter_com_pk PRIMARY KEY (code_insee, territoire_id, actif, date_maj),
  CONSTRAINT r_ter_com_communes_fk FOREIGN KEY (code_insee)
      REFERENCES administratif.communes (code_insee) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_ter_com_territoires_fk FOREIGN KEY (territoire_id)
      REFERENCES territoires.territoires (territoire_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE territoires.r_ter_com
  OWNER TO grp_sig;
GRANT SELECT ON TABLE territoires.r_ter_com TO cen_user;

CREATE INDEX fki_r_ter_com_communes_fk
  ON territoires.r_ter_com
  USING btree
  (code_insee COLLATE pg_catalog."default");
CREATE INDEX fki_r_ter_com_territoires_fk
  ON territoires.r_ter_com
  USING btree
  (territoire_id);

-- View: territoires.v_collectivites
DROP VIEW IF EXISTS territoires.v_collectivites;
CREATE OR REPLACE VIEW territoires.v_collectivites AS 
 SELECT DISTINCT territoires.territoire_id,
    territoires.siren,
    territoires.territoire_lib,
    d_typterritoire.typterritoire_lib,
    r_ter_com.actif,
    territoires.geom
   FROM territoires.territoires
     JOIN territoires.d_typterritoire USING (typterritoire_id)
     JOIN territoires.r_ter_com USING (territoire_id)
     JOIN ( SELECT r_ter_com_1.territoire_id,
            max(r_ter_com_1.date_maj::text) AS max_date_maj,
            r_ter_com_1.code_insee
           FROM territoires.r_ter_com r_ter_com_1
          GROUP BY r_ter_com_1.territoire_id, r_ter_com_1.code_insee
          ORDER BY r_ter_com_1.territoire_id) tmax ON r_ter_com.date_maj::text = tmax.max_date_maj AND r_ter_com.territoire_id = tmax.territoire_id AND r_ter_com.code_insee::text = tmax.code_insee::text
  WHERE r_ter_com.actif = true AND d_typterritoire.collectivites = true
  ORDER BY territoires.territoire_lib;
ALTER TABLE territoires.v_collectivites
  OWNER TO grp_sig;
GRANT SELECT ON TABLE territoires.v_collectivites TO cen_user;

-- View: territoires.v_territoires
DROP VIEW IF EXISTS territoires.v_territoires;
CREATE OR REPLACE VIEW territoires.v_territoires AS 
 SELECT DISTINCT territoires.territoire_id,
    territoires.siren,
    territoires.territoire_lib,
    d_typterritoire.typterritoire_lib,
    r_ter_com.actif,
    territoires.geom
   FROM territoires.territoires
     JOIN territoires.d_typterritoire USING (typterritoire_id)
     JOIN territoires.r_ter_com USING (territoire_id)
     JOIN ( SELECT r_ter_com_1.territoire_id,
            max(r_ter_com_1.date_maj::text) AS max_date_maj,
            r_ter_com_1.code_insee
           FROM territoires.r_ter_com r_ter_com_1
          GROUP BY r_ter_com_1.territoire_id, r_ter_com_1.code_insee
          ORDER BY r_ter_com_1.territoire_id) tmax ON r_ter_com.date_maj::text = tmax.max_date_maj AND r_ter_com.territoire_id = tmax.territoire_id AND r_ter_com.code_insee::text = tmax.code_insee::text
  WHERE r_ter_com.actif = true
  ORDER BY territoires.territoire_lib;
ALTER TABLE territoires.v_territoires
  OWNER TO grp_sig;
GRANT SELECT ON TABLE territoires.v_territoires TO cen_user;


/* Schéma personnes */

CREATE SCHEMA personnes
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA personnes TO first_cnx;
GRANT USAGE ON SCHEMA personnes TO cen_user;

-- Table: personnes.d_fonctpers
DROP SEQUENCE IF EXISTS personnes.d_fonctpers_gid_seq;
CREATE SEQUENCE personnes.d_fonctpers_gid_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE personnes.d_fonctpers_gid_seq
  OWNER TO grp_sig;
SELECT setval('personnes.d_fonctpers_gid_seq', 0, true);
GRANT ALL ON SEQUENCE personnes.d_fonctpers_gid_seq TO grp_foncier;
GRANT ALL ON SEQUENCE personnes.d_fonctpers_gid_seq TO grp_administratif;

DROP TABLE IF EXISTS personnes.d_fonctpers;
CREATE TABLE IF NOT EXISTS personnes.d_fonctpers
(
  fonctpers_id integer NOT NULL DEFAULT nextval('personnes.d_fonctpers_gid_seq'::regclass),
  fonctpers_lib character varying(100),
  CONSTRAINT d_fonctpers_pkey PRIMARY KEY (fonctpers_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personnes.d_fonctpers
  OWNER TO grp_sig;
GRANT SELECT ON TABLE personnes.d_fonctpers TO cen_user;
GRANT UPDATE, INSERT ON TABLE personnes.d_fonctpers TO grp_foncier;
GRANT UPDATE, INSERT ON TABLE personnes.d_fonctpers TO grp_administratif;

INSERT INTO personnes.d_fonctpers VALUES (1, 'Géomaticien');
INSERT INTO personnes.d_fonctpers VALUES (33, 'Notaire');
INSERT INTO personnes.d_fonctpers VALUES (2, 'Responsable SIG');
INSERT INTO personnes.d_fonctpers VALUES (4, 'Responsable Foncier');
INSERT INTO personnes.d_fonctpers VALUES (5, 'Responsable Administratif et financier');
INSERT INTO personnes.d_fonctpers VALUES (6, 'Scientifique');
INSERT INTO personnes.d_fonctpers VALUES (7, 'Technicien');
INSERT INTO personnes.d_fonctpers VALUES (24, 'Responsable scientifique');
INSERT INTO personnes.d_fonctpers VALUES (25, 'Responsable travaux');
INSERT INTO personnes.d_fonctpers VALUES (26, 'Animatrice territoriale');
INSERT INTO personnes.d_fonctpers VALUES (27, 'Technicienne travaux');
INSERT INTO personnes.d_fonctpers VALUES (28, 'Technicien travaux');
INSERT INTO personnes.d_fonctpers VALUES (30, 'Chargé de communication');
INSERT INTO personnes.d_fonctpers VALUES (31, 'Directeur');
INSERT INTO personnes.d_fonctpers VALUES (32, 'Secrétaire');
INSERT INTO personnes.d_fonctpers VALUES (29, 'Responsable Communication');
INSERT INTO personnes.d_fonctpers VALUES (10, 'Stagiaire');
INSERT INTO personnes.d_fonctpers VALUES (34, 'Directeur délégué');
SELECT setval('personnes.d_fonctpers_gid_seq', (SELECT max(d_fonctpers.fonctpers_id) FROM personnes.d_fonctpers), true);

-- Table: personnes.d_individu
DROP SEQUENCE IF EXISTS personnes.d_individu_gid_seq;
CREATE SEQUENCE personnes.d_individu_gid_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE personnes.d_individu_gid_seq
  OWNER TO grp_sig;
SELECT setval('personnes.d_individu_gid_seq', 0, true);
GRANT ALL ON SEQUENCE personnes.d_individu_gid_seq TO grp_foncier;
GRANT ALL ON SEQUENCE personnes.d_individu_gid_seq TO grp_administratif;

DROP TABLE IF EXISTS personnes.d_individu;
CREATE TABLE IF NOT EXISTS personnes.d_individu
(
  individu_id integer NOT NULL DEFAULT nextval('personnes.d_individu_gid_seq'::regclass),
  nom character varying(50),
  prenom character varying(50),
  CONSTRAINT d_individu_pkey PRIMARY KEY (individu_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personnes.d_individu
  OWNER TO grp_sig;
GRANT SELECT ON TABLE personnes.d_individu TO first_cnx;
GRANT SELECT ON TABLE personnes.d_individu TO cen_user;
GRANT UPDATE, INSERT ON TABLE personnes.d_individu TO grp_foncier;
GRANT UPDATE, INSERT ON TABLE personnes.d_individu TO grp_administratif;

-- Table: personnes.d_particules
DROP TABLE IF EXISTS personnes.d_particules;
CREATE TABLE IF NOT EXISTS personnes.d_particules
(
  particules_id integer NOT NULL,
  particules_lib character varying(100),
  particules_ini character varying(10),
  CONSTRAINT d_particules_pkey PRIMARY KEY (particules_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personnes.d_particules
  OWNER TO grp_sig;
GRANT SELECT ON TABLE personnes.d_particules TO cen_user;

INSERT INTO personnes.d_particules VALUES (4, 'Professeur', 'Pr');
INSERT INTO personnes.d_particules VALUES (5, 'Docteur', 'Dr');
INSERT INTO personnes.d_particules VALUES (1, 'Madame', 'Mme');
INSERT INTO personnes.d_particules VALUES (6, 'Maître', 'Me');
INSERT INTO personnes.d_particules VALUES (3, 'Mademoiselle', 'Mlle');
INSERT INTO personnes.d_particules VALUES (2, 'Monsieur', 'M');

-- Table: personnes.d_structures
DROP SEQUENCE IF EXISTS personnes.d_structures_gid_seq;
CREATE SEQUENCE personnes.d_structures_gid_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE personnes.d_structures_gid_seq
  OWNER TO grp_sig;
SELECT setval('personnes.d_structures_gid_seq', 0, true);
GRANT ALL ON SEQUENCE personnes.d_structures_gid_seq TO grp_foncier;
GRANT ALL ON SEQUENCE personnes.d_structures_gid_seq TO grp_administratif;

DROP TABLE IF EXISTS personnes.d_structures;
CREATE TABLE IF NOT EXISTS personnes.d_structures
(
  structure_id integer NOT NULL DEFAULT nextval('personnes.d_structures_gid_seq'::regclass),
  structure_lib character varying(100),
  structure_sigle character varying(25),
  CONSTRAINT d_structures_pkey PRIMARY KEY (structure_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personnes.d_structures
  OWNER TO grp_sig;
GRANT SELECT ON TABLE personnes.d_structures TO cen_user;
GRANT UPDATE, INSERT ON TABLE personnes.d_structures TO grp_foncier;
GRANT UPDATE, INSERT ON TABLE personnes.d_structures TO grp_administratif;

INSERT INTO personnes.d_structures VALUES (1, 'CEN Savoie', 'CEN73');
INSERT INTO personnes.d_structures VALUES (2, 'CEN Rhône-Alpes', 'CENRA');
INSERT INTO personnes.d_structures VALUES (3, 'CEN Isère', 'AVENIR');
INSERT INTO personnes.d_structures VALUES (5, 'CEN Haute-Savoie', 'ASTERS');
INSERT INTO personnes.d_structures VALUES (6, 'Chambre des notaires', 'N.P.');
INSERT INTO personnes.d_structures VALUES (7, 'CEN Auvergne', 'CENAU');
INSERT INTO personnes.d_structures VALUES (8, 'CEN Allier', 'CEN03');
SELECT setval('personnes.d_structures_gid_seq', (SELECT max(d_structures.structure_id) FROM personnes.d_structures), true);

-- Table: personnes.personne
DROP SEQUENCE IF EXISTS personnes.personne_gid_seq;
CREATE SEQUENCE personnes.personne_gid_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE personnes.personne_gid_seq
  OWNER TO grp_sig;
SELECT setval('personnes.personne_gid_seq', 0, true);
GRANT ALL ON SEQUENCE personnes.personne_gid_seq TO grp_foncier;
GRANT ALL ON SEQUENCE personnes.personne_gid_seq TO grp_administratif;

DROP TABLE IF EXISTS personnes.personne;
CREATE TABLE IF NOT EXISTS personnes.personne
(
  personne_id integer NOT NULL DEFAULT nextval('personnes.personne_gid_seq'::regclass),
  particules_id integer NOT NULL,
  fonctpers_id integer NOT NULL,
  individu_id integer NOT NULL,
  structure_id integer NOT NULL,
  email character varying(50),
  adresse1 character varying(150),
  adresse2 character varying(150),
  adresse3 character varying(150),
  fixe_domicile character varying(14),
  fixe_travail character varying(14),
  portable_domicile character varying(14),
  portable_travail character varying(14),
  fax character varying(14),
  remarques text,
  CONSTRAINT personne_pkey PRIMARY KEY (personne_id),
  CONSTRAINT personne_fonctpers_id_fk FOREIGN KEY (fonctpers_id)
      REFERENCES personnes.d_fonctpers (fonctpers_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personne_individu_id_fk FOREIGN KEY (individu_id)
      REFERENCES personnes.d_individu (individu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personne_particules_id_fk FOREIGN KEY (particules_id)
      REFERENCES personnes.d_particules (particules_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personne_structures_id_fk FOREIGN KEY (structure_id)
      REFERENCES personnes.d_structures (structure_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personnes.personne
  OWNER TO grp_sig;
GRANT SELECT ON TABLE personnes.personne TO cen_user;
GRANT SELECT ON TABLE personnes.personne TO first_cnx;
GRANT UPDATE, INSERT ON TABLE personnes.personne TO grp_foncier;
GRANT UPDATE, INSERT ON TABLE personnes.personne TO grp_administratif;


/* Schéma cadastre */

CREATE SCHEMA cadastre
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA cadastre TO cen_user;

/* Schéma foncier : dictionnaires */

CREATE SCHEMA foncier
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA foncier TO cen_user;


/* Schéma cadastre : fonctions */  

-- Function: cadastre.c_modif_fin_id_is_ok(character varying, smallint)
DROP FUNCTION IF EXISTS cadastre.c_modif_fin_id_is_ok(character varying, smallint);
CREATE OR REPLACE FUNCTION cadastre.c_modif_fin_id_is_ok(
    fin_date character varying,
    id_fin_motif smallint)
  RETURNS boolean AS
$BODY$
DECLARE
    enregistrement record;
BEGIN
	IF ((fin_date != 'N.D.') AND (id_fin_motif IS NULL)) THEN
		RAISE EXCEPTION 'Le champ motif_fin_id ne peut pas être %.', id_fin_motif;
		RETURN FALSE;
	ELSE
		RETURN TRUE;
	END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.c_modif_fin_id_is_ok(character varying, smallint)
  OWNER TO grp_sig;
COMMENT ON FUNCTION cadastre.c_modif_fin_id_is_ok(character varying, smallint) IS 'Fonction permettant de contrôler que le motif_fin est bien renseigné.';

-- Function: cadastre.f_nom_conjoint(character varying)
DROP FUNCTION IF EXISTS cadastre.f_nom_conjoint(character varying);
CREATE OR REPLACE FUNCTION cadastre.f_nom_conjoint(var_dnuper character varying)
  RETURNS text AS
$BODY$
DECLARE
    
BEGIN
RETURN t1.nom_conjoint FROM (
	SELECT 
		CASE 
			WHEN ccoqua IS NOT NULL OR gtoper = 1 THEN 
				CASE
					WHEN epxnee = 'EPX' OR epxnee = 'NEE' THEN dnomlp
					ELSE 'N.P.'
				END
			ELSE 'N.P.' 
		END as nom_conjoint
	FROM cadastre.proprios_cen
	WHERE dnuper = var_dnuper
	) t1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.f_nom_conjoint(character varying)
  OWNER TO grp_sig;

-- Function: cadastre.f_nom_jeunefille(character varying)
DROP FUNCTION IF EXISTS cadastre.f_nom_jeunefille(character varying);
CREATE OR REPLACE FUNCTION cadastre.f_nom_jeunefille(var_dnuper character varying)
  RETURNS text AS
$BODY$
DECLARE
    
BEGIN
RETURN t1.nom_jeunefille FROM (
	SELECT 
		CASE 
			WHEN (ccoqua IS NOT NULL OR gtoper = 1) THEN 
				CASE
					WHEN ccoqua = 2 OR ccoqua = 3 THEN left(trim(ddenom), strpos(ddenom,'/')-1)::text
					WHEN ccoqua IS NULL AND epxnee = 'NEE' THEN left(trim(ddenom), strpos(ddenom,'/')-1)::text
					ELSE 'N.P.'
				END
			ELSE 'N.P.' 
		END as nom_jeunefille
	FROM cadastre.proprios_cen
	WHERE dnuper = var_dnuper
	) t1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.f_nom_jeunefille(character varying)
  OWNER TO grp_sig;

-- Function: cadastre.f_nom_usage(character varying)
DROP FUNCTION IF EXISTS cadastre.f_nom_usage(character varying);
CREATE OR REPLACE FUNCTION cadastre.f_nom_usage(var_dnuper character varying)
  RETURNS text AS
$BODY$
DECLARE
    
BEGIN
RETURN t1.nom_usage FROM (
	SELECT 
		CASE 
			WHEN ccoqua IS NOT NULL OR gtoper = 1 THEN 
				CASE
					WHEN epxnee = 'EPX' OR epxnee = 'NEE' OR dnomlp IS NOT NULL THEN dnomlp
					WHEN strpos(ddenom,'/') = 0 THEN ddenom
					ELSE left(trim(ddenom), strpos(ddenom,'/')-1)::text
				END
			ELSE 'N.P.' 
		END as nom_usage
	FROM cadastre.proprios_cen
	WHERE dnuper = var_dnuper
	) t1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.f_nom_usage(character varying)
  OWNER TO grp_sig;

-- Function: cadastre.f_prenom_conjoint(character varying)
DROP FUNCTION IF EXISTS cadastre.f_prenom_conjoint(character varying);
CREATE OR REPLACE FUNCTION cadastre.f_prenom_conjoint(var_dnuper character varying)
  RETURNS text AS
$BODY$
DECLARE
    
BEGIN
RETURN t1.prenom_conjoint FROM (
	SELECT 
		CASE 
			WHEN ccoqua IS NOT NULL OR gtoper = 1 THEN 
				CASE
					WHEN epxnee = 'EPX' AND dprncp IS NOT NULL THEN dprncp
					WHEN epxnee = 'NEE' AND dprnlp IS NOT NULL THEN dprnlp
					ELSE 'N.P.'
				END
			ELSE 'N.P.' 
		END as prenom_conjoint
	FROM cadastre.proprios_cen
	WHERE dnuper = var_dnuper
	) t1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.f_prenom_conjoint(character varying)
  OWNER TO grp_sig;

-- Function: cadastre.f_prenom_usage(character varying)
DROP FUNCTION IF EXISTS cadastre.f_prenom_usage(character varying);
CREATE OR REPLACE FUNCTION cadastre.f_prenom_usage(var_dnuper character varying)
  RETURNS text AS
$BODY$
DECLARE
    
BEGIN
RETURN t1.prenom_usage FROM (
	SELECT 
		CASE 
			WHEN ccoqua IS NOT NULL OR gtoper = 1 THEN 
				CASE
					--WHEN epxnee = 'EPX' OR epxnee = 'NEE' OR dprncp IS NOT NULL THEN dprncp
					WHEN strpos(ddenom,'/') = 0 THEN 'N.D.'
					ELSE substring(trim(ddenom) from strpos(ddenom,'/')+1 for length(ddenom))::text
				END
			ELSE 'N.P.' 
		END as prenom_usage
	FROM cadastre.proprios_cen
	WHERE dnuper = var_dnuper
	) t1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.f_prenom_usage(character varying)
  OWNER TO grp_sig;
  
-- Function: cadastre.import_cptprop_cen(character varying, character varying)
DROP FUNCTION IF EXISTS cadastre.import_cptprop_cen(character varying, character varying);
CREATE OR REPLACE FUNCTION cadastre.import_cptprop_cen(
    prodnu character varying,
    matrice_annee character varying)
  RETURNS boolean AS
$BODY$
  DECLARE

	import_dnupro smallint;
	
	boucle_prop record;
	perdnu character varying(8);
	import_dnuper smallint;

BEGIN

	import_dnupro := (SELECT count(dnupro) FROM cadastre.cptprop_cen WHERE dnupro = prodnu);
	IF (import_dnupro = 0) THEN
		INSERT INTO cadastre.cptprop_cen(dnupro, annee_matrice) SELECT prodnu, matrice_annee;
		RAISE NOTICE 'dnupro importé : %', prodnu; 

		FOR boucle_prop IN SELECT proprios_73.dnuper FROM cadastre.proprios_73 JOIN cadastre.r_prop_cptprop_73 USING (dnuper) WHERE dnupro = prodnu LOOP
			perdnu := boucle_prop.dnuper;
			
			import_dnuper := (SELECT count(dnuper) FROM cadastre.proprios_cen WHERE dnuper = perdnu);
			IF (import_dnuper = 0) THEN
				INSERT INTO cadastre.proprios_cen(dnuper, ccoqua, ddenom, dnomlp, dprnlp, epxnee, dnomcp, dprncp, jdatnss, dldnss, dlign3, dlign4, dlign5, dlign6, email, fixe_domicile, fixe_travail, portable_domicile, portable_travail, fax, gtoper, ccogrm, dnatpr, dsglpm, annee_matrice, maj_user, maj_date) SELECT DISTINCT dnuper, ccoqua, ddenom, dnomlp, dprnlp, epxnee, dnomcp, dprncp, jdatnss, dldnss, dlign3, dlign4, dlign5, dlign6, Null, Null, Null, Null, Null, Null, gtoper, ccogrm, dnatpr, dsglpm, matrice_annee, current_user, date_trunc('second', now()::timestamp)::text FROM cadastre.proprios_73 JOIN cadastre.r_prop_cptprop_73 USING (dnuper) WHERE dnuper = perdnu LIMIT 1;
				RAISE NOTICE 'dnuper importé : %', perdnu; 
				UPDATE cadastre.proprios_cen SET nom_usage = cadastre.f_nom_usage(dnuper), nom_jeunefille = cadastre.f_nom_jeunefille(dnuper), prenom_usage = cadastre.f_prenom_usage(dnuper), nom_conjoint = cadastre.f_nom_conjoint(dnuper), prenom_conjoint = cadastre.f_prenom_conjoint(dnuper) WHERE dnuper = perdnu;
			END IF;	
			INSERT INTO cadastre.r_prop_cptprop_cen(dnuper, dnupro, ccodro, ccodem) SELECT dnuper, dnupro, ccodro, ccodem FROM cadastre.r_prop_cptprop_73 WHERE dnuper = perdnu AND dnupro = prodnu;
		END LOOP;
		
	ELSIF (import_dnupro = 1) THEN
		RAISE NOTICE 'Le compte de propriété % est déjà dans la table cptprop_cen', prodnu;
		--EXIT;
	END IF;

  RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.import_cptprop_cen(character varying, character varying)
  OWNER TO grp_sig;

-- Function: cadastre.import_parcelles_cen(character varying, character varying, character varying, character varying)
DROP FUNCTION IF EXISTS cadastre.import_parcelles_cen(character varying, character varying, character varying, character varying);
CREATE OR REPLACE FUNCTION cadastre.import_parcelles_cen(
    id_par character varying,
    id_site character varying,
    pci_annee character varying,
    matrice_annee character varying)
  RETURNS boolean AS
$BODY$
  DECLARE

	exist_site smallint;
  
	import_par smallint;
	import_vl smallint;

	boucle_lot record;
	id_lot character varying(21);
	maj_dcntlo smallint;

	prodnu character varying(11);
	import_dnupro smallint;

	boucle_prop record;
	perdnu character varying(8);
	import_dnuper smallint;

	id_cen_cad integer;
	id_site_cad integer;

BEGIN

	exist_site := (SELECT count(site_id) FROM sites.sites WHERE site_id = id_site);
	IF (exist_site = 1) THEN
	
		import_par := (SELECT count(par_id) FROM cadastre.parcelles_cen WHERE par_id = id_par);
		IF (import_par = 0) THEN
		
			import_vl := (SELECT count(vl_id) FROM cadastre.vl_cen WHERE vl_id = (SELECT vl_id FROM cadastre.parcelles_73 WHERE par_id = id_par));
			IF (import_vl = 0) THEN
				INSERT INTO cadastre.vl_cen(vl_id, libelle, geom) SELECT * FROM cadastre.vl_73 WHERE vl_id IN (SELECT vl_id FROM cadastre.parcelles_73 WHERE par_id = id_par);
			END IF;
			INSERT INTO cadastre.parcelles_cen(par_id, codcom, vl_id, ccopre, ccosec, dnupla, dparpi, dcntpa, typprop_id, geom, ccocomm, ccoprem, ccosecm, dnuplam, type, annee_pci) SELECT par_id, codcom, vl_id, ccopre, ccosec, dnupla, dparpi, dcntpa, typprop_id, geom, ccocomm, ccoprem, ccosecm, dnuplam, type, pci_annee FROM cadastre.parcelles_73 WHERE par_id = id_par;
			INSERT INTO cadastre.lots_cen(lot_id, par_id, dnulot, dcntlo, geom) SELECT * FROM cadastre.lots_73 WHERE par_id = id_par;
			maj_dcntlo := (SELECT count(dcntlo) FROM cadastre.lots_cen WHERE par_id = id_par);
			IF (maj_dcntlo = 0) THEN
				UPDATE cadastre.lots_cen SET dcntlo = t1.dcntpa FROM cadastre.parcelles_cen t1 WHERE t1.par_id = lots_cen.par_id AND lots_cen.par_id = id_par;
			END IF;

			FOR boucle_lot IN SELECT lot_id FROM cadastre.lots_cen WHERE par_id = id_par LOOP
			
				id_lot := boucle_lot.lot_id;
				RAISE NOTICE 'lot importé : %', id_lot; 

				INSERT INTO cadastre.lots_natcult_cen(lot_id, dsgrpf, cnatsp, dclssf, ccosub, dcntsf) SELECT lot_id, dsgrpf, cnatsp, dclssf, ccosub, dcntsf FROM cadastre.lots_natcult_73 WHERE lots_natcult_73.lot_id = id_lot;
				
				prodnu := (SELECT dnupro FROM cadastre.cadastre_73 WHERE lot_id = id_lot);
				
				import_dnupro := (SELECT count(dnupro) FROM cadastre.cptprop_cen WHERE dnupro = prodnu);
				IF (import_dnupro = 0) THEN
					INSERT INTO cadastre.cptprop_cen(dnupro, annee_matrice) SELECT prodnu, matrice_annee;
					RAISE NOTICE 'dnupro importé : %', prodnu; 

					FOR boucle_prop IN SELECT proprios_73.dnuper FROM cadastre.proprios_73 JOIN cadastre.r_prop_cptprop_73 USING (dnuper) WHERE dnupro = prodnu LOOP
						perdnu := boucle_prop.dnuper;
						
						import_dnuper := (SELECT count(dnuper) FROM cadastre.proprios_cen WHERE dnuper = perdnu);
						IF (import_dnuper = 0) THEN
							INSERT INTO cadastre.proprios_cen(dnuper, ccoqua, ddenom, dnomlp, dprnlp, epxnee, dnomcp, dprncp, jdatnss, dldnss, dlign3, dlign4, dlign5, dlign6, email, fixe_domicile, fixe_travail, portable_domicile, portable_travail, fax, gtoper, ccogrm, dnatpr, dsglpm, annee_matrice, maj_user, maj_date) SELECT DISTINCT dnuper, ccoqua, ddenom, dnomlp, dprnlp, epxnee, dnomcp, dprncp, jdatnss, dldnss, dlign3, dlign4, dlign5, dlign6, Null, Null, Null, Null, Null, Null, gtoper, ccogrm, dnatpr, dsglpm, matrice_annee, current_user, date_trunc('second', now()::timestamp)::text FROM cadastre.proprios_73 JOIN cadastre.r_prop_cptprop_73 USING (dnuper) WHERE dnuper = perdnu LIMIT 1;
							RAISE NOTICE 'dnuper importé : %', perdnu; 
							UPDATE cadastre.proprios_cen SET nom_usage = cadastre.f_nom_usage(dnuper), nom_jeunefille = cadastre.f_nom_jeunefille(dnuper), prenom_usage = cadastre.f_prenom_usage(dnuper), nom_conjoint = cadastre.f_nom_conjoint(dnuper), prenom_conjoint = cadastre.f_prenom_conjoint(dnuper) WHERE dnuper = perdnu;
						END IF;	
						INSERT INTO cadastre.r_prop_cptprop_cen(dnuper, dnupro, ccodro, ccodem) SELECT dnuper, dnupro, ccodro, ccodem FROM cadastre.r_prop_cptprop_73 WHERE dnuper = perdnu AND dnupro = prodnu;
					END LOOP;
					
				END IF;

				id_cen_cad := (SELECT nextval('cadastre.cadastre_cen_cad_cen_id_seq'));
				INSERT INTO cadastre.cadastre_cen(cad_cen_id, lot_id, dnupro, date_deb, date_fin, motif_fin_id) VALUES (id_cen_cad, id_lot, prodnu, current_date, 'N.D.', Null);
				id_site_cad := (SELECT nextval('foncier.cadastre_site_cad_site_id_seq'));
				INSERT INTO foncier.cadastre_site(cad_site_id, cad_cen_id, site_id, date_deb, date_fin, maj_user, maj_date) VALUES (id_site_cad, id_cen_cad, id_site, current_date, 'N.D.', current_user, date_trunc('second', now()::timestamp)::text);

			END LOOP;

		ELSIF (import_par = 1) THEN

			FOR boucle_lot IN SELECT lot_id FROM cadastre.lots_cen WHERE par_id = id_par LOOP

				id_lot := boucle_lot.lot_id;
				RAISE NOTICE 'lot associé : %', id_lot; 

				id_cen_cad := (SELECT cad_cen_id FROM cadastre.cadastre_cen WHERE lot_id = id_lot);
				id_site_cad := (SELECT nextval('foncier.cadastre_site_cad_site_id_seq'));
				INSERT INTO foncier.cadastre_site(cad_site_id, cad_cen_id, site_id, date_deb, date_fin, maj_user, maj_date) VALUES (id_site_cad, id_cen_cad, id_site, current_date, 'N.D.', current_user, date_trunc('second', now()::timestamp)::text);

			END LOOP;
			
		END IF;
		
	ELSIF (exist_site = 0) THEN
		RAISE EXCEPTION 'Le site % n''existe pas', id_site;
	END IF;

  RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION cadastre.import_parcelles_cen(character varying, character varying, character varying, character varying)
  OWNER TO grp_sig;


/* Schéma cadastre : dictionnaires */

-- Table: cadastre.d_ccodem
DROP TABLE IF EXISTS cadastre.d_ccodem;
CREATE TABLE IF NOT EXISTS cadastre.d_ccodem
(
  ccodem character varying(1) NOT NULL, -- Code du démembrement/indivision
  dqualp character varying(50),
  CONSTRAINT d_ccodem_pkey PRIMARY KEY (ccodem)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_ccodem
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_ccodem TO cen_user;
COMMENT ON TABLE cadastre.d_ccodem
  IS 'Code du démembrement/indivision';
COMMENT ON COLUMN cadastre.d_ccodem.ccodem IS 'Code du démembrement/indivision';

INSERT INTO cadastre.d_ccodem VALUES ('C', 'Un des copropriétaires');
INSERT INTO cadastre.d_ccodem VALUES ('I', 'Indivision simple');
INSERT INTO cadastre.d_ccodem VALUES ('V', 'La veuve ou les héritiers de');
INSERT INTO cadastre.d_ccodem VALUES ('L', 'Propriété en litige');
INSERT INTO cadastre.d_ccodem VALUES ('S', 'Succession de');

-- Table: cadastre.d_ccodro
DROP TABLE IF EXISTS cadastre.d_ccodro;
CREATE TABLE IF NOT EXISTS cadastre.d_ccodro
(
  ccodro character varying(1) NOT NULL, -- Code du droit réel ou particulier
  droit_lib character varying(100),
  CONSTRAINT d_ccodro_pkey PRIMARY KEY (ccodro)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_ccodro
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_ccodro TO cen_user;
COMMENT ON TABLE cadastre.d_ccodro
  IS 'Code du droit réel ou particulier';
COMMENT ON COLUMN cadastre.d_ccodro.ccodro IS 'Code du droit réel ou particulier';

INSERT INTO cadastre.d_ccodro VALUES ('A', 'Locataire-Attributaire (associé avec P)');
INSERT INTO cadastre.d_ccodro VALUES ('B', 'Bailleur à construction (associé avec R)');
INSERT INTO cadastre.d_ccodro VALUES ('C', 'Fiduciaire');
INSERT INTO cadastre.d_ccodro VALUES ('D', 'Domanier (associé avec F)');
INSERT INTO cadastre.d_ccodro VALUES ('E', 'Emphytéote (associé avec P)');
INSERT INTO cadastre.d_ccodro VALUES ('F', 'Foncier (associé avec D ou T)');
INSERT INTO cadastre.d_ccodro VALUES ('G', 'Gérant, mandataire, gestionnaire');
INSERT INTO cadastre.d_ccodro VALUES ('H', 'Associé dans une société en transparence fiscale (associé avec P)');
INSERT INTO cadastre.d_ccodro VALUES ('J', 'Jeune agriculteur');
INSERT INTO cadastre.d_ccodro VALUES ('K', 'Antichrésiste (associé avec P)');
INSERT INTO cadastre.d_ccodro VALUES ('L', 'Fonctionnaire logé');
INSERT INTO cadastre.d_ccodro VALUES ('M', 'Occupant d''une parcelle appartenant au département de Mayotte ou à l''État (associé avec P)');
INSERT INTO cadastre.d_ccodro VALUES ('N', 'Nu-propriétaire (associé avec U)');
INSERT INTO cadastre.d_ccodro VALUES ('O', 'Autorisation d''occupation temporaire (70 ans)');
INSERT INTO cadastre.d_ccodro VALUES ('P', 'Propriétaire');
INSERT INTO cadastre.d_ccodro VALUES ('Q', 'Gestionnaire taxé sur les bureaux (Île de France)');
INSERT INTO cadastre.d_ccodro VALUES ('R', 'Preneur à construction (associé avec B)');
INSERT INTO cadastre.d_ccodro VALUES ('S', 'Syndicat de copropriété');
INSERT INTO cadastre.d_ccodro VALUES ('T', 'Tenuyer (associé avec F)');
INSERT INTO cadastre.d_ccodro VALUES ('U', 'Usufruitier (associé avec N)');
INSERT INTO cadastre.d_ccodro VALUES ('V', 'Bailleur d''un bail à réhabilitation (associé avec W)');
INSERT INTO cadastre.d_ccodro VALUES ('W', 'Preneur d''un bail à réhabilitation (associé avec V)');
INSERT INTO cadastre.d_ccodro VALUES ('X', 'La Poste propriétaire et occupant');
INSERT INTO cadastre.d_ccodro VALUES ('Y', 'La Poste occupant - non propriétaire');

-- Table: cadastre.d_ccogrm
DROP TABLE IF EXISTS cadastre.d_ccogrm;
CREATE TABLE IF NOT EXISTS cadastre.d_ccogrm
(
  ccogrm smallint NOT NULL, -- Groupe de personne morale
  ccogrm_lib character varying(50),
  CONSTRAINT d_ccogrm_pkey PRIMARY KEY (ccogrm)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_ccogrm
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_ccogrm TO cen_user;
COMMENT ON TABLE cadastre.d_ccogrm
  IS 'Groupe de personne morale';
COMMENT ON COLUMN cadastre.d_ccogrm.ccogrm IS 'Groupe de personne morale';

INSERT INTO cadastre.d_ccogrm VALUES (0, 'Personnes morales non remarquables');
INSERT INTO cadastre.d_ccogrm VALUES (1, 'État');
INSERT INTO cadastre.d_ccogrm VALUES (2, 'Région');
INSERT INTO cadastre.d_ccogrm VALUES (3, 'Département');
INSERT INTO cadastre.d_ccogrm VALUES (4, 'Commune');
INSERT INTO cadastre.d_ccogrm VALUES (5, 'Office HLM');
INSERT INTO cadastre.d_ccogrm VALUES (6, 'Personnes morales représentant des sociétés');
INSERT INTO cadastre.d_ccogrm VALUES (7, 'Copropriétaire');
INSERT INTO cadastre.d_ccogrm VALUES (8, 'Associé');
INSERT INTO cadastre.d_ccogrm VALUES (9, 'Établissements publics ou organismes assimilés');

-- Table: cadastre.d_ccoqua
DROP TABLE IF EXISTS cadastre.d_ccoqua;
CREATE TABLE IF NOT EXISTS cadastre.d_ccoqua
(
  ccoqua smallint NOT NULL, -- Code qualité de personne physique
  dqualp character varying(3), -- Qualité abrégée
  particule_lib character varying(5),
  CONSTRAINT d_ccoqua_pkey PRIMARY KEY (ccoqua)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_ccoqua
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_ccoqua TO cen_user;
COMMENT ON TABLE cadastre.d_ccoqua
  IS 'Code qualité de personne physique';
COMMENT ON COLUMN cadastre.d_ccoqua.ccoqua IS 'Code qualité de personne physique';
COMMENT ON COLUMN cadastre.d_ccoqua.dqualp IS 'Qualité abrégée';

INSERT INTO cadastre.d_ccoqua VALUES (1, 'M', 'M.');
INSERT INTO cadastre.d_ccoqua VALUES (2, 'MME', 'Mme.');
INSERT INTO cadastre.d_ccoqua VALUES (3, 'MLE', 'Mlle.');

-- Table: cadastre.d_cgrnum
DROP TABLE IF EXISTS cadastre.d_cgrnum;
CREATE TABLE IF NOT EXISTS cadastre.d_cgrnum
(
  cgrnum character varying(2) NOT NULL, -- Groupe de nature de culture
  natcult_grp character varying(25),
  CONSTRAINT d_cgrnum_pkey PRIMARY KEY (cgrnum)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_cgrnum
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_cgrnum TO cen_user;
COMMENT ON TABLE cadastre.d_cgrnum
  IS 'Groupe de nature de culture';
COMMENT ON COLUMN cadastre.d_cgrnum.cgrnum IS 'Groupe de nature de culture';

INSERT INTO cadastre.d_cgrnum VALUES ('01', 'Terres');
INSERT INTO cadastre.d_cgrnum VALUES ('02', 'Prés');
INSERT INTO cadastre.d_cgrnum VALUES ('03', 'Vergers');
INSERT INTO cadastre.d_cgrnum VALUES ('04', 'Vignes');
INSERT INTO cadastre.d_cgrnum VALUES ('05', 'Bois');
INSERT INTO cadastre.d_cgrnum VALUES ('06', 'Landes');
INSERT INTO cadastre.d_cgrnum VALUES ('07', 'Carrières');
INSERT INTO cadastre.d_cgrnum VALUES ('08', 'Eaux');
INSERT INTO cadastre.d_cgrnum VALUES ('09', 'Jardins');
INSERT INTO cadastre.d_cgrnum VALUES ('10', 'Terrains à bâtir');
INSERT INTO cadastre.d_cgrnum VALUES ('11', 'Terrains d''agrément');
INSERT INTO cadastre.d_cgrnum VALUES ('12', 'Chemin de fer');
INSERT INTO cadastre.d_cgrnum VALUES ('13', 'Sol');

-- Table: cadastre.d_cnatsp
DROP TABLE IF EXISTS cadastre.d_cnatsp;
CREATE TABLE IF NOT EXISTS cadastre.d_cnatsp
(
  cnatsp character varying(5) NOT NULL, -- Code nature de culture spéciale
  natcult_natspe character varying(50),
  CONSTRAINT d_cnatsp_pkey PRIMARY KEY (cnatsp)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_cnatsp
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_cnatsp TO cen_user;
COMMENT ON TABLE cadastre.d_cnatsp
  IS 'Code nature de culture spéciale';
COMMENT ON COLUMN cadastre.d_cnatsp.cnatsp IS 'Code nature de culture spéciale';

INSERT INTO cadastre.d_cnatsp VALUES ('ABREU', 'Abreuvoirs');
INSERT INTO cadastre.d_cnatsp VALUES ('ABRIC', 'Abricotiers');
INSERT INTO cadastre.d_cnatsp VALUES ('ACACI', 'Acacias');
INSERT INTO cadastre.d_cnatsp VALUES ('AEROD', 'Aérodromes');
INSERT INTO cadastre.d_cnatsp VALUES ('AIRE', 'Aire ou Airial');
INSERT INTO cadastre.d_cnatsp VALUES ('ALLEE', 'Allée (no groupe)');
INSERT INTO cadastre.d_cnatsp VALUES ('ALLUV', 'Alluvions');
INSERT INTO cadastre.d_cnatsp VALUES ('AMAND', 'Amandiers');
INSERT INTO cadastre.d_cnatsp VALUES ('ARDOI', 'Ardoiseries');
INSERT INTO cadastre.d_cnatsp VALUES ('ARGIL', 'Argilière');
INSERT INTO cadastre.d_cnatsp VALUES ('ASPER', 'Aspergeraie');
INSERT INTO cadastre.d_cnatsp VALUES ('AULNE', 'Aulnaie');
INSERT INTO cadastre.d_cnatsp VALUES ('AVENU', 'Avenue');
INSERT INTO cadastre.d_cnatsp VALUES ('BALLA', 'Ballastière');
INSERT INTO cadastre.d_cnatsp VALUES ('BAMBO', 'Bambouseraie');
INSERT INTO cadastre.d_cnatsp VALUES ('BASS', 'Bassins');
INSERT INTO cadastre.d_cnatsp VALUES ('BIEF', 'Bief');
INSERT INTO cadastre.d_cnatsp VALUES ('BOUL', 'Boulaie');
INSERT INTO cadastre.d_cnatsp VALUES ('BROUS', 'Broussailles ou buissons');
INSERT INTO cadastre.d_cnatsp VALUES ('BRUY', 'Bruyères');
INSERT INTO cadastre.d_cnatsp VALUES ('BTIGE', 'Verger exploite en basses tiges');
INSERT INTO cadastre.d_cnatsp VALUES ('BUIS', 'Buissière');
INSERT INTO cadastre.d_cnatsp VALUES ('CAMP', 'Terrain de camping');
INSERT INTO cadastre.d_cnatsp VALUES ('CANAL', 'Canal');
INSERT INTO cadastre.d_cnatsp VALUES ('CASS', 'Cassis');
INSERT INTO cadastre.d_cnatsp VALUES ('CEDRA', 'Cédratiers');
INSERT INTO cadastre.d_cnatsp VALUES ('CERCL', 'Cerclières');
INSERT INTO cadastre.d_cnatsp VALUES ('CERIS', 'Cerisaie ou cerisiers');
INSERT INTO cadastre.d_cnatsp VALUES ('CHASS', 'Terrain de chasse');
INSERT INTO cadastre.d_cnatsp VALUES ('CHAT', 'Châtaigneraie');
INSERT INTO cadastre.d_cnatsp VALUES ('CHEM', 'Chemin de remembrement');
INSERT INTO cadastre.d_cnatsp VALUES ('CHENE', 'Chênes');
INSERT INTO cadastre.d_cnatsp VALUES ('CHLIE', 'Chênes-lièges');
INSERT INTO cadastre.d_cnatsp VALUES ('CHTRU', 'Chênes truffiers');
INSERT INTO cadastre.d_cnatsp VALUES ('CHVER', 'Chênes verts');
INSERT INTO cadastre.d_cnatsp VALUES ('CIDRE', 'Cidre');
INSERT INTO cadastre.d_cnatsp VALUES ('CITRO', 'Citronniers');
INSERT INTO cadastre.d_cnatsp VALUES ('CLAIR', 'Claires');
INSERT INTO cadastre.d_cnatsp VALUES ('COING', 'Cognassiers');
INSERT INTO cadastre.d_cnatsp VALUES ('COULE', 'Bois de couleur');
INSERT INTO cadastre.d_cnatsp VALUES ('CRAY', 'Crayère');
INSERT INTO cadastre.d_cnatsp VALUES ('CRESS', 'Cressonnière');
INSERT INTO cadastre.d_cnatsp VALUES ('CRYPT', 'Cryptomeria');
INSERT INTO cadastre.d_cnatsp VALUES ('DIGUE', 'Digues');
INSERT INTO cadastre.d_cnatsp VALUES ('DUNE', 'Dunes');
INSERT INTO cadastre.d_cnatsp VALUES ('EAU', 'Pièce d''eau');
INSERT INTO cadastre.d_cnatsp VALUES ('ECOLE', 'Ecole');
INSERT INTO cadastre.d_cnatsp VALUES ('EPICE', 'Epicéas');
INSERT INTO cadastre.d_cnatsp VALUES ('ESPAL', 'Verger exploite en espaliers');
INSERT INTO cadastre.d_cnatsp VALUES ('ETANG', 'Etangs');
INSERT INTO cadastre.d_cnatsp VALUES ('EUCAL', 'Eucalyptus');
INSERT INTO cadastre.d_cnatsp VALUES ('FALAI', 'Falaises');
INSERT INTO cadastre.d_cnatsp VALUES ('FAMIL', 'Verger familial');
INSERT INTO cadastre.d_cnatsp VALUES ('FER', 'Chemin de fer');
INSERT INTO cadastre.d_cnatsp VALUES ('FILAO', 'Filao');
INSERT INTO cadastre.d_cnatsp VALUES ('FLOR', 'Jardin floral');
INSERT INTO cadastre.d_cnatsp VALUES ('FONT', 'Fontaine');
INSERT INTO cadastre.d_cnatsp VALUES ('FOSSE', 'Fosse');
INSERT INTO cadastre.d_cnatsp VALUES ('FOUG', 'Fougeraie');
INSERT INTO cadastre.d_cnatsp VALUES ('FRAMB', 'Framboisiers');
INSERT INTO cadastre.d_cnatsp VALUES ('FRICH', 'Friche');
INSERT INTO cadastre.d_cnatsp VALUES ('GAREN', 'Garenne');
INSERT INTO cadastre.d_cnatsp VALUES ('GENET', 'Genets');
INSERT INTO cadastre.d_cnatsp VALUES ('GLAIS', 'Glaisière');
INSERT INTO cadastre.d_cnatsp VALUES ('GRAVE', 'Gravière');
INSERT INTO cadastre.d_cnatsp VALUES ('HAIES', 'Haies fruitières');
INSERT INTO cadastre.d_cnatsp VALUES ('HERB', 'Herbage');
INSERT INTO cadastre.d_cnatsp VALUES ('HETRE', 'Hêtres');
INSERT INTO cadastre.d_cnatsp VALUES ('HIST', 'Dépendances de monuments historiques');
INSERT INTO cadastre.d_cnatsp VALUES ('HORT', 'Jardins horticoles');
INSERT INTO cadastre.d_cnatsp VALUES ('HOUBL', 'Houblon');
INSERT INTO cadastre.d_cnatsp VALUES ('HTIGE', 'Vergers exploités en hautes tiges');
INSERT INTO cadastre.d_cnatsp VALUES ('HUITR', 'Parc à huîtres');
INSERT INTO cadastre.d_cnatsp VALUES ('IMM', 'Dépendances d''ensemble immobilier');
INSERT INTO cadastre.d_cnatsp VALUES ('IMPRO', 'Lande improductive');
INSERT INTO cadastre.d_cnatsp VALUES ('INTEN', 'Verger industriel');
INSERT INTO cadastre.d_cnatsp VALUES ('JARD', 'Jardin d’agrément');
INSERT INTO cadastre.d_cnatsp VALUES ('JETT', 'Jettins');
INSERT INTO cadastre.d_cnatsp VALUES ('JOUAL', 'Joualle');
INSERT INTO cadastre.d_cnatsp VALUES ('KIWIS', 'Kiwis');
INSERT INTO cadastre.d_cnatsp VALUES ('LAC', 'Lac');
INSERT INTO cadastre.d_cnatsp VALUES ('LAGUN', 'Lagune');
INSERT INTO cadastre.d_cnatsp VALUES ('LAVOI', 'Lavoir');
INSERT INTO cadastre.d_cnatsp VALUES ('LEGUM', 'Légumière de plein champ');
INSERT INTO cadastre.d_cnatsp VALUES ('MAQUI', 'Maquis');
INSERT INTO cadastre.d_cnatsp VALUES ('MARAI', 'Pré marais');
INSERT INTO cadastre.d_cnatsp VALUES ('MARAM', 'Jardin maraîcher aménagé');
INSERT INTO cadastre.d_cnatsp VALUES ('MARE', 'Mare');
INSERT INTO cadastre.d_cnatsp VALUES ('MAREC', 'Marécage');
INSERT INTO cadastre.d_cnatsp VALUES ('MARN', 'Marnière');
INSERT INTO cadastre.d_cnatsp VALUES ('MARNA', 'Jardin maraîcher non aménagé');
INSERT INTO cadastre.d_cnatsp VALUES ('MELEZ', 'Mélèzes');
INSERT INTO cadastre.d_cnatsp VALUES ('MOTTE', 'Mottes');
INSERT INTO cadastre.d_cnatsp VALUES ('MUR', 'Mûraies ou mûriers (vergers)');
INSERT INTO cadastre.d_cnatsp VALUES ('NATUR', 'Bois naturel');
INSERT INTO cadastre.d_cnatsp VALUES ('NOISE', 'Noiseraie ou noisetiers');
INSERT INTO cadastre.d_cnatsp VALUES ('NOYER', 'Noyeraie ou noyers');
INSERT INTO cadastre.d_cnatsp VALUES ('NPECH', 'Etang non potable');
INSERT INTO cadastre.d_cnatsp VALUES ('OLIVE', 'Olivaies ou oliviers');
INSERT INTO cadastre.d_cnatsp VALUES ('ORANG', 'Orangers (vergers)');
INSERT INTO cadastre.d_cnatsp VALUES ('ORME', 'Ormaie ou ormes');
INSERT INTO cadastre.d_cnatsp VALUES ('PACAG', 'Pacage');
INSERT INTO cadastre.d_cnatsp VALUES ('PAFEU', 'Pare-feux');
INSERT INTO cadastre.d_cnatsp VALUES ('PALMI', 'Bois palmiste');
INSERT INTO cadastre.d_cnatsp VALUES ('PARC', 'Parc');
INSERT INTO cadastre.d_cnatsp VALUES ('PASS', 'Passage (non groupe)');
INSERT INTO cadastre.d_cnatsp VALUES ('PATIS', 'Pâtis');
INSERT INTO cadastre.d_cnatsp VALUES ('PATUR', 'Pâture plantée');
INSERT INTO cadastre.d_cnatsp VALUES ('PECH', 'Etangs pêchables');
INSERT INTO cadastre.d_cnatsp VALUES ('PECHE', 'Pêchers');
INSERT INTO cadastre.d_cnatsp VALUES ('PEPIN', 'Pépinières');
INSERT INTO cadastre.d_cnatsp VALUES ('PIEDS', 'Pieds-mères (vignes)');
INSERT INTO cadastre.d_cnatsp VALUES ('PIERR', 'Pierraille, pierrier');
INSERT INTO cadastre.d_cnatsp VALUES ('PIN', 'Pins');
INSERT INTO cadastre.d_cnatsp VALUES ('PLAGE', 'Plage');
INSERT INTO cadastre.d_cnatsp VALUES ('PLATR', 'Plâtrière');
INSERT INTO cadastre.d_cnatsp VALUES ('PLVEN', 'Vergers de plein vent');
INSERT INTO cadastre.d_cnatsp VALUES ('POIRE', 'Poiriers');
INSERT INTO cadastre.d_cnatsp VALUES ('POMME', 'Pommiers');
INSERT INTO cadastre.d_cnatsp VALUES ('POTAG', 'Potagers');
INSERT INTO cadastre.d_cnatsp VALUES ('PROTE', 'Bois de protection');
INSERT INTO cadastre.d_cnatsp VALUES ('PRUNE', 'Pruniers');
INSERT INTO cadastre.d_cnatsp VALUES ('RAIS', 'Raisins de table');
INSERT INTO cadastre.d_cnatsp VALUES ('RESER', 'Réservoir');
INSERT INTO cadastre.d_cnatsp VALUES ('RESIN', 'Résineux');
INSERT INTO cadastre.d_cnatsp VALUES ('RIVAG', 'Rivage (bois de)');
INSERT INTO cadastre.d_cnatsp VALUES ('RIZ', 'Rizière');
INSERT INTO cadastre.d_cnatsp VALUES ('ROC', 'Rocs ou rochers');
INSERT INTO cadastre.d_cnatsp VALUES ('ROUI', 'Routoir ou roussoir');
INSERT INTO cadastre.d_cnatsp VALUES ('RUE', 'Rue');
INSERT INTO cadastre.d_cnatsp VALUES ('RUINE', 'Ruines');
INSERT INTO cadastre.d_cnatsp VALUES ('SABLE', 'Sablière');
INSERT INTO cadastre.d_cnatsp VALUES ('SALIN', 'Marais salant');
INSERT INTO cadastre.d_cnatsp VALUES ('SAPIN', 'Sapins ou sapinière');
INSERT INTO cadastre.d_cnatsp VALUES ('SART', 'Sartières');
INSERT INTO cadastre.d_cnatsp VALUES ('SAULE', 'Saulaie ou saussaie');
INSERT INTO cadastre.d_cnatsp VALUES ('SERRE', 'Serre');
INSERT INTO cadastre.d_cnatsp VALUES ('SOL', 'Sols');
INSERT INTO cadastre.d_cnatsp VALUES ('SOURC', 'Source');
INSERT INTO cadastre.d_cnatsp VALUES ('SPORT', 'Terrain de sport');
INSERT INTO cadastre.d_cnatsp VALUES ('TAMAR', 'Tamarin');
INSERT INTO cadastre.d_cnatsp VALUES ('TAUZ', 'Taillis tauzin');
INSERT INTO cadastre.d_cnatsp VALUES ('TERRI', 'Terrils');
INSERT INTO cadastre.d_cnatsp VALUES ('TOURB', 'Tourbière');
INSERT INTO cadastre.d_cnatsp VALUES ('TOUYA', 'Touyas');
INSERT INTO cadastre.d_cnatsp VALUES ('VADC', 'Vins d''appellation d''origine contrôlée');
INSERT INTO cadastre.d_cnatsp VALUES ('VAGUE', 'Terrain vague');
INSERT INTO cadastre.d_cnatsp VALUES ('VANIL', 'Vanille');
INSERT INTO cadastre.d_cnatsp VALUES ('VAOC', 'Vins d''appellation d''origine contrôlée');
INSERT INTO cadastre.d_cnatsp VALUES ('VCHAS', 'Chasselas');
INSERT INTO cadastre.d_cnatsp VALUES ('VDQS', 'Vins délimités de qualité supérieure');
INSERT INTO cadastre.d_cnatsp VALUES ('VIGNE', 'Vigne');
INSERT INTO cadastre.d_cnatsp VALUES ('VIVIE', 'Vivier');

-- Table: cadastre.d_dnatpr
DROP TABLE IF EXISTS cadastre.d_dnatpr;
CREATE TABLE IF NOT EXISTS cadastre.d_dnatpr
(
  dnatpr character varying(5) NOT NULL, -- Nature de la personne
  dnatpr_lib character varying(50),
  CONSTRAINT d_dnatpr_pkey PRIMARY KEY (dnatpr)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_dnatpr
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_dnatpr TO cen_user;
COMMENT ON TABLE cadastre.d_dnatpr
  IS 'Nature de la personne';
COMMENT ON COLUMN cadastre.d_dnatpr.dnatpr IS 'Nature de la personne';

INSERT INTO cadastre.d_dnatpr VALUES ('ECF', 'physique, Economiquement faible (non servi)');
INSERT INTO cadastre.d_dnatpr VALUES ('FNL', 'physique, Fonctionnaire logé');
INSERT INTO cadastre.d_dnatpr VALUES ('DOM', 'physique, Propriétaire occupant dom.');
INSERT INTO cadastre.d_dnatpr VALUES ('HLM', 'morale, Office HLM');
INSERT INTO cadastre.d_dnatpr VALUES ('SEM', 'morale, Société d''économie mixte');
INSERT INTO cadastre.d_dnatpr VALUES ('TGV', 'morale, SCNF');
INSERT INTO cadastre.d_dnatpr VALUES ('RFF', 'morale, Réseau Ferré de France');
INSERT INTO cadastre.d_dnatpr VALUES ('CLL', 'morale, Collectivité locale');
INSERT INTO cadastre.d_dnatpr VALUES ('CAA', 'morale, Caisse assurance agricole');

-- Table: cadastre.d_dsgrpf
DROP TABLE IF EXISTS cadastre.d_dsgrpf;
CREATE TABLE IF NOT EXISTS cadastre.d_dsgrpf
(
  dsgrpf character varying(2) NOT NULL, -- Sous-groupe de nature de culture
  natcult_ssgrp character varying(50),
  cgrnum character varying(2),
  CONSTRAINT d_dsgrpf_pkey PRIMARY KEY (dsgrpf),
  CONSTRAINT d_dsgrpf_cgrnum_fkey FOREIGN KEY (cgrnum)
      REFERENCES cadastre.d_cgrnum (cgrnum) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_dsgrpf
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_dsgrpf TO cen_user;
COMMENT ON TABLE cadastre.d_dsgrpf
  IS 'Sous-groupe de nature de culture';
COMMENT ON COLUMN cadastre.d_dsgrpf.dsgrpf IS 'Sous-groupe de nature de culture';

CREATE INDEX fki_d_dsgrpf_cgrnum_fkey
  ON cadastre.d_dsgrpf
  USING btree
  (cgrnum COLLATE pg_catalog."default");

INSERT INTO cadastre.d_dsgrpf VALUES ('AB', 'Terrains à bâtir', '10');
INSERT INTO cadastre.d_dsgrpf VALUES ('AG', 'Terrains d’agrément', '11');
INSERT INTO cadastre.d_dsgrpf VALUES ('B', 'Bois', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('BF', 'Futaies Feuillues', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('BM', 'Futaies Mixtes', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('BO', 'Oseraies', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('BP', 'Peupleraies', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('BR', 'Futaies résineuses', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('BS', 'Taillis sous Futaies', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('BT', 'Taillis simples', '05');
INSERT INTO cadastre.d_dsgrpf VALUES ('CA', 'Carrières', '07');
INSERT INTO cadastre.d_dsgrpf VALUES ('CH', 'Chemins de fer, Canaux de Navigation', '12');
INSERT INTO cadastre.d_dsgrpf VALUES ('E', 'Eaux', '08');
INSERT INTO cadastre.d_dsgrpf VALUES ('J', 'Jardins', '09');
INSERT INTO cadastre.d_dsgrpf VALUES ('L', 'Landes', '06');
INSERT INTO cadastre.d_dsgrpf VALUES ('LB', 'Landes Boisées', '06');
INSERT INTO cadastre.d_dsgrpf VALUES ('P', 'Prés', '02');
INSERT INTO cadastre.d_dsgrpf VALUES ('PA', 'Pâtures ou Pâturages', '02');
INSERT INTO cadastre.d_dsgrpf VALUES ('PC', 'Pacages ou Pâtis', '02');
INSERT INTO cadastre.d_dsgrpf VALUES ('PE', 'Prés d''embouche', '02');
INSERT INTO cadastre.d_dsgrpf VALUES ('PH', 'Herbages', '02');
INSERT INTO cadastre.d_dsgrpf VALUES ('PP', 'Prés, Pâtures ou Herbages plantes', '02');
INSERT INTO cadastre.d_dsgrpf VALUES ('S', 'Sols', '13');
INSERT INTO cadastre.d_dsgrpf VALUES ('T', 'Terre', '01');
INSERT INTO cadastre.d_dsgrpf VALUES ('TP', 'Terres plantées', '01');
INSERT INTO cadastre.d_dsgrpf VALUES ('VE', 'Vergers', '03');
INSERT INTO cadastre.d_dsgrpf VALUES ('VI', 'Vignes', '04');
  
-- Table: cadastre.d_epxnee
DROP TABLE IF EXISTS cadastre.d_epxnee;
CREATE TABLE IF NOT EXISTS cadastre.d_epxnee
(
  epxnee character varying(3) NOT NULL, -- Mention du complément
  epxnee_lib character varying(15),
  CONSTRAINT d_epxnee_pkey PRIMARY KEY (epxnee)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_epxnee
  OWNER TO grp_sig;
COMMENT ON TABLE cadastre.d_epxnee
  IS 'Mention du complément';
COMMENT ON COLUMN cadastre.d_epxnee.epxnee IS 'Mention du complément';

INSERT INTO cadastre.d_epxnee VALUES ('EPX', 'Epoux');
INSERT INTO cadastre.d_epxnee VALUES ('NEE', 'Née');

-- Table: cadastre.d_gtoper
DROP TABLE IF EXISTS cadastre.d_gtoper;
CREATE TABLE IF NOT EXISTS cadastre.d_gtoper
(
  gtoper smallint NOT NULL, -- Indicateur de personne physique ou morale
  gtoper_lib character varying(15),
  CONSTRAINT d_gtoper_pkey PRIMARY KEY (gtoper)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_gtoper
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_gtoper TO cen_user;
COMMENT ON TABLE cadastre.d_gtoper
  IS 'Indicateur de personne physique ou morale';
COMMENT ON COLUMN cadastre.d_gtoper.gtoper IS 'Indicateur de personne physique ou morale';

INSERT INTO cadastre.d_gtoper VALUES (1, 'physique');
INSERT INTO cadastre.d_gtoper VALUES (2, 'morale');

-- Table: cadastre.d_motif_fin
DROP TABLE IF EXISTS cadastre.d_motif_fin;
CREATE TABLE IF NOT EXISTS cadastre.d_motif_fin
(
  motif_fin_id smallint NOT NULL,
  motif_fin_lib character varying(50),
  CONSTRAINT d_motif_fin_pkey PRIMARY KEY (motif_fin_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_motif_fin
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_motif_fin TO cen_user;
COMMENT ON TABLE cadastre.d_motif_fin
  IS 'Motif de fin (pourquoi la ligne cadastre_cen n''est plus active ?)';

INSERT INTO cadastre.d_motif_fin VALUES (1, 'Objet supprimé');
INSERT INTO cadastre.d_motif_fin VALUES (2, 'Changement de propriétaire');

-- Table: cadastre.d_typprop
DROP TABLE IF EXISTS cadastre.d_typprop;
CREATE TABLE IF NOT EXISTS cadastre.d_typprop
(
  typprop_id character varying(3) NOT NULL, -- gtoper _ ccogrm
  typprop_lib character varying(50),
  typprop character varying(10), -- Types de propriété
  poids integer,
  CONSTRAINT d_ctypprop_pkey PRIMARY KEY (typprop_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.d_typprop
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.d_typprop TO cen_user;
COMMENT ON TABLE cadastre.d_typprop
  IS 'Types de propriété';
COMMENT ON COLUMN cadastre.d_typprop.typprop_id IS 'gtoper _ ccogrm';
COMMENT ON COLUMN cadastre.d_typprop.typprop IS 'Types de propriété';

INSERT INTO cadastre.d_typprop VALUES ('2_9', 'Établissements publics ou organismes assimilés', 'Public', 100);
INSERT INTO cadastre.d_typprop VALUES ('2_5', 'Office HLM', 'Public', 100);
INSERT INTO cadastre.d_typprop VALUES ('2_3', 'Département', 'Public', 100);
INSERT INTO cadastre.d_typprop VALUES ('2_2', 'Région', 'Public', 100);
INSERT INTO cadastre.d_typprop VALUES ('2_1', 'État', 'Public', 100);
INSERT INTO cadastre.d_typprop VALUES ('2_8', 'Associé', 'Privé', 1);
INSERT INTO cadastre.d_typprop VALUES ('2_7', 'Copropriétaire', 'Privé', 1);
INSERT INTO cadastre.d_typprop VALUES ('2_6', 'Personnes morales représentant des sociétés', 'Privé', 1);
INSERT INTO cadastre.d_typprop VALUES ('1', 'Personnes privées', 'Privé', 1);
INSERT INTO cadastre.d_typprop VALUES ('2_0', 'Personnes morales non remarquables', 'N.P.', 10);
INSERT INTO cadastre.d_typprop VALUES ('2_4', 'Commune', 'Communal', 1000);


/* Schéma cadastre : tables globales */

-- Table: cadastre.vl_73
DROP TABLE IF EXISTS cadastre.vl_73;
CREATE TABLE IF NOT EXISTS cadastre.vl_73
(
  vl_id character varying(10) NOT NULL,
  libelle character varying(50),
  geom geometry(MultiPolygon,2154),
  CONSTRAINT vl_73_pkey PRIMARY KEY (vl_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.vl_73
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.vl_73 TO cen_user;

CREATE INDEX vl_73_geom_gist
  ON cadastre.vl_73
  USING gist
  (geom);

-- Table: cadastre.parcelles_73
DROP TABLE IF EXISTS cadastre.parcelles_73;
CREATE TABLE IF NOT EXISTS cadastre.parcelles_73
(
  par_id character varying(14) NOT NULL, -- Identifiant de la parcelle
  codcom character varying(5) NOT NULL, -- Code insee de la commune
  vl_id character varying(10) NOT NULL, -- Code du lieu-dit
  ccopre character varying(3), -- Préfixe de section ou quartier servi
  ccosec character varying(2), -- Code de la section
  dnupla integer, -- Numéro de la parcelle
  dparpi integer, -- Ancien numéro de la parcelle, parcelle primitive
  dcntpa integer, -- Contenance cadastrale de la parcelle
  typprop_id character varying(3), -- Type de propriété de la parcelle
  geom geometry(MultiPolygon,2154), -- Géométrie de la parcelle
  ccocomm character varying(3), -- Parcelle mère
  ccoprem character varying(3), -- Parcelle mère
  ccosecm character varying(2), -- Parcelle mère
  dnuplam integer, -- Parcelle mère
  type character varying(3), -- Type de filiation
  CONSTRAINT parcelles_73_pkey PRIMARY KEY (par_id),
  CONSTRAINT parcelles_73_codcom_fkey FOREIGN KEY (codcom)
      REFERENCES administratif.communes (code_insee) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT parcelles_73_typprop_id_fkey FOREIGN KEY (typprop_id)
      REFERENCES cadastre.d_typprop (typprop_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT parcelles_73_vl_id_fkey FOREIGN KEY (vl_id)
      REFERENCES cadastre.vl_73 (vl_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.parcelles_73
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.parcelles_73 TO cen_user;
COMMENT ON COLUMN cadastre.parcelles_73.par_id IS 'Identifiant de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_73.codcom IS 'Code insee de la commune';
COMMENT ON COLUMN cadastre.parcelles_73.vl_id IS 'Code du lieu-dit';
COMMENT ON COLUMN cadastre.parcelles_73.ccopre IS 'Préfixe de section ou quartier servi';
COMMENT ON COLUMN cadastre.parcelles_73.ccosec IS 'Code de la section';
COMMENT ON COLUMN cadastre.parcelles_73.dnupla IS 'Numéro de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_73.dparpi IS 'Ancien numéro de la parcelle, parcelle primitive';
COMMENT ON COLUMN cadastre.parcelles_73.dcntpa IS 'Contenance cadastrale de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_73.typprop_id IS 'Type de propriété de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_73.geom IS 'Géométrie de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_73.ccocomm IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_73.ccoprem IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_73.ccosecm IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_73.dnuplam IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_73.type IS 'Type de filiation';

CREATE INDEX fki_parcelles_73_codcom_fkey
  ON cadastre.parcelles_73
  USING btree
  (codcom COLLATE pg_catalog."default");
CREATE INDEX fki_parcelles_73_typprop_id_fkey
  ON cadastre.parcelles_73
  USING btree
  (typprop_id COLLATE pg_catalog."default");
CREATE INDEX fki_parcelles_73_vl_id_fkey
  ON cadastre.parcelles_73
  USING btree
  (vl_id COLLATE pg_catalog."default");
CREATE INDEX parcelles_73_geom_gist
  ON cadastre.parcelles_73
  USING gist
  (geom);

-- Table: cadastre.lots_73
DROP TABLE IF EXISTS cadastre.lots_73;
CREATE TABLE IF NOT EXISTS cadastre.lots_73
(
  lot_id character varying(21) NOT NULL,
  par_id character varying(14) NOT NULL,
  dnulot character varying(7),
  dcntlo integer,
  geom geometry(MultiPolygon,2154),
  CONSTRAINT lots_73_pkey PRIMARY KEY (lot_id),
  CONSTRAINT lots_73_par_id_fkey FOREIGN KEY (par_id)
      REFERENCES cadastre.parcelles_73 (par_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.lots_73
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.lots_73 TO cen_user;

CREATE INDEX fki_lots_73_par_id_fkey
  ON cadastre.lots_73
  USING btree
  (par_id COLLATE pg_catalog."default");
CREATE INDEX lots_73_geom_gist
  ON cadastre.lots_73
  USING gist
  (geom);

-- Table: cadastre.lots_natcult_73
DROP SEQUENCE IF EXISTS cadastre.lots_natcult_73_lotnatcult_id_seq;
CREATE SEQUENCE cadastre.lots_natcult_73_lotnatcult_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE cadastre.lots_natcult_73_lotnatcult_id_seq
  OWNER TO grp_sig;
SELECT setval('cadastre.lots_natcult_73_lotnatcult_id_seq', 0, true);

DROP TABLE IF EXISTS cadastre.lots_natcult_73;
CREATE TABLE IF NOT EXISTS cadastre.lots_natcult_73
(
  lotnatcult_id integer NOT NULL DEFAULT nextval('cadastre.lots_natcult_73_lotnatcult_id_seq'::regclass),
  lot_id character varying(21) NOT NULL,
  dsgrpf character varying(2),
  cnatsp character varying(5),
  dclssf integer,
  ccosub character varying(2),
  dcntsf integer,
  CONSTRAINT lots_natcult_73_pkey PRIMARY KEY (lotnatcult_id),
  CONSTRAINT lots_natcult_73_lot_id_fkey FOREIGN KEY (lot_id)
      REFERENCES cadastre.lots_73 (lot_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.lots_natcult_73
  OWNER TO grp_sig;

CREATE INDEX fki_lots_natcult_73_lot_id_fkey
  ON cadastre.lots_natcult_73
  USING btree
  (lot_id COLLATE pg_catalog."default");

-- Table: cadastre.proprios_73
DROP TABLE IF EXISTS cadastre.proprios_73;
CREATE TABLE IF NOT EXISTS cadastre.proprios_73
(
  dnuper character varying(8) NOT NULL,
  ccoqua smallint,
  ddenom text,
  jdatnss character varying(10),
  dldnss character varying(75),
  dsglpm character varying(25),
  dlign3 text,
  dlign4 text,
  dlign5 text,
  dlign6 text,
  dnatpr character varying(3),
  gtoper smallint,
  ccogrm smallint,
  CONSTRAINT proprios_73_pkey PRIMARY KEY (dnuper)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.proprios_73
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.proprios_73 TO cen_user;

-- Table: cadastre.cptprop_73
DROP TABLE IF EXISTS cadastre.cptprop_73;
CREATE TABLE IF NOT EXISTS cadastre.cptprop_73
(
  dnupro character varying(11) NOT NULL,
  CONSTRAINT cptprop_73_pkey PRIMARY KEY (dnupro)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.cptprop_73
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.cptprop_73 TO cen_user;

-- Table: cadastre.r_prop_cptprop_73
DROP TABLE IF EXISTS cadastre.r_prop_cptprop_73;
CREATE TABLE IF NOT EXISTS cadastre.r_prop_cptprop_73
(
  dnuper character varying(8) NOT NULL,
  dnupro character varying(11) NOT NULL,
  dnomlp character varying(150),
  dprnlp character varying(150),
  epxnee character varying(3),
  dnomcp character varying(150),
  dprncp character varying(150),
  ccodro character varying(1),
  ccodem character varying(1),
  CONSTRAINT r_prop_cptprop_73_pkey PRIMARY KEY (dnuper, dnupro),
  CONSTRAINT r_prop_cptprop_73_dnuper_fkey FOREIGN KEY (dnuper)
      REFERENCES cadastre.proprios_73 (dnuper) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_prop_cptprop_73_dnupro_fkey FOREIGN KEY (dnupro)
      REFERENCES cadastre.cptprop_73 (dnupro) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.r_prop_cptprop_73
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.r_prop_cptprop_73 TO cen_user;

CREATE INDEX fki_r_prop_cptprop_73_dnuper_fkey
  ON cadastre.r_prop_cptprop_73
  USING btree
  (dnuper COLLATE pg_catalog."default");
CREATE INDEX fki_r_prop_cptprop_73_dnupro_fkey
  ON cadastre.r_prop_cptprop_73
  USING btree
  (dnupro COLLATE pg_catalog."default");

-- Table: cadastre.cadastre_73
DROP SEQUENCE IF EXISTS cadastre.cadastre_73_cad_73_id_seq;
CREATE SEQUENCE cadastre.cadastre_73_cad_73_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE cadastre.cadastre_73_cad_73_id_seq
  OWNER TO grp_sig;
SELECT setval('cadastre.cadastre_73_cad_73_id_seq', 0, true);

DROP TABLE IF EXISTS cadastre.cadastre_73;
CREATE TABLE IF NOT EXISTS cadastre.cadastre_73
(
  cad_73_id integer NOT NULL DEFAULT nextval('cadastre.cadastre_73_cad_73_id_seq'::regclass),
  lot_id character varying(21) NOT NULL,
  dnupro character varying(11) NOT NULL,
  CONSTRAINT cadastre_73_pkey PRIMARY KEY (cad_73_id),
  CONSTRAINT cadastre_73_dnupro_fkey FOREIGN KEY (dnupro)
      REFERENCES cadastre.cptprop_73 (dnupro) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cadastre_73_lot_id_fkey FOREIGN KEY (lot_id)
      REFERENCES cadastre.lots_73 (lot_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.cadastre_73
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.cadastre_73 TO cen_user;

CREATE INDEX fki_cadastre_73_dnupro_fkey
  ON cadastre.cadastre_73
  USING btree
  (dnupro COLLATE pg_catalog."default");
CREATE INDEX fki_cadastre_73_lot_id_fkey
  ON cadastre.cadastre_73
  USING btree
  (lot_id COLLATE pg_catalog."default");


/* Schéma cadastre : tables cen */

-- Table: cadastre.vl_cen
DROP TABLE IF EXISTS cadastre.vl_cen;
CREATE TABLE IF NOT EXISTS cadastre.vl_cen
(
  vl_id character varying(10) NOT NULL,
  libelle character varying(50),
  geom geometry(MultiPolygon,2154),
  CONSTRAINT vl_cen_pkey PRIMARY KEY (vl_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.vl_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.vl_cen TO cen_user;
COMMENT ON TABLE cadastre.vl_cen
  IS 'Lieux-dits concernant les parcelles "du CEN".';

CREATE INDEX vl_cen_geom_gist
  ON cadastre.vl_cen
  USING gist
  (geom);

-- Table: cadastre.parcelles_cen
DROP TABLE IF EXISTS cadastre.parcelles_cen;
CREATE TABLE IF NOT EXISTS cadastre.parcelles_cen
(
  par_id character varying(14) NOT NULL, -- Identifiant de la parcelle
  codcom character varying(5) NOT NULL, -- Code insee de la commune
  vl_id character varying(10) NOT NULL, -- Code du lieu-dit
  ccopre character varying(3), -- Préfixe de section ou quartier servi
  ccosec character varying(2), -- Code de la section
  dnupla integer, -- Numéro de la parcelle
  dparpi integer, -- Ancien numéro de la parcelle, parcelle primitive
  dcntpa integer, -- Contenance cadastrale de la parcelle
  typprop_id character varying(3), -- Type de propriété de la parcelle
  geom geometry(MultiPolygon,2154), -- Géométrie de la parcelle
  ccocomm character varying(3), -- Parcelle mère
  ccoprem character varying(3), -- Parcelle mère
  ccosecm character varying(2), -- Parcelle mère
  dnuplam integer, -- Parcelle mère
  type character varying(3), -- Type de filiation
  annee_pci character varying(4), -- Année du cadastre d'origine
  CONSTRAINT parcelles_cen_pkey PRIMARY KEY (par_id),
  CONSTRAINT parcelles_cen_codcom_fkey FOREIGN KEY (codcom)
      REFERENCES administratif.communes (code_insee) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT parcelles_cen_typprop_id_fkey FOREIGN KEY (typprop_id)
      REFERENCES cadastre.d_typprop (typprop_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT parcelles_cen_vl_id_fkey FOREIGN KEY (vl_id)
      REFERENCES cadastre.vl_cen (vl_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.parcelles_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.parcelles_cen TO cen_user;
COMMENT ON TABLE cadastre.parcelles_cen
  IS 'Parcelles "du CEN". Ici sont stockées toutes les parcelles qui intéressent ou ont intéressé le CEN dans le passé. Rien n''est supprimé => historique.';
COMMENT ON COLUMN cadastre.parcelles_cen.par_id IS 'Identifiant de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_cen.codcom IS 'Code insee de la commune';
COMMENT ON COLUMN cadastre.parcelles_cen.vl_id IS 'Code du lieu-dit';
COMMENT ON COLUMN cadastre.parcelles_cen.ccopre IS 'Préfixe de section ou quartier servi';
COMMENT ON COLUMN cadastre.parcelles_cen.ccosec IS 'Code de la section';
COMMENT ON COLUMN cadastre.parcelles_cen.dnupla IS 'Numéro de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_cen.dparpi IS 'Ancien numéro de la parcelle, parcelle primitive';
COMMENT ON COLUMN cadastre.parcelles_cen.dcntpa IS 'Contenance cadastrale de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_cen.typprop_id IS 'Type de propriété de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_cen.geom IS 'Géométrie de la parcelle';
COMMENT ON COLUMN cadastre.parcelles_cen.ccocomm IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_cen.ccoprem IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_cen.ccosecm IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_cen.dnuplam IS 'Parcelle mère';
COMMENT ON COLUMN cadastre.parcelles_cen.type IS 'Type de filiation';
COMMENT ON COLUMN cadastre.parcelles_cen.annee_pci IS 'Année du cadastre d''origine';

CREATE INDEX fki_parcelles_cen_codcom_fkey
  ON cadastre.parcelles_cen
  USING btree
  (codcom COLLATE pg_catalog."default");
CREATE INDEX fki_parcelles_cen_typprop_id_fkey
  ON cadastre.parcelles_cen
  USING btree
  (typprop_id COLLATE pg_catalog."default");
CREATE INDEX fki_parcelles_cen_vl_id_fkey
  ON cadastre.parcelles_cen
  USING btree
  (vl_id COLLATE pg_catalog."default");
CREATE INDEX parcelles_cen_geom_gist
  ON cadastre.parcelles_cen
  USING gist
  (geom);

-- Table: cadastre.lots_cen
DROP TABLE IF EXISTS cadastre.lots_cen;
CREATE TABLE IF NOT EXISTS cadastre.lots_cen
(
  lot_id character varying(21) NOT NULL, -- Identifiant du lot
  par_id character varying(14) NOT NULL, -- Identifiant de la parcelle
  dnulot character varying(7), -- Numéro du lot
  dcntlo integer, -- Contenance cadastrale (m²)
  geom geometry(MultiPolygon,2154), -- Géométrie du lot (si connue et différente de la parcelle d'origine)
  CONSTRAINT lots_cen_pkey PRIMARY KEY (lot_id),
  CONSTRAINT lots_cen_par_id_fkey FOREIGN KEY (par_id)
      REFERENCES cadastre.parcelles_cen (par_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.lots_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.lots_cen TO cen_user;
COMMENT ON TABLE cadastre.lots_cen
  IS 'Lots "du CEN". Le champ geom est obligatoire pour stocker les géométries des lots s''ils sont délimités. Par contre, il n''y a pas forcément besoin de "re-"stocker la géométrie des parcelles non-subdivivisées en lots... choix à faire !';
COMMENT ON COLUMN cadastre.lots_cen.lot_id IS 'Identifiant du lot';
COMMENT ON COLUMN cadastre.lots_cen.par_id IS 'Identifiant de la parcelle';
COMMENT ON COLUMN cadastre.lots_cen.dnulot IS 'Numéro du lot';
COMMENT ON COLUMN cadastre.lots_cen.dcntlo IS 'Contenance cadastrale (m²)';
COMMENT ON COLUMN cadastre.lots_cen.geom IS 'Géométrie du lot (si connue et différente de la parcelle d''origine)';

CREATE INDEX fki_lots_cen_par_id_fkey
  ON cadastre.lots_cen
  USING btree
  (par_id COLLATE pg_catalog."default");
CREATE INDEX lots_cen_geom_gist
  ON cadastre.lots_cen
  USING gist
  (geom);

-- Table: cadastre.lots_natcult_cen
DROP SEQUENCE IF EXISTS cadastre.lots_natcult_cen_lotnatcult_id_seq;
CREATE SEQUENCE cadastre.lots_natcult_cen_lotnatcult_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE cadastre.lots_natcult_cen_lotnatcult_id_seq
  OWNER TO grp_sig;
SELECT setval('cadastre.lots_natcult_cen_lotnatcult_id_seq', 0, true);

DROP TABLE IF EXISTS cadastre.lots_natcult_cen;
CREATE TABLE IF NOT EXISTS cadastre.lots_natcult_cen
(
  lotnatcult_id integer NOT NULL DEFAULT nextval('cadastre.lots_natcult_cen_lotnatcult_id_seq'::regclass), -- N° auto
  lot_id character varying(21) NOT NULL, -- Identifiant du lot
  dsgrpf character varying(2), -- Sous-groupe de nature de culture
  cnatsp character varying(5), -- Code nature de culture spéciale
  dclssf integer, -- Classe dans le groupe et la série tarif
  ccosub character varying(2), -- Lettres indicatives de la suf
  dcntsf integer, -- Contenance de la suf
  CONSTRAINT lots_natcult_cen_pkey PRIMARY KEY (lotnatcult_id),
  CONSTRAINT lots_natcult_cen_cnatsp_fkey FOREIGN KEY (cnatsp)
      REFERENCES cadastre.d_cnatsp (cnatsp) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT lots_natcult_cen_dsgrpf_fkey FOREIGN KEY (dsgrpf)
      REFERENCES cadastre.d_dsgrpf (dsgrpf) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT lots_natcult_cen_lot_id_fkey FOREIGN KEY (lot_id)
      REFERENCES cadastre.lots_cen (lot_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.lots_natcult_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.lots_natcult_cen TO cen_user;
COMMENT ON TABLE cadastre.lots_natcult_cen
  IS 'Nature de culture sur les lots "du CEN".';
COMMENT ON COLUMN cadastre.lots_natcult_cen.lotnatcult_id IS 'N° auto';
COMMENT ON COLUMN cadastre.lots_natcult_cen.lot_id IS 'Identifiant du lot';
COMMENT ON COLUMN cadastre.lots_natcult_cen.dsgrpf IS 'Sous-groupe de nature de culture';
COMMENT ON COLUMN cadastre.lots_natcult_cen.cnatsp IS 'Code nature de culture spéciale';
COMMENT ON COLUMN cadastre.lots_natcult_cen.dclssf IS 'Classe dans le groupe et la série tarif';
COMMENT ON COLUMN cadastre.lots_natcult_cen.ccosub IS 'Lettres indicatives de la suf';
COMMENT ON COLUMN cadastre.lots_natcult_cen.dcntsf IS 'Contenance de la suf';

CREATE INDEX fki_lots_natcult_cen_cnatsp_fkey
  ON cadastre.lots_natcult_cen
  USING btree
  (cnatsp COLLATE pg_catalog."default");
CREATE INDEX fki_lots_natcult_cen_dsgrpf_fkey
  ON cadastre.lots_natcult_cen
  USING btree
  (dsgrpf COLLATE pg_catalog."default");
CREATE INDEX fki_lots_natcult_cen_lot_id_fkey
  ON cadastre.lots_natcult_cen
  USING btree
  (lot_id COLLATE pg_catalog."default");

-- Table: cadastre.proprios_cen
DROP TABLE IF EXISTS cadastre.proprios_cen;
CREATE TABLE IF NOT EXISTS cadastre.proprios_cen
(
  dnuper character varying(8) NOT NULL, -- Numéro de personne MAJIC
  ccoqua smallint, -- Code qualité de personne physique
  ddenom text, -- Dénomimation de personne physique ou morale
  nom_usage character varying(150), -- Nom d'usage - champ calculé à partir de la matrice
  nom_jeunefille character varying(150), -- Nom de jeune fille - champ calculé à partir de la matrice
  prenom_usage character varying(150), -- Prénom d'usage - champ calculé à partir de la matrice
  nom_conjoint character varying(150), -- Nom du conjoint - champ calculé à partir de la matrice
  prenom_conjoint character varying(150), -- Prénom du conjoint - champ calculé à partir de la matrice
  dnomlp character varying(150), -- Nom d'usage
  dprnlp character varying(150), -- Prénoms associés au nom d'usage
  epxnee character varying(3), -- Mention du complément
  dnomcp character varying(150), -- Nom complément
  dprncp character varying(150), -- Prénoms associés au complément
  jdatnss character varying(10), -- Date de naissance de personne physique
  dldnss character varying(75), -- Lieu de naissance de la personne physique
  dlign3 text, -- Adresse1 de personne physique ou morale
  dlign4 text, -- Adresse2 de personne physique ou morale
  dlign5 text, -- Adresse3 de personne physique ou morale
  dlign6 text, -- Adresse4 de personne physique ou morale
  email character varying(50), -- Mail de la personne physique ou morale
  fixe_domicile character varying(14), -- Tél. fixe perso de la personne physique ou morale
  fixe_travail character varying(14), -- Tél. fixe travail de la personne physique ou morale
  portable_domicile character varying(14), -- Tél. mobile perso de la personne physique ou morale
  portable_travail character varying(14), -- Tél. mobile travail de la personne physique ou morale
  fax character varying(14), -- Fax de la personne physique ou morale
  gtoper smallint, -- Indicateur de personne physique ou morale
  ccogrm smallint, -- Groupe de personne morale
  dnatpr character varying(3), -- Nature de la personne
  dsglpm character varying(25), -- Sigle de la personne morale
  commentaires text, -- Commentaires
  annee_matrice character varying(4), -- Année de la matrice d'origine
  maj_user character varying(25),
  maj_date character varying(19),
  CONSTRAINT proprios_cen_pkey PRIMARY KEY (dnuper),
  CONSTRAINT proprios_cen_ccogrm_fkey FOREIGN KEY (ccogrm)
      REFERENCES cadastre.d_ccogrm (ccogrm) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT proprios_cen_ccoqua_fkey FOREIGN KEY (ccoqua)
      REFERENCES cadastre.d_ccoqua (ccoqua) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT proprios_cen_dnatpr_fkey FOREIGN KEY (dnatpr)
      REFERENCES cadastre.d_dnatpr (dnatpr) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT proprios_cen_epxnee_fkey FOREIGN KEY (epxnee)
      REFERENCES cadastre.d_epxnee (epxnee) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT proprios_cen_gtoper_fkey FOREIGN KEY (gtoper)
      REFERENCES cadastre.d_gtoper (gtoper) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.proprios_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.proprios_cen TO cen_user;
GRANT UPDATE ON TABLE cadastre.proprios_cen TO grp_foncier;
COMMENT ON TABLE cadastre.proprios_cen
  IS 'Propriétaires "du CEN". Cette table contient l''ensemble des informations personnelles sur les propriétaires, elles sont nécessaires à l''animation foncière, au suivi du foncier, etc. Rien n''est supprimé => historique.';
COMMENT ON COLUMN cadastre.proprios_cen.dnuper IS 'Numéro de personne MAJIC';
COMMENT ON COLUMN cadastre.proprios_cen.ccoqua IS 'Code qualité de personne physique';
COMMENT ON COLUMN cadastre.proprios_cen.ddenom IS 'Dénomimation de personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.nom_usage IS 'Nom d''usage - champ calculé à partir de la matrice';
COMMENT ON COLUMN cadastre.proprios_cen.nom_jeunefille IS 'Nom de jeune fille - champ calculé à partir de la matrice';
COMMENT ON COLUMN cadastre.proprios_cen.prenom_usage IS 'Prénom d''usage - champ calculé à partir de la matrice';
COMMENT ON COLUMN cadastre.proprios_cen.nom_conjoint IS 'Nom du conjoint - champ calculé à partir de la matrice';
COMMENT ON COLUMN cadastre.proprios_cen.prenom_conjoint IS 'Prénom du conjoint - champ calculé à partir de la matrice';
COMMENT ON COLUMN cadastre.proprios_cen.dnomlp IS 'Nom d''usage';
COMMENT ON COLUMN cadastre.proprios_cen.dprnlp IS 'Prénoms associés au nom d''usage';
COMMENT ON COLUMN cadastre.proprios_cen.epxnee IS 'Mention du complément';
COMMENT ON COLUMN cadastre.proprios_cen.dnomcp IS 'Nom complément';
COMMENT ON COLUMN cadastre.proprios_cen.dprncp IS 'Prénoms associés au complément';
COMMENT ON COLUMN cadastre.proprios_cen.jdatnss IS 'Date de naissance de personne physique';
COMMENT ON COLUMN cadastre.proprios_cen.dldnss IS 'Lieu de naissance de la personne physique';
COMMENT ON COLUMN cadastre.proprios_cen.dlign3 IS 'Adresse1 de personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.dlign4 IS 'Adresse2 de personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.dlign5 IS 'Adresse3 de personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.dlign6 IS 'Adresse4 de personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.email IS 'Mail de la personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.fixe_domicile IS 'Tél. fixe perso de la personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.fixe_travail IS 'Tél. fixe travail de la personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.portable_domicile IS 'Tél. mobile perso de la personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.portable_travail IS 'Tél. mobile travail de la personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.fax IS 'Fax de la personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.gtoper IS 'Indicateur de personne physique ou morale';
COMMENT ON COLUMN cadastre.proprios_cen.ccogrm IS 'Groupe de personne morale';
COMMENT ON COLUMN cadastre.proprios_cen.dnatpr IS 'Nature de la personne';
COMMENT ON COLUMN cadastre.proprios_cen.dsglpm IS 'Sigle de la personne morale';
COMMENT ON COLUMN cadastre.proprios_cen.commentaires IS 'Commentaires';
COMMENT ON COLUMN cadastre.proprios_cen.annee_matrice IS 'Année de la matrice d''origine';

CREATE INDEX fki_proprios_cen_ccogrm_fkey
  ON cadastre.proprios_cen
  USING btree
  (ccogrm);
CREATE INDEX fki_proprios_cen_ccoqua_fkey
  ON cadastre.proprios_cen
  USING btree
  (ccoqua);
CREATE INDEX fki_proprios_cen_dnatpr_fkey
  ON cadastre.proprios_cen
  USING btree
  (dnatpr COLLATE pg_catalog."default");
CREATE INDEX fki_proprios_cen_epxnee_fkey
  ON cadastre.proprios_cen
  USING btree
  (epxnee COLLATE pg_catalog."default");
CREATE INDEX fki_proprios_cen_gtoper_fkey
  ON cadastre.proprios_cen
  USING btree
  (gtoper);

-- Table: cadastre.cptprop_cen
DROP TABLE IF EXISTS cadastre.cptprop_cen;
CREATE TABLE IF NOT EXISTS cadastre.cptprop_cen
(
  dnupro character varying(11) NOT NULL, -- Numéro de compte communal
  annee_matrice character varying(4), -- Année de la matrice d'origine
  CONSTRAINT cptprop_cen_pkey PRIMARY KEY (dnupro)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.cptprop_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.cptprop_cen TO cen_user;
COMMENT ON TABLE cadastre.cptprop_cen
  IS 'Comptes de propriété associés à proprios_cen. Rien n''est supprimé => historique.';
COMMENT ON COLUMN cadastre.cptprop_cen.dnupro IS 'Numéro de compte communal';
COMMENT ON COLUMN cadastre.cptprop_cen.annee_matrice IS 'Année de la matrice d''origine';

-- Table: cadastre.r_prop_cptprop_cen
DROP TABLE IF EXISTS cadastre.r_prop_cptprop_cen;
CREATE TABLE IF NOT EXISTS cadastre.r_prop_cptprop_cen
(
  dnuper character varying(8) NOT NULL, -- Numéro de personne MAJIC
  dnupro character varying(11) NOT NULL, -- Numéro de compte communal
  ccodro character varying(1), -- Code du droit réel ou particulier
  ccodem character varying(1), -- Code du démembrement/indivision
  CONSTRAINT r_prop_cptprop_cen_pkey PRIMARY KEY (dnuper, dnupro),
  CONSTRAINT r_prop_cptprop_cen_ccodem_fkey FOREIGN KEY (ccodem)
      REFERENCES cadastre.d_ccodem (ccodem) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_prop_cptprop_cen_ccodro_fkey FOREIGN KEY (ccodro)
      REFERENCES cadastre.d_ccodro (ccodro) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_prop_cptprop_cen_dnuper_fkey FOREIGN KEY (dnuper)
      REFERENCES cadastre.proprios_cen (dnuper) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_prop_cptprop_cen_dnupro_fkey FOREIGN KEY (dnupro)
      REFERENCES cadastre.cptprop_cen (dnupro) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.r_prop_cptprop_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.r_prop_cptprop_cen TO cen_user;
COMMENT ON TABLE cadastre.r_prop_cptprop_cen
  IS 'Relations entre proprios_cen et cptprop_cen. Rien n''est supprimé => historique.';
COMMENT ON COLUMN cadastre.r_prop_cptprop_cen.dnuper IS 'Numéro de personne MAJIC';
COMMENT ON COLUMN cadastre.r_prop_cptprop_cen.dnupro IS 'Numéro de compte communal';
COMMENT ON COLUMN cadastre.r_prop_cptprop_cen.ccodro IS 'Code du droit réel ou particulier';
COMMENT ON COLUMN cadastre.r_prop_cptprop_cen.ccodem IS 'Code du démembrement/indivision';

CREATE INDEX fki_r_prop_cptprop_cen_dnuper_fkey
  ON cadastre.r_prop_cptprop_cen
  USING btree
  (dnuper COLLATE pg_catalog."default");
CREATE INDEX fki_r_prop_cptprop_cen_dnupro_fkey
  ON cadastre.r_prop_cptprop_cen
  USING btree
  (dnupro COLLATE pg_catalog."default");

-- Table: cadastre.cadastre_cen
DROP SEQUENCE IF EXISTS cadastre.cadastre_cen_cad_cen_id_seq;
CREATE SEQUENCE cadastre.cadastre_cen_cad_cen_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE cadastre.cadastre_cen_cad_cen_id_seq
  OWNER TO grp_sig;
SELECT setval('cadastre.cadastre_cen_cad_cen_id_seq', 0, true);

DROP TABLE IF EXISTS cadastre.cadastre_cen;
CREATE TABLE IF NOT EXISTS cadastre.cadastre_cen
(
  cad_cen_id integer NOT NULL DEFAULT nextval('cadastre.cadastre_cen_cad_cen_id_seq'::regclass), -- N° auto
  lot_id character varying(21) NOT NULL, -- Identifiant du lot
  dnupro character varying(11) NOT NULL, -- Numéro de compte communal
  date_deb character varying(10), -- Date de début d'association lot-cptprop
  date_fin character varying(10), -- Date de fin d'association lot-cptprop
  motif_fin_id smallint, -- Pourquoi la ligne cadastre_cen n'est plus active ?
  CONSTRAINT cadastre_cen_pkey PRIMARY KEY (cad_cen_id),
  CONSTRAINT cadastre_cen_dnupro_fkey FOREIGN KEY (dnupro)
      REFERENCES cadastre.cptprop_cen (dnupro) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cadastre_cen_lot_id_fkey FOREIGN KEY (lot_id)
      REFERENCES cadastre.lots_cen (lot_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cadastre_cen_motif_fin_id_fkey FOREIGN KEY (motif_fin_id)
      REFERENCES cadastre.d_motif_fin (motif_fin_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_modif_fin_id_is_ok CHECK (cadastre.c_modif_fin_id_is_ok(date_fin, motif_fin_id))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cadastre.cadastre_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE cadastre.cadastre_cen TO cen_user;
COMMENT ON TABLE cadastre.cadastre_cen
  IS 'Associations de parcelles_cen et de cptprop_cen, cette table va permettre d''historiser le cadastre (changements géométriques et/ou d''informations propriétaires). Il n''y a pas besoin de champ geom puisqu''il est déjà stocké dans les tables parcelles_cen et lots_cen. Rien n''est supprimé => historique.';
COMMENT ON COLUMN cadastre.cadastre_cen.cad_cen_id IS 'N° auto';
COMMENT ON COLUMN cadastre.cadastre_cen.lot_id IS 'Identifiant du lot';
COMMENT ON COLUMN cadastre.cadastre_cen.dnupro IS 'Numéro de compte communal';
COMMENT ON COLUMN cadastre.cadastre_cen.date_deb IS 'Date de début d''association lot-cptprop';
COMMENT ON COLUMN cadastre.cadastre_cen.date_fin IS 'Date de fin d''association lot-cptprop';
COMMENT ON COLUMN cadastre.cadastre_cen.motif_fin_id IS 'Pourquoi la ligne cadastre_cen n''est plus active ?';

CREATE INDEX fki_cadastre_cen_dnupro_fkey
  ON cadastre.cadastre_cen
  USING btree
  (dnupro COLLATE pg_catalog."default");
CREATE INDEX fki_cadastre_cen_lot_id_fkey
  ON cadastre.cadastre_cen
  USING btree
  (lot_id COLLATE pg_catalog."default");


/* Schéma sites : tables */

CREATE SCHEMA sites
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA sites TO cen_user;

-- Table: sites.d_milieux
DROP TABLE IF EXISTS sites.d_milieux;
CREATE TABLE IF NOT EXISTS sites.d_milieux
(
  milieu_id smallint NOT NULL,
  milieu_lib text,
  milieu_lib_simpl text,
  milieu_descrip text,
  CONSTRAINT d_milieux_pkey PRIMARY KEY (milieu_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.d_milieux
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.d_milieux TO cen_user;

INSERT INTO sites.d_milieux VALUES (15, 'Sites d''espèces remarquables autres que chiroptères', 'Sites d''espèces remarquables', NULL);
INSERT INTO sites.d_milieux VALUES (14, 'Ecosystèmes montagnards', 'Milieux montagnards', NULL);
INSERT INTO sites.d_milieux VALUES (13, 'Sites géologiques', 'Sites géologiques', NULL);
INSERT INTO sites.d_milieux VALUES (12, 'Milieux artificialisés (carrières, terrils, gravières, etc.)', 'Milieux artificialisés', NULL);
INSERT INTO sites.d_milieux VALUES (11, 'Habitats rocheux', 'Habitats rocheux', NULL);
INSERT INTO sites.d_milieux VALUES (9, 'Ecosystèmes lacustres', 'Milieux lacustres', NULL);
INSERT INTO sites.d_milieux VALUES (8, 'Ecosystèmes forestiers', 'Milieux forestiers', NULL);
INSERT INTO sites.d_milieux VALUES (5, 'Gîtes à chiroptères et milieux souterrains', 'Gîtes à chiroptères', NULL);
INSERT INTO sites.d_milieux VALUES (4, 'Ecosystèmes alluviaux', 'Milieux alluviaux', NULL);
INSERT INTO sites.d_milieux VALUES (3, 'Landes, prairies et fruticées', 'Landes, prairies', NULL);
INSERT INTO sites.d_milieux VALUES (2, 'Pelouses sèches', 'Pelouses sèches', NULL);
INSERT INTO sites.d_milieux VALUES (1, 'Marais alcalins', 'Marais de plaine', NULL);
INSERT INTO sites.d_milieux VALUES (7, 'Ecosystèmes aquatiques', 'Milieux aquatiques', NULL);
INSERT INTO sites.d_milieux VALUES (0, 'N.D.', 'N.P.', NULL);
INSERT INTO sites.d_milieux VALUES (16, 'Tourbières', 'Tourbières', NULL);

-- Table: sites.d_typsite
DROP TABLE IF EXISTS sites.d_typsite;
CREATE TABLE IF NOT EXISTS sites.d_typsite
(
  typsite_id character varying(3) NOT NULL,
  typsite_lib text,
  typsite_descrip text,
  CONSTRAINT d_typsite_pkey PRIMARY KEY (typsite_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.d_typsite
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.d_typsite TO cen_user;

INSERT INTO sites.d_typsite VALUES ('4', 'Site en dormance', NULL);
INSERT INTO sites.d_typsite VALUES ('3', 'Site en étude préalable', NULL);
INSERT INTO sites.d_typsite VALUES ('2-1', 'Site à assistance technique et territoriale', NULL);
INSERT INTO sites.d_typsite VALUES ('2-2', 'Site à expertise sans vocation conservatoire', NULL);
INSERT INTO sites.d_typsite VALUES ('1-1', 'Site Conservatoire', 'Site du Conservatoire avec document de gestion');
INSERT INTO sites.d_typsite VALUES ('0', 'N.D.', NULL);
INSERT INTO sites.d_typsite VALUES ('1-2', 'Site Conservatoire (sans doc. de gestion)', 'Site du Conservatoire sans document de gestion');

-- Table: sites.sites
DROP TABLE IF EXISTS sites.sites;
CREATE TABLE IF NOT EXISTS sites.sites
(
  site_id character varying(10) NOT NULL,
  site_nom text NOT NULL,
  typsite_id character varying(3) NOT NULL,
  milieu_id smallint NOT NULL,
  annee_creation character varying(4) NOT NULL,
  surf_m2 numeric(10,2),
  geom geometry(Geometry,2154),
  geom_mfu geometry(MultiPolygon,2154),
  geom_cad geometry(MultiPolygon,2154),
  geom_ecolo geometry(Polygon,2154),
  geom_ef geometry(Polygon,2154),
  fcen_id character varying(10),
  mnhn_id character varying(10),
  CONSTRAINT sites_pkey PRIMARY KEY (site_id),
  CONSTRAINT sites_milieu_id_fkey FOREIGN KEY (milieu_id)
      REFERENCES sites.d_milieux (milieu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT sites_typsite_id_fkey FOREIGN KEY (typsite_id)
      REFERENCES sites.d_typsite (typsite_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.sites
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.sites TO cen_user;

CREATE INDEX sites_geom_ecolo_gist
  ON sites.sites
  USING gist
  (geom_ecolo);
CREATE INDEX sites_geom_ef_gist
  ON sites.sites
  USING gist
  (geom_ef);
CREATE INDEX sites_geom_gist
  ON sites.sites
  USING gist
  (geom);
CREATE INDEX sites_geom_mfu_gist
  ON sites.sites
  USING gist
  (geom_mfu);
CREATE INDEX sites_geom_parc_gist
  ON sites.sites
  USING gist
  (geom_cad);

-- Table: sites.d_typreferent
DROP TABLE IF EXISTS sites.d_typreferent;
CREATE TABLE IF NOT EXISTS sites.d_typreferent
(
  typreferent_id smallint NOT NULL,
  typreferent_lib text,
  typreferent_descrip text,
  CONSTRAINT d_typreferent_pkey PRIMARY KEY (typreferent_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.d_typreferent
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.d_typreferent TO cen_user;

INSERT INTO sites.d_typreferent VALUES (1, 'Référent principal', NULL);
INSERT INTO sites.d_typreferent VALUES (2, 'Référent scientifique', NULL);
INSERT INTO sites.d_typreferent VALUES (3, 'Référent foncier', NULL);
INSERT INTO sites.d_typreferent VALUES (5, 'Référent animation territoriale', NULL);
INSERT INTO sites.d_typreferent VALUES (4, 'Référent travaux', NULL);

-- Table: sites.r_sites_referents
DROP TABLE IF EXISTS sites.r_sites_referents;
CREATE TABLE IF NOT EXISTS sites.r_sites_referents
(
  site_id character varying(10) NOT NULL,
  personne_id integer NOT NULL,
  typreferent_id smallint NOT NULL,
  CONSTRAINT r_sites_referents_pkey PRIMARY KEY (site_id, personne_id, typreferent_id),
  CONSTRAINT r_sites_referents_personne_id_fkey FOREIGN KEY (personne_id)
      REFERENCES personnes.personne (personne_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_sites_referents_site_id_fkey FOREIGN KEY (site_id)
      REFERENCES sites.sites (site_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_sites_referents_typreferent_id_fkey FOREIGN KEY (typreferent_id)
      REFERENCES sites.d_typreferent (typreferent_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.r_sites_referents
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.r_sites_referents TO cen_user;
COMMENT ON TABLE sites.r_sites_referents
  IS 'Référents sur les sites';

CREATE INDEX fki_r_sites_referents_personne_id_fkey
  ON sites.r_sites_referents
  USING btree
  (personne_id);
CREATE INDEX fki_r_sites_referents_site_id_fkey
  ON sites.r_sites_referents
  USING btree
  (site_id COLLATE pg_catalog."default");
CREATE INDEX fki_r_sites_referents_typreferent_id_fkey
  ON sites.r_sites_referents
  USING btree
  (typreferent_id);

-- Table: sites.d_typmetasite
DROP TABLE IF EXISTS sites.d_typmetasite;
CREATE TABLE IF NOT EXISTS sites.d_typmetasite
(
  typmetasite_id smallint NOT NULL,
  typmetasite_lib character varying(50) NOT NULL,
  CONSTRAINT d_typmetasite_pkey PRIMARY KEY (typmetasite_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.d_typmetasite
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.d_typmetasite TO cen_user;

INSERT INTO sites.d_typmetasite VALUES (1, 'Projet / Code analytique');
INSERT INTO sites.d_typmetasite VALUES (2, 'Natura 2000');
INSERT INTO sites.d_typmetasite VALUES (3, 'Réserve naturelle');

-- Table: sites.metasites
DROP TABLE IF EXISTS sites.metasites;
CREATE TABLE IF NOT EXISTS sites.metasites
(
  metasite_id character varying(10) NOT NULL,
  metasite_nom text,
  typmetasite_id smallint NOT NULL,
  surf_m2 numeric(10,2),
  geom geometry(MultiPolygon,2154),
  CONSTRAINT metasites_pkey PRIMARY KEY (metasite_id),
  CONSTRAINT sites_typmetasite_id_fkey FOREIGN KEY (typmetasite_id)
      REFERENCES sites.d_typmetasite (typmetasite_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.metasites
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.metasites TO cen_user;

CREATE INDEX metasites_geom_gist
  ON sites.metasites
  USING gist
  (geom);

-- Table: sites.r_sites_metasites
DROP TABLE IF EXISTS sites.r_sites_metasites;
CREATE TABLE IF NOT EXISTS sites.r_sites_metasites
(
  site_id character varying(10) NOT NULL,
  metasite_id character varying(10) NOT NULL,
  CONSTRAINT r_sites_metasites_pkey PRIMARY KEY (site_id, metasite_id),
  CONSTRAINT r_sites_metasites_metasite_id_fkey FOREIGN KEY (metasite_id)
      REFERENCES sites.metasites (metasite_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_sites_metasites_site_id_fkey FOREIGN KEY (site_id)
      REFERENCES sites.sites (site_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sites.r_sites_metasites
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.r_sites_metasites TO cen_user;

CREATE INDEX fki_r_sites_metasites_metasite_id_fkey
  ON sites.r_sites_metasites
  USING btree
  (metasite_id COLLATE pg_catalog."default");
CREATE INDEX fki_r_sites_metasites_site_id_fkey
  ON sites.r_sites_metasites
  USING btree
  (site_id COLLATE pg_catalog."default");


/* Schéma foncier : fonctions */  

-- Function: foncier.c_agri_is_agri(integer)
DROP FUNCTION IF EXISTS foncier.c_agri_is_agri(integer);
CREATE OR REPLACE FUNCTION foncier.c_agri_is_agri(id_agri integer)
  RETURNS boolean AS
$BODY$
DECLARE
    enregistrement record;
BEGIN
RETURN (id_agri IN (SELECT agri_id FROM personnes.personne JOIN personnes.d_fonctpers USING(fonctpers_id) WHERE fonctpers_lib = 'Agriculteur'))
OR id_agri IS NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION foncier.c_agri_is_agri(integer)
  OWNER TO grp_sig;
COMMENT ON FUNCTION foncier.c_agri_is_agri(integer) IS 'Fonction permettant de savoir si la personne est bien agriculteur (cf. table => foncier.gp_conventions).';

-- Function: foncier.c_notaire_is_notaire(integer)
DROP FUNCTION IF EXISTS foncier.c_notaire_is_notaire(integer);
CREATE OR REPLACE FUNCTION foncier.c_notaire_is_notaire(id_notaire integer)
  RETURNS boolean AS
$BODY$
DECLARE
    enregistrement record;
BEGIN
RETURN (id_notaire IN (SELECT personne_id FROM personnes.personne JOIN personnes.d_fonctpers USING(fonctpers_id) WHERE fonctpers_lib = 'Notaire'))
OR id_notaire IS NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION foncier.c_notaire_is_notaire(integer)
  OWNER TO grp_sig;
COMMENT ON FUNCTION foncier.c_notaire_is_notaire(integer) IS 'Fonction permettant de savoir si la personne est bien notaire (cf. table => foncier.mf_acquisitions).';

-- Function: foncier.parcelles_conventions_on_update()
DROP FUNCTION IF EXISTS foncier.parcelles_conventions_on_update();
CREATE OR REPLACE FUNCTION foncier.parcelles_conventions_on_update()
  RETURNS trigger AS
$BODY$
  DECLARE

    parcelle_origine character varying(14);
    nb_lot integer;
    
    surface_conv_origine integer;
    geometry_origine geometry;
    
    contains text;
    surface_conv integer;
    
  BEGIN
	
	parcelle_origine := (SELECT par_id FROM foncier.cadastre_site JOIN cadastre.cadastre_cen USING (cad_cen_id) JOIN cadastre.lots_cen USING (lot_id) WHERE cadastre_site.cad_site_id = OLD.cad_site_id);
	geometry_origine := (SELECT t1.geom FROM cadastre.parcelles_cen t1 JOIN cadastre.lots_cen t2 ON (t2.par_id = t1.par_id) JOIN cadastre.cadastre_cen t3 ON (t3.lot_id = t2.lot_id) JOIN foncier.cadastre_site t4 ON (t4.cad_cen_id = t3.cad_cen_id) WHERE t4.cad_site_id = OLD.cad_site_id);
	nb_lot := (SELECT count(*) FROM cadastre.lots_cen WHERE par_id = parcelle_origine);
	
	surface_conv_origine := (SELECT round((t1.surf_mu::numeric /100), 0)*100 FROM foncier.r_cad_site_mu t1 WHERE t1.cad_site_id = OLD.cad_site_id AND t1.mu_id = OLD.mu_id);
	
	contains := (st_contains(st_multi(st_buffer(geometry_origine, 0.5))::geometry(MultiPolygon,2154), st_multi(st_buffer(NEW.geom, -0.5))::geometry(MultiPolygon,2154))); --0.000000005
	surface_conv := (round((st_area(NEW.geom)::numeric / nb_lot /100), 0)*100);
	
	IF (contains = 't' OR contains = 'true') THEN
		IF (surface_conv_origine IS NULL) THEN
			UPDATE foncier.r_cad_site_mu SET geom = NEW.geom, surf_mu = surface_conv WHERE cad_site_id = OLD.cad_site_id AND mu_id = OLD.mu_id;
			UPDATE foncier.mu_conventions SET maj_date = (SELECT LEFT(current_timestamp(0)::text, 19)), maj_user = (SELECT t1.utilisateur_id FROM admin_sig.utilisateurs t1, pg_user t2 WHERE t1.oid = t2.usesysid AND t2.usename = "current_user"()) WHERE mu_id = OLD.mu_id;
			IF (nb_lot > 1) THEN
				UPDATE foncier.r_cad_site_mu SET geom = NEW.geom, surf_mu = surface_conv WHERE cad_site_id IN (SELECT cad_site_id FROM foncier.cadastre_site JOIN cadastre.cadastre_cen USING (cad_cen_id) JOIN cadastre.lots_cen USING (lot_id) WHERE par_id = parcelle_origine) AND mu_id = OLD.mu_id;
			END IF;
		ELSE
			IF (surface_conv_origine > surface_conv) THEN
				RAISE EXCEPTION 'La surface obtenue est trop petite - % pour %', surface_conv, surface_conv_origine;
			ELSIF (surface_conv_origine < surface_conv) THEN
				RAISE EXCEPTION 'La surface obtenue est trop grande - % pour %', surface_conv, surface_conv_origine;
			ELSIF (surface_conv_origine = surface_conv) THEN
				UPDATE foncier.r_cad_site_mu SET geom = NEW.geom WHERE cad_site_id = OLD.cad_site_id AND mu_id = OLD.mu_id;
				UPDATE foncier.mu_conventions SET maj_date = (SELECT LEFT(current_timestamp(0)::text, 19)), maj_user = (SELECT t1.utilisateur_id FROM admin_sig.utilisateurs t1, pg_user t2 WHERE t1.oid = t2.usesysid AND t2.usename = "current_user"()) WHERE mu_id = OLD.mu_id;
				IF (nb_lot > 1) THEN
					UPDATE foncier.r_cad_site_mu SET geom = NEW.geom WHERE cad_site_id IN (SELECT cad_site_id FROM foncier.cadastre_site JOIN cadastre.cadastre_cen USING (cad_cen_id) JOIN cadastre.lots_cen USING (lot_id) WHERE par_id = parcelle_origine) AND mu_id = OLD.mu_id;
				END IF;
			END IF;
		END IF;
	ELSE
		RAISE EXCEPTION 'Votre modification ne peut pas être supérieur à la parcelle d''origine - % - %', contains, OLD.cad_site_id;
	END IF;
	
  RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION foncier.parcelles_conventions_on_update()
  OWNER TO grp_sig;


/* Schéma foncier : dictionnaires */

-- Table: foncier.d_statutgp
DROP TABLE IF EXISTS foncier.d_statutgp;
CREATE TABLE IF NOT EXISTS foncier.d_statutgp
(
  statutgp_id smallint NOT NULL,
  statutgp character varying(25),
  CONSTRAINT d_statutgp_pkey PRIMARY KEY (statutgp_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_statutgp
  OWNER TO grp_sig;
COMMENT ON TABLE foncier.d_statutgp
  IS 'Statut d''une gestion partenariale';

INSERT INTO foncier.d_statutgp VALUES (1, 'Signée en cours');
INSERT INTO foncier.d_statutgp VALUES (2, 'Terminée');

-- Table: foncier.d_statutmf
DROP TABLE IF EXISTS foncier.d_statutmf;
CREATE TABLE IF NOT EXISTS foncier.d_statutmf
(
  statutmf_id smallint NOT NULL,
  statutmf character varying(25),
  CONSTRAINT d_statutmf_pkey PRIMARY KEY (statutmf_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_statutmf
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.d_statutmf TO cen_user;
COMMENT ON TABLE foncier.d_statutmf
  IS 'Statut d''une acquisition';

INSERT INTO foncier.d_statutmf VALUES (0, 'Projet');
INSERT INTO foncier.d_statutmf VALUES (3, 'Échec');
INSERT INTO foncier.d_statutmf VALUES (1, 'Réalisée');
INSERT INTO foncier.d_statutmf VALUES (2, 'Vendue');

CREATE OR REPLACE RULE "DELETE_D_STATUTMF" AS
    ON DELETE TO foncier.d_statutmf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "INSERT_D_STATUTMF" AS
    ON INSERT TO foncier.d_statutmf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "UPDATE_D_STATUTMF" AS
    ON UPDATE TO foncier.d_statutmf DO INSTEAD NOTHING;

-- Table: foncier.d_statutmu
DROP TABLE IF EXISTS foncier.d_statutmu;
CREATE TABLE IF NOT EXISTS foncier.d_statutmu
(
  statutmu_id smallint NOT NULL,
  statutmu character varying(25),
  CONSTRAINT d_statutmu_pkey PRIMARY KEY (statutmu_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_statutmu
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.d_statutmu TO cen_user;
COMMENT ON TABLE foncier.d_statutmu
  IS 'Statut d''une convention';

INSERT INTO foncier.d_statutmu VALUES (0, 'Projet');
INSERT INTO foncier.d_statutmu VALUES (3, 'Échec');
INSERT INTO foncier.d_statutmu VALUES (1, 'Signée en cours');
INSERT INTO foncier.d_statutmu VALUES (4, 'Dénoncée');
INSERT INTO foncier.d_statutmu VALUES (2, 'Échue');

CREATE OR REPLACE RULE "DELETE_D_STATUTMU" AS
    ON DELETE TO foncier.d_statutmu DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "INSERT_D_STATUTMU" AS
    ON INSERT TO foncier.d_statutmu DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "UPDATE_D_STATUTMU" AS
    ON UPDATE TO foncier.d_statutmu DO INSTEAD NOTHING;

-- Table: foncier.d_typfraismf
DROP TABLE IF EXISTS foncier.d_typfraismf;
CREATE TABLE IF NOT EXISTS foncier.d_typfraismf
(
  typfraismf_id smallint NOT NULL,
  typfraismf_lib character varying(50),
  CONSTRAINT d_typfraismf_pkey PRIMARY KEY (typfraismf_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_typfraismf
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.d_typfraismf TO cen_user;
COMMENT ON TABLE foncier.d_typfraismf
  IS 'Types de frais engagés sur l''acquisition';

INSERT INTO foncier.d_typfraismf VALUES (3, 'Remboursement sur frais');
INSERT INTO foncier.d_typfraismf VALUES (4, 'Usufruit');
INSERT INTO foncier.d_typfraismf VALUES (5, 'Acquisition par un tiers');
INSERT INTO foncier.d_typfraismf VALUES (1, 'Acquisition par le Cen');
INSERT INTO foncier.d_typfraismf VALUES (2, 'Provision frais d''acquisition du Cen');

CREATE OR REPLACE RULE "DELETE_D_TYPFRAISMF" AS
    ON DELETE TO foncier.d_typfraismf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "INSERT_D_TYPFRAISMF" AS
    ON INSERT TO foncier.d_typfraismf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "UPDATE_D_TYPFRAISMF" AS
    ON UPDATE TO foncier.d_typfraismf DO INSTEAD NOTHING;

-- Table: foncier.d_typfraismu
DROP TABLE IF EXISTS foncier.d_typfraismu;
CREATE TABLE IF NOT EXISTS foncier.d_typfraismu
(
  typfraismu_id smallint NOT NULL,
  typfraismu_lib character varying(50),
  CONSTRAINT d_typfraismu_pkey PRIMARY KEY (typfraismu_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_typfraismu
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.d_typfraismu TO cen_user;
COMMENT ON TABLE foncier.d_typfraismu
  IS 'Types de frais engagés sur la convention';

INSERT INTO foncier.d_typfraismu VALUES (1, 'Frais d''acte');
INSERT INTO foncier.d_typfraismu VALUES (2, 'Redevance annuelle au propriétaire');

CREATE OR REPLACE RULE "DELETE_D_TYPFRAISMU" AS
    ON DELETE TO foncier.d_typfraismu DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "INSERT_D_TYPFRAISMU" AS
    ON INSERT TO foncier.d_typfraismu DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "UPDATE_D_TYPFRAISMU" AS
    ON UPDATE TO foncier.d_typfraismu DO INSTEAD NOTHING;

-- Table: foncier.d_typgp
DROP TABLE IF EXISTS foncier.d_typgp;
CREATE TABLE IF NOT EXISTS foncier.d_typgp
(
  typgp_id smallint NOT NULL,
  typgp_lib character varying(50),
  CONSTRAINT d_typgp_pkey PRIMARY KEY (typgp_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_typgp
  OWNER TO grp_sig;
COMMENT ON TABLE foncier.d_typgp
  IS 'GP, type de gestion partenariale';

INSERT INTO foncier.d_typgp VALUES (1, 'Prêt à usage');
INSERT INTO foncier.d_typgp VALUES (2, 'Bail rural');
INSERT INTO foncier.d_typgp VALUES (3, 'Bail rural à clauses environnementales');
INSERT INTO foncier.d_typgp VALUES (4, 'Convention plurianuelle de pâturage');
INSERT INTO foncier.d_typgp VALUES (5, 'Convention d''assistance technique ou scientifique');

-- Table: foncier.d_typmf
DROP TABLE IF EXISTS foncier.d_typmf;
CREATE TABLE IF NOT EXISTS foncier.d_typmf
(
  typmf_id smallint NOT NULL,
  typmf_lib character varying(50),
  CONSTRAINT d_typmf_pkey PRIMARY KEY (typmf_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_typmf
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.d_typmf TO cen_user;
COMMENT ON TABLE foncier.d_typmf
  IS 'MF, type de maîtrise foncière de l''acquisition';

INSERT INTO foncier.d_typmf VALUES (1, 'Acquisition - Pleine propriété');
INSERT INTO foncier.d_typmf VALUES (2, 'Usufruit');
INSERT INTO foncier.d_typmf VALUES (5, 'Nue-propriété');
INSERT INTO foncier.d_typmf VALUES (11, 'Acquisition en indivision');

CREATE OR REPLACE RULE "DELETE_D_TYPMF" AS
    ON DELETE TO foncier.d_statutmf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "INSERT_D_TYPMF" AS
    ON INSERT TO foncier.d_statutmf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "UPDATE_D_TYPMF" AS
    ON UPDATE TO foncier.d_statutmf DO INSTEAD NOTHING;

-- Table: foncier.d_typmu
DROP TABLE IF EXISTS foncier.d_typmu;
CREATE TABLE IF NOT EXISTS foncier.d_typmu
(
  typmu_id smallint NOT NULL,
  typmu_lib character varying(50),
  mfu boolean,
  CONSTRAINT d_typmu_pkey PRIMARY KEY (typmu_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_typmu
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.d_typmu TO cen_user;
COMMENT ON TABLE foncier.d_typmu
  IS 'MU, type de maîtrise d''usage de la convention';

INSERT INTO foncier.d_typmu VALUES (1, 'Bail civil / Convention d''usage', true);
INSERT INTO foncier.d_typmu VALUES (2, 'Commodat', true);
INSERT INTO foncier.d_typmu VALUES (3, 'Bail emphytéotique', true);
INSERT INTO foncier.d_typmu VALUES (6, 'Convention de gestion', true);
INSERT INTO foncier.d_typmu VALUES (7, 'Autorisation d''occupation temporaire (AOT)', true);
INSERT INTO foncier.d_typmu VALUES (8, 'Accord oral', false);
INSERT INTO foncier.d_typmu VALUES (11, 'Convention d''usage au bénéfice d''une collectivité', false);
INSERT INTO foncier.d_typmu VALUES (12, 'Convention de gestion sur terrain du CELRL', true);
INSERT INTO foncier.d_typmu VALUES (13, 'Convention de partenariat sur terrains militaires', true);
INSERT INTO foncier.d_typmu VALUES (14, 'Convention de mise à disposition SAFER', true);
INSERT INTO foncier.d_typmu VALUES (15, 'Convention d''Occupation Précaire (COP)', true);
INSERT INTO foncier.d_typmu VALUES (16, 'Amodiation', true);
INSERT INTO foncier.d_typmu VALUES (18, 'Servitude conventionnelle', true);
INSERT INTO foncier.d_typmu VALUES (20, 'Gestion partenariale', false);
INSERT INTO foncier.d_typmu VALUES (4, 'Location', true);
INSERT INTO foncier.d_typmu VALUES (9, 'Autre (en maîtrise)', true);
INSERT INTO foncier.d_typmu VALUES (10, 'Autre (hors maîtrise)', false);
INSERT INTO foncier.d_typmu VALUES (17, 'Convention de gestion agricole', false);

CREATE OR REPLACE RULE "DELETE_D_TYPMU" AS
    ON DELETE TO foncier.d_statutmf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "INSERT_D_TYPMU" AS
    ON INSERT TO foncier.d_statutmf DO INSTEAD NOTHING;
CREATE OR REPLACE RULE "UPDATE_D_TYPMU" AS
    ON UPDATE TO foncier.d_statutmf DO INSTEAD NOTHING;


/* Schéma foncier : tables */

-- Table: foncier.cadastre_site
DROP SEQUENCE IF EXISTS foncier.cadastre_site_cad_site_id_seq;
CREATE SEQUENCE foncier.cadastre_site_cad_site_id_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 9223372036854775807
  START 0
  CACHE 1;
ALTER TABLE foncier.cadastre_site_cad_site_id_seq
  OWNER TO grp_sig;
SELECT setval('foncier.cadastre_site_cad_site_id_seq', 0, true);

DROP TABLE IF EXISTS foncier.cadastre_site;
CREATE TABLE IF NOT EXISTS foncier.cadastre_site
(
  cad_site_id integer NOT NULL DEFAULT nextval('foncier.cadastre_site_cad_site_id_seq'::regclass), -- N° auto
  cad_cen_id integer NOT NULL, -- Identifiant de cadastre_cen
  site_id character varying(10) NOT NULL, -- Code du site
  date_deb character varying(10), -- Date d'entrée de la parcelle associée au site
  date_fin character varying(10), -- Date de fin de la parcelle associée au site - à renseigner uniquement au cas où la parcelle existe toujours mais plus reliée au site
  maj_user character varying(25),
  maj_date character varying(19),
  CONSTRAINT cadastre_site_pkey PRIMARY KEY (cad_site_id),
  CONSTRAINT cadastre_site_cad_cen_id_fkey FOREIGN KEY (cad_cen_id)
      REFERENCES cadastre.cadastre_cen (cad_cen_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cadastre_site_site_id_fkey FOREIGN KEY (site_id)
      REFERENCES sites.sites (site_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cadastre_site_cad_cen_id_date_fin_is_unique UNIQUE (cad_cen_id, date_fin)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.cadastre_site
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.cadastre_site TO cen_user;
GRANT UPDATE ON TABLE foncier.cadastre_site TO grp_foncier;
COMMENT ON TABLE foncier.cadastre_site
  IS 'Foncier sur nos sites. Permet de gérer et d''historiser le parcellaire de nos sites (paf, mfu). Les géométries des parcelles/ lots devront être récupérées dans les tables parcelles_cen et lots_cen. Si une parcelle est gérée "en partie", il faudra stocker cette géométrie (découpée) ici. Rien n''est supprimé => historique.';
COMMENT ON COLUMN foncier.cadastre_site.cad_site_id IS 'N° auto';
COMMENT ON COLUMN foncier.cadastre_site.cad_cen_id IS 'Identifiant de cadastre_cen';
COMMENT ON COLUMN foncier.cadastre_site.site_id IS 'Code du site';
COMMENT ON COLUMN foncier.cadastre_site.date_deb IS 'Date d''entrée de la parcelle associée au site';
COMMENT ON COLUMN foncier.cadastre_site.date_fin IS 'Date de fin de la parcelle associée au site - à renseigner uniquement au cas où la parcelle existe toujours mais plus reliée au site';

CREATE INDEX fki_cadastre_site_cad_cen_id_fkey
  ON foncier.cadastre_site
  USING btree
  (cad_cen_id);
CREATE INDEX fki_cadastre_site_site_id_fkey
  ON foncier.cadastre_site
  USING btree
  (site_id);

-- Table: foncier.gp_conventions
DROP TABLE IF EXISTS foncier.gp_conventions;
CREATE TABLE IF NOT EXISTS foncier.gp_conventions
(
  gp_id character varying(20) NOT NULL,
  gp_lib text,
  gp_gestion text,
  typgp_id smallint,
  date_sign_conv character varying(10),
  gp_surf_conv double precision,
  agri_id integer,
  CONSTRAINT gp_conventions_pkey PRIMARY KEY (gp_id),
  CONSTRAINT gp_conventions_agri_id_fkey FOREIGN KEY (agri_id)
      REFERENCES personnes.personne (personne_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT gp_conventions_typgp_id_fkey FOREIGN KEY (typgp_id)
      REFERENCES foncier.d_typgp (typgp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT agri_is_agri CHECK (foncier.c_agri_is_agri(agri_id))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.gp_conventions
  OWNER TO grp_sig;
COMMENT ON TABLE foncier.gp_conventions
  IS 'Gestion partenariale';

CREATE INDEX fki_gp_conventions_agri_id_fkey
  ON foncier.gp_conventions
  USING btree
  (agri_id);
CREATE INDEX fki_gp_conventions_typgp_id_fkey
  ON foncier.gp_conventions
  USING btree
  (typgp_id);

-- Table: foncier.mf_acquisitions
DROP TABLE IF EXISTS foncier.mf_acquisitions;
CREATE TABLE IF NOT EXISTS foncier.mf_acquisitions
(
  mf_id character varying(20) NOT NULL,
  mf_lib text,
  interloc_id integer,
  typmf_id smallint,
  date_compromis_pv_proprio character varying(10),
  date_compromis_pa_president character varying(10),
  date_delib_ca character varying(10),
  date_sign_acte character varying(10),
  notaire_id integer,
  notaire_contact character varying(50),
  mf_courier_recep_notaire boolean,
  mf_courier_recep_notaire_date character varying(10),
  mf_clauses_part boolean,
  mf_commentaires text,
  mf_pdf text,
  maj_user character varying(25),
  maj_date character varying(19),
  CONSTRAINT mf_acquisitions_pkey PRIMARY KEY (mf_id),
  CONSTRAINT mf_acquisitions_interloc_id_fkey FOREIGN KEY (interloc_id)
      REFERENCES personnes.personne (personne_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mf_acquisitions_notaire_id_fkey FOREIGN KEY (notaire_id)
      REFERENCES personnes.personne (personne_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mf_acquisitions_typmf_id_fkey FOREIGN KEY (typmf_id)
      REFERENCES foncier.d_typmf (typmf_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT notaire_is_notaire CHECK (foncier.c_notaire_is_notaire(notaire_id))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.mf_acquisitions
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.mf_acquisitions TO cen_user;
GRANT UPDATE, INSERT ON TABLE foncier.mf_acquisitions TO grp_foncier;
COMMENT ON TABLE foncier.mf_acquisitions
  IS 'Acquisitions';

CREATE INDEX fki_mf_acquisitions_notaire_id_fkey
  ON foncier.mf_acquisitions
  USING btree
  (notaire_id);
CREATE INDEX fki_mf_acquisitions_typmf_id_fkey
  ON foncier.mf_acquisitions
  USING btree
  (typmf_id);

-- Table: foncier.mu_conventions
DROP TABLE IF EXISTS foncier.mu_conventions;
CREATE TABLE IF NOT EXISTS foncier.mu_conventions
(
  mu_id character varying(20) NOT NULL,
  mu_lib text,
  interloc_id integer,
  typmu_id smallint,
  date_sign_conv character varying(10),
  date_effet_conv character varying(10),
  date_fin_conv character varying(10),
  mu_duree integer, -- en mois
  recond_tacite boolean,
  recond_nbmax integer, -- -1 => infini ; 0 => pas de reconduction
  recond_duree integer, -- en mois
  recond_daterappel character varying(10),
  mu_courier_envoi_proprio boolean,
  mu_courier_envoi_proprio_date character varying(10),
  mu_clauses_part boolean,
  mu_commentaires text,
  mu_pdf text,
  maj_user character varying(25),
  maj_date character varying(19),
  CONSTRAINT mu_conventions_pkey PRIMARY KEY (mu_id),
  CONSTRAINT mu_conventions_interloc_id_fkey FOREIGN KEY (interloc_id)
      REFERENCES personnes.personne (personne_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mu_conventions_typmu_id_fkey FOREIGN KEY (typmu_id)
      REFERENCES foncier.d_typmu (typmu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.mu_conventions
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.mu_conventions TO cen_user;
GRANT UPDATE, INSERT ON TABLE foncier.mu_conventions TO grp_foncier;
COMMENT ON TABLE foncier.mu_conventions
  IS 'Conventions';
COMMENT ON COLUMN foncier.mu_conventions.mu_duree IS 'en mois';
COMMENT ON COLUMN foncier.mu_conventions.recond_nbmax IS '-1 => infini ; 0 => pas de reconduction';
COMMENT ON COLUMN foncier.mu_conventions.recond_duree IS 'en mois';

CREATE INDEX fki_mu_conventions_typmu_id_fkey
  ON foncier.mu_conventions
  USING btree
  (typmu_id);

-- Table: foncier.r_cad_site_gp
DROP TABLE IF EXISTS foncier.r_cad_site_gp;
CREATE TABLE IF NOT EXISTS foncier.r_cad_site_gp
(
  cad_site_id integer NOT NULL,
  gp_id character varying(20) NOT NULL,
  date_entree character varying(10),
  date_sortie character varying(10),
  CONSTRAINT r_cad_site_gp_pkey PRIMARY KEY (cad_site_id, gp_id),
  CONSTRAINT r_cad_site_gp_cad_site_id_fkey FOREIGN KEY (cad_site_id)
      REFERENCES foncier.cadastre_site (cad_site_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_cad_site_gp_gp_id_fkey FOREIGN KEY (gp_id)
      REFERENCES foncier.gp_conventions (gp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_cad_site_gp
  OWNER TO grp_sig;
COMMENT ON TABLE foncier.r_cad_site_gp
  IS 'Relations entre cadastre_site et gp_conventions. Quelles parcelles sont concernées par la gestion partenariale ?';

CREATE INDEX fki_r_cad_site_gp_cad_site_id_fkey
  ON foncier.r_cad_site_gp
  USING btree
  (cad_site_id);
CREATE INDEX fki_r_cad_site_gp_gp_id_fkey
  ON foncier.r_cad_site_gp
  USING btree
  (gp_id COLLATE pg_catalog."default");

-- Table: foncier.r_cad_site_mf
DROP TABLE IF EXISTS foncier.r_cad_site_mf;
CREATE TABLE IF NOT EXISTS foncier.r_cad_site_mf
(
  cad_site_id integer NOT NULL,
  mf_id character varying(20) NOT NULL,
  date_entree character varying(19),
  date_sortie character varying(19),
  CONSTRAINT r_cad_site_mf_pkey PRIMARY KEY (cad_site_id, mf_id),
  CONSTRAINT r_cad_site_mf_cad_site_id_fkey FOREIGN KEY (cad_site_id)
      REFERENCES foncier.cadastre_site (cad_site_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_cad_site_mf_mf_id_fkey FOREIGN KEY (mf_id)
      REFERENCES foncier.mf_acquisitions (mf_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_cad_site_mf
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.r_cad_site_mf TO cen_user;
GRANT UPDATE, INSERT, DELETE ON TABLE foncier.r_cad_site_mf TO grp_foncier;
COMMENT ON TABLE foncier.r_cad_site_mf
  IS 'Relations entre cadastre_site et mf_acquisitions. Quelles parcelles sont concernées par l''acquisition ?';

CREATE INDEX fki_r_cad_site_mf_cad_site_id_fkey
  ON foncier.r_cad_site_mf
  USING btree
  (cad_site_id);
CREATE INDEX fki_r_cad_site_mf_mf_id_fkey
  ON foncier.r_cad_site_mf
  USING btree
  (mf_id COLLATE pg_catalog."default");

-- Table: foncier.r_cad_site_mu
DROP TABLE IF EXISTS foncier.r_cad_site_mu;
CREATE TABLE IF NOT EXISTS foncier.r_cad_site_mu
(
  cad_site_id integer NOT NULL,
  mu_id character varying(20) NOT NULL,
  date_entree character varying(19) NOT NULL,
  date_sortie character varying(19),
  surf_mu integer, -- Surface de la parcelle SEULEMENT pour les parcelles conventionnées DÉCOUPÉES = en partie...
  geom geometry(MultiPolygon,2154), -- Géométrie de la parcelle SEULEMENT pour les parcelles conventionnées DÉCOUPÉES = en partie
  CONSTRAINT r_cad_site_mu_pkey PRIMARY KEY (cad_site_id, mu_id, date_entree),
  CONSTRAINT r_cad_site_mu_cad_site_id_fkey FOREIGN KEY (cad_site_id)
      REFERENCES foncier.cadastre_site (cad_site_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_cad_site_mu_mu_id_fkey FOREIGN KEY (mu_id)
      REFERENCES foncier.mu_conventions (mu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_cad_site_mu
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.r_cad_site_mu TO cen_user;
GRANT UPDATE, INSERT, DELETE ON TABLE foncier.r_cad_site_mu TO grp_foncier;
COMMENT ON TABLE foncier.r_cad_site_mu
  IS 'Relations entre cadastre_site et mu_conventions. Quelles parcelles sont concernées par la convention ?';
COMMENT ON COLUMN foncier.r_cad_site_mu.surf_mu IS 'Surface de la parcelle SEULEMENT pour les parcelles conventionnées DÉCOUPÉES = en partie
(arrondie à l''are)';
COMMENT ON COLUMN foncier.r_cad_site_mu.geom IS 'Géométrie de la parcelle SEULEMENT pour les parcelles conventionnées DÉCOUPÉES = en partie';

CREATE INDEX fki_r_cad_site_mu_cad_site_id_fkey
  ON foncier.r_cad_site_mu
  USING btree
  (cad_site_id);
CREATE INDEX fki_r_cad_site_mu_mu_id_fkey
  ON foncier.r_cad_site_mu
  USING btree
  (mu_id COLLATE pg_catalog."default");
CREATE INDEX r_cad_site_mu_geom_gist
  ON foncier.r_cad_site_mu
  USING gist
  (geom);

-- Table: foncier.r_frais_mf
DROP TABLE IF EXISTS foncier.r_frais_mf;
CREATE TABLE IF NOT EXISTS foncier.r_frais_mf
(
  mf_id character varying(20) NOT NULL,
  typfraismf_id smallint NOT NULL,
  mffrais numeric(10,2),
  CONSTRAINT r_frais_mf_pkey PRIMARY KEY (mf_id, typfraismf_id),
  CONSTRAINT r_frais_mf_mf_id_fkey FOREIGN KEY (mf_id)
      REFERENCES foncier.mf_acquisitions (mf_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_frais_mf_typfraismf_id_fkey FOREIGN KEY (typfraismf_id)
      REFERENCES foncier.d_typfraismf (typfraismf_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_frais_mf
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.r_frais_mf TO cen_user;
GRANT UPDATE, INSERT, DELETE ON TABLE foncier.r_frais_mf TO grp_foncier;
COMMENT ON TABLE foncier.r_frais_mf
  IS 'Frais engagés sur l''acquisition';

CREATE INDEX fki_r_frais_mf_mf_id_fkey
  ON foncier.r_frais_mf
  USING btree
  (mf_id COLLATE pg_catalog."default");
CREATE INDEX fki_r_frais_mf_typfraismf_id_fkey
  ON foncier.r_frais_mf
  USING btree
  (typfraismf_id);

-- Table: foncier.r_frais_mu
DROP TABLE IF EXISTS foncier.r_frais_mu;
CREATE TABLE IF NOT EXISTS foncier.r_frais_mu
(
  mu_id character varying(20) NOT NULL,
  typfraismu_id smallint NOT NULL,
  mufrais numeric(10,2),
  CONSTRAINT r_frais_mu_pkey PRIMARY KEY (mu_id, typfraismu_id),
  CONSTRAINT r_frais_mu_mu_id_fkey FOREIGN KEY (mu_id)
      REFERENCES foncier.mu_conventions (mu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_frais_mu_typfraismu_id_fkey FOREIGN KEY (typfraismu_id)
      REFERENCES foncier.d_typfraismu (typfraismu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_frais_mu
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.r_frais_mu TO cen_user;
GRANT UPDATE, INSERT, DELETE ON TABLE foncier.r_frais_mu TO grp_foncier;
COMMENT ON TABLE foncier.r_frais_mu
  IS 'Frais engagés sur la convention';

CREATE INDEX fki_r_frais_mu_mu_id_fkey
  ON foncier.r_frais_mu
  USING btree
  (mu_id COLLATE pg_catalog."default");
CREATE INDEX fki_r_frais_mu_typfraismu_id_fkey
  ON foncier.r_frais_mu
  USING btree
  (typfraismu_id);

-- Table: foncier.r_gpstatut
DROP TABLE IF EXISTS foncier.r_gpstatut;
CREATE TABLE IF NOT EXISTS foncier.r_gpstatut
(
  gp_id character varying(20) NOT NULL,
  statutgp_id smallint NOT NULL,
  date character varying(19) NOT NULL,
  CONSTRAINT r_gpstatut_pkey PRIMARY KEY (gp_id, statutgp_id, date),
  CONSTRAINT r_gpstatut_gp_id_fkey FOREIGN KEY (gp_id)
      REFERENCES foncier.gp_conventions (gp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_gpstatut_statutgp_id_fkey FOREIGN KEY (statutgp_id)
      REFERENCES foncier.d_statutgp (statutgp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_gpstatut
  OWNER TO grp_sig;
COMMENT ON TABLE foncier.r_gpstatut
  IS 'Statut en cours de la gestion partenariale';

CREATE INDEX fki_r_gpstatut_gp_id_fkey
  ON foncier.r_gpstatut
  USING btree
  (gp_id COLLATE pg_catalog."default");
CREATE INDEX fki_r_gpstatut_statutgp_id_fkey
  ON foncier.r_gpstatut
  USING btree
  (statutgp_id);

-- Table: foncier.r_mfstatut
DROP TABLE IF EXISTS foncier.r_mfstatut;
CREATE TABLE IF NOT EXISTS foncier.r_mfstatut
(
  mf_id character varying(20) NOT NULL,
  statutmf_id smallint NOT NULL,
  date character varying(19) NOT NULL,
  CONSTRAINT r_mfstatut_pkey PRIMARY KEY (mf_id, statutmf_id, date),
  CONSTRAINT r_mfstatut_mf_id_fkey FOREIGN KEY (mf_id)
      REFERENCES foncier.mf_acquisitions (mf_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_mfstatut_statutmf_id_fkey FOREIGN KEY (statutmf_id)
      REFERENCES foncier.d_statutmf (statutmf_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_mfstatut
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.r_mfstatut TO cen_user;
GRANT UPDATE, INSERT ON TABLE foncier.r_mfstatut TO grp_foncier;
COMMENT ON TABLE foncier.r_mfstatut
  IS 'Statut en cours de l''acquisition';

CREATE INDEX fki_r_mfstatut_mf_id_fkey
  ON foncier.r_mfstatut
  USING btree
  (mf_id COLLATE pg_catalog."default");
CREATE INDEX fki_r_mfstatut_statutmf_id_fkey
  ON foncier.r_mfstatut
  USING btree
  (statutmf_id);

-- Table: foncier.r_mustatut
DROP TABLE IF EXISTS foncier.r_mustatut;
CREATE TABLE IF NOT EXISTS foncier.r_mustatut
(
  mu_id character varying(20) NOT NULL,
  statutmu_id smallint NOT NULL,
  date character varying(19) NOT NULL,
  CONSTRAINT r_mustatut_pkey PRIMARY KEY (mu_id, statutmu_id, date),
  CONSTRAINT r_mustatut_mu_id_fkey FOREIGN KEY (mu_id)
      REFERENCES foncier.mu_conventions (mu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_mustatut_statutmu_id_fkey FOREIGN KEY (statutmu_id)
      REFERENCES foncier.d_statutmu (statutmu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.r_mustatut
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.r_mustatut TO cen_user;
GRANT UPDATE, INSERT ON TABLE foncier.r_mustatut TO grp_foncier;
COMMENT ON TABLE foncier.r_mustatut
  IS 'Statut en cours de la convention';

CREATE INDEX fki_r_mustatut_mu_id_fkey
  ON foncier.r_mustatut
  USING btree
  (mu_id COLLATE pg_catalog."default");
CREATE INDEX fki_r_mustatut_statutmu_id_fkey
  ON foncier.r_mustatut
  USING btree
  (statutmu_id);


/* Schéma foncier : tables d'administration */

-- Table: foncier.d_grant_elt
DROP TABLE IF EXISTS foncier.d_grant_elt;
CREATE TABLE foncier.d_grant_elt
(
  grant_elt_id smallint NOT NULL,
  grant_elt_lib character varying(80),
  grant_elt_form_id character varying(50),
  grant_elt_type_acte character varying(10),
  CONSTRAINT d_grant_elt_pkey PRIMARY KEY (grant_elt_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.d_grant_elt
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.d_grant_elt TO cen_user;

INSERT INTO foncier.d_grant_elt VALUES (1, 'Créer un nouvel acte en projet', 'N.P.', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (2, 'Modifier le statut d''un acte', 'need_acte_statut', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (3, 'Modifier la liste des parcelles d''un acte', 'acte_parc', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (4, 'Modifier le type d''acte', 'need_acte_type', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (5, 'Modifier le libellé d''un acte', 'need_acte_lib', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (6, 'Saisir ou modifier les montants d''un acte', 'acte_frais_', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (7, 'Saisir ou modifier les clauses particulières d''un acte', 'acte_chk_clauses', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (8, 'Saisir ou modifier les commentaires d''un acte', 'acte_txt_com', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (9, 'Ajouter ou modifier le pdf associé à un acte', '_acte_pdf', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (10, 'Saisir ou modifier le suivi administratif d''un acte', 'N.P.', 'acte');
INSERT INTO foncier.d_grant_elt VALUES (11, 'Saisir ou modifier la date de compromis de vente', 'acte_date_compromis', 'mf');
INSERT INTO foncier.d_grant_elt VALUES (12, 'Saisir ou modifier la date de promesse d''achat', 'acte_date_promesse', 'mf');
INSERT INTO foncier.d_grant_elt VALUES (13, 'Saisir ou modifier la date de délibération en CA', 'acte_date_delib_ca', 'mf');
INSERT INTO foncier.d_grant_elt VALUES (14, 'Saisir ou modifier la date de signature d''une acquisition', 'acte_date_signature', 'mf');
INSERT INTO foncier.d_grant_elt VALUES (15, 'Saisir ou modifier le notaire d''une acquisition', 'acte_notaire', 'mf');
INSERT INTO foncier.d_grant_elt VALUES (16, 'Saisir ou modifier la date de signature d''une convention', 'acte_date_signature', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (17, 'Saisir ou modifier la date d''effet (ou date de début) d''une convention', 'acte_date_debut', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (18, 'Saisir ou modifier la date de fin d''une convention', 'acte_date_fin', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (19, 'Saisir ou modifier la durée initiale d''une convention', 'acte_duree', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (20, 'Saisir ou modifier la reconduction tacite d''une convention', 'acte_chk_reconduction', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (21, 'Saisir ou modifier le nombre de reconduction max d''une convention', 'acte_recond_nbmax', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (22, 'Saisir ou modifier le pas de temps de la reconduction d''une convention', 'acte_pas_tps', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (23, 'Saisir ou modifier la date de rappel avant la reconduction d''une convention', 'acte_date_rappel', 'mu');
INSERT INTO foncier.d_grant_elt VALUES (24, 'Modifier une fiche propriétaire', 'modif_proprio', 'proprio');

-- Table: foncier.grant_group
DROP TABLE IF EXISTS foncier.grant_group;
CREATE TABLE foncier.grant_group
(
  grant_group_id character varying(50) NOT NULL,
  grant_elt_id smallint NOT NULL,
  grant_group_droit boolean,
  CONSTRAINT grant_group_pkey PRIMARY KEY (grant_group_id, grant_elt_id),
  CONSTRAINT grant_group_grant_elt_id_fkey FOREIGN KEY (grant_elt_id)
      REFERENCES foncier.d_grant_elt (grant_elt_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE foncier.grant_group
  OWNER TO grp_sig;
GRANT SELECT ON TABLE foncier.grant_group TO cen_user;


/* Schéma foncier : vues */

-- View: foncier.emprise_actes
DROP VIEW IF EXISTS foncier.emprise_actes;
CREATE OR REPLACE VIEW foncier.emprise_actes AS 
 SELECT mf_acquisitions.mf_id AS acte_id,
    'Acquisition'::text AS acte_type,
    mf_acquisitions.mf_lib AS acte_lib,
    d_statutmf.statutmf AS acte_statut,
    st_union(t1.geom) AS geom
   FROM foncier.cadastre_site t4,
    cadastre.cadastre_cen t3,
    cadastre.lots_cen t2,
    cadastre.parcelles_cen t1,
    foncier.r_cad_site_mf,
    foncier.mf_acquisitions,
    foncier.d_typmf,
    foncier.r_mfstatut,
    foncier.d_statutmf,
    ( SELECT r_mfstatut_1.mf_id,
            max(r_mfstatut_1.date::text) AS statut_date
           FROM foncier.r_mfstatut r_mfstatut_1
          GROUP BY r_mfstatut_1.mf_id) tt1
  WHERE t4.cad_site_id = r_cad_site_mf.cad_site_id AND r_cad_site_mf.mf_id::text = mf_acquisitions.mf_id::text AND mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id::text = r_mfstatut.mf_id::text AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id AND r_mfstatut.date::text = tt1.statut_date AND r_mfstatut.mf_id::text = tt1.mf_id::text AND t1.par_id::text = t2.par_id::text AND t2.lot_id::text = t3.lot_id::text AND t3.cad_cen_id = t4.cad_cen_id AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text
  GROUP BY mf_acquisitions.mf_id, d_statutmf.statutmf
UNION
 SELECT mu_conventions.mu_id AS acte_id,
    'Convention'::text AS acte_type,
    mu_conventions.mu_lib AS acte_lib,
    d_statutmu.statutmu AS acte_statut,
    st_union(
        CASE
            WHEN r_cad_site_mu.geom IS NOT NULL THEN r_cad_site_mu.geom
            ELSE
            CASE
                WHEN t2.geom IS NOT NULL THEN t2.geom
                ELSE
                CASE
                    WHEN t1.geom IS NOT NULL THEN t1.geom
                    ELSE NULL::geometry
                END
            END
        END) AS geom
   FROM foncier.cadastre_site t4,
    cadastre.cadastre_cen t3,
    cadastre.lots_cen t2,
    cadastre.parcelles_cen t1,
    foncier.r_cad_site_mu,
    foncier.mu_conventions,
    foncier.d_typmu,
    foncier.r_mustatut,
    foncier.d_statutmu,
    ( SELECT r_mustatut_1.mu_id,
            max(r_mustatut_1.date::text) AS statut_date
           FROM foncier.r_mustatut r_mustatut_1
          GROUP BY r_mustatut_1.mu_id) tt1
  WHERE t4.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id::text = mu_conventions.mu_id::text AND mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id::text = r_mustatut.mu_id::text AND r_mustatut.statutmu_id = d_statutmu.statutmu_id AND r_mustatut.date::text = tt1.statut_date AND r_mustatut.mu_id::text = tt1.mu_id::text AND t1.par_id::text = t2.par_id::text AND t2.lot_id::text = t3.lot_id::text AND t3.cad_cen_id = t4.cad_cen_id AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text AND r_cad_site_mu.date_sortie::text = 'N.D.'::text
  GROUP BY mu_conventions.mu_id, d_statutmu.statutmu;
ALTER TABLE foncier.emprise_actes
  OWNER TO grp_sig;

-- View: foncier.parcelles_conventions
DROP VIEW IF EXISTS foncier.parcelles_conventions;
CREATE OR REPLACE VIEW foncier.parcelles_conventions AS 
 SELECT row_number() OVER (ORDER BY t1.par_id) AS gid,
    t1.par_id,
    t1.ccosec,
    t1.dnupla,
    t4.cad_site_id,
    mu_conventions.mu_id AS acte_id,
    mu_conventions.mu_lib AS acte_lib,
    d_statutmu.statutmu AS acte_statut,
        CASE
            WHEN r_cad_site_mu.geom IS NOT NULL THEN r_cad_site_mu.geom
            ELSE
            CASE
                WHEN t2.geom IS NOT NULL THEN t2.geom
                ELSE t1.geom
            END
        END AS geom
   FROM foncier.cadastre_site t4,
    cadastre.cadastre_cen t3,
    cadastre.lots_cen t2,
    cadastre.parcelles_cen t1,
    foncier.r_cad_site_mu,
    foncier.mu_conventions,
    foncier.d_typmu,
    foncier.r_mustatut,
    foncier.d_statutmu,
    ( SELECT r_mustatut_1.mu_id,
            max(r_mustatut_1.date::text) AS statut_date
           FROM foncier.r_mustatut r_mustatut_1
          GROUP BY r_mustatut_1.mu_id) tt1,
    ( SELECT r_cad_site_mu_1.cad_site_id,
            r_cad_site_mu_1.mu_id,
            r_cad_site_mu_1.date_sortie,
            max(r_cad_site_mu_1.date_entree::text) AS date_entree
           FROM foncier.r_cad_site_mu r_cad_site_mu_1
          GROUP BY r_cad_site_mu_1.cad_site_id, r_cad_site_mu_1.mu_id, r_cad_site_mu_1.date_sortie) tt2
  WHERE t4.cad_site_id = r_cad_site_mu.cad_site_id AND r_cad_site_mu.mu_id::text = mu_conventions.mu_id::text AND mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id::text = r_mustatut.mu_id::text AND r_mustatut.statutmu_id = d_statutmu.statutmu_id AND d_statutmu.statutmu_id = 1 AND r_mustatut.date::text = tt1.statut_date AND r_mustatut.mu_id::text = tt1.mu_id::text AND t1.par_id::text = t2.par_id::text AND t2.lot_id::text = t3.lot_id::text AND t3.cad_cen_id = t4.cad_cen_id AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text AND r_cad_site_mu.date_sortie::text = 'N.D.'::text AND tt2.cad_site_id = t4.cad_site_id AND tt2.mu_id::text = mu_conventions.mu_id::text AND r_cad_site_mu.date_entree::text = tt2.date_entree AND tt2.date_sortie::text = 'N.D.'::text AND t1.geom IS NOT NULL;
ALTER TABLE foncier.parcelles_conventions
  OWNER TO grp_sig;

CREATE OR REPLACE RULE parcelles_conventions_on_delete AS
    ON DELETE TO foncier.parcelles_conventions DO INSTEAD NOTHING;
CREATE OR REPLACE RULE parcelles_conventions_on_insert AS
    ON INSERT TO foncier.parcelles_conventions DO INSTEAD NOTHING;
CREATE TRIGGER parcelles_conventions_on_update
  INSTEAD OF UPDATE
  ON foncier.parcelles_conventions
  FOR EACH ROW
  EXECUTE PROCEDURE foncier.parcelles_conventions_on_update();

-- View: foncier.parcelles_dispo
DROP VIEW IF EXISTS foncier.parcelles_dispo;
CREATE OR REPLACE VIEW foncier.parcelles_dispo AS 
 SELECT row_number() OVER (ORDER BY t1.par_id) AS gid,
    sites.site_id,
    t1.par_id,
    array_to_string(array_agg(t4.cad_site_id), ','::text) AS tbl_cad_site_id,
    t1.codcom,
    t1.ccosec,
    t1.dnupla,
    array_to_string(array_agg(t2.dnulot), ','::text) AS tbl_dnulot,
    t1.geom
   FROM foncier.cadastre_site t4,
    cadastre.cadastre_cen t3,
    cadastre.lots_cen t2,
    cadastre.parcelles_cen t1,
    sites.sites
  WHERE sites.site_id::text = t4.site_id::text AND t1.par_id::text = t2.par_id::text AND t2.lot_id::text = t3.lot_id::text AND t3.cad_cen_id = t4.cad_cen_id AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text AND NOT (t4.cad_site_id IN ( SELECT DISTINCT t4_1.cad_site_id
           FROM foncier.cadastre_site t4_1,
            cadastre.cadastre_cen t3_1,
            foncier.r_cad_site_mu,
            foncier.mu_conventions,
            foncier.r_mustatut,
            ( SELECT r_mustatut_1.mu_id,
                    max(r_mustatut_1.date::text) AS statut_date
                   FROM foncier.r_mustatut r_mustatut_1
                  GROUP BY r_mustatut_1.mu_id
                  ORDER BY r_mustatut_1.mu_id) tmax
          WHERE t3_1.cad_cen_id = t4_1.cad_cen_id AND r_cad_site_mu.cad_site_id = t4_1.cad_site_id AND r_cad_site_mu.mu_id::text = mu_conventions.mu_id::text AND r_mustatut.mu_id::text = mu_conventions.mu_id::text AND r_mustatut.date::text = tmax.statut_date AND r_mustatut.mu_id::text = tmax.mu_id::text AND r_mustatut.statutmu_id = 1::smallint AND r_cad_site_mu.date_sortie::text = 'N.D.'::text
        UNION
         SELECT DISTINCT t4_1.cad_site_id
           FROM foncier.cadastre_site t4_1,
            cadastre.cadastre_cen t3_1,
            foncier.r_cad_site_mf,
            foncier.mf_acquisitions,
            foncier.r_mfstatut,
            foncier.d_typmf,
            ( SELECT r_mfstatut_1.mf_id,
                    max(r_mfstatut_1.date::text) AS statut_date
                   FROM foncier.r_mfstatut r_mfstatut_1
                  GROUP BY r_mfstatut_1.mf_id
                  ORDER BY r_mfstatut_1.mf_id) tmax
          WHERE mf_acquisitions.typmf_id = d_typmf.typmf_id AND d_typmf.typmf_lib::text <> 'Acquisition par un tiers'::text AND t3_1.cad_cen_id = t4_1.cad_cen_id AND r_cad_site_mf.cad_site_id = t4_1.cad_site_id AND r_cad_site_mf.mf_id::text = mf_acquisitions.mf_id::text AND r_mfstatut.mf_id::text = mf_acquisitions.mf_id::text AND r_mfstatut.date::text = tmax.statut_date AND r_mfstatut.mf_id::text = tmax.mf_id::text AND r_mfstatut.statutmf_id = 1::smallint))
  GROUP BY sites.site_id, t1.codcom, t1.ccosec, t1.dnupla, t1.geom, t1.par_id
  ORDER BY sites.site_id;
ALTER TABLE foncier.parcelles_dispo
  OWNER TO grp_sig;


/* Schéma alertes : vues */

CREATE SCHEMA alertes
  AUTHORIZATION grp_sig;
GRANT USAGE ON SCHEMA alertes TO cen_user;
GRANT USAGE ON SCHEMA alertes TO first_cnx;

-- Materialized View: alertes.v_tdb_mfu
DROP MATERIALIZED VIEW IF EXISTS alertes.v_tdb_mfu;
CREATE MATERIALIZED VIEW alertes.v_tdb_mfu AS 
 SELECT 'Convention'::character varying(15) AS acte,
    array_to_string(array_agg(tsyn.site_id), ','::text) AS tbl_site_id,
    COALESCE(sum(tsyn.nbr_parcelle), 0::numeric) AS nbr_parcelle,
    mu_conventions.mu_id AS mfu_id,
    mu_conventions.typmu_id AS typmfu_id,
    d_typmu.typmu_lib AS typmfu_lib,
    d_statutmu.statutmu_id AS statutmfu_id,
    d_statutmu.statutmu AS statutmfu_lib,
    mu_conventions.date_effet_conv::text AS date_oblig_acte,
    mu_conventions.date_sign_conv AS date_sign_acte,
    mu_conventions.date_effet_conv,
    mu_conventions.date_fin_conv,
        CASE
            WHEN mu_conventions.mu_duree > 0 THEN mu_conventions.mu_duree
            ELSE 0
        END AS mu_duree,
    mu_conventions.recond_tacite,
    mu_conventions.recond_nbmax,
    mu_conventions.recond_duree,
    mu_conventions.recond_daterappel AS date_rappel,
        CASE
            WHEN t2.first_end IS NULL THEN 'N.D.'::character varying(10)
            ELSE t2.first_end::character varying(10)
        END AS first_end,
    t2.nbr_reconduction,
        CASE
            WHEN mu_conventions.recond_tacite = true AND mu_conventions.mu_duree > 0 THEN
            CASE
                WHEN 'now'::text::date::character varying(10)::text <= t2.first_end::character varying(10)::text THEN t2.first_end::character varying(10)
                ELSE
                CASE
                    WHEN t2.nbr_reconduction > mu_conventions.recond_nbmax AND mu_conventions.recond_nbmax > 0 THEN ((date(t2.first_end) + '1 mon'::interval * (mu_conventions.recond_duree * mu_conventions.recond_nbmax)::double precision))::character varying(10)
                    ELSE ((t2.first_end::date + '1 mon'::interval * (mu_conventions.recond_duree * t2.nbr_reconduction)::double precision))::character varying(10)
                END
            END
            ELSE 'N.P.'::character varying(10)
        END AS next_end,
        CASE
            WHEN mu_conventions.recond_tacite = true THEN
            CASE
                WHEN mu_conventions.mu_duree > 0 AND mu_conventions.recond_nbmax = (-1) THEN 'infini'::character varying(10)
                WHEN mu_conventions.mu_duree <= 0 OR mu_conventions.recond_nbmax = 0 THEN 'N.P.'::character varying(10)
                ELSE ((date(t2.first_end) + '1 mon'::interval * (mu_conventions.recond_duree * mu_conventions.recond_nbmax)::double precision))::character varying(10)
            END
            ELSE
            CASE
                WHEN t2.first_end IS NULL THEN 'N.P.'::character varying(10)
                ELSE t2.first_end::character varying(10)
            END
        END AS last_end,
    mu_conventions.maj_user AS maj_user_acte
   FROM foncier.mu_conventions
     JOIN ( SELECT mu_conventions_1.mu_id,
            t1.first_end,
                CASE
                    WHEN mu_conventions_1.recond_tacite = true AND mu_conventions_1.mu_duree > 0 AND mu_conventions_1.recond_duree <> 0 THEN
                    CASE
                        WHEN date_part('day'::text, age(t1.first_end)) > 0::double precision THEN admin_sig.f_special_round(((date_part('year'::text, age(t1.first_end)) * 12::double precision + date_part('month'::text, age(t1.first_end)) + 1::double precision) / mu_conventions_1.recond_duree::double precision)::numeric, 0)::integer
                        ELSE ((date_part('year'::text, age(t1.first_end)) * 12::double precision + date_part('month'::text, age(t1.first_end)) + 0::double precision) / mu_conventions_1.recond_duree::double precision - 0::double precision)::integer
                    END
                    ELSE 0
                END AS nbr_reconduction
           FROM foncier.mu_conventions mu_conventions_1
             JOIN ( SELECT mu_conventions_2.mu_id,
                        CASE
                            WHEN mu_conventions_2.date_effet_conv IS NOT NULL AND mu_conventions_2.date_effet_conv::text <> 'N.D.'::text AND mu_conventions_2.date_effet_conv::text <> ''::text AND mu_conventions_2.mu_duree > 0 THEN mu_conventions_2.date_effet_conv::date + '1 mon'::interval * mu_conventions_2.mu_duree::double precision
                            ELSE NULL::timestamp without time zone
                        END AS first_end
                   FROM foncier.mu_conventions mu_conventions_2) t1 USING (mu_id)) t2 USING (mu_id)
     LEFT JOIN admin_sig.utilisateurs ON mu_conventions.maj_user::text = utilisateurs.utilisateur_id::text
     LEFT JOIN personnes.d_individu USING (individu_id),
    foncier.d_typmu,
    foncier.r_mustatut,
    foncier.d_statutmu,
    ( SELECT r_mustatut_1.mu_id,
            max(r_mustatut_1.date::text) AS statut_date
           FROM foncier.r_mustatut r_mustatut_1
          GROUP BY r_mustatut_1.mu_id
          ORDER BY r_mustatut_1.mu_id) tmax
     LEFT JOIN ( SELECT sites.site_id,
            sites.site_nom,
            r_cad_site_mu.mu_id,
            count(DISTINCT t1.par_id) AS nbr_parcelle,
            sum(t1.dcntpa) AS surf_pa_total,
            count(DISTINCT t2_1.lot_id) AS nbr_lot,
            sum(t2_1.dcntlo::numeric) AS surf_lo_total,
            count(DISTINCT t3.cad_cen_id) AS nbr_cad_cen,
            count(DISTINCT t4.cad_site_id) AS nbr_cad_site
           FROM cadastre.parcelles_cen t1,
            cadastre.lots_cen t2_1,
            cadastre.cadastre_cen t3,
            foncier.cadastre_site t4,
            sites.sites,
            foncier.r_cad_site_mu
          WHERE t1.par_id::text = t2_1.par_id::text AND t2_1.lot_id::text = t3.lot_id::text AND t3.cad_cen_id = t4.cad_cen_id AND t4.cad_site_id = r_cad_site_mu.cad_site_id AND t4.site_id::text = sites.site_id::text AND r_cad_site_mu.date_sortie::text = 'N.D.'::text AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text
          GROUP BY sites.site_id, sites.site_nom, r_cad_site_mu.mu_id
          ORDER BY r_cad_site_mu.mu_id) tsyn USING (mu_id)
  WHERE mu_conventions.typmu_id = d_typmu.typmu_id AND mu_conventions.mu_id::text = r_mustatut.mu_id::text AND r_mustatut.statutmu_id = d_statutmu.statutmu_id AND r_mustatut.date::text = tmax.statut_date AND mu_conventions.mu_id::text = tmax.mu_id::text
  GROUP BY mu_conventions.mu_id, d_typmu.typmu_lib, d_statutmu.statutmu_id, t2.first_end, t2.nbr_reconduction, d_individu.individu_id
UNION
 SELECT 'Acquisition'::character varying(15) AS acte,
    array_to_string(array_agg(tsyn.site_id), ','::text) AS tbl_site_id,
    COALESCE(sum(tsyn.nbr_parcelle), 0::numeric) AS nbr_parcelle,
    mf_acquisitions.mf_id AS mfu_id,
    mf_acquisitions.typmf_id AS typmfu_id,
    d_typmf.typmf_lib AS typmfu_lib,
    d_statutmf.statutmf_id AS statutmfu_id,
    d_statutmf.statutmf AS statutmfu_lib,
    (mf_acquisitions.date_delib_ca::text || ' - '::text) || mf_acquisitions.date_sign_acte::text AS date_oblig_acte,
    mf_acquisitions.date_sign_acte,
    NULL::character varying AS date_effet_conv,
    NULL::character varying AS date_fin_conv,
    NULL::integer AS mu_duree,
    NULL::boolean AS recond_tacite,
    NULL::integer AS recond_nbmax,
    NULL::integer AS recond_duree,
    NULL::character varying AS date_rappel,
    NULL::character varying AS first_end,
    NULL::integer AS nbr_reconduction,
    NULL::character varying AS next_end,
    NULL::character varying AS last_end,
    mf_acquisitions.maj_user AS maj_user_acte
   FROM foncier.mf_acquisitions
     LEFT JOIN admin_sig.utilisateurs ON mf_acquisitions.maj_user::text = utilisateurs.utilisateur_id::text
     LEFT JOIN personnes.d_individu USING (individu_id),
    foncier.d_typmf,
    foncier.r_mfstatut,
    foncier.d_statutmf,
    ( SELECT r_mfstatut_1.mf_id,
            max(r_mfstatut_1.date::text) AS statut_date
           FROM foncier.r_mfstatut r_mfstatut_1
          GROUP BY r_mfstatut_1.mf_id
          ORDER BY r_mfstatut_1.mf_id) tmax
     LEFT JOIN ( SELECT sites.site_id,
            sites.site_nom,
            r_cad_site_mf.mf_id,
            count(DISTINCT t1.par_id) AS nbr_parcelle,
            sum(t1.dcntpa) AS surf_pa_total,
            count(DISTINCT t2.lot_id) AS nbr_lot,
            sum(t2.dcntlo::numeric) AS surf_lo_total,
            count(DISTINCT t3.cad_cen_id) AS nbr_cad_cen,
            count(DISTINCT t4.cad_site_id) AS nbr_cad_site
           FROM cadastre.parcelles_cen t1,
            cadastre.lots_cen t2,
            cadastre.cadastre_cen t3,
            foncier.cadastre_site t4,
            sites.sites,
            foncier.r_cad_site_mf
          WHERE t1.par_id::text = t2.par_id::text AND t2.lot_id::text = t3.lot_id::text AND t3.cad_cen_id = t4.cad_cen_id AND t4.cad_site_id = r_cad_site_mf.cad_site_id AND t4.site_id::text = sites.site_id::text AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text
          GROUP BY sites.site_id, sites.site_nom, r_cad_site_mf.mf_id
          ORDER BY r_cad_site_mf.mf_id) tsyn USING (mf_id)
  WHERE mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id::text = r_mfstatut.mf_id::text AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id AND r_mfstatut.date::text = tmax.statut_date AND mf_acquisitions.mf_id::text = tmax.mf_id::text
  GROUP BY mf_acquisitions.mf_id, d_typmf.typmf_lib, d_statutmf.statutmf_id, d_individu.individu_id
  ORDER BY 4
WITH DATA;
ALTER TABLE alertes.v_tdb_mfu
  OWNER TO foncier;
GRANT ALL ON TABLE alertes.v_tdb_mfu TO grp_sig;
GRANT SELECT ON TABLE alertes.v_tdb_mfu TO cen_user;
COMMENT ON MATERIALIZED VIEW alertes.v_tdb_mfu
  IS 'REFRESH MATERIALIZED VIEW alertes.v_tdb_mfu;';

-- View: alertes.v_alertes_mu
DROP VIEW IF EXISTS alertes.v_alertes_mu;
CREATE OR REPLACE VIEW alertes.v_alertes_mu AS 
 SELECT talerte.acte,
    talerte.site_id,
    talerte.mu_id,
    talerte.statutmu_id,
    talerte.date_effet_conv,
    talerte.first_end,
    talerte.mu_duree,
    talerte.recond_tacite,
    talerte.recond_nbmax,
    talerte.recond_duree,
    talerte.next_end,
    talerte.last_end,
    talerte.alerte
   FROM ( WITH tcalc AS (
                 SELECT v_tdb_mfu_1.mfu_id,
                        CASE
                            WHEN v_tdb_mfu_1.next_end::text <> 'N.P.'::text THEN - justify_interval(age('now'::text::date::timestamp with time zone, v_tdb_mfu_1.next_end::date::timestamp with time zone))
                            ELSE NULL::interval
                        END AS recond_time,
                        CASE
                            WHEN v_tdb_mfu_1.last_end::text <> 'infini'::text THEN - justify_interval(age('now'::text::date::timestamp with time zone, v_tdb_mfu_1.last_end::date::timestamp with time zone))
                            ELSE NULL::interval
                        END AS fin_time
                   FROM alertes.v_tdb_mfu v_tdb_mfu_1
                  WHERE v_tdb_mfu_1.acte::text = 'Convention'::text AND v_tdb_mfu_1.statutmfu_id = 1 AND v_tdb_mfu_1.date_effet_conv::text <> 'N.D.'::text AND v_tdb_mfu_1.mu_duree > 0
                )
         SELECT v_tdb_mfu.acte,
            v_tdb_mfu.mfu_id AS mu_id,
            v_tdb_mfu.tbl_site_id AS site_id,
            v_tdb_mfu.statutmfu_id AS statutmu_id,
            v_tdb_mfu.date_effet_conv,
            v_tdb_mfu.first_end,
            v_tdb_mfu.mu_duree,
            v_tdb_mfu.recond_tacite,
            v_tdb_mfu.recond_nbmax,
            v_tdb_mfu.recond_duree,
            v_tdb_mfu.next_end,
            v_tdb_mfu.last_end,
                CASE
                    WHEN v_tdb_mfu.last_end::text < 'now'::text::date::character varying(10)::text THEN 'terminée'::text
                    WHEN v_tdb_mfu.last_end::text = 'now'::text::date::character varying(10)::text THEN 'finie aujourd''hui'::text
                    WHEN (tcalc.recond_time = tcalc.fin_time OR tcalc.recond_time IS NULL) AND date_part('year'::text, tcalc.fin_time) = 0::double precision AND date_part('month'::text, tcalc.fin_time) = 0::double precision THEN 'fin dans moins d''un mois'::text
                    WHEN (tcalc.recond_time = tcalc.fin_time OR tcalc.recond_time IS NULL) AND date_part('year'::text, tcalc.fin_time) = 0::double precision AND date_part('month'::text, tcalc.fin_time) < 6::double precision THEN 'fin dans moins de 6 mois'::text
                    ELSE
                    CASE
                        WHEN v_tdb_mfu.next_end::text = 'now'::text::date::character varying(10)::text THEN 'reconduite aujourd''hui'::text
                        WHEN (tcalc.recond_time < tcalc.fin_time OR tcalc.fin_time IS NULL) AND date_part('year'::text, tcalc.recond_time) = 0::double precision AND date_part('month'::text, tcalc.recond_time) = 0::double precision THEN 'reconduction dans moins d''un mois'::text
                        WHEN (tcalc.recond_time < tcalc.fin_time OR tcalc.fin_time IS NULL) AND date_part('year'::text, tcalc.recond_time) = 0::double precision AND date_part('month'::text, tcalc.recond_time) < 6::double precision THEN 'reconduction dans moins de 6 mois'::text
                        ELSE 'trop loin'::text
                    END
                END AS alerte
           FROM alertes.v_tdb_mfu
             JOIN tcalc USING (mfu_id)) talerte
  WHERE talerte.alerte <> 'trop loin'::text;
ALTER TABLE alertes.v_alertes_mu
  OWNER TO grp_sig;
GRANT SELECT ON TABLE alertes.v_alertes_mu TO cen_user;

-- View: alertes.v_alertes_rappel
DROP VIEW IF EXISTS alertes.v_alertes_rappel;
CREATE OR REPLACE VIEW alertes.v_alertes_rappel AS 
 SELECT v_tdb_mfu.acte,
    v_tdb_mfu.mfu_id,
    v_tdb_mfu.date_rappel,
        CASE
            WHEN v_tdb_mfu.date_rappel::text = 'now'::text::date::character varying(10)::text THEN 'aujourd''hui'::text
            WHEN date_part('year'::text, - justify_interval(age('now'::text::date::timestamp with time zone, v_tdb_mfu.date_rappel::date::timestamp with time zone))) = 0::double precision AND date_part('month'::text, - justify_interval(age('now'::text::date::timestamp with time zone, v_tdb_mfu.date_rappel::date::timestamp with time zone))) = 0::double precision THEN 'dans moins d''un mois'::text
            WHEN date_part('year'::text, - justify_interval(age('now'::text::date::timestamp with time zone, v_tdb_mfu.date_rappel::date::timestamp with time zone))) = 0::double precision AND date_part('month'::text, - justify_interval(age('now'::text::date::timestamp with time zone, v_tdb_mfu.date_rappel::date::timestamp with time zone))) < 6::double precision THEN 'dans moins de 6 mois'::text
            ELSE NULL::text
        END AS alerte
   FROM alertes.v_tdb_mfu
  WHERE v_tdb_mfu.date_rappel IS NOT NULL AND v_tdb_mfu.date_rappel::text <> 'N.D.'::text AND v_tdb_mfu.date_rappel::text >= 'now'::text::date::character varying(10)::text;
ALTER TABLE alertes.v_alertes_rappel
  OWNER TO grp_sig;
GRANT SELECT ON TABLE alertes.v_alertes_rappel TO cen_user;

-- View: alertes.v_prob_cadastre_cen
DROP VIEW IF EXISTS alertes.v_prob_cadastre_cen;
CREATE OR REPLACE VIEW alertes.v_prob_cadastre_cen AS 
 SELECT parcelles_cen.par_id,
    lots_cen.lot_id,
    cadastre_cen.cad_cen_id,
    cadastre_cen.date_fin AS cadcen_date_fin,
    cadastre_cen.motif_fin_id AS cadcen_motif_fin,
    d_motif_fin.motif_fin_lib,
    cadastre_site.cad_site_id,
    cadastre_site.site_id,
    cadastre_site.date_fin AS cadsit_date_fin,
    'Acquisition'::character varying(15) AS acte,
    r_mfstatut.statutmf_id AS statutmfu_id,
    mf_acquisitions.mf_id AS mfu_id
   FROM cadastre.parcelles_cen
     JOIN cadastre.lots_cen USING (par_id)
     JOIN cadastre.cadastre_cen USING (lot_id)
     JOIN foncier.cadastre_site USING (cad_cen_id)
     JOIN foncier.r_cad_site_mf USING (cad_site_id)
     JOIN foncier.mf_acquisitions USING (mf_id)
     JOIN foncier.r_mfstatut USING (mf_id)
     JOIN cadastre.d_motif_fin USING (motif_fin_id)
     JOIN ( SELECT r_mfstatut_1.mf_id,
            max(r_mfstatut_1.date::text) AS statut_date
           FROM foncier.r_mfstatut r_mfstatut_1
          GROUP BY r_mfstatut_1.mf_id
          ORDER BY r_mfstatut_1.mf_id) tstatut USING (mf_id)
  WHERE cadastre_cen.date_fin::text <> 'N.D.'::text AND cadastre_site.date_fin::text = 'N.D.'::text AND r_cad_site_mf.date_sortie::text = 'N.D.'::text AND r_mfstatut.date::text = tstatut.statut_date AND (r_mfstatut.statutmf_id = ANY (ARRAY[0, 1]))
UNION
 SELECT parcelles_cen.par_id,
    lots_cen.lot_id,
    cadastre_cen.cad_cen_id,
    cadastre_cen.date_fin AS cadcen_date_fin,
    cadastre_cen.motif_fin_id AS cadcen_motif_fin,
    d_motif_fin.motif_fin_lib,
    cadastre_site.cad_site_id,
    cadastre_site.site_id,
    cadastre_site.date_fin AS cadsit_date_fin,
    'Convention'::character varying(15) AS acte,
    r_mustatut.statutmu_id AS statutmfu_id,
    mu_conventions.mu_id AS mfu_id
   FROM cadastre.parcelles_cen
     JOIN cadastre.lots_cen USING (par_id)
     JOIN cadastre.cadastre_cen USING (lot_id)
     JOIN foncier.cadastre_site USING (cad_cen_id)
     JOIN foncier.r_cad_site_mu USING (cad_site_id)
     JOIN foncier.mu_conventions USING (mu_id)
     JOIN foncier.r_mustatut USING (mu_id)
     JOIN cadastre.d_motif_fin USING (motif_fin_id)
     JOIN ( SELECT r_mustatut_1.mu_id,
            max(r_mustatut_1.date::text) AS statut_date
           FROM foncier.r_mustatut r_mustatut_1
          GROUP BY r_mustatut_1.mu_id
          ORDER BY r_mustatut_1.mu_id) tstatut USING (mu_id)
  WHERE cadastre_cen.date_fin::text <> 'N.D.'::text AND cadastre_site.date_fin::text = 'N.D.'::text AND r_cad_site_mu.date_sortie::text = 'N.D.'::text AND r_mustatut.date::text = tstatut.statut_date AND (r_mustatut.statutmu_id = ANY (ARRAY[0, 1]));
ALTER TABLE alertes.v_prob_cadastre_cen
  OWNER TO grp_sig;
GRANT SELECT ON TABLE alertes.v_prob_cadastre_cen TO cen_user;

-- View: alertes.v_prob_mfu_baddata
DROP VIEW IF EXISTS alertes.v_prob_mfu_baddata;
CREATE OR REPLACE VIEW alertes.v_prob_mfu_baddata AS 
 SELECT v_tdb_mfu.acte,
    v_tdb_mfu.tbl_site_id AS site_id,
    v_tdb_mfu.mfu_id,
    v_tdb_mfu.statutmfu_id,
    v_tdb_mfu.date_oblig_acte,
    v_tdb_mfu.mu_duree,
    v_tdb_mfu.recond_tacite,
    v_tdb_mfu.recond_duree,
    replace(btrim(
        CASE
            WHEN v_tdb_mfu.acte::text = 'Acquisition'::text AND v_tdb_mfu.statutmfu_id = 1 AND v_tdb_mfu.date_oblig_acte ~~* '%N.D.%'::text THEN (
            CASE
                WHEN "left"(v_tdb_mfu.date_oblig_acte, 4) = 'N.D.'::text THEN 'date_delib_ca'::text
                ELSE ''::text
            END || ';'::text) ||
            CASE
                WHEN "right"(v_tdb_mfu.date_oblig_acte, 4) = 'N.D.'::text THEN 'date_sign_acte'::text
                ELSE ''::text
            END
            WHEN v_tdb_mfu.acte::text = 'Convention'::text AND (v_tdb_mfu.statutmfu_id = ANY (ARRAY[1, 2, 4])) THEN (((
            CASE
                WHEN v_tdb_mfu.date_oblig_acte = 'N.D.'::text THEN 'date_effet_conv'::text
                ELSE ''::text
            END || ';'::text) ||
            CASE
                WHEN v_tdb_mfu.mu_duree <= 0 THEN 'mu_duree'::text
                ELSE ''::text
            END) || ';'::text) ||
            CASE
                WHEN v_tdb_mfu.recond_tacite = true AND v_tdb_mfu.recond_duree = 0 THEN 'recond_duree'::text
                ELSE ''::text
            END
            ELSE '???'::text
        END, ';'::text), ';;'::text, ';'::text) AS chp_baddata
   FROM alertes.v_tdb_mfu
  WHERE v_tdb_mfu.acte::text = 'Acquisition'::text AND v_tdb_mfu.statutmfu_id = 1 AND v_tdb_mfu.date_oblig_acte ~~* '%N.D.%'::text OR v_tdb_mfu.acte::text = 'Convention'::text AND (v_tdb_mfu.statutmfu_id = ANY (ARRAY[1, 2, 4])) AND (v_tdb_mfu.date_oblig_acte = 'N.D.'::text OR v_tdb_mfu.mu_duree <= 0 OR v_tdb_mfu.recond_tacite = true AND v_tdb_mfu.recond_duree = 0);
ALTER TABLE alertes.v_prob_mfu_baddata
  OWNER TO grp_sig;
GRANT SELECT ON TABLE alertes.v_prob_mfu_baddata TO cen_user;

-- View: alertes.v_prob_mfu_indiv
DROP VIEW IF EXISTS alertes.v_prob_mfu_indiv;
CREATE OR REPLACE VIEW alertes.v_prob_mfu_indiv AS 
 SELECT 'Acquisition'::character varying(15) AS acte,
    mf_acquisitions.mf_id AS mfu_id,
    d_statutmf.statutmf_id AS statutmfu_id,
    d_statutmf.statutmf AS statutmfu_lib,
    tfrais.mffrais
   FROM foncier.mf_acquisitions,
    foncier.d_typmf,
    foncier.r_mfstatut,
    foncier.d_statutmf,
    ( SELECT r_mfstatut_1.mf_id,
            max(r_mfstatut_1.date::text) AS statut_date
           FROM foncier.r_mfstatut r_mfstatut_1
          GROUP BY r_mfstatut_1.mf_id
          ORDER BY r_mfstatut_1.mf_id) tmax
     LEFT JOIN ( SELECT r_frais_mf.mf_id,
            COALESCE(r_frais_mf.mffrais, 0::numeric) AS mffrais
           FROM foncier.r_frais_mf
          WHERE r_frais_mf.typfraismf_id = 5) tfrais USING (mf_id)
  WHERE mf_acquisitions.typmf_id = d_typmf.typmf_id AND mf_acquisitions.mf_id::text = r_mfstatut.mf_id::text AND r_mfstatut.statutmf_id = d_statutmf.statutmf_id AND r_mfstatut.date::text = tmax.statut_date AND mf_acquisitions.mf_id::text = tmax.mf_id::text AND mf_acquisitions.typmf_id = 11 AND tfrais.mffrais IS NULL AND d_statutmf.statutmf_id = 1;
ALTER TABLE alertes.v_prob_mfu_indiv
  OWNER TO grp_sig;
GRANT SELECT ON TABLE alertes.v_prob_mfu_indiv TO cen_user;

-- View: alertes.v_prob_mfu_sansparc
DROP VIEW IF EXISTS alertes.v_prob_mfu_sansparc;
CREATE OR REPLACE VIEW alertes.v_prob_mfu_sansparc AS 
 SELECT v_tdb_mfu.acte,
    v_tdb_mfu.mfu_id
   FROM alertes.v_tdb_mfu
  WHERE v_tdb_mfu.nbr_parcelle = 0::numeric AND (v_tdb_mfu.statutmfu_id = ANY (ARRAY[0, 1]));
ALTER TABLE alertes.v_prob_mfu_sansparc
  OWNER TO grp_sig;
GRANT SELECT ON TABLE alertes.v_prob_mfu_sansparc TO cen_user;


/* Schéma sites : vues */

-- View: sites.v_sites
DROP VIEW IF EXISTS sites.v_sites;
CREATE OR REPLACE VIEW sites.v_sites AS 
 WITH tcommunes AS (
         SELECT t1.site_id,
            COALESCE(array_to_string(array_agg(t1.nomcomm ORDER BY t1.nomcomm), ' / '::text), ''::text) AS nomcomm
           FROM ( SELECT DISTINCT cadastre_site.site_id,
                    upper(communes.nom::text) AS nomcomm
                   FROM administratif.communes
                     JOIN cadastre.parcelles_cen ON parcelles_cen.codcom::text = communes.code_insee::text
                     JOIN cadastre.lots_cen USING (par_id)
                     JOIN cadastre.cadastre_cen USING (lot_id)
                     JOIN foncier.cadastre_site USING (cad_cen_id)
                  WHERE cadastre_cen.date_fin::text = 'N.D.'::text AND cadastre_site.date_fin::text = 'N.D.'::text) t1
          GROUP BY t1.site_id
        )
 SELECT row_number() OVER (ORDER BY sites.site_id) AS gid,
    sites.site_id,
    sites.site_nom,
    COALESCE((tcommunes.nomcomm || ' - '::text) || sites.site_nom, 'N.C. - Problème de cadastre'::text) AS site_nomcomm,
    d_typsite.typsite_lib,
    d_milieux.milieu_lib,
    sites.geom_ecolo AS geom
   FROM sites.sites
     JOIN sites.d_typsite USING (typsite_id)
     JOIN sites.d_milieux USING (milieu_id)
     LEFT JOIN tcommunes USING (site_id)
  WHERE (sites.typsite_id::text <> ALL (ARRAY['3'::character varying::text, '4'::character varying::text])) AND sites.geom_ecolo IS NOT NULL
  ORDER BY sites.site_id;
ALTER TABLE sites.v_sites
  OWNER TO grp_sig;
GRANT SELECT ON TABLE sites.v_sites TO cen_user;

-- Materialized View: sites.v_sites_parcelles
DROP MATERIALIZED VIEW IF EXISTS sites.v_sites_parcelles;
CREATE MATERIALIZED VIEW sites.v_sites_parcelles AS 
 SELECT DISTINCT row_number() OVER (ORDER BY parcelles_cen.par_id) AS gid,
    parcelles_cen.par_id,
    parcelles_cen.codcom,
    parcelles_cen.ccosec,
    parcelles_cen.dnupla,
    parcelles_cen.dcntpa,
    cadastre_site.site_id,
        CASE
            WHEN strpos(array_to_string(array_agg(COALESCE(r_mfstatut.statutmf_id::integer, 9)), '/'::text), '1'::text) > 0 AND strpos(array_to_string(array_agg(COALESCE(r_mustatut.statutmu_id::integer, 9)), '/'::text), '1'::text) > 0 THEN 'Acquisition / Convention'::text
            WHEN strpos(array_to_string(array_agg(COALESCE(r_mfstatut.statutmf_id::integer, 9)), '/'::text), '1'::text) > 0 AND strpos(array_to_string(array_agg(COALESCE(r_mustatut.statutmu_id::integer, 9)), '/'::text), '1'::text) = 0 THEN
            CASE
                WHEN strpos(array_to_string(array_agg(COALESCE(r_mfstatut.statutmf_id::integer, 9)), '/'::text), '9'::text) > 0 THEN 'Acquis en partie BND'::text
                ELSE 'Acquisition'::text
            END
            WHEN strpos(array_to_string(array_agg(COALESCE(r_mfstatut.statutmf_id::integer, 9)), '/'::text), '1'::text) = 0 AND strpos(array_to_string(array_agg(COALESCE(r_mustatut.statutmu_id::integer, 9)), '/'::text), '1'::text) > 0 THEN
            CASE
                WHEN strpos(array_to_string(array_agg(COALESCE(
                CASE
                    WHEN d_typmu.mfu = true THEN r_mustatut.statutmu_id::integer
                    WHEN d_typmu.mfu = false THEN 0
                    ELSE 9
                END, 9)), '/'::text), '0'::text) > 0 THEN 'Autre'::text
                WHEN strpos(array_to_string(array_agg(COALESCE(r_mustatut.statutmu_id::integer, 9)), '/'::text), '9'::text) > 0 THEN 'Convention en partie BND'::text
                WHEN trmu.surf_m2 IS NOT NULL THEN 'Convention pour partie'::text
                ELSE 'Convention'::text
            END
            ELSE
            CASE
                WHEN strpos(array_to_string(array_agg(COALESCE(r_mfstatut.statutmf_id::integer, 9)), '/'::text), '0'::text) > 0 OR strpos(array_to_string(array_agg(COALESCE(r_mustatut.statutmu_id::integer, 9)), '/'::text), '0'::text) > 0 THEN 'Projet en cours'::text
                ELSE 'Pas de maîtrise'::text
            END
        END AS mfu,
    st_perimeter(
        CASE
            WHEN trmu.geom IS NOT NULL THEN trmu.geom
            ELSE
            CASE
                WHEN lots_cen.geom IS NOT NULL THEN lots_cen.geom
                ELSE parcelles_cen.geom
            END
        END)::numeric(10,2) AS lng_m,
    st_area(
        CASE
            WHEN trmu.geom IS NOT NULL THEN trmu.geom
            ELSE
            CASE
                WHEN lots_cen.geom IS NOT NULL THEN lots_cen.geom
                ELSE parcelles_cen.geom
            END
        END)::numeric(10,2) AS surf_m2,
        CASE
            WHEN trmu.geom IS NOT NULL THEN trmu.geom
            ELSE
            CASE
                WHEN lots_cen.geom IS NOT NULL THEN lots_cen.geom
                ELSE parcelles_cen.geom
            END
        END AS geom
   FROM cadastre.parcelles_cen
     JOIN cadastre.lots_cen USING (par_id)
     JOIN cadastre.cadastre_cen USING (lot_id)
     JOIN foncier.cadastre_site USING (cad_cen_id)
     LEFT JOIN ( SELECT r_cad_site_mf.cad_site_id,
            r_cad_site_mf.mf_id
           FROM foncier.r_cad_site_mf
          WHERE NOT (r_cad_site_mf.mf_id::text IN ( SELECT v_prob_cadastre_cen.mfu_id
                   FROM alertes.v_prob_cadastre_cen
                UNION
                 SELECT v_prob_mfu_baddata.mfu_id
                   FROM alertes.v_prob_mfu_baddata))) trmf USING (cad_site_id)
     LEFT JOIN foncier.mf_acquisitions USING (mf_id)
     LEFT JOIN (foncier.r_mfstatut
     JOIN ( SELECT r_mfstatut_1.mf_id AS mf_ok,
            max(r_mfstatut_1.date::text) AS statut_date
           FROM foncier.r_mfstatut r_mfstatut_1
          GROUP BY r_mfstatut_1.mf_id) tmaxmf ON r_mfstatut.date::text = tmaxmf.statut_date AND r_mfstatut.mf_id::text = tmaxmf.mf_ok::text) USING (mf_id)
     LEFT JOIN ( SELECT r_cad_site_mu.cad_site_id,
            r_cad_site_mu.mu_id,
            r_cad_site_mu.date_sortie,
            r_cad_site_mu.surf_mu AS surf_m2,
            r_cad_site_mu.geom
           FROM foncier.r_cad_site_mu
          WHERE r_cad_site_mu.date_sortie::text = 'N.D.'::text AND NOT (r_cad_site_mu.mu_id::text IN ( SELECT v_alertes_mu.mu_id AS mfu_id
                   FROM alertes.v_alertes_mu
                  WHERE v_alertes_mu.alerte = 'terminée'::text
                UNION
                 SELECT v_prob_cadastre_cen.mfu_id
                   FROM alertes.v_prob_cadastre_cen
                UNION
                 SELECT v_prob_mfu_baddata.mfu_id
                   FROM alertes.v_prob_mfu_baddata))) trmu USING (cad_site_id)
     LEFT JOIN foncier.mu_conventions USING (mu_id)
     LEFT JOIN foncier.d_typmu USING (typmu_id)
     LEFT JOIN (foncier.r_mustatut
     JOIN ( SELECT r_mustatut_1.mu_id AS mu_ok,
            max(r_mustatut_1.date::text) AS statut_date
           FROM foncier.r_mustatut r_mustatut_1
          GROUP BY r_mustatut_1.mu_id
          ORDER BY r_mustatut_1.mu_id) tmaxmu ON r_mustatut.date::text = tmaxmu.statut_date AND r_mustatut.mu_id::text = tmaxmu.mu_ok::text) USING (mu_id)
  WHERE cadastre_cen.date_fin::text = 'N.D.'::text AND cadastre_site.date_fin::text = 'N.D.'::text AND parcelles_cen.geom IS NOT NULL
  GROUP BY parcelles_cen.par_id, parcelles_cen.codcom, parcelles_cen.ccosec, parcelles_cen.dnupla, parcelles_cen.dcntpa, cadastre_site.site_id, trmu.surf_m2, trmu.geom, lots_cen.geom
UNION
 SELECT DISTINCT - row_number() OVER (ORDER BY t1.site_id) AS gid,
    'N.P.'::text AS par_id,
    'N.P.'::text AS codcom,
    'N.P.'::text AS ccosec,
    0 AS dnupla,
    0 AS dcntpa,
    t1.site_id,
    'Ces données ne sont pas à jour'::text AS mfu,
    0 AS lng_m,
    0 AS surf_m2,
    st_multi(st_envelope(COALESCE(sites.geom_ecolo, sites.geom_mfu, sites.geom_cad)))::geometry(MultiPolygon,2154) AS geom
   FROM ( SELECT DISTINCT v_prob_mfu_baddata.site_id
           FROM alertes.v_prob_mfu_baddata
        UNION
         SELECT DISTINCT v_prob_cadastre_cen.site_id
           FROM alertes.v_prob_cadastre_cen
          WHERE v_prob_cadastre_cen.statutmfu_id = 1
        UNION
         SELECT DISTINCT sites_1.site_id
           FROM sites.sites sites_1
          WHERE sites_1.geom_mfu IS NULL AND "left"(sites_1.typsite_id::text, 1) = '1'::text
        UNION
         SELECT DISTINCT "substring"(v_prob_mfu_sansparc.mfu_id::text, 4, 4) AS site_id
           FROM alertes.v_prob_mfu_sansparc) t1
     JOIN sites.sites USING (site_id)
  WHERE t1.site_id IS NOT NULL AND t1.site_id <> ''::text AND t1.site_id <> 'PCEN'::text
WITH DATA;
ALTER TABLE sites.v_sites_parcelles
  OWNER TO foncier;
GRANT ALL ON TABLE sites.v_sites_parcelles TO grp_sig;
GRANT SELECT ON TABLE sites.v_sites_parcelles TO cen_user;
COMMENT ON MATERIALIZED VIEW sites.v_sites_parcelles
  IS 'REFRESH MATERIALIZED VIEW sites.v_sites_parcelles;';

CREATE INDEX v_sites_parcelles_geom_gist
  ON sites.v_sites_parcelles
  USING gist
  (geom);

-- View: sites.v_sites_mfu
DROP VIEW IF EXISTS sites.v_sites_mfu;
CREATE OR REPLACE VIEW sites.v_sites_mfu AS 
 SELECT row_number() OVER (ORDER BY sites.site_id) AS gid,
    sites.site_id,
    sites.site_nom,
    d_typsite.typsite_lib,
    d_milieux.milieu_lib,
    st_multi(st_union(v_sites_parcelles.geom))::geometry(MultiPolygon,2154) AS geom
   FROM sites.sites
     JOIN sites.d_typsite USING (typsite_id)
     JOIN sites.d_milieux USING (milieu_id)
     JOIN sites.v_sites_parcelles USING (site_id)
  WHERE (sites.typsite_id::text <> ALL (ARRAY['3'::character varying::text, '4'::character varying::text])) AND v_sites_parcelles.mfu <> 'Pas de maîtrise'::text AND v_sites_parcelles.mfu <> 'Ces données ne sont pas à jour'::text
  GROUP BY sites.site_id, sites.site_nom, d_typsite.typsite_lib, d_milieux.milieu_lib
  ORDER BY sites.site_id;
ALTER TABLE sites.v_sites_mfu
  OWNER TO grp_sig;
