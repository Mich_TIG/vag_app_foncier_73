﻿CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;

CREATE SCHEMA animation_fonciere
  AUTHORIZATION grp_sig;
GRANT ALL ON SCHEMA animation_fonciere TO grp_sig;
GRANT USAGE ON SCHEMA animation_fonciere TO grp_animation_fonciere;

CREATE TABLE animation_fonciere.d_saf_origines
(
  saf_origine_id smallint NOT NULL,
  saf_origine_lib character varying(100),
  CONSTRAINT d_saf_origines_pkey PRIMARY KEY (saf_origine_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_saf_origines
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_saf_origines TO grp_sig;
GRANT SELECT ON TABLE animation_fonciere.d_saf_origines TO grp_animation_fonciere;

INSERT INTO animation_fonciere.d_saf_origines(saf_origine_id, saf_origine_lib) VALUES
	(1, 'Plan de gestion'),  
	(2, 'Animation territoriale'),  
	(3, 'Opportunité'),  
	(4, 'Transfert de gestion / cogestion');  

CREATE SEQUENCE animation_fonciere.session_af_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.session_af_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.session_af_id_seq TO grp_sig;
GRANT USAGE ON SEQUENCE animation_fonciere.session_af_id_seq TO grp_animation_fonciere;

CREATE TABLE animation_fonciere.session_af
(
    session_af_id integer NOT NULL DEFAULT nextval('animation_fonciere.session_af_id_seq'::regclass),
    session_af_lib character varying(100) NOT NULL,
    date_creation character varying(10) NOT NULL,
    date_maj character varying(19) NOT NULL,
    utilisateur character varying(25) NOT NULL,
    origine_id integer,
    geom geometry(MultiPolygon,2154),
    CONSTRAINT session_af_pk PRIMARY KEY (session_af_id),
    CONSTRAINT session_af_utilisateur_id_fk FOREIGN KEY (utilisateur)
      REFERENCES admin_sig.utilisateurs (utilisateur_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT session_af_origine_id_fk FOREIGN KEY (origine_id)
      REFERENCES animation_fonciere.d_saf_origines (saf_origine_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.session_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.session_af TO grp_sig;
GRANT SELECT, UPDATE, INSERT ON TABLE animation_fonciere.session_af TO grp_animation_fonciere;

CREATE INDEX fki_session_af_id_fkey
  ON animation_fonciere.session_af
  USING btree
  (session_af_id);

CREATE INDEX session_af_geom_gist
  ON animation_fonciere.session_af
  USING gist
  (geom);
  
  
CREATE SEQUENCE animation_fonciere.entite_af_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.entite_af_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.entite_af_id_seq TO grp_sig;
GRANT USAGE ON SEQUENCE animation_fonciere.entite_af_id_seq TO grp_animation_fonciere;

CREATE TABLE animation_fonciere.entite_af
(
    entite_af_id integer NOT NULL DEFAULT nextval('animation_fonciere.entite_af_id_seq'::regclass),
	commentaires text,
    CONSTRAINT entite_af_pk PRIMARY KEY (entite_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.entite_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.entite_af TO grp_sig;
GRANT SELECT, UPDATE, INSERT ON TABLE animation_fonciere.entite_af TO grp_animation_fonciere;

CREATE INDEX fki_entite_af_id_fkey
  ON animation_fonciere.entite_af
  USING btree
  (entite_af_id); 



 -- Table: animation_fonciere.d_etat_interloc
CREATE TABLE animation_fonciere.d_etat_interloc
(
  etat_interloc_id smallint NOT NULL,
  etat_interloc_lib character varying(25),
  CONSTRAINT d_etat_interloc_pkey PRIMARY KEY (etat_interloc_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_etat_interloc
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_etat_interloc TO grp_sig;   
GRANT SELECT ON TABLE animation_fonciere.d_etat_interloc TO grp_animation_fonciere;   

INSERT INTO animation_fonciere.d_etat_interloc(etat_interloc_id, etat_interloc_lib) VALUES
	(1, 'Actif'),
	(2, 'Introuvable'),
	(3, 'Injoignable'),
	(4, 'Décédé');	  


CREATE SEQUENCE animation_fonciere.interloc_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.interloc_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.interloc_id_seq TO grp_sig;
GRANT USAGE ON SEQUENCE animation_fonciere.interloc_id_seq TO grp_animation_fonciere;

-- Table: animation_fonciere.interlocuteurs

CREATE TABLE animation_fonciere.interlocuteurs
(
  interloc_id integer NOT NULL DEFAULT nextval('animation_fonciere.interloc_id_seq'::regclass),
  etat_interloc_id integer NOT NULL DEFAULT 1,
  dnuper character varying(8), -- n° de personne MAJIC
  ccoqua smallint, -- Code qualité de personne physique
  ddenom text, -- Dénomimation de personne physique ou morale
  nom_usage character varying(150),
  nom_jeunefille character varying(150),
  prenom_usage character varying(150),
  nom_conjoint character varying(150),
  prenom_conjoint character varying(150),
  dnomlp character varying(150), -- Nom d'usage
  dprnlp character varying(150), -- Prénoms associés au nom d'usage
  epxnee character varying(3), -- Mention du complément
  dnomcp character varying(150), -- Nom complément
  dprncp character varying(150), -- Prénoms associés au complément
  jdatnss character varying(10), -- Date de naissance de personne physique
  dldnss character varying(75), -- Lieu de naissance
  dlign3 text, -- Adresse1 de personne physique ou morale
  dlign4 text, -- Adresse2, etc de personne physique ou morale
  dlign5 text,
  dlign6 text,
  email character varying(50),
  fixe_domicile character varying(14),
  fixe_travail character varying(14),
  portable_domicile character varying(14),
  portable_travail character varying(14),
  fax character varying(14),
  gtoper smallint, -- Indicateur de personne physique ou morale
  ccogrm smallint, -- Groupe de personne morale
  dnatpr character varying(3), -- Nature de la personne
  dsglpm character varying(25), -- Sigle de la personne morale
  commentaires text,
  annee_matrice character varying(4),
  maj_user character varying(25),
  maj_date character varying(19),
  CONSTRAINT interlocuteurs_pkey PRIMARY KEY (interloc_id),
  CONSTRAINT interlocuteurs_etat_fkey FOREIGN KEY (etat_interloc_id)
      REFERENCES animation_fonciere.d_etat_interloc (etat_interloc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT interlocuteurs_ccogrm_fkey FOREIGN KEY (ccogrm)
      REFERENCES cadastre.d_ccogrm (ccogrm) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT interlocuteurs_ccoqua_fkey FOREIGN KEY (ccoqua)
      REFERENCES cadastre.d_ccoqua (ccoqua) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT interlocuteurs_dnatpr_fkey FOREIGN KEY (dnatpr)
      REFERENCES cadastre.d_dnatpr (dnatpr) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT interlocuteurs_gtoper_fkey FOREIGN KEY (gtoper)
      REFERENCES cadastre.d_gtoper (gtoper) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.interlocuteurs
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.interlocuteurs TO grp_sig;
																	
GRANT SELECT, INSERT, UPDATE ON TABLE animation_fonciere.interlocuteurs TO grp_animation_fonciere;

CREATE INDEX fki_interloc_id_fkey
  ON animation_fonciere.interlocuteurs
  USING btree
  (interloc_id);
  
-- Table: animation_fonciere.rel_proprio_interloc
CREATE TABLE animation_fonciere.rel_proprio_interloc
(
    dnuper character varying(8) NOT NULL,
    interloc_id integer NOT NULL,
    CONSTRAINT rel_proprio_interloc_pkey PRIMARY KEY (dnuper, interloc_id),
    CONSTRAINT rel_proprio_interloc_dnuper_fkey FOREIGN KEY (dnuper)
      REFERENCES cadastre.proprios_cen (dnuper) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_proprio_interloc_interloc_id_fkey FOREIGN KEY (interloc_id)
      REFERENCES animation_fonciere.interlocuteurs (interloc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_proprio_interloc
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_proprio_interloc TO grp_sig;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_proprio_interloc TO grp_animation_fonciere;

-- Table: animation_fonciere.d_objectif_af

CREATE TABLE animation_fonciere.d_objectif_af
(
  objectif_af_id smallint NOT NULL,
  objectif_af_lib character varying(50),
  CONSTRAINT d_objectif_af_pkey PRIMARY KEY (objectif_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_objectif_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_objectif_af TO grp_sig; 
GRANT SELECT ON TABLE animation_fonciere.d_objectif_af TO grp_animation_fonciere; 

INSERT INTO animation_fonciere.d_objectif_af(objectif_af_id, objectif_af_lib) VALUES
	(1, 'Objectif d''acquisition'),
	(2, 'Objectif de convention'),
	(3, 'Pas d''objectif');

-- Table: animation_fonciere.d_paf

CREATE TABLE animation_fonciere.d_paf
(
  paf_id smallint NOT NULL,
  paf_lib text,
  paf_descrip text,
  CONSTRAINT d_paf_pkey_new PRIMARY KEY (paf_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_paf
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_paf TO grp_sig;
GRANT SELECT ON TABLE animation_fonciere.d_paf TO grp_animation_fonciere;

INSERT INTO animation_fonciere.d_paf(paf_id, paf_lib) VALUES
	(0, 'Pas dans le paf'),
	(1, 'Priorité d''animation 1'),
	(2, 'Priorité d''animation 2'),
	(3, 'Priorité d''animation 3');


-- Sequence: animation_fonciere.rel_eaf_foncier_id_seq

CREATE SEQUENCE animation_fonciere.rel_eaf_foncier_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.rel_eaf_foncier_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.rel_eaf_foncier_id_seq TO grp_sig;  
GRANT USAGE ON SEQUENCE animation_fonciere.rel_eaf_foncier_id_seq TO grp_animation_fonciere;  


CREATE TABLE animation_fonciere.rel_eaf_foncier
(
    rel_eaf_foncier_id integer NOT NULL DEFAULT nextval('animation_fonciere.rel_eaf_foncier_id_seq'::regclass),
    cad_site_id integer,
    entite_af_id integer,
	utilissol text,
    CONSTRAINT rel_eaf_foncier_pkey PRIMARY KEY (rel_eaf_foncier_id),
    CONSTRAINT rel_eaf_foncier_cad_site_id_fkey FOREIGN KEY (cad_site_id)
      REFERENCES foncier.cadastre_site (cad_site_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_eaf_foncier_entite_af_id_fkey FOREIGN KEY (entite_af_id)
      REFERENCES animation_fonciere.entite_af (entite_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_eaf_foncier_chk1 UNIQUE (cad_site_id, entite_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_eaf_foncier
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_eaf_foncier TO grp_sig;
																	 
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_eaf_foncier TO grp_animation_fonciere;

CREATE INDEX fki_rel_eaf_foncier_id_fkey
  ON animation_fonciere.rel_eaf_foncier
  USING btree
  (rel_eaf_foncier_id);	
	
	
-- Sequence: animation_fonciere.rel_saf_foncier_id_seq

CREATE SEQUENCE animation_fonciere.rel_saf_foncier_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.rel_saf_foncier_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.rel_saf_foncier_id_seq TO grp_sig;  
GRANT USAGE ON SEQUENCE animation_fonciere.rel_saf_foncier_id_seq TO grp_animation_fonciere;  


CREATE TABLE animation_fonciere.rel_saf_foncier
(
    rel_saf_foncier_id integer NOT NULL DEFAULT nextval('animation_fonciere.rel_saf_foncier_id_seq'::regclass),
    rel_eaf_foncier_id integer,
    session_af_id integer,
    objectif_af_id integer DEFAULT 1,
    paf_id integer,
	prix_parc_arrondi numeric(10,2),
	prix_eaf_arrondi numeric(10,2),
	prix_eaf_negocie numeric(10,2),
	lot_bloque boolean DEFAULT false,
	surf_mu_init numeric(10,2),
	commentaires text,
    geom geometry(MultiPolygon,2154),    
    CONSTRAINT rel_saf_foncier_pkey PRIMARY KEY (rel_saf_foncier_id),
    CONSTRAINT rel_saf_foncier_rel_eaf_foncier_id_fkey FOREIGN KEY (rel_eaf_foncier_id)
      REFERENCES animation_fonciere.rel_eaf_foncier (rel_eaf_foncier_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_session_af_id_fkey FOREIGN KEY (session_af_id)
      REFERENCES animation_fonciere.session_af (session_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_objectif_af_id_fkey FOREIGN KEY (objectif_af_id)
      REFERENCES animation_fonciere.d_objectif_af (objectif_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_paf_id_fkey FOREIGN KEY (paf_id)
      REFERENCES animation_fonciere.d_paf (paf_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_chk1 UNIQUE (rel_eaf_foncier_id, session_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_saf_foncier
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_saf_foncier TO grp_sig;
																	 
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_saf_foncier TO grp_animation_fonciere;

CREATE INDEX fki_rel_saf_foncier_id_fkey
  ON animation_fonciere.rel_saf_foncier
  USING btree
  (rel_saf_foncier_id);
  
 -- Table: animation_fonciere.d_groupe_interloc
CREATE TABLE animation_fonciere.d_groupe_interloc
(
  groupe_interloc_id smallint NOT NULL,
  groupe_interloc_lib character varying(50),
  groupe_interloc_color character varying(7),
  all_eaf boolean,  
  can_be_ref boolean,
  can_be_under_ref boolean,
  can_be_subst boolean,
  can_be_under_subst boolean,
  CONSTRAINT d_groupe_interloc_pkey PRIMARY KEY (groupe_interloc_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_groupe_interloc
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_groupe_interloc TO grp_sig;   
GRANT SELECT ON TABLE animation_fonciere.d_groupe_interloc TO grp_animation_fonciere;   

INSERT INTO animation_fonciere.d_groupe_interloc(groupe_interloc_id, groupe_interloc_lib, groupe_interloc_color, all_eaf, can_be_ref, can_be_under_ref, can_be_subst, can_be_under_subst) VALUES
	(1, 'Véritable propriétaire', '#004494', 'f', 't', 't', 'f', 't'),	
	(2, 'Ayants-droit', '#2b5824', 'f', 't', 't', 'f', 't'), 
	(3, 'Représentant', '#000000', 'f', 'f', 'f', 't', 'f'),
	(4, 'Interlocuteur de substitution', '#7500be', 't', 't', 't', 't', 'f'),
	(5, 'Intermédiaire', '#5a5a5a', 't', 'f', 'f', 'f', 'f'); 
  
 -- Table: animation_fonciere.d_type_interloc
CREATE TABLE animation_fonciere.d_type_interloc
(
  type_interloc_id smallint NOT NULL,
  type_interloc_lib character varying(100),
  groupe_interloc_id smallint NOT NULL,
  can_nego boolean,
  avis_favorable boolean,
  CONSTRAINT d_type_interloc_pkey PRIMARY KEY (type_interloc_id),
  CONSTRAINT d_type_interloc_groupe_interloc_id_fkey FOREIGN KEY (groupe_interloc_id)
      REFERENCES animation_fonciere.d_groupe_interloc (groupe_interloc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_type_interloc
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_type_interloc TO grp_sig;   
GRANT SELECT ON TABLE animation_fonciere.d_type_interloc TO grp_animation_fonciere;   

INSERT INTO animation_fonciere.d_type_interloc(type_interloc_id, type_interloc_lib, groupe_interloc_id, can_nego, avis_favorable) VALUES
	(1, 'Véritable propriétaire', 1, 't', 'f'),
	
	(2, 'Agriculteur (bail rural)', 2, 't', 'f'),
	(3, 'Agriculteur (contrat d''exploitation)', 2, 'f', 't'),
	(4, 'Agriculteur sans droit ni titre', 2, 'f', 't'),
	(5, 'Agriculteur sans contrat identifié', 2, 'f', 't'),
	(6, 'Chasseur (bail de chasse)', 2, 'f', 't'),
	(7, 'Pêcheur (bail de pêche)', 2, 'f', 't'),
	
	(8, 'Représentant', 3, 'f', 'f'),
	
	(9, 'Mandataire spécial', 4, 't', 'f'),
	(10, 'Curateur', 4, 't', 'f'),
	(11, 'Tuteur', 4, 't', 'f'),
	(12, 'Mandataire', 4, 't', 'f'),
		
	(13, 'Notaire', 5, 'f', 'f'),
	(14, 'Généalogiste', 5, 'f', 'f'),
	(15, 'Service de la Publicité Foncière', 5, 'f', 'f'),
	(16, 'Archives cantonales (CH)', 5, 'f', 'f'),
	(17, 'Contrôle des habitants (CH)', 5, 'f', 'f'),
	(18, 'Office cantonal de la population et des migrations (CH)', 5, 'f', 'f'),
	(19, 'Registre foncier (CH)', 5, 'f', 'f'),
	(20, 'Justice de paix (CH)', 5, 'f', 'f');
	 
 -- Sequence: animation_fonciere.rel_saf_foncier_interlocs_id_seq
CREATE SEQUENCE animation_fonciere.rel_saf_foncier_interlocs_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.rel_saf_foncier_interlocs_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.rel_saf_foncier_interlocs_id_seq TO grp_sig;
GRANT USAGE ON SEQUENCE animation_fonciere.rel_saf_foncier_interlocs_id_seq TO grp_animation_fonciere;

CREATE TABLE animation_fonciere.rel_saf_foncier_interlocs
(
    rel_saf_foncier_interlocs_id integer NOT NULL DEFAULT nextval('animation_fonciere.rel_saf_foncier_interlocs_id_seq'::regclass),
    interloc_id integer,
    rel_saf_foncier_id integer,
    type_interloc_id integer,
    ccodro character varying(1), -- Code du droit réel ou particulier
    ccodem character varying(1), -- Code du démembrement/indivision
    commentaires text,
    CONSTRAINT rel_saf_foncier_interlocs_pkey PRIMARY KEY (rel_saf_foncier_interlocs_id),   
    CONSTRAINT rel_saf_foncier_interlocs_interloc_id_fkey FOREIGN KEY (interloc_id)
      REFERENCES animation_fonciere.interlocuteurs (interloc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_interlocs_rel_saf_foncier_id_fkey FOREIGN KEY (rel_saf_foncier_id)
      REFERENCES animation_fonciere.rel_saf_foncier (rel_saf_foncier_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_interlocs_type_interloc_id_fkey FOREIGN KEY (type_interloc_id)
      REFERENCES animation_fonciere.d_type_interloc (type_interloc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_interlocs_ccodem_fkey FOREIGN KEY (ccodem)
      REFERENCES cadastre.d_ccodem (ccodem) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_interlocs_ccodro_fkey FOREIGN KEY (ccodro)
      REFERENCES cadastre.d_ccodro (ccodro) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_interlocs_id_unique_key UNIQUE (interloc_id, rel_saf_foncier_id, type_interloc_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_saf_foncier_interlocs
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_saf_foncier_interlocs TO grp_sig;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_saf_foncier_interlocs TO grp_animation_fonciere;

CREATE INDEX fki_rel_saf_foncier_interlocs_id_fkey
  ON animation_fonciere.rel_saf_foncier_interlocs
  USING btree
  (rel_saf_foncier_interlocs_id); 

  
--Table animation_fonciere.interloc_substitution
CREATE TABLE animation_fonciere.interloc_substitution
(
    rel_saf_foncier_interlocs_id_vp integer NOT NULL, --veritable proprietaire
	rel_saf_foncier_interlocs_id_is integer NOT NULL, --interlocuteur de substitution
    CONSTRAINT interloc_substitution_pkey PRIMARY KEY (rel_saf_foncier_interlocs_id_vp, rel_saf_foncier_interlocs_id_is),
    CONSTRAINT interloc_substitution_vp_id_fkey FOREIGN KEY (rel_saf_foncier_interlocs_id_vp)
      REFERENCES animation_fonciere.rel_saf_foncier_interlocs (rel_saf_foncier_interlocs_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT interloc_substitution_is_id_fkey FOREIGN KEY (rel_saf_foncier_interlocs_id_is)
      REFERENCES animation_fonciere.rel_saf_foncier_interlocs (rel_saf_foncier_interlocs_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.interloc_substitution
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.interloc_substitution TO grp_sig;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.interloc_substitution TO grp_animation_fonciere;

--Table animation_fonciere.interloc_referent
CREATE TABLE animation_fonciere.interloc_referent
(
    rel_saf_foncier_interlocs_id_ref integer NOT NULL, --veritable proprietaire
	rel_saf_foncier_interlocs_id integer NOT NULL, --interlocuteur de substitution
    CONSTRAINT interloc_referent_pkey PRIMARY KEY (rel_saf_foncier_interlocs_id_ref, rel_saf_foncier_interlocs_id),
    CONSTRAINT interloc_referent_interloc_ref_id_fkey FOREIGN KEY (rel_saf_foncier_interlocs_id_ref)
      REFERENCES animation_fonciere.rel_saf_foncier_interlocs (rel_saf_foncier_interlocs_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT interloc_referent_interloc_id_fkey FOREIGN KEY (rel_saf_foncier_interlocs_id)
      REFERENCES animation_fonciere.rel_saf_foncier_interlocs (rel_saf_foncier_interlocs_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.interloc_referent
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.interloc_referent TO grp_sig;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.interloc_referent TO grp_animation_fonciere;
  
-- Table: animation_fonciere.d_types_echanges_af

CREATE TABLE animation_fonciere.d_types_echanges_af
(
  type_echanges_af_id smallint NOT NULL,
  type_echanges_af_lib character varying(50),
  CONSTRAINT d_type_echanges_af_pkey PRIMARY KEY (type_echanges_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_types_echanges_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_types_echanges_af TO grp_sig;  
GRANT SELECT ON TABLE animation_fonciere.d_types_echanges_af TO grp_animation_fonciere;  


INSERT INTO animation_fonciere.d_types_echanges_af(type_echanges_af_id, type_echanges_af_lib) VALUES
	(1, 'Courrier individuel'),
	(2, 'Mail'),
	(3, 'Téléphone'),
	(4, 'Rencontre individuelle');
	
-- Table: animation_fonciere.d_nego_af

CREATE TABLE animation_fonciere.d_result_echanges_af
(
  result_echange_af_id smallint NOT NULL,
  result_echange_af_lib character varying(50),
  CONSTRAINT d_result_echanges_af_pkey PRIMARY KEY (result_echange_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_result_echanges_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_result_echanges_af TO grp_sig;  
GRANT SELECT ON TABLE animation_fonciere.d_result_echanges_af TO grp_animation_fonciere;  

INSERT INTO animation_fonciere.d_result_echanges_af(result_echange_af_id, result_echange_af_lib) VALUES
	(1, 'Interlocuteur injoignable'),
	(2, 'En attente d''une réponse'),
	(3, 'Modification des informations personnelles'); 		

-- Sequence: animation_fonciere.echanges_foncier_echange_id_seq

CREATE SEQUENCE animation_fonciere.echange_af_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.echange_af_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.echange_af_id_seq TO grp_sig;  
GRANT USAGE ON SEQUENCE animation_fonciere.echange_af_id_seq TO grp_animation_fonciere;  

-- Table: animation_fonciere.echanges_af

CREATE TABLE animation_fonciere.echanges_af
(
  echange_af_id integer NOT NULL DEFAULT nextval('animation_fonciere.echange_af_id_seq'::regclass),
  echange_af_date character varying(10),
  type_echanges_af_id smallint,
  echange_af_description text,
  result_echange_af_id integer,
  echange_referent boolean,
  CONSTRAINT echanges_foncier_pkey PRIMARY KEY (echange_af_id),
  CONSTRAINT echanges_foncier_typech_id_fkey FOREIGN KEY (type_echanges_af_id)
      REFERENCES animation_fonciere.d_types_echanges_af (type_echanges_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT echanges_foncier_result_echange_af_id_fkey FOREIGN KEY (result_echange_af_id)
      REFERENCES animation_fonciere.d_result_echanges_af (result_echange_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.echanges_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.echanges_af TO grp_sig;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE animation_fonciere.echanges_af TO grp_animation_fonciere;
  

CREATE INDEX fki_echange_af_id_fkey
  ON animation_fonciere.echanges_af
  USING btree
  (echange_af_id);
	
-- Table: animation_fonciere.d_nego_af

CREATE TABLE animation_fonciere.d_nego_af
(
  nego_af_id smallint NOT NULL,
  nego_af_lib character varying(50),
  CONSTRAINT d_nego_af_pkey PRIMARY KEY (nego_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_nego_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_nego_af TO grp_sig;  
GRANT SELECT ON TABLE animation_fonciere.d_nego_af TO grp_animation_fonciere;  

INSERT INTO animation_fonciere.d_nego_af(nego_af_id, nego_af_lib) VALUES
	(0, 'Refus'),
	(1, 'Perspective d''acte'),
	(2, 'Perspective de convention'),
	(3, 'Perspective d''acquisition'),
	(4, 'Accord pour une convention'),
	(5, 'Accord pour une acquisition'),
	(6, 'Favorable'),
	(7, 'Défavorable');   
  
CREATE SEQUENCE animation_fonciere.rel_af_echanges_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.rel_af_echanges_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.rel_af_echanges_id_seq TO grp_sig;
GRANT USAGE ON SEQUENCE animation_fonciere.rel_af_echanges_id_seq TO grp_animation_fonciere;

CREATE TABLE animation_fonciere.rel_af_echanges
(
    rel_af_echanges_id integer NOT NULL DEFAULT nextval('animation_fonciere.rel_af_echanges_id_seq'::regclass),
    echange_af_id integer NOT NULL,
	rel_saf_foncier_interlocs_id integer,
	rel_saf_foncier_representant_id integer,
	nego_af_id smallint,
	nego_af_prix numeric,
    date_recontact character varying(10),
    commentaires text,
	rappel_traite boolean,
	echange_direct boolean,
    CONSTRAINT rel_af_echanges_pkey PRIMARY KEY (rel_af_echanges_id),   
    CONSTRAINT echange_af_id_fkey FOREIGN KEY (echange_af_id)
      REFERENCES animation_fonciere.echanges_af (echange_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT rel_saf_foncier_interlocs_id_fkey FOREIGN KEY (rel_saf_foncier_interlocs_id)
      REFERENCES animation_fonciere.rel_saf_foncier_interlocs (rel_saf_foncier_interlocs_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT rel_saf_foncier_representant_id_fkey FOREIGN KEY (rel_saf_foncier_representant_id)
      REFERENCES animation_fonciere.rel_saf_foncier_interlocs (rel_saf_foncier_interlocs_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT nego_af_id_fkey FOREIGN KEY (nego_af_id)
      REFERENCES animation_fonciere.d_nego_af (nego_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_af_echanges_unique_key UNIQUE (echange_af_id, rel_saf_foncier_interlocs_id)
)

WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_af_echanges
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_af_echanges TO grp_sig;
																	 
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_af_echanges TO grp_animation_fonciere;

																								  

-- Table: animation_fonciere.d_types_evenements_af

CREATE TABLE animation_fonciere.d_types_evenements_af
(
  type_evenements_af_id smallint NOT NULL,
  type_evenements_af_lib character varying(50),
  CONSTRAINT d_type_evenements_af_pkey PRIMARY KEY (type_evenements_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_types_evenements_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_types_evenements_af TO grp_sig;  
GRANT SELECT ON TABLE animation_fonciere.d_types_evenements_af TO grp_animation_fonciere;


INSERT INTO animation_fonciere.d_types_evenements_af(type_evenements_af_id, type_evenements_af_lib) VALUES
	(1, 'Réunion de lancement'),
	(2, 'Réunion courante'),
	(3, 'Courrier collectif'),
	(4, 'Rencontre collective'),
	(5, 'Autre');
	
-- Sequence: animation_fonciere.echanges_foncier_echange_id_seq

CREATE SEQUENCE animation_fonciere.evenement_af_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 2147483647
  START 1
  CACHE 1;
ALTER TABLE animation_fonciere.evenement_af_id_seq
  OWNER TO grp_sig;
GRANT ALL ON SEQUENCE animation_fonciere.evenement_af_id_seq TO grp_sig;  	
GRANT USAGE ON SEQUENCE animation_fonciere.evenement_af_id_seq TO grp_animation_fonciere;  	

CREATE TABLE animation_fonciere.evenements_af
(
  evenement_af_id integer NOT NULL DEFAULT nextval('animation_fonciere.evenement_af_id_seq'::regclass),
  evenement_af_date character varying(10),
  type_evenements_af_id smallint,
  evenement_af_description text,
  CONSTRAINT evenements_foncier_pkey PRIMARY KEY (evenement_af_id),
  CONSTRAINT evenements_foncier_type_evenements_af_id_fkey FOREIGN KEY (type_evenements_af_id)
      REFERENCES animation_fonciere.d_types_evenements_af (type_evenements_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.evenements_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.evenements_af TO grp_sig;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE animation_fonciere.evenements_af TO grp_animation_fonciere;

CREATE TABLE animation_fonciere.rel_af_evenements
(
    session_af_id integer NOT NULL,
    evenement_af_id integer NOT NULL,
    interloc_id integer NOT NULL,
    CONSTRAINT rel_af_evenements_pkey PRIMARY KEY (session_af_id, evenement_af_id, interloc_id),   
    CONSTRAINT rel_af_evenements_session_af_id_fkey FOREIGN KEY (session_af_id)
      REFERENCES animation_fonciere.session_af (session_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT rel_af_evenements_evenement_af_id_fkey FOREIGN KEY (evenement_af_id)
      REFERENCES animation_fonciere.evenements_af (evenement_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT rel_af_evenements_interloc_id_fkey FOREIGN KEY (interloc_id)
      REFERENCES animation_fonciere.interlocuteurs (interloc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_af_evenements
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_af_evenements TO grp_sig;
																	   
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_af_evenements TO grp_animation_fonciere;



-- Table: animation_fonciere.d_type_occsol_af

CREATE TABLE animation_fonciere.d_type_occsol_af
(
  type_occsol_af_id smallint NOT NULL,
  type_occsol_af_lib text,
  CONSTRAINT d_occsol_af_pkey PRIMARY KEY (type_occsol_af_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.d_type_occsol_af
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.d_type_occsol_af TO grp_sig;
GRANT SELECT ON TABLE animation_fonciere.d_type_occsol_af TO grp_animation_fonciere;

INSERT INTO animation_fonciere.d_type_occsol_af(type_occsol_af_id, type_occsol_af_lib) VALUES
	(0, 'Prairie'),
	(1, 'Culture'),
	(2, 'Marais'),
	(3, 'Pelouse sèche'),
	(4, 'Boisement'),
	(5, 'Plantation'),
	(6, 'Lande/ friche'),
	(7, 'Eau libre'),
	(8, 'Milieu artificialisé'); 

CREATE TABLE animation_fonciere.rel_saf_foncier_occsol
(
    rel_saf_foncier_id integer NOT NULL,
    type_occsol_af_id integer NOT NULL,
	pourcentage integer NOT NULL,
	montant numeric,
	commentaire text,
    CONSTRAINT rel_saf_foncier_occsol_pkey PRIMARY KEY (rel_saf_foncier_id, type_occsol_af_id),
    CONSTRAINT rel_saf_foncier_occsol_rel_saf_foncier_id_fkey FOREIGN KEY (rel_saf_foncier_id)
      REFERENCES animation_fonciere.rel_saf_foncier (rel_saf_foncier_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT rel_saf_foncier_occsol_occsol_af_id_fkey FOREIGN KEY (type_occsol_af_id)
      REFERENCES animation_fonciere.d_type_occsol_af (type_occsol_af_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_saf_foncier_occsol
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_saf_foncier_occsol TO grp_sig;
																			
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_saf_foncier_occsol TO grp_animation_fonciere;


CREATE TABLE animation_fonciere.rel_saf_foncier_prix
(
    rel_saf_foncier_id integer NOT NULL,
    prix_lib character varying(60) NOT NULL,
    montant numeric NOT NULL,
	quantite integer NOT NULL,
    CONSTRAINT rel_saf_foncier_prix_pkey PRIMARY KEY (rel_saf_foncier_id, prix_lib),
    CONSTRAINT rel_saf_foncier_prix_rel_saf_foncier_id_fkey FOREIGN KEY (rel_saf_foncier_id)
      REFERENCES animation_fonciere.rel_saf_foncier (rel_saf_foncier_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION   
)
WITH (
  OIDS=FALSE
);
ALTER TABLE animation_fonciere.rel_saf_foncier_prix
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.rel_saf_foncier_prix TO grp_sig;
																		  
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE animation_fonciere.rel_saf_foncier_prix TO grp_animation_fonciere;

-- Function: animation_fonciere.f_get_eaf(integer)

-- DROP FUNCTION animation_fonciere.f_get_eaf(integer);

CREATE OR REPLACE FUNCTION animation_fonciere.f_get_eaf(id_cad_site integer)
  RETURNS SETOF integer AS
$BODY$ 
	SELECT DISTINCT 
		entite_af_id
	FROM
		cadastre.cptprop_cen
		JOIN cadastre.cadastre_cen USING (dnupro)
		JOIN foncier.cadastre_site USING (cad_cen_id)
		JOIN animation_fonciere.rel_eaf_foncier USING (cad_site_id)
	WHERE dnupro IN (
		SELECT DISTINCT r_prop_cptprop_cen.dnupro--, array_agg(r_prop_cptprop_cen.dnuper ORDER BY r_prop_cptprop_cen.dnuper)
		FROM cadastre.r_prop_cptprop_cen, (
			SELECT array_agg(r_prop_cptprop_cen.dnuper ORDER BY r_prop_cptprop_cen.dnuper) as lst_dnuper
			FROM 
				foncier.cadastre_site
				JOIN cadastre.cadastre_cen USING (cad_cen_id)
				JOIN cadastre.cptprop_cen USING (dnupro)
				JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
			WHERE cad_site_id IN (id_cad_site)) t1
		GROUP BY r_prop_cptprop_cen.dnupro, t1.lst_dnuper
		HAVING t1.lst_dnuper = array_agg(r_prop_cptprop_cen.dnuper ORDER BY r_prop_cptprop_cen.dnuper))

	UNION

	SELECT 
		entite_af_id
	FROM
		(SELECT DISTINCT 
			interloc_id,
			entite_af_id
		FROM 
			animation_fonciere.rel_eaf_foncier
			JOIN animation_fonciere.rel_saf_foncier USING (rel_eaf_foncier_id)
			JOIN animation_fonciere.rel_saf_foncier_interlocs USING (rel_saf_foncier_id)
			JOIN animation_fonciere.interlocuteurs USING (interloc_id)
			LEFT JOIN animation_fonciere.rel_proprio_interloc USING (interloc_id)) t2,
		(SELECT array_agg(rel_proprio_interloc.interloc_id ORDER BY rel_proprio_interloc.interloc_id) as lst_interloc
		FROM 
			foncier.cadastre_site
			JOIN cadastre.cadastre_cen USING (cad_cen_id)
			JOIN cadastre.cptprop_cen USING (dnupro)
			JOIN cadastre.r_prop_cptprop_cen USING (dnupro)
			JOIN cadastre.proprios_cen USING (dnuper)
			LEFT JOIN animation_fonciere.rel_proprio_interloc USING (dnuper)
		WHERE cad_site_id IN (id_cad_site)) t1
	GROUP BY entite_af_id, t1.lst_interloc
	HAVING t1.lst_interloc = array_agg(interloc_id ORDER BY interloc_id);
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION animation_fonciere.f_get_eaf(integer)
  OWNER TO grp_sig;
COMMENT ON FUNCTION animation_fonciere.f_get_eaf(integer) IS 'SELECT admin_sig.f_get_eaf(15295);';
GRANT EXECUTE ON FUNCTION animation_fonciere.f_get_eaf(integer) TO grp_sig;
GRANT EXECUTE ON FUNCTION animation_fonciere.f_get_eaf(integer) TO grp_animation_fonciere;

-- FUNCTION: animation_fonciere.parcelles_saf_pour_partie_on_update()

-- DROP FUNCTION animation_fonciere.parcelles_saf_pour_partie_on_update();

CREATE OR REPLACE FUNCTION animation_fonciere.parcelles_saf_pour_partie_on_update()
    RETURNS trigger AS
	$BODY$
	DECLARE
    
    surface_conv_origine integer;
    geometry_origine geometry;    
    contains text;
    surface_conv integer;
    
  BEGIN
	
	geometry_origine := (SELECT geom FROM cadastre.parcelles_cen WHERE par_id = OLD.par_id);	
	surface_conv_origine := (SELECT surf_mu_init FROM animation_fonciere.rel_saf_foncier WHERE rel_saf_foncier_id = OLD.rel_saf_foncier_id);	
	contains := (st_contains(st_multi(st_buffer(geometry_origine, 0.5))::geometry(MultiPolygon,2154), st_multi(st_buffer(NEW.geom, -0.5))::geometry(MultiPolygon,2154))); surface_conv := (round((st_area(NEW.geom)::numeric), 0));
	
	IF ((contains = 't') OR (contains = 'true')) THEN
		IF (surface_conv_origine IS NULL) THEN
			UPDATE animation_fonciere.rel_saf_foncier SET geom = NEW.geom, surf_mu_init = surface_conv WHERE rel_saf_foncier_id = OLD.rel_saf_foncier_id;
		ELSE
			UPDATE animation_fonciere.rel_saf_foncier SET geom = NEW.geom WHERE rel_saf_foncier_id = OLD.rel_saf_foncier_id;
		END IF;
	ELSE
		RAISE EXCEPTION 'Votre modification ne peut pas être supérieure à la parcelle d''origine - % - %', contains, OLD.par_id;
	END IF;
	
  RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION animation_fonciere.parcelles_saf_pour_partie_on_update()
    OWNER TO grp_sig;
GRANT EXECUTE ON FUNCTION animation_fonciere.parcelles_saf_pour_partie_on_update() TO grp_sig;
GRANT EXECUTE ON FUNCTION animation_fonciere.parcelles_saf_pour_partie_on_update() TO grp_animation_fonciere;



-- View: animation_fonciere.parcelles_dispo_saf

CREATE OR REPLACE VIEW animation_fonciere.parcelles_dispo_saf AS 
 SELECT row_number() OVER (ORDER BY t1.par_id) AS gid,
    sites.site_id,
    t1.par_id,
    array_to_string(array_agg(t4.cad_site_id), ','::text) AS tbl_cad_site_id,
    t1.codcom,
    t1.ccosec,
    t1.dnupla,
    array_to_string(array_agg(t2.dnulot), ','::text) AS tbl_dnulot,
    t1.geom
   FROM foncier.cadastre_site t4,
    cadastre.cadastre_cen t3,
    cadastre.lots_cen t2,
    cadastre.parcelles_cen t1,
    sites.sites
  WHERE sites.site_id::text = t4.site_id::text AND t1.par_id::text = t2.par_id::text AND t2.lot_id::text = t3.lot_id::text AND t3.cad_cen_id = t4.cad_cen_id AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text AND NOT (t4.cad_site_id IN ( SELECT DISTINCT t4_1.cad_site_id
           FROM foncier.cadastre_site t4_1,
            cadastre.cadastre_cen t3_1,
            foncier.r_cad_site_mf,
            foncier.mf_acquisitions,
            foncier.r_mfstatut,
            foncier.d_typmf,
            ( SELECT r_mfstatut_1.mf_id,
                    max(r_mfstatut_1.date::text) AS statut_date
                   FROM foncier.r_mfstatut r_mfstatut_1
                  GROUP BY r_mfstatut_1.mf_id
                  ORDER BY r_mfstatut_1.mf_id) tmax
          WHERE mf_acquisitions.typmf_id = d_typmf.typmf_id AND d_typmf.typmf_lib::text <> 'Acquisition par un tiers'::text AND t3_1.cad_cen_id = t4_1.cad_cen_id AND r_cad_site_mf.cad_site_id = t4_1.cad_site_id AND r_cad_site_mf.mf_id::text = mf_acquisitions.mf_id::text AND r_mfstatut.mf_id::text = mf_acquisitions.mf_id::text AND r_mfstatut.date::text = tmax.statut_date AND r_mfstatut.mf_id::text = tmax.mf_id::text AND r_mfstatut.statutmf_id = 1::smallint))
  GROUP BY sites.site_id, t1.codcom, t1.ccosec, t1.dnupla, t1.geom, t1.par_id
  ORDER BY sites.site_id;

ALTER TABLE animation_fonciere.parcelles_dispo_saf
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.parcelles_dispo_saf TO grp_sig;
GRANT SELECT ON TABLE animation_fonciere.parcelles_dispo_saf TO grp_animation_fonciere;

-- View: animation_fonciere.parcelles_saf_pour_partie

CREATE OR REPLACE VIEW animation_fonciere.parcelles_saf_pour_partie AS 
 SELECT row_number() OVER (ORDER BY t1.par_id) AS gid,
    t4.site_id,
    rel_saf_foncier.session_af_id,
    rel_saf_foncier.rel_saf_foncier_id,
    t1.par_id,
    t4.cad_site_id::text AS cad_site_id,
    t1.codcom,
    t1.ccosec,
    t1.dnupla,
        CASE
            WHEN rel_saf_foncier.geom IS NULL THEN t1.geom
            ELSE rel_saf_foncier.geom
        END AS geom
   FROM animation_fonciere.rel_saf_foncier
     JOIN animation_fonciere.rel_eaf_foncier USING (rel_eaf_foncier_id)
     JOIN foncier.cadastre_site t4 USING (cad_site_id)
     JOIN cadastre.cadastre_cen t3 USING (cad_cen_id)
     JOIN cadastre.lots_cen t2 USING (lot_id)
     JOIN cadastre.parcelles_cen t1 USING (par_id)
     JOIN ( SELECT parcelles_cen.par_id
           FROM cadastre.parcelles_cen
             JOIN cadastre.lots_cen USING (par_id)
          GROUP BY parcelles_cen.par_id
         HAVING count(lots_cen.lot_id) = 1) t_nbr_lot USING (par_id)
  WHERE rel_saf_foncier.objectif_af_id = 2 AND t4.date_fin::text = 'N.D.'::text AND t3.date_fin::text = 'N.D.'::text
  ORDER BY t4.site_id;

ALTER TABLE animation_fonciere.parcelles_saf_pour_partie
  OWNER TO grp_sig;
GRANT ALL ON TABLE animation_fonciere.parcelles_saf_pour_partie TO grp_sig;
GRANT SELECT ON TABLE animation_fonciere.parcelles_saf_pour_partie TO grp_animation_fonciere;

-- Rule: parcelles_saf_pour_partie_on_delete ON animation_fonciere.parcelles_saf_pour_partie

-- DROP RULE parcelles_saf_pour_partie_on_delete ON animation_fonciere.parcelles_saf_pour_partie;

CREATE OR REPLACE RULE parcelles_saf_pour_partie_on_delete AS
    ON DELETE TO animation_fonciere.parcelles_saf_pour_partie DO INSTEAD NOTHING;

-- Rule: parcelles_saf_pour_partie_on_insert ON animation_fonciere.parcelles_saf_pour_partie

-- DROP RULE parcelles_saf_pour_partie_on_insert ON animation_fonciere.parcelles_saf_pour_partie;

CREATE OR REPLACE RULE parcelles_saf_pour_partie_on_insert AS
    ON INSERT TO animation_fonciere.parcelles_saf_pour_partie DO INSTEAD NOTHING;


-- Trigger: parcelles_saf_pour_partie_on_update on animation_fonciere.parcelles_saf_pour_partie

-- DROP TRIGGER parcelles_saf_pour_partie_on_update ON animation_fonciere.parcelles_saf_pour_partie;

CREATE TRIGGER parcelles_saf_pour_partie_on_update
  INSTEAD OF UPDATE
  ON animation_fonciere.parcelles_saf_pour_partie
  FOR EACH ROW
  EXECUTE PROCEDURE animation_fonciere.parcelles_saf_pour_partie_on_update();

